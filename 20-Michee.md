# Miykayah (Michée) (Mi.)

Signification : Qui est comme Yah ?

Auteur : Miykah ou Miykayah (Michée)

Thème : Le jugement et le royaume

Date de rédaction : 8ème siècle av. J.-C.

Originaire de Morésheth, Miykayah exerça son service dans le royaume du sud au temps de Yehizqiyah (Ézéchias), roi de Yéhouda. Il était contemporain d'Hoshea (Osée), d'Amowc (Amos) et de Yesha`yah (Ésaïe). Alors que la corruption et l'idolâtrie régnaient en Samarie et à Yeroushalaim (Jérusalem), Miykayah appela le peuple à se détourner de ses iniquités et le prévint du danger qui le menaçait. Il prédit également le rétablissement final de la nation juive et mit en exergue la miséricorde divine.

## Chapitre 1

1:1	Parole de YHWH qui apparut à Miykah de Morésheth aux jours de Yotham, d'Achaz et de Yehizqiyah, rois de Yéhouda - ce qu'il a vu au sujet de Samarie et de Yeroushalaim.
1:2	Écoutez, vous tous, peuples ! Sois attentive, Terre, toi et ce qui te remplit ! Qu'Adonaï YHWH soit témoin contre vous, Adonaï, du palais de sa sainteté !
1:3	Car voici, YHWH sortira de son lieu, il descendra et marchera sur les hauts lieux de la Terre<!--Cette prophétie annonce à la fois la destruction du royaume du nord par Salmanasar V (règne de 727 - 722 av. J.-C.) en 722 av. J.-C. (2 R. 17:1-23), l'invasion de Sanchérib (règne de 705 - 681 av. J.-C.) (2 R. 18:13-19:37) ainsi que celle de Neboukadnetsar (règne de 604 - 562 av. J.-C.) (2 R. 24 et 25).-->.
1:4	Les montagnes se fondront sous lui, et les vallées se fendront, elles seront comme de la cire devant le feu et comme des eaux qui coulent sur une pente.
1:5	Tout cela arrivera à cause de la transgression de Yaacov et à cause des péchés de la maison d'Israël. Quelle est la transgression de Yaacov ? N'est-ce pas Samarie ? Et quels sont les hauts lieux de Yéhouda ? N'est-ce pas Yeroushalaim ?
1:6	Je ferai de Samarie un monceau dans les champs, un lieu où l'on plante des vignes. Je ferai rouler ses pierres dans la vallée, et je découvrirai ses fondements.
1:7	Toutes ses images gravées seront brisées, tous ses salaires de prostituée seront brûlés au feu, et je mettrai tous ses faux elohîm en désolation, car elle les a rassemblés avec le salaire des prostituées et ils retourneront aussi au salaire de prostituées.
1:8	C'est pourquoi je gémirai et je hurlerai, je m'en irai dépouillé et nu, je ferai des gémissements comme les dragons et je mènerai le deuil comme les filles de l'autruche.
1:9	Car sa plaie est incurable, elle atteint même Yéhouda. Elle frappe jusqu'à la porte de mon peuple, jusqu'à Yeroushalaim.
1:10	Ne l'annoncez pas dans Gath, ne pleurez pas ! Ne pleurez pas ! Roule-toi dans la poussière à Beth-Leaphra.
1:11	Passe, habitante de Shaphir, dans la nudité et la honte ! L'habitante de Tsaanan n'est pas sortie, le gémissement de Beth-Haëtsel vous prive de son abri.
1:12	L'habitante de Maroth est dans l'angoisse à cause de son bien-être, parce que le malheur est descendu de la part de YHWH jusqu'à la porte de Yeroushalaim.
1:13	Attache le cheval au char, habitante de Lakish ! Le commencement du péché de la fille de Sion, car en toi ont été trouvées les transgressions d'Israël.
1:14	C'est pourquoi tu donneras des cadeaux d'adieu à Moréshet-Gath. Les maisons d'Aczib seront un mensonge pour les rois d'Israël.
1:15	Je t'amènerai un autre héritier, habitante de Maréshah, et la gloire d'Israël s'en ira jusqu'à Adoullam.
1:16	Rends-toi chauve, fais-toi tondre à cause de tes fils qui font tes délices ! Élargis ta calvitie comme le vautour, car ils s'en vont en exil loin de toi.

## Chapitre 2

2:1	Malheur à ceux qui projettent la méchanceté et qui forgent le mal sur leurs lits ! À la lumière du matin, ils l’exécutent, parce qu'ils ont le pouvoir en main.
2:2	Ils convoitent des possessions et s'en emparent<!--Voir 1 Roi 21:1-16.--> : des maisons, et ils les enlèvent. Ainsi ils oppriment l'homme fort et sa maison, l'homme et son héritage.
2:3	C'est pourquoi ainsi parle YHWH : Voici, je projette contre cette famille un malheur dont vous n'enleverez votre cou, et vous ne marcherez pas la tête levée, car ce sera un temps de malheur.
2:4	En ce jour-là, on fera de vous le sujet d'un proverbe, on gémira d'un gémissement, d'un gémissement, en disant : Nous sommes dévastés, dévastés, la part de mon peuple, il la change de mains ! Comment nous enlève-t-il et partage-t-il notre terre à l'infidèle ?
2:5	C'est pourquoi il n'y aura personne qui jettera le cordeau pour ton lot dans l'assemblée de YHWH.
2:6	Ne prophétisez pas ! disent-ils. Ne prophétisez pas de telles choses ! L'opprobre ne s'éloignera pas !
2:7	La maison de Yaacov dit : L'Esprit de YHWH est-il impatient<!--Signifie aussi « être court », « être vexé », « être affligé ».--> ? Sont-ce là ses actes ? Mes paroles ne sont-elles pas bonnes pour celui qui marche droit ?
2:8	Hier mon peuple s’est levé en ennemi : vous dépouillez le manteau de dessus le vêtement à ceux qui passent avec sécurité en revenant de la guerre.
2:9	Vous chassez les femmes de mon peuple hors des maisons de leurs délices, vous ôtez pour toujours ma gloire de dessus leurs enfants.
2:10	Levez-vous et marchez ! Car ce n’est pas ici un lieu de repos. À cause de la souillure, il vous détruira d'une violente destruction.
2:11	Si un homme marchant selon le vent et le mensonge, ment en disant : Je te prophétiserai sur du vin et sur les boissons fortes, il deviendra le prophète de ce peuple.
2:12	Je te rassemblerai tout entier, Yaacov ! Et je recueillerai, je recueillerai les restes d'Israël et je les réunirai comme les brebis d'une bergerie, comme un troupeau au milieu de son pâturage. Il y aura un grand bruit à cause de la foule d'êtres humains.
2:13	Celui qui fait la brèche monte devant eux ; ils font la brèche, passeront la porte et sortiront par elle. Leur Roi passera devant eux, YHWH sera à leur tête.

## Chapitre 3

3:1	Je dis : Écoutez, s'il vous plaît, têtes de Yaacov, et vous conducteurs de la maison d'Israël ! N'est-ce pas à vous de connaître ce qui est juste ?
3:2	Vous haïssez ce qui est bon et vous aimez ce qui est mauvais. Vous leur arrachez la peau et la chair de dessus les os.
3:3	Vous dévorez la chair de mon peuple et vous lui écorchez la peau : ils brisent les os et les mettent en pièces comme dans un pot, comme de la viande dans une chaudière.
3:4	Alors ils crieront vers YHWH, mais il ne leur répondra pas. Il leur cachera ses faces en ce temps-là, parce qu'ils se sont mal conduits dans leurs actions.
3:5	Ainsi parle YHWH contre les prophètes qui égarent mon peuple, qui proclament la paix quand leurs dents ont de quoi mordre, et si quelqu'un ne leur met rien dans la bouche, ils publient la guerre contre lui.
3:6	C'est pourquoi la nuit sera sur vous afin que vous n'ayez plus de vision ! Elle s'obscurcira afin que vous ne deviniez plus ! Le soleil se couchera sur ces prophètes-là et le jour sera ténébreux pour eux.
3:7	Les voyants seront honteux et les devins seront confondus, ils se couvriront tous la barbe, parce qu'il n'y aura aucune réponse d'Elohîm.
3:8	Mais moi, je suis rempli de force, de justice et de courage par l'Esprit de YHWH, pour déclarer à Yaacov sa transgression et à Israël, son péché.
3:9	Écoutez ceci, s'il vous plaît, têtes de la maison de Yaacov, et vous conducteurs de la maison d'Israël, vous qui avez la justice en abomination et qui pervertissez tout ce qui est droit,
3:10	vous qui bâtissez Sion avec le sang, et Yeroushalaim avec l'injustice !
3:11	Ses têtes jugent pour des pots-de-vin<!--Présents.-->, ses prêtres enseignent pour un prix, et ses prophètes devinent pour de l'argent, ensuite ils s'appuient sur YHWH, en disant : YHWH n'est-il pas parmi nous ? Le mal ne nous atteindra pas.
3:12	C'est pourquoi, à cause de vous, Sion sera labourée comme un champ, et Yeroushalaim sera réduite en ruine, et la montagne du temple en hauts lieux d'une forêt.

## Chapitre 4

4:1	Il arrivera dans les derniers jours<!--Voir commentaire en Ge. 49:1-2.-->, que la montagne de la maison de YHWH<!--Dans les Écritures, les montagnes symbolisent parfois une grande puissance terrestre, et les collines celles de moindre importance. Cette prophétie confirme l'établissement du Royaume messianique dont la capitale sera Yeroushalaim (Jérusalem) (2 S. 7:14-16). Yesha`yah (Ésaïe) avait reçu la même prophétie que l'on peut découvrir au chapitre 2 de son livre.--> sera établie au sommet des montagnes. Elle s'élèvera au-dessus des collines et toutes les nations y afflueront.
4:2	Et beaucoup de nations iront et diront : Venez et montons à la montagne de YHWH, à la maison de l'Elohîm de Yaacov ! Il nous enseignera ses voies et nous marcherons dans ses sentiers. Car la torah sortira de Sion, et la parole de YHWH de Yeroushalaim.
4:3	Il jugera au milieu de beaucoup de peuples, il réprimandera des nations puissantes et lointaines. De leurs épées elles forgeront des hoyaux, de leurs lances des serpes : une nation ne lèvera plus l'épée contre une nation, on n'apprendra plus la guerre.
4:4	Chaque homme s'assiéra sous sa vigne et sous son figuier, et il n'y aura personne pour les effrayer, car la bouche de YHWH Tsevaot a parlé.
4:5	Oui, tous les peuples marchent, chaque homme au nom de son elohîm<!--1 Ch. 16:26.-->, mais nous, nous marchons au Nom de YHWH, notre Elohîm, pour toujours et à perpétuité.
4:6	En ce jour-là, - déclaration de YHWH -, je rassemblerai les boiteux, je recueillerai ceux que j'avais bannis et ceux auxquels j’avais fait du mal.
4:7	Je ferai des boiteux un reste et de ceux qui étaient éloignés, une nation puissante. YHWH régnera sur eux à la montagne de Sion, dès lors et pour toujours.
4:8	Et toi, tour du troupeau, la colline de la fille de Sion viendra jusqu'à toi, et la première domination viendra, le royaume sera à la fille de Yeroushalaim.
4:9	Maintenant, pourquoi pousses-tu des cris ? N'y a-t-il pas de roi au milieu de toi ? Ou ton conseiller a-t-il péri, car la douleur t’a saisie comme celle qui enfante ?
4:10	Tords-toi et gémis, fille de Sion, comme celle qui enfante ! Car maintenant tu sortiras de la ville, tu habiteras dans les champs et tu iras jusqu'à Babel ! Là tu seras délivrée, c'est là que YHWH te rachètera de la paume de tes ennemis.
4:11	Maintenant beaucoup de nations se sont rassemblées contre toi<!--Il est ici question de la guerre d'Harmaguédon. Voir commentaire en Ap. 16:12-16.--> et disent : Qu'elle soit profanée ! Et que nos yeux regardent Sion<!--Yeroushalaim (Jérusalem) est une horloge d'Elohîm. Le Seigneur a fait en sorte que les nations aient les yeux toujours tournés vers ce bout de terre, car c'est là que débutera la troisième guerre mondiale, l'Harmaguédon (Mt. 24:15-28 ; Ap. 16:12-16, 19:11-21). Là aura lieu le jugement des nations, dans la vallée de Yehoshaphat (Joë. 4:2-12). Le Mashiah (Christ) reviendra (Es. 59:20-21 ; Za. 14:1-8 ; Ac. 1:10-11) et gouvernera le monde depuis Yeroushalaim (Za. 14:9-21).-->.
4:12	Mais elles ne connaissent pas les pensées de YHWH et ne comprennent pas ses desseins, car il les a rassemblées comme des gerbes dans l'aire.
4:13	Lève-toi et foule, fille de Sion ! Car je te ferai une corne de fer et je te ferai des sabots de cuivre, tu écraseras des peuples nombreux, tu consacreras par interdit leurs profits à YHWH et leurs richesses au Seigneur de toute la Terre.
4:14	Maintenant, rassemble tes troupes, fille de troupes ! On a mis le siège contre nous, on frappera le juge d'Israël avec la verge sur la joue.

## Chapitre 5

5:1	Mais toi, Bethléhem Éphrata, petite pour être parmi les milliers de Yéhouda, de toi sortira pour moi celui qui dominera sur Israël, dont les origines remontent à l’antiquité, aux jours éternels<!--Il est question ici de Yéhoshoua ha Mashiah (Jésus-Christ). Ce passage nous parle de sa préexistence éternelle. Les pharisiens, scribes et principaux prêtres avaient la connaissance de cette prophétie concernant le Mashiah (Mt. 2:1-6).-->.
5:2	C'est pourquoi il les livrera jusqu'au temps où enfantera celle qui doit enfanter, et le reste de ses frères retournera avec les fils d'Israël.
5:3	Il se tiendra debout et fera paître avec la force de YHWH, avec la majesté du Nom de YHWH son Elohîm. Ils auront une habitation, car maintenant il sera grand jusqu’aux extrémités de la Terre.
5:4	Celui-ci sera paix ! Quand l'Assyrien viendra sur notre terre, quand il foulera nos palais, nous élèverons contre lui sept bergers et huit princes d’entre les humains.
5:5	Ils ravageront la terre d'Assyrie avec l'épée et la terre de Nimrod à ses portes. Il nous délivrera de l'Assyrien, s’il venait contre notre terre, s’il foulait notre territoire.
5:6	Le reste de Yaacov deviendra, au milieu de peuples nombreux, comme une rosée qui vient de YHWH, comme de grosses averses sur l'herbe, qui n'espèrent rien de l'homme, qui n'attendent rien des fils des humains.
5:7	Le reste de Yaacov deviendra, parmi les nations, au milieu de peuples nombreux, comme un lion parmi les bêtes de la forêt, comme un lionceau parmi les troupeaux de brebis qui, en passant, foule et déchire, sans que personne ne puisse les sauver.
5:8	Ta main se lèvera sur tes adversaires et tous tes ennemis seront exterminés.
5:9	Il arrivera en ce jour-là, - déclaration de YHWH -, que j'exterminerai du milieu de toi tes chevaux et je détruirai tes chars.
5:10	J'exterminerai les villes de ta terre et je renverserai toutes tes forteresses.
5:11	J'exterminerai de ta main les sorcelleries et tu n'auras plus aucun enchanteur.
5:12	J'exterminerai du milieu de toi tes images gravées et tes monuments, et tu ne te prosterneras plus devant l'ouvrage de tes mains.
5:13	J'arracherai du milieu de toi les asherah<!--Ex. 34:13.--> et je détruirai tes villes.
5:14	J'exercerai ma vengeance avec colère et avec courroux contre toutes les nations qui ne m'auront pas écouté.

## Chapitre 6

6:1	Écoutez, s'il vous plaît, ce que dit YHWH : Lève-toi, plaide devant les montagnes et que les collines entendent ta voix !
6:2	Écoutez le procès de YHWH, montagnes, et vous, solides fondations de la Terre ! Car YHWH a un procès avec son peuple et il plaidera avec Israël.
6:3	Mon peuple, que t'ai-je fait, ou en quoi t'ai-je causé de la peine ? Réponds-moi !
6:4	Car je t'ai fait monter hors de la terre d'Égypte, je t'ai délivré de la maison des esclaves et j'ai envoyé devant toi Moshé, Aaron et Myriam.
6:5	Mon peuple souviens-toi, s'il te plaît, du conseil qu'avait donné Balak, roi de Moab et de ce que lui répondit Balaam, fils de Béor, depuis Shittim jusqu'à Guilgal, afin que tu connaisses la justice de YHWH.
6:6	Avec quoi irai-je à la rencontre de YHWH et me courberai-je devant l'Elohîm Très-Haut ? Irai-je à sa rencontre avec des holocaustes et avec des veaux, fils d'un an ?
6:7	YHWH prendra-t-il plaisir à des milliers de béliers ou à des myriades de torrents d'huile ? Donnerai-je pour ma transgression mon premier-né<!--Selon la torah (Ex. 13:2,12), les premiers-nés de l'homme et des animaux appartenaient au Seigneur. Ceux des animaux étaient offerts en sacrifice ; et quant à ceux des hommes, le sacrifice était fait de manière symbolique par une présentation au temple. En Israël, le sacrifice des enfants était formellement interdit sous peine de mort (Lé. 18:21, 20:2-5 ; De. 12:31, 18:10).-->, le fruit de mes entrailles pour le péché de mon âme ?
6:8	Humain ! Il t'a fait connaître ce qui est bon et ce que YHWH exige de toi : que tu fasses ce qui est juste, que tu aimes la miséricorde et que tu marches en toute humilité avec ton Elohîm.
6:9	La voix de YHWH appelle la ville : la sagesse, c’est de considérer son Nom. Écoutez la verge et celui qui l'a fixée !
6:10	Y a-t-il encore dans la maison du méchant des trésors de méchanceté, et un épha maigre, exprimant de l'indignation ?
6:11	Est-on pur avec des balances de méchanceté et avec un sac aux poids trompeurs ? 
6:12	Ses riches sont pleins de violence, ses habitants usent de mensonge et ils ont une langue trompeuse dans leur bouche.
6:13	C'est pourquoi je te rendrai malade en te frappant, je te ravagerai à cause de tes péchés.
6:14	Tu mangeras, mais tu ne seras pas rassasiée, et ton ventre sera vide. Tu mettras de côté, mais tu n'en sauveras rien. Ce que tu auras sauvé, je le livrerai à l'épée.
6:15	Tu sèmeras mais tu ne moissonneras pas, tu presseras l'olive mais tu ne t'oindras pas d'huile, et le vin nouveau, mais tu ne boiras pas le vin.
6:16	On observe les statuts d'Omri et toutes les œuvres de la maison d'Achab, vous marchez selon leurs conseils. C'est pourquoi je te livrerai à la désolation, je ferai de tes habitants un objet de raillerie et vous porterez l'insulte de mon peuple.

## Chapitre 7

7:1	Malheur à moi ! Car je suis comme à la récolte des fruits d'été, comme aux grappillages de la vendange : il n'y a ni grappe pour manger, ni les premiers fruits que mon âme désire.
7:2	Le fidèle est exterminé de la terre et il n'y a plus de juste parmi les humains. Ils sont tous en embuscade pour verser le sang, chaque homme fait la chasse à son frère avec des filets.
7:3	Pour faire du mal, leurs paumes sont prêtes : le gouverneur pratique la mendicité, le juge demande une rétribution, le grand ne parle que du désir de son âme, et ils tissent cela ensemble.
7:4	Le meilleur d'entre eux est comme une ronce et le plus juste est pire qu'une haie d'épines. Le jour annoncé par tes sentinelles, ton châtiment arrive. C'est alors qu'ils seront dans la confusion.
7:5	Ne croyez pas à un ami, ne faites pas confiance à un intime. Garde-toi d'ouvrir ta bouche devant celle qui dort dans ton sein.
7:6	Car le fils traite son père avec mépris, la fille s'élève contre sa mère, la belle-fille contre sa belle-mère, et l'homme a pour ennemis les personnes de sa maison<!--Mt. 10:35-36.-->.
7:7	Mais moi, je regarderai vers YHWH, je m'attendrai à l'Elohîm de mon salut, mon Elohîm m'écoutera.
7:8	Toi mon ennemie, ne te réjouis pas à mon sujet ! Même si je suis tombée, je me relèverai. Même si je suis assise dans les ténèbres, YHWH m'éclairera.
7:9	Je supporterai la fureur de YHWH, car j'ai péché contre lui, jusqu'à ce qu'il défende ma cause et qu'il me fasse justice : il me conduira à la lumière, je verrai sa justice.
7:10	Et mon ennemie le verra, et la honte la couvrira, elle qui me disait : Où est YHWH, ton Elohîm ? Mes yeux la verront, et alors elle sera foulée aux pieds comme la boue des rues.
7:11	Le jour où il rebâtira tes murs, en ce jour-là tes limites seront reculées.
7:12	En ce jour-là on viendra jusqu'à toi d'Assyrie et des villes d'Égypte, et depuis les villes d'Égypte jusqu'au fleuve, et depuis une mer jusqu'à l'autre mer, et depuis une montagne jusqu'à l'autre montagne.
7:13	La terre deviendra une désolation à cause de ses habitants, à cause du fruit de leurs actions.
7:14	Pais ton peuple avec ta houlette, le troupeau de ton héritage, qui demeure seul dans les forêts au milieu de Carmel. Qu'ils paissent en Bashân et en Galaad, comme aux temps anciens.
7:15	Je lui ferai voir des choses merveilleuses, comme au jour où tu sortis de la terre d'Égypte.
7:16	Les nations le verront et elles seront honteuses avec toute leur force. Elles mettront la main sur la bouche et leurs oreilles seront sourdes.
7:17	Elles lécheront la poussière comme le serpent, comme les reptiles de la Terre. Elles trembleront dans leurs forteresses et sortiront tout effrayées vers YHWH, notre Elohîm, elles te craindront.
7:18	Quel el est semblable à toi, qui pardonne l'iniquité et qui passe par-dessus les transgressions du reste de son héritage ? Il ne garde pas pour toujours sa colère, parce qu'il prend plaisir à la miséricorde.
7:19	Il nous fera encore miséricorde, il assujettira nos iniquités. Tu jetteras tous nos péchés au fond de la mer.
7:20	Tu feras voir ta fidélité à Yaacov et ta miséricorde à Abraham, comme tu l'as juré à nos pères dès les temps anciens<!--Les versets de Mi. 7:18-20 sont lus chaque année dans les synagogues le jour des expiations.-->.
