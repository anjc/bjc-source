# Habaqqouq (Habakuk) (Ha.)

Signification : Embrasser, amour

Auteur : Habaqqouq

Thème : Du doute à la foi

Date de rédaction : 7ème siècle av. J.-C.

Habaqqouq, contemporain de Nahoum, Tsephanyah (Sophonie) et Yirmeyah (Jérémie), exerça son service dans le royaume de Yéhouda (Juda). Véritable sentinelle, il fut chargé d'annoncer le châtiment de Yéhouda par les Chaldéens. Ce récit, qui est en partie un dialogue entre Elohîm et Habaqqouq, témoigne de la relation qui les liait. Il est aussi une invitation à la patience et à la foi en YHWH.

## Chapitre 1

### Quand la méchanceté semble triompher de la justice

1:1	Fardeau. Ce que Habaqqouq, le prophète, a vu.
1:2	Jusqu'à quand, YHWH, appellerai-je au secours sans que tu écoutes, crierai-je vers toi : Violence ! sans que tu sauves ?
1:3	Pourquoi me fais-tu voir la méchanceté<!--La perplexité d'Habaqqouq était la même que celle de Iyov (Job 21:7), d'Asaph (Ps. 73) et de Yirmeyah (Jérémie) (Jé. 12:1-2). Les méchants semblent prospérer tandis que les justes pleurent et sont persécutés (Mal. 3:12-15).--> et regardes-tu le malheur ? Ravage et violence sont devant moi, il y a des querelles, et la dispute s'élève.
1:4	Alors, la torah est engourdie et le jugement ne sort jamais. Alors le méchant environne le juste, et le jugement sort tordu<!--Jé. 5:26 ; Am. 5:7.-->.

### La réponse de YHWH

1:5	Regardez parmi les nations, voyez, soyez étonnés et stupéfaits ! Car je vais faire en vos jours une œuvre que vous ne croiriez pas si on vous la racontait<!--Ac. 13:41.-->.
1:6	Car voici, je vais susciter les Chaldéens, ce peuple cruel et impétueux, marchant sur l'étendue de la terre pour prendre possession des tabernacles qui ne lui appartiennent pas.
1:7	Il est redoutable et terrible, sa justice et sa dignité viennent de lui-même.
1:8	Ses chevaux sont plus légers que les léopards, plus tranchants que les loups du soir. Ses cavaliers gambadent, ses cavaliers viennent de loin, ils volent comme un aigle qui fond sur sa proie<!--Jé. 5:6 ; So. 3:3.-->.
1:9	Ils viennent tous pour la violence. L'assemblage de leurs faces est comme le vent d'orient, et ils rassemblent les captifs comme du sable.
1:10	Ce peuple se moque des rois, et les princes sont l'objet de ses dérisions. Il se rit de toutes les forteresses : il entasse de la poussière et les prend !
1:11	Alors il passe comme le vent, il passe outre et se rend coupable, car sa force est son elohîm.

### YHWH est souverain

1:12	N'es-tu pas de toute éternité, YHWH, mon Elohîm, mon Saint<!--Ma. 1:24 ; Lu. 1:35.--> ? Nous ne mourrons pas ! YHWH, tu l'as établi pour exécuter tes jugements. Rocher<!--Voir commentaire en Es. 8:13-14.-->, tu l'as fondé pour punir.
1:13	Tu as les yeux trop purs pour voir ce qui est mauvais, tu ne peux pas regarder le malheur. Pourquoi regarderais-tu les traîtres ? Pourquoi garderais-tu le silence quand un méchant dévore son prochain qui est plus juste que lui ?
1:14	Aurais-tu fait l'être humain pareil aux poissons de la mer, pareil aux choses rampantes sur lesquels personne ne domine ?
1:15	Il les fait tous monter avec un hameçon, les rassemble dans son filet et les réunit dans son filet de pêche. C'est pourquoi il exulte et se réjouit<!--Am. 4:2.-->.
1:16	À cause de cela, il sacrifie à son filet et il offre de l'encens à ses filets de pêche, parce que par leur moyen, sa portion est grasse et sa nourriture succulente.
1:17	Videra-t-il pour cela son filet et tuera-t-il toujours les nations sans épargner ?

## Chapitre 2

### Une sentinelle

2:1	Je me tiendrai à mon poste de garde<!--Es. 21:1-6 ; Jé. 6:17 ; Ez. 33:1-19.-->, je resterai debout sur le rempart. Je veillerai pour voir ce qu'il me dira et ce que je répliquerai au sujet de mes reproches.

### La rétribution des justes ne tardera pas

2:2	YHWH m'a répondu et m'a dit : Écris la vision et grave-la sur des tablettes afin que celui qui la lit puisse courir.
2:3	Car la vision est encore pour le temps fixé<!--Ge. 1:14.-->, et elle parle de ce qui arrivera à la fin, et elle ne mentira pas. Si elle tarde, attends-la, car venir, elle viendra<!--Voir commentaire en Ge. 2:17.-->, et elle ne tardera pas<!--Hé. 10:37.-->.
2:4	Voici, l'âme de celui qui s'élève n'est pas droite en lui, mais le juste vivra par sa foi<!--Ro. 1:17 ; Hé. 10:38.-->.
2:5	Oui, en effet le vin est traître : l'homme fort qui est arrogant n’aura pas d’oasis. Il élargit son âme comme le shéol. Comme la mort, il ne se rassasie pas. Il rassemble auprès de lui toutes les nations, il réunit auprès de lui tous les peuples.
2:6	Tous ceux-là n’élèveront-ils pas contre lui un proverbe, un chant moqueur et des énigmes sur lui ? Et on dira : Malheur à celui qui accumule ce qui ne lui appartient pas ! Jusqu'à quand ? À celui qui a des dettes lourdes et pesantes !
2:7	Ceux qui te prêtent avec intérêt ne se lèveront-ils pas soudainement ? Ne se réveilleront-ils pas pour te faire trembler ? Et tu deviendras leur proie.
2:8	Parce que tu as pillé beaucoup de nations, tout le reste des peuples te pillera, à cause du sang humain et de la violence contre la terre, la ville et tous ses habitants<!--Es. 33:1 ; Na. 3:1.-->.
2:9	Malheur à celui qui gagne par une violence injuste un profit malhonnête pour sa maison, afin de placer son nid dans un lieu élevé, pour être délivré de la paume du malheur !
2:10	C'est pour la honte de ta maison que tu as pris conseil en détruisant beaucoup de peuples, et c'est contre ton âme que tu as péché.
2:11	Car la pierre crie du milieu de la muraille, et de la charpente, la poutre lui répond.
2:12	Malheur à celui qui bâtit des villes avec du sang et qui établit des villes sur l'injustice.
2:13	Voici, n’est-ce pas par YHWH Tsevaot que les peuples se fatiguent pour du feu, et que les nations se lassent pour du vide ?
2:14	Car la Terre sera remplie de la connaissance de la gloire de YHWH<!--Es. 11:9.-->, comme le fond de la mer par les eaux qui le couvrent.
2:15	Malheur à celui qui fait boire son ami en lui approchant son outre et qui l'enivre afin qu'on voie sa nudité<!--Ge. 9:21-24 ; Es. 5:22.-->.
2:16	Tu seras rassasié de honte plutôt que de gloire. Toi aussi, bois et découvre-toi ! La coupe de la droite de YHWH fera le tour jusqu'à toi, et l'ignominie sera répandue sur ta gloire.
2:17	Car la violence contre le Liban retombera sur toi, ainsi que les ravages des bêtes épouvantées, à cause du sang humain, à cause de la violence contre la terre, la ville et tous ses habitants.
2:18	À quoi sert l'idole, pour qu'un ouvrier la taille ? Une image en métal fondu qui enseigne le mensonge, pour que l'ouvrier qui la façonne mette sa confiance en elle, faisant ainsi des faux elohîm muets ?
2:19	Malheur à ceux qui disent au bois : « Réveille-toi ! », à une pierre muette : « Réveille-toi ! » Enseignera-t-elle ? Voici, elle est couverte d'or et d'argent, et il n'y a aucun esprit au-dedans d'elle.
2:20	Mais YHWH est dans le temple de sa sainteté. Silence devant lui, toute la Terre !

## Chapitre 3

### Psaume d'Habaqqouq

3:1	Prière d'Habaqqouq, le prophète. Sur shiggayown<!--Terme musical, complainte ou hymne lyrique.-->.
3:2	YHWH, j'ai entendu ta rumeur, et j’ai eu peur. YHWH ! Fais revenir à la vie ton œuvre aux entrailles des années, fais-la connaître aux entrailles des années ! Dans la colère, souviens-toi de tes compassions !
3:3	Éloah vient de Théman, le Saint<!--Voir commentaire en Ac. 3:14.-->, du Mont de Paran, Sélah. Sa majesté couvre les cieux et la Terre est remplie de sa louange.
3:4	Son éclat devient comme une lumière aux cornes de sa main, là, dans la cachette de sa force.
3:5	La peste marche devant lui, une flamme sort de ses pieds.
3:6	Il se tient debout et fait trembler la Terre. Il regarde et secoue les nations. Les montagnes antiques se brisent et les collines éternelles s'affaissent. Ses marches sont éternelles.
3:7	Je vois les tentes de l'Éthiopie sous le malheur, les tapis de la terre de Madian tremblent.
3:8	Est-ce contre les fleuves que s'irrite YHWH ? Ta colère est-elle contre les fleuves, et ta fureur contre la mer, que tu sois monté sur tes chevaux et sur tes chars de salut ?
3:9	À nu ton arc se découvre, tes serments sont les flèches de ta parole. Sélah. Tu fends la Terre et tu en fais sortir des fleuves<!--Ps. 78:15-16, 105:41.-->.
3:10	Les montagnes te voient et elles tremblent<!--Ps. 114:4-7.-->. Des torrents d'eau se précipitent, l'abîme fait retentir sa voix de la profondeur, il élève ses mains en haut.
3:11	Le soleil, la lune s’arrêtent en leur résidence. Ils marchent à la lumière de tes flèches, à l’éclat de l’éclair de ta lance<!--Jos. 10:12 ; Ap. 22:5.-->.
3:12	Tu marches sur la Terre avec indignation, tu foules les nations avec colère.
3:13	Tu sors pour la délivrance de ton peuple, pour la délivrance de ton mashiah. Tu transperces le chef<!--Il s'agit de la lutte du Mashiah contre l'Anti-Mashiah (Antichrist). Voir Ps. 110:6.--> afin qu'il n'y en ait plus dans la maison du méchant, tu en découvres le fondement jusqu'au fond. Sélah.
3:14	Tu perces avec ses flèches la tête de ses chefs quand ils viennent comme une tempête pour me dissiper. Ils s'égayent comme pour dévorer l'affligé dans sa retraite.
3:15	Tu marches avec tes chevaux par la mer, dans les monceaux des grandes eaux.
3:16	J'ai entendu, et mon ventre s’agitait. À la voix, mes lèvres tremblotaient, la pourriture entrait dans mes os et je tremblai sous moi. Moi, qui serai en repos au jour de la détresse, quand montera contre le peuple celui qui l'envahira.
3:17	Car le figuier ne fleurira pas, il n'y aura pas de fruit dans les vignes, le fruit de l'olivier sera insuffisant, les champs ne donneront pas de nourriture. Les brebis seront exterminées de leurs enclos, et il n'y aura pas de bœufs dans les étables.
3:18	Mais moi, je triompherai en YHWH et j'exulterai dans l'Elohîm de ma délivrance.
3:19	YHWH Adonaï, ma force ! Il rend mes pieds semblables à ceux des biches et me fait marcher sur mes lieux élevés<!--De. 32:13 ; Ps. 18:33-34.-->. Au chef, sur instruments à cordes.
