# Hoshea (Osée) (Os.)

Signification : Salut, Sauve

Auteur : Hoshea (Osée)

Thème : Israël sera rejeté à cause de son apostasie. D'autres nations seront appelées à sa place.

Date de rédaction : 8ème siècle av. J.-C.

Hoshea (Osée), fils de Béeri, exerça son service dans le royaume du nord au temps de Yoash (Joas), roi d'Israël. Il était contemporain des prophètes Amowc (Amos), Miykayah (Michée) et Yesha`yah (Ésaïe).

YHWH demanda à Hoshea d'épouser une prostituée pour que le prophète puisse partager plus profondément son fardeau et la tristesse qu'il subissait en raison de l'infidélité du peuple qu'il aimait tant. Malgré la rébellion et les mauvais agissements d'Israël, YHWH manifesta une fois de plus sa patience et utilisa Hoshea pour avertir et inviter les fils de Yaacov (Jacob) à la repentance.

## Chapitre 1

1:1	La parole de YHWH qui vint à Hoshea, fils de Béeri, aux jours d'Ouzyah, de Yotham, d'Achaz et de Yehizqiyah, rois de Yéhouda, et aux jours de Yarobam, fils de Yoash, roi d'Israël.
1:2	Au commencement YHWH parla à Hoshea. YHWH dit à Hoshea : Va et prends pour toi une femme de prostitution et des enfants de prostitution, car la terre se prostitue, elle se prostitue loin derrière YHWH !
1:3	Il alla et prit Gomer, fille de Diblaïm. Elle devint enceinte et lui enfanta un fils.
1:4	Et YHWH lui dit : Appelle-le du nom de Yizre`e'l<!--Jizréel.-->, car encore un peu de temps, et je punirai la maison de Yehuw pour le sang versé à Yizre`e'l, et je ferai cesser le royaume de la maison d'Israël<!--Cette prophétie s'est accomplie en 722 av. J.-C. Voir 2 R. 17.-->.
1:5	Et il arrivera qu'en ce jour-là, je briserai l'arc d'Israël dans la vallée de Yizre`e'l.
1:6	Elle devint encore enceinte et enfanta une fille. Il lui dit : Appelle-la du nom de Lo-Rouhama<!--Tout au long de ce livre, certains mots sont symboliquement utilisés pour nommer Israël. À savoir :\\- « rouhama » : miséricorde ;\\- « Lo-Rouhama » : celle dont on ne fait pas miséricorde ;\\- « ammi » : mon peuple ;\\- « Lo-Ammi » : pas mon peuple.\\Ces noms illustrent ainsi l'infidélité d'Israël envers YHWH.-->, car je ne continuerai plus à faire miséricorde à la maison d'Israël, mais je les enlèverai, je les enlèverai.
1:7	Mais je ferai miséricorde à la maison de Yéhouda. Je les sauverai par YHWH, leur Elohîm, et je ne les sauverai ni par l'arc, ni par l'épée, ni par les combats, ni par les chevaux, ni par les cavaliers.
1:8	Elle sevra Lo-Rouhama, puis elle devint enceinte et enfanta un fils.
1:9	Et YHWH dit : Appelle-le du nom de Lo-Ammi, car vous n'êtes pas mon peuple, et je ne serai pas à vous.

## Chapitre 2

2:1	Le nombre des fils d'Israël deviendra comme le sable de la mer<!--Ge. 15:1-6.-->, qui ne peut ni se mesurer ni se compter. Il arrivera que dans le lieu où il leur est dit : Vous n'êtes pas mon peuple ! On leur dira : Vous êtes les fils du El Haï<!--El vivant.--> !
2:2	Les fils de Yéhouda et les fils d'Israël se réuniront ensemble, ils s'établiront un chef et monteront hors de la terre. Car grand sera le jour de Yizre`e'l.
2:3	Appelez vos frères : Ammi ! Et vos sœurs : Rouhama !
2:4	Plaidez, plaidez avec votre mère, car elle n'est pas ma femme, et je ne suis pas son homme ! Qu'elle ôte ses prostitutions de devant elle, et ses adultères de son sein !
2:5	De peur que je la dépouille pour qu'elle soit toute nue, que je ne la mette comme au jour de sa naissance, que je ne la réduise en désert, que je ne la rende comme une terre aride et ne la fasse mourir de soif.
2:6	Je n'aurai pas de miséricorde envers ses fils, car ce sont des fils de prostitution.
2:7	Parce que leur mère s'est prostituée, celle qui les a conçus s'est déshonorée. Car elle a dit : J'irai après ceux que j'aime, qui me donnent mon pain et mes eaux, ma laine et mon lin, mon huile et mes boissons.
2:8	C'est pourquoi voici, je boucherai ton chemin avec des épines, j'y élèverai un mur, afin qu'elle ne trouve plus ses sentiers.
2:9	Elle poursuivra ceux qui l'aiment, mais ne les atteindra pas. Elle les cherchera, mais elle ne les trouvera pas. Puis elle dira : J'irai, et je retournerai vers mon premier homme, car alors j'étais plus heureuse que maintenant.
2:10	Elle n'a pas su que c'était moi qui lui donnais le blé, le vin nouveau et l'huile, qui lui avais multiplié l'argent et l'or avec lesquels ils ont façonné un Baal<!--Baal : voir commentaire en Jg. 2:13.-->.
2:11	C'est pourquoi je reviendrai pour prendre mon blé en son temps, mon vin nouveau au temps fixé<!--Voir Ge. 1:14.-->, et je retirerai ma laine et mon lin qui couvraient sa nudité.
2:12	Et maintenant je découvrirai son impudicité aux yeux de ceux qui l'aiment, et aucun homme ne la délivrera de ma main.
2:13	Je ferai cesser toute sa joie, ses fêtes, ses nouvelles lunes, ses shabbats, et toutes ses solennités.
2:14	Je ravagerai ses vignes et ses figuiers, dont elle disait : Ce sont ici mes salaires que ceux qui m'aiment m'ont donnés ! J'en ferai une forêt, et les bêtes des champs les dévoreront.
2:15	Je la punirai pour les jours des Baalim auxquels elle brûlait de l’encens, quand elle se parait de ses anneaux et de ses colliers et qu'elle s'en allait après ceux qui l'aimaient ; et moi, elle m’oubliait, - déclaration de YHWH.
2:16	C’est pourquoi, voici que moi je l’attirerai, je la conduirai au désert et je parlerai à son cœur.
2:17	Je lui donnerai ses vignes depuis ce lieu-là, et la vallée d'Acor, pour la porte de son espoir, et là, elle chantera comme au temps de sa jeunesse, et comme au jour où elle monta de la terre d'Égypte.
2:18	Il arrivera en ce jour-là - déclaration de YHWH - que tu m'appelleras : Mon homme ! Tu ne m'appelleras plus : Mon Baal<!--Baal ne sera plus l'époux d'Israël.--> !
2:19	J'ôterai de sa bouche les noms des Baalim, et on ne fera plus mention de leurs noms.
2:20	Je traiterai pour eux une alliance, en ce jour, avec les bêtes des champs, avec les créatures volantes des cieux, et avec les choses rampantes du sol. Je briserai de la terre l'arc, l'épée et la guerre, et je les ferai se reposer en sécurité.
2:21	Je te fiancerai à moi pour toujours. Je te fiancerai à moi par la justice, la droiture, la bonté et la matrice,
2:22	et je te fiancerai par la fermeté, et tu connaîtras YHWH.
2:23	Et il arrivera en ce jour-là, que je répondrai, - déclaration de YHWH -, je répondrai aux cieux, et les cieux répondront à la terre,
2:24	et la terre répondra au blé, au vin nouveau et à l'huile, et ils répondront à Yizre`e'l.
2:25	Je la sèmerai pour moi sur la terre, et je ferai miséricorde à Lo-Rouhama. Je dirai à Lo-Ammi : Tu es mon peuple ! Et lui, il dira : Mon Elohîm !

## Chapitre 3

3:1	YHWH me dit : Va encore aimer une femme aimée d'un ami et adultère, selon l'amour de YHWH envers les fils d'Israël, qui toutefois se tournent vers d'autres elohîm et aiment les gâteaux de raisins.
3:2	Je l’achetai<!--« obtenir par commerce », « acheter », « traiter un marché ».--> pour 15 pièces d'argent, un omer et demi d'orge<!--Voir dans les annexes les tableaux des poids et mesures.-->.
3:3	Je lui dis : Tu resteras avec moi pendant beaucoup de jours, tu ne t'abandonneras plus à la prostitution, tu ne seras à aucun homme, et je serai aussi fidèle envers toi.
3:4	Car les fils d'Israël resteront beaucoup de jours sans roi, sans chef, sans sacrifice, sans monument, sans éphod et sans théraphim<!--Cette prophétie s'est accomplie d'une manière extraordinaire à travers l'histoire du peuple d'Israël depuis la première venue de Yéhoshoua ha Mashiah (Jésus-Christ). Les Israélites étaient dispersés, sans unité politique faute de roi, empêchés d'offrir des sacrifices depuis la destruction du temple par Titus (39-81), fils de l'empereur romain Vespasien (9-79), en l'an 70.-->.
3:5	Après cela, les fils d'Israël reviendront<!--La repentance et la conversion nationale d'Israël auront lieu lors du retour du Mashiah (Christ) (Es. 59:20-21 ; Ro. 11:26-27). Elohîm n'a pas abandonné son peuple, il viendra lui-même le délivrer.--> et chercheront YHWH, leur Elohîm, et David, leur roi. Ils trembleront devant YHWH et devant sa bonté, dans les derniers jours<!--Les derniers jours : voir commentaire en Ge. 49:1.-->.

## Chapitre 4

4:1	Écoutez la parole de YHWH, fils d'Israël ! Car YHWH a un procès avec les habitants de la terre, parce qu'il n'y a ni vérité, ni miséricorde, ni connaissance d'Elohîm sur la terre.
4:2	Il n'y a que parjures et mensonges, meurtres, vols et adultères. On use de violence, et le sang se mêle au sang.
4:3	C'est pourquoi la terre sera dans le deuil, et tous ceux qui l'habitent seront languissants, et avec eux toutes les bêtes des champs et toutes les créatures volantes des cieux ; même les poissons de la mer périront.
4:4	Cependant, que nul homme ne conteste et que nul homme ne juge ! Car ton peuple est comme ceux qui contestent avec les prêtres.
4:5	Tu tomberas en plein jour, et le prophète aussi tombera avec toi de nuit, et j'exterminerai ta mère.
4:6	Mon peuple est détruit<!--Le verbe « détruire » vient de l'hébreu « damah » qui signifie aussi « égorger ». Satan est celui qui vient égorger, dérober et détruire, notamment avec ses faux prophètes (Jn. 10:10). Chaque disciple de Yéhoshoua ha Mashiah (Jésus-Christ) doit avoir une vie de prière et de méditation quotidienne afin de résister aux attaques de l'ennemi.-->, faute de connaissance. Puisque tu as rejeté la connaissance, je te rejetterai afin que tu ne remplisses plus les fonctions de la prêtrise devant moi. De même que tu as oublié la torah de ton Elohîm, j'oublierai aussi tes fils.
4:7	À mesure qu'ils se sont multipliés, ils ont péché contre moi : je changerai leur gloire en ignominie.
4:8	Ils se nourrissent des péchés de mon peuple, leur âme soutient leur iniquité.
4:9	Il en sera du peuple comme du prêtre. Je le châtierai selon ses voies, et je lui rendrai selon ses œuvres.
4:10	Ils mangeront, mais ils ne seront pas rassasiés, ils se prostitueront et ne se multiplieront pas, parce qu'ils ont abandonné YHWH et ne lui ont prêté aucune attention.
4:11	La prostitution, le vin et le vin nouveau prennent possession du cœur.
4:12	Mon peuple consulte son bois, et c'est son bâton qui l'informe ! En effet, l'esprit de prostitution égare, ils commettent l'adultère loin de leur Elohîm.
4:13	Ils sacrifient sur le sommet des montagnes, ils brûlent de l'encens sur les collines, sous les chênes, sous les peupliers, et les térébinthes, parce que leur ombrage est agréable. C'est pourquoi vos filles se prostituent, et vos belles-filles commettent l'adultère.
4:14	Je ne punirai pas vos filles parce qu'elles se prostituent, ni vos belles-filles parce qu'elles commettent l'adultère, car eux-mêmes se retirent avec des prostituées, et sacrifient avec des femmes débauchées. Ainsi le peuple qui est sans intelligence sera jeté dehors.
4:15	Si tu commets l'adultère, Israël, que Yéhouda ne se rende pas coupable ! N'entrez pas dans Guilgal, et ne montez pas à Beth-Aven, et ne jurez pas : YHWH est vivant !
4:16	Parce qu'Israël se révolte comme une génisse têtue, maintenant YHWH les fera paître comme des agneaux dans de vastes plaines.
4:17	Éphraïm s'est associé aux idoles, abandonne-le !
4:18	Dès qu'ils se détournent de leur boisson, ils se prostituent, ils se prostituent. Leurs chefs aiment ce qui fait leur honte.
4:19	Le vent l'a enfermé dans ses ailes, et ils auront honte de leurs sacrifices.

## Chapitre 5

5:1	Écoutez ceci, prêtres ! Maison d'Israël, sois attentive ! Maison du roi, prêtez l'oreille ! Car c'est à vous que s'adresse le jugement, parce que vous étiez devenus un piège à Mitspah, et un filet tendu sur le Thabor.
5:2	Ils tuent et se révoltent profondément, mais je serai pour eux tous un châtiment !
5:3	Je sais qui est Éphraïm, et Israël ne m'est pas caché. En effet, maintenant, Éphraïm, tu t'es prostitué et Israël s'est rendu impur.
5:4	Leurs mauvaises habitudes ne leur permettront pas de revenir à leur Elohîm, parce que l'esprit de fornication est au milieu d'eux, et ils ne connaissent pas YHWH.
5:5	L'orgueil d'Israël témoigne en face de lui : Israël et Éphraïm trébucheront dans leur iniquité ; Yéhouda aussi trébuchera avec eux.
5:6	Ils iront avec leurs brebis et leurs bœufs chercher YHWH, mais ils ne le trouveront pas : il s'est retiré du milieu d'eux.
5:7	Ils se sont montrés infidèles envers YHWH, car ils ont engendré des fils étrangers ; maintenant un mois suffira pour les dévorer avec leurs biens.
5:8	Sonnez du shofar à Guibea. Sonnez de la trompette à Ramah ! Poussez des cris de guerre à Beth-Aven ! Derrière toi, Benyamin !
5:9	Éphraïm sera en désolation au jour de la correction. Aux tribus d'Israël, je le fais connaître avec fidélité.
5:10	Les chefs de Yéhouda sont comme ceux qui déplacent les bornes ; je répandrai sur eux ma fureur comme un torrent.
5:11	Éphraïm est opprimé, écrasé par le jugement, parce qu'il s’était résolu à marcher derrière les préceptes<!--Utilisé comme moquerie dans les paroles de Yesha`yah (Esaïe), pour ce qui n'est pas un vrai commandement d'Elohîm.-->.
5:12	Je serai comme une mite pour Éphraïm, comme de la pourriture pour la maison de Yéhouda.
5:13	Éphraïm voit sa maladie, et Yéhouda ses plaies. Éphraïm s'en est allé vers le roi d'Assyrie, et s'est adressé au roi Yareb. Mais il ne pourra ni vous guérir, ni panser vos plaies.
5:14	Car je serai comme un lion pour Éphraïm, comme un lionceau pour la maison de Yéhouda. C'est moi, c'est moi qui déchirerai, puis je m'en irai, j'emporterai, et il n’y aura personne qui délivre.
5:15	Je m'en irai, je reviendrai en mon lieu, jusqu'à ce qu'ils se reconnaissent coupables, et qu'ils cherchent mes faces. Ils me chercheront de bonne heure dans leur détresse.

## Chapitre 6

6:1	Venez, nous retournerons vers YHWH ! Oui, il a déchiré, mais il nous guérira, il a frappé, mais il nous bandera.
6:2	En deux jours, il nous fera vivre, et le troisième jour, il nous rétablira et nous vivrons en face de lui.
6:3	Nous connaîtrons, et nous suivrons YHWH afin de le connaître ! Sa venue<!--Il est question ici du retour du Seigneur Yéhoshoua ha Mashiah (Jésus-Christ). Voir commentaire en Za. 14:1.--> est certaine comme l'aurore. Il viendra pour nous comme la pluie, comme la pluie du printemps<!--La dernière pluie ou pluies de mars et avril qui mûrissent les récoltes de Kena'ân (Canaan). Voir commentaire en Joë. 2:23.--> qui arrose la terre.
6:4	Que te ferai-je, Éphraïm ? Que te ferai-je, Yéhouda ? Votre piété est comme la nuée du matin, comme la rosée qui se dissipe dès le matin.
6:5	C'est pourquoi je les ai taillés en pièces par mes prophètes, je les ai tués par les paroles de ma bouche<!--Hé. 4:12 ; Ap. 1:16, 19:15.-->, et mon jugement sort comme la lumière.
6:6	Car je prends plaisir à la miséricorde et non aux sacrifices<!--Mt. 12:7.-->, et à la connaissance d'Elohîm plus qu'aux holocaustes.
6:7	Comme Adam, ils ont transgressé l'alliance. C'est là qu'ils m'ont trahi.
6:8	Galaad est une ville d'ouvriers d'iniquité, couverte de traces de sang.
6:9	Comme des bandes de maraudeurs qui guettent un homme, telle est la troupe des prêtres. Ils assassinent sur le chemin de Shekem, car ils exécutent leurs méchants desseins.
6:10	J'ai vu des choses infâmes dans la maison d'Israël : Là Éphraïm se prostitue, Israël en est souillé.
6:11	À toi aussi Yéhouda, une moisson est préparée, quand je ramènerai les captifs de mon peuple.

## Chapitre 7

7:1	Comme je guérissais Israël, l'iniquité d'Éphraïm et la méchanceté de Samarie se sont révélées, car ils ont pratiqué la fausseté. Le voleur entre, tandis que la bande dépouille au-dehors.
7:2	Et ils ne se disent pas dans leur cœur que je me souviens de toute leur méchanceté. Maintenant leurs œuvres les entourent, elles sont en face de moi.
7:3	Ils réjouissent le roi par leur méchanceté, et les chefs par leurs mensonges.
7:4	Eux tous commettent l'adultère, pareils à un four allumé par le boulanger, qui cesse d'éveiller depuis qu'il a pétri la pâte, jusqu'à ce qu'elle soit levée.
7:5	Au jour de notre roi, les chefs l'ont rendu malade par la chaleur du vin, il a tendu la main aux moqueurs.
7:6	Oui, dans leur embuscade, ils présentent leur cœur semblable à un four qui, même si le boulanger dort toute la nuit, est brûlant au matin comme un feu de flammes.
7:7	Ils sont tous échauffés comme un four, ils dévorent leurs chefs et tous leurs rois tombent. Aucun d'eux ne fait appel à moi.
7:8	Éphraïm même se mêle avec les peuples, Éphraïm est comme un gâteau qui n'a pas été retourné.
7:9	Les étrangers ont dévoré sa force, et il ne le sait pas. Les cheveux gris sont aussi éparpillés sur lui, et il ne le sait pas.
7:10	L'orgueil d'Israël témoigne contre sa face : ils ne reviennent pas à YHWH, leur Elohîm, et ils ne le cherchent pas, malgré tout cela.
7:11	Éphraïm est devenu comme une colombe naïve, sans cœur : ils appellent l'Égypte, ils vont vers l'Assyrie.
7:12	Quand ils s'en iront, j'étendrai mon filet sur eux, et je les abattrai comme les créatures volantes des cieux, je les châtierai comme ils l'ont entendu dans leur assemblée.
7:13	Malheur à eux, parce qu'ils me fuient ! La destruction sera sur eux, parce qu'ils ont agi méchamment contre moi ! Moi, je les rachèterai, mais ils disent des mensonges contre moi.
7:14	Ils ne crient pas vers moi dans leur cœur, quand ils gémissent sur leurs couches. Ils se liguent pour du blé et du vin nouveau, et ils se détournent de moi.
7:15	Je les ai châtiés, et j'ai fortifié leurs bras, mais ils projettent le mal contre moi.
7:16	Ce n'est pas au Très-Haut qu'ils retournent. Ils sont devenus comme un arc qui trompe : leurs chefs tomberont par l'épée, à cause de la colère de leur langue. Voilà leur moquerie en terre d'Égypte !

## Chapitre 8

8:1	Mets le shofar à ta bouche ! Il vient comme un aigle contre la maison de YHWH, parce qu'ils ont transgressé mon alliance, et qu'ils ont agi méchamment contre ma torah.
8:2	Ils crieront à moi : Mon Elohîm, nous te connaissons, nous, Israël !
8:3	Israël a rejeté ce qui est bon : l'ennemi le poursuivra.
8:4	Ils ont fait des rois, mais non de ma part, ils ont établi des princes que je ne connaissais pas. Ils se sont fait des idoles avec leur argent et leur or. C'est pourquoi ils seront retranchés.
8:5	Samarie, ton veau t'a rejetée ! Ma colère s'est embrasée contre eux. Jusqu'à quand seront-ils incapables d'innocence ?
8:6	Car il vient d'Israël, c'est un artisan qui l'a fait, et il n'est pas Elohîm ! C'est pourquoi le veau de Samarie sera mis en pièces.
8:7	Parce qu'ils sèment du vent, ils moissonneront le vent d'orage<!--Ga. 6:7.-->. Ils n'auront pas un épi de blé. Le grain qui poussera ne donnera pas de farine, et s'il en faisait, les étrangers la dévoreraient.
8:8	Israël est dévoré ! Il est maintenant parmi les nations comme un vase dont on ne se soucie pas.
8:9	Oui, ils sont montés vers l'Assyrie, un âne sauvage qui s’isole. Éphraïm a donné des faveurs amoureuses à ses amours.
8:10	Mais parce qu'ils ont donné des faveurs amoureuses aux nations, je les rassemblerai maintenant, et ils commenceront à être amoindris à cause du fardeau du roi des princes.
8:11	Parce qu'Éphraïm a multiplié les autels afin de pécher, ils auront des autels pour pécher.
8:12	J’ai écrit pour lui des myriades torah, on les considère une chose étrange.
8:13	Quant aux sacrifices de mes offrandes, ils sacrifient de la chair et la mangent : YHWH ne les accepte pas. Maintenant il se souviendra de leur iniquité, et punira leurs péchés : ils retourneront en Égypte.
8:14	Israël a oublié celui qui l'a fait, et il a bâti des palais, et Yéhouda a multiplié les villes fortifiées. C'est pourquoi j'enverrai le feu dans ses villes et il dévorera leurs palais.

## Chapitre 9

9:1	Israël, ne te réjouis pas, ne sois pas dans l'allégresse, comme les autres peuples, car tu t'es prostitué loin de ton Elohîm, tu as aimé le salaire de prostituée dans toutes les aires à blé !
9:2	L'aire et la cuve ne les nourriront pas, et le vin nouveau les trompera.
9:3	Ils ne resteront pas sur la terre de YHWH : Éphraïm retournera en Égypte, et ils mangeront en Assyrie ce qui est impur.
9:4	Ils ne feront pas d'aspersions de vin à YHWH : elles ne lui seraient pas agréables. Leurs sacrifices seront pour eux comme le pain de deuil. Tous ceux qui en mangeront se rendront impurs, car leur pain ne sera que pour leur âme, il n'entrera pas dans la maison de YHWH.
9:5	Que ferez-vous aux jours des fêtes solennelles, aux jours des fêtes de YHWH ?
9:6	Car voici, ils partent à cause de la dévastation. L'Égypte les recueillera, Moph les enterrera. Les ronces prendront possession de leur argent précieux, et les épines seront dans leurs tentes.
9:7	Les jours du châtiment sont venus, les jours de la rétribution sont venus, et Israël le saura ! Le prophète est fou, l'homme de l'esprit est insensé, à cause de la grandeur de ton iniquité, et de la grande animosité.
9:8	La sentinelle d'Éphraïm est avec mon Elohîm, le prophète est un filet d'oiseleur sur toutes ses voies, en animosité contre la maison de son Elohîm.
9:9	Ils se sont profondément corrompus, comme aux jours de Guibea. YHWH se souviendra de leur iniquité, il punira leurs péchés.
9:10	J'ai trouvé Israël comme des raisins dans le désert, j'ai vu vos pères comme les premiers fruits d'un figuier, mais ils sont allés vers Baal-Peor<!--Baal-Peor, « seigneur de la brèche », était une divinité adorée à Peor avec des rites licencieux (No. 23:28, 25:1-3 ; Ps. 106:28-29).-->, ils se sont consacrés à l'infâme idole, et ils sont devenus abominables comme ce qu'ils ont aimé.
9:11	La gloire d'Éphraïm s'envolera aussi vite qu'une créature volante : plus de naissance, plus de grossesse, plus de conception.
9:12	Oui, s’ils font grandir leurs fils, je les priverai d’humains. Oui, malheur aussi à eux quand je me détournerai d'eux !
9:13	Éphraïm, quand je l’ai vu, était comme Tyr, plantée dans un pâturage. Éphraïm fera sortir ses fils vers celui qui les tuera.
9:14	YHWH, donne-leur ! Que leur donnerais-tu ? Donne-leur un sein qui avorte et des mamelles desséchées.
9:15	Toute leur méchanceté s'est manifestée à Guilgal, c'est là que je les ai haïs. Je les chasserai de ma maison à cause de la méchanceté de leurs actions. Je ne les aimerai plus, tous leurs chefs sont des rebelles.
9:16	Éphraïm est frappé, sa racine est devenue sèche. Ils ne porteront plus de fruit et, s'ils enfantent, je tuerai les délices de leur ventre.
9:17	Mon Elohîm les rejettera, parce qu'ils ne l'ont pas écouté, et ils deviendront errants parmi les nations.

## Chapitre 10

10:1	Israël est une vigne dévastée, il ne fait de fruit que pour lui-même. Plus ses fruits étaient abondants, plus il a multiplié les autels. Plus sa terre était prospère, plus il a embelli ses monuments.
10:2	Leur cœur est partagé : ils vont être déclarés coupables. Il renversera leurs autels, il détruira leurs monuments.
10:3	Oui, maintenant ils diront : Nous n'avons pas de roi car nous n'avons pas craint YHWH, et le roi, que pourrait-il faire pour nous ?
10:4	Ils ont prononcé des paroles, jurant faussement quand ils ont traité alliance. C'est pourquoi le châtiment germera dans les sillons des champs, comme une plante vénéneuse.
10:5	À cause des génisses de Beth-Aven, les habitants de Samarie auront peur. Oui, le peuple mènera deuil sur elle, ses prêtres idolâtres exulteront à cause d’elle, à cause de sa gloire. Oui, elle s'en va en exil, loin d'eux.
10:6	Même elle sera transportée en Assyrie, pour en faire un présent au roi Yareb. Éphraïm sera dans la confusion, et Israël aura honte de ses desseins.
10:7	Samarie est ravagée, son roi est comme l'écume sur les faces des eaux.
10:8	Les hauts lieux de Beth-Aven, le péché d'Israël, seront détruits, les épines et les chardons monteront sur leurs autels. Ils diront aux montagnes : Couvrez-nous ! Et aux collines : Tombez sur nous !
10:9	Israël, tu as péché dès les jours de Guibea ! Là ils restèrent debout, la guerre contre les fils de l'iniquité ne les atteignit pas à Guibea.
10:10	Je désire les châtier ! Des peuples se rassembleront contre eux et lieront à leurs deux sources<!--Le mot hébreu signifie aussi « yeux ».-->.
10:11	Éphraïm est une génisse exercée, qui aime à fouler le blé, mais je m'approcherai de son superbe cou. J'attellerai Éphraïm, Yéhouda labourera, Yaacov brisera ses mottes.
10:12	Semez pour vous selon la justice, moissonnez à la bouche de la miséricorde, labourez pour vous un sol labourable ! Il est temps de chercher YHWH, jusqu'à ce qu'il vienne, et répande sur vous sa justice.
10:13	Vous avez cultivé la méchanceté, et vous avez moissonné l'injustice, vous avez mangé le fruit du mensonge. En effet, vous avez eu confiance dans vos voies, dans la multitude de vos vaillants hommes.
10:14	Un vacarme s'élèvera parmi ton peuple et on détruira toutes tes forteresses, comme la destruction par Shalman de Beth-Arbel, au jour de la bataille, où la mère fut écrasée sur les fils.
10:15	Voilà ce que vous fera Béth-El, face à la méchanceté de vos méchancetés. Le roi d'Israël sera détruit, il sera détruit dès l'aurore.

## Chapitre 11

11:1	Quand Israël était jeune enfant, je l'ai aimé, et j'ai appelé mon fils hors d'Égypte<!--La sortie des Hébreux de l'Égypte sous Moshé (Moïse) était une préfiguration de celle de Yéhoshoua ha Mashiah (Jésus-Christ) lorsqu'il fuyait le massacre décrété par Hérode (Mt. 2:15).-->.
11:2	Plus je les ai appelés, plus ils se sont éloignés de ma présence. Ils ont sacrifié aux Baalim, et offert de l'encens aux images gravées.
11:3	Et j'ai appris à Éphraïm à marcher en le prenant par les bras et ils n'ont pas vu que je les guérissais.
11:4	Je les ai tirés avec des cordes humaines, avec des chaînes d'amour. J'étais devenu pour eux comme celui qui soulève le joug de dessus leur mâchoire, je me suis incliné vers eux pour les nourrir.
11:5	Ils ne retourneront pas en terre d'Égypte, mais le roi d'Assyrie sera leur roi, parce qu'ils n'ont pas voulu revenir.
11:6	L'épée fondra sur leurs villes, les réduira à néant, consumera leurs forces, et les dévorera, à cause des desseins qu'ils ont eus.
11:7	Et mon peuple s'est accroché à son apostasie. On les appelle vers le Très-Haut, mais aucun d'eux ne l'exalte.
11:8	Comment te donnerais-je, Éphraïm ? Te livrerai-je, Israël ? Comment te donnerais-je en Admah ? Te placerai-je comme Tseboïm ? Mon cœur s'agite au-dedans de moi, mes compassions sont brûlantes.
11:9	Je n'agirai pas selon la chaleur de mes narines, je ne reviendrai pas détruire Éphraïm, car je suis El et non pas un homme, le Saint<!--Voir commentaire en Ac. 3:14.--> au milieu de toi, je n'entrerai pas dans la ville.
11:10	Ils marcheront après YHWH, qui rugira comme un lion<!--YHWH rugit comme un lion : Yéhoshoua ha Mashiah (Jésus-Christ) est le lion de la tribu de Yéhouda, car selon la chair, il est issu de la postérité de Yéhouda (Lu. 3:23-38 ; Ap. 5:5). Le lion est le roi des animaux, or Yaacov fut le premier a avoir annoncé la venue du Shiyloh, c'est-à-dire celui à qui appartient le sceptre (Ge. 49:8-12).-->. Quand il rugira, lui, ses fils accourront tremblants, de la mer.
11:11	Ils trembleront comme un oiseau depuis l'Égypte, - et comme une colombe, depuis la terre d'Assyrie. Je les ferai habiter dans leurs maisons, - déclaration de YHWH.

## Chapitre 12

12:1	Éphraïm m'entoure de mensonges, et la maison d'Israël de tromperies. Mais Yéhouda va encore çà et là avec El, avec le Saint, le Fidèle.
12:2	Éphraïm se nourrit de vent et court après le vent d'orient. Il multiplie tout le jour le mensonge et la violence. Il traite alliance avec l'Assyrie et l'on porte des huiles de senteur en Égypte.
12:3	YHWH a un procès avec Yéhouda, et il punira Yaacov pour sa conduite, il lui rendra selon ses œuvres.
12:4	Dans le ventre, il a supplanté son frère<!--Ge. 25:26.--> et, par sa force, il a lutté avec Elohîm<!--Ge. 32:25-29.-->.
12:5	Il a lutté avec l'ange et il a vaincu, il a pleuré et lui a demandé grâce. Il l'a trouvé à Béth-El, et c'est là qu'il nous a parlé.
12:6	YHWH, l'Elohîm Tsevaot, YHWH, son souvenir<!--ou sa mémoire. Ex. 3:15.-->.
12:7	Et toi, reviens à ton Elohîm, garde la miséricorde et la justice, et aie continuellement espérance en ton Elohîm.
12:8	Kena'ân<!--Éphraïm est appelé « marchand », littéralement « Kena'ân ». Notez que l'ange de Laodicée s'exprime comme Éphraïm : « Je suis riche, abondant en ressources matérielles (...) » (Ap. 3:14-19).--> a dans sa main des balances trompeuses, il aime frauder.
12:9	Éphraïm dit : En effet, je suis devenu riche, je me suis acquis des richesses, mais c'est entièrement le produit de mon travail. On ne trouvera en moi aucune iniquité, rien qui soit un péché.
12:10	Et moi, je suis YHWH, ton Elohîm, dès la terre d'Égypte, je te ferai encore habiter dans des tentes, comme aux jours des temps fixés.
12:11	Je parlerai par les prophètes, et je multiplierai les visions, et par la main des prophètes je proposerai des comparaisons.
12:12	Si Galaad n’est qu’iniquité, certainement ils ne seront que vanité. Ils sacrifient des bœufs à Guilgal ! Même leurs autels seront comme des monceaux de pierres sur les sillons des champs.
12:13	Yaacov s'enfuit en terre de Syrie, Israël servit pour une femme, et pour une femme il fut gardien.
12:14	YHWH fit monter Israël hors d'Égypte par un prophète, et par un prophète, Israël fut gardé.
12:15	Éphraïm l'a provoqué amèrement à la colère, c'est pourquoi son Adonaï rejettera sur lui le sang qu'il a répandu, et lui rendra ses insultes.

## Chapitre 13

13:1	Dès qu'Éphraïm parlait, on tremblait. Il s'élevait en Israël. Mais il se rendit coupable par Baal, et il est mort.
13:2	Et maintenant ils continuent de pécher : ils se sont fait avec leur argent des images en métal fondu, des idoles selon leur intelligence ; toutes sont l'œuvre d'artisans. Ils disent à leur sujet : Que les hommes qui sacrifient embrassent<!--C'est une expression d'hommage.--> les veaux !
13:3	C'est pourquoi ils deviendront comme la nuée du matin, comme la rosée qui s’en va de bonne heure, comme la balle chassée par la tempête hors de l’aire, comme la fumée sortant de la cheminée.
13:4	Moi, je suis YHWH, ton Elohîm, depuis la terre d'Égypte, d'Elohîm, excepté moi, tu n’en connais pas, et de sauveur, il n’en est pas en dehors de moi<!--YHWH dit qu'il n'y a pas d'autre Sauveur que lui (Es. 43:11). Et les Écrits de la nouvelle alliance nous présentent clairement notre Sauveur : Yéhoshoua ha Mashiah (Jésus-Christ) (Mt. 1:21 ; Ac. 13:23 ; 2 Ti. 1:10 ; Tit. 1:4).-->.
13:5	Je t'ai connu dans le désert, dans une terre de la sécheresse.
13:6	Dans leurs pâturages, ils se sont rassasiés, ils se sont rassasiés, et leur cœur s’est élevé. C’est ainsi qu’ils m’ont oublié.
13:7	Je deviendrai pour eux comme un lion : je les épierai sur la route comme un léopard.
13:8	Je les rencontrerai comme une ourse à qui on a enlevé ses petits et je déchirerai l'enveloppe de leur cœur. Là, je les dévorerai comme une lionne, les bêtes des champs les mettront en pièces.
13:9	Tu t'es détruit toi-même, Israël ! Mais en moi réside ton secours.
13:10	Où est maintenant ton roi ? Qu'il te sauve dans toutes tes villes ! Où sont tes juges, au sujet desquels tu as dit : Donne-moi un roi et des princes ?
13:11	Je t'ai donné un roi<!--Ce passage concerne Shaoul (Saül), premier roi d'Israël (1 S. 8-10).--> dans ma colère, et je te le prendrai dans ma fureur.
13:12	L'iniquité d'Éphraïm est mise à l'étroit, son péché est caché.
13:13	Les douleurs de celle qui enfante viendront sur lui, lui, le fils non sage, car il ne se tient pas à temps à la brèche des fils.
13:14	Je les rachèterai de la main du shéol, je les délivrerai de la mort<!--L'auteur de la lettre aux Hébreux applique ce passage à la victoire que le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ) a remportée face à la mort lors de sa résurrection (Hé. 2:14-18). Depuis la chute d'Adam, les êtres humains ont toujours eu peur de la mort. Cette peur est d'autant plus forte de nos jours, car la plupart des gens sont angoissés par son aspect imprévisible, inévitable et par son non-sens. Et bien que beaucoup ne croient pas à l'existence de la vie après la mort (au paradis ou à l'enfer), la mort associée à l'annihilation, au non-être, apparaît d'autant plus monstrueuse et insupportable. Or notre Seigneur Yéhoshoua ha Mashiah a vaincu la mort et il promet la vie éternelle à ceux qui croient en lui (Jn. 3:16, 5:24-29 ; Ap. 1:18). En plaçant notre foi en lui, nous avons non seulement la victoire sur la mort, mais aussi sur l'angoisse qu'elle produit dans le cœur de tout humain.-->. Mort, où est ta peste ? Shéol, où est ta destruction<!--1 Co. 15:55-57.--> ? La repentance se cache à mes yeux !
13:15	S'il fructifie entre ses frères, le vent d’orient viendra, le vent de YHWH qui monte du désert. Il desséchera sa source et tarira sa fontaine. C'est lui qui pillera le trésor, tous les objets précieux.

## Chapitre 14

14:1	Samarie sera châtiée, car elle s'est rebellée contre son Elohîm. Ils tomberont par l'épée, leurs petits enfants seront écrasés et l'on fendra le ventre de leurs femmes enceintes.
14:2	Israël, reviens à YHWH ton Elohîm ! Car tu es tombé par ton iniquité.
14:3	Prenez avec vous des paroles et revenez à YHWH ! Dites-lui : Pardonne toute iniquité et prends ce qui est bon ! Nous t'offrirons pour sacrifices la louange de nos lèvres.
14:4	L'Assyrie ne nous sauvera pas. Nous ne monterons plus sur des chevaux et nous ne dirons plus à l'ouvrage de nos mains : Notre elohîm ! Car c'est auprès de toi que l'orphelin trouve de la compassion.
14:5	Je guérirai leur apostasie, je les aimerai de bon cœur, parce que ma colère s'est détournée d'eux.
14:6	Je deviendrai comme la rosée pour Israël, il fleurira comme le lis, et il poussera ses racines comme le Liban.
14:7	Ses branches s'étendront, et sa majesté deviendra comme celle de l'olivier, avec un parfum comme celui du Liban.
14:8	Ils reviendront s'asseoir sous son ombre. Ils feront revivre le blé, ils feront fleurir la vigne. Son souvenir sera comme le vin du Liban.
14:9	Éphraïm, qu'ai-je à faire encore avec les idoles ? C'est moi qui lui répondrai, qui le regarderai. Je serai pour lui comme un cyprès verdoyant. C'est de moi que tu recevras ton fruit.
14:10	Qui est sage pour comprendre ces choses, intelligent pour les connaître ? Car les voies de YHWH sont droites, et les justes y marcheront, mais les rebelles y tomberont.
