# Mishlei (Proverbes) (Pr.)

Signification : Proverbes, paraboles

Auteurs : Shelomoh (Salomon), Agour et Lemouel

Thème : La sagesse

Date de rédaction : 10ème siècle av. J.-C.

Le mot « proverbe » désigne un genre littéraire appliqué à une sentence, une énigme, une comparaison, un oracle, une parabole ou une parole de sagesse. Le livre des proverbes est un recueil de sentences dont la majeure partie est attribuée à Shelomoh. Véritable collection de maximes morales et spirituelles, la sagesse, la crainte d'Elohîm et la tempérance en sont les thèmes principaux.

Ce livre met en évidence l'opposition entre la voie du méchant et celle du juste, entre la femme étrangère et la femme talentueuse, entre l'orgueil et l'humilité, entre la sagesse et la folie, entre le chemin de la vie et celui de la mort. Comme il était coutume au Moyen-Orient, ces écrits s'adressaient particulièrement aux jeunes gens en vue de leur instruction.

## Chapitre 1

### But du livre : connaître la sagesse

1:1	Les proverbes de Shelomoh, fils de David et roi d'Israël.
1:2	Pour connaître la sagesse et la correction, pour discerner les paroles du discernement,
1:3	pour recevoir une correction de bon sens, de justice, de jugement et d'équité.
1:4	Pour donner de la sagacité aux stupides, de la connaissance et de la réflexion aux jeunes hommes.
1:5	Le sage écoutera, et il augmentera son instruction. Celui qui discerne acquerra de sages conseils,
1:6	afin de discerner les proverbes et les énigmes, les discours des sages et leurs énigmes.

### Le fondement de la sagesse : la crainte d'Elohîm

1:7	La crainte de YHWH<!--Pr. 8:13.--> est le commencement de la connaissance. Les fous méprisent la sagesse et la correction.
1:8	Mon fils, écoute la correction de ton père et n'abandonne pas la torah<!--« Instruction, enseignement, direction » etc.--> de ta mère.
1:9	Car elles sont, sur ta tête, une couronne de grâce et des colliers autour de ton cou.
1:10	Mon fils, si les pécheurs te séduisent, n’y consens pas.
1:11	S'ils disent : Viens avec nous ! Nous nous mettrons en embuscade pour le sang, nous nous cacherons pour guetter l’innocent, sans cause !
1:12	Nous les engloutirons vivants comme le shéol, et tout entiers comme ceux qui descendent dans la fosse.
1:13	Nous trouverons toutes sortes de richesses précieuses, nous remplirons nos maisons de butin.
1:14	Tu tireras au sort ton lot au milieu de nous, une seule bourse sera pour nous tous !
1:15	Mon fils, ne te mets pas en chemin avec eux, détourne ton pied de leur sentier !
1:16	Car leurs pieds courent vers le mal, et ils se hâtent de répandre le sang<!--Es. 59:7.-->.
1:17	Car en vain se tend le filet aux yeux de tout Baal ailé<!--Ec. 10:20.-->.
1:18	Mais eux, c’est contre leur propre sang qu’ils se mettrons en embuscade, ils se cacherons pour guetter leurs propres âmes.
1:19	Telle est la voie de celui qui convoite le gain injuste, qui enlève l’âme de ses maîtres.

### La sagesse crie

1:20	La sagesse crie bien haut au-dehors<!--Dehors, extérieur, rue, en dehors de.-->, elle donne de sa voix sur les places<!--Endroit large ou ouvert.-->.
1:21	En tête des lieux tumultueux, elle crie ; aux entrées des portes, dans la ville, elle prononce ces paroles :
1:22	Stupides, jusqu'à quand aimerez-vous la stupidité, et les moqueurs prendront-ils plaisir à la moquerie, et les insensés haïront-ils la connaissance ?
1:23	Revenez à ma réprimande ! Voici, je verserai sur vous mon Esprit, je vous ferai connaître mes paroles.
1:24	Oui, j'ai appelé et vous avez refusé, j'ai étendu ma main et personne n’a été attentif !
1:25	Vous avez ignoré tous mes conseils, vous n'avez pas accepté ma réprimande.
1:26	Moi aussi je rirai de votre malheur, je me moquerai quand la terreur viendra sur vous.
1:27	Quand la terreur viendra sur vous comme une tempête dévastatrice, quand votre désastre arrivera comme un vent d'orage, quand la détresse et l'angoisse viendront sur vous.
1:28	Alors on m’appellera et je ne répondrai pas, on me cherchera de bonne heure et on ne me trouvera pas<!--De. 31:18 ; Job 35:12.-->.
1:29	Parce qu'ils ont haï la connaissance et qu'ils n'ont pas choisi la crainte de YHWH,
1:30	qu'ils n'ont pas voulu de mes conseils et qu’ils ont rejeté toutes mes réprimandes,
1:31	ils se nourriront du fruit de leur voie, ils se rassasieront de leurs propres conseils.
1:32	Car l'apostasie des stupides les tuera, et la tranquillité des insensés les perdra.
1:33	Mais celui qui m'écoute habitera en sécurité et sera tranquille, sans craindre aucun mal.

## Chapitre 2

### La sagesse nous libère du mal

2:1	Mon fils, si tu reçois mes paroles et que tu gardes précieusement avec toi mes commandements,
2:2	si tu rends ton oreille attentive à la sagesse et que tu inclines ton cœur à l'intelligence,
2:3	oui, si tu appelles à toi le discernement et que tu donnes ta voix à l'intelligence,
2:4	si tu la cherches comme de l'argent et si tu la recherches soigneusement comme des trésors,
2:5	alors tu connaîtras la crainte de YHWH et tu trouveras la connaissance d'Elohîm.
2:6	Car YHWH donne la sagesse, et de sa bouche, la connaissance et l'intelligence.
2:7	Il garde précieusement le succès durable pour ceux qui sont droits, et il est le bouclier de ceux qui marchent dans l'intégrité,
2:8	gardien des sentiers de la justice, veillant sur la voie de ses fidèles.
2:9	Alors tu comprendras la justice, le jugement, l'équité, et toutes les routes du bonheur.
2:10	Oui, la sagesse viendra dans ton cœur et la connaissance fera les délices de ton âme.
2:11	La discrétion te gardera, l'intelligence veillera sur toi,
2:12	pour te délivrer de la voie mauvaise et de l'homme qui parle avec perversité,
2:13	de ceux qui abandonnent les sentiers de la droiture pour marcher dans les voies des ténèbres,
2:14	qui se réjouissent de faire du mal et qui exultent dans la perversité des méchants,
2:15	eux dont les sentiers sont tortueux et les voies pleines de détours.
2:16	Pour te délivrer de la femme étrange<!--Ou « la femme étrangère ». Elle est la prostituée ou l'esprit de Iyzebel (Jézabel) qui séduit les hommes. Voir Pr. 6:24, 7:5.-->, de l'étrangère aux paroles flatteuses,
2:17	qui abandonne l'ami de sa jeunesse et qui oublie l'alliance de son Elohîm.
2:18	Car sa maison penche vers la mort, et son chemin mène vers les fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».-->.
2:19	Tous ceux qui entrent chez elle ne reviennent pas, n’atteignent pas les sentiers de vies.
2:20	Ainsi tu marcheras dans la voie de ceux qui sont bons, et tu garderas les sentiers des justes.
2:21	Car les justes habiteront la Terre, les intègres y seront laissés.
2:22	Mais les méchants seront retranchés de la Terre, et les traîtres en seront arrachés.

## Chapitre 3

### La sagesse bénit et protège

3:1	Mon fils, n'oublie pas ma torah, et que ton cœur garde mes commandements,
3:2	car la longueur des jours, les années de vies et la paix te seront ajoutées.
3:3	Que la bonté et la vérité ne t'abandonnent pas, lie-les à ton cou, écris-les sur la tablette de ton cœur,
3:4	et tu trouveras la grâce et une bonne compréhension aux yeux d'Elohîm et des humains.
3:5	Confie-toi de tout ton cœur en YHWH et ne t'appuie pas sur ton discernement.
3:6	Considère-le dans toutes tes voies et il dirigera tes sentiers.
3:7	Ne sois pas sage à tes propres yeux, crains YHWH et détourne-toi du mal :
3:8	cela deviendra la guérison de ton nombril et un rafraîchissement pour tes os.
3:9	Honore YHWH avec tes richesses, avec les premiers de tout ton revenu<!--De. 12:6.--> !
3:10	Tes greniers seront remplis d'abondance, et tes cuves regorgeront de vin nouveau.
3:11	Mon fils, ne refuse pas la correction de YHWH et n’aie pas son châtiment en aversion.
3:12	Car YHWH châtie<!--Hé. 12:4-11 ; Ap. 3:19.--> celui qu'il aime, comme un père le fils auquel il prend plaisir.
3:13	Heureux l'être humain qui a trouvé la sagesse, et l'être humain qui possède l'intelligence !
3:14	Car son gain est meilleur que le gain de l'argent, et son revenu est meilleur que l'or.
3:15	Elle est plus précieuse que les perles, et tous tes délices ne peuvent l’égaler.
3:16	Il y a la longueur des jours à sa droite, la richesse et la gloire à sa gauche.
3:17	Ses voies sont des voies agréables, et tous ses sentiers sont paix.
3:18	Elle est un arbre de vies pour ceux qui se fortifient en elle, et tous ceux qui la saisissent sont heureux<!--Ge. 2:9 ; Ap. 22:2.-->.
3:19	YHWH a fondé la Terre par la sagesse, il a disposé les cieux par l'intelligence.
3:20	C'est par sa connaissance que les abîmes se sont ouverts et que les nuages distillent la rosée.
3:21	Mon fils, que ces enseignements ne s'écartent pas de tes yeux ! Garde le succès durable et la réflexion !
3:22	Ils deviendront vies pour ton âme et ornement pour ton cou.
3:23	Alors tu marcheras avec sécurité dans ta voie, et ton pied ne trébuchera pas.
3:24	Si tu te couches, tu seras sans crainte, et quand tu seras couché ton sommeil sera doux.
3:25	Tu ne craindras ni la terreur soudaine ni la ruine des méchants quand elle arrivera.
3:26	Car YHWH deviendra ton espérance<!--Ps. 39:8, 71:5 ; 1 Ti. 1:1.-->, et il gardera ton pied de la capture.
3:27	Ne refuse pas un bienfait à son possesseur, quand El te donne le pouvoir de le faire<!--Ga. 6:10.-->.
3:28	Ne dis pas à ton prochain : « Va-t’en et reviens, demain je te donnerai ! » quand tu as la chose avec toi.
3:29	Ne médite pas le mal contre ton prochain alors qu'il demeure auprès de toi en sécurité.
3:30	Ne conteste pas sans motif avec un humain, s'il ne t'a fait aucun mal<!--Ro. 12:18.-->.
3:31	Ne porte pas envie à l'homme violent, et ne choisis aucune de ses voies.
3:32	Car le pervers est une abomination pour YHWH, mais son intimité est pour ceux qui sont justes.
3:33	La malédiction de YHWH est dans la maison du méchant, mais il bénit la demeure des justes.
3:34	S'il se moque des moqueurs, il accorde sa grâce aux humbles.
3:35	Les sages hériteront la gloire, mais la honte élève les insensés.

## Chapitre 4

### Instructions et conseils d'un père

4:1	Écoutez, mes fils, la correction d'un père, et soyez attentifs pour connaître le discernement.
4:2	Car je vous donne une bonne instruction, ne rejetez pas ma torah.
4:3	J'étais devenu un fils pour mon père, un fils tendre et unique auprès de ma mère.
4:4	Il m'a enseigné et m'a dit : Que ton cœur retienne mes paroles ! Garde mes commandements et tu vivras.
4:5	Acquiers la sagesse, acquiers le discernement ! N'oublie pas les paroles de ma bouche et ne t'en détourne pas.
4:6	Ne l'abandonne pas et elle te gardera. Aime-la et elle te protégera.
4:7	La principale chose, c'est la sagesse : acquiers la sagesse, et avec toutes tes possessions, acquiers le discernement.
4:8	Exalte-la, et elle t'élèvera. Elle te glorifiera quand tu l'auras embrassée.
4:9	Elle donnera à ta tête une couronne de grâce, elle te livrera une couronne de beauté.
4:10	Écoute, mon fils, et reçois mes paroles, ainsi les années de ta vie te seront multipliées.
4:11	Je t'ai enseigné la voie de la sagesse, je t'ai conduit dans les sentiers de la droiture<!--Ps. 23:3.-->.
4:12	Quand tu marcheras, ton pas ne sera pas à l'étroit, et si tu cours, tu ne trébucheras pas<!--Ps. 121:3.-->.
4:13	Fortifie-toi dans la correction, ne la laisse pas tomber ! Garde-la, car elle est ta vie.
4:14	N'entre pas dans le sentier des méchants et ne marche pas dans la voie des mauvais.
4:15	Évite-la, n'y passe pas ! Détourne-t'en et passe outre !
4:16	Car ils ne dorment pas s’ils ne font pas le mal, et leur sommeil est volé s’ils ne font pas trébucher quelqu'un.
4:17	Parce qu'ils mangent le pain de méchanceté, et qu'ils boivent le vin de la violence.
4:18	Le sentier des justes est comme la lumière éclatante, continuant à briller jusqu’à ce que le jour soit établi.
4:19	La voie des méchants c'est l'obscurité : ils ne savent pas sur quoi ils trébuchent.
4:20	Mon fils, sois attentif à mes paroles, incline ton oreille à mes discours.
4:21	Qu'ils ne s’éloignent pas de tes yeux ! Garde-les au milieu de ton cœur.
4:22	Car ils sont vie pour ceux qui les trouvent et santé pour toute leur chair.
4:23	Garde ton cœur plus que tout ce que l’on garde, car de lui procèdent les sources de vies<!--Mt. 12:35, 15:18-19.-->.
4:24	Détourne-toi de la bouche tortueuse, et éloigne de toi la perversité des lèvres.
4:25	Que tes yeux regardent en face et que tes paupières se dirigent droit devant toi !
4:26	Aplanis la route de tes pieds et que toutes tes voies soient bien stables !
4:27	Ne te tourne ni à droite ni à gauche et détourne ton pied du mal !

## Chapitre 5

### Se garder de l'immoralité

5:1	Mon fils, sois attentif à ma sagesse, incline ton oreille à mon intelligence,
5:2	afin que tu gardes mes avis et que tes lèvres conservent la connaissance.
5:3	Car les lèvres de l'étrangère distillent des rayons de miel, et son palais est plus doux que l'huile.
5:4	Mais à la fin elle est amère comme de l'absinthe, tranchante comme une épée à deux tranchants<!--Deux bouches.-->.
5:5	Ses pieds descendent à la mort, ses pas atteignent le shéol,
5:6	elle n'aplanit pas le chemin de vies, ses pistes sont errantes sans que tu le saches.
5:7	Maintenant, fils, écoutez-moi, et ne vous détournez pas des paroles de ma bouche !
5:8	Éloigne ta voie de la femme étrangère et n'approche pas de l'entrée de sa maison !
5:9	De peur que tu ne donnes ta splendeur à d'autres et tes années à ce qui est cruel,
5:10	de peur que les étrangers ne se rassasient de tes biens et que le fruit de ton travail ne soit dans la maison d'un étranger,
5:11	de peur que tu ne gémisses quand tu seras près de ta fin, quand ta chair et ton corps seront consumés.
5:12	Tu diras : Comment ai-je haï la correction ? Comment mon cœur a-t-il méprisé les réprimandes ?
5:13	Comment n'ai-je pas obéi à la voix de ceux qui m'instruisaient, et n'ai-je pas incliné mon oreille à ceux qui m'enseignaient ?
5:14	J’ai été presque dans toutes sortes de maux au milieu de la congrégation et de l’assemblée.
5:15	Bois les eaux de ta citerne et de ce qui coule du milieu de ton puits !
5:16	Tes sources se disperseront-elles dans les rues ? Tes canaux d'eaux sur les places publiques ?
5:17	Qu'ils soient à toi seul, et non aux étrangers avec toi.
5:18	Que ta source soit bénie, et réjouis-toi de la femme de ta jeunesse,
5:19	biche des amours, chèvre de montagne pleine de grâce ! Que ses mamelles te rassasient en tout temps, et sois continuellement égaré par son amour !
5:20	Pourquoi, mon fils, serais-tu égaré par une étrangère et embrasserais-tu le sein d'une inconnue ?
5:21	Car les voies de l'homme sont devant les yeux de YHWH qui aplanit toutes ses pistes<!--Jé. 16:17 ; Hé. 4:13.-->.
5:22	Les iniquités du méchant le captureront, et il sera saisi par les cordes de son péché.
5:23	Il mourra faute de correction et il s'égarera par la grandeur de sa folie.

## Chapitre 6

### Recommandations diverses

6:1	Mon fils, si tu t'es porté garant auprès de ton prochain, si tu as frappé dans la main de l'étranger,
6:2	tu es enlacé par les paroles de ta bouche, tu es pris par les paroles de ta bouche.
6:3	Mon fils, fais maintenant ceci, et délivre-toi, car tu es venu dans la paume de ton ami, va, prosterne-toi et importune ton ami.
6:4	Ne donne pas de sommeil à tes yeux et ne laisse pas sommeiller tes paupières.
6:5	Délivre-toi comme une gazelle de la main et comme un oiseau de la main de l’oiseleur.
6:6	Va, paresseux, vers la fourmi, regarde ses voies et deviens sage !
6:7	Elle n'a ni chef, ni commissaire, ni gouverneur.
6:8	Elle prépare en été son pain, elle amasse pendant la moisson sa nourriture.
6:9	Paresseux, jusqu'à quand resteras-tu couché ? Quand te lèveras-tu de ton sommeil ?
6:10	Un peu de sommeil, un peu d'assoupissement, un peu croiser les mains en se couchant...
6:11	Et elle viendra, elle arrivera, la pauvreté, et le besoin, comme l'homme au bouclier.
6:12	Un être humain de Bélial<!--Voir commentaire en De. 13:14.-->, un homme méchant, marche avec une bouche tortueuse.
6:13	Il fait signe de ses yeux, il parle de ses pieds, il enseigne de ses doigts.
6:14	Il y a la perversité dans son cœur, il complote le mal en tout temps, il fait naître des querelles.
6:15	C'est pourquoi sa calamité viendra soudainement, il sera subitement brisé, il n'y aura pas de guérison.
6:16	Il y a six choses qu'Elohîm hait, et il y en a sept qui sont en abomination à son âme :
6:17	les yeux hautains<!--Ps. 101:5.-->, la langue mensongère<!--Ps. 120:2-3.-->, les mains qui répandent le sang innocent<!--Es. 1:15.-->,
6:18	le cœur qui complote des projets méchants<!--Ps. 36:5.-->, les pieds qui se hâtent de courir au mal<!--Es. 59:7.-->,
6:19	le faux témoin qui profère des mensonges<!--Ps. 27:12.--> et celui qui sème des querelles entre les frères<!--Jud. 1:16-19.-->.
6:20	Mon fils, garde le commandement de ton père et n'abandonne pas la torah de ta mère !
6:21	Lie-les constamment sur ton cœur, attache-les à ton cou !
6:22	Ils te conduiront quand tu marcheras, ils te garderont quand tu te coucheras, ils te parleront quand tu te réveilleras.
6:23	Car le commandement est une lampe et la torah une lumière<!--Ps. 119:105.-->, et les réprimandes de la correction sont la voie de vies :
6:24	pour te garder de la mauvaise femme et des flatteries de la langue étrangère.
6:25	Ne convoite pas sa beauté dans ton cœur, qu’elle ne te prenne pas par ses paupières<!--Mt. 5:28.-->.
6:26	Car pour une femme prostituée on en vient jusqu’à un morceau de pain, et la femme d'un homme fait la chasse à une âme précieuse.
6:27	Un homme prendra-t-il du feu dans son sein sans que ses vêtements brûlent ?
6:28	Si un homme marche sur des charbons ardents ses pieds ne seront-ils pas brûlés ?
6:29	Ainsi en est-il de celui qui va vers la femme de son ami : quiconque la touche ne sera pas tenu pour innocent.
6:30	On ne méprise pas un voleur qui vole pour remplir son âme affamée,
6:31	si on le trouve, il rendra sept fois autant, il donnera toutes les richesses de sa maison.
6:32	Celui qui commet un adultère avec une femme manque de cœur. Celui qui le fera, détruira son âme.
6:33	Il ne trouvera que plaies et ignominie, et son insulte ne sera pas effacée.
6:34	Car la jalousie est le courroux d'un homme fort, et il n'épargnera pas au jour de la vengeance.
6:35	Il n'aura égard à aucune rançon<!--Littéralement : Il ne portera pas la face de toute rançon.--> et n'acceptera rien, quand même tu multiplierais les pots-de-vin.

## Chapitre 7

### Mise en garde contre la femme prostituée

7:1	Mon fils, observe mes paroles, et garde avec toi mes commandements !
7:2	Garde mes commandements et tu vivras. Garde ma torah comme la prunelle de tes yeux<!--Lé. 18:5.--> !
7:3	Lie-les à tes doigts, écris-les sur la tablette de ton cœur !
7:4	Dis à la sagesse : Tu es ma sœur ! et appelle le discernement ton parent.
7:5	Afin qu'ils te préservent de la femme étrangère, de l'étrangère qui emploie des paroles doucereuses.
7:6	Comme je regardais de la fenêtre de ma maison à travers mon treillis,
7:7	je vis parmi les stupides, j’aperçus parmi les fils un jeune homme qui manquait de cœur.
7:8	Il passait dans la rue, près de son angle, et il marchait sur le chemin de sa maison,
7:9	sur le soir à la fin du jour, lorsque la nuit devenait noire et obscure.
7:10	Et voici qu’une femme vint à sa rencontre, ayant le vêtement d’une prostituée et la ruse dans le cœur.
7:11	Elle était bruyante et rebelle. Ses pieds ne restaient pas dans sa maison :
7:12	tantôt dehors, tantôt sur les places, elle était en embuscade à chaque coin de rue.
7:13	Elle le saisit et l'embrassa et, avec une face forte, lui dit :
7:14	J'ai chez moi des sacrifices d'offrande de paix, j'ai aujourd'hui accompli mes vœux.
7:15	C'est pourquoi je suis sortie à ta rencontre, pour chercher tes faces, et je t'ai trouvé.
7:16	J'ai orné mon lit de couvre-pieds, d'étoffes de fil d'Égypte.
7:17	J'ai aspergé ma couche de myrrhe, d'aloès et de cinnamome.
7:18	Viens, enivrons-nous d'amour jusqu'au matin, faisons-nous plaisir avec l'objet aimé.
7:19	Car mon mari n'est pas à la maison, il est parti pour un voyage lointain.
7:20	Il a pris dans sa main un sac d'argent. Au jour de la pleine lune, il viendra dans sa maison.
7:21	Elle le détourna par la multitude de ses instructions, elle le bannit, avec ses lèvres flatteuses.
7:22	Soudain il s'en alla après elle, comme un bœuf qui va à l'abattage, comme le fou qu'on lie pour le corriger,
7:23	jusqu'à ce que la flèche lui ait transpercé le foie. Comme l'oiseau qui se hâte vers le filet, sans savoir qu’il y va de son âme même.
7:24	Maintenant, fils, écoutez-moi et soyez attentifs aux paroles de ma bouche.
7:25	Que ton cœur ne se détourne pas vers les voies d'une telle femme, ne t'égare pas dans ses sentiers.
7:26	Car elle en a fait tomber beaucoup, blessés à mort. Ils sont nombreux<!--« Puissant », « fort ».-->, tous ceux qu'elle a tués.
7:27	Sa maison est le chemin du shéol qui descend vers les chambres<!--« Pièce », « parloir », « partie interne », « dedans ».--> de la mort.

## Chapitre 8

### La sagesse préférable aux richesses

8:1	La sagesse ne crie-t-elle pas ? L'intelligence ne fait-elle pas entendre sa voix ?
8:2	Elle s'est présentée sur le sommet des lieux élevés, sur le chemin, aux carrefours.
8:3	À côté des portes, à la bouche de la ville, à l’entrée des ouvertures, elle crie :
8:4	Hommes, c'est vers vous que je crie, ma voix aux fils de l’humain !
8:5	Vous, esprits simples, apprenez la sagacité ! Et vous, insensés, devenez intelligents de cœur.
8:6	Écoutez, car je parlerai des choses princières, l'ouverture de mes lèvres est pour la droiture.
8:7	Car ma bouche prononce la vérité, et la méchanceté est abominable à mes lèvres.
8:8	Tous les discours de ma bouche sont selon la justice, il n'y a rien en eux de faux, ni de déformé.
8:9	Ils sont tous justes pour celui qui est intelligent, et droits pour ceux qui ont trouvé la connaissance.
8:10	Prenez ma correction et non pas de l'argent, la connaissance plutôt que l'or le plus précieux.
8:11	Car la sagesse est meilleure que les perles, et tous les délices ne peuvent l’égaler<!--Ps. 19:11, 119:127 ; Job 28:18.-->.
8:12	Moi, la sagesse, je demeure dans la sagacité, et je possède la connaissance de la réflexion.
8:13	La crainte de YHWH c'est la haine du mal. Je hais l'orgueil et l'arrogance, la voie de la méchanceté et la bouche hypocrite.
8:14	À moi le conseil, le succès durable. Moi, le discernement, à moi la force !
8:15	Par moi règnent les rois, et par moi les princes décrètent ce qui est juste.
8:16	Par moi gouvernent les seigneurs, les princes, et tous les juges de la Terre.
8:17	J'aime ceux qui m'aiment, et ceux qui me cherchent me trouvent<!--Mt. 7:7 ; Lu. 11:9 ; Jn. 14:23-24.-->.
8:18	Avec moi sont la richesse et la gloire, les richesses durables et la justice.
8:19	Mon fruit est meilleur que l'or, que l'or raffiné, et mon produit que l'argent de choix.
8:20	Je marche dans le chemin de la justice, au milieu des sentiers de la droiture,
8:21	pour faire hériter les biens réels à ceux qui m’aiment, et pour remplir leurs trésors.
8:22	YHWH m'a acquise au commencement de sa voie, avant ses œuvres, dès lors.
8:23	J'ai été établie depuis l'éternité, en tête, bien avant la Terre.
8:24	J'ai été engendrée lorsqu'il n'y avait pas encore d'abîmes, ni de sources chargées d'eaux.
8:25	Avant que les montagnes soient affermies, avant que les collines existent, j'ai été engendrée.
8:26	Il n'avait encore fait ni la Terre, ni les campagnes, ni le premier grain de la poussière du monde.
8:27	Quand il affermit les cieux, moi, j’étais là, quand il traça un cercle sur les faces de l'abîme,
8:28	quand il fixa les nuages en haut et que les sources de l'abîme jaillirent avec force,
8:29	quand il donna une limite à la mer, pour que les eaux ne franchissent pas les bords, quand il posa les fondements de la Terre,
8:30	j'étais devenue architecte auprès de lui, j'étais devenue ses délices, de jour en jour, jouant en face de lui en tout temps.
8:31	Jouant dans le monde, sur sa terre, et trouvant mes délices avec les fils de l'être humain.
8:32	Maintenant, mes fils, écoutez-moi : Heureux ceux qui gardent mes voies.
8:33	Écoutez la correction pour devenir sages, ne la rejetez pas !
8:34	Heureux l'être humain qui m'écoute, qui veille tout le jour à mes portes, et qui monte la garde aux montants de mes portes !
8:35	Car celui qui me trouve trouve la vie et obtient la faveur de YHWH.
8:36	Mais celui qui pèche contre moi fait mal à son âme. Tous ceux qui me haïssent aiment la mort.

## Chapitre 9

### La sagesse, source de vie

9:1	La Sagesse a bâti sa maison, elle a taillé ses sept colonnes.
9:2	Elle a abattu ses animaux, elle a mêlé son vin, elle a aussi dressé sa table.
9:3	Elle a envoyé ses servantes, elle crie du haut des lieux les plus élevés de la ville :
9:4	Que celui qui est stupide fasse un détour par ici ! Elle dit à ceux qui manquent de cœur :
9:5	Venez, mangez de mon pain, et buvez du vin que j'ai mêlé.
9:6	Abandonnez la naïveté et vous vivrez, marchez dans la voie du discernement !
9:7	Celui qui corrige le moqueur en reçoit de l'ignominie, et celui qui reprend le méchant en reçoit une tache.
9:8	Ne reprends pas le moqueur, de peur qu'il ne te haïsse ! Reprends le sage et il t'aimera<!--Ps. 141:5.-->.
9:9	Donne au sage et il deviendra encore plus sage, donne la connaissance au juste et il augmentera son instruction.
9:10	Le commencement de la sagesse est la crainte de YHWH<!--Ps. 19:10.-->. La connaissance des saints, c'est le discernement.
9:11	Car tes jours se multiplieront par moi, et des années de vie te seront ajoutées.
9:12	Si tu es sage, tu es sage pour toi-même, si tu es moqueur, tu en porteras seul la peine.
9:13	La femme folle est bruyante, stupide, et elle ne connaît rien.
9:14	Et elle s'assied à la porte de sa maison sur un siège, dans les lieux élevés de la ville,
9:15	pour appeler ceux qui passent sur la route, qui vont droit leur chemin :
9:16	Que celui qui est stupide entre ici ! Et elle dit à celui qui manque de cœur :
9:17	Les eaux volées sont douces et le pain pris en secret est agréable !
9:18	Et il ne sait pas que là sont les fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».-->, que ceux qu’elle a invités sont dans le plus profond du shéol.

## Chapitre 10

### La justice s'oppose à la méchanceté

10:1	Proverbes de Shelomoh. Le fils sage réjouit son père, mais le fils insensé est le chagrin de sa mère.
10:2	Les trésors de méchanceté ne profitent pas, mais la justice délivre de la mort.
10:3	YHWH ne laisse pas l'âme du juste avoir faim, mais il repousse les désirs des méchants.
10:4	Celui qui agit d'une paume paresseuse s'appauvrit, mais la main des diligents enrichit.
10:5	L'enfant prudent amasse en été, celui qui dort durant la moisson est un enfant qui fait honte.
10:6	Les bénédictions seront sur la tête du juste, mais la violence couvrira la bouche des méchants.
10:7	La mémoire du juste est en bénédiction<!--Ps. 112:6.-->, mais la réputation des méchants tombe en pourriture.
10:8	Celui qui est sage de cœur reçoit les commandements, mais celui qui est fou des lèvres sera jeté dehors.
10:9	Celui qui marche dans l'intégrité marche avec assurance, mais celui qui pervertit ses voies sera découvert.
10:10	Celui qui cligne de l'œil cause de la douleur, et celui qui est fou des lèvres sera jeté dehors.
10:11	La bouche du juste est une source de vies, mais la violence couvre la bouche des méchants.
10:12	La haine excite les querelles, mais l'amour couvre toutes les transgressions<!--1 Pi. 4:8.-->.
10:13	La sagesse se trouve sur les lèvres de celui qui est intelligent, mais la verge est pour le dos de celui qui manque de cœur.
10:14	Les sages tiennent la connaissance en réserve, mais la bouche du fou est près de la destruction.
10:15	La richesse du riche est sa ville forte, mais la pauvreté des misérables est leur ruine.
10:16	L'œuvre du juste est pour la vie, mais le revenu du méchant est pour le péché.
10:17	Celui qui fait attention à la correction est dans le chemin de vies, mais celui qui néglige la réprimande s'égare.
10:18	Celui qui couvre la haine a des lèvres menteuses, et celui qui répand la diffamation est un insensé.
10:19	Dans la multitude de paroles, la transgression ne manque pas, mais celui qui retient ses lèvres est prudent.
10:20	La langue du juste est un argent choisi, mais le cœur des méchants est bien peu de chose.
10:21	Les lèvres du juste en nourrissent beaucoup, mais les fous mourront par manque de cœur.
10:22	C'est la bénédiction de YHWH qui enrichit, et il n'y ajoute aucune peine.
10:23	C'est comme un jeu pour un insensé de pratiquer la méchanceté, mais la sagesse appartient à l'homme intelligent.
10:24	Ce que craint le méchant, c’est ce qui lui arrive, et ce que désirent les justes leur est accordé.
10:25	Comme le vent d'orage passe, ainsi le méchant disparaît, mais le juste possède un fondement éternel.
10:26	Ce que le vinaigre est aux dents et la fumée aux yeux, tel est le paresseux pour ceux qui l'envoient.
10:27	La crainte de YHWH augmente les jours, mais les années des méchants sont écourtées.
10:28	L'espérance des justes c'est la joie, mais l'attente des méchants périra.
10:29	La voie de YHWH est le refuge pour l'intégrité, mais elle est la ruine pour ceux qui pratiquent la méchanceté.
10:30	Le juste ne sera jamais ébranlé, mais les méchants ne demeureront pas sur la Terre.
10:31	La bouche du juste produit la sagesse, mais la langue perverse sera retranchée.
10:32	Les lèvres du juste connaissent la faveur, la bouche des méchants, la perversité.

## Chapitre 11

### La justice s'oppose à la méchanceté (suite)

11:1	La balance trompeuse est une abomination pour YHWH, mais le poids complet est pour lui un plaisir<!--Lé. 19:35-36 ; De. 25:13-16.-->.
11:2	Quand l'orgueil vient, la honte vient aussi, mais la sagesse est avec ceux qui sont modestes.
11:3	L'intégrité des justes les conduit, mais la perversité des traîtres les détruit.
11:4	Les richesses ne servent à rien au jour de la colère, mais la justice délivre de la mort.
11:5	La justice de l'innocent rend droite sa voie, mais le méchant tombe par sa méchanceté.
11:6	La justice des justes les délivre, mais les traîtres sont pris par leur méchanceté.
11:7	Quand un être humain méchant meurt, l'espoir périt, et l'espérance douloureuse périt aussi.
11:8	Le juste est délivré de la détresse, et le méchant y entre à sa place.
11:9	Par sa bouche l'athée corrompt son prochain, mais les justes en sont délivrés par la connaissance.
11:10	La ville se réjouit quand les justes sont heureux, et quand les méchants périssent, ce sont des cris de joie.
11:11	La ville s’élève par la bénédiction des justes, mais elle est renversée par la bouche des méchants.
11:12	Celui qui méprise son prochain manque de cœur, mais l'homme prudent se tait.
11:13	Celui qui va et calomnie, révèle les secrets, mais celui dont l'esprit est fidèle couvre la chose.
11:14	Quand il n’y a pas de direction le peuple tombe, mais la délivrance est dans la multitude de conseillers.
11:15	Mauvais, c'est mauvais quand on cautionne un étranger ; celui qui hait les topes<!--Accepter un défi, taper dans la main de quelqu'un pour signifier qu'on accepte, qu'on conclut le marché.--> est en sécurité.
11:16	La femme de grâce se saisit de la gloire, et les terrifiants se saisissent de la richesse.
11:17	Un homme de bonté fait du bien à son âme, mais le cruel trouble sa chair.
11:18	Le méchant fait une œuvre trompeuse, mais celui qui sème la justice a un vrai salaire<!--Os. 10:12.-->.
11:19	Ainsi la justice mène à la vie, mais celui qui poursuit le mal aboutit à sa mort.
11:20	Ceux qui ont le cœur pervers sont en abomination pour YHWH, mais ceux qui sont intègres dans leurs voies lui sont agréables.
11:21	De la main à la main, le méchant ne restera pas impuni, mais la postérité des justes sera délivrée.
11:22	Une belle femme qui se détourne du jugement est un anneau d'or au groin d'un pourceau.
11:23	Le souhait des justes n'est que le bien, l'attente des méchants c'est l'indignation.
11:24	Tel éparpille et augmente encore, tel autre épargne ce qui est dû et se trouve dans la pauvreté.
11:25	L'âme qui bénit deviendra prospère, et celui qui arrose abondamment sera lui-même arrosé.
11:26	Sera maudit du peuple, celui qui cache le froment, mais la bénédiction est sur la tête de celui qui le vend.
11:27	Celui qui cherche ce qui est bon désire la faveur, mais le mal atteindra celui qui le recherche.
11:28	Celui qui se confie dans ses richesses tombera, mais les justes pousseront comme le feuillage<!--Ps. 1:3 ; Jé. 17:8.-->.
11:29	Celui qui trouble sa maison héritera du vent, et le fou deviendra l'esclave de celui qui est sage de cœur.
11:30	Le fruit du juste est un arbre de vies, et celui qui gagne les âmes est sage.
11:31	Si le juste est payé de retour sur la Terre, combien plus le méchant et le pécheur !

## Chapitre 12

### La justice s'oppose à la méchanceté (suite)

12:1	Celui qui aime la correction aime la connaissance, mais celui qui hait la réprimande est abruti.
12:2	L'homme bon obtient la faveur de YHWH, mais il condamne l'homme qui forme des complots.
12:3	L'être humain ne sera pas affermi par la méchanceté, mais la racine des justes ne sera pas ébranlée.
12:4	La femme talentueuse est la couronne de son mari<!--Pr. 31:10.-->, mais celle qui fait honte est comme la pourriture dans ses os.
12:5	Les pensées des justes ne sont que justice, mais les conseils des méchants ne sont que fraude.
12:6	Les paroles des méchants ne tendent qu'à dresser des embûches pour répandre le sang, mais la bouche des justes les délivrera.
12:7	Les méchants sont renversés, et ils ne sont plus, mais la maison des justes se maintiendra.
12:8	Un homme est loué d’après sa prudence, mais celui qui a le cœur pervers devient l'objet du mépris.
12:9	Mieux vaut un homme méprisé qui a un serviteur qu'un homme qui se glorifie et manque de pain.
12:10	Le juste a égard à l'âme de sa bête, mais les matrices des méchants sont cruelles.
12:11	Celui qui cultive son sol sera rassasié de pain, mais celui qui court après des futilités manque de cœur.
12:12	Le méchant convoite le filet des hommes mauvais, mais la racine des justes produit.
12:13	Il y a dans la transgression des lèvres un piège pernicieux, mais le juste sortira de la détresse.
12:14	L'homme sera rassasié de ce qui est bon par le fruit de sa bouche, et on rendra à l'être humain selon l'œuvre de ses mains.
12:15	La voie du fou est droite à ses yeux, mais celui qui écoute le conseil est sage.
12:16	Quant au fou, sa colère est révélée le jour même, mais l'homme bien avisé couvre son ignominie.
12:17	Celui qui prononce des choses véritables rend un témoignage juste, mais le faux témoin fait des rapports trompeurs.
12:18	Tel qui parle à la légère perce comme une épée, mais la langue des sages apporte la guérison.
12:19	La lèvre de vérité est affermie pour toujours, mais la langue de mensonge n’est que pour un instant<!--Ps. 52:6-7.-->.
12:20	La tromperie est dans le cœur de ceux qui complotent le mal, mais la joie est pour ceux qui conseillent la paix.
12:21	Aucun malheur n'arrivera au juste, mais les méchants seront remplis de maux.
12:22	Les lèvres mensongères sont une abomination à YHWH<!--Ap. 22:15.-->, mais ceux qui agissent fidèlement lui sont agréables.
12:23	L'humain prudent cache sa connaissance, mais le cœur des insensés proclame la folie.
12:24	La main des diligents gouvernera, mais la main paresseuse sera tributaire.
12:25	L'anxiété dans le cœur de l'homme le courbe, mais une bonne parole le réjouit.
12:26	Le juste explore la voie pour son compagnon, mais le chemin des méchants les égare.
12:27	Le paresseux ne rôtit pas son gibier, mais les richesses précieuses de l'être humain sont au diligent.
12:28	La vie est dans le chemin de la justice, et la voie de son sentier ne tend pas à la mort.

## Chapitre 13

### La justice s'oppose à la méchanceté (suite)

13:1	Un fils sage écoute la correction de son père, mais le moqueur n'écoute pas la réprimande<!--Ps. 1:1.-->.
13:2	Par le fruit de sa bouche l’homme mangera ce qui est bon, mais l'âme des traîtres n'est que violence.
13:3	Celui qui veille<!--Surveiller.--> sur sa bouche garde son âme, mais celui qui ouvre grand ses lèvres se ruine<!--Ps. 39:2.-->.
13:4	L'âme du paresseux a des désirs qu'il ne peut satisfaire, mais l'âme des diligents sera engraissée.
13:5	Le juste hait la parole mensongère, mais elle rend le méchant odieux et le fait tomber dans la confusion.
13:6	La justice garde celui qui est intègre dans sa voie, mais la méchanceté tord le pécheur.
13:7	Tel fait le riche et n'a rien du tout, tel autre fait le pauvre et possède de grandes richesses.
13:8	Les richesses d'un homme servent de rançon pour son âme, mais le pauvre n'entend pas les réprimandes.
13:9	La lumière des justes réjouit, mais la lampe des méchants sera éteinte.
13:10	L'orgueil ne produit que des querelles, mais la sagesse est avec ceux qui écoutent les conseils.
13:11	Les richesses provenant de la vanité seront diminuées, mais celui qui amasse à la main les augmentera.
13:12	Un espoir différé fait languir le cœur, mais un désir accompli est un arbre de vies.
13:13	Celui qui méprise la parole sera détruit, mais celui qui craint le commandement sera récompensé.
13:14	La torah du sage est une source de vies pour détourner des pièges de la mort.
13:15	Une bonne compréhension donne de la grâce, mais la voie des traîtres est perpétuelle.
13:16	Tout homme prudent agit avec connaissance, mais l'insensé déploie sa folie<!--Da. 11:32.-->.
13:17	Un messager méchant tombe dans le malheur, mais un messager digne de confiance apporte la guérison.
13:18	La pauvreté et l'ignominie arrivent à celui qui rejette la correction, mais celui qui garde la réprimande est honoré.
13:19	Un désir accompli est doux à l'âme, mais se détourner du mal est une abomination pour les insensés.
13:20	Celui qui marche avec les sages deviendra sage, mais le compagnon<!--« Faire paître », « soigner », « nourrir ».--> des insensés sera détruit.
13:21	Le mal poursuit les pécheurs, mais le bien sera rendu aux justes.
13:22	Quelqu’un qui est bon laissera de quoi hériter aux fils de ses fils, mais la richesse du pécheur est mise en réserve pour le juste<!--Voir Ec. 2:26.-->.
13:23	Le sol labourable du pauvre abonde de nourriture, mais il en est qui périssent faute de justice.
13:24	Celui qui épargne sa verge hait son fils, mais celui qui l'aime se hâte de le corriger.
13:25	Le juste mange jusqu’à ce que son âme soit rassasiée, mais le ventre des méchants est vide.

## Chapitre 14

### La justice s'oppose à la méchanceté (suite)

14:1	La femme sage bâtit sa maison, mais la folle la ruine de ses mains.
14:2	Celui qui marche dans la droiture craint YHWH, mais celui dont les voies sont perverses le méprise.
14:3	La verge d'orgueil est dans la bouche du fou, mais les lèvres des sages les garderont.
14:4	Là où il n'y a pas de bœuf, la crèche est vide, mais l'abondance du revenu provient de la force du bœuf.
14:5	Le témoin digne de confiance ne ment pas, mais le faux témoin exhale le mensonge.
14:6	Le moqueur cherche la sagesse et ne la trouve pas, mais la connaissance est aisée à trouver pour l'homme intelligent.
14:7	Va-t’en de devant l'homme insensé : ce n'est pas sur ses lèvres que tu aperçois la connaissance.
14:8	La sagesse d'un homme prudent est de connaître les règles de sa voie, mais la folie des insensés est la tromperie.
14:9	Les fous se moquent de la culpabilité, mais parmi les justes se trouve la faveur.
14:10	Le cœur connaît l’amertume de son âme et nul étranger ne partage sa joie.
14:11	La maison des méchants sera détruite, mais la tente des justes fleurira.
14:12	Il y a une voie qui semble droite à l'homme, mais dont l'issue est la voie de la mort.
14:13	Même en riant le cœur peut être triste, et la fin de la joie c'est le chagrin.
14:14	Celui qui a un cœur hypocrite sera rassasié de ses voies, mais l'homme bon sera rassasié de ce qui est en lui.
14:15	Le simple croit à toute parole, mais le prudent discerne ses pas.
14:16	Le sage craint et se détourne du mal, mais l'insensé se met en colère et il est tout confiant.
14:17	Celui qui est prompt à la colère agit follement<!--Ps. 37:8.-->, et l'homme plein de ruse est haï.
14:18	Les naïfs hériteront la folie, mais les prudents seront couronnés de connaissance.
14:19	Les mauvais s'inclinent devant les bons, et les méchants aux portes du juste.
14:20	Le pauvre est haï même par son ami, mais les amis du riche sont nombreux !
14:21	Celui qui méprise son prochain commet un péché, mais celui qui a pitié des pauvres est heureux.
14:22	Ceux qui méditent le mal ne s'égarent-ils pas ? Mais la bonté et la vérité sont pour ceux qui méditent le bien.
14:23	Tout travail procure un profit, mais les discours des lèvres ne mènent qu'à la disette.
14:24	La richesse est une couronne pour les sages ; la folie des insensés est folie.
14:25	Le témoin fidèle délivre les âmes, mais celui qui respire le mensonge est trompeur.
14:26	Dans la crainte de YHWH il y a une ferme assurance, et une retraite pour ses fils.
14:27	La crainte de YHWH est une source de vies pour se détourner des pièges de la mort.
14:28	La gloire d'un roi, c'est la multitude du peuple, mais quand le peuple manque, c'est la ruine du prince.
14:29	Celui qui est lent à la colère a une grande intelligence, mais celui qui est prompt à s'emporter excite la folie.
14:30	La vie de la chair, c'est la santé du cœur, mais la jalousie est la pourriture des os.
14:31	Celui qui fait tort au pauvre déshonore celui qui l'a fait, mais celui qui a pitié de l'indigent honore YHWH<!--De. 24:11 ; Ps. 107:41.-->.
14:32	Le méchant est chassé par son mal, mais le juste trouve un refuge dans sa mort.
14:33	La sagesse repose dans un cœur intelligent, et elle se fait connaître au milieu des insensés.
14:34	La justice élève une nation, mais le péché est la honte des peuples.
14:35	Le roi prend plaisir au serviteur prudent, mais sa fureur sera contre celui qui lui fait honte.

## Chapitre 15

### La justice s'oppose à la méchanceté (suite)

15:1	Une réponse douce détourne le courroux, mais une parole qui peine fait monter la colère.
15:2	La langue des sages se réjouit de la connaissance, mais la bouche des insensés profère la sottise.
15:3	Les yeux de YHWH sont en tous lieux, observant les méchants et les bons.
15:4	La langue guérisseuse est un arbre de vies, mais celle où il y a de la perversité est une brèche dans l'esprit.
15:5	Le fou méprise la correction de son père, mais celui qui prend garde à la réprimande agit avec prudence.
15:6	Il y a un grand trésor dans la maison du juste, mais il y a du trouble dans les revenus du méchant.
15:7	Les lèvres des sages répandent partout la connaissance, mais il n'en est pas ainsi du cœur des insensés.
15:8	Le sacrifice des méchants est une abomination pour YHWH, mais la prière des justes lui est agréable.
15:9	La voie du méchant est une abomination pour YHWH, mais il aime celui qui poursuit la justice.
15:10	La correction est désagréable pour celui qui abandonne le chemin, et celui qui hait la réprimande mourra.
15:11	Le shéol et l'abîme sont devant YHWH. Combien plus les cœurs des fils des humains !
15:12	Celui qui parle avec arrogance n'aime pas qu'on le corrige, et il ne va pas vers les sages.
15:13	Le cœur joyeux rend les faces heureuses, mais l'esprit est abattu par la douleur du cœur.
15:14	Le cœur de l'homme prudent cherche la connaissance, mais la bouche des insensés se nourrit de folie.
15:15	Tous les jours de l'affligé sont mauvais, mais un cœur heureux est un festin perpétuel.
15:16	Mieux vaut peu avec la crainte de YHWH, qu'un grand trésor avec lequel il y a du trouble<!--Ps. 37:16.-->.
15:17	Mieux vaut un plat de légumes là où il y a de l'amitié, qu'un bœuf engraissé là où il y a de la haine.
15:18	Un homme coléreux excite la querelle, mais l'homme lent à la colère apaise la dispute.
15:19	La voie du paresseux est comme une haie d'épines, mais le chemin des justes est aplani.
15:20	Un fils sage réjouit son père, et un homme insensé méprise sa mère.
15:21	La stupidité est la joie de celui qui manque de cœur, mais un homme prudent dresse ses pas au chemin de la droiture.
15:22	Les projets échouent là où il n'y a pas de conseil, mais ils s'accomplissent quand il y a de nombreux conseillers.
15:23	L'homme a de la joie dans les réponses de sa bouche, et combien est bonne une parole dite en son temps !
15:24	Le chemin de vies est en haut pour celui qui est prudent, afin qu'il se détourne du shéol qui est en bas<!--Es. 14:15.-->.
15:25	YHWH renverse la maison des orgueilleux, mais il affermit la borne de la veuve.
15:26	Les pensées mauvaises sont une abomination pour YHWH, mais les paroles pleines de bonté sont pures.
15:27	Celui qui est avide de gain injuste trouble sa maison, mais celui qui hait les présents vivra.
15:28	Le cœur du juste médite pour répondre, mais la bouche des méchants profère des choses mauvaises.
15:29	YHWH est loin des méchants, mais il écoute la prière des justes.
15:30	La lumière des yeux réjouit le cœur, et la bonne nouvelle oint les os.
15:31	L'oreille qui écoute la correction qui donne la vie habite parmi les sages.
15:32	Celui qui rejette la correction méprise son âme, mais celui qui écoute la réprimande acquiert le cœur.
15:33	La crainte de YHWH est la discipline de la sagesse, et l'humilité précède la gloire<!--Ps. 19:10.-->.

## Chapitre 16

### La justice s'oppose à la méchanceté (suite)

16:1	Les projets du cœur dépendent de l'être humain, mais la réponse de la langue vient de YHWH.
16:2	Toutes les voies de l'homme sont pures à ses yeux, mais celui qui pèse les esprits, c'est YHWH.
16:3	Recommande tes affaires à YHWH, et tes projets seront bien ordonnés.
16:4	YHWH a tout fait pour sa réponse, même le méchant pour le jour mauvais.
16:5	Tout cœur hautain est en abomination à YHWH. De la main à la main, il ne restera pas impuni.
16:6	Par la miséricorde et la vérité on fait la propitiation de l'iniquité, et par la crainte de YHWH on se détourne du mal.
16:7	Quand YHWH prend plaisir aux voies d'un homme, il apaise envers lui même ses ennemis.
16:8	Il vaut mieux peu avec justice, qu'un gros revenu là où il n'y a pas de justice.
16:9	Le cœur de l'être humain médite sur sa voie, mais YHWH conduit ses pas.
16:10	La divination est sur les lèvres du roi : que sa bouche ne commette pas un délit dans le jugement !
16:11	La balance et le poids justes sont à YHWH, tous les poids du sachet sont aussi son œuvre.
16:12	Agir avec méchanceté est une abomination pour les rois, car un trône s'affermit par la justice.
16:13	La faveur du roi est pour les lèvres justes, et celui qui parle avec droiture il l'aime.
16:14	Le courroux du roi est comme des messagers de mort, mais l'homme sage l'apaisera.
16:15	La lumière sur les faces du roi, c'est la vie, et sa faveur est comme une pluie de printemps<!--Ou la dernière pluie.-->.
16:16	Combien acquérir la sagesse est meilleur que l'or ! Et acquérir le discernement est préférable à l'argent !
16:17	La grande route des justes, c'est de se détourner du mal. Celui qui veille sur sa voie garde son âme.
16:18	Avant l'écrasement, il y a l'orgueil, et avant la chute, l'esprit hautain.
16:19	Mieux vaut être d'un esprit humble avec les humbles, que de partager le butin avec les orgueilleux.
16:20	Celui qui prête attention à la parole trouve le bonheur, et celui qui se confie en YHWH est heureux<!--Ps. 2:12.-->.
16:21	On appellera prudent le sage de cœur, et la douceur des lèvres augmente l'instruction.
16:22	La prudence est une source de vies pour ceux qui la possèdent, mais la correction des fous c'est leur folie.
16:23	Le cœur sage rend la bouche prudente et augmente l'instruction sur ses lèvres.
16:24	Les paroles agréables sont des rayons de miel, douces à l'âme et santé pour les os.
16:25	Il existe une voie qui semble droite à l'homme, mais dont la fin est la voie de la mort.
16:26	L'âme de celui qui travaille, travaille pour lui-même, parce que sa bouche l'y excite<!--Ec. 6:7.-->.
16:27	L'homme de Bélial<!--Voir commentaire en De. 13:14.--> creuse le mal, et il y a comme un feu brûlant sur ses lèvres.
16:28	L'homme qui use de perversité sème des querelles, et le rapporteur divise les grands amis.
16:29	L'homme violent attire son compagnon et le fait marcher dans une voie qui n'est pas bonne.
16:30	Il ferme les yeux pour méditer des choses perverses, et remuant ses lèvres il exécute le mal.
16:31	Les cheveux gris sont une couronne d'honneur : c'est sur la voie de la justice qu'on la trouve.
16:32	Celui qui est lent à la colère vaut mieux que l'homme vaillant, et celui qui est maître de son cœur vaut mieux que celui qui prend des villes.
16:33	On jette le sort dans un creux, mais c'est de YHWH que vient tout jugement<!--Ou « action de décider d'une cause ».-->.

## Chapitre 17

### La justice s'oppose à la méchanceté (suite)

17:1	Mieux vaut un morceau de pain sec là où il y a la paix, qu'une maison pleine de viandes, là où il y a des querelles.
17:2	Un serviteur prudent dominera sur le fils qui fait honte, et il partagera l'héritage avec les frères.
17:3	Le creuset est pour éprouver l'argent, et le fourneau l'or, mais YHWH éprouve les cœurs<!--Jé. 17:10 ; Mal. 3:3 ; Ps. 26:2.-->.
17:4	Le méchant est attentif à la lèvre trompeuse, et le menteur écoute la mauvaise langue.
17:5	Celui qui se moque du pauvre déshonore celui qui l'a fait. Celui qui se réjouit d'un désastre ne restera pas impuni.
17:6	Les fils des fils sont la couronne des vieillards<!--Ps. 127:3, 128:3.-->, et les pères sont la splendeur de leurs fils.
17:7	Le langage excellent ne convient pas à un insensé<!--Vient de l'hébreu « nabal » qui signifie aussi « imbécile », « bouffon ».-->. Combien moins au noble le langage de mensonge ! 
17:8	Les pots-de-vin sont une pierre précieuse aux yeux de ceux qui les possèdent : où qu'ils se tournent, ils réussissent.
17:9	Celui qui couvre une transgression cherche l'amour, mais celui qui répète la chose divise les amis.
17:10	Une réprimande pénètre<!--Vient d'un mot hébreu qui signifie « descendre, aller en bas ».--> plus dans celui qui est intelligent que 100 coups dans l'insensé.
17:11	Le méchant ne cherche que rébellion, mais le messager cruel sera envoyé contre lui.
17:12	Il vaut mieux qu'un homme rencontre une ourse privée de ses petits qu'un fou dans sa folie.
17:13	Le mal ne partira pas de la maison de celui qui rend le mal pour le bien.
17:14	Le commencement d'une dispute est comme de l'eau qui s'échappe : avant que la querelle ne soit exposée, va-t’en !
17:15	Celui qui déclare juste le méchant et celui qui déclare méchant le juste, sont tous deux en abomination à YHWH<!--Ex. 23:7 ; Es. 5:23.-->.
17:16	À quoi sert le prix dans la main du fou ? Il n'a pas à cœur d'acheter la sagesse !
17:17	L'ami aime en tout temps, et un frère est né pour la détresse.
17:18	L'être humain qui manque de cœur frappe dans la paume et s'engage comme caution envers son prochain.
17:19	Celui qui aime les querelles aime la transgression, celui qui élève sa porte cherche sa ruine.
17:20	Un cœur pervers ne trouve pas le bonheur, et celui qui tourne sa langue ici et là tombe dans le malheur.
17:21	Celui qui engendre un insensé en aura du chagrin, et le père d'un insensé ne se réjouira pas.
17:22	Le cœur joyeux est un remède, mais l'esprit abattu dessèche les os.
17:23	Le méchant prend les pots-de-vin en secret pour pervertir les voies du jugement.
17:24	La sagesse est en face de l'homme prudent, mais les yeux du fou sont à l'extrémité de la Terre.
17:25	Un fils insensé est le chagrin de son père, et l'amertume de celle qui l'a enfanté.
17:26	Il n'est pas bon de condamner l'innocent à l'amende, ni aux nobles de frapper quelqu'un pour avoir agi avec droiture.
17:27	Celui qui retient ses paroles sait ce qu'est la connaissance, et celui qui a l'esprit calme est un homme intelligent.
17:28	Même le fou, quand il se tait, est réputé sage, et celui qui ferme ses lèvres est réputé intelligent.

## Chapitre 18

### La justice s'oppose à la méchanceté (suite)

18:1	Celui qui veut se séparer cherche son propre désir, et il s'emporte contre tout succès durable.
18:2	L'insensé ne prend pas plaisir à l'intelligence, mais à ce que son cœur soit manifesté.
18:3	Quand vient le méchant, vient aussi le mépris, et avec la honte, l’insulte.
18:4	Les paroles de la bouche d'un homme sont des eaux profondes, et la source de la sagesse est un torrent qui bouillonne<!--Jn. 4:14.-->.
18:5	Il n'est pas bon de porter les faces du méchant en faisant fléchir le droit du juste.
18:6	Les lèvres de l'insensé entrent en querelle, et sa bouche appelle les combats.
18:7	La bouche de l'insensé est une ruine pour lui, et ses lèvres sont un piège pour son âme.
18:8	Les paroles du rapporteur sont comme des friandises, elles descendent jusqu'au fond des entrailles.
18:9	Celui qui se relâche dans son travail est frère du maître destructeur.
18:10	Le Nom de YHWH est une tour forte, le juste y court et y trouve une haute retraite.
18:11	Les richesses du riche sont sa ville forte et comme une haute muraille de retraite, selon son imagination.
18:12	Le cœur de l'homme s'élève avant que la ruine arrive, mais l'humilité précède la gloire.
18:13	Celui qui répond à un discours avant de l'avoir entendu, fait un acte de folie et attire la confusion.
18:14	L'esprit d'un homme le soutiendra dans sa maladie, mais l'esprit abattu, qui le relèvera ?
18:15	Le cœur de celui qui est intelligent acquiert la connaissance, et l'oreille des sages cherche la connaissance.
18:16	Le présent d'un être humain l'élargit et le conduit devant les grands.
18:17	Dans un procès, le premier est juste, mais son prochain vient, et l’examine.
18:18	Le sort fait cesser les procès et fait les partages entre les puissants.
18:19	Un frère qui se rebelle est plus inaccessible qu'une ville forte, et les disputes sont comme les verrous d'une forteresse.
18:20	Le ventre de chacun est rassasié du fruit de sa bouche, il se rassasie du revenu de ses lèvres.
18:21	La mort et la vie sont au pouvoir de la langue<!--Littéralement : « Mort et vie dans la main de la langue. ». Mt. 12:37.-->, et celui qui l'aime en mangera les fruits.
18:22	Celui qui trouve une femme trouve le bonheur et il obtient une faveur de YHWH.
18:23	Le pauvre ne prononce que des supplications, mais le riche répond avec des paroles fortes.
18:24	Avoir des amis peut être nuisible à un homme, mais il existe tel ami plus attaché qu’un frère.

## Chapitre 19

### La justice s'oppose à la méchanceté (suite)

19:1	Le pauvre qui marche dans son intégrité vaut mieux que celui qui pervertit ses lèvres et qui est un insensé.
19:2	De même, il n'est pas bon que l'âme soit sans la connaissance, et celui qui se hâte avec ses pieds pèche<!--Ou manque le but.-->.
19:3	La folie de l'être humain tord sa voie et c'est contre YHWH que son cœur se fâche.
19:4	Les richesses attirent un grand nombre d'amis, mais celui qui est pauvre est abandonné même par son ami.
19:5	Le faux témoin ne restera pas impuni, et celui qui profère des mensonges n'échappera pas.
19:6	Beaucoup supplient en face l'homme généreux, et tout le monde est ami de l'homme qui fait des présents.
19:7	Tous les frères du pauvre le haïssent, à plus forte raison ses amis s'éloignent-ils de lui ! Il les poursuit de ses discours, mais ils ne sont plus là.
19:8	Celui qui acquiert un cœur aime son âme, et celui qui prend garde à l'intelligence trouvera le bonheur.
19:9	Le faux témoin ne restera pas impuni, et celui qui profère des mensonges périra.
19:10	Il ne convient pas à un insensé de vivre dans le luxe, encore moins à un esclave de gouverner les princes !
19:11	La prudence d'un humain retient sa colère, et sa splendeur, c'est de passer par-dessus la transgression.
19:12	La fureur du roi est comme le rugissement d'un jeune lion, mais sa faveur est comme la rosée sur l'herbe.
19:13	Un fils insensé est un grand malheur pour son père, et les querelles d'une femme sont une gouttière continuelle.
19:14	Maison et richesse sont l’héritage des pères, mais la femme prudente vient de YHWH.
19:15	La paresse fait venir le sommeil, et l'âme paresseuse a faim.
19:16	Celui qui garde le commandement garde son âme, celui qui méprise ses voies mourra.
19:17	Celui qui a pitié du pauvre prête à YHWH, qui lui rendra son bienfait.
19:18	Châtie ton fils tandis qu'il y a de l'espérance, mais ne t’emporte pas jusqu’à faire mourir son âme.
19:19	Un homme qui se laisse emporter par le courroux en portera la peine. Si tu l'en délivres, tu devras recommencer.
19:20	Écoute le conseil et reçois la correction, afin que tu deviennes sage à la fin de tes jours.
19:21	Il y a dans le cœur de l'homme beaucoup de projets, mais c'est le conseil de YHWH qui s'accomplit<!--Es. 46:10 ; Ps. 33:11.-->.
19:22	Le désir d'un être humain c'est la bonté, et le pauvre vaut mieux que l’homme menteur.
19:23	La crainte de YHWH conduit à la vie, et celui qui l'a, passe la nuit étant rassasié, sans qu'il soit visité par aucun mal.
19:24	Le paresseux cache sa main dans le plat, mais il ne la ramène pas à sa bouche.
19:25	Frappe le moqueur, le stupide prendra garde. Corrige celui qui est intelligent, et il comprendra la connaissance.
19:26	Celui qui ruine son père et qui fait fuir sa mère est un fils qui fait honte et qui cause de l'embarras.
19:27	Cesse, mon fils, d'obéir à une discipline, si c'est pour t'égarer loin des paroles de la sagesse.
19:28	Le témoin de Bélial<!--Voir commentaire en De. 13:14.--> se moque de la justice, et la bouche des méchants engloutit l'iniquité.
19:29	Les jugements sont établis pour les moqueurs, et les coups pour le dos des insensés.

## Chapitre 20

### La justice s'oppose à la méchanceté (suite)

20:1	Le vin est moqueur et la boisson forte est bruyante, quiconque en fait excès n'est pas sage.
20:2	La terreur du roi est comme le rugissement d'un jeune lion, celui qui se met en colère contre lui pèche contre son âme.
20:3	C'est une gloire pour l'homme de s'abstenir des disputes, mais tous les fous s'emportent.
20:4	Le paresseux ne laboure pas à cause de l’hiver, au temps de la moisson, il demandera, mais il n’y aura rien.
20:5	Le conseil dans le cœur d’un homme est une eau profonde, et l’homme d’intelligence y puise.
20:6	Beaucoup d'humains se proclament hommes de bonté, mais qui trouvera un homme de confiance ?
20:7	Le juste marche dans son intégrité, heureux ses fils après lui !
20:8	Le roi assis sur le trône de justice, de ses yeux, il disperse tout mal !
20:9	Qui est-ce qui peut dire : J'ai purifié mon cœur, je suis pur de mon péché ?
20:10	Pierre et pierre, épha et épha<!--Deux poids, deux mesures.--> : les deux sont aussi en abomination pour YHWH.
20:11	On reconnaît en effet par les actions d'un jeune homme si son œuvre sera pure et si elle sera droite.
20:12	L'oreille qui entend et l'œil qui voit, YHWH les a faits tous les deux.
20:13	N'aime pas le sommeil ! De peur que tu ne deviennes pauvre. Ouvre tes yeux et tu seras rassasié de pain.
20:14	Mauvais ! Mauvais ! Dit l'acheteur, puis il s'en va et se vante.
20:15	Il y a de l'or et beaucoup de perles, mais les lèvres de la connaissance sont un vase précieux.
20:16	Si quelqu'un se porte garant pour un étranger, saisis son vêtement et prends de lui un gage, à cause de cet inconnu !
20:17	Le pain du mensonge est doux à l'homme, mais ensuite sa bouche sera remplie de gravier.
20:18	Les projets s'affermissent par le conseil : fais la guerre avec de sages conseils.
20:19	Celui qui marche en calomniant révèle les secrets. Ne te mêle pas avec celui qui séduit par ses lèvres.
20:20	La lampe de celui qui traite avec mépris son père ou sa mère, s'éteindra au milieu des ténèbres les plus noires<!--Ex. 21:17 ; Lé. 20:9 ; Mt. 15:4.-->.
20:21	L'héritage pour lequel on s'est hâté d'acquérir au début ne sera pas béni à la fin.
20:22	Ne dis pas : Je rendrai le mal. Mais espère en YHWH, et il te sauvera.
20:23	Pierre et pierre<!--Le double poids.--> sont en abomination à YHWH, et une balance trompeuse n'est pas une chose bonne.
20:24	Les pas de l'homme fort sont dirigés par YHWH, comment l'être humain peut-il comprendre sa voie ?
20:25	C'est un piège pour l'être humain de dire précipitamment : C'est sacré ! et, après les vœux, de réfléchir.
20:26	Un roi sage dissipe les méchants et fait tourner la roue sur eux.
20:27	L'esprit de l'être humain est une lampe de YHWH : elle fouille jusqu'au fond des entrailles.
20:28	La bonté et la vérité gardent le roi, et il soutient son trône par la bonté.
20:29	La force des jeunes hommes est leur splendeur, et les cheveux gris sont l'honneur des anciens.
20:30	Les plaies d'une blessure sont un remède au mal, de même les coups qui pénètrent jusqu'au fond de l'âme.

## Chapitre 21

### La justice s'oppose à la méchanceté (suite)

21:1	Le cœur du roi est un canal d'eau dans la main de YHWH : il l'incline partout où il veut.
21:2	Chaque voie de l'homme lui semble droite, mais c'est YHWH qui pèse les cœurs.
21:3	Pratiquer la justice et le droit est pour YHWH préférable au sacrifice.
21:4	Les yeux hautains et le cœur enflé, ce sol labourable des méchants n'est que péché.
21:5	Les projets d'un homme diligent produisent l'abondance, mais tout homme qui se presse tombe dans la pauvreté.
21:6	Acquérir des trésors par une langue trompeuse, c'est une vanité poussée au loin par ceux qui cherchent la mort.
21:7	La violence des méchants les abat, parce qu'ils refusent de pratiquer le droit.
21:8	La voie de l'homme criminel est tortueuse, mais l'œuvre de celui qui est pur est droite.
21:9	Il vaut mieux habiter au coin d'un toit, que dans une maison spacieuse avec une femme querelleuse.
21:10	L'âme du méchant désire le mal, et son prochain ne trouve pas grâce à ses yeux.
21:11	Quand on punit le moqueur, le stupide devient sage, et quand on instruit le sage, il reçoit la connaissance.
21:12	Le juste considère la maison du méchant et il tord les méchants dans la détresse.
21:13	Celui qui ferme son oreille au cri<!--Cri de détresse.--> du pauvre criera lui-même et on ne lui répondra pas.
21:14	Le présent fait en secret apaise le courroux, et un pot-de-vin fait en cachette calme une fureur violente.
21:15	C'est une joie pour le juste de pratiquer le droit, mais la destruction est pour ceux qui pratiquent la méchanceté.
21:16	L'humain qui se détourne du chemin de la sagesse aura sa demeure dans l'assemblée des fantômes<!--Vient d'un mot hébreu qui signifie « fantômes de morts, ombres, revenants » ou encore « esprits ».-->.
21:17	L'homme qui aime les plaisirs sera dans la pauvreté, et celui qui aime le vin et l'huile ne s'enrichit pas.
21:18	Le méchant sert de rançon pour l'innocent, et l'infidèle pour les justes.
21:19	Il vaut mieux habiter dans une terre déserte qu'avec une femme querelleuse et qui se met en colère.
21:20	Il y a un trésor précieux et de l'huile dans la demeure du sage, mais un être humain fou les engloutit.
21:21	Celui qui poursuit la justice et la miséricorde trouve la vie, la justice et la gloire.
21:22	Le sage entre dans la ville des hommes vaillants et il rabaisse la force de sa confiance.
21:23	Celui qui garde sa bouche et sa langue garde son âme de la détresse.
21:24	Moqueur, tel est le nom de l'arrogant, de l'orgueilleux qui agit dans un débordement d'orgueil.
21:25	Le désir du paresseux le tue, parce que ses mains refusent de travailler.
21:26	Tout le jour il convoite et il désire, mais le juste donne et n'épargne rien.
21:27	Le sacrifice des méchants est une abomination, combien plus quand ils l'apportent avec une mauvaise intention<!--1 S. 15:22.--> ?
21:28	Le témoin menteur périra, mais l'homme qui écoute parlera avec gain de cause.
21:29	L'homme méchant durcit ses faces, mais le juste affermit ses voies.
21:30	Il n'y a ni sagesse, ni intelligence, ni conseil, contre YHWH.
21:31	Le cheval est équipé pour le jour de la bataille, mais la délivrance vient de YHWH.

## Chapitre 22

### La justice s'oppose à la méchanceté (suite)

22:1	Un nom<!--Vient de l'hébreu « shem » et signifie aussi « réputation », « renommée ». Voir Es. 56:5 ; Ap. 2:17 ; Ec. 7:1.--> est préférable à de grandes richesses<!--Ec. 7:1.-->, et la grâce est meilleure que l'or et l'argent.
22:2	Le riche et le pauvre se rencontrent. Celui qui les a tous faits, c’est YHWH<!--Lu. 16.-->.
22:3	L'homme prudent voit le mal et se cache, mais les stupides passent et en portent la peine.
22:4	La récompense de l'humilité et de la crainte de YHWH sont les richesses, la gloire et la vie.
22:5	Il y a des épines et des pièges sur la voie du pervers, celui qui aime son âme s'en éloigne.
22:6	Entraîne<!--« Entraîner », « dédier », « inaugurer ». Littéralement : « Consacre » ou « dédie », ou encore : « entraîne le jeune homme à la bouche (entrée) de sa voie ».--> le jeune homme à l'entrée<!--Bouche.--> de sa voie, même quand il sera vieux, il ne s'en détournera pas.
22:7	Le riche domine sur les pauvres<!--Ja. 2:6.-->, et celui qui emprunte est l'esclave de l'homme qui prête.
22:8	Celui qui sème l'injustice moissonne le malheur<!--Job 4:8 ; Ga. 6:7.-->, et la verge de sa colère prendra fin.
22:9	Celui qui a l'œil bienveillant sera béni, parce qu'il donne de son pain au pauvre.
22:10	Chasse le moqueur et la discorde s’en ira, les disputes et la honte cesseront.
22:11	Le roi est ami de celui qui aime la pureté de cœur, et qui a de la grâce dans ses paroles.
22:12	Les yeux de YHWH veillent sur la connaissance, et il tord les paroles du traître.
22:13	Le paresseux dit : Le lion est dehors ! Je serai tué dans les rues !
22:14	La bouche des étrangers est une fosse profonde, celui contre qui YHWH est indigné y tombera.
22:15	La folie est liée au cœur du jeune homme, la verge de la correction l'éloignera de lui.
22:16	Celui qui opprime le pauvre pour s’accroître ou qui donne au riche, finira en effet dans la pauvreté.
22:17	Prête ton oreille, écoute les paroles des sages et applique ton cœur à ma connaissance.
22:18	Car il est agréable que tu les gardes dans ton ventre, pour qu’elles soient établies ensemble sur tes lèvres.
22:19	Afin que ta confiance soit en YHWH, je te l'ai aujourd'hui fait connaître à toi.
22:20	N’ai-je pas écrit pour toi comme auparavant sur le conseil et la connaissance,
22:21	pour te faire connaître la vérité, des paroles de vérité, afin de répondre par des paroles de vérité à ceux qui t'envoient ?
22:22	Ne dépouille pas le pauvre parce qu'il est pauvre, et n'opprime pas le malheureux à la porte.
22:23	Car YHWH défendra leur cause et il dépouillera l’âme de ceux qui les auront dépouillés.
22:24	Ne t'associe pas au maître colère, ne va pas avec l'homme coléreux !
22:25	De peur que tu n'apprennes ses voies<!--1 Co. 15:33.--> et que tu ne reçoives un piège pour ton âme.
22:26	Ne sois pas parmi ceux qui frappent dans la paume, parmi ceux qui se portent garants pour des prêts.
22:27	Si tu n'as pas de quoi payer, pourquoi prendrait-on ton lit de dessous toi ?
22:28	Ne déplace pas la borne ancienne que tes pères ont faite.
22:29	As-tu vu un homme habile dans son travail ? Il se tiendra devant des rois, il ne se tiendra pas devant des gens insignifiants.

## Chapitre 23

### La justice s'oppose à la méchanceté (suite)

23:1	Quand tu t'assieds pour manger avec un gouverneur, considère avec attention celui qui est devant toi.
23:2	Mets un couteau à la gorge si tu es maître de ton âme !
23:3	Ne désire pas ses mets, car c'est une nourriture trompeuse.
23:4	Ne te fatigue<!--« Peiner », « travailler », « se lasser », « être las ».--> pas pour devenir riche<!--1 Ti. 6:9.-->, aie le discernement de t'arrêter.
23:5	Tes yeux voleraient-ils vers elle ? Elle ne sera plus là ! Car elle se fera, elle se fera des ailes et s'envolera, elle s'envolera comme un aigle vers les cieux.
23:6	Ne mange pas le pain de celui qui a mauvais œil, et ne désire pas ses mets,
23:7	car il est tel qu'il pense dans son âme. Il te dira bien : Mange et bois, mais son cœur n'est pas avec toi.
23:8	Tu vomiras le morceau que tu as mangé, et tu auras perdu tes paroles agréables.
23:9	Ne parle pas aux oreilles de l'insensé, car il méprise la prudence de ton discours.
23:10	Ne déplace pas la borne ancienne et n'entre pas dans les champs des orphelins,
23:11	car leur Racheteur est puissant, il défendra leur cause contre toi.
23:12	Applique ton cœur à la correction, et tes oreilles aux paroles de connaissance.
23:13	Ne refuse pas la correction au garçon ! Si tu le frappes de la verge, il n'en mourra pas.
23:14	En le frappant de la verge, tu sauveras son âme du shéol.
23:15	Mon fils, si ton cœur est sage, mon cœur à moi s'en réjouira.
23:16	Mes reins se réjouiront quand tes lèvres diront ce qui est droit.
23:17	Que ton cœur ne porte pas d'envie aux pécheurs, mais sois tout le jour dans la crainte de YHWH.
23:18	Car il y aura certainement une issue, et ton espérance ne sera pas détruite.
23:19	Toi, mon fils, écoute et sois sage ! Dirige ton cœur dans cette voie !
23:20	Ne sois pas parmi les ivrognes, parmi les gaspilleurs des viandes<!--Ro. 13:13 ; Ep. 5:18 ; Ga. 5:18-21.-->.
23:21	Car l'ivrogne et le gaspilleur s'appauvrissent, et la paresse fait porter des vêtements déchirés.
23:22	Écoute ton père, celui qui t'a engendré ! Ne méprise pas ta mère quand elle sera devenue vieille !
23:23	Achète la vérité, et ne la vends pas, achète la sagesse, la correction et le discernement.
23:24	Le père du juste sera dans l’allégresse, il exultera, et celui qui engendre un sage s'en réjouira.
23:25	Ton père et ta mère se réjouiront, celle qui t'a enfanté exultera !
23:26	Mon fils, donne-moi ton cœur, et que tes yeux observent mes voies.
23:27	Car la femme débauchée est une fosse profonde, et l'étrangère un puits de détresse.
23:28	Aussi se tient-elle aux aguets pour une proie, et elle augmente parmi les humains le nombre des infidèles.
23:29	Pour qui le malheur ? Pour qui les hélas ? Pour qui les disputes ? Pour qui les bruits ? Pour qui les blessures sans cause ? Pour qui les yeux rouges ?
23:30	Pour ceux qui s'arrêtent auprès du vin, pour ceux qui vont chercher des vins mélangés.
23:31	Ne regarde pas le vin quand il se montre rouge, quand il donne sa couleur dans la coupe, et qu'il coule aisément.
23:32	Il mord par derrière comme un serpent, et il pique comme une vipère.
23:33	Tes yeux regarderont les femmes étrangères, et ton cœur dira des choses perverses.
23:34	Tu deviendras comme celui qui se couche au cœur de la mer, comme celui qui se couche au sommet d'un mât :
23:35	On m'a battu, mais je ne suis pas devenu malade ! On m’a battu, mais je ne l’ai pas su ! Quand me réveillerai-je ? J'irai en chercher encore !

## Chapitre 24

### La justice s'oppose à la méchanceté (suite et fin)

24:1	N'envie pas les hommes mauvais et ne désire pas être avec eux.
24:2	Car leur cœur médite la destruction et leurs lèvres parlent de malheur.
24:3	C'est par la sagesse qu'une maison se bâtit et c'est par l'intelligence qu'elle s'affermit.
24:4	Par la connaissance les chambres sont remplies de toutes les richesses précieuses et agréables.
24:5	Un homme fort et sage a de la force, un homme qui a de la connaissance affermit son pouvoir.
24:6	Car c'est avec de sages conseils qu'on fait la guerre, et le salut consiste dans le grand nombre des conseillers.
24:7	Il n'y a pas de sagesse qui ne soit trop haute pour le fou, il n'ouvrira pas sa bouche à la porte.
24:8	Celui qui projette de faire le mal, on l'appelle le maître des complots.
24:9	Le méchant dessein de la folie n'est que péché, et le moqueur est en abomination aux humains.
24:10	Si tu perds courage au jour de la détresse, ta force sera étroite.
24:11	Ne te retiens pas de délivrer ceux qu’on emmène vers la mort, ceux qui sont sur le point d'être assassinés.
24:12	Si tu dis : Voici, nous ne savions pas ! Celui qui pèse les cœurs ne le comprend-il pas ? Celui qui garde ton âme ne le sait-il pas ? Et ne rendra-t-il pas à l'être humain selon son œuvre ?
24:13	Mon fils, mange le miel, car il est bon. Le rayon de miel, car il est doux à ton palais.
24:14	De même, connais la sagesse pour ton âme : si tu la trouves, il y a une issue pour toi et ton espérance ne sera pas retranchée.
24:15	Méchant, n'épie pas le domicile du juste, et ne détruis pas le lieu où il se repose.
24:16	Car le juste tombe sept fois et se relève<!--Ps. 34:20 ; Job 5:19.-->, mais les méchants trébuchent dans le mal.
24:17	Si ton ennemi tombe, ne t'en réjouis pas ! S'il trébuche, que ton cœur n'exulte pas,
24:18	de peur que YHWH ne le voie, que cela ne lui déplaise et qu'il ne détourne de lui sa colère.
24:19	Ne t'irrite pas à cause de ceux qui font le mal, ne porte pas envie aux méchants,
24:20	car il n'y a pas de récompense pour le méchant, la lampe des méchants sera éteinte.
24:21	Mon fils, crains YHWH et le roi ! N'entreprends rien avec ceux qui s'altèrent,
24:22	car leur désastre s'élèvera soudainement, et qui connaît la ruine des deux ?
24:23	Ces choses aussi sont pour les sages : Il n'est pas bon d'avoir égard à l'apparence des personnes dans le jugement.
24:24	Celui qui dit au méchant : Tu es juste ! Les peuples le maudiront et les nations seront indignées contre lui.
24:25	Mais ceux qui le réprimandent seront agréables, sur eux viendra une heureuse bénédiction.
24:26	Il donne un baiser sur les lèvres, celui qui répond par des paroles justes.
24:27	Arrange ton travail au-dehors, prépare ton champ ! Après tu bâtiras ta maison.
24:28	Ne témoigne pas contre ton prochain sans cause ! Car voudrais-tu tromper de tes lèvres<!--Ep. 4:25.--> ?
24:29	Ne dis pas : Je le traiterai comme il m'a traité, je rendrai à cet homme selon ce qu'il m'a fait.
24:30	Je suis passé près du champ d'un homme paresseux, et près de la vigne d'un humain qui manque de cœur,
24:31	et voici, les chardons y croissaient partout, les orties en couvraient la surface et son mur de pierres était renversé.
24:32	En voyant cela, je l’ai mis dans mon cœur et j'ai reçu la correction de ce que j'ai vu.
24:33	Un peu de sommeil, un peu d'assoupissement, un peu croiser les mains pour dormir ! ...
24:34	Et ta pauvreté viendra, elle arrivera, et ton besoin comme un homme au bouclier.

## Chapitre 25

### Avertissements et conseils

25:1	Ceux-ci aussi sont des proverbes de Shelomoh<!--1 R. 5:10.-->, que les hommes d'Hizqiyah<!--Ézéchias.-->, roi de Yéhouda, ont recueillis.
25:2	La gloire d'Elohîm, c'est de cacher les choses, et la gloire des rois, c'est de sonder les choses.
25:3	Les cieux dans leur hauteur, la terre dans sa profondeur, et le cœur des rois, sont insondables.
25:4	Ôte les scories de l'argent, et il en sortira un vase pour l'orfèvre.
25:5	De même, ôte le méchant de devant le roi, et son trône sera affermi par la justice.
25:6	Ne te glorifie pas devant le roi, et ne te tiens pas à la place des grands.
25:7	Car il vaut mieux qu'on te dise : Monte ici ! que si l'on t'abaisse devant un prince que tes yeux voient<!--Lu. 14:8-11.-->.
25:8	Ne sors pas à la hâte pour contester, car que feras-tu à la fin, - quand ton prochain t’aura confondu ?
25:9	Plaide ta cause contre ton prochain, mais ne révèle pas le secret d'un autre.
25:10	De peur que celui qui l'entend ne te couvre de honte et que ta diffamation soit sans retour.
25:11	Des pommes d'or avec des images sculptées d'argent, telle est une parole dite à propos.
25:12	Quand on reprend le sage qui a l'oreille attentive, c'est comme une bague d'or ou comme un joyau d'or fin.
25:13	Le messager fidèle est pour ceux qui l'envoient comme la fraîcheur de la neige au temps de la moisson : il restaure l'âme de son seigneur.
25:14	Vent et nuages sans pluie, tel est l'homme qui se vante d'un don mensonger.
25:15	Le chef est fléchi par la patience, et la langue douce brise les os.
25:16	Si tu trouves du miel, manges-en ce qui te suffit, de peur qu'en étant rassasié, tu ne le vomisses.
25:17	Que ton pied soit rare<!--estimer, être prisé, avoir de la valeur, être précieux, coûteux, être apprécié.--> dans la maison de ton prochain, de peur qu'étant rassasié de toi, il ne te haïsse.
25:18	L'homme qui porte un faux témoignage contre son prochain est un marteau, une épée et une flèche aiguisée.
25:19	La confiance qu'on met en un traître au jour de la détresse est une dent cassée et un pied qui glisse.
25:20	Celui qui chante des chansons à un cœur triste est comme celui qui ôte sa robe dans un jour froid, et comme du vinaigre répandu sur le savon.
25:21	Si celui qui te hait a faim, donne-lui du pain à manger, et, s’il a soif, donne-lui de l’eau à boire<!--Mt. 5:39-44.-->.
25:22	Car tu amasseras des charbons ardents sur sa tête<!--Cette expression peut également être traduite par « ce sont des braises que tu enlèves de sa tête ». Cette expression est une métaphore qui tire son origine de la méthode consistant à fondre les métaux dans les anciens fours. Ces métaux étaient introduits dans un four ; ensuite, on mettait du charbon au-dessous et une couche épaisse au-dessus de leur partie supérieure. La chaleur étant augmentée, le métal fondait et se séparait des impuretés qu’il contenait. L’amas de charbons à sa partie supérieure amollissait et purifiait le minerai. Ainsi, témoigner de l’amour envers un ennemi peut l’aider à se séparer des impuretés. Ro. 12:20.-->, et YHWH te le rendra.
25:23	Le vent du nord engendre la pluie, et la langue calomnieuse les faces indignées.
25:24	Il vaut mieux habiter à l'angle d'un toit que de partager la demeure d'une femme querelleuse.
25:25	De l'eau fraîche pour une âme altérée et épuisée, telle est une bonne nouvelle venant d'une terre lointaine.
25:26	Le juste qui tremble devant le méchant est une fontaine foulée aux pieds et une source corrompue.
25:27	Il n'est pas bon de manger beaucoup de miel, ni de rechercher gloire sur gloire.
25:28	Une ville en brèche, sans murailles, tel est l'homme qui n'a pas autorité sur son esprit.

## Chapitre 26

### Avertissements et conseils (suite)

26:1	Comme la neige en été, et la pluie pendant la moisson, ainsi la gloire ne convient pas à un insensé.
26:2	Comme l'oiseau qui va çà et là, comme l’hirondelle qui s’envole, ainsi la malédiction sans cause<!--Job 2:3.--> n'atteint pas.
26:3	Le fouet est pour le cheval, le mors pour l'âne, et la verge pour le dos des insensés.
26:4	Ne réponds pas à l'insensé selon sa folie, de peur que tu ne lui ressembles toi-même.
26:5	Réponds à l'insensé selon sa folie, de peur qu’il ne devienne un sage à ses yeux.
26:6	Celui qui envoie des paroles par la main d'un insensé se coupe les pieds et boit la violence.
26:7	Les jambes du boiteux sont sans force : tel est un proverbe dans la bouche des insensés.
26:8	Celui qui donne gloire à un insensé, c'est comme s'il jetait un sachet de pierres précieuses dans un monceau de pierres.
26:9	Une épine montée dans la main d’un ivrogne, tel est un proverbe dans la bouche des insensés.
26:10	Les grands font trembler tout le monde : ils prennent à gage les insensés et les transgresseurs.
26:11	Comme le chien retourne à ce qu'il a vomi, l'insensé répète sa folie<!--2 Pi. 2:22.-->.
26:12	Si tu vois un homme qui est sage à ses propres yeux, il y a plus à espérer d'un insensé que de lui.
26:13	Le paresseux dit : Il y a un lion rugissant sur le chemin, il y a un lion dans les rues.
26:14	La porte tourne sur ses gonds, et le paresseux sur son lit.
26:15	Le paresseux plonge sa main dans le plat, et il trouve fatigant de la ramener à sa bouche.
26:16	Le paresseux est plus sage à ses yeux que sept qui répondent avec du jugement.
26:17	Celui qui, en passant, se met en colère pour une dispute qui ne le touche en rien est comme celui qui prend un chien par les oreilles.
26:18	Tel celui qui, faisant le fou, lance des flèches enflammées, des flèches et la mort,
26:19	tel est l'homme qui trompe son ami, et qui après cela dit : C'était pour rire !
26:20	Le feu s'éteint faute de bois, ainsi quand il n'y a plus de rapporteurs les querelles s'apaisent.
26:21	Du charbon sur le brasier et du bois sur le feu, ainsi est l'homme de disputes pour enflammer les querelles.
26:22	Les paroles du rapporteur sont comme des friandises, elles descendent jusqu'au fond des entrailles.
26:23	Les lèvres brûlantes et le cœur mauvais sont comme un vase de terre recouvert de scories d'argent.
26:24	Celui qui a de la haine se déguise par ses discours, mais au-dedans de lui il maintient la tromperie.
26:25	Lorsqu'il met de la grâce dans sa voix, ne le crois pas, car il y a sept abominations dans son cœur.
26:26	Sa haine peut se couvrir de dissimulation, son mal sera découvert dans l'assemblée.
26:27	Celui qui creuse la fosse y tombe, et la pierre retourne sur celui qui la roule<!--Ps. 7:16-17, 57:7 ; Ec. 10:8.-->.
26:28	La langue fausse a de la haine pour ceux qu'elle écrase et la bouche flatteuse fait trébucher.

## Chapitre 27

### Avertissements et conseils (suite)

27:1	Ne te vante pas du jour de demain, car tu ne sais pas ce qu'un jour enfantera<!--Ja. 4:13-15.-->.
27:2	Qu'un autre te vante, et non pas ta propre bouche, un étranger, et non pas tes lèvres.
27:3	La pierre est pesante, et le sable est lourd, mais l'irritation du fou est plus pesante que tous les deux.
27:4	Il y a de la cruauté dans le courroux et du débordement dans la colère, mais qui tiendra devant la jalousie ?
27:5	Une réprimande ouverte vaut mieux qu'un amour caché.
27:6	Les blessures faites par un ami sont fidèles, mais les baisers d'un ennemi sont à craindre<!--Il est question ici de Yéhouda Iskariote (Judas).-->.
27:7	L'âme rassasiée foule les rayons de miel, mais pour l'âme qui a faim, toute chose amère est douce.
27:8	Tel un oiseau qui s'écarte de son nid, tel est l'homme qui erre loin de son lieu.
27:9	L'huile et l'encens réjouissent le cœur, telle est la douceur d'un ami dont le conseil vient de l'âme.
27:10	Ne quitte pas ton ami ni l'ami de ton père, et n'entre pas dans la maison de ton frère au temps de ta détresse : un voisin proche vaut mieux qu'un frère éloigné.
27:11	Mon fils, sois sage et réjouis mon cœur, afin que je retourne la parole à celui qui m’insulte.
27:12	L'homme prudent voit le mal et se cache. Les stupides passent à travers et sont punis.
27:13	Prends son vêtement, car il a cautionné un étranger, et exige de lui un gage à cause de l’étrangère.
27:14	Celui qui bénit son ami à grande voix, se levant tôt le matin, cela lui est compté pour une malédiction.
27:15	Une gouttière continuelle au temps de la grosse pluie et une femme querelleuse sont semblables.
27:16	Celui qui la cache, cache le vent, et dans sa droite l’huile crie.
27:17	Le fer aiguise le fer, et l'homme aiguise les faces de son prochain.
27:18	Celui qui garde le figuier mangera de son fruit, et celui qui garde son maître sera honoré.
27:19	Comme sur des eaux les faces font faces, ainsi le cœur de l’être humain pour l’être humain.
27:20	Le shéol et l'abîme ne sont jamais rassasiés, de même les yeux des humains ne sont jamais rassasiés<!--Ec. 1:8 ; 2 Pi. 2:14.-->.
27:21	Comme le fourneau est pour éprouver l'argent, et le creuset pour l'or, ainsi est à l'homme la bouche qui le loue.
27:22	Si tu pilais le fou dans un mortier, au milieu des grains qu'on pile avec un pilon, sa folie ne se détournerait pas de lui.
27:23	Connaître, connais les faces de tes brebis, et fixe ton cœur sur tes troupeaux.
27:24	Car le trésor ne dure pas pour toujours, et la couronne n'est pas d'âges en âges.
27:25	L'herbe se découvre, la végétation se voit, et on recueille la verdure des montagnes.
27:26	Les agneaux sont pour te vêtir, et les boucs sont le prix d'un champ,
27:27	et le lait des chèvres suffit à ta nourriture et à la nourriture de ta maison, et pour la vie de tes servantes.

## Chapitre 28

### Avertissements et conseils (suite)

28:1	Le méchant prend la fuite sans qu'on le poursuive, mais les justes ont confiance comme un jeune lion.
28:2	Quand la rébellion règne dans une terre, ses chefs sont nombreux, mais, avec un être humain qui a du discernement et du savoir, la stabilité se prolonge.
28:3	Un homme fort et pauvre qui opprime les pauvres, c’est une pluie violente et plus de pain !
28:4	Ceux qui abandonnent la torah louent le méchant, mais ceux qui gardent la torah lui font la guerre.
28:5	Les hommes mauvais ne comprennent pas la justice, mais ceux qui cherchent YHWH comprennent tout.
28:6	Le pauvre qui marche dans son intégrité vaut mieux qu'un pervers aux voies tortueuses quoiqu'il soit riche.
28:7	Celui qui garde la torah est un fils prudent, mais celui qui nourrit les gaspilleurs fait honte à son père.
28:8	Celui qui multiplie sa richesse par l'intérêt et l'usure, l'amasse pour celui qui a pitié des pauvres<!--Ec. 2:26.-->.
28:9	Celui qui détourne son oreille pour ne pas écouter la torah, sa prière même est une abomination<!--La prière doit être faite selon la parole d'Elohîm, en conformité avec sa volonté. Le Seigneur n'exauce que ceux qui obéissent à sa Parole (Mt. 6:9-10 ; Jn. 9:31, 15:7 ; 1 Jn. 5:14-15).-->.
28:10	Celui qui égare les justes dans le mauvais chemin tombe dans la fosse qu'il a faite, mais ceux qui sont intègres héritent le bonheur.
28:11	L’homme riche est sage à ses yeux, mais le pauvre qui discerne le sondera.
28:12	Quand les justes se réjouissent, la splendeur est grande, mais quand les méchants sont élevés, l'être humain se déguise.
28:13	Celui qui cache ses transgressions ne prospère pas, mais celui qui les confesse et les délaisse, obtient miséricorde<!--Ec. 1:8 ; 2 Pi. 2:14.-->.
28:14	Heureux l'être humain qui est continuellement dans la crainte, mais celui qui endurcit son cœur tombera dans la calamité.
28:15	Le méchant qui domine sur un peuple pauvre est un lion rugissant et un ours assoiffé.
28:16	Un chef qui manque d'intelligence opprime beaucoup, celui qui hait le gain injuste prolonge ses jours.
28:17	Un être humain chargé du sang d'une âme fuira jusqu'à la fosse sans qu'on le retienne.
28:18	Celui qui marche dans l'intégrité sera sauvé, mais celui qui suit des voies tortueuses tombera une fois pour toutes.
28:19	Celui qui travaille son sol sera rassasié de pain, mais celui qui suit les fainéants sera rassasié de pauvreté.
28:20	L'homme fidèle abondera en bénédictions, mais celui qui se hâte de s'enrichir ne demeurera pas impuni.
28:21	Il n'est pas bon d'avoir égard aux faces, et pour un morceau de pain un homme fort se révolte.
28:22	L'homme qui a mauvais œil est pressé d'avoir des richesses ! Il ne sait pas que la pauvreté viendra sur lui.
28:23	Celui qui reprend un humain trouve ensuite plus de faveur que celui qui flatte de sa langue.
28:24	Celui qui pille son père ou sa mère, et qui dit que ce n'est pas une transgression, est compagnon de l'homme destructeur.
28:25	Celui qui a l'âme enflée excite les querelles, mais celui qui se confie en YHWH sera rassasié.
28:26	Celui qui se confie en son propre cœur est un insensé, mais celui qui marche sagement sera délivré.
28:27	Celui qui donne au pauvre ne sera pas dans le besoin, mais celui qui en détourne ses yeux abondera en malédictions.
28:28	Quand les méchants s'élèvent, les humains se cachent, mais quand ils périssent, les justes se multiplient.

## Chapitre 29

### Avertissements et conseils (suite et fin)

29:1	Un homme qui durcit son cou contre les réprimandes sera soudainement brisé sans qu'il y ait de guérison !
29:2	Quand les justes sont nombreux, le peuple se réjouit, mais quand le méchant domine, le peuple gémit.
29:3	L'homme qui aime la sagesse, réjouit son père, mais celui qui se plaît avec les femmes prostituées dissipe ses richesses<!--Lu. 15:11-32.-->.
29:4	Le roi maintient la terre par la justice, mais l'homme qui aime les offrandes la ruine.
29:5	L'homme fort qui flatte son prochain lui tend un piège sous ses pas.
29:6	Dans la transgression d'un homme méchant il y a un piège, mais le juste chante et se réjouit.
29:7	Le juste connaît la cause des pauvres, le méchant ne discerne pas la connaissance.
29:8	Les hommes moqueurs troublent la ville, mais les sages apaisent la colère.
29:9	Un homme sage qui entre en jugement avec un homme fou, qu’il s’agite ou qu’il rie, il n’aura pas de repos.
29:10	Les hommes de sang haïssent l'innocent, mais les justes cherchent son âme.
29:11	L'insensé fait sortir tout son esprit, mais le sage a du recul et le calme.
29:12	Tous les serviteurs d'un prince qui prête l'oreille à la parole mensongère sont méchants.
29:13	Le pauvre et l’homme d’oppressions se rencontrent, c'est YHWH qui donne la lumière aux yeux de tous les deux.
29:14	Le trône du roi qui rend justice selon la vérité aux pauvres, sera établi à perpétuité.
29:15	La verge et la réprimande donnent la sagesse, mais le jeune homme livré à lui-même fait honte à sa mère.
29:16	Quand les méchants se multiplient, la transgression se multiplie, mais les justes verront leur ruine.
29:17	Corrige ton fils, il te mettra en repos et il donnera du plaisir à ton âme.
29:18	Quand il n'y a pas de vision<!--Le manque de vision n'est bon pour personne. Elohîm donne une vision aux personnes qu'il a appelées. La vision peut être un rêve, une directive, une prophétie, etc. Il s'agit des objectifs à atteindre.-->, le peuple s'abandonne au désordre, mais heureux est celui qui garde la torah !
29:19	L'esclave ne se corrige pas par des paroles, car il comprendra mais ne répondra pas.
29:20	As-tu vu un homme pressé dans ses paroles ? Il y a plus à espérer d'un fou que de lui.
29:21	Le serviteur devient enfin fils de celui qui le traite avec délicatesse dès sa jeunesse.
29:22	L'homme coléreux excite les querelles, et l'homme furieux a beaucoup de transgressions.
29:23	L'orgueil de l'humain l'abaisse, mais celui qui est humble d'esprit obtient la gloire<!--Mt. 23:12 ; Lu. 14:11 ; 1 Pi. 5:5.-->.
29:24	Celui qui partage avec un voleur hait son âme : il entend bien la malédiction, mais il n'avoue pas.
29:25	La terreur de l’humain tend un piège, mais celui qui se confie en YHWH est inaccessiblement haut.
29:26	Beaucoup cherchent les faces de celui qui domine, mais c'est de YHWH que vient le jugement de tout homme.
29:27	L'homme injuste est en abomination aux justes, et celui dont la voie est droite est en abomination au méchant.

## Chapitre 30

### Proverbe d'Agour

30:1	Les paroles d'Agour, fils de Yaqeh : fardeau. La déclaration de cet homme fort à Ithiel. À Ithiel et Oucal.
30:2	Oui, je suis le plus abruti de tous les hommes, et je n’ai pas le discernement de l’humain.
30:3	Je n'ai pas appris la sagesse, et je ne connais pas la connaissance des saints.
30:4	Qui est celui qui est monté aux cieux et qui en est descendu<!--Jn. 3:13 ; Ro. 10:6-7.--> ? Qui est celui qui a recueilli le vent dans le creux de sa main, qui a lié les eaux dans son manteau, qui a dressé toutes les bornes de la Terre ? Quel est son nom, et quel est le nom de son fils, puisque tu le sais ?
30:5	Toute la parole d'Éloah est éprouvée. Il est un bouclier pour ceux qui ont leur refuge en lui<!--Ps. 18:31, 115:9-11.-->.
30:6	N'ajoute rien à ses paroles, de peur qu'il ne te reprenne et que tu ne sois convaincu de mensonge<!--De. 4:2 ; Ap. 22:18.-->.
30:7	Je te demande deux choses : Ne me les refuse pas avant que je meure !
30:8	Éloigne de moi la vanité et la parole de mensonge. Ne me donne ni pauvreté ni richesse, fournis-moi du pain qui m'est prescrit !
30:9	De peur qu'étant rassasié, je ne te renie et que je ne dise : Qui est YHWH ? De peur qu'étant appauvri, je ne vole et que je ne m’attaque au nom de mon Elohîm.
30:10	Ne calomnie pas un serviteur devant son maître, de peur qu’il ne te maudisse, et qu'il ne t'en arrive du mal.
30:11	Il existe une génération qui maudit son père et qui ne bénit pas sa mère.
30:12	Il existe une génération qui se croit pure et qui toutefois n'est pas lavée de ses excréments.
30:13	Il existe une génération dont les yeux sont hautains et les paupières élevées.
30:14	Il existe une génération dont les dents sont des épées et les mâchoires sont des couteaux pour dévorer les malheureux sur la Terre et les pauvres parmi les humains.
30:15	La sangsue a deux filles : Donne ! Donne ! Il y a trois choses qui ne se rassasient jamais, il y en a même quatre qui ne disent jamais : Assez !
30:16	Le shéol, la matrice stérile<!--restriction, coercition, contrainte, stérilité (de la matrice).-->, la terre qui n'est pas rassasiée d'eau et le feu qui ne dit jamais : Assez !
30:17	L’œil qui se moque d’un père et qui méprise l’obéissance envers une mère, les corbeaux du torrent le crèveront et les petits de l’aigle le mangeront.
30:18	Il y a trois choses qui sont trop merveilleuses pour moi, même quatre que je ne connais pas :
30:19	le chemin de l'aigle dans les cieux, le chemin du serpent sur un rocher, le chemin d'un navire au milieu de la mer, et le chemin de l'homme fort chez la vierge.
30:20	Tel est le chemin de la femme adultère : Elle mange et s'essuie la bouche, puis elle dit : Je n'ai rien fait de mal !
30:21	Sous trois choses tremble la Terre, et sous quatre qu’elle ne peut porter :
30:22	sous l’esclave quand il règne, et un insensé quand il est rassasié de pain,
30:23	sous la haïe quand elle est épousée, et la servante quand elle dépossède sa maîtresse.
30:24	Il y a quatre choses petites sur la Terre, qui toutefois sont sages et bien avisées :
30:25	Les fourmis, un peuple qui n'est pas fort, et qui néanmoins préparent leur nourriture pendant l'été,
30:26	les damans, un peuple qui n'est pas puissant, et qui néanmoins placent leurs maisons dans les rochers,
30:27	les sauterelles, qui n'ont pas de roi mais sortent toutes par divisions,
30:28	le lézard, que tu peux attraper avec tes mains et qui se trouve dans les palais des rois.
30:29	Il y a trois choses qui ont une belle allure, même quatre, qui ont une belle marche :
30:30	Le lion, qui est le plus fort parmi les animaux, et qui ne recule devant qui que ce soit,
30:31	le cheval<!--Peut-être un animal disparu, sens exact inconnu. Certains traduisent par « le coq aux reins solides ».--> qui a les reins ceints, ou le bouc, et le roi devant qui personne ne résiste.
30:32	Si tu as fait l’insensé en t’élevant, et si tu en as la pensée, mets la main sur ta bouche.
30:33	Car la pression du lait produit du beurre, la pression du nez produit du sang, et la pression de la colère produit des disputes.

## Chapitre 31

### Proverbe de Lemouel

31:1	Les paroles du roi Lemouel. Le fardeau par lequel sa mère l'instruisit.
31:2	Quoi, mon fils ! quoi, fils de mes entrailles ! quoi, fils de mes vœux !
31:3	Ne donne pas ta force aux femmes ni tes voies à celles qui détruisent les rois.
31:4	Ce n'est pas aux rois, Lemouel, ce n'est pas aux rois de boire du vin, ni aux princes des boissons fortes,
31:5	de peur qu'en buvant, ils n'oublient les décrets, et n'altèrent la cause de tous les fils de l'affliction.
31:6	Donnez des boissons fortes à celui qui périt, et du vin à celui qui a l'amertume dans le cœur,
31:7	qu'il boive et qu'il oublie sa pauvreté, qu'il ne se souvienne plus de sa peine ! 
31:8	Ouvre ta bouche pour le muet, pour la cause de tous les fils destinés à la destruction.
31:9	Ouvre ta bouche, juge avec justice, plaide la cause du pauvre et de l'indigent.

### La femme talentueuse

31:10	[Aleph.] Qui trouvera une femme talentueuse ? Son prix dépasse de loin celui des perles.
31:11	[Beth.] Le cœur de son mari a entièrement confiance en elle, ainsi il ne manque pas de butin.
31:12	[Guimel.] Elle lui fait du bien tous les jours de sa vie, et jamais du mal.
31:13	[Daleth.] Elle cherche de la laine et du lin, et elle travaille de bon cœur avec ses paumes.
31:14	[He.] Elle est comme les navires d'un marchand, elle amène sa nourriture de loin.
31:15	[Vav.] Elle se lève lorsqu'il est encore nuit, elle distribue la nourriture à sa maison, et elle donne à ses servantes leur portion.
31:16	[Zayin.] Elle pense à un champ et le prend. Elle plante la vigne du fruit de ses paumes.
31:17	[Heth.] Elle ceint ses reins de force, et affermit ses bras.
31:18	[Teth.] Elle goûte en effet le meilleur de son gain. Sa lampe ne s'éteint pas la nuit.
31:19	[Yod.] Elle étend les mains vers la quenouille, et ses paumes tiennent le fuseau.
31:20	[Kaf.] Elle déploie ses paumes pour le pauvre, elle étend les bras vers l'indigent.
31:21	[Lamed.] Elle ne craint pas la neige pour sa maison, car toute sa maison est vêtue d'écarlate.
31:22	[Mem.] Elle se fait des couvre-pieds, le fin lin et le pourpre sont ce dont elle s'habille.
31:23	[Noun.] Son mari est connu aux portes, il s'assied avec les anciens de la terre.
31:24	[Samech.] Elle fait des chemises et les vend, et elle livre des ceintures au marchand.
31:25	[Ayin.] Force et splendeur sont ses vêtements, et elle se rit du jour à venir.
31:26	[Pe.] Elle ouvre sa bouche avec sagesse, et la torah de la bonté est sur sa langue.
31:27	[Tsade.] Elle surveille la marche de sa maison, et ne mange pas le pain de la paresse.
31:28	[Qof.] Ses fils se lèvent pour la proclamer heureuse, son mari, pour la louer :
31:29	[Resh.] Beaucoup de filles agissent avec compétence, mais toi, tu es élevée au-dessus d'elles toutes !
31:30	[Shin.] La grâce est trompeuse, et la beauté vaine. La femme qui craint YHWH est celle qui sera louée.
31:31	[Tav.] Donnez-lui du fruit de ses mains, et que ses œuvres la louent aux portes.
