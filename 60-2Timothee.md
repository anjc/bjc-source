# 2 Timotheos (2 Timothée) (2 Ti.)

Signification : Qui adore ou honore Elohîm

Auteur : Paulos (Paul)

Thème : Le maintien de la vérité

Date de rédaction : Env. 67 ap. J.-C.

Cette lettre s'adresse à Timotheos (Timothée) dont le père était grec et la mère juive. Le jeune homme se convertit au Mashiah avec sa mère et sa grand-mère, dès le premier voyage missionnaire de Paulos, au cours duquel ce dernier passa à Lystre.

Paulos écrivit cette lettre pastorale alors qu'il était emprisonné à Rome, après avoir été arrêté dans une province orientale à Éphèse ou Troas.

Ses conditions de détention étant plus rudes que la première fois, Paulos est dubitatif quant à sa remise en liberté. Il demande donc à Timotheos, son fils dans la foi et fidèle compagnon d'œuvre, de le rejoindre à Rome afin de recevoir, semble-t-il, ses dernières volontés. Après avoir exposé à Timotheos les qualités et les devoirs d'un bon serviteur de l'Évangile, il l'encourage à lutter contre les faux docteurs et l'apostasie en prêchant la parole en toutes circonstances.

## Chapitre 1

1:1	Paulos, apôtre de Yéhoshoua Mashiah, par la volonté d'Elohîm, selon la promesse de la vie qui est en Mashiah Yéhoshoua,
1:2	à Timotheos, mon fils bien-aimé : grâce, miséricorde, shalôm, de la part d'Elohîm le Père et du Mashiah Yéhoshoua notre Seigneur !
1:3	Je rends grâce à Elohîm, que je sers depuis mes ancêtres, avec une conscience pure, faisant sans cesse mention de toi dans mes supplications nuit et jour,
1:4	désirant te voir, me souvenant de tes larmes, afin d'être rempli de joie.
1:5	Conservant en effet le souvenir de la foi sincère qui est en toi et qui a premièrement habité en Loïs, ta grand-mère, et en Eunike<!--« Bénie par la victoire ».-->, ta mère, et qui, j'en suis persuadé, est aussi en toi.
1:6	C'est à cause de cela que je te rappelle de ranimer le don de grâce d'Elohîm qui est en toi à travers l'imposition de mes mains.
1:7	Car Elohîm ne nous a pas donné un esprit de timidité<!--« Crainte », « frayeur », « lâcheté ».-->, mais de force, d'amour et d'auto-contrôle<!--« Une réprimande ou un appel à un esprit sain, à la modération et au contrôle de soi », « modération », « de maîtrise de soi ».-->.
1:8	N'aie donc pas honte du témoignage de notre Seigneur, ni de moi son prisonnier. Mais souffre avec moi pour l'Évangile, selon la force d'Elohîm,
1:9	qui nous a sauvés et nous a appelés par une sainte vocation, non selon nos œuvres, mais selon son propre dessein et selon la grâce qui nous a été accordée en Yéhoshoua Mashiah avant les temps éternels,
1:10	mais qui maintenant a été manifestée à travers l'apparition de notre Sauveur Yéhoshoua Mashiah, qui a en effet aboli<!--Vient du grec « katargeo » qui signifie « rendre vain », « inemployé », « inactif », « inopérant » ; « priver de force », « d'influence », « de pouvoir » ; « faire cesser », « amener à une fin », « annuler ». Ce verbe est au temps passé indéterminé (aoriste). L'aoriste est un temps de la conjugaison grecque correspondant approximativement au passé simple et au passé antérieur français. Dans 1 Co. 15:26, Paul utilise le même verbe au présent passif. Une phrase est à la voix passive lorsque le sujet de la phrase subit l'action au lieu de la faire. Ici c'est la mort qui est le sujet. C'est aussi elle qui a subi l'action. Yéhoshoua a vaincu la mort. Ap. 1:18--> la mort et qui a mis en lumière la vie et l'immortalité à travers l'Évangile,
1:11	pour lequel j'ai été établi prédicateur, apôtre et docteur des nations.
1:12	C'est pourquoi aussi je souffre ces choses, mais je n'en ai pas honte, car je connais celui en qui j'ai cru et je suis persuadé qu'il est puissant pour garder mon dépôt<!--Il est question ici de la connaissance correcte et de la pure doctrine de l'Évangile qui doit être fermement et fidèlement gardée, ainsi que consciencieusement délivrée aux autres.--> jusqu'à ce jour-là.
1:13	Retiens dans la foi et dans l'amour qui est en Yéhoshoua Mashiah, le modèle des saines paroles que tu as apprises de moi.
1:14	Garde le bon dépôt par le moyen de l'Esprit Saint qui habite en nous.
1:15	Tu sais ceci : que tous ceux qui sont en Asie se sont détournés de moi, parmi eux sont Phygèlos et Hermogène.
1:16	Que le Seigneur accorde la miséricorde à la maison d'Onésiphoros, parce qu’il m’a souvent rafraîchi et qu'il n'a pas eu honte de ma chaîne.
1:17	Au contraire, dès son arrivée à Rome, il m'a cherché très rapidement et il m'a trouvé.
1:18	Que le Seigneur lui donne de trouver miséricorde auprès du Seigneur en ce jour-là ! Et tu sais très bien comment il a servi à Éphèse.

## Chapitre 2

2:1	Toi donc, mon fils, sois fortifié dans la grâce qui est dans le Mashiah Yéhoshoua.
2:2	Et les choses que tu as entendues de moi à travers beaucoup de témoins, confie-les à des humains fidèles qui seront suffisants<!--2 Co. 3:5.--> pour les enseigner aussi à d'autres.
2:3	Toi donc, souffre avec moi comme un bon soldat de Yéhoshoua Mashiah.
2:4	Nul qui va à la guerre ne s'empêtre<!--2 Pi. 2:20.--> dans les affaires de la vie, afin de plaire à celui qui l'a enrôlé comme soldat.
2:5	Et de même, si quelqu'un lutte dans un jeu sportif, il n'est couronné que s'il a lutté légalement.
2:6	Il faut que le laboureur travaille d’abord avant de recueillir les fruits.
2:7	Considère ce que je dis, car le Seigneur te donne de l'intelligence en toutes choses.
2:8	Rappelle-toi Yéhoshoua Mashiah, de la postérité de David, réveillé d’entre les morts, selon mon Évangile,
2:9	pour lequel je souffre jusqu'à être dans les liens comme un malfaiteur. Mais la parole d'Elohîm n'est pas liée.
2:10	C'est à cause de cela que je supporte toutes choses à cause des élus, afin qu'eux aussi obtiennent le salut qui est en Yéhoshoua Mashiah, avec la gloire éternelle.
2:11	Cette parole est sûre : en effet, si nous mourons avec lui, nous vivrons aussi avec lui.
2:12	Si nous supportons bravement et calmement les mauvais traitements, nous régnerons aussi avec lui. Si nous le renions, il nous reniera aussi<!--Lu. 9:26.-->.
2:13	Si nous sommes infidèles, il demeure fidèle, car il ne peut se renier lui-même.
2:14	Rappelle ces choses aux autres, attestant devant le Seigneur de ne pas se battre pour des mots. Cela ne sert à rien, si ce n'est au renversement des auditeurs.
2:15	Efforce-toi de te présenter<!--Voir 2 Co. 11:2.--> approuvé<!--Le participe passé « approuvé » vient du grec « dokimos ». Du temps de Paulos (Paul), les systèmes bancaires actuels n'existaient pas, toute la monnaie était en métal. Pour obtenir les pièces de monnaie, le métal était fondu et versé dans des moules et après le démoulage, il était nécessaire d'enlever les bavures. Or de nombreuses personnes les grattaient pour récupérer le surplus de métal et même davantage, ce qui faussait le poids de la monnaie. Face à ce problème, de nombreuses lois furent promulguées à Athènes pour éradiquer la pratique du rognage des pièces en circulation. Il existait toutefois quelques changeurs intègres qui ne mettaient en circulation que des pièces au bon poids. On appelait ces personnes des « dokimos », ce qui signifie « éprouvés » ou « approuvés ».--> devant Elohîm, un ouvrier qui n'a pas à avoir honte, qui enseigne directement et correctement<!--couper droit, agir avec droiture, avancer sur de droits chemins, tenir une voie droite, équivaut à faire bien, rendre droit et lisse, manipuler bien droit, enseigner la vérité directement et correctement. Vient d'un composé de ὀρθός (orthos : droit, rectiligne Actes 4:12 ; Hé. 12:13) et de τομός (tomoteros : plus pointu, plus tranchant Hé. 4:12)--> la parole de la vérité.
2:16	Mais évite les discours vains, inutiles et profanes, car ceux qui les tiennent avanceront toujours plus dans l'impiété,
2:17	et leur parole, comme une gangrène, aura du pâturage<!--Les brebis du Seigneur trouveront un pâturage selon Jean 10:9. De même, la parole des faux prophètes aura des pâturages. Ces pâturages représentent les personnes qui tombent dans leur piège.-->. De ce nombre sont Hymenaïos et Philètos,
2:18	qui se sont écartés<!--Vient d'un mot grec qui signifie « dévier », « s'écarter de », « manquer le but ». À l'époque des apôtres, il y avait plusieurs faux frères qui semaient la zizanie au milieu des enfants d'Elohîm. Parmi eux étaient Alexandros, le forgeron (1 Ti. 1:18-20), Hymenaïos (1 Ti. 1:18-20), Philètos (2 Ti. 2:16-18), les judaïsants (Ac. 15:1-29), Diotrephes (3 Jn. 1:9-11). Les faux frères sont des séducteurs.--> de la vérité, disant que la résurrection est déjà arrivée, et qui renversent<!--Voir Tit. 1:11.--> la foi de quelques-uns.
2:19	Néanmoins, le solide fondement d'Elohîm tient debout, ayant ce sceau : Le Seigneur connaît ceux qui sont à lui<!--Le Seigneur connaît ses brebis. Voir No. 16:5 ; Jn. 10:14.--> et : Quiconque invoque le nom du Seigneur, qu'il s'éloigne de l'injustice.
2:20	Or dans une grande maison, il n'y a pas seulement des vases d'or et d'argent, mais il y en a aussi de bois et de terre. Les uns sont en effet des vases d'honneur, mais les autres sont d'un usage vil.
2:21	Si donc quelqu'un se nettoie complètement de ceux-ci, il sera un vase d'honneur, sanctifié et utile au Seigneur, et préparé pour toute bonne œuvre.
2:22	Mais fuis les désirs de la jeunesse et cours après la justice, la foi, l'amour et la paix avec ceux qui invoquent le Seigneur à partir d’un cœur pur.
2:23	Mais refuse les débats insensés<!--Questions folles. Il est question ici de disputes, débats, discussions ou questions oiseuses. Voir Tit. 3:9.--> et qui sont sans instruction, sachant qu'ils engendrent des querelles.
2:24	Or il n'est pas juste et correct qu'un esclave du Seigneur ait des querelles, mais qu'il soit doux envers tous, capable d'enseigner, patient aux fautes,
2:25	corrigeant avec douceur ceux qui ont un sentiment contraire : peut-être Elohîm leur donnera-t-il la repentance pour la connaissance précise et correcte de la vérité,
2:26	et qu'ils retournent à la sobriété, hors du piège du diable, ayant été capturés<!--« Prendre vivant », « attraper », « capturer », « s'emparer ».--> par lui pour sa volonté.

## Chapitre 3

3:1	Mais sache aussi ceci, que dans les derniers jours<!--Les derniers jours. Voir Ge. 49:1.--> seront présents des temps difficiles à supporter.
3:2	Car les humains seront amoureux d'eux-mêmes<!--« Trop absorbé par son propre intérêt », « égoïste ».-->, aimant l'argent, fanfarons, orgueilleux<!--« Se montrer au-dessus des autres », « surpasser », « se mettre en évidence, avec une trop haute estime de ses propres moyens ou de ses mérites », « mépriser les autres ou même les traiter avec mépris, d'une façon hautaine ».-->, blasphémateurs, rebelles à leurs parents, ingrats, sans religion,
3:3	sans affection naturelle, sans loyauté, calomniateurs<!--Vient du grec « diabolos » qui peut être aussi traduit par « diable ». Voir Mt. 4:1.-->, sans auto-contrôle, cruels, opposés à la bonté et aux hommes bons,
3:4	traîtres, téméraires, enflés d'orgueil, amis des voluptés plutôt qu'amis d'Elohîm<!--Le mot grec « philotheos » du préfixe « philos » qui signifie « amis, être lié d'amitié avec quelqu'un » (Mt. 11:19 ; Lu. 7:6 ; Jn. 15:13-15) et de « theos » généralement traduit par « dieu » (traduit par « elohîm » dans la présente Bible).-->,
3:5	ayant l'apparence<!--Le mot « apparence » vient du grec « morphosis » et du latin « forma » qui donnent « forme » en français. Il est question du formalisme, de l'attachement excessif aux règles, aux rites, aux coutumes et aux traditions. Dans l'assemblée de Laodicée, l'accent est plutôt mis sur les règles à observer et les apparences que sur la vie spirituelle et intérieure. Les manifestations extérieures du formalisme sont : les lieux « sacrés » pour adorer (temples, cathédrales, pèlerinages, etc.) ; l'observation des jours sacrés (dimanche et shabbat) ; les rituels censés permettre au croyant d'expérimenter Elohîm et de rentrer dans une vie bénie (circoncision, ordination, bénédiction nuptiale, paiement de la dîme, présentation des enfants à Elohîm par le pasteur, etc.) ; une manière spéciale de s'habiller (toge, soutane, collet clérical, kippa, voile, costume/cravate) ; un régime alimentaire spécial, etc. Voir Mt. 6:1-8.--> de la piété, mais ayant renié sa puissance. Éloigne-toi aussi de ceux-là.
3:6	Car d’entre eux sont issus ceux qui se glissent dans les maisons et qui emmènent captives de petites femmes, chargées de péchés et guidées par toutes sortes de désirs,
3:7	apprenant toujours et ne pouvant jamais venir<!--Voir Jn. 6:44-45 et 65 ; 1 Ti. 2:4.--> à la connaissance précise et correcte de la vérité.
3:8	Mais comme Jannès et Jambrès s'opposèrent à Moshé, ceux-ci de même s'opposent à la vérité, humains à l'esprit corrompu et réprouvés<!--1 Co. 9:27.--> en ce qui concerne la foi.
3:9	Mais ils ne feront pas de plus grands progrès, car leur folie sera manifestée à tous, comme le fut aussi celle de ces deux-là.
3:10	Mais toi, tu as suivi de près mon enseignement, la conduite, le dessein, la foi, la patience, l'amour, la persévérance,
3:11	les persécutions, les souffrances, celles qui me sont arrivées à Antioche, à Icone et à Lystre<!--Ac. 14.-->. Persécutions que j'ai supportées, et le Seigneur m'a délivré hors de toutes.
3:12	Mais tous ceux qui veulent vivre pieusement dans le Mashiah Yéhoshoua seront aussi persécutés.
3:13	Mais les humains méchants et imposteurs<!--un gémisseur, un pleureur, un hurleur, un jongleur, enchanteur (parce que des incantations étaient poussées dans un genre de hurlement), un trompeur, un charlatan.--> iront en empirant, égarant les autres et en s'égarant eux-mêmes.
3:14	Mais toi, demeure dans les choses que tu as apprises et dont tu as été assuré, sachant de qui tu les as apprises,
3:15	et que, dès l’enfance<!--Vient du grec « brephos » qui signifie « un enfant à naître », « un embryon », « un fœtus », « un enfant nouveau-né », « un nourrisson », « un bébé ».-->, tu connais les Lettres<!--Jn. 7:15.--> Sacrées, qui peuvent te rendre sage pour le salut par le moyen de la foi en Mashiah Yéhoshoua.
3:16	Toute Écriture est inspirée d'Elohîm et utile pour la doctrine, pour la conviction<!--Hé. 11:1.-->, pour la correction, pour l’éducation<!--Toute l'éducation des enfants (qui est relative à la culture de l'esprit et de la moralité, et qui utilise dans ce but tantôt des ordres et des avertissements, tantôt des réprimandes, des corrections et des punitions). Elle inclut également l'exercice et le soin du corps.--> dans la justice,
3:17	afin que l'humain d'Elohîm soit complet, accompli pour toute bonne œuvre.

## Chapitre 4

4:1	Je témoigne donc en présence d'Elohîm et du Seigneur Yéhoshoua Mashiah, qui est sur le point de juger les vivants et les morts lors de son apparition et de son règne,
4:2	prêche la parole, sois présent<!--« Se tenir », « être prêt », « survenir ». Le mot grec « ephistemi » est utilisé pour des personnes survenant soudainement.--> quand l'occasion se présente ou hors saison. Réprimande d'une manière tranchante, censure, exhorte avec une entière patience et doctrine.
4:3	Car il y aura un temps où ils ne supporteront pas la saine doctrine, mais aimant qu'on leur chatouille les oreilles, ils accumuleront en piles des docteurs selon leurs propres désirs<!--Beaucoup refusent la saine doctrine et acceptent un évangile basé sur les biens matériels.-->.
4:4	Et ils détourneront vraiment l'oreille de la vérité et se tourneront vers les fables.
4:5	Mais toi, veille en toutes choses, endure les souffrances, fais l'œuvre d'un évangéliste, assure pleinement ton ministère.
4:6	Car pour moi, je suis déjà versé comme une libation et le temps de mon départ<!--un relâchement (de choses liées, tissées, attachées), une dissolution (en parties séparées), métaphore qui vient de l'idée de relâcher les amarres avant de mettre la voile.--> est survenu<!--Terme grec utilisé pour des personnes survenant soudainement ou un mal survenant sur quelqu'un. Voir 2 Ti. 4:2.-->.
4:7	J'ai combattu le bon combat, j'ai terminé la course, j'ai gardé la foi.
4:8	Désormais, m'est réservée la couronne de justice. Le Seigneur, le juste Juge, me la remettra en ce jour-là, et non seulement à moi, mais aussi à tous ceux qui auront aimé son apparition.
4:9	Hâte-toi de venir vers moi au plus tôt.
4:10	Car Démas m'a abandonné, ayant aimé le présent âge. Il est parti pour Thessalonique. Kreskes est allé en Galatie et Titos en Dalmatie.
4:11	Loukas seul est avec moi. Prends Markos et amène-le avec toi, car il m’est utile pour le service.
4:12	Et j'ai envoyé Tuchikos à Éphèse.
4:13	En venant apporte le manteau de voyage que j'ai laissé à Troas chez Karpos, et les livres, surtout les parchemins.
4:14	Alexandros le forgeron m'a fait beaucoup de mal. Le Seigneur lui rendra selon ses œuvres.
4:15	Garde-toi aussi de lui, car il s'est fortement opposé à nos paroles.
4:16	Personne n'est venu à mon aide dans ma première plaidoirie, mais tous m'ont abandonné. Qu'il ne leur en soit pas tenu compte !
4:17	Mais le Seigneur s'est tenu près de moi et m'a fortifié, afin que par mon moyen la prédication soit pleinement assurée et que toutes les nations l'entendent. Et j'ai été délivré de la gueule du lion.
4:18	Le Seigneur me délivrera aussi de toute mauvaise œuvre et me sauvera dans son Royaume céleste. À lui la gloire pour les âges des âges ! Amen !
4:19	Salue Priska<!--Priscilla.--> et Akulas, et la famille d'Onésiphoros.
4:20	Erastos est resté à Corinthe, et j'ai laissé Trophimos malade à Milet.
4:21	Hâte-toi de venir avant l'hiver. Eubulus et Poudes, et Linos, et Klaudia, et tous les frères te saluent.
4:22	Que le Seigneur Yéhoshoua Mashiah soit avec ton esprit ! Que la grâce soit avec vous ! Amen !
