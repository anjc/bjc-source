# Yéhouda (Jude) (Jud.)

Signification : Qu'il (YHWH) soit loué

Auteur : Yéhouda (Jude)

(Gr. : Ioudas)

Thème : Le combat de la foi

Date de rédaction : Env. 68 ap. J.-C.

Cette lettre, écrite par Yéhouda, l'un des frères de Yéhoshoua ha Mashiah (Jésus le Christ) homme, était destinée aux assemblées d'Asie Mineure. Il les avertit contre les faux docteurs et les exhorte à se confier en Yéhoshoua pour leur salut éternel.

## Chapitre 1

1:1	Yéhouda, esclave de Yéhoshoua Mashiah et frère de Yaacov, aux appelés, sanctifiés en Elohîm le Père et gardés par Yéhoshoua Mashiah :
1:2	que la miséricorde, le shalôm et l'amour vous soient multipliés !
1:3	Bien-aimés, je le fais en toute hâte en vous écrivant au sujet de notre salut commun, je me suis trouvé dans la nécessité de vous écrire afin de vous exhorter à combattre pour la foi qui a été donnée une fois pour toutes aux saints.
1:4	Car certains humains se sont glissés furtivement, qui depuis longtemps ont été dépeints d’avance pour ce jugement : des impies, qui changent la grâce de notre Elohîm en luxure sans bride, et qui renient le seul Maître<!--Vient du grec « despotes ». Voir Lu. 2:29 ; Ac. 4:24 ; 2 Pi. 2:1 ; Ap. 6:10. Yéhoshoua (Jésus) est le Seul Maître.--> Yéhoshoua Mashiah, notre Elohîm et Seigneur.
1:5	Mais je veux vous rappeler, à vous qui l’avez su une fois, que le Seigneur, ayant sauvé le peuple de la terre d'Égypte, détruisit ensuite ceux qui n'avaient pas cru.
1:6	Et les anges qui n'avaient pas gardé leur origine mais qui avaient quitté leur propre demeure, il les a gardés sous l'obscurité, dans des liens éternels<!--Voir 2 Pi. 2:4.-->, pour le jugement du grand jour.
1:7	Comme Sodome et Gomorrhe, et les villes d’alentour qui, de la même manière que ceux-ci, s’étaient livrées à la prostitution et étaient allées après une autre chair, sont désignées comme exemple et subissent le châtiment du feu éternel.
1:8	De la même façon cependant, ceux-ci aussi, plongés dans leurs rêves, souillent en effet la chair, méprisent la seigneurie et blasphèment contre les gloires.
1:9	Or l'archange Miyka'el, lorsqu'il contestait avec le diable et lui disputait le corps de Moshé, n'osa pas prononcer contre lui un jugement blasphématoire, mais il dit : Que le Seigneur te réprimande d'une manière tranchante !
1:10	Mais ceux-là blasphèment en effet contre tout ce qu'ils ne connaissent pas, et ils se corrompent dans tout ce qu'ils savent naturellement, comme les bêtes dépourvues de raison.
1:11	Malheur à eux ! Parce qu'ils ont suivi la voie de Qayin<!--Caïn.-->, et qu'ils se sont jetés dans l'égarement de Balaam pour une récompense, et qu'ils ont péri par la rébellion de Koré<!--No. 16:1-35 ; 2 Pi. 2:15 ; Ap. 2:14.-->.
1:12	Ce sont des écueils dans vos agapes, faisant des festins somptueux avec vous sans crainte, se nourrissant eux-mêmes. Ce sont des nuées sans eau, emportées par des vents çà et là, des arbres de fin d'automne sans fruits, deux fois morts, déracinés,
1:13	des grosses vagues sauvages de la mer, rejetant l'écume de leurs propres hontes<!--2 Co. 4:2 ; Ap. 3:18.-->, des étoiles errantes, à qui l'obscurité de la ténèbre est réservée pour l’éternité.
1:14	Mais c'est aussi pour eux que Hanowk<!--Hénoc.-->, le septième après Adam, a prophétisé, en disant : Voici, le Seigneur est venu avec la multitude innombrable de ses saints<!--Mt. 25:31 ; Mc. 8:38 ; Lu. 9:26 ; 1 Th. 3:13 ; 2 Th. 1:10.-->,
1:15	pour exercer un jugement<!--Voir Ps. 96 et 98.--> contre tous et pour convaincre<!--Le mot grec « exelegcho » signifie aussi « prouver qu'une chose est fausse, condamner ».--> tous les impies d'entre eux, de toutes leurs actions d'impiété qu'ils ont commises d'une manière impie, et de toutes les paroles injurieuses que les pécheurs impies ont proférées contre lui.
1:16	Ce sont eux qui murmurent continuellement, qui se plaignent de leur sort, qui marchent selon leurs convoitises, dont la bouche prononce des propos enflés, et qui admirent les personnes pour l'amour du profit.
1:17	Mais vous, bien-aimés, souvenez-vous des paroles qui ont été dites auparavant par les apôtres de notre Seigneur Yéhoshoua Mashiah.
1:18	Qui vous disaient que dans le dernier temps, il y aura des moqueurs, marchant selon leurs convoitises impies.
1:19	Ce sont ceux qui se séparent, des animaux qui n’ont pas l'Esprit.
1:20	Mais vous, bien-aimés, vous édifiant vous-mêmes sur votre très sainte foi, priant par l'Esprit Saint,
1:21	gardez-vous dans l'amour d'Elohîm, attendant la miséricorde de notre Seigneur Yéhoshoua Mashiah pour la vie éternelle.
1:22	Et ayez vraiment pitié de ceux qui doutent<!--Vient du grec « diakrino » qui signifie aussi « faire une distinction, s'opposer, lutter, être en désaccord avec quelqu'un, hésiter, douter, apprendre par discernement ».--> ;
1:23	mais les autres, sauvez-les avec crainte en les arrachant hors du feu, en haïssant même la robe souillée<!--Les vêtements représentent la justice (Es. 61:10 ; Ap. 19:8). La robe souillée par la chair représente la justice humaine (Es. 64:5 ; La. 1:9 ; Za. 3:1-9). Voir aussi Ec. 9:8. Les œuvres de la chair décrites en Ga. 5:19-21 souillent les humains.--> par la chair.
1:24	Or, à celui qui peut vous garder de trébucher et vous placer sans défaut en présence de sa gloire avec allégresse,
1:25	à Elohîm, seul sage, notre Sauveur, soient gloire et majesté, force souveraine et autorité, et maintenant et pour tous les âges ! Amen !
