# 1 Thessaloniciens (1 Th.)

Auteur : Paulos (Paul)

Thème : Le retour du Mashiah (Christ)

Date de rédaction : Env. 51 ap. J.-C.

Autrefois appelée Therme ou Therma, qui signifie « source chaude », Thessalonique reçut son nouveau nom de Cassandre, en l'honneur de sa femme Thessalonike, qui était aussi la sœur d'Alexandre le Grand (356 – 323 av. J.-C.), à qui il succéda. 

Cette ville est située au nord de la Grèce, sur la côte de la Mer Égée. Du temps de Paulos (Paul), la Grèce était divisée en deux parties. Dans la région du nord, la Macédoine, se trouvaient les villes de Philippes, Thessalonique et Bérée. Quant à la région du sud, l'Achaïe, elle comportait les villes d'Athènes et de Corinthe.

En ce temps-là, Thessalonique comptait environ 200 000 habitants (grecs, romains et juifs) et jouissait d'une importante fréquentation puisqu'elle figurait parmi les trois ports principaux de la Méditerranée et se situait sur l'une des plus grandes routes commerciales : La Voie Égnatienne reliant Rome à Byzance.

Sur le plan religieux, les habitants étaient polythéistes et pratiquaient une variété de cultes, dont le culte impérial. Durant trois semaines, Paulos enseigna dans une synagogue à Thessalonique et constitua un groupe de croyants composé de Juifs, des nations, de pauvres et de plusieurs femmes de la haute société. Toutefois, une violente persécution l'obligea à quitter promptement la ville, laissant la communauté nouvellement formée vulnérable et fragile.

La première lettre adressée par Paulos aux Thessaloniciens avait pour but d'affermir ces derniers dans les vérités fondamentales qui leur avaient été enseignées, de les exhorter à vivre une vie de sainteté pour être agréables à Elohîm, de les éclairer quant au devenir des défunts et de les assurer du retour certain du Seigneur.

## Chapitre 1

1:1	Paulos, et Silvanos, et Timotheos, à l'assemblée des Thessaloniciens, en Elohîm le Père et Seigneur Yéhoshoua Mashiah : à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:2	Nous rendons toujours grâces à Elohîm au sujet de vous tous, faisant mention de vous dans nos prières,
1:3	en nous rappelant sans cesse l'œuvre de votre foi, le travail de votre amour, et la persévérance de l'espérance en notre Seigneur Yéhoshoua Mashiah devant notre Elohîm et Père,
1:4	sachant, frères aimés d'Elohîm, votre élection.
1:5	Parce que notre Évangile ne vous est pas arrivé en parole seulement, mais aussi avec puissance, avec l'Esprit Saint et avec une grande plénitude d'assurance. Vous savez en effet que c'est ainsi que nous sommes venus parmi vous à cause de vous.
1:6	Et vous êtes devenus nos imitateurs et ceux du Seigneur, ayant reçu la parole avec la joie du Saint-Esprit au milieu d'une grande tribulation.
1:7	De sorte que vous êtes devenus des modèles pour tous ceux qui croient dans la Macédoine<!--La Macédoine était la terre natale d'Alexandre le Grand. Elle fut conquise par les Romains et devint une province romaine, dont la capitale était Thessalonique.--> et dans l'Achaïe<!--L'Achaïe était une province romaine placée sous l'autorité d'un proconsul résidant dans la capitale qui était Corinthe (2 Co. 1:1).-->.
1:8	Car la parole du Seigneur a retenti de chez vous, non seulement dans la Macédoine et dans l'Achaïe, mais votre foi envers Elohîm est sortie aussi en tout lieu, de sorte que nous n'avons pas besoin d'en parler.
1:9	Car eux-mêmes racontent à notre sujet quelle entrée nous avons eue auprès de vous et comment vous vous êtes convertis à Elohîm, en vous séparant des idoles, pour servir l'Elohîm vivant et véritable,
1:10	et pour attendre des cieux son Fils, qu'il a réveillé des morts, Yéhoshoua, qui nous délivre de la colère qui vient<!--La colère à venir. Voir les sept coupes de la colère d'Elohîm (Ap. 6:16 ; 15:5-8, 16:1-21).-->.

## Chapitre 2

2:1	Car vous-mêmes, frères, vous savez que notre entrée au milieu de vous n'a pas été vaine.
2:2	Mais après avoir souffert et subi des outrages à Philippes<!--Philippes était une ville de Macédoine située en Thrace, près de la côte nord de la Mer Égée. Voir Ac. 16:12-40 et la lettre de Paulos (Paul) aux Philippiens.-->, comme vous le savez, nous avons pris de l'assurance en notre Elohîm, pour vous annoncer l'Évangile d'Elohîm au milieu d'un grand combat.
2:3	Car notre exhortation n’est pas issue de l’égarement, ni issue de l'impureté, ni dans une tromperie.
2:4	Mais comme nous avons été éprouvés<!--Voir 1 Ti. 3:10.--> par Elohîm pour que l'Évangile nous fût confié, ainsi nous parlons non comme pour plaire aux humains, mais à Elohîm qui éprouve nos cœurs.
2:5	Car nous ne sommes jamais arrivés avec une parole flatteuse, comme vous le savez, ni avec la cupidité pour mobile<!--Un prétexte (une raison alléguée, une cause prétendue).-->, Elohîm en est témoin.
2:6	Nous ne cherchons pas une gloire issue des humains, ni de vous, ni des autres, quoique nous pouvons être un fardeau comme apôtres du Mashiah,
2:7	mais nous avons été doux au milieu de vous, de même qu'une nourrice chérit d'un tendre amour ses propres enfants.
2:8	Ainsi, dans notre affection pour vous, nous étions prêts à vous donner, non seulement l'Évangile d'Elohîm, mais encore notre propre vie, tant vous nous étiez devenus chers.
2:9	Car vous vous rappelez, frères, notre peine et notre travail dur et difficile. Car c'est en travaillant nuit et jour, pour n'être à la charge d'aucun d'entre vous, que nous vous avons prêché l'Évangile d'Elohîm.
2:10	Vous êtes témoins et Elohîm aussi, combien notre conduite envers vous qui croyez a été sainte, juste, et irréprochable.
2:11	Ainsi que vous le savez, exhortant, consolant et implorant chacun de vous, comme un père ses enfants,
2:12	de marcher d'une manière digne d'Elohîm, qui vous appelle à son Royaume et à sa gloire.
2:13	C'est aussi la raison pour laquelle nous rendons sans cesse grâces à Elohîm de ce que, recevant la parole d’Elohîm que nous vous avons fait entendre, vous l'avez reçue non comme la parole des humains, mais comme ce qu'elle est vraiment : la parole d'Elohîm qui agit puissamment aussi en vous qui croyez.
2:14	Car vous, frères, vous êtes devenus les imitateurs des assemblées d'Elohîm qui, dans la Judée, sont dans le Mashiah Yéhoshoua, parce que vous aussi, vous avez souffert de la part de ceux de votre propre nation les mêmes choses qu'elles ont souffertes de la part des Juifs,
2:15	qui ont même tué le Seigneur Yéhoshoua et leurs propres prophètes, qui vous ont persécutés, qui ne plaisent pas à Elohîm et qui sont opposés à tous les humains,
2:16	nous empêchant de parler aux nations pour qu'elles soient sauvées et mettent ainsi en tout temps le comble à leur péché. Mais à la fin la colère est venue sur eux.
2:17	Mais pour nous, frères, après avoir été quelque temps séparés de vous de face et non de cœur, nous avons eu d'autant plus d'ardeur et d'empressement de voir votre face.
2:18	C’est pourquoi nous avons voulu venir chez vous, moi, Paulos, en effet, une et même deux fois, mais Satan nous en a empêchés.
2:19	Car qui est notre espérance, ou joie, ou couronne de gloire ? N'est-ce pas vous aussi, en face de notre Seigneur Yéhoshoua Mashiah, en sa parousie ?
2:20	Car vous êtes notre gloire et la joie.

## Chapitre 3

3:1	C'est pourquoi, ne pouvant attendre davantage, nous avons trouvé bon de rester seuls à Athènes.
3:2	Et nous avons envoyé Timotheos, notre frère, serviteur d'Elohîm, et notre compagnon d'œuvre dans l'Évangile du Mashiah, pour vous affermir et vous exhorter au sujet de votre foi,
3:3	afin que personne ne soit agité<!--agiter la queue comme un chien.--> par ces tribulations, puisque vous savez vous-mêmes que nous sommes destinés à ces choses.
3:4	Car aussi, lorsque nous étions avec vous, nous vous prédisions que nous avons à subir des oppressions, comme cela est aussi arrivé, et vous le savez.
3:5	C'est pour cela que, moi aussi, n'y tenant plus, j'ai envoyé pour connaître votre foi, de peur que le Tentateur ne vous ait tentés et que notre travail ne soit devenu inutile.
3:6	Mais Timotheos, récemment venu de chez vous vers nous, nous a apporté la bonne nouvelle de votre foi et de votre amour et du bon souvenir que vous avez toujours de nous, désirant nous voir comme nous aussi par rapport à vous.
3:7	À cause de cela, frères, nous avons été consolés à votre sujet par le moyen de votre foi, dans toute notre tribulation et notre calamité.
3:8	Parce que maintenant nous vivons, si vous demeurez fermes dans le Seigneur.
3:9	Car quelle action de grâce nous pouvons rendre à Elohîm à votre sujet, pour toute la joie que nous éprouvons devant notre Elohîm, à cause de vous.
3:10	Priant nuit et jour avec beaucoup d’insistance pour voir votre visage et compléter<!--Compléter : du grec « katartizo » qui signifie « redresser », « ajuster », « compléter », « raccommoder » (ce qui a été abîmé), « réparer ». Ce verbe est également utilisé dans Mt. 4:21 lorsque Yaacov et Yohanan (Jacques et Jean) réparaient leurs filets. Le terme « katartismos » traduit par « perfectionnement » dans Ep. 4:11 vient de ce verbe. Ainsi, l'un des rôles de ces services est le perfectionnement des saints et non leur destruction.--> ce qui manque à votre foi !
3:11	Mais qu'Elohîm lui-même, notre Père, et notre Seigneur Yéhoshoua Mashiah, dirige<!--On constate que le verbe « diriger » est conjugué au singulier, y compris dans le texte original grec, ce qui atteste l'unité entre le Père et le Fils. Voir 2 Th. 2:16-17.--> notre chemin vers vous !
3:12	Et que le Seigneur vous fasse croître et abonder en amour les uns envers les autres et envers tous, comme il en est de nous envers vous !
3:13	Pour affermir vos cœurs pour qu’ils soient irréprochables en sainteté, devant notre Elohîm et Père, en la parousie de notre Seigneur Yéhoshoua Mashiah avec tous ses saints<!--Yéhoshoua ha Mashiah est YHWH (Es. 34:5, 40:10-11, 62:11-12 ; Za. 14:5 ; Jud. 14-15.).-->.

## Chapitre 4

4:1	Au reste donc, frères, nous vous le demandons et nous vous y exhortons dans le Seigneur Yéhoshoua, que, comme vous avez appris de nous de quelle manière il vous faut marcher et plaire à Elohîm, vous y abondiez de plus en plus.
4:2	Car vous savez quels commandements nous vous avons donnés à travers le Seigneur Yéhoshoua.
4:3	Car c’est ici la volonté d'Elohîm : votre sanctification<!--La sanctification personnelle (1 Pi. 1:15-18 ; Hé. 12:14 ; Ap. 22:11). Chaque chrétien doit fournir un effort, en se servant quotidiennement de la parole d'Elohîm et de la prière, pour se maintenir dans la sanctification. Cela implique la séparation d'avec le mal et des mauvaises compagnies (2 Co. 6:14-18). La sanctification se développe au prix de nombreuses souffrances et de multiples sacrifices (Ro. 12:1-3).-->. Que vous vous absteniez de relation sexuelle illicite,
4:4	que chacun de vous sache posséder son propre vase<!--« Vase » était une métaphore grecque commune pour « le corps » car les Grecs pensaient que l'âme vivait temporairement dans les corps.--> dans la sanctification et dans l'honneur,
4:5	non pas dans la passion<!--Voir Ro. 1:26.--> du désir, comme les nations qui ne connaissent pas Elohîm.
4:6	Ne pas frauder<!--« Marcher sur, au-dessus de, au-delà de », « outrepasser les limites convenables, violer ».--> et profiter<!--« Avoir plus, ou une plus grande part ou part », « avoir un avantage sur, gagner ou profiter d'un autre ».--> de son frère en affaire<!--Une transaction commerciale.-->, parce que le Seigneur punit toutes ces choses, comme nous vous l'avons dit et attesté.
4:7	Car Elohîm ne nous a pas appelés à l'impureté, mais dans la sanctification.
4:8	C'est pourquoi celui qui rejette ceci ne rejette pas un être humain, mais Elohîm qui nous a aussi donné son Saint-Esprit.
4:9	Mais concernant l'amour fraternel, vous n'avez pas besoin que je vous en écrive, car vous êtes vous-mêmes enseignés par Elohîm à vous aimer les uns les autres,
4:10	car c'est aussi ce que vous faites à l'égard de tous les frères qui sont dans toute la Macédoine. Mais nous vous exhortons, frères, à y abonder de plus en plus,
4:11	et de tâcher sincèrement de rester tranquilles, de vous occuper de vos propres affaires, et de travailler de vos propres mains, ainsi que nous vous l'avons ordonné,
4:12	afin que vous marchiez d'une manière bienséante envers ceux du dehors et que vous n'ayez besoin de personne.
4:13	Or frères, je ne veux pas que vous soyez dans l'ignorance au sujet de ceux qui se sont endormis, afin que vous ne soyez pas attristés comme les autres aussi qui n'ont pas d'espérance.
4:14	Car si nous croyons que Yéhoshoua est mort et qu'il s’est relevé, de même aussi ceux qui se sont endormis en Yéhoshoua, Elohîm les ramènera avec lui.
4:15	Car nous vous disons ceci par la parole du Seigneur, c'est que nous, les vivants restés pour la parousie du Seigneur, nous ne précéderons jamais ceux qui se sont endormis.
4:16	Parce que le Seigneur lui-même, avec un cri de commandement<!--L'expression « cri de commandement » vient du grec « keleuma », ce mot signifie un ordre, et en particulier un cri stimulant, comme celui que reçoit un animal pressé par un homme, tels les chevaux par les conducteurs de chariots, les chiens de chasse par les chasseurs, etc. ; ou par lequel un ordre est donné par le capitaine d'un navire, aux soldats par un chef, un appel de trompette. La sagesse d'Elohîm crie (Pr. 8). Yesha`yah (Ésaïe) devait crier à plein gosier (Es. 58:1). Le cri du Seigneur ne sera entendu que par l'Assemblée véritable qui est son épouse (Mt. 25:6). Voir Za. 9:14.-->, avec une voix d'archange et avec la trompette<!--Za. 9:14.--> d'Elohîm, descendra du ciel et les morts en Mashiah ressusciteront premièrement.
4:17	Ensuite nous, les vivants restés, nous serons enlevés ensemble avec eux dans les nuées à la rencontre du Seigneur dans les airs, et ainsi nous serons toujours avec le Seigneur.
4:18	C'est pourquoi consolez-vous les uns les autres par ces paroles.

## Chapitre 5

5:1	Mais concernant les temps<!--Chronos est le temps physique. Il permet de segmenter le temps : passé, présent et futur.--> et les mesures de temps<!--Kairos est un temps métaphysique. Kairos n'est pas linéaire, il est qualitatif et se ressent. C'est le temps favorable.-->, frères, vous n'avez pas besoin qu'on vous en écrive,
5:2	car vous savez vous-mêmes précisément que le jour du Seigneur vient comme un voleur dans la nuit<!--Mt. 25:6 ; 2 Pi. 3:10 ; Ap. 3:3, 16:15.-->.
5:3	Car quand ils disent : Paix et sûreté ! alors une destruction soudaine vient soudainement sur eux, comme les douleurs de l'enfantement sur celle qui est enceinte<!--Littéralement "qui l'a dans le ventre.-->, et ils n'échapperont jamais.
5:4	Mais vous, frères, vous n'êtes pas dans la ténèbre pour que ce jour vous saisisse comme un voleur.
5:5	Tous vous êtes fils de lumière<!--Les disciples de Yéhoshoua sont les enfants de la lumière (Jn. 12:36 ; Ep. 5:8). Yéhoshoua est la lumière du monde (Jn. 8:12).--> et fils du jour. Nous ne sommes pas de la nuit ni de la ténèbre.
5:6	Ainsi donc ne dormons pas comme les autres, mais veillons et soyons sobres.
5:7	Car ceux qui dorment, dorment la nuit, et ceux qui s'enivrent sont ivres la nuit.
5:8	Mais nous qui sommes du jour, soyons sobres, ayant revêtu la cuirasse de la foi et de l'amour, et pour casque, l'espérance du salut<!--Ro. 13:12 ; Ep. 6:14,17.-->.
5:9	Parce qu'il ne nous a pas placés, l'Elohîm, pour la colère<!--La colère à venir. Voir 1 Th. 1:9-10.-->, mais pour l'acquisition du salut au moyen de notre Seigneur Yéhoshoua Mashiah,
5:10	qui est mort en notre faveur, afin que soit que nous veillions, soit que nous dormions, nous vivions avec lui.
5:11	C'est pourquoi exhortez-vous réciproquement, et édifiez-vous tous, les uns les autres, comme aussi vous le faites.
5:12	Mais nous vous prions, frères, de considérer ceux qui travaillent parmi vous, et qui vous dirigent dans le Seigneur, et qui vous avertissent.
5:13	Et ayez pour eux beaucoup d'estime et d'amour<!--Littéralement « agape » : amour fraternel, affection.--> en raison de leur œuvre. Soyez en paix entre vous.
5:14	Mais nous vous y exhortons, frères, avertissez les désordonnés<!--Mt. 18:15 ; Ga. 6:1.-->, encouragez les poltrons<!--« Ceux qui manquent d'audace », « les peureux », « trouillards », « craintifs ».-->, supportez les faibles, et soyez patients envers tous.
5:15	Prenez garde que personne ne rende à autrui mal pour mal<!--Mt. 5:44 ; Ro. 12:21.-->, mais poursuivez toujours ce qui est bon, et les uns envers les autres, et envers tous.
5:16	Soyez toujours joyeux.
5:17	Priez sans cesse.
5:18	En toute chose rendez grâces, car telle est la volonté d'Elohîm en Mashiah Yéhoshoua pour vous.
5:19	N'éteignez pas l'Esprit.
5:20	Ne méprisez pas les prophéties.
5:21	Éprouvez<!--Vient du grec « dokimazo » qui, en français, se traduit par « mettre à l'épreuve », « examiner », « éprouver (épreuve de métaux) », « reconnaître comme véritable après examen », « juger digne ». La racine de « dokimazo » est « dokimos » : « approuver ».--> toutes choses<!--Voir 1 Ti. 3:10.-->, retenez ce qui est bon.
5:22	Abstenez-vous de toute forme de mal.
5:23	Mais que l'Elohîm de paix lui-même vous sanctifie parfaitement, et que votre être entier, l’esprit, et l’âme et le corps soit gardé sans reproche en la parousie de notre Seigneur Yéhoshoua Mashiah<!--La parousie du Seigneur. Voir Mt. 24:1-3.--> !
5:24	Celui qui vous appelle est fidèle, et il le fera.
5:25	Frères, priez à notre sujet.
5:26	Saluez tous les frères par un saint baiser.
5:27	Je vous adjure par le Seigneur que cette lettre soit lue à tous les saints frères.
5:28	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous ! Amen !
