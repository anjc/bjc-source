#!/bin/bash

error=false
files="[0-9][0-9]-*.md"


#echo "Trouver le mot Nom en majuscule."
#if grep --exclude README.md --color=always " Nom[ ,.]" *md; then
#  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
#fi
echo "Trouver le mot mer en minuscule. Ex: 'mer Salée' au lieu de 'Mer Salée'"
if grep --exclude README.md --color=always "\bmer [A-ZÂÄÉÈÊËÔÖÎÏ]" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les verbes se terminant par un 'd' ou un 't' avec '-t-' qui suit"
if grep --exclude README.md --color=always -i -P "[td]\-t\-.[ln]" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les \"il\" aux singulier alors que le verbe est au pluriel"
if grep --exclude README.md --color=always -i -P "\bil [neysmlovtaui'’]* *['’\-a-zéëêèáâäàôöûüîï]+r[eo]nt\b" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les \"de d'\""
if grep --exclude README.md --color=always -i " de d['’]" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les mauvais traits d'union"
if grep --exclude README.md --color=auto "–[^,\. ]" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver la lettre O à la place d'un zéro 0"
if grep --exclude README.md --color=always -i '[0-9]o' *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -i 'o[0-9]' *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver le mot 'peronne' au lieu de 'personne'"
if grep --exclude README.md --color=always -i "peronne" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo 'Trouver les mauvais guillemets (")'
if grep --exclude README.md --color=always -P '[^=]["“][^"”=]+["”]' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les espaces manquants après un commentaire"
if grep --exclude README.md --color=auto -P '\-\->[^ \n\[,-\.”’\)]' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les espaces en trop avant un commentaire"
if grep --exclude README.md --color=always -P ' <\!' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les espaces en trop avant une virgule"
if grep --exclude README.md --color=always ' ,' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les mots collés entre eux avec une virgule au milieu"
if grep --exclude README.md --color=auto -P '[^0-9\t ],[^0-9\t\n ]' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles 'lorsque que'"
if grep --exclude README.md --color=always -i -P 'lorsque que ' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles 'ii' au lieu de 'il'"
if grep --exclude README.md --color=always -i -P '[#\.\?\!:;,<>\)\t] *ii ' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles 'll' au lieu de 'il'"
if grep --exclude README.md --color=always -i -P '[#\.\?\!:;,<>\)\t] *ll ' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles 'IL' ou 'ILs' au lieu de 'Il' ou 'Ils'"
if grep --exclude README.md --color=always -P '[#\.\?\!:;,<>\)\t] *ILs? ' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les Ils (majuscule) avec un point manquant"
if grep --exclude README.md --color=always -P '[^.\!\?\:#\]«–] Ils' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[^.\!\?\:#\]«–] Elle' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles 'ells' au lieu de 'elle'"
if grep --exclude README.md --color=always -iP ' +ells' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver le tétragramme mal orthographié"
if grep --color=always "YWHW" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver le mot 'sabbat' au lieu de 'shabbat'"
if grep --exclude README.md --color=always -iP "\bsabbats?\b" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"	
fi;
echo "Trouver le mot 'elohim', 'élohim', 'elohim', 'élohîm' ou 'elohïm'... au lieu de 'elohîm'"
if grep --exclude README.md --color=always -i -P "([eé]lohim|élohîm|[eé]lohïm)" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver le mot 'assembée' au lieu de 'assemblée'"
if grep --exclude README.md --color=always -i -P "assembée" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles lettres en début de mot."
if grep --exclude README.md --color=always -P '[\t ]aa' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[\t ]aA' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md -P '[\t ]Aa' *.md | grep -v "Aaron" | grep --color=always -P '[\t ]Aa'; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]àà' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ââ' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]bb' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]cc' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]dd' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ee' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]éé' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]êê' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ff' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]gg' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]hh' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '\tii' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[ ]ii' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[\t ]iI' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[\t ]Ii' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]jj' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]kk' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ll' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]mm' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]nn' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]oo' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ôô' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]pp' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]qq' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]rr' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ss' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]tt' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]uu' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]vv' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]ww' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]xx' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]yy' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP '[\t ]zz' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les triples lettres en début de mot."
if grep --exclude README.md --color=always -iP 'a{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'b{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'c{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'd{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'e{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'f{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'g{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'h{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -i 'i{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -i 'Ii{2}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'j{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'k{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'l{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'm{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'n{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'o{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'p{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'q{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'r{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 's{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 't{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'u{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'v{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'w{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'x{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'y{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -iP 'z{3}' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les ponctuations écrits en double"
if grep --exclude README.md --color=always -P "\? *\?" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "\! *\!" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P ": *:" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P ", *," *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "\) *\)" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "\( *\(" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "; *;" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "« *«" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "» *»" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "” *”" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "“ *“" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les doubles espaces"
if grep --exclude README.md --color=always -P " {2,}." *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les ponctuations collées entre elles avec un commentaire au milieu"
if grep --exclude README.md --color=always -P '[\.\?\;\!-,] *<!--.+--> *[\.\?\;\!-,]' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P ': *<!--.+--> *:' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les mots collés à un point"
if grep --exclude README.md --color=always -i "[^\.]\.[a-zéêëèäâàçîïôöûüùò]" *md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les ponctuations collées entre elles"
if grep --exclude README.md --color=always " \.[^\.]" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md -P "[^\.][^\.]\. *," *.md | grep -v "J.-C." | grep -v "etc." | grep --color=always -P "[^\.][^\.]\. *,"; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les références de versets avec le numéro collée au point"
if grep --exclude README.md --color=always -P "[^aA-Zz](Ge|Ex|Lé|No|De|Jos|Jg|1 S|2 S|1 R|2 R|Es|Jé|Ez|Os|Joë|Am|Ab|Jon|Mi|Na|Ha|So|Ag|Za|Mal|Ps|Pr|Job|Ca|Ru|La|Ec|Est|Da|Esd|Né|1 Ch|2 Ch|Mt|Mc|Lu|Jn|Ac|Ja|Ga|1 Th|2 Th|1 Co|2 Co|Ro|Ep|Ph|Col|Phm|1 Ti|Tit|1 Pi|2 Pi|2 Ti|Jud|Hé|1 Jn|2 Jn|3 Jn|Ap)\.[1-9]" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les versets ne se terminant pas par une ponctuation et enchaînant sur un chapitre suivant"
if grep --exclude README.md --color=always -Pzo --binary-files=text '[\r\n]([0-9: ]+\t).+[a-zêéëèàâäáãæôöòóõōïîìíùûüúçœß][ \r\n]+#' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les commentaires non fermés"
if grep --exclude README.md --color=always -P '<![^-].+>' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '<!-[^-].+->' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '<!.+[^-]+->' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '<![^>]+$' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P '[^<]\!--[^>]' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les versets en double"
if grep --exclude README.md --color=always -i -Pzo --binary-files=text '[\r\n]([0-9]+:[0-9]+).+[\r\n]\1\t' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -i -P '[\r\n]([0-9]+:[0-9]+).+\1\t' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les minuscules après un point (.) ou un point d'interrogation et un nouveau verset"
if grep --exclude README.md --color=always -Pzo --binary-files=text "[\.\?][ \r\n]+(#.+[ \r\n]+)?[0-9]+:[0-9]+[\t]+ *[a-zêéëèàâäáãæôöòóõōïîìíùûüúçœß].+" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
# FIXME
# if grep --exclude README.md --color=always "\. *[a-zêéëèàâäáãæôöòóõōïîìíùûüúçœß]" *.md; then
#   error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
# fi
echo "Trouver les majuscules après un point (.) sans espace"
if grep --exclude README.md --color=always "[a-zêéëèàâäáãæôöòóõōïîìíùûüúçœß]\.[A-ZÊÉËÈÀÂÄÁÃÆÔÖÒÓÕŌÏÎÌÍÙÛÜÚÇŒẞ]" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver l'espace avant la fin du commentaire"
if grep --exclude README.md --color=always -P ' ((?=-->)[^>]+>)' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les commentaires non reliés à un mot mais à une espace ou une ponctuation"
if grep --exclude README.md --color=always -P '^[^#]+[\.\?\;\!-,]\s?((?=<!--)[^>"]+>)' *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les début de verset avec un espace au lieu d'une tabulation"
if grep --exclude README.md --color=always -i -P "^[0-9]+:[0-9]+ " *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les tabulation avec un espace en trop"
if grep --exclude README.md --color=always -i -P "^[0-9]+:[0-9]+\t " *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les mauvaises tabulations"
if grep --exclude README.md --color=always -i -P "[\.,; \!\?\:][0-9]+:[0-9]+\t" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les lettres seules au milieu d'espaces"
if grep --exclude README.md -i -P "[\t ][b-np-xzêéëèâäáãæöòóõōïîìíùûüúçœß] " *.md | grep -v "Agrippa" | grep -v "Salmanasar" | grep --color=always -i -P "[\t ][b-np-xzêéëèâäáãæöòóõōïîìíùûüúçœß] "; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
if grep --exclude README.md --color=always -P "[\t ][OA] " *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les caractères seules sur une ligne"
if grep -n --exclude README.md -P "^.{1}$" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les sous-chapitres dans le sous-niveau est supérieur ou égale à 4"
if grep --exclude README.md --color=always -P "####+" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver les titres de chapitre mal orthographiés. Ex: '## CHAPITRE 1' au lieu de '## Chapitre 1'"
if grep -P '^## ' *.md | grep --color=always --invert-match -P 'Chapitre [1-9]([0-9])*'; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
echo "Trouver le verset contenant un retour à la ligne dans le texte"
if grep --exclude README.md --color=always -Pzo "[\r\n]+\d+:\d+.*[\r\n]+[^\d+:\d+\t][^#].*" *.md; then
  error=true
fi
echo "Trouver le nombre de chapitre"
countBooks=$(ls $files | wc -l)
if [ "$countBooks" != "66" ]; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
  echo "Attendu 66 - trouvé $countBooks livres"
fi
#echo "Trouver et vérifier le nombre verset et de chapitre"
#var1=$(<tests/number_of_verse.txt)
#var2=$(
#for f in $files; do
#    result=$(grep -Po "^[0-9]+:[0-9]+\t" $f | cut -d":" -f1 | uniq -c | awk -F " " '{print $2","$1}')
#    for res in $result; do
#        echo "$f:$res"
#    done
#done
#)
#differences=$(diff  <(echo "$var1") <(echo "$var2"))
#if [ "$differences" ]; then
#  echo "$differences"
#  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
#fi
echo "Trouver les sauts de chapitre"
for i in `seq 0 150`;
do
  j=$(($i+1));
  y=$(($i+2));
  found=$(grep --exclude README.md -Pzo --binary-files=text "[\r\n]## Chapitre $i([ \r\n][\r\n\xa0[:alnum:]A-ZÉÊÈËÔÖÒÓÕŌÎÏÌÍÀÄÂÁÃÆÇÛÜÙÚŒẞa-zêéëèàâäáãæôöòóõōïîìíùûüúçœß '\"‘’,;?\!\:#\.<>«»“”\t\(\)–\-\[\]\\\/\*\+\`\´\=\*\%\—\~τ]+)(## Chapitre $y[ \r\n]([ \r\n][\r\n\xa0[:alnum:]A-ZÉÊÈËÔÖÒÓÕŌÎÏÌÍÀÄÂÁÃÆÇÛÜÙÚŒẞa-zêéëèàâäáãæôöòóõōïîìíùûüúçœß '\"‘’,;?\!\:#\.<>«»“”\t\(\)–\-\[\]\\\/\*\+\`\´\=\*\%\—\~τ]+))## Chapitre $y[ \r\n]" 01*md | grep -P --invert-match "^## Chapitre $j")
  if [ "$found" ]; then
    echo "$found" | grep -P '\.md';
    echo "$found" | grep -P --color=always '^## Chapitre .+'
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
  fi
done
echo "Trouver les sauts de versets"
if grep --exclude README.md --color=always -Pzo --binary-files=text "Chapitre.+[\r\n]+(#.+[\r\n]+)?[0-9]+:[^1]" *.md; then
  error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
fi
for i in `seq 0 300`;
do
  y=$(($i+2));
  if grep --exclude README.md --color=always -i -Pzo --binary-files=text "[\r\n]([0-9]+):$i\t+.+[ \r\n]+(#.+[ \r\n]+)?\1:$y\t" *.md; then
    error=true ;   echo -e "\033[0;31mTest FAILED\033[0m"
  fi
done

if $error; then
  # red
  echo -e "\033[0;31mTests failed\033[0m"
  exit 1
else
  # green
  echo -e "\033[0;32mSuccess\033[0m"
  exit 0
fi
