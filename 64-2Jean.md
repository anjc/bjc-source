# 2 Yohanan (2 Jean) (2 Jn.)

Signification : YHWH a fait grâce

Auteur : Yohanan (Jean)

(Gr. : Ioannes)

Thème : Amour et vérité

Date de rédaction : Env. 85 ap. J.-C.

Il semblerait que cette lettre était adressée à une assemblée se réunissant chez une personne du nom de Kuria. Yohanan les invite à demeurer dans la communion avec Elohîm et les met en garde contre les hérésies et la fréquentation des faux docteurs.

## Chapitre 1

1:1	L'ancien, à Kuria l'élue et à ses enfants que j'aime en vérité - et non pas moi seul, mais aussi tous ceux qui ont connu la vérité,
1:2	à cause de la vérité qui demeure en nous et qui sera avec nous pour l’éternité.
1:3	Avec nous sera grâce, miséricorde, shalôm d'Elohîm le Père et du Seigneur Yéhoshoua Mashiah, le Fils du Père, en vérité et amour !
1:4	Je me suis beaucoup réjoui parce que j’ai trouvé de tes enfants qui marchent dans la vérité, selon le commandement que nous avons reçu du Père.
1:5	Et maintenant, je te prie, Kuria, comme t'écrivant, non pas un commandement nouveau mais celui que nous avons eu dès le commencement, que nous ayons de l'amour les uns pour les autres.
1:6	Et c'est ici l'amour, que nous marchions selon ses commandements. C'est ici le commandement, comme vous l'avez entendu dès le commencement, afin que vous y marchiez.
1:7	Parce que beaucoup de trompeurs sont venus dans le monde, ne confessant pas Yéhoshoua Mashiah venant en chair. C'est là le Trompeur et l'Anti-Mashiah<!--Antichrist.-->.
1:8	Discernez vous-mêmes, afin que nous ne perdions pas ce que nous avons produit, mais que nous recevions la pleine récompense.
1:9	Quiconque va au-delà<!--« Aller sur le côté », « dépasser sans toucher à une chose », « dépasser », « négliger », « violer », « transgresser ».--> et ne demeure pas dans la doctrine du Mashiah n'a pas Elohîm. Celui qui demeure dans la doctrine du Mashiah a le Père et le Fils.
1:10	Si quelqu'un vient vers vous et n'apporte pas cette doctrine, ne le recevez pas dans votre maison et ne lui dites pas : Salut !
1:11	Car celui qui lui dit : Salut, participe à ses mauvaises œuvres.
1:12	Ayant beaucoup de choses à vous écrire, je n'ai pas voulu les écrire avec du papier et de l'encre, mais j'espère aller vers vous et vous parler bouche à bouche afin que notre joie soit accomplie.
1:13	Les enfants de ta sœur élue te saluent. Amen !
