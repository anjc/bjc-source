# Philémon (Phm.)

Signification : Attentionné, qui embrasse

Auteur : Paulos (Paul)

(Gr. : Philemon)

Thème : Un exemple d'amour

Date de rédaction : Env. 60 ap. J.-C.

Paulos (Paul) écrivit cette lettre en prison, lors de sa deuxième captivité à Rome vers l'été 62, en même temps que la lettre aux Colossiens. Il s'adresse à Philémon, chrétien fortuné de Colosses ainsi qu'à sa femme Apphia, son fils Archippos, et à l'assemblée qui se réunissait dans leur maison. Paulos demande à Philémon de pardonner à Onesimos (Onésime), son esclave, de s'être échappé d'auprès de lui. Il assure à Philémon que désormais une nouvelle relation le lierait à Onesimos qui avait accepté Yéhoshoua ha Mashiah (Jésus-Christ) dans sa vie. Il va même jusqu'à proposer de payer personnellement ce qu'Onesimos lui devait, tout en exprimant l'espoir que Philémon ferait plus que ce qu'il lui demande. Ainsi, Paulos plaide pour Onesimos comme le Mashiah le fit en notre faveur.

## Chapitre 1

1:1	Paulos, prisonnier de Yéhoshoua Mashiah, et le frère Timotheos, à Philémon notre bien-aimé et compagnon d'œuvre,
1:2	et à Apphia, notre bien-aimée, et à Archippos, notre compagnon de combat, et à l'assemblée qui est dans ta maison :
1:3	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:4	Je rends grâce à mon Elohîm, faisant toujours mention de toi dans mes prières,
1:5	en entendant parler de l'amour et de la foi que tu as envers le Seigneur Yéhoshoua et pour tous les saints,
1:6	afin que la communion de ta foi devienne efficace par la connaissance précise et correcte de tout ce qui est bon en vous, pour Mashiah Yéhoshoua.
1:7	Car, mon frère, nous avons une grande grâce et une grande consolation au sujet de ton amour, parce que les entrailles des saints ont été mises en repos par ton moyen.
1:8	C'est pourquoi, bien qu’ayant une grande liberté en Mashiah de t'ordonner ce qui est convenable,
1:9	à cause de l’amour, je te supplie plutôt, étant ce que je suis : moi, Paulos, un vieillard, et même maintenant un prisonnier de Yéhoshoua Mashiah.
1:10	Je te supplie au sujet de mon enfant Onesimos, que j'ai engendré dans mes liens,
1:11	qui t'a été autrefois inutile, mais qui maintenant est bien utile à toi et à moi.
1:12	Je te le renvoie, et toi, reçois-le donc comme mes propres entrailles.
1:13	Je voulais le retenir auprès de moi, afin qu'il me serve en faveur de toi dans les liens que je porte pour l'Évangile,
1:14	mais je n'ai rien voulu faire sans ton avis, afin que ta bonne action ne soit pas comme une nécessité imposée par les circonstances, mais volontaire.
1:15	Car c’est peut-être pour cette raison qu’il a été séparé de toi pour un temps, afin que tu l'aies<!--Synonymes : retrouver, reconquérir, regagner.--> pour toujours,
1:16	non plus comme un esclave, mais comme étant au-dessus d'un esclave, comme un frère bien-aimé, principalement de moi, et combien plus de toi, soit dans la chair, soit dans le Seigneur ?
1:17	Si donc tu me tiens pour ton compagnon, reçois-le comme moi-même.
1:18	Et s'il t'a fait quelque tort, ou s'il te doit quelque chose, mets-le sur mon compte.
1:19	Moi Paulos, j'ai écrit ceci de ma propre main, je te le payerai, pour ne pas te dire que tu te dois toi-même à moi.
1:20	Oui, mon frère, que je reçoive de toi cet avantage, dans le Seigneur : réjouis mes entrailles en notre Seigneur.
1:21	Étant persuadé de ton obéissance, je t'écris sachant que tu feras même plus que ce que je te dis.
1:22	Mais en même temps, prépare-moi un logement, car j'espère que je vous serai rendu par le moyen de vos prières.
1:23	Te saluent, Épaphras, mon compagnon de captivité en Mashiah Yéhoshoua,
1:24	Markos, Aristarchos, Démas, Loukas, mes compagnons d'œuvre.
1:25	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec votre esprit ! Amen !
