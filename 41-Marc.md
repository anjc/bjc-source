# Markos (Marc) (Mc.)

Signification : Une défense, grand marteau

Auteur : Markos (Marc)

Thème : Yéhoshoua, le Serviteur

Date de rédaction : Env. 68 ap. J.-C.

Originaire de Yeroushalaim (Jérusalem), Markos fut l'auteur de l'évangile du même nom. Il était également appelé Yohanan. Cousin de Barnabas et collaborateur de Paulos (Paul), ce dernier l'éconduit lors d'un voyage, car Markos l'avait abandonné lors d'une précédente mission. Ce fut d'ailleurs la cause de la séparation de Barnabas et Paulos. Par la suite, il renoua le contact avec Paulos et devint un de ses fidèles compagnons de service. Lié à Petros (Pierre) tel un fils, ce fut probablement sous son autorité qu'il écrivit. En effet, l'évangile de Markos expose le témoignage de Petros sur Mashiah.

Adressé aux nations, cet évangile contient peu de références à la première alliance. On y découvre Yéhoshoua (Jésus), l'inlassable serviteur d'Elohîm et des humains. Markos y présente la richesse de ses bonnes œuvres, son incomparable dévouement, révélant les sentiments intimes du Maître, tandis que le récit des miracles met en exergue toute la puissance du Mashiah.

## Chapitre 1

### Yohanan le Baptiste (Jean-Baptiste), l'envoyé d'Elohîm<!--Mt. 3:1-12 ; Lu. 3:1-20 ; Jn. 1:6-8,15-37.-->

1:1	Le commencement de l'Évangile de Yéhoshoua Mashiah, le Fils d'Elohîm,
1:2	selon qu'il est écrit dans les prophètes : Voici, moi j'envoie mon messager devant ta face, lequel préparera ta voie devant toi.
1:3	La voix de celui qui crie : Dans le désert, préparez la voie du Seigneur, aplanissez ses sentiers<!--Es. 40:3 ; Mal. 3:1.-->.
1:4	Yohanan vint, baptisant dans le désert, et prêchant le baptême de repentance pour la rémission des péchés.
1:5	Et tout le pays de Judée, et les habitants de Yeroushalaim allaient vers lui, et confessant leurs péchés, ils se faisaient tous baptiser par lui dans le fleuve du Yarden<!--Jourdain.-->.
1:6	Or Yohanan était vêtu de poils de chameau, il avait une ceinture de cuir autour de ses reins, et mangeait des sauterelles et du miel sauvage.
1:7	Et il prêchait, en disant : Il vient après moi, celui qui est plus puissant que moi, et je ne suis pas digne de délier en me baissant le lacet de ses sandales.
1:8	Moi, je vous ai en effet baptisés dans l'eau, mais lui vous baptisera dans le Saint-Esprit.

### Le baptême de Yéhoshoua ha Mashiah (Jésus le Christ)<!--Mt. 3:13-17 ; Lu. 3:21-22 ; Jn. 1:31-34.-->

1:9	Or il arriva en ce temps-là que Yéhoshoua vint depuis Nazareth de Galilée, et il fut baptisé par Yohanan dans le Yarden.
1:10	Et immédiatement, en sortant de l’eau, il vit les cieux s'ouvrir, et le Saint-Esprit descendre sur lui comme une colombe.
1:11	Et une voix vint des cieux : Tu es mon Fils bien-aimé, en qui j'ai pris plaisir.

### La tentation de Yéhoshoua ha Mashiah<!--Mt. 4:1-11 ; Lu. 4:1-13.-->

1:12	Et immédiatement l'Esprit le pousse dehors dans un désert.
1:13	Et il fut dans le désert 40 jours, tenté par Satan. Il était avec les bêtes sauvages et les anges le servaient.

### Yéhoshoua en Galilée<!--Mt. 4:12-17 ; Lu. 4:14-15.-->

1:14	Or après que Yohanan eut été mis en prison, Yéhoshoua alla dans la Galilée, prêchant l'Évangile du Royaume d'Elohîm,
1:15	et disant : Le temps est accompli et le Royaume d'Elohîm s'est approché. Repentez-vous et croyez à l'Évangile.

### Appel de Shim’ôn Petros (Simon Pierre), Andreas (André), Yaacov (Jacques) et Yohanan (Jean)<!--Lu. 5:1-11 ; Jn. 1:35-51.-->

1:16	Et en marchant près de la Mer de Galilée, il vit Shim’ôn et Andreas, son frère, jetant leurs filets dans la mer, car ils étaient pêcheurs.
1:17	Et Yéhoshoua leur dit : Venez derrière moi et je vous ferai devenir des pêcheurs d'êtres humains.
1:18	Et immédiatement, laissant leurs filets, ils le suivirent.
1:19	Et en allant un peu plus loin, il vit Yaacov, celui de Zabdi<!--Généralement traduit par Zébédée.--> et Yohanan, son frère, qui, eux aussi étaient dans un bateau, réparant leurs filets.
1:20	Et immédiatement il les appela et, laissant leur père Zabdi dans le bateau avec les ouvriers, ils le suivirent.

### Yéhoshoua délivre un démoniaque<!--Lu. 4:31-37.-->

1:21	Et ils entrèrent dans Capernaüm. Et immédiatement, le jour du shabbat, étant entré dans la synagogue, il enseignait.
1:22	Et ils étaient choqués par sa doctrine, car il les enseignait comme ayant autorité et non pas comme les scribes.
1:23	Or il se trouva dans leur synagogue un homme qui avait un esprit impur, et qui s'écria,
1:24	en disant : Ah ! Qu'y a-t-il entre toi et nous, Yéhoshoua de Nazareth ? Es-tu venu pour nous détruire ? Je sais qui tu es ! Le Saint<!--Yéhoshoua (Jésus) est le Saint dont Iyov (Job) n'avait pas transgressé la loi (Job 6:10). Voir Lu. 1:35, 4:34. Voir commentaire en Ac. 3:14.--> d'Elohîm.
1:25	Et Yéhoshoua le réprimanda d'une manière tranchante, en disant : Sois muselé, et sors de cet homme !
1:26	Et l'esprit impur sortit hors de lui en le faisant se convulser et en poussant un grand cri.
1:27	Et tous furent effrayés, de sorte qu'ils discutaient entre eux, en disant : Qu'est-ce que ceci ? Quelle est cette nouvelle doctrine ? Il commande avec autorité même aux esprits impurs et ils lui obéissent !
1:28	Et la rumeur à son sujet se répandit immédiatement dans toute la région environnante de la Galilée.

### Yéhoshoua guérit la belle-mère de Petros (Pierre)<!--Mt. 8:14-15 ; Lu. 4:38-39.-->

1:29	Et immédiatement après, étant sortis de la synagogue, ils se rendirent avec Yaacov et Yohanan à la maison de Shim’ôn et d'Andreas.
1:30	Or la belle-mère de Shim’ôn était couchée, ayant la fièvre, et au même moment, on lui parla d'elle.
1:31	Et s'étant approché, il la réveilla en la prenant par la main, et immédiatement la fièvre la quitta. Et elle les servit.

### Yéhoshoua guérit les malades et chasse les démons<!--Mt. 8:16-17 ; Lu. 4:40-44.-->

1:32	Or le soir étant venu, comme le soleil se couchait, on lui amena tous les malades et les démoniaques.
1:33	Et toute la ville était assemblée devant la porte.
1:34	Et il guérit beaucoup de malades qui avaient différentes maladies et chassa beaucoup de démons ; et il ne permit pas aux démons de dire qu'ils le connaissaient.

### La vie de prière du Maître

1:35	Et dès le matin<!--La quatrième veille de la nuit, qui va approximativement de 3 à 6 heures.-->, pendant qu'il faisait excessivement nuit, s’étant levé, il sortit et s’en alla dans un lieu désert, et là, il priait.
1:36	Et Shim’ôn et ceux qui étaient avec lui allèrent à sa recherche,
1:37	et l'ayant trouvé, ils lui disent : Tous te cherchent.
1:38	Et il leur dit : Allons dans les bourgades voisines, afin que j'y prêche aussi, car c'est pour cela que je suis venu.
1:39	Et il prêchait dans leurs synagogues par toute la Galilée et chassant les démons.

### Yéhoshoua guérit un lépreux<!--Mt. 8:2-4 ; Lu. 5:12-14.-->

1:40	Et un lépreux vient à lui, le priant et tombant à genoux devant lui, et lui dit : Si tu le veux, tu peux me rendre pur.
1:41	Et Yéhoshoua, ému de compassion, étendit sa main et le toucha, en lui disant : Je le veux, sois pur.
1:42	Et dès qu'il eut parlé, immédiatement la lèpre le quitta, et il fut purifié.
1:43	Et il le renvoya immédiatement en lui donnant un sérieux avertissement,
1:44	et lui dit : Fais attention ! Ne dis rien à personne, mais va te montrer au prêtre et présente pour ta purification les choses que Moshé a commandées, pour leur servir de témoignage<!--Loi sur la purification de la lèpre : Lé. 14:1-32. Avant sa mort et sa résurrection, Yéhoshoua ha Mashiah (Jésus le Christ) observait la torah de Moshé (loi de Moïse) (Mt. 23:1-3).-->.
1:45	Mais, s'en étant allé, il commença à publier ouvertement beaucoup de choses et à divulguer l’affaire, de sorte que Yéhoshoua ne pouvait plus entrer publiquement dans la ville. Mais il se tenait dehors, dans des lieux déserts, et l'on venait à lui de toutes parts.

## Chapitre 2

### Yéhoshoua pardonne et guérit un paralytique<!--Mt. 9:2-8 ; Lu. 5:18-26.-->

2:1	Et quelques jours après, il entra de nouveau dans Capernaüm et l’on apprit qu’il était dans une maison.
2:2	Et immédiatement il se rassembla un si grand nombre de personnes, que l'espace même devant la porte ne pouvait plus les contenir. Il leur annonçait la parole.
2:3	Et ils viennent, amenant vers lui un paralytique porté à quatre.
2:4	Et ne pouvant s’approcher de lui à cause de la foule, ils enlevèrent le toit du lieu où il était et, l'ayant arraché, ils descendirent le lit de camp sur lequel le paralytique était étendu.
2:5	Et Yéhoshoua, voyant leur foi, dit au paralytique : Enfant, tes péchés sont remis.
2:6	Et quelques scribes qui étaient assis là, raisonnaient dans leurs cœurs :
2:7	Pourquoi celui-ci profère-t-il ainsi des blasphèmes ? Qui peut remettre les péchés, excepté un seul : Elohîm ?
2:8	Et Yéhoshoua, ayant aussitôt connu par son esprit qu'ils raisonnaient ainsi en eux-mêmes, leur dit : Pourquoi faites-vous ces raisonnements dans vos cœurs ?
2:9	Qu'est-ce qui est plus facile ? Dire au paralytique : Tes péchés sont remis, ou lui dire : Lève-toi<!--Le mot grec utilisé est « egeiro » qui signifie « se lever », « faire lever », « se réveiller », « revenir à la vie », « faire naître ».-->, prends ton lit de camp et marche ?
2:10	Mais afin que vous sachiez que le Fils d'humain a l'autorité de remettre les péchés sur la Terre - il dit au paralytique :
2:11	Je te dis : Lève-toi, prends ton lit de camp, et va dans ta maison.
2:12	Et il se réveilla aussitôt, et ayant pris son lit de camp, il sortit en présence de tous, de sorte qu'ils en furent tous étonnés, et ils glorifièrent Elohîm, en disant : Nous n'avons jamais rien vu de pareil.

### Appel de Lévi<!--Mt. 9:9 ; Lu. 5:27-28.-->

2:13	Et il sortit de nouveau du côté de la mer, et toute la foule venait à lui, et il les enseignait.
2:14	En passant, il vit Lévi, celui d'Alphaios, assis au bureau des péages, et il lui dit : Suis-moi. Et s'étant levé, il le suivit.

### Appel des pécheurs à la repentance<!--Mt. 9:10-15 ; Lu. 5:29-35.-->

2:15	Et il arriva qu'étant à table dans sa maison, beaucoup de publicains et des pécheurs se mirent aussi à table avec Yéhoshoua et avec ses disciples, car ils étaient nombreux, et ils le suivaient.
2:16	Et les scribes et les pharisiens le voyant manger avec les publicains et les pécheurs, disaient à ses disciples : Pourquoi mange-t-il et boit-il avec les publicains et les pécheurs ?
2:17	Et Yéhoshoua ayant entendu cela, leur dit : Ce ne sont pas ceux qui se portent bien qui ont besoin de médecin, mais les malades. Je ne suis pas venu appeler des justes, mais des pécheurs à la repentance.

### Les pharisiens et les disciples de Yohanan (Jean) interrogent Yéhoshoua sur le jeûne

2:18	Or les disciples de Yohanan et ceux des pharisiens étaient en train de jeûner. Ils viennent et lui disent : En raison de quoi les disciples de Yohanan et ceux des pharisiens jeûnent-ils, tandis que tes disciples ne jeûnent pas ?
2:19	Et Yéhoshoua leur répondit : Les fils de la chambre nuptiale peuvent-ils jeûner pendant que l'Époux est avec eux ? Aussi longtemps qu'ils ont avec eux l'Époux, ils ne peuvent jeûner.
2:20	Mais les jours viendront où l'Époux leur sera ôté, alors ils jeûneront en ce jour-là.

### Parabole du drap neuf et des outres neuves<!--Mt. 9:16-17 ; Lu. 5:36-39.-->

2:21	Et personne ne coud un ajout de tissu neuf à un vieux vêtement ; autrement la plénitude elle-même emporte le neuf du vieux, et la déchirure devient pire.
2:22	Et personne ne verse du vin nouveau dans de vieilles outres, autrement le vin nouveau brise en morceau les outres et le vin se répand, et les outres se perdent. Mais le vin nouveau doit être mis dans des outres neuves<!--Mt. 9:16-17 ; Lu. 5:36-39. Voir Job. 32:19.-->.

### Yéhoshoua, le Seigneur du shabbat<!--Mt. 12:1-8 ; Lu. 6:1-5.-->

2:23	Et il arriva un jour de shabbat qu'il passa à travers les champs ensemencés. Et ses disciples en marchant se mirent à arracher des épis.
2:24	Et les pharisiens lui dirent : Regarde, pourquoi font-ils ce qui n'est pas légal pendant les shabbats ?
2:25	Et il leur dit : N'avez-vous jamais lu ce que fit David quand il fut dans la nécessité, et qu'il eut faim, lui et ceux qui étaient avec lui ?
2:26	Comment il entra dans la maison d'Elohîm du temps du grand-prêtre Abiathar, et mangea les pains des faces<!--1 S. 21:1-7.-->, ce qui n'est légal que pour les prêtres de manger, et en donna même à ceux qui étaient avec lui ?
2:27	Et il leur dit : Le shabbat a été fait à cause de l'être humain, et non l'être humain à cause du shabbat.
2:28	C’est pourquoi le Fils d'humain est aussi Seigneur du shabbat.

## Chapitre 3

### Yéhoshoua guérit l'homme à la main desséchée<!--Mt. 12:9-13 ; Lu. 6:6-11.-->

3:1	Et il entra de nouveau dans la synagogue, et il y avait là un homme qui avait une main desséchée.
3:2	Et ils l'observaient si pendant les shabbats il le guérirait afin de l'accuser.
3:3	Et il dit à l'homme qui a la main desséchée : Réveille-toi ! Au milieu !
3:4	Et il leur dit : Est-il légal de faire du bien les shabbats ou de faire du mal, de sauver une âme ou de la tuer ? Mais ils gardèrent le silence.
3:5	Et les regardant tous avec colère, et étant affligé de l'endurcissement de leur cœur, il dit à cet homme : Étends ta main. Il l'étendit, et sa main fut restaurée à l'état initial comme l'autre.

### Les pharisiens complotent contre Yéhoshoua

3:6	Et les pharisiens sortirent, et aussitôt, ils se consultèrent contre lui avec les Hérodiens, sur comment ils feraient pour le détruire.

### Yéhoshoua guérit de nombreux malades<!--Mt. 12:15-16 ; Lu. 6:17-19.-->

3:7	Mais Yéhoshoua se retira vers la mer avec ses disciples. Une grande multitude le suivit de la Galilée, et de la Judée,
3:8	et de Yeroushalaim, et de l'Idumée, et d'au-delà du Yarden, et des environs de Tyr et de Sidon, une grande multitude, ayant entendu parler des grandes choses qu'il faisait, vint vers lui.
3:9	Et il dit à ses disciples de tenir toujours à sa disposition un petit bateau, à cause de la foule, pour qu'ils ne le pressent pas.
3:10	Car, comme il guérissait beaucoup de gens, tous ceux qui avaient des fléaux tombaient sur lui pour le toucher.
3:11	Et les esprits impurs, quand ils le voyaient, tombaient devant lui et s'écriaient en disant : Tu es le Fils d'Elohîm.
3:12	Mais il les réprimandait beaucoup, d'une manière tranchante, afin qu'ils ne le fassent pas connaître.

### L'appel des douze apôtres<!--Mt. 10:1-4 ; Lu. 6:13-16.-->

3:13	Et il monte sur la montagne, et appelle à lui ceux qu'il voulait, et ils allèrent vers lui.
3:14	Et il en fait douze pour qu’ils soient avec lui, et pour qu’il les envoie prêcher,
3:15	et pour avoir l'autorité de guérir les maladies, et de chasser les démons.
3:16	Et Shim’ôn à qui il imposa le nom de Petros,
3:17	et Yaacov, celui de Zabdi, et Yohanan, frère de Yaacov, et il leur imposa les noms de Boanergès, c'est-à-dire Fils du tonnerre,
3:18	et Andreas, et Philippos, et Bar-Talmaï, et Mattithyah, et Thomas, et Yaacov, celui d'Alphaios, et Thaddaios, et Shim’ôn le Qananite,
3:19	et Yéhouda Iskariote, celui qui le livra. Et il se rend dans une maison,
3:20	et une foule s'assemble de nouveau, au point de ne plus pouvoir, eux, manger même du pain.
3:21	Et ses parents, ayant entendu cela, sortirent pour se saisir de lui. Car ils disaient : Il est fou.

### Le blasphème contre le Saint-Esprit<!--Mt. 12:24-32 ; Lu. 11:15-23.-->

3:22	Et les scribes, qui étaient descendus de Yeroushalaim, disaient qu'il a Béelzéboul et c'est par le chef des démons qu'il chasse les démons.
3:23	Et les ayant appelés près de lui, il leur disait sous forme de paraboles : Comment Satan peut-il chasser Satan ?
3:24	Et si un royaume est divisé contre lui-même, ce royaume ne peut pas tenir debout,
3:25	et si une maison est divisée contre elle-même, cette maison ne peut pas tenir debout.
3:26	Et si le Satan s'élève contre lui-même, il est divisé, il ne peut pas tenir debout, mais il a une fin.
3:27	Personne ne peut entrer dans la maison d'un homme fort et piller ses biens s’il n’a d’abord lié cet homme fort. Alors il pillera sa maison.
3:28	Amen, je vous dis que tous les péchés seront remis aux fils des humains, ainsi que les blasphèmes par lesquels ils auront blasphémé.
3:29	Mais quiconque aura blasphémé<!--Voir le dictionnaire en annexe.--> contre le Saint-Esprit n'a pas de rémission pour l’éternité, mais il est passible du jugement éternel.
3:30	Parce qu'ils disaient : Il a un esprit impur.

### La famille spirituelle<!--Mt. 12:46-50 ; Lu. 8:19-21.-->

3:31	Alors arrivent ses frères et sa mère, et se tenant dehors, ils envoyèrent vers lui pour l’appeler.
3:32	Et une foule était assise autour de lui, et on lui dit : Voici ta mère et tes frères, dehors, ils te cherchent.
3:33	Mais il leur répondit, en disant : Qui est ma mère et qui sont mes frères ?
3:34	Et, jetant les regards sur ceux qui sont assis tout autour de lui, il dit : Voici ma mère et mes frères.
3:35	Car quiconque aura fait la volonté d'Elohîm, celui-là est mon frère, ma sœur et ma mère.

## Chapitre 4

### Parabole du semeur et des quatre terrains<!--Mt. 13:1-9 ; Lu. 8:4-10.-->

4:1	Et il commença de nouveau à enseigner au bord de la mer. Et une grande foule se rassembla auprès de lui, de sorte qu'il monta dans un bateau, sur la mer et s'y assit. Et toute la foule était à terre, près de la mer.
4:2	Et il leur enseignait beaucoup de choses en paraboles, et il leur disait dans son enseignement :
4:3	Écoutez ! Voilà que le semeur est sorti pour semer.
4:4	Et il arriva qu'en semant, la semence tomba en effet à côté de la voie, et les oiseaux du ciel vinrent et la mangèrent.
4:5	Mais une autre tomba dans un endroit pierreux où elle n'avait pas beaucoup de terre, et elle leva immédiatement, parce qu'elle n'entrait pas profondément dans la terre<!--Ou la terre où elle était n'avait pas de profondeur. Voir Ep. 3:18.-->.
4:6	Mais, quand le soleil parut, elle fut brûlée par la chaleur et, parce qu'elle n'a pas de racine, elle sécha.
4:7	Et une autre tomba dans les épines, et les épines montèrent et l'étouffèrent, et elle ne donna pas de fruit.
4:8	Et une autre tomba dans la bonne terre et donna du fruit montant et croissant. L'un porta 30, un autre 60 et un autre 100.
4:9	Et il leur disait : Que celui qui a des oreilles pour entendre, qu'il entende !
4:10	Mais lorsqu'il fut à l'écart, ceux qui étaient autour de lui avec les douze l'interrogèrent concernant la parabole.
4:11	Et il leur dit : Il vous a été donné de connaître le mystère du Royaume d'Elohîm, mais pour ceux de dehors, tout arrive en paraboles,
4:12	afin qu'en voyant, ils voient et n'aperçoivent pas, et qu'en entendant, ils entendent et ne comprennent pas, de peur qu'ils ne se convertissent, et que leurs péchés ne soient remis.

### Explication de la parabole du semeur<!--Mt. 13:18-23 ; Lu. 8:11-15.-->

4:13	Et il leur dit : Ne comprenez-vous pas cette parabole ? Et comment donc comprendrez-vous toutes les paraboles ?
4:14	Celui qui sème, sème la parole.
4:15	Ceux qui sont à côté de la voie, sont ceux en qui la parole est semée. Mais après qu'ils l'ont entendue, Satan vient immédiatement et enlève la parole qui a été semée dans leurs cœurs.
4:16	Et de même ceux qui sont semés sur les endroits pierreux, sont ceux qui, ayant entendu la parole, la reçoivent immédiatement avec joie,
4:17	et ils n'ont pas de racine en eux-mêmes, mais ils sont temporaires<!--pour une saison, supportant seulement pour un moment.--> et, dès que survient une tribulation ou une persécution à cause de la parole, ils sont immédiatement scandalisés.
4:18	Et il y en a d’autres qui sont semés dans les épines : ce sont ceux qui entendent la parole,
4:19	et les soucis de cet âge, et la séduction des richesses et les convoitises à l'égard des autres choses s'introduisent et étouffent la parole, et elle devient stérile.
4:20	Et ceux qui sont semés sur la bonne terre, sont ceux qui entendent la parole, la reçoivent et portent du fruit : L'un 30, et l'autre 60, et l'autre 100.

### Parabole de la lampe<!--Mt. 5:15-16 ; Lu. 8:16-18, 11:33-36.-->

4:21	Il leur disait aussi : Apporte-t-on la lampe pour la mettre sous le boisseau<!--Mesure de volume pour matière sèche d'environ 9 litres.--> ou sous le lit ? N'est-ce pas pour la mettre sur le chandelier ?
4:22	Car rien n’est secret si ce n’est pour être manifesté, et rien n’a été caché qu’afin de venir en évidence.
4:23	Si quelqu'un a des oreilles pour entendre, qu'il entende !
4:24	Il leur disait aussi : Discernez ce que vous entendez. De la mesure dont vous mesurerez, il vous sera mesuré ; mais à vous qui entendez, il sera ajouté.
4:25	Car on donnera à celui qui a, mais à celui qui n'a pas on ôtera même ce qu'il a.

### Parabole de la semence

4:26	Il disait aussi : Il en est du Royaume d'Elohîm comme quand un être humain jette la semence en terre,
4:27	et il s’endort et se réveille nuit et jour. Et la semence germe et croît, sans qu'il sache comment.
4:28	Car la terre porte du fruit d'elle-même<!--Vient du Grec « automatos » qui signifie : Agir par ses propres impulsions, sans l'intervention d'autrui. Automatos est traduit en Français par « automate ». Un automate est un dispositif reproduisant de manière autonome une séquence d'actions prédéterminées sans intervention humaine, le système faisant toujours la même chose.--> : premièrement l'herbe, puis l'épi, puis le grain formé dans l'épi,
4:29	et quand le fruit est mûr, on y met immédiatement la faucille, parce que la moisson est prête.

### Parabole du grain de sénevé<!--Mt. 13:31-32 ; Lu. 13:18-19.-->

4:30	Il disait aussi : À quoi comparerons-nous le Royaume d'Elohîm, ou par quelle parabole le représenterons-nous ?
4:31	Il en est comme du grain de sénevé qui, lorsqu'on le sème dans la terre, est la plus petite de toutes les semences qui sont sur la terre.
4:32	Mais après qu'il a été semé, il monte et devient plus grand que toutes les plantes, et pousse de grandes branches, en sorte que les oiseaux du ciel peuvent faire leurs nids sous son ombre.
4:33	Et c'est par beaucoup de paraboles de cette sorte qu'il leur annonçait la parole, selon qu'ils pouvaient l'entendre.
4:34	Et il ne leur parlait pas sans paraboles, mais, en particulier, il expliquait tout à ses disciples.

### L'autorité de Yéhoshoua sur les vents et la mer<!--Mt. 8:23-27 ; Lu. 8:22-25.-->

4:35	Et ce jour-là, le soir venu, il leur dit : Passons sur l'autre bord.
4:36	Et ayant renvoyé la foule, ils l’emmènent dans le petit bateau où il se trouvait. Et il y avait aussi d'autres petits bateaux avec lui.
4:37	Et un grand tourbillon de vent survient, et les grosses vagues se jetaient dans le bateau au point de le remplir.
4:38	Et lui, il était à la poupe, dormant sur l'oreiller. Et ils le réveillent et lui disent : Docteur, ne t'inquiètes-tu pas de ce que nous périssons ?
4:39	Et s'étant réveillé, il réprimanda d'une manière tranchante le vent, et dit à la mer : Silence ! Sois muselée ! Et le vent cessa, et il eut un grand calme<!--Ps. 107:29.-->.
4:40	Et il leur dit : Pourquoi avez-vous si peur ? Comment n'avez-vous pas de foi ?
4:41	Et ils craignirent d'une grande crainte, et ils se dirent l'un à l'autre : Qui est donc celui-ci, à qui obéissent le vent et la mer ?

## Chapitre 5

### Yéhoshoua ha Mashiah (Jésus le Christ) délivre un démoniaque à Gadara<!--Mt. 8:28-34 ; Lu. 8:26-40.-->

5:1	Et ils arrivèrent sur l'autre bord de la mer, dans le pays des Gadaréniens.
5:2	Et comme il sortait du bateau, immédiatement un homme possédé d’un esprit impur alla à sa rencontre depuis les sépulcres ;
5:3	lequel avait sa demeure dans les sépulcres, et personne ne pouvait le lier, même avec des chaînes.
5:4	Pour cette raison, il avait souvent eu les fers aux pieds et avait été lié de chaînes, mais il avait rompu les chaînes et brisé les fers, et personne n'avait la force de le dompter.
5:5	Et il était continuellement, nuit et jour sur les montagnes et dans les sépulcres, criant et se mutilant avec des pierres.
5:6	Mais ayant vu Yéhoshoua de loin, il accourut et l'adora.
5:7	Et criant à grande voix, il dit : Qu'y a-t-il entre toi et moi, Yéhoshoua, Fils d'Elohîm, du Très-Haut ? Je t’adjure par Elohîm de ne pas me tourmenter.
5:8	Car il lui disait : Sors de cet homme, esprit impur !
5:9	Et il lui demanda : Quel est ton nom ? Et il répondit et dit : Légion<!--Une légion romaine contenait entre 3 000 et 6 000 soldats. C'est d'autant de démons que cet homme était possédé.--> est mon nom, parce que nous sommes beaucoup.
5:10	Et il le suppliait avec insistance de ne pas les envoyer hors de cette contrée.
5:11	Or il y avait là, vers les montagnes, un grand troupeau de pourceaux, paissant.
5:12	Et tous ces démons le suppliaient en disant : Envoie-nous dans les pourceaux, afin que nous entrions en eux.
5:13	Et immédiatement Yéhoshoua le leur permit. Et ces esprits impurs étant sortis, entrèrent dans les pourceaux, et le troupeau se précipita des pentes escarpées dans la mer. Il y en avait environ 2 000, et ils se noyèrent dans la mer.
5:14	Mais ceux qui faisaient paître les pourceaux s'enfuirent et répandirent la nouvelle dans la ville et dans les campagnes. Et ils sortirent pour voir ce qui était arrivé.
5:15	Et ils arrivent auprès de Yéhoshoua et ils voient le démoniaque, celui qui avait eu la légion, assis et vêtu, et dans son bon sens. Et ils furent saisis de crainte.
5:16	Et ceux qui l’avaient vu leur racontèrent ce qui était arrivé au démoniaque et à l’égard des pourceaux.
5:17	Et ils se mirent à le supplier de quitter leurs territoires.
5:18	Et comme il montait dans le bateau, le démoniaque le supplia afin de rester avec lui.
5:19	Mais Yéhoshoua ne le lui permit pas, mais il lui dit : Va dans ta maison, vers les tiens, et raconte-leur les grandes choses que le Seigneur a faites pour toi, et comment il a eu pitié de toi.
5:20	Et il s'en alla donc, et se mit à publier dans la Décapole<!--Voir Mt. 4:25.--> les grandes choses que Yéhoshoua avait faites pour lui. Et tous furent dans l'étonnement.

### La résurrection de la fille de Yaïr (Jaïrus) et la guérison de la femme atteinte d'une perte de sang<!--Mt. 9:18-26 ; Lu. 8:41-56.-->

5:21	Et Yéhoshoua ayant de nouveau traversé en bateau vers l'autre rive, une grande foule se rassembla près de lui et il était près de la mer.
5:22	Et voici qu'un des chefs de la synagogue, du nom de Yaïr, arrive, et le voyant, tombe à ses pieds.
5:23	Et il le suppliait beaucoup, en disant : Ma petite fille est à l'extrémité. Viens et impose-lui les mains pour qu'elle soit sauvée, et elle vivra !
5:24	Et il s'en alla avec lui. Et une grande foule le suivait et on le pressait.
5:25	Et il y avait une femme qui avait une perte de sang depuis 12 ans.
5:26	Et elle avait beaucoup souffert du fait de nombreux médecins. Et elle avait gaspillé<!--Le verbe « gaspiller » vient du grec « dapanao » qui veut dire aussi « éparpiller, consumer, dépenser ». Voir Lu. 15:14 ; Ja. 4:3.--> tout ce qu'elle possédait, sans avoir éprouvé aucun soulagement, mais était allée plutôt en empirant.
5:27	Ayant entendu parler de Yéhoshoua, elle vint dans la foule par derrière et toucha son vêtement.
5:28	Car elle se disait : Même si je ne touche que ses vêtements, je serai sauvée !
5:29	Et immédiatement, la source de son sang se dessécha, et elle sut, dans son corps, qu'elle était guérie de ce fléau.
5:30	Et immédiatement Yéhoshoua, ayant su en lui-même qu'une force était sortie de lui, et s’étant retourné dans la foule, il disait : Qui a touché mes vêtements ?
5:31	Et ses disciples lui dirent : Tu vois que la foule te presse, et tu dis : Qui m'a touché ?
5:32	Et il regardait tout autour pour voir celle qui avait fait cela.
5:33	Mais la femme effrayée et tremblante, sachant ce qui s'était passé en elle, vint et tomba devant lui et lui dit toute la vérité.
5:34	Et il lui dit : Ma fille, ta foi t'a sauvée. Va en paix, et sois guérie de ton fléau.
5:35	Comme il parlait encore, des gens viennent de chez le chef de la synagogue, disant : Ta fille est morte. Pourquoi ennuyer encore le Docteur ?
5:36	Mais aussitôt que Yéhoshoua eut entendu cette parole, il dit au chef de la synagogue : N'aie pas peur, crois seulement.
5:37	Et il ne permit à personne de le suivre, excepté Petros, Yaacov et Yohanan, le frère de Yaacov.
5:38	Et ils arrivent à la maison du chef de la synagogue et il voit du tumulte, des gens qui pleurent et poussent beaucoup de gémissements.
5:39	Et, étant entré, il leur dit : Pourquoi faites-vous tout ce bruit, et pourquoi pleurez-vous ? La petite fille n'est pas morte, mais elle dort.
5:40	Et ils le tournaient en dérision. Mais lui, les ayant tous jetés dehors, prend avec lui le père et la mère de la petite fille, et ceux qui étaient avec lui, et il entre là où la petite fille était couchée.
5:41	Et prenant la main de l'enfant, il lui dit : Talitha koumi, ce qui, interprété, est : Jeune fille, je te dis réveille-toi !
5:42	Et immédiatement la petite fille se leva et se mit à marcher, car elle était âgée de 12 ans. Et ils furent dans un grand étonnement.
5:43	Et il leur ordonna à plusieurs reprises de ne le faire savoir à personne et il dit de lui donner à manger.

## Chapitre 6

### Yéhoshoua à Nazareth

6:1	Et il partit de là et vint dans son pays natal. Et ses disciples le suivent.
6:2	Et le shabbat étant venu, il se mit à enseigner dans la synagogue. Et beaucoup en l'entendant étaient choqués, disant : D'où lui viennent ces choses ? Et quelle est cette sagesse qui lui a été donnée, et comment de tels prodiges se font-ils par ses mains ?
6:3	Celui-ci n'est-il pas le charpentier, le fils de Myriam, le frère de Yaacov, de Yosséi, de Yéhouda, et de Shim’ôn ? Et ses sœurs ne sont-elles pas ici parmi nous ? Et ils étaient scandalisés en lui.
6:4	Mais Yéhoshoua leur dit : Un prophète n'est pas sans honneur, excepté dans son pays natal, parmi ses parents et dans sa famille.
6:5	Et il ne put faire là aucun miracle, si ce n'est qu'il guérit quelques malades en leur imposant les mains.
6:6	Et il s'étonnait de leur incrédulité. Il parcourait les villages d'alentour en enseignant.

### Mission des apôtres<!--Mt. 10:1-42 ; Lu. 9:1-6.-->

6:7	Et il appelle les douze, et commença à les envoyer deux à deux, en leur donnant l'autorité sur les esprits impurs.
6:8	Et il leur ordonna de ne rien prendre pour le chemin, excepté un bâton : ni sac, ni pain, ni monnaie dans leur ceinture,
6:9	mais d'être chaussés de sandales et de ne pas mettre deux tuniques.
6:10	Et il leur disait : Si, quelque part, vous entrez dans une maison, demeurez-y jusqu'à ce que vous sortiez de là.
6:11	Et tous ceux qui ne vous recevront pas et ne vous écouteront pas, en partant de là, secouez la poussière qui sera sous vos pieds en témoignage contre eux. Je vous le dis en vérité que le sort de Sodome et de Gomorrhe sera plus supportable au jour du jugement, que celui de cette ville-là.
6:12	Et ils partirent et prêchèrent pour que les gens se repentent.
6:13	Et ils chassèrent beaucoup de démons et oignirent d'huile<!--Vient du grec « elaion ». Il s'agit de l'huile d'olive. Voir Mt. 25:1-8 ; Hé. 1:9 et Ja. 5:14.--> beaucoup de malades et les guérirent.

### Yohanan le Baptiste décapité<!--Mt. 14:1-14 ; Lu. 9:7-9.-->

6:14	Et le roi Hérode en entendit parler, car son nom était devenu célèbre. Et il disait : Yohanan, celui qui baptisait, est réveillé des morts, c'est pourquoi la puissance de faire des miracles agit puissamment en lui.
6:15	Les autres disaient : C'est Éliyah. Et les autres disaient : C'est un prophète, ou comme l'un des prophètes.
6:16	Mais Hérode en apprenant cela, disait : Ce Yohanan que moi j'ai fait décapiter, c'est lui qui est réveillé des morts !
6:17	En effet, Hérode lui-même ayant envoyé saisir Yohanan, l'avait lié et mis en prison, à cause d'Hérodias, femme de Philippos, son frère, parce qu'il l'avait épousée.
6:18	Car Yohanan disait à Hérode : Il n'est pas légal pour toi d'avoir la femme de ton frère.
6:19	Or Hérodias gardait de la rancune contre lui et voulait le tuer, mais elle ne le pouvait pas, 
6:20	car Hérode craignait Yohanan, sachant que c'était un homme juste et saint et il le préservait. Et il faisait beaucoup de choses après l'avoir entendu et l'écoutait avec plaisir.
6:21	Et un jour opportun arriva lorsque Hérode, pour l'anniversaire de sa naissance, fit un souper pour les grands seigneurs de sa cour, les tribuns<!--Un chiliarque, le commandant d'un millier de soldats, le commandant d'une cohorte romaine (un tribunal militaire), un tribun, tout chef militaire.--> et les principaux de la Galilée.
6:22	Et la fille de cette même Hérodias entra dans la salle, elle dansa et plut à Hérode et à ceux qui étaient à table avec lui. Le roi dit à la jeune fille : Demande-moi ce que tu voudras et je te le donnerai.
6:23	Et il ajouta avec serment : Quoi que ce soit que tu me demandes, je te le donnerai, jusqu'à la moitié de mon royaume.
6:24	Mais étant sortie, elle dit à sa mère : Que demanderai-je ? Et sa mère lui dit : La tête de Yohanan le Baptiste.
6:25	Et étant revenue en toute hâte vers le roi, elle lui fit sa demande, en disant : Je veux que tu me donnes à l'instant sur un plat, la tête de Yohanan le Baptiste.
6:26	Et le roi devint très triste, mais à cause de son serment et des convives, il ne voulut pas refuser.
6:27	Il envoya sur-le-champ un spekoulator<!--Un terme d'origine latine. Le spekoulator était un espion ou un éclaireur sous les empereurs, un serviteur et membre du corps de garde, employé comme messager, veilleur, et comme chargé des exécutions. Ce nom était donné à un serviteur d'Hérode Antipas qui agissait comme un bourreau.--> et lui ordonna qu'on apportât la tête de Yohanan. Et celui-ci s’en alla le décapiter dans la prison, 
6:28	et apporta sa tête dans un plat, et la donna à la jeune fille. Et la jeune fille la donna à sa mère.
6:29	Et les disciples de Yohanan ayant appris cela, vinrent et emportèrent son cadavre, et le mirent dans un sépulcre.
6:30	Et les apôtres se rassemblent auprès de Yéhoshoua, et lui racontèrent tout, et les choses qu'ils avaient faites et celles qu'ils avaient enseignées.
6:31	Et il leur dit : Venez à l'écart dans un lieu désert, et reposez-vous un peu, car il y avait beaucoup d'allants et de venants, et ils n'avaient même pas l'occasion de manger.

### Multiplication des pains<!--Mt. 14:12-21 ; Lu. 9:15-17 ; Jn. 6:1-14.-->

6:32	Et ils s'en allèrent donc dans un bateau, à l'écart, dans un lieu désert.
6:33	Et le peuple vit qu'ils s'en allaient, et beaucoup l'ayant reconnu, s'y précipitèrent à pied de toutes les villes, et y arrivèrent avant eux, et se rassemblèrent auprès de lui.
6:34	Et Yéhoshoua, étant sorti, vit une grande foule, et fut ému de compassion pour elle, parce qu'ils étaient comme des brebis qui n'ont pas de berger, et il se mit à leur enseigner beaucoup de choses.
6:35	Et comme l'heure était déjà avancée, ses disciples s'approchant lui disent : Ce lieu est désert, et l'heure déjà avancée,
6:36	renvoie-les, afin qu'ils s'en aillent dans les campagnes et dans les villages des environs pour s'acheter des pains, car ils n'ont rien à manger.
6:37	Mais répondant, il leur dit : Donnez-leur vous-mêmes à manger. Et ils lui disent : Irions-nous acheter des pains pour 200 deniers, afin de leur donner à manger ?
6:38	Et il leur dit : Combien avez-vous de pains ? Allez et voyez. Et quand ils le surent, ils disent : Cinq, et deux poissons.
6:39	Et il leur ordonna de les faire tous asseoir, groupes d'invités par groupes d'invités, sur l'herbe verte.
6:40	Et ils s'assirent par rangées de 100 et de 50.
6:41	Et prenant les cinq pains et les deux poissons, et levant les yeux vers le ciel, il prononça la prière de bénédiction. Et il rompit les pains et les donna à ses disciples afin qu'ils les mettent devant eux. Il partagea aussi les deux poissons entre tous.
6:42	Et tous mangèrent et furent rassasiés.
6:43	Et l'on emporta douze paniers pleins de morceaux de pains et de ce qui restait des poissons.
6:44	Et ceux qui avaient mangé les pains étaient environ 5 000 hommes.

### Yéhoshoua marche sur la mer<!--Mt. 14:22-33 ; Jn. 6:15-21.-->

6:45	Et immédiatement après, il força ses disciples à monter sur le bateau, et à le devancer sur l'autre bord, vers Bethsaïda, pendant que lui-même renverrait la foule.
6:46	Et l'ayant renvoyée, il s'en alla sur la montagne pour prier.
6:47	Et le soir étant venu, le bateau était au milieu de la mer, mais lui était seul à terre.
6:48	Et il vit qu'ils se tourmentaient à ramer, car le vent leur était contraire. Et vers la quatrième veille de la nuit, il alla vers eux marchant sur la mer, et il voulait les dépasser.
6:49	Mais quand ils le virent marchant sur la mer, ils pensèrent que c'est un fantôme et ils s'écrièrent,
6:50	car ils le voyaient tous, et ils furent troublés. Mais immédiatement il parla avec eux, et leur dit : Ayez confiance, c'est moi, n'ayez pas peur !
6:51	Et il monta vers eux dans le bateau, et le vent cessa. Et ils furent en eux-mêmes excessivement étonnés et remplis d'admiration.
6:52	Car ils n'avaient pas compris au sujet des pains, parce que leur cœur était endurci.

### Yéhoshoua guérit les malades à Génésareth<!--Mt. 14:34-36.-->

6:53	Et après la traversée, ils arrivèrent dans la contrée de Génésareth, où ils abordèrent.
6:54	Et lorsqu'ils furent sortis du bateau, on le reconnut immédiatement,
6:55	et on courut dans toute la région environnante, et on se mit à apporter de tous côtés les malades sur des lits de camp, partout où l'on apprenait qu'il était.
6:56	Et partout où il entrait, dans les villages, dans les villes ou dans les campagnes, on mettait les malades sur les places du marché et on le suppliait afin de toucher seulement le bord de son vêtement. Et tous ceux qui le touchaient étaient sauvés.

## Chapitre 7

### Yéhoshoua condamne les traditions<!--Mt. 15:1-9.-->

7:1	Et les pharisiens et quelques scribes qui étaient venus de Yeroushalaim, se rassemblent auprès de lui.
7:2	Et ayant vu certains de ses disciples mangeant des pains avec des mains impures, c'est-à-dire non lavées, ils les blâmèrent.
7:3	Car les pharisiens et tous les Juifs ne mangent pas s'ils ne se sont pas lavés jusqu'au coude, retenant la tradition des anciens.
7:4	Et au retour de la place du marché<!--Vient du Grec « agora ». C'était une place publique dans les cités grecques qui servait pour le marché, et pour certains actes civils et politiques.-->, s'ils ne se sont pas baptisés<!--Le verbe plonger vient du grec « baptizo » : « plonger », « immerger », « submerger », « purifier en plongeant ou en submergeant », « laver », « rendre pur avec de l'eau », « se baigner » (Mt. 3:6-16, 28:19 ; Ac. 1:5, 2:38 ; 1 Co. 12:13, etc.). Craignant de se retrouver en état d'impureté, beaucoup de Juifs du premier siècle, en particulier les esséniens et les pharisiens, se soumettaient quotidiennement à de nombreux rituels de purification avec de l'eau. À titre d'exemple, les jarres de Qanah (Cana) étaient utilisées à cet effet (Jn. 2:6).-->, ils ne mangent pas. Il y a aussi beaucoup d'autres choses qu'ils ont reçues pour les retenir : les lavages des coupes, et des cruches, et des vases en bronze et des lits.
7:5	Ensuite les pharisiens et les scribes lui demandèrent : En raison de quoi tes disciples ne marchent-ils pas selon la tradition des anciens, mais mangent-ils leur pain avec des mains non lavées ?
7:6	Mais lui, répondant, leur dit : Yesha`yah a bien prophétisé sur vous, hypocrites, comme il est écrit : Ce peuple m'honore des lèvres, mais leur cœur est distant, loin de moi<!--Es. 29:13.-->.
7:7	Mais c'est en vain qu'ils m'adorent, en enseignant des doctrines qui sont des commandements humains.
7:8	Car, laissant de côté le commandement d'Elohîm, vous retenez la tradition des humains : le lavage des cruches et des coupes, et vous faites beaucoup d'autres choses semblables.
7:9	Il leur disait aussi : Vous rejetez bien le commandement d'Elohîm, afin de garder votre tradition.
7:10	Car Moshé a dit : Honore ton père et ta mère et : Celui qui maudit son père ou sa mère finit à la mort.
7:11	Mais vous, vous dites : Si un homme dit à son père ou à sa mère : Tout ce dont tu pourrais être assisté par moi est corban, c'est-à-dire offrande...
7:12	Et vous ne lui laissez plus rien faire pour son père ou pour sa mère,
7:13	annulant la parole d'Elohîm par votre tradition que vous avez établie. Et vous faites encore beaucoup d'autres choses semblables.

### Le cœur humain<!--Mt. 15:10-20.-->

7:14	Et ayant appelé la foule, il leur dit : Écoutez-moi vous tous, et entendez !
7:15	Il n'y a rien d'extérieur à l'être humain qui puisse le rendre impur en entrant en lui. Mais ce qui sort de l'être humain, c'est ce qui le rend impur.
7:16	Si quelqu'un a des oreilles pour entendre, qu'il entende !
7:17	Et quand il fut entré dans la maison, loin de la foule, ses disciples l'interrogèrent sur cette parabole.
7:18	Et il leur dit : Vous aussi, êtes-vous sans intelligence ? Ne comprenez-vous pas que rien d'extérieur, en entrant dans l'être humain ne peut le rendre impur,
7:19	parce que cela n’entre pas dans son cœur, mais dans son ventre, et sort dans le lieu privé, ce qui purifie tous les aliments ?
7:20	Mais il leur disait : Ce qui sort de l'être humain, c'est ce qui rend l'être humain impur.
7:21	Car c'est du dedans, du cœur des humains, que sortent les mauvaises pensées, les adultères, les relations sexuelles illicites<!--Voir Mt. 5:32.-->, les meurtres,
7:22	les vols, les cupidités, les méchancetés, la tromperie, la luxure sans bride<!--Comportement de quelqu'un qui se livre sans retenue aux plaisirs sexuels. Cette expression vient du grec « aselgeia » qui signifie aussi « excès », « lasciveté », « libertinage », « impudence », « insolence ». Voir Ro. 13:13 ; 2 Co. 12:21 ; Ga. 5:19 ; Ep. 4:19 ; 1 Pi. 4:3 ; 2 Pi. 2:7,18 et Jud. 1:4.-->, l'œil méchant, le blasphème, l'orgueil, la folie.
7:23	Toutes ces choses mauvaises sortent du dedans et rendent l'être humain impur.

### Yéhoshoua et la femme syro-phénicienne<!--Mt. 15:21-28.-->

7:24	Et étant parti de là, il s'en alla dans le territoire de Tyr et de Sidon. Et il entra dans une maison, désirant que personne ne le sache, mais il ne put rester caché.
7:25	Car une femme, dont la petite fille était possédée d'un esprit impur, ayant entendu parler de lui, vint et tomba à ses pieds.
7:26	Or cette femme était grecque, syro-phénicienne d'origine. Elle le supplia de chasser le démon hors de sa fille.
7:27	Mais Yéhoshoua lui dit : Laisse premièrement les enfants se rassasier, car il n'est pas raisonnable de prendre le pain des enfants, et de le jeter aux petits chiens.
7:28	Mais elle répondit et lui dit : Oui, Seigneur ! Car même les petits chiens mangent sous la table les miettes des petits enfants.
7:29	Et il lui dit : À cause de cette parole, va, le démon est sorti de ta fille.
7:30	Et s'en étant allée dans sa maison, elle trouva le démon sorti, et sa fille couchée sur le lit.

### Yéhoshoua guérit un sourd-muet<!--Mt. 15:29-31.-->

7:31	Et étant de nouveau sorti des territoires de Tyr et de Sidon, il vint vers la Mer de Galilée, par le milieu des territoires de la Décapole.
7:32	Et on lui amène un sourd qui avait la parole empêchée, et on le supplie pour qu’il lui impose la main.
7:33	Et le prenant hors de la foule, à part, il lui mit ses doigts dans les oreilles et, avec de la salive, il lui toucha la langue.
7:34	Et, levant les yeux vers le ciel, il soupira et lui dit : Éphphatha ! C'est-à-dire : Ouvre-toi !
7:35	Et immédiatement ses oreilles s'ouvrirent, et le lien de sa langue se délia, et il parla aisément.
7:36	Et il leur ordonna de ne le dire à personne, mais plus il le leur ordonnait, plus ils le publiaient.
7:37	Et ils étaient extrêmement choqués, disant : Il fait tout à merveille : il fait même entendre les sourds et parler les muets !

## Chapitre 8

### Seconde multiplication des pains<!--Mt. 15:32-39.-->

8:1	En ces jours-là, une foule très nombreuse s'étant de nouveau réunie et n'ayant pas de quoi manger, Yéhoshoua appelant à lui ses disciples, leur dit :
8:2	Je suis ému de compassion envers cette foule, car il y a déjà trois jours qu'ils restent auprès de moi, et ils n'ont rien à manger.
8:3	Et si je les renvoie dans leur maison à jeun, ils s'épuiseront en route, car quelques-uns d'eux sont venus de loin.
8:4	Et ses disciples lui répondirent : Comment pourra-t-on les rassasier de pains, ici, dans une région inhabitée ?
8:5	Et il leur demanda : Combien avez-vous de pains ? Et ils dirent : 7.
8:6	Et il ordonna à la foule de s'asseoir par terre et, prenant les 7 pains, il rendit grâce, les rompit et les donna à ses disciples pour qu'ils les posent devant. Et ils les posèrent devant la foule.
8:7	Ils avaient aussi quelques petits poissons. Et après avoir prononcé la prière de bénédiction, il ordonna de les mettre aussi devant eux.
8:8	Et ils mangèrent et furent rassasiés, et l'on remporta 7 corbeilles pleines des morceaux qui restaient.
8:9	Or ceux qui avaient mangé étaient environ 4 000. Et il les renvoya.

### Mise en garde contre l'enseignement des pharisiens<!--Mt. 16:1-12.-->

8:10	Et immédiatement, montant dans le bateau avec ses disciples, il vint dans le territoire de Dalmanoutha.
8:11	Et les pharisiens sortirent et commencèrent à discuter avec lui, lui demandant, pour le tenter, un signe venant du ciel.
8:12	Et soupirant profondément en son esprit, il dit : Pourquoi cette génération demande-t-elle un signe ? Amen, je vous le dis : si un signe sera donné à cette génération...
8:13	Et les laissant, il monta dans le bateau pour passer à l'autre rivage.
8:14	Et ils avaient oublié de prendre des pains et ils n'en avaient qu'un seul avec eux dans le bateau.
8:15	Et il leur donna cet ordre en disant : Faites attention ! Discernez depuis le levain des pharisiens et du levain d'Hérode.
8:16	Et ils raisonnaient entre eux, disant : C'est parce que nous n'avons pas de pains.
8:17	Et Yéhoshoua, le sachant, leur dit : Pourquoi raisonnez-vous sur ce que vous n'avez pas de pains ? N'entendez-vous pas encore, et ne comprenez-vous pas ? Avez-vous encore votre cœur endurci ? 
8:18	Ayant des yeux, ne voyez-vous pas ? Et ayant des oreilles, n'entendez-vous pas ? Et ne vous rappelez-vous pas,
8:19	quand j'ai rompu les 5 pains pour les 5 000, combien de paniers pleins de morceaux avez-vous emportés ? Ils lui dirent : 12.
8:20	Et quand j'ai rompu les 7 pains pour les 4 000, combien de corbeilles pleines de morceaux avez-vous emportées ? Et ils dirent : 7.
8:21	Et il leur dit : Comment ne comprenez-vous pas ?

### Yéhoshoua guérit un aveugle

8:22	Et il vient à Bethsaïda. Et on lui amène un aveugle, et on le supplie pour qu'il le touche.
8:23	Et prenant la main de l'aveugle, et le conduisit hors du village. Et après lui avoir mis de la salive sur les yeux et lui avoir imposé les mains, il lui demanda s'il voyait quelque chose.
8:24	Et ayant recouvré la vue, il dit : Je vois des humains, parce que j’aperçois comme des arbres, et ils marchent.
8:25	Alors il lui imposa de nouveau les mains sur les yeux et le fit regarder. Et il fut restauré à l'état initial et les voyait tous de loin et clairement.
8:26	Et il le renvoya dans sa maison, en disant : N'entre pas dans le village et ne le dis à personne dans le village.

### Petros (Pierre) reconnaît Yéhoshoua comme étant le Mashiah<!--Mt. 16:13-16 ; Lu. 9:18-21 ; Jn. 6:67-71.-->

8:27	Et Yéhoshoua s'en alla avec ses disciples dans les villages de Césarée de Philippos, et sur le chemin, il interrogea ses disciples en leur disant : Qui dit-on que je suis ?
8:28	Et ils répondirent : Yohanan le Baptiste ; les autres, Éliyah ; et les autres, l'un des prophètes.
8:29	Et il leur dit : Mais vous, qui dites-vous que je suis ? Petros répondant, lui dit : Tu es le Mashiah.
8:30	Et il les réprimanda d'une manière tranchante afin qu'ils ne parlent de lui à personne.
8:31	Et il commença à leur enseigner qu'il fallait que le Fils d'humain souffre beaucoup, et qu'il soit rejeté par les anciens, par les principaux prêtres et par les scribes, et qu'il soit mis à mort, et qu'il ressuscite trois jours après.
8:32	Et il tenait ce discours ouvertement. Et Petros l'ayant pris à part, se mit à le réprimander d'une manière tranchante.
8:33	Mais lui, se retournant et regardant ses disciples, réprimanda d'une manière tranchante Petros en disant : Va-t'en derrière moi, Satan ! Car tu ne penses pas aux choses d'Elohîm, mais à celles des humains.

### Le renoncement à soi-même<!--Mt. 16:24-28 ; Lu. 9:23-26.-->

8:34	Et ayant appelé la foule et ses disciples, il leur dit : Si quelqu'un veut venir après moi, qu'il renonce à lui-même, qu'il se charge de sa croix et qu'il me suive.
8:35	Car celui qui veut sauver son âme, la perdra, mais quiconque perdra son âme à cause de moi et de l'Évangile, celui-là la sauvera.
8:36	Car que profiterait-il à un être humain s'il gagnait le monde entier, et que son âme subissait un dommage ?
8:37	Ou, que donnera l'être humain en échange de son âme ?
8:38	Car celui qui aura eu honte<!--Voir Ro. 1:16 ; 2 Ti. 1:8,12,16.--> de moi et de mes paroles dans cette génération adultère<!--Littéralement : « une femme adultère ».--> et pécheresse, le Fils d'humain aura aussi honte de lui, quand il viendra environné de la gloire de son Père avec les saints anges.

## Chapitre 9

### La transfiguration de Yéhoshoua ha Mashiah (Jésus le Christ)<!--Mt. 17:1-9 ; Lu. 9:27-36.-->

9:1	Il leur disait aussi : Amen, je vous dis qu'il y a quelques-uns de ceux qui sont ici présents, qui ne goûteront jamais la mort, non, jusqu'à ce qu'ils aient vu le Royaume d'Elohîm venu avec puissance<!--Voir commentaire Mt. 16:28.-->.
9:2	Et six jours après, Yéhoshoua prend avec lui Petros, Yaacov et Yohanan et les mène seuls, à l'écart, sur une haute montagne. Il fut transfiguré devant eux,
9:3	et ses vêtements devinrent resplendissants et blancs comme de la neige, tels qu'il n'est pas de foulon<!--Ouvrier qui enlève l'huile et la graisse des étoffes non dégrossies et les apprête par des procédés de foulage. Celui qui nettoie des habits sales.--> sur la Terre qui puisse blanchir ainsi.
9:4	Et Éliyah avec Moshé leur apparurent et ils étaient en train de parler avec Yéhoshoua.
9:5	Et Petros prenant la parole, dit à Yéhoshoua : Rabbi, il est bon que nous soyons ici. Faisons donc trois tabernacles, un pour toi, un pour Moshé, et un pour Éliyah.
9:6	Car il ne savait pas quoi dire, car ils étaient extrêmement effrayés.
9:7	Et une nuée vint les couvrir de son ombre et il y eut une voix venant de la nuée : Celui-ci est mon Fils bien-aimé, écoutez-le !
9:8	Et soudainement, regardant tout autour, ils ne virent plus personne, si ce n'est Yéhoshoua seul avec eux.
9:9	Mais en descendant de la montagne, il leur ordonna de ne raconter à personne ce qu'ils avaient vu, si ce n’est quand le Fils d'humain se serait relevé d’entre les morts.
9:10	Et ils retinrent cette parole en eux-mêmes, en discutant entre eux : Qu’est-ce que se relever d’entre les morts ?
9:11	Et ils l’interrogeaient, en disant : Pourquoi les scribes disent-ils qu'il faut qu'Éliyah vienne premièrement ?
9:12	Il répondit et leur dit : En effet, Éliyah étant venu premièrement, restaure à l'état initial toutes choses. Et comme il est écrit du Fils d'humain, il faut qu'il souffre beaucoup, et qu'il soit traité avec mépris.
9:13	Mais je vous dis qu'Éliyah est venu en effet, et ils l'ont traité comme ils ont voulu, selon ce qui est écrit de lui.

### L'incrédulité des disciples<!--Mt. 17:14-21 ; Lu. 9:37-43.-->

9:14	Et lorsqu'il fut arrivé près des disciples, il vit une grande foule autour d'eux, et des scribes disputant avec eux.
9:15	Et immédiatement toute la foule fut frappée de stupeur en le voyant et, courant vers lui, elle se mit à le saluer.
9:16	Et il interrogea les scribes, en disant : De quoi disputez-vous avec eux ?
9:17	Et quelqu'un de la foule répondant, dit : Docteur, je t'ai amené mon fils qui est possédé d'un esprit muet.
9:18	Et en quelque lieu qu'il le saisisse, il le jette par terre, l'enfant écume, grince des dents, et devient tout raide. J'ai dit à tes disciples de le chasser, mais ils n’en ont pas été capables.
9:19	Mais lui, leur répondant, dit : Ô génération incrédule ! Jusqu'à quand serai-je avec vous ? Jusqu'à quand vous supporterai-je ? Amenez-le-moi !
9:20	Ils le lui amenèrent. Et en le voyant, immédiatement l'esprit le fit se convulser ; il tomba par terre, et se roulait en écumant.
9:21	Et il demanda au père de l'enfant : Depuis combien de temps cela lui arrive-t-il ? Et il dit : Depuis son enfance.
9:22	Et souvent il l'a jeté dans le feu et dans l'eau pour le faire périr. Mais si tu peux quelque chose, secours-nous, aie compassion de nous.
9:23	Mais Yéhoshoua lui dit : Si tu peux le croire, toutes choses sont possibles pour celui qui croit.
9:24	Et immédiatement le père de l'enfant s'écriant avec larmes, dit : Je crois, Seigneur ! Secours mon incrédulité !
9:25	Mais quand Yéhoshoua vit la foule accourir ensemble, il réprimanda d'une manière tranchante l'esprit impur, et lui dit : Esprit muet et sourd ! Moi, je t'ordonne, sors de lui et n'y rentre plus !
9:26	Et il sortit en poussant des cris et en le faisant beaucoup se convulser. Il devint comme mort, de sorte que beaucoup disaient qu'il était mort.
9:27	Mais Yéhoshoua, l'ayant pris par la main, le réveilla. Et il se tint debout.
9:28	Et lorsqu'il fut entré à la maison, ses disciples lui demandèrent en particulier : Pourquoi n'avons-nous pas pu le chasser ?
9:29	Et il leur dit : Cette race-là ne peut sortir que par la prière et le jeûne.

### Yéhoshoua annonce sa mort et sa résurrection<!--Mt. 17:22-23 ; Lu. 9:44-45.-->

9:30	Et étant partis de là, ils traversèrent la Galilée. Et il ne voulait pas qu'on le sache.
9:31	Car il enseignait ses disciples, et il leur disait : Le Fils d'humain est livré entre les mains des humains, et ils le feront mourir, mais après qu'il aura été mis à mort, il ressuscitera le troisième jour.
9:32	Mais ils ne comprenaient pas cette parole, et ils craignaient de l'interroger.

### L'humilité, le secret de la vraie grandeur<!--Mt. 18:1-6 ; Lu. 9:46-48.-->

9:33	Et après ces choses il vint à Capernaüm. Et quand il fut arrivé dans la maison, il leur demanda : Sur quoi raisonniez-vous en route ?
9:34	Mais ils gardèrent le silence, car ils avaient discuté entre eux, en route, qui serait le plus grand.
9:35	Et s'étant assis, il appela à lui les douze, et leur dit : Si quelqu'un veut être le premier parmi vous, il sera le dernier de tous, et le serviteur de tous.
9:36	Et ayant pris un enfant, il le mit au milieu d'eux, et après l'avoir pris entre ses bras, il leur dit :
9:37	Quiconque recevra en mon nom un de ces enfants, me reçoit, et quiconque me recevra, ce n'est pas moi qu'il reçoit, mais celui qui m'a envoyé.

### Yéhoshoua condamne le sectarisme<!--Lu. 9:49-50.-->

9:38	Mais Yohanan lui répondit en disant : Docteur, nous avons vu quelqu'un qui chasse les démons en ton nom, qui ne nous suit pas, et nous l'en avons empêché, parce qu'il ne nous suit pas.
9:39	Mais Yéhoshoua leur dit : Ne l'en empêchez pas, parce qu'il n'est personne qui, faisant un miracle en mon nom, puisse promptement après parler mal de moi.
9:40	Car qui n'est pas contre nous est en notre faveur.
9:41	Et quiconque vous aura donné à boire une coupe d'eau en mon nom, parce que vous êtes au Mashiah, amen, je vous le dis, il ne perdra jamais sa récompense.

### Les scandales et les occasions de chute

9:42	Mais quiconque scandalisera un de ces petits qui croient en moi, il vaudrait mieux pour lui qu'on lui mette une pierre de moulin autour de son cou, et qu'on le jette dans la mer.
9:43	Et si ta main te scandalise, coupe-la. Mieux vaut pour toi entrer estropié dans la vie que d'avoir les deux mains et d'aller dans la géhenne, dans le feu qui ne s'éteint pas,
9:44	là où leur ver ne meurt pas, et où le feu ne s'éteint pas.
9:45	Et si ton pied te scandalise, coupe-le. Mieux vaut pour toi entrer boiteux dans la vie que d'avoir les deux pieds et d'être jeté dans la géhenne, dans le feu qui ne s'éteint pas,
9:46	là où leur ver ne meurt pas, et où le feu ne s'éteint pas.
9:47	Et si ton œil te scandalise, arrache-le. Mieux vaut pour toi entrer dans le Royaume d'Elohîm n'ayant qu'un œil, que d'avoir les deux yeux, et d'être jeté dans la géhenne du feu,
9:48	là où leur ver ne meurt pas, et où le feu ne s'éteint pas.
9:49	Car chacun sera salé de feu et tout sacrifice sera salé de sel<!--Voir Lé. 2:13-16 ; Mt. 5:13.-->.
9:50	Le sel est une bonne chose, mais si le sel devient sans saveur, avec quoi lui rendra-t-on sa saveur ? Ayez du sel en vous-mêmes et soyez en paix les uns avec les autres.

## Chapitre 10

### Enseignement de Yéhoshoua sur le mariage et le divorce<!--Mt. 5:31-32, 19:1-9 ; Lu. 16:18 ; 1 Co. 7:10-16 ; Ro. 7:1-3.-->

10:1	Et s'étant levé, il se rend de là vers les territoires de la Judée, par l’autre rive du Yarden. Et de nouveau des foules s'assemblent auprès de lui, et de nouveau, selon sa coutume, il les enseignait.
10:2	Et les pharisiens s'étant approchés, lui demandèrent, pour le tenter, s'il est légal pour un homme de répudier une femme.
10:3	Mais répondant, il leur dit : Que vous a ordonné Moshé ?
10:4	Et ils dirent : Moshé a permis d'écrire une lettre de répudiation et de répudier<!--De. 24:1.-->.
10:5	Et Yéhoshoua répondant, leur dit : C'est à cause de la dureté<!--De. 29:18 ; Jé. 3:17, 7:24, 9:14, 11:8, 13:10, 16:12, 18:12, 23:17 ; Ps. 81:13.--> de votre cœur qu'il a écrit pour vous ce commandement.
10:6	Mais, dès le commencement de la création, Elohîm les fit mâle et femelle.
10:7	À cause de cela, l'homme quittera son père et sa mère, et se joindra à sa femme,
10:8	et les deux ne seront qu'une seule chair. Ainsi ils ne sont plus deux, mais une seule chair.
10:9	Que l'homme donc ne sépare pas ce qu'Elohîm a mis ensemble sous un joug<!--Voir commentaire Mt. 19:6.-->.
10:10	Et dans la maison, ses disciples l'interrogèrent de nouveau sur le même sujet.
10:11	Et il leur dit : Celui qui aura répudié sa femme, et en aura épousé une autre commet un adultère contre elle.
10:12	Et si une femme répudie son mari et en épouse un autre, elle commet l’adultère.

### Yéhoshoua et les enfants<!--Mt. 19:13-15 ; Lu. 18:15-17.-->

10:13	Et on lui apporta des enfants afin qu'il les touche. Mais les disciples réprimandèrent d'une manière tranchante ceux qui les apportent.
10:14	Mais Yéhoshoua, voyant cela, fut indigné et leur dit : Laissez venir à moi les enfants et ne les en empêchez pas, car le Royaume d'Elohîm appartient à ceux qui leur ressemblent.
10:15	Amen, je vous le dis, quiconque ne recevra pas comme un enfant le Royaume d'Elohîm, il n'y entrera jamais.
10:16	Et les ayant pris dans ses bras, il les bénit en posant les mains sur eux.

### Le jeune homme riche<!--Mt. 19:16-26 ; Lu. 18:18-27.-->

10:17	Et en sortant sur la route, un homme accourut et tomba à genoux devant lui, et lui fit cette demande : Bon Docteur, que dois-je faire pour hériter la vie éternelle ?
10:18	Et Yéhoshoua lui dit : Pourquoi m'appelles-tu bon ? Nul n'est bon, excepté un seul : Elohîm<!--La même histoire est racontée en Lu. 18:18 qui précise que c'était un chef qui avait interrogé Yéhoshoua. La réponse du Seigneur est ironique. Yéhoshoua aurait aussi pu lui poser la question comme suit : « Puisque tu penses que je ne suis qu'un simple humain, pourquoi m'appelles-tu bon ? ».-->.
10:19	Tu connais les commandements : Ne commets pas d'adultère, n'assassine pas, ne vole pas, ne dis pas de faux témoignage, ne fais aucun tort à personne, honore ton père et ta mère.
10:20	Et répondant, il lui dit : Docteur, j'ai gardé toutes ces choses dès ma jeunesse.
10:21	Mais Yéhoshoua, l'ayant regardé, l'aima, et lui dit : Il te manque une chose : Va et vends tout ce que tu as, et donne-le aux pauvres, et tu auras un trésor dans le ciel. Et viens et suis-moi en te chargeant de la croix.
10:22	Mais, affligé de cette parole, il s'en alla tout triste parce qu'il avait de nombreuses propriétés.

### Rien n'est impossible à Elohîm

10:23	Et Yéhoshoua, ayant regardé autour de lui, dit à ses disciples : Qu'il est difficile à ceux qui ont des richesses d'entrer dans le Royaume d'Elohîm.
10:24	Et les disciples furent effrayés par ses paroles. Mais Yéhoshoua répondant encore leur dit : Enfants, qu'il est difficile à ceux qui se confient dans les richesses d'entrer dans le Royaume d'Elohîm !
10:25	Il est plus facile à un chameau de passer par le trou d'une aiguille<!--Voir commentaire Mt. 19:24.-->, qu'à un riche d'entrer dans le Royaume d'Elohîm.
10:26	Mais ils furent encore plus choqués, et ils se dirent les uns les autres : Et qui peut être sauvé ?
10:27	Mais Yéhoshoua les ayant regardés, leur dit : Pour les humains cela est impossible, mais non pas pour Elohîm, car tout est possible pour Elohîm.

### La récompense du disciple

10:28	Et Petros se mit à lui dire : Voici, nous avons tout quitté et nous t'avons suivi.
10:29	Et répondant, Yéhoshoua dit : Amen, je vous le dis, il n'est personne qui, ayant quitté à cause de moi et de l'Évangile, maison, ou frères, ou sœurs, ou père, ou mère, ou femme, ou enfants, ou terres,
10:30	ne reçoive maintenant, en ce temps-ci, au centuple, des maisons, des frères, des sœurs, des mères, des enfants, et des terres, avec des persécutions et, dans l'âge à venir, la vie éternelle.
10:31	Mais beaucoup des premiers seront les derniers et beaucoup des derniers seront les premiers.

### Yéhoshoua annonce sa mort et sa résurrection<!--Mt. 20:17-19 ; Lu. 18:31-34.-->

10:32	Or ils étaient en route, montant à Yeroushalaim, et Yéhoshoua allait devant eux. Et ils étaient effrayés et, en le suivant, ils avaient peur. Et ayant de nouveau pris à l'écart les douze, il commença à leur déclarer ce qui était sur le point de lui arriver :
10:33	Voici que nous montons à Yeroushalaim et le Fils d'humain sera livré aux principaux prêtres et aux scribes. Ils le condamneront à mort et le livreront aux nations
10:34	et ils se joueront de lui, et ils le châtieront avec un fouet, et ils cracheront sur lui et le feront mourir, et il ressuscitera le troisième jour.

### Yéhoshoua répond à la question de Yaacov et Yohanan (Jacques et Jean)

10:35	Et Yaacov et Yohanan, les fils de Zabdi, s'approchent de lui et lui disent : Docteur, nous voulons que tu fasses pour nous ce que nous te demanderons.
10:36	Et il leur dit : Que voulez-vous que je fasse pour vous ?
10:37	Et ils lui dirent : Accorde-nous que nous soyons assis l'un à partir de tes droites et l'autre à partir de tes gauches, dans ta gloire.
10:38	Mais Yéhoshoua leur dit : Vous ne savez pas ce que vous demandez. Pouvez-vous boire la coupe que moi je bois, et être baptisés du baptême dont moi je suis baptisé ?
10:39	Et ils lui dirent : Nous le pouvons. Et Yéhoshoua leur dit : En effet, vous boirez la coupe que je bois, et vous serez baptisés du baptême dont je suis baptisé.
10:40	Mais d'être assis à partir de mes droites et à partir de mes gauches, ce n’est pas à moi de le donner, mais ce sera pour ceux à qui cela a été préparé<!--Yéhoshoua est celui qui a préparé pour nous une place dans son Royaume céleste. Voir Jn. 14:3.-->.
10:41	Et les dix autres, ayant entendu cela, commencèrent à s'indigner contre Yaacov et Yohanan.
10:42	Mais Yéhoshoua les appela et leur dit : Vous savez que ceux qui pensent être les chefs<!--Être un chef, diriger, régner. Yéhoshoua viendra pour être le chef des nations. Voir Ro. 15:12.--> des nations dominent sur elles en maîtres et que les grands exercent leur autorité sur elles.
10:43	Il n'en sera pas de même parmi vous, mais si quelqu'un veut devenir grand parmi vous, il sera votre serviteur,
10:44	et quiconque veut devenir le premier parmi vous sera l'esclave de tous.
10:45	Car le Fils d'humain est venu, non pour être servi, mais pour servir et donner sa vie en rançon pour beaucoup.

### Yéhoshoua guérit l'aveugle Bartimaios (Bartimée)<!--Mt. 20:29-34 ; Lu. 18:35-43.-->

10:46	Et ils arrivent à Yeriycho. Et lorsqu'il sort de Yeriycho avec ses disciples et une grande foule, le fils de Timaios, Bartimaios l'aveugle, était assis au bord de la route, demandant l'aumône.
10:47	Et ayant entendu que c'était Yéhoshoua le Nazaréen, il se mit à crier et à dire : Yéhoshoua, Fils de David, aie pitié de moi !
10:48	Et beaucoup le réprimandaient d'une manière tranchante pour le faire taire, mais il criait beaucoup plus fort : Fils de David, aie pitié de moi !
10:49	Et Yéhoshoua s'étant arrêté dit : Appelez-le ! Ils appelèrent donc l'aveugle en lui disant : Prends courage ! Réveille-toi ! Il t'appelle.
10:50	Et jetant son manteau, il se leva et vint vers Yéhoshoua.
10:51	Et Yéhoshoua, répondant, lui dit : Que veux-tu que je fasse pour toi ? Et l'aveugle lui dit : Rhabboni, que je recouvre la vue.
10:52	Et Yéhoshoua lui dit : Va, ta foi t'a sauvé. Et immédiatement il recouvra la vue, et suivit Yéhoshoua dans le chemin.

## Chapitre 11

### Yéhoshoua ha Mashiah (Jésus le Christ) entre dans Yeroushalaim (Jérusalem)<!--Za. 9:9 ; Mt. 21:1-11 ; Lu. 19:28-40 ; Jn. 12:12-19.-->

11:1	Et quand ils approchent de Yeroushalaim, vers Bethphagé et Béthanie, près du Mont des Oliviers, il envoie deux de ses disciples,
11:2	et leur dit : Allez au village qui est devant vous. Dès que vous y serez entrés, vous trouverez un ânon attaché, sur lequel aucun être humain ne s'est encore assis. Détachez-le, et amenez-le.
11:3	Et si quelqu'un vous dit : Pourquoi faites-vous cela ? Dites que le Seigneur en a besoin, et immédiatement, il le laissera venir ici.
11:4	Et ils partirent et trouvèrent l'ânon qui était attaché dehors, près d'une porte, au contour du chemin, et ils le détachèrent.
11:5	Et quelques-uns de ceux qui étaient là leur dirent : Que faites-vous ? Vous détachez cet ânon ?
11:6	Mais ils leur répondirent comme Yéhoshoua l'avait ordonné, et on les laissa faire.
11:7	Et ils amenèrent donc l'ânon à Yéhoshoua, sur lequel ils jetèrent leurs vêtements, et il s'assit dessus.
11:8	Or beaucoup étendaient leurs vêtements sur la route et d’autres coupaient des branches d’arbres, et en couvraient la route.
11:9	Et ceux qui allaient devant, et ceux qui suivaient, criaient, en disant : Hosanna ! Béni soit celui qui vient au nom du Seigneur !
11:10	Béni soit le royaume de David, notre père, qui vient au nom du Seigneur ! Hosanna dans les lieux très hauts !
11:11	Et Yéhoshoua entra ainsi à Yeroushalaim, dans le temple. Et après avoir regardé de tous côtés, comme il était déjà tard, il sortit pour aller à Béthanie avec les douze.

### Le figuier stérile<!--Mt. 21:18-22.-->

11:12	Et le lendemain, après qu'ils furent sortis de Béthanie, Yéhoshoua eut faim.
11:13	Et voyant de loin un figuier qui avait des feuilles, il alla voir s'il y trouverait quelque chose, mais s'en étant approché, il ne trouva que des feuilles, car ce n'était pas la saison des figues.
11:14	Yéhoshoua répondit et lui dit : Que personne ne mange de ton fruit, à jamais ! Et les disciples l'entendirent.

### Yéhoshoua chasse les marchands du temple<!--Mt. 21:12-13 ; Lu. 19:45-46 ; Jn. 2:13-17.-->

11:15	Et ils arrivent donc à Yeroushalaim, et Yéhoshoua entra dans le temple. Il commença par chasser dehors ceux qui vendaient et achetaient dans le temple, et il renversa les tables des changeurs et les sièges de ceux qui vendaient des colombes.
11:16	Et il ne laissait personne porter aucun objet à travers le temple.
11:17	Et il enseignait en leur disant : N'est-il pas écrit : Ma maison sera appelée une maison de prière pour toutes les nations ? Mais vous, vous en avez fait une caverne de brigands<!--Jé. 7:11.-->.

### Les scribes et les prêtres complotent contre Yéhoshoua

11:18	Et les scribes et les principaux prêtres l'ayant entendu, cherchaient comment le détruire, car ils le craignaient, parce que toute la foule était choquée par sa doctrine.
11:19	Et quand le soir fut venu, il sortit de la ville.

### La prière avec foi<!--1 Jn. 5:14-15.-->

11:20	Et le matin, comme ils passaient, ils virent le figuier devenu sec dès les racines.
11:21	Et Petros se souvenant, lui dit : Rabbi, voici, le figuier que tu as maudit a séché.
11:22	Et Yéhoshoua répondant, leur dit : Ayez foi en Elohîm.
11:23	Amen, car je vous dis que quiconque dira à cette montagne : Sois enlevée et jetée dans la mer, et qui n'aura pas douté en son cœur, mais qui croit que ce qu'il dit arrivera, ce qu'il dit se fera pour lui.
11:24	C'est pourquoi je vous dis : Tout ce que vous demanderez en priant, croyez que vous le recevez, et cela sera pour vous.

### Le pardon

11:25	Et quand vous êtes debout pour offrir des prières, si vous avez quelque chose contre quelqu'un, remettez-le-lui, afin que votre Père qui est dans les cieux vous remette aussi vos fautes.
11:26	Mais si vous ne le lui remettez pas, votre Père qui est dans les cieux ne vous remettra pas aussi vos fautes.

### L'autorité de Yéhoshoua et celle de Yohanan le Baptiste<!--Mt. 21:23-27 ; Lu. 20:1-8.-->

11:27	Et ils viennent de nouveau à Yeroushalaim. Et comme il marchait dans le temple, les principaux prêtres, les scribes et les anciens viennent à lui,
11:28	et lui disent : Par quelle autorité fais-tu ces choses et qui t'a donné l'autorité de les faire ?
11:29	Mais Yéhoshoua répondant, leur dit : Je vous interrogerai, moi aussi sur une parole, et répondez-moi. Et je vous dirai par quelle autorité je fais ces choses.
11:30	Le baptême de Yohanan étail-il issu du ciel ou issu des humains ? Répondez-moi !
11:31	Et ils raisonnaient entre eux, en disant : Si nous disons : Issu du ciel, il nous dira : En raison de quoi donc ne l'avez-vous pas cru ?
11:32	Et si nous disons : Issu des humains, nous avons à craindre le peuple, car tous considéraient que Yohanan était vraiment un prophète.
11:33	Et répondant à Yéhoshoua, ils disent : Nous ne savons pas. Et Yéhoshoua répondant leur dit : Moi non plus je ne vous dis pas par quelle autorité je fais ces choses.

## Chapitre 12

### Parabole des vignerons<!--Es. 5:1-7 ; Mt. 21:33-46 ; Lu. 20:9-18.-->

12:1	Et il se mit à leur parler en paraboles : Un homme planta une vigne, et mit autour une clôture, et y creusa une fosse pour un pressoir et bâtit une tour. Et il la laissa en location à des vignerons et partit pour un pays lointain.
12:2	Et au temps favorable et opportun, il envoya un esclave vers les vignerons pour recevoir des vignerons le fruit de la vigne.
12:3	Mais s'étant saisi de lui, ils le battirent et le renvoyèrent à vide.
12:4	Et il envoya de nouveau un autre esclave vers eux. Ils lui jetèrent des pierres, le frappèrent à la tête et le renvoyèrent après l'avoir déshonoré.
12:5	Et il en envoya de nouveau un autre qu'ils tuèrent en effet, et beaucoup d'autres, et ils battirent les uns, et tuèrent les autres.
12:6	Ayant donc encore un fils unique, son bien-aimé, il l'envoya, lui aussi, vers eux, le dernier, en se disant : Ils respecteront en effet mon fils.
12:7	Mais ces vignerons se dirent entre eux : Puisque celui-ci est l'héritier, venez, tuons-le, et l'héritage sera à nous !
12:8	Et l'ayant saisi, ils le tuèrent et le jetèrent hors de la vigne.
12:9	Que fera donc le maître de la vigne ? Il viendra, et fera périr ces vignerons, et donnera la vigne à d'autres.
12:10	N'avez-vous pas lu cette Écriture ? La pierre qu'ont rejetée ceux qui bâtissaient est devenue la tête de l'angle<!--Yéhoshoua ha Mashiah (Jésus le Christ), la pierre angulaire : Es. 8:13-15 ; Ps. 118:22-23.-->.
12:11	C'est par le Seigneur qu'elle l'est devenue, et elle est merveilleuse à nos yeux.
12:12	Et ils cherchaient à se saisir de lui, mais ils craignirent la foule. Car ils avaient compris que c'était contre eux qu'il avait dit cette parabole. Et le laissant, ils s'en allèrent.

### Le tribut à César<!--Mt. 22:15-22 ; Lu. 20:19-26.-->

12:13	Et ils envoient quelques-uns des pharisiens et des hérodiens auprès de lui afin de le surprendre par ses discours.
12:14	Et ils viennent lui dire : Docteur, nous savons que tu es véritable et que tu ne te soucies de personne, car tu ne regardes pas à la face des gens, mais tu enseignes la voie d'Elohîm selon la vérité. Est-il légal de payer le tribut à César ou non ?
12:15	Le payerons-nous, ou ne le payerons-nous pas ? Mais lui, connaissant leur hypocrisie, leur dit : Pourquoi me tentez-vous ? Apportez-moi un denier afin que je le voie.
12:16	Et ils lui en apportèrent. Et il leur dit : De qui porte-t-il l'image et l'inscription ? De César, lui répondirent-ils.
12:17	Et Yéhoshoua répondant, leur dit : Rendez à César ce qui est à César, et à Elohîm ce qui est à Elohîm. Et ils furent remplis d'admiration pour lui.

### Enseignement sur la résurrection<!--Mt. 22:23-33 ; Lu. 20:27-38.-->

12:18	Et les sadducéens, qui disent qu'il n'y a pas de résurrection, viennent vers lui. Et ils l'interrogeaient en disant :
12:19	Docteur, voici ce que Moshé nous a prescrit : Si le frère de quelqu'un meurt et laisse sa femme sans avoir d'enfants, son frère épousera sa veuve et suscitera une postérité à son frère.
12:20	Il y avait sept frères dont le premier prit une femme et mourut sans laisser d'enfants.
12:21	Et le second la prit et mourut, et il ne laissa pas non plus de postérité. Et il en fut de même du troisième,
12:22	et les sept la prirent et ne laissèrent pas de postérité. Après eux tous, la femme mourut aussi.
12:23	À la résurrection donc, quand ils ressusciteront, duquel d'entre eux sera-t-elle la femme ? Car les sept l'ont eue pour femme.
12:24	Et Yéhoshoua répondant, leur dit : N'est-ce pas à cause de ceci que vous vous égarez, c'est que vous ne connaissez pas les Écritures ni la puissance d'Elohîm ?
12:25	Car, lorsqu'ils ressuscitent d’entre les morts, ils ne se marient ni ne donnent en mariage, mais ils sont comme les anges qui sont dans les cieux.
12:26	Mais au sujet des morts, parce qu’ils sont réveillés, n'avez-vous pas lu dans le livre de Moshé, comment Elohîm lui parla dans le buisson, en disant : Moi, je suis l'Elohîm d'Abraham, et l'Elohîm de Yitzhak, et l'Elohîm de Yaacov ?
12:27	Or il n'est pas l'Elohîm des morts, mais l'Elohîm des vivants. Vous donc, vous vous égarez grandement.

### Le plus grand des commandements<!--Mt. 22:34-40 ; Lu. 10:25-28.-->

12:28	Mais un des scribes qui les avait entendus discuter, voyant qu'il leur avait bien répondu, s'approcha de lui, et lui demanda : Quel est le premier de tous les commandements ?
12:29	Mais Yéhoshoua lui répondit : Le premier de tous les commandements est : Écoute Israël<!--Écoute Israël : Yéhoshoua se réfère ici à De. 6:4 : « Écoute, Israël ! YHWH, notre Elohîm YHWH est un. Tu aimeras donc YHWH, ton Elohîm, de tout ton cœur, de toute ton âme, et de toute ta force ». On remarque cependant que le texte grec de Mc. 12:30 ne mentionne pas le Nom de l'Elohîm d'Israël : YHWH, mais le remplace par « le Seigneur ». Voir commentaire en Lu. 4:18-19. Le Shema Israël est le noyau central de la prière que le Juif adulte doit lire matin et soir. C'est la confession de foi juive. Yaacov (Jacob) est le premier à l'avoir enseignée à ses enfants dans Ge. 49:1-2.-->, le Seigneur, notre Elohîm, le Seigneur est un<!--Yéhoshoua ha Mashiah (Jésus le Christ), notre Seigneur et notre modèle, a confirmé le Shema Israël qui déclare haut et fort qu'Elohîm est un, et non trois en un. Le scribe, homme versé dans les Écritures, était satisfait de la réponse de Yéhoshoua parce qu'il croyait aussi en un seul Elohîm. Car le monothéisme est le fondement de la foi juive et des premiers chrétiens. Voir Za. 14:9 ; Ja. 2:19 ; Ga. 3:20 ; 1 Ti. 2:5.-->.
12:30	Et tu aimeras le Seigneur ton Elohîm de<!--Vient du grec "ek" qui est une préposition dénotant une origine c'est-à-dire le point d'où une action ou un mouvement procède.--> tout ton cœur, et de toute ton âme, et de toute ta pensée, et de toute ta force. C'est là le premier commandement<!--De. 6:4-5.-->.
12:31	Et le second lui est semblable : Tu aimeras ton prochain comme toi-même<!--Lé. 19:18.-->. Il n'y a pas d'autre commandement plus grand que ceux-ci.
12:32	Et le scribe lui dit : Docteur, tu as bien dit selon la vérité qu'Elohîm est un et qu'il n'y en a pas d'autre que lui,
12:33	et que l'aimer de tout son cœur, et de toute son intelligence, et de toute son âme, et de toute sa force, et d'aimer son prochain comme soi-même, c'est plus que tous les holocaustes et les sacrifices.
12:34	Et Yéhoshoua voyant qu'il avait répondu prudemment, lui dit : Tu n'es pas loin du Royaume d'Elohîm. Et personne n'osait plus l'interroger.
12:35	Et Yéhoshoua, enseignant dans le temple, répondit et dit : Comment les scribes disent-ils que le Mashiah est le Fils de David ?
12:36	Car David lui-même a dit par le Saint-Esprit : Le Seigneur a dit à mon Seigneur : Assieds-toi à partir de mes droites, jusqu'à ce que j'aie mis tes ennemis pour le marchepied de tes pieds.
12:37	David lui-même donc l'appelle Seigneur, comment est-il son fils ? Et une grande foule l'écoutait avec plaisir.

### Yéhoshoua dénonce les scribes<!--Mt. 22:41-46 ; Lu. 20:39-44.-->

12:38	Et il leur disait dans son enseignement : Gardez-vous des scribes qui prennent plaisir à se promener en robes longues et qui aiment les salutations sur les places du marché,
12:39	et les premiers sièges dans les synagogues, et les premières places dans les soupers,
12:40	qui dévorent entièrement les maisons des veuves, même sous le prétexte de faire de longues prières. Ils seront jugés plus sévèrement.

### L'offrande de la pauvre veuve<!--Lu. 21:1-4.-->

12:41	Et Yéhoshoua, s'étant assis vis-à-vis du trésor, regardait comment la foule jetait de la monnaie dans le trésor. Et beaucoup de riches y jetaient beaucoup.
12:42	Et une pauvre veuve vint, elle y jeta deux petites pièces, faisant le quart d'un sou.
12:43	Et ayant appelé à lui ses disciples, il leur dit : Amen je vous dis que cette pauvre veuve a jeté dans le trésor plus que tous ceux qui avaient jeté.
12:44	Car tous ont jeté de leur abondance, mais elle, de sa pauvreté, elle a jeté tout ce qu'elle avait, tout son bien<!--« Vie », « ce par quoi la vie est assurée, ressources, richesses, biens ».-->.

## Chapitre 13

### Prophétie sur la destruction du temple<!--Mt. 24:3 ; Lu. 21:7.-->

13:1	Et en sortant du temple, un de ses disciples lui dit : Docteur, regarde quelles pierres et quelles constructions !
13:2	Et Yéhoshoua répondant, lui dit : Vois-tu ces grandes constructions ? Il ne sera jamais laissé pierre sur pierre qui ne soit détruite.

### Les temps de la fin

13:3	Et étant assis, lui, sur le Mont des Oliviers, en face du temple, Petros, Yaacov, Yohanan et Andreas, l'interrogèrent en particulier :
13:4	Dis-nous : Quand ces choses arriveront-elles, et quel sera le signe lorsque toutes ces choses seront sur le point de s'accomplir ?
13:5	Et Yéhoshoua leur répondant, se mit à dire : discernez, que personne ne vous égare.
13:6	Car beaucoup viendront en mon nom, disant : Moi, je suis. Et ils en égareront beaucoup.
13:7	Et quand vous entendrez parler de guerres et de bruits de guerres, ne soyez pas troublés, parce qu'il faut que ces choses arrivent. Mais ce ne sera pas encore la fin.
13:8	Car nation se réveillera contre nation, et royaume contre royaume, et il y aura des tremblements de terre en divers lieux, et il y aura des famines et des troubles. Ces choses ne seront que les premières douleurs.
13:9	Mais vous, discernez vous-mêmes ! Car ils vous livreront aux sanhédrins et aux synagogues, vous serez battus de verges et vous serez présentés devant les gouverneurs et devant les rois, à cause de moi, en témoignage pour eux.
13:10	Mais il faut premièrement que l'Évangile soit prêché à toutes les nations.
13:11	Mais quand ils vous mèneront pour vous livrer, ne vous inquiétez pas d’avance de ce que vous direz et ne le préméditez pas non plus, mais selon ce qui vous sera donné en cette heure-là, dites-le. Car ce ne sera pas vous qui parlerez, mais le Saint-Esprit.
13:12	Mais le frère livrera son frère à la mort, et le père son enfant : et les enfants se soulèveront contre leurs parents et les feront mourir.
13:13	Et vous serez haïs de tous à cause de mon nom, mais celui qui supportera bravement et calmement les mauvais traitements<!--Vient d'une racine qui signifie : rester le même, ne pas devenir un autre ou différent.--> jusqu'à la fin, celui-là sera sauvé.

### L'abomination de la désolation<!--Ps. 2:5 ; Mt. 24:15-28 ; Lu. 21:20-24 ; Ap. 7:14.-->

13:14	Mais quand vous aurez vu l'abomination de la désolation<!--Voir commentaire Mt. 24:15.--> déclarée par Daniye'l, le prophète, établie là où elle ne doit pas être, que celui qui lit comprenne ! Alors, que ceux qui seront en Judée fuient dans les montagnes.
13:15	Et que celui qui sera sur le toit ne descende pas dans la maison, et n'entre pas pour emporter quoi que ce soit hors de sa maison,
13:16	et que celui qui sera dans le champ ne retourne pas en arrière pour emporter son manteau.
13:17	Mais malheur à celles qui seront enceintes, et à celles qui allaiteront en ces jours-là.
13:18	Mais priez que votre fuite n'arrive pas en hiver,
13:19	car ces jours seront une tribulation telle qu’il n’en est pas arrivé de pareille depuis le commencement de la création qu'Elohîm a créée jusqu’à maintenant, et qu’il n’en arrivera plus.
13:20	Et si le Seigneur n’avait pas abrégé ces jours, aucune chair ne serait sauvée. Mais à cause des élus qu'il a choisis, il a abrégé ces jours.
13:21	Et alors si quelqu'un vous dit : Voici, le Mashiah est ici. Ou : Voici, il est là. Ne le croyez pas.
13:22	Car des faux mashiah et des faux prophètes se réveilleront, et ils donneront<!--Voir Mc. 14:44.--> des signes et des miracles pour égarer, s'il était possible, même les élus.
13:23	Mais vous, discernez ! voici, je vous ai tout dit d'avance.

### Le retour du Mashiah sur la Terre<!--Mt. 24:29-31 ; Lu. 21:25-28.-->

13:24	Mais en ces jours-là, après cette tribulation là, le soleil s'obscurcira, et la lune ne donnera plus sa lumière,
13:25	et les étoiles du ciel tomberont, et les puissances qui sont dans les cieux seront ébranlées.
13:26	Et ils verront alors le Fils d'humain venant sur les nuées, avec une grande puissance et une grande gloire.
13:27	Et alors il enverra ses anges, et il rassemblera ses élus des quatre vents, de l'extrémité de la Terre jusqu'à l'extrémité du ciel.

### Parabole du figuier<!--Mt. 24:32-35 ; Lu. 21:29-33.-->

13:28	Mais apprenez la leçon tirée de la parabole du figuier. Dès que sa jeune branche devient tendre et pousse ses feuilles, vous savez que l'été est proche.
13:29	De même vous aussi, quand vous verrez arriver ces choses, sachez qu'il est proche, aux portes.
13:30	Amen, je vous le dis, cette génération ne passera jamais, que toutes ces choses ne soient arrivées.
13:31	Le ciel et la Terre passeront, mais mes paroles ne passeront jamais.

### Exhortation de Yéhoshoua à la vigilance<!--Mt. 24:36-51 ; Lu. 21:34-38.-->

13:32	Or pour ce qui est du jour ou de l'heure, personne ne le sait, ni les anges dans le ciel, ni le Fils<!--Comment expliquer l'ignorance du Fils quant à l'heure de son retour ? En prenant la condition d'un homme, Yéhoshoua s'est dépouillé de ses prérogatives divines et a connu des limites propres au genre humain (Ph. 2:7) : la fatigue (Jn. 4:6 ; Mc. 4:38), la faim (Mc. 11:12), l'angoisse et la peur (Mc. 14:33), la mortalité physique... Ce dépouillement incluait le renoncement à l'omniscience, d'où le fait que Yéhoshoua ha Mashiah (Jésus le Christ) homme ne connaissait pas le jour et l'heure de son retour.-->, excepté le Père.
13:33	Discernez ! Veillez et priez ! Car vous ne savez pas quand c'est le temps.
13:34	Comme un être humain qui, allant à l'étranger, a laissé sa maison et donné autorité à ses esclaves, à chacun sa tâche et il a donné au portier l'ordre de veiller.
13:35	Veillez donc, car vous ne savez pas quand le Seigneur de la maison vient, ou le soir, ou à minuit, ou à l'heure où le coq chante, ou le matin,
13:36	de peur qu’étant venu soudainement, il ne vous trouve dormant.
13:37	Mais ce que je vous dis, je le dis à tous : Veillez !

## Chapitre 14

### Le complot contre Yéhoshoua<!--Mt. 26:1-5 ; Lu. 22:1-2.-->

14:1	Or, deux jours après, c’était la Pâque et les pains sans levain. Et les principaux prêtres et les scribes cherchaient comment ils se saisiraient de lui par ruse et le tueraient.
14:2	Mais ils se disaient : Non pas pendant la fête, sinon il y aura un tumulte du peuple.

### Myriam de Béthanie répand du baume sur Yéhoshoua<!--Mt. 26:6-13 ; Jn. 12:1-8.-->

14:3	Et étant à Béthanie, dans la maison de Shim’ôn le lépreux, à table, une femme vint à lui avec un vase d'albâtre, rempli d'un baume de nard, pur et très coûteux. Et ayant brisé le vase, elle le répandit sur sa tête.
14:4	Mais certains de ceux qui étaient là furent indignés et se dirent entre eux : Pourquoi cette perte du baume a-t-elle été faite ?
14:5	Car on aurait pu le vendre plus de 300 deniers, et les donner aux pauvres. Et ils la menaçaient<!--Ou « donner un sérieux avertissement, menacer pour enjoindre ».-->.
14:6	Mais Yéhoshoua dit : Laissez-la. Pourquoi lui faites-vous de la peine ? Elle a fait une bonne œuvre envers moi.
14:7	Car vous avez toujours les pauvres avec vous et vous pouvez leur faire du bien quand vous voulez, mais vous ne m'avez pas toujours.
14:8	Elle a fait ce qu'elle a pu, elle a d'avance embaumé mon corps pour la sépulture.
14:9	Amen, je vous le dis, partout où cet Évangile sera prêché, dans le monde entier, on racontera aussi en mémoire d'elle ce qu'elle a fait.

### La trahison de Yéhouda (Judas)<!--Mt. 26:14-16 ; Lu. 22:3-6.-->

14:10	Et Yéhouda Iskariote, l'un des douze, s’en alla vers les principaux prêtres afin qu’il le leur livre.
14:11	Et l'ayant entendu, ils furent dans la joie, et promirent de lui donner de l'argent. Et Yéhouda cherchait une occasion favorable pour le livrer.

### La dernière Pâque<!--Mt. 26:17-25 ; Lu. 22:7-20 ; Jn. 13:1-12.-->

14:12	Et le premier jour des pains sans levain, où l'on immolait la Pâque, ses disciples lui disent : Où veux-tu que nous allions préparer pour que tu manges la Pâque ?
14:13	Et il envoya deux de ses disciples, et leur dit : Allez dans la ville, vous rencontrerez un humain portant une cruche d'eau, suivez-le.
14:14	Et où qu’il entre, dites au maître de la maison : Le Docteur dit : Où est l'auberge où je mangerai la Pâque avec mes disciples ?
14:15	Et il vous montrera une grande chambre haute, meublée et toute prête : c'est là que vous ferez les préparatifs pour nous.
14:16	Et ses disciples partirent, arrivèrent dans la ville, et ils trouvèrent les choses comme il l'avait dit et ils préparèrent la Pâque.
14:17	Et le soir étant arrivé, il vient avec les douze.
14:18	Et comme ils étaient à table et qu'ils mangeaient, Yéhoshoua dit : Amen, je vous le dis, l'un de vous, qui mange avec moi, me livrera.
14:19	Et ils commencèrent à s'attrister, et ils lui dirent l'un après l'autre : Est-ce moi ? Et un autre : Est-ce moi ?
14:20	Mais répondant, il leur dit : C'est l'un des douze qui trempe avec moi dans le plat.
14:21	En effet, le Fils d'humain s'en va, selon qu'il est écrit<!--Ps. 41:10.--> de lui, mais malheur à l'humain par qui le Fils d'humain est livré ! Mieux vaudrait pour cet humain qu'il ne soit pas né.

### Le repas de la Pâque<!--Mt. 26:26-29 ; Lu. 22:17-20 ; Jn. 13:12-30 ; 1 Co. 11:23-26.-->

14:22	Et pendant qu'ils mangeaient, Yéhoshoua prenant du pain, et ayant prononcé la prière de bénédiction, le rompit et le leur donna, et dit : Prenez, mangez, ceci est mon corps.
14:23	Et, prenant la coupe, et ayant rendu grâces, la leur donna et ils en burent tous.
14:24	Et il leur dit : Ceci est mon sang<!--Nouvelle alliance : voir Jn. 19:30.-->, celui de la nouvelle alliance, qui est répandu pour beaucoup.
14:25	Amen, je vous le dis, je ne boirai plus du fruit de la vigne jusqu'au jour où j'en boirai du nouveau dans le Royaume d'Elohîm.

### Yéhoshoua annonce à Petros (Pierre) son triple reniement<!--Mt. 26:30-35 ; Lu. 22:31-34 ; Jn. 13:36-38.-->

14:26	Et ayant chanté des hymnes pascals<!--Cantique : voir Mt. 26:30.-->, ils se rendirent à la Montagne des Oliviers.
14:27	Et Yéhoshoua leur dit : Vous serez tous cette nuit scandalisés de moi, car il est écrit : Je frapperai le Berger, et les brebis seront dispersées<!--Za. 13:7.-->.
14:28	Mais après que je serai réveillé, je vous précéderai en Galilée.
14:29	Mais Petros lui dit : Même si tous seront scandalisés, mais moi, jamais.
14:30	Et Yéhoshoua lui dit : Amen, je te le dis, aujourd'hui, cette nuit même, avant qu'un coq ait chanté deux fois, tu me renieras trois fois.
14:31	Mais il disait encore plus fortement : Quand même il me faudrait mourir avec toi, je ne te renierai jamais. Et tous lui dirent la même chose.

### Gethsémané<!--Mt. 26:36-46 ; Lu. 22:39-46 ; Jn. 18:1.-->

14:32	Et ils se rendent dans un lieu appelé Gethsémané, et Yéhoshoua dit à ses disciples : Asseyez-vous ici jusqu'à ce que j'aie prié.
14:33	Et il prend avec lui Petros, Yaacov et Yohanan, et il commença à être saisi de frayeur et d'angoisse<!--Voir Mt. 26:37.-->.
14:34	Et il leur dit : Mon âme est très triste jusqu'à la mort, restez ici et veillez !

### Première prière de Yéhoshoua<!--Mt. 26:39 ; Lu. 22:41-42.-->

14:35	Et s'en allant un peu plus en avant, il tomba par terre et pria pour que, s'il était possible, cette heure passe loin de lui.
14:36	Et il disait : Abba, Père, toutes choses te sont possibles, éloigne de moi cette coupe ! Toutefois, non pas ce que je veux, mais ce que tu veux.
14:37	Et il vient et les trouve endormis, et il dit à Petros : Shim’ôn, tu dors ! Tu n’as pas été capable de veiller une heure !
14:38	Veillez et priez afin que vous n'entriez pas en tentation. En effet, l'esprit est bien disposé, mais la chair est faible.

### Deuxième prière de Yéhoshoua<!--Mt. 26:42 ; Lu. 22:44.-->

14:39	Et s'étant éloigné de nouveau, il fit la même prière, en disant les mêmes paroles.
14:40	Et étant revenu, et les trouva encore endormis, car leurs yeux étaient appesantis. Ils ne surent que lui répondre.

### Troisième prière de Yéhoshoua<!--Mt. 26:44.-->

14:41	Et il vient pour la troisième fois, et leur dit : Dormez maintenant, et reposez-vous ! C'est assez ! L'heure est venue : voici, le Fils d'humain s'en va être livré entre les mains des pécheurs.
14:42	Réveillez-vous ! Allons ! Voici, il s'approche, celui qui me livre.

### Yéhoshoua trahi, abandonné et arrêté<!--Mt. 26:47-56 ; Lu. 22:47-53 ; Jn. 18:2-11.-->

14:43	Et immédiatement, tandis qu'il parlait encore, survient, Yéhouda, l'un des douze, et avec lui une grande foule ayant des épées et des bâtons, envoyée par les principaux prêtres, par les scribes et par les anciens.
14:44	Or celui qui le livrait leur avait donné ce signe : Celui que j'embrasserai, c'est lui. Saisissez-le, et emmenez-le sûrement.
14:45	Et aussitôt arrivé, il s'approcha de lui en disant : Rabbi, Rabbi ! Et il l'embrassa tendrement.
14:46	Et ils mirent les mains sur lui et le saisirent.
14:47	Mais un de ceux qui étaient là présents, tirant son épée, frappa l'esclave du grand-prêtre et lui emporta l'oreille.
14:48	Et Yéhoshoua, répondant, leur dit : Vous êtes venus comme après un brigand, avec des épées et des bâtons, pour m'arrêter.
14:49	J'étais tous les jours parmi vous, enseignant dans le temple, et vous ne m'avez pas saisi. Mais c'est afin que les Écritures soient accomplies<!--Es. 53.-->.
14:50	Et eux tous l'abandonnèrent et s'enfuirent.
14:51	Et un certain jeune homme le suivait, enveloppé d'un linceul sur le corps nu. Et quelques jeunes gens le saisirent.
14:52	Mais lui, abandonnant son linceul, leur échappa tout nu.

### Yéhoshoua comparaît devant Kaïaphas et le sanhédrin<!--Mt. 26:57-68 ; Jn. 18:12-14,19-24.-->

14:53	Et ils emmenèrent Yéhoshoua chez le grand-prêtre, où s'assemblèrent tous les principaux prêtres, les anciens et les scribes.
14:54	Et Petros le suivait de loin jusque dans la cour du grand-prêtre. Et il était assis avec les serviteurs et se chauffait près du feu.
14:55	Mais les principaux prêtres et tout le sanhédrin cherchaient quelque témoignage contre Yéhoshoua pour le faire mourir, mais ils n'en trouvaient pas.
14:56	Car beaucoup rendaient de faux témoignages contre lui, mais les témoignages n’étaient pas égaux.
14:57	Et quelques-uns s'élevèrent, et portèrent de faux témoignages contre lui, disant :
14:58	Nous l'avons entendu disant : Moi, je détruirai ce temple fait par la main d'homme, et en trois jours, j'en rebâtirai un autre qui ne sera pas fait de main.
14:59	Et même de cette manière leur témoignage n’était pas égal.
14:60	Et le grand-prêtre se levant au milieu, interrogea Yéhoshoua en disant : Ne réponds-tu rien ? Qu'est-ce que ceux-ci témoignent contre toi ? 
14:61	Mais il garda le silence, et ne répondit rien<!--1 Pi. 2:22-23.-->. Le grand-prêtre l'interrogea de nouveau, et lui dit : Es-tu le Mashiah, le Fils du Béni ?
14:62	Et Yéhoshoua lui répondit : Moi, JE SUIS. Et vous verrez le Fils d'humain assis à la droite de la puissance et venant sur les nuées du ciel.
14:63	Et le grand-prêtre déchira ses tuniques<!--Lé. 10:6 et Mt. 26:65.--> et dit : Qu'avons-nous encore besoin de témoins ?
14:64	Vous avez entendu le blasphème. Que vous en semble ? Alors tous le condamnèrent comme étant digne de mort.

### Yéhoshoua maltraité par les Juifs

14:65	Et quelques-uns se mirent à cracher sur lui, et à lui voiler le visage, et à le frapper à coups de poing et à lui dire : Prophétise ! Et les serviteurs lui donnaient des coups avec leurs verges.

### Le triple reniement de Petros (Pierre)<!--Mt. 26:69-75 ; Lu. 22:54-62 ; Jn. 18:15-18,25-27.-->

14:66	Or pendant que Petros était en bas dans la cour, une des servantes du grand-prêtre vint.
14:67	Et, apercevant Petros qui se chauffait, et l'ayant regardé en face, elle lui dit : Toi aussi, tu étais avec Yéhoshoua de Nazareth.
14:68	Mais il nia, en disant : Je ne sais ni ne comprends ce que tu dis ! Et il sortit dehors pour aller dans le vestibule. Et le coq chanta.
14:69	Et la servante l'ayant vu de nouveau, elle se mit à dire à ceux qui étaient là présents : Celui-ci est de ces gens-là. 
14:70	Mais il le nia de nouveau. Peu après, ceux qui étaient là présents dirent de nouveau à Petros : Certainement tu es de ces gens-là, car tu es Galiléen, et ton langage<!--Ou dialecte. Voir Mt. 26:73.--> ressemble au leur.
14:71	Mais il se mit à se maudire, et à jurer, en disant : Je ne connais pas cet homme-là dont vous parlez.
14:72	Et le coq chanta pour la seconde fois. Et Petros se souvint de cette parole que Yéhoshoua lui avait dite : Avant que le coq chante deux fois, tu me renieras trois fois. Et en y réfléchissant, il pleurait.

## Chapitre 15

### Yéhoshoua comparaît devant Pilate<!--Mt. 27:1-2,11-15 ; Lu. 23:1-7,13-16 ; Jn. 18:28-38, 19:1-15.-->

15:1	Et immédiatement, dès le matin, les principaux prêtres tinrent conseil avec les anciens et les scribes, et tout le sanhédrin. Et ayant lié Yéhoshoua, ils l'emmenèrent, et le livrèrent à Pilate.
15:2	Et Pilate l’interrogea : Tu es le roi des Juifs ? Et répondant, il lui dit : Tu le dis.
15:3	Et les principaux prêtres l'accusaient de beaucoup de choses.
15:4	Mais Pilate l'interrogea de nouveau : Ne réponds-tu rien ? Vois combien de choses ils témoignent contre toi !
15:5	Mais Yéhoshoua ne donna plus aucune réponse, ce qui étonna Pilate.

### Yéhoshoua ou Barabbas ?<!--Mt. 27:15-26 ; Lu. 23:17-25 ; Jn. 18:39-40.-->

15:6	Or à chaque fête, il relâchait un prisonnier, celui que demandait la foule.
15:7	Or il y en avait un, nommé Barabbas, qui était prisonnier avec ses compagnons d'insurrection pour une insurrection, dans laquelle ils avaient commis un meurtre.
15:8	Et la foule poussant un cri, se mit à demander ce qu’il faisait perpétuellement pour eux.
15:9	Mais Pilate leur répondit, en disant : Voulez-vous que je vous relâche le Roi des Juifs ?
15:10	Car il savait que les principaux prêtres l’avaient livré par<!--à cause.--> envie<!--C'est la convoitise mêlée de dépit et de haine, à la vue du bonheur ou des biens de quelqu'un.-->.
15:11	Mais les principaux prêtres excitèrent la foule, afin que Pilate leur relâche plutôt Barabbas.
15:12	Et Pilate, répondant, leur dit encore : Que voulez-vous donc que je fasse de celui que vous appelez Roi des Juifs ?
15:13	Et ils s'écrièrent encore : Crucifie-le !
15:14	Et Pilate leur dit : Mais quel mal a-t-il fait ? Et ils s'écrièrent encore plus fort : Crucifie-le !
15:15	Mais Pilate, voulant satisfaire la foule, leur relâcha Barabbas. Et après avoir fait battre de verges Yéhoshoua, il le livra pour être crucifié.

### Le Roi couronné d'épines<!--Mt. 27:27-31 ; Jn. 19:1-3.-->

15:16	Et les soldats l'emmenèrent dans l'intérieur de la cour, c'est-à-dire dans le prétoire, et ils rassemblent toute la cohorte<!--Une cohorte était une unité tactique de base de la légion romaine qui comptait environ 600 hommes, soit un dixième de la légion. Elle était constituée de trois manipules (Étendard d'une compagnie militaire romaine).-->.
15:17	Et ils le revêtent d'une robe de pourpre et posent sur sa tête une couronne d'épines qu'ils avaient tressée.
15:18	Et ils commencèrent à le saluer, en lui disant : Nous te saluons, Roi des Juifs !

### Yéhoshoua maltraité par les soldats

15:19	Et ils lui frappaient la tête avec un roseau, et crachaient contre lui, et fléchissant les genoux, ils l'adoraient.
15:20	Et après s'être ainsi joués de lui, ils le dépouillèrent de la pourpre, le revêtirent de ses propres vêtements et le conduisirent dehors pour le crucifier.
15:21	Et ils forcèrent un certain Shim’ôn de Cyrène, père d'Alexandros et de Rhouphos qui passait par là en revenant des champs, de porter sa croix.
15:22	Et ils le conduisent au lieu appelé Golgotha<!--Golgotha (le lieu du Crâne) était une colline située à l'extérieur de Yeroushalaim (Jérusalem), sur laquelle les Romains crucifiaient les condamnés.-->, ce qui, interprété, est : le lieu du Crâne.
15:23	Et ils lui donnèrent à boire du vin mêlé de myrrhe, mais il ne le prit pas.

### La crucifixion de Yéhoshoua<!--Mt. 27:33-56 ; Lu. 23:33-49 ; Jn. 19:16-37.-->

15:24	Et après l'avoir crucifié, ils partagèrent ses vêtements en jetant le sort dessus, à qui en emporterait une part.
15:25	Or c'était la troisième heure, quand ils le crucifièrent.
15:26	Et l'inscription de sa condamnation était ainsi écrite : LE ROI DES JUIFS.
15:27	Et ils crucifient aussi avec lui deux brigands, l'un à sa main droite et l'autre à sa gauche.
15:28	Et ainsi fut accomplie l'Écriture qui dit : Et il a été mis au rang des violeurs de la torah<!--Du grec « anomos » qui signifie « sans loi », « impie », etc. Es. 53:12.-->.
15:29	Et les passants blasphémaient contre lui et secouaient la tête en disant : Hé ! Toi qui détruis le temple et qui le rebâtis en trois jours,
15:30	sauve-toi toi-même, et descends de la croix !
15:31	De même aussi les principaux prêtres, se jouant entre eux avec les scribes, disaient : Il a sauvé les autres, et il ne peut se sauver lui-même.
15:32	Que le Mashiah, le Roi d'Israël, descende maintenant de la croix, afin que nous le voyions et que nous croyions ! Ceux qui étaient crucifiés avec lui l'insultaient aussi.
15:33	Mais la sixième heure étant venue, la ténèbre<!--Le mot est au singulier.--> survint sur toute la Terre jusqu'à la neuvième heure.
15:34	Et à la neuvième heure, Yéhoshoua cria d'une grande voix en disant : Éloï, Éloï, lama sabachthani ? Ce qui, interprété, est : Mon El<!--Voir le dictionnaire en annexe.--> ! Mon El ! Pourquoi m'as-tu abandonné<!--Voir Ps. 22:2.--> ?
15:35	Et quelques-uns de ceux qui étaient présents, l'ayant entendu, disaient : Voici, il appelle Éliyah.
15:36	Et l'un d'eux courut remplir une éponge de vinaigre<!--Le vinaigre : voir Mt. 27:34.-->, et l'ayant fixée au bout d'un roseau, il lui donna à boire, en disant : Laissez, voyons si Éliyah viendra le descendre de la croix.
15:37	Mais Yéhoshoua, ayant poussé un grand cri, expira<!--Rendre son dernier souffle.-->.

### FIN DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE<!--Hé. 9:16-18.-->


### Le voile du temple déchiré

15:38	Et le voile du temple se déchira en deux, depuis le haut jusqu'en bas<!--Hé. 10:19-20.-->.
15:39	Et le centenier, qui était en face de lui, voyant qu’il avait expiré en criant ainsi, dit : Cet homme était vraiment le Fils d'Elohîm.
15:40	Or il y avait aussi des femmes regardant de loin. Parmi lesquelles étaient Myriam-Magdeleine, Myriam, mère de Yaacov le petit et de Yossef, et Shelomit,
15:41	lesquelles aussi, lorsqu'il était en Galilée, le suivaient et le servaient, et beaucoup d'autres qui étaient montées avec lui à Yeroushalaim.

### Yossef d'Arimathée demande le corps de Yéhoshoua

15:42	Et le soir étant venu, comme c'était la préparation, c'est-à-dire le jour qui précède un shabbat,
15:43	arriva Yossef d'Arimathée, conseiller honorable, qui attendait aussi le Royaume d'Elohîm. Il osa se rendre vers Pilate pour demander le corps de Yéhoshoua.
15:44	Et Pilate s'étonna qu'il soit déjà mort. Et ayant fait venir le centenier, il lui demanda s'il était mort depuis longtemps.
15:45	Et l’ayant appris du centenier, il donna le corps à Yossef.
15:46	Et ayant acheté un linceul, le descendit de la croix, et l'enveloppa du linceul, et le déposa dans un sépulcre taillé dans le rocher. Et il roula une pierre sur l'entrée du sépulcre.
15:47	Mais Myriam-Magdeleine, et Myriam, celle de Yossef, regardaient où on le mettait.

## Chapitre 16

### Myriam-Magdeleine (Marie de Magdala), Myriam (Marie), mère de Yaacov (Jacques) et Shelomit (Salomé) se rendent au sépulcre

16:1	Et le shabbat<!--Il est question ici du shabbat annuel, observé lors de la fête des pains sans levain. Ce shabbat n'a rien à voir avec le shabbat hebdomadaire ou le samedi. Voir Mt. 28:1.--> étant passé, Myriam-Magdeleine, Myriam, mère de Yaacov et Shelomit achetèrent des aromates pour venir l’oindre<!--Lu. 7:38 et 46 ; Jn. 11:2 et 12:3.-->.
16:2	Et très tôt le matin d'un des shabbats<!--Il est question ici du shabbat hebdomadaire, c'est-à-dire le septième jour ou le samedi.-->, elles viennent au sépulcre, comme le soleil venait de se lever.
16:3	Et elles se disaient entre elles : Qui nous roulera la pierre de l'entrée du sépulcre ?
16:4	Et levant les yeux, elles voient que la pierre, qui était très grande, avait été roulée.

### La résurrection de Yéhoshoua annoncée par un ange

16:5	Et elles entrèrent dans le sépulcre, elles virent un jeune homme assis à droite, vêtu d'une robe blanche, et elles furent terrifiées.
16:6	Mais il leur dit : Ne soyez pas terrifiées ! Vous cherchez Yéhoshoua de Nazareth qui a été crucifié. Il est réveillé, il n'est pas ici ! Voici le lieu où on l'avait mis.
16:7	Mais allez, et dites à ses disciples, et à Petros, qu'il vous précède en Galilée. C'est là que vous le verrez, comme il vous l'a dit.
16:8	Et sortant promptement du sépulcre, elles s'enfuirent car la peur et le trouble les avaient saisies. Et elles ne dirent rien à personne, car elles avaient peur.

### La résurrection de Yéhoshoua annoncée par Myriam-Magdeleine

16:9	Et étant réveillé, le matin du premier shabbat<!--Le premier shabbat. Il fallait compter 7 shabbats, du shabbat hebdomadaire (dans la semaine de la fête des pains sans levain) à la pentecôte selon Lévitique 23:15-22. Il est donc question ici du shabbat hebdomadaire, c'est-à-dire le septième jour ou le vendredi / samedi (Voir Mt. 28:1). Pour beaucoup, Yéhoshoua ha Mashiah serait mort le vendredi soir et ressuscité le dimanche matin. Cette théorie ne tient pas lorsqu'elle est confrontée au récit des évangiles. Le Seigneur a déclaré qu'il resterait « trois jours et trois nuits dans le cœur de la Terre » (Mt. 12:40), et qu'il ressusciterait « trois jours après » (Mc. 8:31). Or, de toute évidence, si Yéhoshoua est mort le vendredi pour ressusciter le dimanche matin, cela ne fait pas trois jours et trois nuits. Les Écritures ne précisent pas quel jour le Seigneur est mort, mais elles nous donnent quelques indices. Tout d'abord, il convient de signaler que chez les Hébreux (selon Ge. 1), le jour commence au coucher du soleil, aux environs de 18 heures, et s'achève le lendemain au coucher du soleil. Chez les Romains, le jour commence à minuit et se termine le lendemain à minuit. C'est de cette manière que l'évangile de Yohanan (Jean) compte les heures. Dans les autres évangiles, les journées commencent avec le lever du soleil. Nous savons que Yéhoshoua a été crucifié à « la troisième heure » (Mc. 15:25), ce qui correspond à 9 heures du matin. Ensuite, il est précisé qu'il y a eu des ténèbres sur la Terre de la sixième à la neuvième heure, donc de midi à 15 heures (Mt. 27:45-46 ; Mc. 15:33-34 ; Lu. 23:44). Yéhoshoua est donc mort avant 18 heures. Ainsi, il est évident qu'il n'a pas pu passer toute la journée du vendredi au tombeau. Nous savons aussi que Yéhoshoua a été crucifié « le jour qui précède un shabbat » (Mc. 15:42), ce qui pourrait donner raison à la théorie selon laquelle il serait mort le vendredi. Or, les Hébreux ont des shabbats hebdomadaires (le vendredi / samedi) et des grands shabbats annuels, qui correspondent aux fêtes de YHWH (Lé. 23). Ainsi, le shabbat en question était sans doute la fête des pains sans levain. (Mt. 26 ; Mc. 14 ; Lu. 22) cette fête durait 7 jours. Il y avait plusieurs shabbats semaine-là : la pâque, la fête des pains sans levain et le shabbat hebdomadaire. En Mt. 28:1 ; Mc. 16:1 et Lu. 24:1 où nous apprenons que des femmes sont allées acheter des aromates pour embaumer le corps du Seigneur « lorsque le shabbat fut passé » (il s'agit de la fête des pains sans levain). Le premier shabbat est le shabbat hebdomadaire de la semaine des pains sans levain. Il est relaté en Lu. 23:54-56 où il est dit qu'avant le début du shabbat, les femmes préparèrent des aromates et des baumes, et qu'ensuite elles se reposèrent. Or, il est impossible qu'elles aient acheté les aromates après le shabbat et qu'elles les aient préparées avant, à moins qu'il n'y ait eu 2 shabbats cette semaine-là. Ainsi, pour que le Seigneur puisse effectivement passer trois jours et trois nuits dans le cœur de la terre (Mt. 12:40), il a dû être arrêté la nuit de lundi à mardi à Getsémané (en sachant que pour les Hébreux, le lundi à 18 heures correspond au début de mardi). Il comparut devant le sanhédrin le lundi soir (Mt. 26:57-66). Et le mardi matin à 9 heures, il fut crucifié. Il resta à la croix six heures (de 9 heures à 15 heures) et il fut mis au tombeau le soir, vers 18 heures (ce qui correspond au début du mercredi pour les Hébreux ; Mt. 27:57-60). Nous avons donc :\\-Du mardi soir au mercredi soir : un jour et une nuit.\\-Du mercredi soir au jeudi soir : 2 jours et 2 nuits.\\-Du jeudi soir au vendredi soir : trois jours et trois nuits.\\Le Seigneur est donc ressuscité juste au début du shabbat hebdomadaire, soit le vendredi soir à partir de 18 heures (ce qui correspond au début du shabbat hebdomadaire pour les Hébreux et au vendredi soir pour les Romains). Quand les femmes arrivèrent au sépulcre à l'aube du shabbat ou le samedi matin, le Seigneur n'y était plus (Mt. 28:1 ; Mc. 16:2 ; Lu. 24:1-3). Voir annexe : La mort de Yéhoshoua ha Mashiah.--> il apparut d'abord à Myriam-Magdeleine, de laquelle il avait chassé sept démons.
16:10	Celle-ci s'en étant allée, porta la nouvelle à ceux qui avaient été avec lui, et qui se lamentaient et pleuraient.
16:11	Mais eux, entendant qu'il était vivant et qu'il avait été vu par elle, ne crurent pas.
16:12	Or après ces choses, il fut manifesté sous une autre forme à deux d'entre eux, qui étaient en chemin pour aller à la campagne.
16:13	Et ceux-ci s’en allèrent et portèrent la nouvelle aux autres, mais ils ne les crurent pas non plus.
16:14	Enfin, il fut manifesté aux onze eux-mêmes pendant qu'ils étaient à table, et il leur reprocha leur incrédulité et leur dureté de cœur, parce qu'ils n'avaient pas cru ceux qui l'avaient vu réveillé.

### La mission des disciples<!--Mt. 28:16-20 ; Lu. 24:46-48 ; Jn. 17:18, 20:21 ; Ac. 1:8.-->

16:15	Et il leur dit : Étant allés dans le monde entier, prêchez l'Évangile à toute créature.
16:16	Celui qui aura cru et qui aura été baptisé sera sauvé, mais celui qui n’aura pas cru sera condamné.
16:17	Et voici les signes qui suivront de près<!--« Suivre quelqu'un pour être toujours à ses côtés », « suivre exactement », « accompagner ».--> ceux qui auront cru : ils chasseront les démons en mon nom, ils parleront de nouvelles langues,
16:18	ils prendront dans leurs mains les serpents, et s'ils boivent quelque chose de mortel, elle ne leur fera jamais de mal. Ils imposeront les mains aux malades et ils seront guéris.

### Yéhoshoua enlevé au ciel<!--Lu. 24:50-53 ; Ac. 1:9-11.-->

16:19	Après leur avoir parlé, le Seigneur fut enlevé au ciel et s'assit en effet à la droite d'Elohîm.
16:20	Et eux, étant partis, prêchèrent partout. Le Seigneur travaillant avec eux et confirmant la parole par le moyen des signes qui l'accompagnaient. Amen.
