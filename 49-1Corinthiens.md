# 1 Corinthiens (1 Co.)

Auteur : Paulos (Paul)

Thème : Le comportement du chrétien

Date de rédaction : Env. 56 ap. J.-C.

Dans l'Antiquité, Corinthe, capitale de l'Achaïe, était la ville la plus prospère et puissante de Grèce. Située sur un isthme séparant la Mer Égée de la Mer Ionienne, Corinthe était au carrefour de l'Asie et de l'Italie, et constituait un véritable centre commercial où les produits orientaux et occidentaux se croisaient.

Paulos (Paul) arriva à Corinthe en 51 ap. J.-C., sous le règne de l'empereur romain Claude (10 av. J.-C. - 54 ap. J.-C.), et y demeura 18 mois. Il trouva une ville riche en pleine expansion, une population parlant diverses langues et rendant des cultes à une multitude de divinités. Rédigée au terme de trois années passées à Éphèse, la première lettre de Paulos aux Corinthiens répond à une lettre dans laquelle ceux-ci s'interrogeaient sur le mariage et sur les choses sacrifiées aux idoles. Ce fut aussi pour Paulos, l'occasion de reprendre cette jeune assemblée dont l'état charnel constituait un frein à son avancée spirituelle. Les Corinthiens avaient en effet confondu le culte raisonnable et les pratiques liées aux cultes à mystères.

## Chapitre 1

1:1	Paulos, appelé apôtre de Yéhoshoua Mashiah à travers la volonté d'Elohîm, et le frère Sosthènes,
1:2	à l'assemblée d'Elohîm qui est à Corinthe, aux sanctifiés en Mashiah Yéhoshoua, appelés, saints, avec tous ceux qui, en tout lieu, invoquent<!--Voir 1 Pi. 1:17.--> le nom de notre Seigneur Yéhoshoua Mashiah, le leur et le nôtre :
1:3	à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:4	Je rends toujours grâce à mon Elohîm à votre sujet, pour la grâce d'Elohîm qui vous a été donnée en Mashiah Yéhoshoua,
1:5	parce qu'en toute chose vous avez été enrichis en lui en toute parole et toute connaissance,
1:6	selon que le témoignage du Mashiah a été confirmé en vous.
1:7	Ainsi, il ne vous manque aucun don de grâce, à vous qui attendez assidûment et patiemment la révélation de notre Seigneur Yéhoshoua Mashiah,
1:8	qui vous affermira aussi jusqu'à la fin pour être irréprochables au jour de notre Seigneur Yéhoshoua Mashiah.
1:9	Il est fidèle, l'Elohîm à travers qui vous avez été appelés à la communion de son Fils, Yéhoshoua Mashiah, notre Seigneur.
1:10	Mais je vous exhorte, frères, à travers le nom de notre Seigneur Yéhoshoua Mashiah, à parler tous un même langage, et qu'il n'y ait pas de divisions parmi vous, mais soyez ajustés dans une même pensée et dans un même avis.
1:11	Car il m’a été déclaré à votre sujet, mes frères, par ceux de chez Chloé qu'il y a des disputes parmi vous.
1:12	Mais je dis ceci, parce que chacun de vous dit : Moi, je suis vraiment de Paulos ! Et moi, d'Apollos ! Et moi, de Kephas ! Et moi, du Mashiah !
1:13	Le Mashiah est-il divisé ? Paulos a-t-il été crucifié en votre faveur ? Ou est-ce pour le nom de Paulos que vous avez été baptisés ?
1:14	Je rends grâce à Elohîm de ce que je n'ai baptisé aucun de vous, excepté Crispus et Gaïos,
1:15	afin que personne ne dise que j’ai baptisé pour mon nom.
1:16	Et j'ai aussi baptisé la maison de Stéphanas. Pour le reste, je ne sais pas si j'ai baptisé quelqu'un d'autre.
1:17	Car Mashiah ne m'a pas envoyé baptiser, mais évangéliser, non pas avec des discours de la sagesse humaine, afin que la croix du Mashiah ne soit pas rendue inutile.
1:18	Car la parole de la croix est en effet une folie pour ceux qui périssent, mais pour nous qui sommes sauvés, elle est la puissance d'Elohîm.
1:19	Car il est écrit : Je détruirai la sagesse des sages et j'anéantirai l'intelligence des intelligents<!--Es. 29:14.-->.
1:20	Où est le sage ? Où est le scribe ? Où est le disputeur de cet âge ? Elohîm n'a-t-il pas prouvé que la sagesse de ce monde est folle ?
1:21	Car puisque, dans la sagesse d'Elohîm, le monde n’a pas connu Elohîm à travers la sagesse, il a plu à Elohîm de sauver les croyants à travers la folie de la prédication. 
1:22	Et tandis que les Juifs demandent des signes et que les Grecs cherchent la sagesse,
1:23	mais nous, nous prêchons Mashiah crucifié, scandale en effet pour les Juifs et folie pour les Grecs,
1:24	mais pour les appelés eux-mêmes, tant Juifs que Grecs, Mashiah, puissance d'Elohîm et sagesse d'Elohîm.
1:25	Parce que la folie d'Elohîm est plus sage que les humains, et la faiblesse d'Elohîm est plus forte que les humains.
1:26	Car vous voyez votre appel, frères : qu’il n’y a pas beaucoup de sages selon la chair, ni beaucoup de puissants, ni beaucoup de nobles<!--« Bien né », « haute naissance ».-->.
1:27	Mais Elohîm a choisi les folies du monde pour faire honte aux sages, et Elohîm a choisi les faibles du monde pour faire honte aux forts.
1:28	Et Elohîm a choisi ceux qui dans le monde sont de basse naissance et les méprisés, même ceux qui ne sont pas, pour rendre vains ceux qui sont,
1:29	afin qu'aucune chair ne se glorifie devant lui.
1:30	Or c'est à partir de lui que vous êtes en Mashiah Yéhoshoua, qui est devenu pour vous de la part d'Elohîm, sagesse, justice, sanctification et rédemption,
1:31	afin que, selon qu'il est écrit, celui qui se glorifie, se glorifie dans le Seigneur<!--Jé. 9:23.-->.

## Chapitre 2

2:1	Moi aussi, frères, quand je suis venu chez vous, ce n'est pas avec une supériorité de parole ou de sagesse que je suis venu vous annoncer le témoignage d'Elohîm.
2:2	Car je n'ai pas jugé bon de savoir autre chose parmi vous, excepté Yéhoshoua Mashiah, et celui-ci crucifié.
2:3	Moi-même, je suis arrivé chez vous dans la faiblesse, dans la crainte et dans un grand tremblement.
2:4	Et ma parole et ma prédication n'ont pas été en paroles persuasives de la sagesse humaine, mais en démonstration d'Esprit et de puissance,
2:5	afin que votre foi ne soit pas en la sagesse humaine, mais en la puissance d'Elohîm.
2:6	Or nous prêchons une sagesse parmi les parfaits, sagesse non de cet âge, ni des chefs de cet âge qui sont rendus vains.
2:7	Mais nous prêchons la sagesse d'Elohîm en mystère, celle qui a été cachée, qu'Elohîm avant les âges, avait prédestinée pour notre gloire,
2:8	aucun des chefs de cet âge ne l’a connue, car s'ils l'avaient connue, ils n'auraient pas crucifié le Seigneur de gloire.
2:9	Mais, selon qu'il est écrit : Ce sont des choses que l’œil n’a pas vues, et que l’oreille n’a pas entendues, et qui ne sont pas montées au cœur de l'être humain, des choses qu'Elohîm a préparées pour ceux qui l'aiment<!--Es. 64:3.-->.
2:10	Mais Elohîm nous les a révélées par le moyen de son Esprit. Car l'Esprit sonde toutes choses, même les profondeurs<!--Ep. 3:18.--> d'Elohîm.
2:11	Car lequel des humains a connu les choses de l'être humain, excepté l'esprit<!--Job 32:8.--> de l'être humain qui est en lui ? De même aussi, personne n’a connu les choses d'Elohîm, excepté l'Esprit d'Elohîm.
2:12	Or nous, nous n'avons pas reçu l'esprit du monde, mais l'Esprit qui issu d'Elohîm, afin que nous connaissions les choses qu'Elohîm nous a gracieusement données,
2:13	desquelles aussi nous parlons, non pas en paroles enseignées par la sagesse humaine, mais enseignées par l'Esprit Saint, interprétant les choses spirituelles aux spirituels.
2:14	Mais l'être humain animal<!--Depuis la chute d'Adam, l'être humain est devenu un animal parmi d'autres. En se convertissant, il reçoit du Seigneur le pouvoir de dominer son côté animal dont les pulsions le poussent sans cesse au péché (1 Co. 9:27 ; Ga. 5:16-24). Une personne non convertie n'a pas le contrôle total sur ses pulsions animales et vit par conséquent dans le désordre. Voir 1 Co. 15:44-46 ; Ja. 3:15 ; Jud. 1:19.--> ne reçoit pas les choses de l'Esprit d'Elohîm, car elles sont une folie pour lui, il ne peut les connaître non plus, parce que c'est spirituellement qu'on en juge.
2:15	Mais celui qui est spirituel<!--L'être humain spirituel est celui dont l'esprit est régénéré et qui marche par l'Esprit d'Elohîm. Il a la pensée du Mashiah et porte le fruit de l'Esprit.--> juge en effet de toutes choses et n'est jugé lui-même par personne.
2:16	Car qui a connu la pensée du Seigneur pour l’instruire<!--Es. 40:13.--> ? Mais nous, nous avons la pensée du Mashiah.

## Chapitre 3

3:1	Et moi, frères, je n'ai pas pu vous parler comme à des spirituels, mais comme à des charnels, comme à des enfants en Mashiah<!--L'être humain charnel, bien qu'ayant déjà connu le Mashiah, ne se laisse pas gouverner par l'Esprit d'Elohîm (Ga. 5:16-21), mais écoute son côté animal. Il est donc dominé par les passions de sa chair. C'est un enfant en Mashiah, littéralement un « ignorant », raison pour laquelle il est comparé à un esclave (Ga. 4:1).-->.
3:2	Je vous ai donné du lait à boire, et non quelque chose à manger, car vous n'en étiez pas encore capables. Et même maintenant, vous n'en êtes pas encore capables, 
3:3	car vous êtes encore charnels. Car puisqu'il y a parmi vous de la jalousie, des disputes et des divisions, n'êtes-vous pas charnels et ne marchez-vous pas selon l'être humain ?
3:4	Car quand l'un dit : Moi, je suis vraiment de Paulos ! Et un autre : Moi, d'Apollos ! N'êtes-vous pas charnels ?
3:5	Qui donc est Paulos, et qui est Apollos ? Mais l'un ou l'autre des serviteurs par le moyen desquels vous avez cru, selon que le Seigneur l'a donné à chacun.
3:6	J'ai planté, Apollos a arrosé, mais c'est Elohîm qui a fait croître.
3:7	Ainsi, ce n'est pas celui qui plante qui est quelque chose, ni celui qui arrose, mais Elohîm qui fait croître.
3:8	Or celui qui plante et celui qui arrose ne sont qu'un, mais chacun recevra sa propre récompense selon son propre travail.
3:9	Car nous sommes des compagnons d'Elohîm dans l'œuvre<!--Un compagnon dans le travail, un compagnon d'œuvre.-->. Vous êtes le champ cultivé d'Elohîm et la construction d'Elohîm.
3:10	Selon la grâce d'Elohîm qui m'a été donnée, j'ai posé le fondement comme un sage architecte, et un autre édifie dessus. Mais que chacun discerne comment il édifie dessus.
3:11	Car personne ne peut poser un autre fondement que celui qui est déjà posé, lequel est Yéhoshoua ha Mashiah.
3:12	Mais si, sur ce fondement, quelqu'un édifie de l'or, de l'argent, des pierres précieuses, du bois, du foin, du chaume, 
3:13	l'œuvre de chacun sera manifestée, car le jour la fera connaître, parce qu'elle se révèle dans le feu et le feu éprouvera ce qu’est l’œuvre de chacun.
3:14	Si l'œuvre que quelqu'un a édifiée dessus demeure<!--L'œuvre qu'Elohîm fait demeure. Voir Jn. 15:16.-->, il recevra la récompense.
3:15	Si l'œuvre de quelqu'un est brûlée, il en souffrira la perte. Mais pour lui, il sera ainsi sauvé, mais comme à travers un feu.
3:16	Ne savez-vous pas que vous êtes le temple<!--Les véritables temples d'Elohîm ne sont pas des bâtiments, mais les chrétiens scellés du Saint-Esprit (Ep. 1:13, 4:30). Voir aussi Es. 66:1 ; Ac. 17:24 ; 1 Co. 6:19.--> d'Elohîm et que l'Esprit d'Elohîm habite en vous ?
3:17	Si quelqu'un détruit le temple d'Elohîm, celui-ci, l'Elohîm le détruira, car le temple d'Elohîm est saint et c'est ce que vous êtes.
3:18	Que personne ne se trompe lui-même : si quelqu'un parmi vous pense être sage en cet âge, qu'il devienne fou afin de devenir sage.
3:19	Car la sagesse de ce monde est une folie devant Elohîm, car il est écrit : Il surprend les sages dans leur ruse<!--Job 5:13.-->.
3:20	Et encore : Le Seigneur connaît les raisonnements des sages, qu’ils sont vains<!--Ps. 94:11.-->.
3:21	Que personne donc ne se glorifie dans les humains, car toutes choses sont à vous,
3:22	soit Paulos, soit Apollos, soit Kephas, soit le monde, soit la vie, soit la mort, soit les choses présentes, soit les choses imminentes, toutes choses sont à vous,
3:23	et vous au Mashiah, et Mashiah à Elohîm.

## Chapitre 4

4:1	Ainsi, qu'on nous estime comme des serviteurs du Mashiah et des gestionnaires des mystères d'Elohîm.
4:2	Mais, du reste, ce que l’on cherche dans les gestionnaires, c’est que chacun soit trouvé fidèle.
4:3	Mais pour moi, c'est une moindre chose que je sois jugé par vous ou par un jour<!--Le jour est considéré comme le moment d'obtention de l'indulgence, car le crime et les mauvaises actions sont perpétrés la nuit, dans l'obscurité. Voir 1 Co. 3:12-13.--> humain. Bien plus, je ne me juge pas moi-même, 
4:4	car je n'ai rien sur ma conscience. Mais ce n'est pas en cela que je suis justifié. Mais celui qui me juge, c'est le Seigneur.
4:5	C'est pourquoi ne jugez pas avant le temps, jusqu’à ce que vienne le Seigneur qui illuminera<!--« Éclairer », « illuminer ».--> les secrets de la ténèbre et manifestera les desseins des cœurs. Et alors, pour chacun, la louange viendra d'Elohîm.
4:6	Mais c'est à cause de vous, frères, que j'ai tourné figurément ces choses sur moi et sur Apollos, afin qu'en nous, vous appreniez à ne pas penser au-delà de ce qui est écrit, de peur que, individuellement, vous ne vous enfliez en faveur de l'un contre l'autre.
4:7	Car qui est-ce qui te distingue ? Mais qu'as-tu que tu n'aies reçu ? Et si tu l'as reçu, pourquoi te glorifies-tu comme si tu ne l'avais pas reçu<!--Les diverses grâces qu'Elohîm accorde à ses enfants doivent les amener à l'humilité.--> ?
4:8	Vous êtes déjà rassasiés ! Vous êtes déjà riches<!--2 Co. 6:10, 8:9, 9:11.--> ! Vous êtes des rois sans nous ! Si seulement vous étiez vraiment des rois ! Alors nous aussi nous régnerions avec vous !
4:9	Car je pense qu'Elohîm nous a exposés les derniers, nous les apôtres, comme des condamnés à mort, puisque nous sommes devenus un spectacle<!--Un théâtre, lieu où se déroulent des jeux et des spectacles dramatiques, et où se tiennent des assemblées publiques (les Grecs utilisant le théâtre comme forum). Ac. 19:29 et 31.--> pour le monde, et pour les anges et pour les humains.
4:10	Nous sommes fous à cause du Mashiah, mais vous êtes sages en Mashiah ! Nous sommes faibles, mais vous êtes forts ! Vous êtes dans l'estime mais nous sommes déshonorés !
4:11	Jusqu'à cette heure, nous souffrons la faim et la soif, et nous sommes pauvrement vêtus, et frappés à coups de poing et sans domiciles fixes.
4:12	Et nous nous fatiguons à travailler de nos propres mains. Injuriés, nous bénissons ; persécutés, nous supportons.
4:13	Calomniés, nous encourageons. Nous sommes devenus comme les déchets du monde, comme les raclures<!--Vient du grec « peripsema » qui signifie « ce qui est essuyé », « la saleté qui est enlevée », « déchets du nettoyage ». Les Athéniens, pour repousser les calamités publiques, jetaient chaque année un criminel à la mer, en offrande à Poséidon (dieu de la mer dans la mythologie grecque). Ce terme fut utilisé pour une offrande expiatoire, une rançon. Il est utilisé pour un homme qui s'offre pour le salut des autres.--> de tous, jusqu'à maintenant.
4:14	Je n'écris pas ces choses pour vous faire honte, mais je vous avertis comme mes enfants bien-aimés.
4:15	Car même si vous avez 10 000 pédagogues<!--Un tuteur, gardien et guide de garçons. Parmi les Grecs et les Romains, le mot était appliqué aux esclaves dignes de confiance qui étaient chargés de veiller à la vie et à la moralité des garçons appartenant aux classes supérieures. Les garçons ne pouvaient faire le moindre pas hors de la maison sans ces tuteurs tant qu'ils n'avaient pas atteint leur majorité.--> en Mashiah, vous n'avez pourtant pas beaucoup de pères, car c'est moi qui vous ai engendrés dans le Mashiah Yéhoshoua par le moyen de l'Évangile.
4:16	Je vous supplie donc, devenez mes imitateurs.
4:17	C'est pour cela que je vous ai envoyé Timotheos, qui est mon fils bien-aimé, et qui est fidèle dans le Seigneur, afin qu'il vous rappelle quelles sont mes voies en Mashiah et comment j'enseigne partout dans chaque assemblée.
4:18	Mais quelques-uns se sont enflés, comme si je ne devais pas venir chez vous.
4:19	Mais je viendrai rapidement chez vous, si le Seigneur le veut, et je connaîtrai non la parole, mais la puissance de ceux qui se sont enflés.
4:20	Car le Royaume d'Elohîm n'est pas en parole, mais en puissance.
4:21	Que voulez-vous ? Que je vienne chez vous avec la verge, ou avec amour et dans un esprit de douceur ?

## Chapitre 5

5:1	On entend tout à fait parler de relation sexuelle illicite parmi vous, et une relation sexuelle illicite telle qu'elle n'a pas même de nom parmi les nations ; c'est ainsi que l'un de vous a la femme de son père<!--L'inceste est interdit par la loi (Lé. 18:6-8).-->.
5:2	Et vous, vous êtes enflés ! Et vous ne vous êtes pas plutôt lamentés, afin que celui qui a commis cette action soit ôté du milieu de vous !
5:3	Car pour moi, étant en effet absent de corps, mais présent en esprit, j'ai déjà jugé comme si j'étais présent, celui qui a commis une telle action.
5:4	Dans le nom de notre Seigneur Yéhoshoua Mashiah, vous et mon esprit étant assemblés avec la puissance de notre Seigneur Yéhoshoua Mashiah,
5:5	qu'un tel homme soit livré à Satan<!--À travers son Assemblée, Elohîm peut livrer un pécheur à Satan pour la destruction de sa chair, afin de l'amener à la repentance. En l'occurrence, Paulos (Paul) appliquait dans les assemblées un usage courant en Grèce : l'ostracisme. Il s'agissait donc d'exclure la personne de l'assemblée de manière temporaire ou définitive. Par cet acte, il a peut-être aussi fait référence à Iyov (Job) qui, bien qu'étant juste, fut livré par Elohîm à Satan en permettant qu'il soit touché par divers maux dont la maladie (Job 1:12), etc. Paulos, l'apôtre, a usé de cette même autorité à l'égard d'Alexandros le forgeron (1 Ti. 1:20).--> pour la destruction de la chair, afin que l'esprit soit sauvé au jour du Seigneur Yéhoshoua.
5:6	Il n’est pas beau, votre sujet de gloire ! Ne savez-vous pas qu'un peu de levain<!--Le levain symbolise le péché, en particulier l'orgueil puisqu'il fait enfler (Pr. 21:4 ; 1 Co. 4:18, 13:4), l'hypocrisie et les fausses doctrines (Mt. 16:11-12 ; Lu. 12:1).--> fait lever toute la pâte ?
5:7	Alors nettoyez complètement le vieux levain, afin que vous soyez une nouvelle pâte, puisque vous êtes sans levain, car le Mashiah, notre Pâque<!--Ex. 12.-->, a été sacrifié pour nous.
5:8	C'est pourquoi célébrons donc la fête, non avec du vieux levain, non avec un levain de méchanceté et de malice, mais avec les pains sans levain de la sincérité et de la vérité.
5:9	Je vous ai écrit dans la lettre de ne pas vous mêler<!--« Mêler » vient du grec « sunanamignumi » qui signifie « mêler ensemble », « se tenir en compagnie avec », « être intime avec quelqu'un », « avoir des relations », « être en communication » (Ps. 1:1 ; 1 Co. 15:33 ; Ro. 16:17-18 ; Tit. 3:10).--> avec les hommes qui se prostituent<!--Vient du grec « pornos » qui signifie : « un homme qui prostitue son corps et le loue à la convoitise d'un autre », « un prostitué », « homme qui se complaît dans la relation sexuelle illicite, un homme qui fornique ».-->,
5:10	mais non pas d'une manière absolue avec les hommes qui se prostituent de ce monde, ou avec les cupides, ou les ravisseurs, ou les idolâtres - autrement, vous devez donc sortir du monde.
5:11	Mais maintenant, ce que je vous ai écrit, c'est de ne pas vous mêler avec quelqu'un qui se nomme frère, s'il est un homme qui se prostitue, ou cupide, ou idolâtre, ou railleur, ou ivrogne, ou ravisseur, de ne pas même manger avec un tel homme.
5:12	Car qu’ai-je à juger ceux du dehors ? N'est-ce pas ceux du dedans que vous avez à juger ?
5:13	Mais ceux de dehors, l'Elohîm les jugera et vous ôterez les méchants du milieu de vous-mêmes.

## Chapitre 6

6:1	L'un d'entre vous, ayant une affaire avec un autre, ose-t-il aller en jugement devant les injustes et non devant les saints ?
6:2	Ne savez-vous pas que les saints jugeront le monde<!--L'Assemblée jugera les nations. Les douze apôtres jugeront Israël (Mt. 19:28 ; Lu. 22:30).--> ? Or si le monde est jugé par vous, êtes-vous indignes des plus petits jugements ?
6:3	Ne savez-vous pas que nous jugerons les anges<!--Le mot « ange » vient du grec « aggelos » et veut dire « messager, envoyé, ange ». Ce terme s'applique donc aussi bien aux hommes qu'aux créatures spirituelles.--> ? Combien plus les choses concernant cette vie ?
6:4	Si donc vous avez à prononcer des jugements sur les affaires de cette vie, vous faites siéger en effet ceux qui sont les moins considérés dans l’Assemblée !
6:5	Je le dis à votre honte. N'y a-t-il donc pas de sages parmi vous ? Non, pas même un seul qui puisse juger entre frères ?
6:6	Mais un frère va en jugement avec un frère, et cela devant des incrédules !
6:7	C'est donc déjà une défaite complète en effet en vous, que vous ayez des jugements les uns contre les autres. Pourquoi ne vous laissez-vous pas plutôt faire du tort ? Pourquoi ne vous laissez-vous pas plutôt spolier ?
6:8	Mais c’est vous qui faites du tort et qui spoliez, et cela envers des frères !
6:9	Ou bien ne savez-vous pas que les injustes n'hériteront pas le Royaume d'Elohîm ? Ne vous égarez pas : ni les hommes qui se prostituent, ni les idolâtres, ni les adultères, ni les efféminés<!--Il est question d'un garçon ayant des relations homosexuelles avec un homme ou d'un homme qui soumet son corps à de l'impudicité hors nature.-->, ni les homosexuels,
6:10	ni les voleurs, ni les avares, ni les ivrognes, ni les railleurs, ni les ravisseurs, n'hériteront le Royaume d'Elohîm.
6:11	Et c'est là ce que vous étiez, certains d'entre vous. Mais vous avez été lavés, mais vous avez été sanctifiés, mais vous avez été justifiés dans le nom du Seigneur Yéhoshoua et dans l’Esprit de notre Elohîm.
6:12	Toutes choses sont légales pour moi, mais toutes choses ne conviennent pas. Toutes choses sont légales pour moi, mais je ne serai assujetti sous la puissance d'aucune chose.
6:13	Les aliments sont pour le ventre et le ventre pour les aliments, et Elohîm détruira l'un comme les autres. Mais le corps n'est pas pour la relation sexuelle illicite, mais pour le Seigneur, et le Seigneur pour le corps.
6:14	Mais Elohîm qui a réveillé le Seigneur, nous réveillera<!--Vient du grec « exegeiro » qui signifie « réveiller hors de ».--> aussi par le moyen de sa puissance.
6:15	Ne savez-vous pas que vos corps sont les membres du Mashiah ? Prenant donc les membres du Mashiah, en ferai-je les membres d'une prostituée ? Que cela n'arrive jamais !
6:16	Ou bien ne savez-vous pas que celui qui se joint à la prostituée devient un même corps avec elle ? Car il est dit : Les deux deviendront une seule chair<!--Ge. 2:24.-->.
6:17	Mais celui qui se joint au Seigneur est avec lui un seul esprit.
6:18	Fuyez la relation sexuelle illicite<!--Voir Mt. 5:32.-->. Tout péché qu'un être humain commet est hors du corps, mais celui qui se prostitue pèche contre son propre corps.
6:19	Ou ne savez-vous pas que votre corps est le temple du Saint-Esprit qui est en vous, et que vous avez reçu d'Elohîm, et que vous ne vous appartenez pas à vous-mêmes ?
6:20	Car vous avez été achetés<!--Vient du grec « agorazo » qui signifie « être sur une place de marché, s'y activer », « y faire des affaires, acheter ou vendre », « pour les désœuvrés : hanter cette place, y flâner ».--> à un prix. Glorifiez donc Elohîm dans votre corps et dans votre esprit, qui appartiennent à Elohîm.

## Chapitre 7

7:1	Mais, pour ce qui concerne les choses au sujet desquelles vous m'avez écrit, il est bon pour l'homme de ne pas toucher de femme.
7:2	Mais à cause des relations sexuelles illicites, que chacun ait sa femme, et que chacune ait son propre mari.
7:3	À la femme, que le mari paye ce qui lui est dû, et de même aussi, la femme au mari.
7:4	La femme n'a pas de pouvoir sur son propre corps, mais le mari. De même aussi le mari n'a pas de pouvoir sur son propre corps, mais la femme.
7:5	Ne vous privez<!--« Frauder », « voler », « spolier ou dépouiller ».--> pas l'un de l'autre, excepté d'un commun accord pour un temps, afin de cesser le travail<!--« Être exempt de travail », « être aux loisirs », « être désœuvré », « avoir des loisirs pour une chose », « se donner à une chose ». Vient d'une racine qui signifie « absence de travail ».--> pour le jeûne et la prière, mais après cela retournez ensemble, de peur que Satan ne vous tente par votre manque d'auto-contrôle.
7:6	Mais je dis cela par indulgence, et non comme un ordre,
7:7	car je voudrais que tous les humains soient comme moi, mais chacun a reçu d'Elohîm en effet son don de grâce particulier, l'un d'une manière, l'autre d'une autre.
7:8	Mais je dis aux non-mariés<!--célibataires.--> et aux veuves, qu'il leur est bon de demeurer comme moi.
7:9	Mais s’ils ne se maîtrisent<!--être maître de soi, être continent, se dominer, montrer sa maîtrise de soi, se conduire avec tempérance, une image des athlètes se préparant à des jeux et s'abstenant de certains aliments, de vin, et de realtions sexuelles.--> pas, qu'ils se marient, car il vaut mieux se marier que de brûler.
7:10	Mais à ceux qui sont mariés, je leur ordonne, non pas moi, mais le Seigneur, que la femme ne se sépare pas de son mari.
7:11	Et même si elle est séparée, qu'elle demeure non-mariée<!--célibataire.--> ou qu'elle se réconcilie avec son mari. Que le mari aussi ne quitte pas sa femme.
7:12	Mais aux autres, ce n'est pas le Seigneur, c'est moi qui dis : Si un frère a une femme incrédule et qu'elle consente à habiter avec lui, qu'il ne la quitte pas.
7:13	Et si une femme a un mari incrédule et qu'il consente à habiter avec elle, qu'elle ne le quitte pas.
7:14	Car le mari incrédule est sanctifié par la femme, et la femme incrédule est sanctifiée par le mari ; parce qu'autrement vos enfants seraient impurs, mais maintenant ils sont saints.
7:15	Mais si l'incrédule se sépare, qu'il se sépare ! Le frère ou la sœur n'est pas esclave<!--rendre esclave de, réduire à l'esclavage. Métaphore : se donner entièrement à quelqu'un en services et besoins, se rendre esclave de--> en de tels cas<!--de ce type, de cette sorte, comme ceci, de tel genre, de telle sorte.-->, mais Elohîm nous a appelés à la paix.
7:16	Car que sais-tu, femme, si tu sauveras ton mari ? Ou que sais-tu, mari, si tu sauveras ta femme ?
7:17	Sinon, comme l'Elohîm l’a imparti<!--accorder, donner. Vient d'une racine qui signifie : lot, part.--> à chacun, comme le Seigneur a appelé chacun, qu'il marche ainsi. Et c'est ainsi que je l'ordonne dans toutes les assemblées.
7:18	Quelqu'un a-t-il été appelé étant circoncis ? Qu’il ne se fasse pas épispasmer<!--Vient du grec « epispaomai » qui signifie « ne pas devenir incirconcis ». Certains Juifs hellénisés se faisaient chirurgicalement reconstituer le prépuce : cette opération se nommait « épispasme ». Aux jours d'Antiochos IV Épiphane (215 - 164 av. J.-C.), certains Juifs voulant échapper aux persécutions cachaient le signe de leur nationalité, la circoncision, en se faisant reproduire artificiellement le prépuce, par une opération chirurgicale qui étendait la peau restante. Paulos fait ici allusion à cette pratique. À l'origine, cette pratique fut imposée aux Hébreux par Antiochos IV Épiphane (règne : 175 - 164 av. J.-C.) qui voulait les forcer à adopter la culture grecque. Du temps de Paulos, certains Juifs convertis à Yéhoshoua (Jésus), désireux de marquer leur rupture avec le judaïsme, adoptèrent volontairement la « décirconcision ». Par la suite, elle fut d'usage courant chez les Juifs pour échapper aux persécutions. Il convient donc de noter que les Écritures nous parlent de plusieurs types de circoncis et d'incirconcis avec des mots grecs spécifiques à chaque situation.\\1 - « Peritome » : ceux qui sont passés par le rite de la circoncision, en particulier les Juifs (Ac. 10:45 ; Ga. 2:8 ; Ro. 3:30 ; Ep. 2:11 ; Ph. 3:5 ; Col. 4:11 ; Tit. 1:10 ; etc.).\\2 - « Katatome » : « couper, une mutilation ». Ce terme est utilisé une seule fois en Ph. 3:2 pour désigner les faux circoncis.\\3 - « Akrobustia » : ne pas être circoncis, un Gentil (gens des nations), un païen (Ac. 11:3 ; Ro. 2:26-27 ; Ep. 2:11 ; Col. 3:11 ; etc.).\\4 - « Epispaomai » : ne pas redevenir incirconcis, littéralement « tirer sur le prépuce » ; terme que l'on retrouve uniquement en 1 Co. 7:18. On appelait ces hommes des « epispatikos ». Dans ce passage, Paulos demande aux Juifs de ne pas essayer de cacher le signe visible de leur judéité en devenant des faux incirconcis dans la chair, et aux nations de ne pas s'imposer la circoncision.-->. Quelqu'un a-t-il été appelé dans l'incirconcision ? Qu'il ne se fasse pas circoncire.
7:19	La circoncision n'est rien, et l’incirconcision n'est rien, mais l'observation des commandements d'Elohîm...
7:20	Que chacun demeure dans l'appel dans lequel il a été appelé !
7:21	As-tu été appelé étant esclave ? Ne t'en inquiète pas mais, si tu peux aussi devenir libre, uses-en plutôt.
7:22	Car l'esclave appelé dans le Seigneur est un affranchi du Seigneur. De même aussi, celui qui a été appelé étant libre est un esclave du Mashiah.
7:23	Vous avez été achetés<!--De. 32:6.--> à un prix, ne devenez pas esclaves des humains.
7:24	Chacun dans ce qu’il a été appelé, frères, que ce soit dans ceci qu’il demeure auprès d'Elohîm.
7:25	Mais pour ce qui concerne les vierges, je n'ai pas de commandement du Seigneur, mais je donne un avis comme ayant obtenu miséricorde du Seigneur pour être fidèle.
7:26	J'estime donc que cela est bon, à cause de la détresse présente, qu'il est bon à un être humain d'être ainsi.
7:27	Es-tu lié à une femme ? Ne cherche pas la rupture du lien<!--« Relâcher », « rendre libre ».-->. Es-tu délié<!--« Desserrer une personne (ou une chose) attachée ou liée », « relâcher une limite c'est-à-dire délier des liens, relâcher, rendre libre ».--> d’une femme ? Ne cherche pas de femme !
7:28	Mais si tu t'es marié, tu n'as pas péché, et si la vierge s'est mariée, elle n'a pas péché. Mais de telles personnes auront la tribulation dans la chair, mais moi, je vous épargne.
7:29	Or ce que je dis, frères, c’est que la mesure de temps qui reste a été raccourcie, afin que ceux aussi qui ont des femmes soient comme n’en ayant pas,
7:30	et ceux qui pleurent comme ne pleurant pas, et ceux qui se réjouissent comme ne se réjouissant pas, et ceux qui achètent comme ne possédant pas,
7:31	et ceux qui usent de ce monde comme n'en abusant pas, car la figure de ce monde passe.
7:32	Or je veux que vous soyez sans soucis. Le non-marié<!--célibataire.--> s’inquiète des choses du Seigneur, et de la manière dont il plaira au Seigneur.
7:33	Mais celui qui est marié s’inquiète des choses de ce monde, de la manière dont il plaira à sa femme, 
7:34	il est partagé. La femme et la vierge : la non-mariée s’inquiète des choses du Seigneur, afin d'être sainte et de corps et d'esprit. Mais celle qui est mariée s’inquiète des choses du monde, de la manière dont elle plaira à son mari.
7:35	Or je dis cela pour votre propre avantage, non pas pour jeter sur vous un nœud coulant<!--Nœud coulant, collet par lequel une personne ou une chose est saisie, ou suspendue. Jeter un nœud coulant sur quelqu'un : terme de chasse ou de guerre qui permet de forcer celui qui est saisi d'obéir à celui qui l'a pris au piège.-->, mais en vue de ce qui est honorable, et afin que vous soyez dévoués au Seigneur, sans distraction.
7:36	Mais si quelqu'un pense agir de façon malséante envers sa vierge en la laissant dépasser la fleur de l'âge, et qu’il doit en être ainsi, qu’il fasse ce qu’il veut ! Il ne pèche pas. Qu’ils se marient !
7:37	Mais celui qui demeure ferme dans son cœur, ne ressentant aucune nécessité, mais en ayant le pouvoir de sa propre volonté, et qui décide en son cœur de se garder vierge, fait bien.
7:38	C’est pourquoi aussi celui qui donne en mariage fait bien, mais celui qui ne donne pas en mariage fait mieux.
7:39	La femme est liée par la torah à son mari aussi longtemps qu’il est vivant<!--Elohîm est contre le divorce. Pour le Seigneur, le mariage doit être un engagement à vie (Mal. 2:16 ; Ro. 7:1-3).-->, mais si son mari s’endort<!--Ce qui fait dormir, s'endormir.-->, elle est libre de se marier avec qui elle veut, dans le Seigneur seulement.
7:40	Mais elle est plus bénie si elle demeure ainsi, selon mon avis. Or j'estime que j'ai aussi l'Esprit d'Elohîm.

## Chapitre 8

8:1	Mais pour ce qui concerne les choses sacrifiées aux idoles<!--À Corinthe, on offrait des viandes sacrifiées aux idoles en rituel. À ces occasions, certaines parties des animaux sacrifiés étaient déposées sur l'autel de l'idole, d'autres étaient données aux prêtres et aux adorateurs, qui les mangeaient lors d'un repas ou d'un festin, soit dans le temple, soit dans une maison particulière. Certains morceaux de la chair offerte aux idoles étaient ensuite apportés au marché pour être vendus (Da. 1).-->, nous savons que nous avons tous de la connaissance. La connaissance enfle, mais l'amour édifie.
8:2	Mais si quelqu'un croit savoir quelque chose, il n'a encore rien connu comme il faut connaître.
8:3	Mais si quelqu'un aime Elohîm, celui-là est connu par lui.
8:4	Donc, pour ce qui concerne l'action de manger des choses sacrifiées aux idoles, nous savons qu'une idole n'est rien dans le monde et qu'il n'y a aucun autre Elohîm, excepté un seul<!--Paulos (Paul) affirme avec force que l'Elohîm Créateur n'est pas mélangé avec d'autres divinités. Voir De. 6:4.-->.
8:5	Car même s'il est vrai qu'il y a des êtres qui sont appelés elohîm, soit dans le ciel, soit sur la Terre (comme, en effet, il y a beaucoup d'elohîm et beaucoup de seigneurs<!--César Auguste était aussi appelé seigneur. Voir Ac. 25:26.-->),
8:6	mais pour nous, il n'y a qu'un seul Elohîm<!--Ro. 3:30.-->, le Père, de qui viennent toutes choses, et nous sommes pour lui, et un seul Seigneur, Yéhoshoua Mashiah, par le moyen duquel sont toutes choses et nous par son moyen.
8:7	Mais cette connaissance n'est pas en tous. Or quelques-uns, ayant jusqu’à maintenant conscience de l’idole, mangent une chose comme sacrifiée aux idoles, et leur conscience qui est faible en est souillée.
8:8	Or ce n'est pas un aliment qui nous place près d'Elohîm. Car si nous en mangeons, nous n'avons rien de plus, et si nous n'en mangeons pas, nous n'avons rien de moins.
8:9	Mais prenez garde, de peur que ce pouvoir que vous avez ne devienne peut-être un scandale pour les faibles.
8:10	Car si quelqu'un te voit, toi qui as de la connaissance, à table dans le temple des idoles, la conscience de celui qui est faible ne sera-t-elle pas bâtie au point de manger les choses sacrifiées aux idoles ?
8:11	Et le frère qui est faible, à cause duquel Mashiah mourut, périra par ta connaissance.
8:12	Or en péchant ainsi contre les frères et en blessant leur conscience qui est faible, vous péchez contre Mashiah.
8:13	C'est pourquoi, si un aliment scandalise mon frère, je ne mangerai pas de chair<!--D'un animal sacrifié.-->, à jamais, pour ne pas scandaliser mon frère.

## Chapitre 9

9:1	Ne suis-je pas apôtre ? Ne suis-je pas libre ? N'ai-je pas vu Yéhoshoua Mashiah notre Seigneur ? N'êtes-vous pas mon œuvre dans le Seigneur ?
9:2	Si pour d'autres je ne suis pas apôtre, je le suis au moins pour vous, car vous êtes le sceau de mon apostolat dans le Seigneur.
9:3	C'est là ma plaidoirie contre ceux qui me jugent.
9:4	N'avons-nous pas le pouvoir de manger et de boire ?
9:5	N'avons-nous pas pouvoir de mener avec nous une sœur, une femme, comme font les autres apôtres, et les frères du Seigneur, et Kephas ?
9:6	Ou bien, est-ce que moi seul et Barnabas nous n'avons pas le pouvoir de ne pas travailler ?
9:7	Qui fait une expédition militaire à sa propre solde<!--« La paye d'un soldat », « la part du soldat donnée en supplément à la paye (les rations) et la monnaie dans laquelle il est payé », « salaire ». Voir Lu. 3:14.--> ? Qui est-ce qui plante une vigne et n'en mange pas le fruit ? Qui est-ce qui fait paître un troupeau et ne mange pas du lait du troupeau ?
9:8	Est-ce en humain que je parle de ces choses, ou bien la torah aussi ne dit-elle pas ces choses ?
9:9	Car il est écrit dans la torah de Moshé : Tu ne muselleras pas le bœuf qui foule le grain<!--De. 25:4.-->. Elohîm se met-il en peine des bœufs ?
9:10	Ou n’est-ce pas sûrement à cause de nous qu’il parle ainsi ? Car c'est à cause de nous qu'il a été écrit que celui qui laboure doit labourer avec espérance, et celui qui foule le grain, avec espérance de participer à l'espérance.
9:11	Si nous avons semé en vous les choses spirituelles, est-ce une grande chose si nous moissonnons vos choses charnelles ?
9:12	Si d'autres participent à ce pouvoir sur vous, pourquoi pas plutôt nous-mêmes ? Cependant, nous n'avons pas usé de ce pouvoir, mais nous supportons tout, afin de ne pas faire obstacle à l'Évangile du Mashiah.
9:13	Ne savez-vous pas que ceux qui exercent les fonctions sacrées mangent de ce qui vient du temple, et que ceux qui servent assidûment à l'autel ont part à l'autel<!--No. 18:8-31.--> ?
9:14	Ainsi le Seigneur a aussi ordonné à ceux qui annoncent l’Évangile de vivre à partir de l’Évangile.
9:15	Mais pour moi, je n’ai usé d’aucune de ces choses, et je n'ai pas écrit ces choses pour que cela se passe ainsi avec moi, car pour moi, il serait plus beau de mourir, que si quelqu’un rendait vain mon sujet de gloire. 
9:16	Car si je prêche l’Évangile, ce n'est pas pour moi un sujet de gloire, c'est parce que la nécessité m'en est imposée, et malheur à moi si je ne prêche pas l’Évangile !
9:17	Car, si je fais cela volontairement, j'en ai un salaire ; mais si je le fais contre ma volonté, c’est une gestion<!--Lu. 16:2 ; Ep. 3:2 ; Col. 1:25.--> qui m’est confiée.
9:18	Quel est donc mon salaire<!--Lu. 10:7 ; 1 Ti. 5:18.--> ? C’est qu’en prêchant l’Évangile, j'expose l'Évangile du Mashiah sans qu'il en coûte rien<!--Paulos (Paul) annonçait l'Évangile gratuitement, tout comme le faisait le Seigneur Yéhoshoua (Jésus) lorsqu'il était sur Terre et conformément à l'ordre qu'il avait donné à ses disciples : « Vous avez reçu gratuitement, donnez gratuitement » (Mt. 10:8). Si nous sommes comme Christ (car là est le sens du mot chrétien), nous devons agir comme lui. Voir aussi Ap. 21:6, 22:17.-->, et sans abuser de mon pouvoir dans l'Évangile.
9:19	Car moi, qui suis libre à l'égard de tous, je me suis pourtant fait l'esclave de tous afin de gagner le plus grand nombre.
9:20	Et je suis devenu pour les Juifs comme un Juif, afin de gagner les Juifs. Pour ceux qui sont sous la torah, comme étant sous la torah, afin de gagner ceux qui sont sous la torah ;
9:21	pour les violeurs de la torah comme violeur de la torah, n'étant pas violeur de la torah d'Elohîm, mais lié par la loi du Mashiah - afin de gagner les violeurs de la torah.
9:22	Je suis devenu pour les faibles comme faible, afin de gagner les faibles. Je suis devenu toutes choses pour tous, afin que de toute manière j’en sauve quelques-uns.
9:23	Mais je fais cela à cause de l'Évangile, afin d'en devenir participant avec les autres.
9:24	Ne savez-vous pas que ceux qui courent dans le stade, courent tous en effet, mais qu'un seul reçoit le prix<!--La récompense du vainqueur dans les jeux. Ph. 3:14.--> ? Courez de telle manière que vous le saisissiez.
9:25	Or quiconque lutte dans les jeux sportifs se maîtrise en toutes choses. Ceux-là donc afin de recevoir en effet une couronne corruptible<!--Paulos (Paul) fait ici allusion aux athlètes vainqueurs des jeux olympiques panhelléniques qui recevaient pour récompense une couronne qui était effectivement corruptible puisqu'elle était faite, selon les rites, de feuillages d'olivier, de laurier, de pin ou encore de céleri.-->, mais nous, une incorruptible.
9:26	Moi donc je cours ainsi, non pas d'une façon incertaine ; c'est ainsi que je combats, non pas comme battant l'air.
9:27	Mais je traite durement mon corps et je le réduis en esclavage, de peur d'être moi-même réprouvé<!--Vient du grec « adokimos » qui signifie « refusé à l'épreuve », « non approuvé », « utilisé pour monnaie et poids », « celui qui ne prouve pas ce qu'il devrait ».--> après avoir prêché aux autres.

## Chapitre 10

10:1	Or je ne veux pas que vous ignoriez, frères, que nos pères ont tous été sous la nuée et qu'ils sont tous passés au travers de la mer,
10:2	et qu'ils ont tous été baptisés en Moshé dans la nuée et dans la mer,
10:3	et qu'ils ont tous mangé le même aliment spirituel,
10:4	et qu'ils ont tous bu la même boisson spirituelle, car ils buvaient au rocher spirituel qui les suivait, or le rocher<!--Yéhoshoua Mashiah, le Rocher des âges. Voir 2 S. 22:32 ; Es. 8:13-17, 48:21 ; Ps. 78:16 et 35.--> était le Mashiah.
10:5	Mais ce n'est pas dans la plupart d'entre eux qu'Elohîm trouva son plaisir, car ils furent abattus dans le désert.
10:6	Or ces choses sont devenues des types<!--Un type c'est-à-dire une personne ou une chose préfigurant un futur (Messianique), personne ou chose.--> pour nous, afin que nous ne convoitions pas des choses mauvaises, comme eux-mêmes les ont convoitées.
10:7	Et ne devenez pas idolâtres, comme quelques-uns d'entre eux, selon qu'il est écrit : Le peuple s'assit pour manger et pour boire, et ils se levèrent pour jouer<!--Ex. 32:6.-->.
10:8	Et ne nous prostituons pas, comme quelques-uns d'entre eux se prostituèrent, de sorte qu'il en tomba 23 000 en un jour<!--No. 25.-->.
10:9	Et ne tentons<!--Tenter : du grec « ekpeirazo » qui signifie « mettre à l'épreuve ; éprouver le caractère d'Elohîm et son pouvoir ».--> pas le Mashiah, comme quelques-uns d'entre eux le tentèrent<!--Tenter provient du grec « peirazo » qui signifie également « essayer si une chose peut être faite ; éprouver malicieusement, astucieusement, pour prouver ses sentiments et ses jugements ; essayer ou éprouver la foi, la vertu, le caractère par la séduction du péché ; pousser à pécher ; infliger des maux dans le but d'éprouver ». Ce terme est aussi utilisé lorsque les humains veulent tenter Elohîm en montrant leur méfiance, par une conduite impie ou méchante, pour éprouver la justice et la patience d'Elohîm, et le défier, pour le pousser à donner une preuve de ses perfections.--> et furent détruits par les serpents<!--No. 21:6-9.-->.
10:10	Et ne murmurez pas, comme quelques-uns d'entre eux murmurèrent et périrent par le destructeur<!--No. 14:2-29, 26:63-65.-->.
10:11	Or toutes ces choses leur arrivaient comme types<!--1 Co. 10:6-->, et elles ont été écrites pour notre avertissement, nous qui sommes arrivés à la fin des âges.
10:12	Que celui donc qui pense être debout prenne garde de tomber !
10:13	Aucune épreuve ne vous est survenue qui n'ait été humaine. Mais Elohîm qui est fidèle ne permettra pas que vous soyez mis à l'épreuve au-delà de vos forces, mais avec l'épreuve, il préparera aussi le moyen d’en sortir, pour que vous puissiez la supporter.
10:14	C'est pourquoi, mes bien-aimés, fuyez loin de l’idolâtrie.
10:15	Je parle comme à des personnes intelligentes, jugez vous-mêmes de ce que je dis.
10:16	La coupe de bénédiction que nous bénissons n'est-elle pas la communion du sang du Mashiah ? Et le pain que nous rompons n'est-il pas la communion du corps du Mashiah ?
10:17	Comme il y a un seul pain, nous, les nombreux, nous sommes un seul corps, car tous c'est à partir d'un seul pain que nous participons.
10:18	Voyez l'Israël selon la chair : ceux qui mangent les sacrifices ne sont-ils pas associés à l’autel ?
10:19	Que dis-je donc ? Que l'idole soit quelque chose ? Ou que ce qui est sacrifié à l'idole soit quelque chose ?
10:20	Mais que les choses que les nations sacrifient, elles les sacrifient à des démons<!--De. 32:17 ; Ps. 106:37.--> et non à Elohîm. Or je ne veux pas que vous deveniez les associés<!--« Un partenaire », « un associé », « un camarade », « un compagnon ». Voir Lu. 5:10 ; 2 Co. 8:23. Ce mot fait aussi allusion à l'autel de Yeroushalaim (Jérusalem) sur lequel les sacrifices étaient offerts.--> des démons.
10:21	Vous ne pouvez pas boire la coupe du Seigneur et la coupe des démons. Vous ne pouvez pas participer à la table du Seigneur et à la table des démons<!--Paulos (Paul) nous parle de deux sortes de tables : la table du Seigneur et la table des démons. La table du Seigneur a été révélée à Moshé (Ex. 25:23-30 ; Lé. 24:5-9). Il y avait dessus douze pains destinés à la consommation des prêtres. Ces pains étaient renouvelés chaque shabbat et représentaient le Mashiah, le Pain d'Elohîm, qui est l'aliment du croyant-prêtre (Jn. 6:33-58). Satan est maître en matière de déguisement et d'imitation (2 Co. 11:13-15). Il a donc imité la table du Seigneur et propose aux hommes les mets du roi et le vin de la débauche (Da. 1). Il invite ceux qui cherchent Elohîm à sa table afin de les détourner de la vision du ciel. Voir Mt. 6:24 ; Lu. 16:13.-->.
10:22	Ou bien, excitons-nous la jalousie du Seigneur ? Nous ne sommes pas plus forts que lui.
10:23	Toutes choses sont légales pour moi, mais toutes ne sont pas utiles ; toutes choses sont légales pour moi, mais toutes n'édifient pas.
10:24	Que personne ne cherche son propre intérêt, mais que chacun cherche celui d’autrui.
10:25	Mangez de tout ce qui se vend au marché de la viande sans faire des recherches à cause de la conscience<!--1 Ti. 4:3-5.-->,
10:26	car c'est au Seigneur qu'est la Terre et sa plénitude<!--Ps. 24:1, 50:12.-->.
10:27	Mais si quelqu'un des incrédules vous invite et que vous vouliez aller, mangez de tout ce qui sera mis devant vous sans faire des recherches à cause de la conscience.
10:28	Mais si quelqu'un vous dit : Ceci est sacrifié aux idoles ! N'en mangez pas, à cause de celui qui l'a révélé et à cause de la conscience, c'est au Seigneur qu'est la Terre et sa plénitude.
10:29	Or je dis : la conscience, non la tienne, mais celle de l’autre. Car pourquoi ma liberté est-elle jugée par la conscience d'un autre ?
10:30	Et si par la grâce j'en suis participant, pourquoi suis-je blâmé pour ce dont je rends grâce ?
10:31	Soit donc que vous mangiez, soit que vous buviez, soit que vous fassiez quelque autre chose, faites tout pour la gloire d'Elohîm.
10:32	Ne devenez une cause d’achoppement ni aux Juifs, ni aux Grecs, ni à l'Assemblée d'Elohîm,
10:33	comme moi aussi, je m'efforce de plaire à tous en toutes choses, ne cherchant pas mon propre intérêt, mais celui de plusieurs, afin qu'ils soient sauvés.

## Chapitre 11

11:1	Devenez mes imitateurs, tout comme moi je le suis du Mashiah.
11:2	Or je vous loue frères, de ce qu'en toutes choses vous vous souvenez de moi, et de ce que vous retenez les traditions telles que je vous les ai transmises.
11:3	Mais je veux que vous sachiez que le Mashiah est la tête<!--Le mot « tête » vient du grec « kephale » qui signifie aussi « chef ». Yéhoshoua Mashiah (Jésus-Christ) est la seule tête et l'unique chef de l'Assemblée (Ep. 1:22-23 ; Col. 1:18). Toute personne qui se proclame tête de l'Assemblée devient naturellement anti-mashiah (antichrist).--> de tout homme<!--Vient du grec « aner » qui signifie : « mâle », « époux ».-->, que l'homme est la tête de la femme, et qu'Elohîm est la tête du Mashiah.
11:4	Tout homme qui prie ou qui prophétise ayant quelque chose sur la tête déshonore sa tête.
11:5	Mais toute femme qui prie ou qui prophétise la tête non couverte déshonore sa tête à elle. Car c'est la même chose que si elle était rasée.
11:6	Car si une femme n'est pas couverte, qu'on lui coupe aussi les cheveux. Or s'il est honteux pour une femme d'avoir les cheveux coupés, ou d'être rasée, qu'elle se voile !
11:7	Car l'homme ne doit pas se couvrir la tête en effet : il est l'image et la gloire d'Elohîm, mais la femme est la gloire de l'homme.
11:8	Car l'homme n'est pas issu de la femme, mais la femme est issue de l'homme.
11:9	Car aussi l'homme n'a pas été créé à cause de la femme, mais la femme à cause de l'homme.
11:10	C'est pourquoi la femme à cause des anges doit avoir sur la tête une autorité.
11:11	Toutefois, dans le Seigneur, l'homme n'est pas sans la femme ni la femme sans l'homme.
11:12	Car comme la femme est issue de l'homme, de même aussi l'homme est par le moyen de la femme, mais toutes choses sont issues d'Elohîm.
11:13	Jugez-en vous-mêmes : est-il convenable que la femme prie Elohîm sans être couverte ?
11:14	Bien, la nature elle-même ne vous enseigne-t-elle pas que, si un homme a des cheveux longs c'est vraiment une honte pour lui,
11:15	mais que si la femme a des cheveux longs, c’est une gloire pour elle, parce que la chevelure lui a été donnée à la place<!--opposé à, en avant, vis à vis, pour, au lieu de cela, à la place de (quelque chose), pour ceci, parce que, pour cette cause.--> du voile ?
11:16	Mais si quelqu'un semble être querelleur, nous n'avons pas une telle coutume, ni les assemblées d'Elohîm non plus.
11:17	Mais en ordonnant cela, je ne vous loue pas : parce que vous vous assemblez, non pour le plus excellent<!--Ou « plus utile », « plus utilisable », « plus avantageux ».-->, mais pour l'inférieur.
11:18	Car premièrement, en effet, lorsque vous vous réunissez en assemblée, j'apprends qu'il y a en effet des divisions<!--Vient du grec « schisma » qui signifie aussi « déchirure ». Voir Mt. 9:16 ; Mc. 2:21.--> parmi vous et je le crois en partie,
11:19	car il faut qu'il y ait même des hérésies<!--Le mot grec « hairesis » peut être aussi traduit par « secte ».--> parmi vous, afin que ceux qui sont approuvés deviennent manifestes parmi vous.
11:20	Quand donc vous vous réunissez dans un même lieu, ce n'est pas pour manger le souper du Seigneur,
11:21	car lorsqu'on mange, chacun prend d'avance son propre souper, et l'un a faim en effet, mais l'autre est ivre.
11:22	Car n'avez-vous pas de maisons pour manger et pour boire ? Ou méprisez-vous l'Assemblée d'Elohîm et faites-vous honte à ceux qui n'ont rien ? Que vous dirai-je ? Vous louerai-je ? Je ne vous loue pas en cela.
11:23	Car j'ai reçu du Seigneur ce que je vous ai donné. C'est que le Seigneur Yéhoshoua, la nuit où il fut livré, prit du pain,
11:24	et après avoir rendu grâce, le rompit, et dit : Prenez, mangez. Ceci est mon corps qui est rompu en votre faveur. Faites ceci en mémoire de moi.
11:25	De même aussi la coupe, après le souper, en disant : Cette coupe est la nouvelle alliance en mon sang. Faites ceci toutes les fois que vous en boirez, en mémoire de moi<!--Mt. 26:26-28 ; Mc. 14:22-24 ; Lu. 22:19-20.-->.
11:26	Car toutes les fois que vous mangez ce pain et que vous buvez cette coupe, vous annoncez la mort du Seigneur jusqu'à ce qu'il vienne.
11:27	C'est pourquoi quiconque mange le pain ou boit la coupe du Seigneur indignement sera coupable envers le corps et le sang du Seigneur.
11:28	Mais que l'être humain s'examine lui-même et qu'ainsi, il mange de ce pain et boive de cette coupe.
11:29	Car celui qui en mange et qui en boit indignement, mange et boit un jugement contre lui-même, ne discernant pas le corps du Seigneur.
11:30	C'est pour cette raison qu'il y a parmi vous beaucoup d'infirmes et de malades, et qu'un assez grand nombre s'endorment.
11:31	Car si nous nous discernions nous-mêmes, nous ne serions pas jugés.
11:32	Mais quand nous sommes jugés par le Seigneur, nous sommes châtiés, afin que nous ne soyons pas condamnés avec le monde.
11:33	C'est pourquoi, mes frères, quand vous vous réunissez pour manger, attendez-vous les uns les autres.
11:34	Et si quelqu'un a faim, qu'il mange à la maison, afin que vous ne vous réunissiez pas pour un jugement. Mais pour les autres choses, j'en ordonnerai<!--1 Co. 16:1 ; Tit. 1:5.--> quand je viendrai.

## Chapitre 12

12:1	Mais pour ce qui concerne les choses spirituelles, je ne veux pas, frères, que vous soyez ignorants.
12:2	Vous savez que, nations, vous étiez conduits vers les idoles muettes, selon que vous étiez menés.
12:3	C'est pourquoi je vous fais connaître que personne parlant par l'Esprit d'Elohîm, ne dit : Yéhoshoua est anathème<!--Le mot « anathème » signifie « chose offerte et laissée sur place, par exemple offrande résultant d'un vœu, et pendue au mur ou sur une colonne du temple, ou encore une chose dévouée à Elohîm, sans espoir de rachat, ou un animal destiné à être tué ; donc personne ou chose vouée à la destruction, dévouement par interdit ».--> ! Et personne ne peut dire : Seigneur Yéhoshoua ! sinon par le Saint-Esprit.
12:4	Or il y a diversité de dons de grâce, mais c'est le même Esprit.
12:5	Il y a aussi diversité de services, mais c'est le même Seigneur.
12:6	Il y a aussi diversité d'opérations, mais c'est le même Elohîm qui opère toutes choses en tous.
12:7	Or à chacun est donnée la manifestation de l'Esprit pour être utile<!--1 Co. 6:12, 10:23 ; 2 Co. 12:1.-->.
12:8	Car à l'un est donnée en effet par le moyen de l'Esprit la parole de sagesse, mais à un autre, la parole de connaissance, selon le même Esprit,
12:9	mais à un autre, la foi par le même Esprit, mais à un autre, les dons de guérisons par le même Esprit,
12:10	mais à un autre, les opérations des miracles, mais à un autre, la prophétie, mais à un autre, les discernements d'esprits, mais à un autre, diverses langues, mais à un autre, l'interprétation de langues.
12:11	Mais un seul et même Esprit opère toutes ces choses, les distribuant à chacun en particulier comme il le veut.
12:12	Car, comme le corps est un et qu'il a beaucoup de membres, et que tous les membres de ce corps qui est un, bien qu'il y en ait beaucoup, sont un seul corps, de même en est-il du Mashiah.
12:13	Car nous avons tous été baptisés dans un seul Esprit<!--Les signes du baptême du Saint-Esprit (la conversion) se manifestent par le fruit de l'Esprit qui est évoqué de manière non exhaustive en Ga. 5:22. Les Écritures ne stipulent à aucun endroit que le parler en langues, qui est un don gratuit (Mt. 7:16-20), est en soi le signe du baptême du Saint-Esprit. Ainsi, il nous est dit que chaque croyant au Mashiah a le Saint-Esprit (1 Co. 12:13 ; Ro. 8:9 ; Ep. 1:13-14) mais que tous les croyants ne parlent pas forcément en langues (1 Co. 12:29-31).--> pour être un seul corps, soit Juifs, soit Grecs, soit esclaves, soit libres, et nous avons tous été abreuvés pour être un seul esprit<!--Ep. 4:4.-->.
12:14	Car le corps n'est pas en effet un seul membre, mais beaucoup.
12:15	Si le pied disait : Parce que je ne suis pas la main, ne suis-je pas hors du corps ? Il n’est pas pour autant hors du corps !
12:16	Et si l'oreille disait : Parce que je ne suis pas l'œil, ne suis-je pas hors du corps ? Elle n’est pas pour autant hors du corps !
12:17	Si tout le corps était l'œil, où serait l'ouïe ? Si tout était l'ouïe, où serait l'odorat ?
12:18	Mais maintenant Elohîm a placé<!--« Mettre », « poser » ou « établir » voir Jn. 15:16.--> chaque membre dans le corps comme il a voulu.
12:19	Et si tous étaient un seul membre, où serait le corps ?
12:20	Mais maintenant, il y a en effet beaucoup de membres et un seul corps.
12:21	Et l'œil ne peut pas dire à la main : Je n'ai pas besoin de toi. Ni la tête dire aux pieds : Je n'ai pas besoin de vous.
12:22	Mais bien au contraire, les membres du corps qui semblent être les plus faibles sont nécessaires.
12:23	Et ceux que nous estimons être déshonorés dans le corps, nous les couvrons d'un plus grand honneur, et ce que nous avons de malséant, obtient plus de bienséance,
12:24	mais nos membres honorables n'en ont pas besoin. Mais Elohîm a mêlé ensemble<!--« Unir », « grouper plusieurs parties pour les combiner dans une structure », « qui est le corps, unir une chose à une autre. » Mt. 16:18.--> le corps en donnant un plus grand honneur à ce qui en manquait,
12:25	afin qu'il n'y ait pas de division dans le corps, mais que les membres s'inquiètent de la même façon les uns des autres.
12:26	Et si un membre souffre, tous les membres souffrent avec lui. Si un membre est glorifié, tous les membres se réjouissent avec lui.
12:27	Or vous êtes le corps du Mashiah, et chacun un membre à part.
12:28	Et Elohîm a en effet placé dans l'Assemblée premièrement des apôtres, deuxièmement des prophètes, troisièmement des docteurs, ensuite des miracles, puis des dons de guérisons, des secours, des gouvernements, des langues diverses.
12:29	Tous sont-ils apôtres ? Tous sont-ils prophètes ? Tous sont-ils docteurs ? Tous ont-ils le don des miracles ?
12:30	Tous ont-ils les dons de guérisons ? Tous parlent-ils en langues ? Tous interprètent-ils ?
12:31	Mais désirez sincèrement les dons les plus utiles. Et je vais encore vous montrer une voie par excellence.

## Chapitre 13

13:1	Si je parle toutes les langues des humains<!--Les langues des humains. Les 120 Galiléens ont été rendus capables de s'exprimer dans diverses langues afin de pouvoir annoncer la vérité aux personnes en voyage à Yeroushalaim dans leurs propres langues. Voir Es. 28:11-12 ; Ac. 2:1-13.--> et même des anges<!--La langue des anges ou langue inconnue est incompréhensible à notre intelligence. Elle est un moyen par lequel nous disons des mystères à Elohîm. Voir 1 Co. 14:2,28 ; Ro. 8:26. Il faut une interprétation si l'on veut parler cette langue dans l'assemblée à cause des non-croyants qui nous visitent (1 Co. 14:23). Voir Mc. 16:17.-->, mais que je n’aie pas l'amour<!--Il est question ici de l'amour « agape » : l'amour divin et désintéressé, l'amour fraternel.-->, je suis devenu un cuivre qui résonne ou une cymbale qui répète fréquemment le cri alala<!--Le cri « alala » était une coutume des soldats grecs entrant dans la bataille. Dans la mythologie grecque, Alala est la déesse du cri de la guerre. Voir Mc. 5:38.-->.
13:2	Même si j'ai la prophétie et que je connaisse tous les mystères et la connaissance de toutes choses, et même si j'ai toute la foi jusqu'à transporter les montagnes, mais que je n’aie pas l'amour, je ne suis rien.
13:3	Et si je donnais tous mes biens pour nourrir quelqu'un, et si je livrais mon corps pour être brûlé, mais que je n’aie pas l'amour, cela ne me sert à rien.
13:4	L'amour est patient, il se montre doux, l'amour n'est pas envieux, l'amour ne se vante pas, il ne s'enfle pas d'orgueil,
13:5	il n'agit pas d'une manière malséante, il ne cherche pas son propre intérêt, il ne s'irrite pas, il ne tient pas compte du mal,
13:6	il ne se réjouit pas de l'injustice, mais il se réjouit avec la vérité.
13:7	Il couvre<!--Le verbe « couvrir » vient du terme grec « stego » qui signifie « toiture, toit, couverture », « protéger ou garder en recouvrant, préserver », « couvrir du silence, garder secret ». L'amour ne rappelle pas sans cesse les erreurs des uns et des autres, mais il sait préserver son prochain en gardant le secret sur les fautes expiées (Pr. 10:12, 17:9). Toutefois, il ne saurait se compromettre en ne dénonçant pas les œuvres des ténèbres (Mt. 18:15-18 ; Ja. 5:19-20).--> tout, il croit tout, il espère tout, il supporte tout.
13:8	L'amour ne périt jamais. Mais quant aux prophéties, elles seront abolies. Et quant aux langues, elles cesseront. Quant à la connaissance, elle sera abolie.
13:9	Car nous connaissons en partie et nous prophétisons en partie.
13:10	Mais quand la perfection sera venue, alors ce qui est en partie sera aboli.
13:11	Quand j'étais enfant, je parlais comme un enfant, je jugeais comme un enfant, je pensais comme un enfant. Mais quand je suis devenu homme, j'ai mis fin à ce qui était de l'enfant.
13:12	Car maintenant nous voyons à travers un miroir, en énigme<!--Ou à travers une parole obscure.-->, mais alors nous verrons face à face. Maintenant je connais en partie, mais alors je connaîtrai comme j'ai été connu.
13:13	Mais maintenant, il reste foi, espérance, amour, ces trois choses, mais la plus grande des trois, c'est l'amour.

## Chapitre 14

14:1	Poursuivez l'amour. Désirez sincèrement les dons spirituels, mais surtout que vous prophétisiez.
14:2	Car celui qui parle en langue ne parle pas aux humains, mais à Elohîm, car personne ne le comprend, et c'est en esprit qu'il dit des mystères.
14:3	Mais celui qui prophétise parle aux humains pour la construction et l'exhortation et la consolation.
14:4	Celui qui parle en langue s'édifie lui-même, mais celui qui prophétise bâtit l'Assemblée.
14:5	Or je désire que vous parliez tous en langues, mais surtout que vous prophétisiez. Celui qui prophétise est plus grand que celui qui parle en langues, à moins qu'il n'interprète, pour que l'Assemblée reçoive de la construction.
14:6	Mais maintenant, frères, si je venais à vous parlant en langues, à quoi vous serais-je utile, si je ne vous parlais par révélation, ou par connaissance, ou par prophétie, ou par doctrine ?
14:7	De même, si les choses inanimées qui rendent un son, comme une flûte ou une cithare, ne rendent pas des sons distincts, comment reconnaîtra-t-on ce qui est joué sur la flûte ou sur la cithare ?
14:8	Car si la trompette aussi rend un son incertain, qui se préparera à la bataille ?
14:9	De même, vous aussi, si par le moyen de la langue vous ne donnez pas une parole distincte, comment saura-t-on ce qui est dit ? Car vous serez de ceux qui parlent en l’air.
14:10	Il y a, selon qu'il se rencontre, tant d'espèces de sons dans le monde, et aucun d'eux n'est muet.
14:11	Si donc je ne connais pas la force du son, je serai un barbare pour celui qui parle, et celui qui parle sera un barbare pour moi.
14:12	Et vous de même, puisque vous désirez avec ardeur les dons spirituels, que ce soit pour la construction de l'Assemblée que vous cherchiez à en posséder abondamment.
14:13	C'est pourquoi que celui qui parle en langue prie pour qu’il interprète.
14:14	Car si je prie en langue, mon esprit prie, mais mon intelligence est stérile.
14:15	Qu’est-ce donc ? Je prierai en esprit, mais je prierai aussi avec l'intelligence. Je chanterai en esprit, mais je chanterai aussi avec l'intelligence.
14:16	Autrement, si tu bénis en esprit, comment celui qui occupe la place d'un ignorant dira-t-il : Amen ! à ton action de grâce<!--L'expression « action de grâce » vient du grec « eucharisteo » qui signifie « être reconnaissant, rendre grâces, remercier ». Contrairement à ce que l'on enseigne dans beaucoup d'assemblées, il n'est pas question ici de faire une offrande d'argent, mais de se montrer reconnaissant envers le Seigneur. Voir aussi commentaires en Lé. 3:1, 7:11.-->, puisqu'il ne sait pas ce que tu dis ?
14:17	Car tu rends grâce en effet, mais l'autre n'est pas bâti.
14:18	Je rends grâce à mon Elohîm de ce que je parle plus de langues que vous tous.
14:19	Mais, dans une assemblée, j’aime mieux prononcer 5 paroles au moyen de mon intelligence, afin d'instruire aussi les autres, que 10 000 paroles en langue.
14:20	Frères, ne devenez pas des enfants quant à la faculté de perception et de jugement, mais devenez des enfants à l'égard de la malice. Et quant à la faculté de perception et de jugement, devenez parfaits.
14:21	Il est écrit dans la torah : C'est par des gens d'une autre langue et par des lèvres étrangères que je parlerai à ce peuple, et même ainsi, ils ne m'écouteront pas, dit le Seigneur<!--Es. 28:11.-->.
14:22	C'est pourquoi les langues sont un signe, non pour les croyants, mais pour les incrédules ; et la prophétie, non pour les incrédules, mais pour les croyants.
14:23	Si donc l'assemblée entière se rassemble en un même lieu et que tous parlent en langues, et qu'il entre des ignorants ou des incrédules, ne diront-ils pas que vous êtes fous ?
14:24	Mais si tous prophétisent, et qu'il entre quelque incrédule ou un ignorant, il est convaincu par tous et il est jugé par tous ;
14:25	et ainsi les secrets de son cœur deviennent manifestes. Et ainsi, tombant sur sa face<!--Da. 2:46.-->, il adorera Elohîm en proclamant qu'Elohîm est vraiment au milieu de vous !
14:26	Qu'est-ce donc, frères ? Lorsque vous vous rassemblez, chacun de vous a-t-il un psaume, a-t-il un enseignement, a-t-il une langue, a-t-il une révélation, a-t-il une interprétation ? Que toutes choses se fassent pour la construction.
14:27	Et si quelqu'un parle une langue, que cela se fasse par deux, ou tout au plus par trois, chacun à son tour, et que quelqu'un interprète.
14:28	Mais s'il n'y a pas d'interprète, qu'on se taise dans l'assemblée, et qu'on parle à soi-même et à Elohîm.
14:29	Mais pour les prophètes, que deux ou trois parlent et que les autres jugent.
14:30	Mais si quelque chose est révélée à un autre qui est assis, que le premier se taise.
14:31	Car vous pouvez tous prophétiser l'un après l'autre, afin que tous soient enseignés et que tous soient encouragés.
14:32	Et les esprits des prophètes se soumettent aux prophètes.
14:33	Car Elohîm n'est pas pour le désordre<!--Le mot grec signifie aussi « un état de désordre », « instabilité », « dérangement », « confusion ».-->, mais pour la paix<!--Ou « shalôm ». Voir Ge. 14:18 ; Jg. 6:24 ; Es. 9:5.-->, comme dans toutes les assemblées des saints.
14:34	Que vos femmes se taisent dans les assemblées, car il ne leur est pas permis d'y parler, mais elles doivent être soumises, comme le dit aussi la torah.
14:35	Mais si elles veulent apprendre quelque chose, qu'elles interrogent leurs propres maris à la maison, car il est honteux à des femmes de parler dans l'assemblée.
14:36	Ou bien la parole d'Elohîm est-elle sortie de chez vous ? Ou est-elle arrivée seulement chez vous ?
14:37	Si quelqu'un croit être prophète, ou spirituel, qu'il reconnaisse que les choses que je vous écris sont des commandements du Seigneur.
14:38	Et si quelqu'un est ignorant, qu'il soit ignorant.
14:39	C'est pourquoi, frères, désirez sincèrement de prophétiser, et n'empêchez pas de parler en langues.
14:40	Que toutes choses se fassent d'une manière bienséante et avec ordre.

## Chapitre 15

15:1	Or je vous fais connaître, frères, l'Évangile que je vous ai annoncé, que vous avez aussi reçu et dans lequel vous demeurez fermes,
15:2	et par le moyen duquel vous êtes sauvés, si vous retenez la parole que je vous ai annoncée, à moins que vous n'ayez cru en vain.
15:3	Car je vous ai livré en premier lieu ce que j'avais aussi reçu, que Mashiah est mort en faveur de nos péchés, selon les Écritures,
15:4	et qu'il a été enseveli, et qu'il a été réveillé<!--La résurrection de Yéhoshoua (Jésus) est un espoir pour tous les êtres humains. Elle est le fondement de la foi chrétienne. Toutes les autres religions ont été fondées par des hommes, leurs prophètes ou fondateurs sont morts et aucun n'est revenu à la vie. En tant que disciples de Yéhoshoua, nous sommes réconfortés par le fait que notre Elohîm s'est fait homme, afin de mourir pour nos péchés, et est ressuscité le troisième jour, car l'enfer ne pouvait pas le retenir. Désormais, il tient les clés de l'Hadès et de la mort (Ap. 1:18). Yéhoshoua Mashiah est la résurrection et la vie (Jn. 11:25-26).--> le troisième jour, selon les Écritures,
15:5	et qu'il est apparu à Kephas, et ensuite aux douze.
15:6	Ensuite, il est apparu à plus de 500 frères à la fois, dont la plupart demeurent encore à présent, mais dont quelques-uns aussi se sont endormis.
15:7	Ensuite, il est apparu à Yaacov, et ensuite à tous les apôtres.
15:8	Mais, en dernier de tous, il m'est apparu à moi aussi comme à un avorton.
15:9	Car moi, je suis le plus petit des apôtres, je ne suis pas digne<!--« Suffisant », « assez grand ».--> d'être appelé apôtre, parce que j'ai persécuté l'Assemblée d'Elohîm.
15:10	Mais par la grâce d'Elohîm, je suis ce que je suis, et sa grâce envers moi n'a pas été vaine, mais j'ai travaillé plus qu'eux tous, toutefois non pas moi, mais la grâce d'Elohîm qui est avec moi.
15:11	Ainsi donc que ce soit moi, que ce soit eux, voilà ce que nous prêchons et vous l'avez cru ainsi.
15:12	Or si l'on prêche que Mashiah a été réveillé d’entre les morts, comment quelques-uns d'entre vous disent-ils qu'il n'y a pas de résurrection des morts ?
15:13	Car s'il n'y a pas de résurrection des morts, Mashiah aussi n’a pas été réveillé.
15:14	Et si Mashiah n’a pas été réveillé, notre prédication est donc vaine, et votre foi aussi est vaine.
15:15	Et même nous sommes trouvés de faux témoins de la part d'Elohîm, car nous avons rendu témoignage à l'égard d'Elohîm qu'il a réveillé le Mashiah, alors qu'il ne l'a pas réveillé, si en effet les morts ne sont pas réveillés.
15:16	Mais si les morts ne sont pas réveillés, Mashiah non plus n'a pas été réveillé.
15:17	Et si Mashiah n'a pas été réveillé, votre foi est vaine, et vous êtes encore dans vos péchés,
15:18	ceux donc aussi qui se sont endormis en Mashiah sont perdus.
15:19	Si c'est dans cette vie seulement que nous avons espéré en Mashiah, nous sommes les plus misérables de tous les humains.
15:20	Mais maintenant Mashiah a été réveillé d’entre les morts, il est devenu l'offrande du premier fruit<!--Prémices. Lé. 23:9-11. La fête des prémices (premiers fruits) annonce d'abord la résurrection du Seigneur Yéhoshoua Mashiah (Jésus-Christ), ensuite celle de tous ceux qui lui appartiennent (1 Th. 4:13-18). Elle commençait le premier jour de la semaine suivant le shabbat de la fête des pains sans levain, au mois de Nisan. Voir aussi Jn. 12:24.--> de ceux qui se sont endormis.
15:21	Car puisque la mort est à travers un être humain<!--Ge. 1:26.-->, c'est aussi à travers un être humain qu'est la résurrection des morts.
15:22	Car comme tous meurent en Adam, de même aussi tous seront vivifiés dans le Mashiah.
15:23	Mais chacun en son propre rang<!--« Ce qui a été arrangé », « de choses placées en ordre », « un ensemble de soldats », « un corps », « une bande », « une troupe », « une classe ».--> : l'offrande du premier fruit, le Mashiah, ensuite ceux du Mashiah en sa parousie.
15:24	Ensuite la fin, quand il aura remis le Royaume à Elohîm le Père, quand il aura aboli toute principauté, et toute autorité, et toute puissance.
15:25	Car il faut qu'il règne jusqu'à ce qu'il ait mis tous les ennemis sous ses pieds<!--Ps. 110:1.-->.
15:26	Le dernier ennemi qui est aboli<!--Le verbe grec « katargeo » est au présent passif. Une phrase est à la voix passive lorsque le sujet de la phrase subit l'action au lieu de la faire. Ici c'est la mort qui est le sujet. C'est aussi elle qui a subi l'action. Paul utilise le même verbe au temps passé indéterminé (aoriste) dans 2 Timothée 1:10. Yéhoshoua a vaincu la mort. Ap. 1:18.-->, c’est la mort.
15:27	Car il a soumis toutes choses sous ses pieds. Or quand il a dit que toutes choses ont été soumises, il est évident que celui qui lui a soumis toutes choses est excepté.
15:28	Et quand toutes choses lui auront été soumises, alors le Fils lui-même sera soumis à celui qui lui a soumis toutes choses, afin qu'Elohîm soit tout en tous.
15:29	Autrement, que feront ceux qui sont baptisés pour les morts ? Si les morts ne sont absolument pas réveillés, pourquoi aussi sont-ils baptisés en faveur des morts ?
15:30	Et nous, pourquoi sommes-nous en danger à toute heure ?
15:31	Je meurs chaque jour, je l'atteste par le sujet que j'ai de me glorifier de vous en Mashiah Yéhoshoua notre Seigneur.
15:32	Si c'est selon l'humain que j'ai combattu contre les bêtes sauvages à Éphèse, quel profit en ai-je ? Si les morts ne sont pas réveillés, mangeons et buvons, car demain nous mourrons<!--Es. 22:13.--> !
15:33	Ne vous égarez pas : Les mauvaises compagnies corrompent les bonnes mœurs<!--« Une demeure habituelle », « lieu d'habitation », « coutume », « usage », « morale », « caractère ».-->.
15:34	Quittez l'ivrognerie<!--Le mot grec « eknepho » signifie « revenir à soi d'une ivresse, quitter l'ivrognerie, devenir sobre ».-->, comme il convient et ne péchez pas ! Car quelques-uns sont dans l'ignorance d'Elohîm, je le dis à votre honte.
15:35	Mais quelqu'un dira : Comment les morts sont-ils réveillés, et avec quel corps viennent-ils ?
15:36	Insensé ! Ce que tu sèmes n'est pas ramené à la vie s'il ne meurt pas<!--Jn. 12:24.-->.
15:37	Et ce que tu sèmes, ce n’est pas le corps qui surgira que tu sèmes, mais le grain nu, selon qu'il se rencontre, de blé peut-être ou de quoi que ce soit d’autre.
15:38	Mais Elohîm lui donne un corps, comme il veut, et à chaque semence il donne un corps qui lui est propre.
15:39	Toute chair n'est pas de la même chair, mais autre en effet est la chair des humains, et autre la chair des bêtes, autre celle des poissons, et autre celle des oiseaux.
15:40	Il y a aussi des corps existants dans le ciel et des corps existants sur la Terre, mais autre en effet est la gloire des corps existants dans le ciel, et autre celle des existants sur la Terre.
15:41	Autre est la gloire du soleil, et autre la gloire de la lune, et autre la gloire des étoiles. Car une étoile diffère d'une autre étoile en gloire.
15:42	Il en sera aussi de même à la résurrection des morts. On est semé dans la corruption, on est réveillé dans l’incorruptibilité.
15:43	On est semé dans le déshonneur, on est réveillé dans la gloire. On est semé dans la faiblesse, on est réveillé dans la force.
15:44	On est semé corps animal<!--Voir 1 Co. 15:46.-->, on est réveillé corps spirituel. S'il y a un corps animal, il y a aussi un corps spirituel.
15:45	Comme aussi il est écrit : Le premier être humain<!--Ge. 1:26.-->, Adam, devint une âme vivante<!--Ge. 2:7.-->. Le dernier Adam, en Esprit, donne la vie<!--Jn. 5:21 ; Ro. 8:11. Yéhoshoua Mashiah est le dernier Adam. Voir Ph. 2:7 ; 1 Ti. 3:16.-->.
15:46	Mais ce qui est spirituel n'est pas le premier, mais ce qui est animal<!--Voir 1 Co. 2:14, 15:44 ; Ja. 3:15 ; Jud. 1:19.--> ; ensuite ce qui est spirituel.
15:47	Le premier être humain, issu de la terre<!--Issu du sol.-->, est fait de terre, le second être humain, le Seigneur, est issu du ciel.
15:48	Tel est celui qui est fait de terre, tels sont aussi ceux qui sont faits de terre, et tel est l'existant dans le ciel, tels sont aussi les existants dans le ciel.
15:49	Et comme nous avons porté l'image de celui qui est fait de terre, nous porterons aussi l'image de l'existant dans le ciel.
15:50	Or je dis cela, frères, parce que la chair et le sang ne peuvent hériter du Royaume d'Elohîm, et que la corruption n'hérite pas de l'incorruptibilité.
15:51	Voici, je vous dis un mystère : Nous ne nous endormirons pas tous en effet, mais tous, nous serons changés,
15:52	en un atome de temps<!--Vient du grec « atomos » qui signifie « indivisible ». L'atome en français est un minuscule morceau de matière, une sorte de « brique » qui la constitue. Un atome contient un noyau, c'est-à-dire l'ensemble de protons et de neutrons, et autour de ce noyau, il y a des électrons. Les électrons sont des nuages qui enveloppent le noyau. Les électrons sont les plus petites particules qui, en se baladant matérialisent l'électricité. La vitesse d'un électron peut être proche de celle de la lumière. En une seconde, l'électron traverse deux fois un territoire grand comme la France.-->, en un clin d'œil, à la dernière trompette<!--Le mot dernier dans ce passage est « eschatos » qui signifie « dernier en temps ou en lieu, dernier dans des séries de lieux, dernier dans une succession dans le temps ». Paulos (Paul) associe le mystère de la résurrection à la dernière trompette. Or dans le livre d'Apocalypse il n'y a que sept trompettes (Ap. 8 ; Ap. 9 et Ap. 11:15-19), et c'est à la dernière, c'est-à-dire la septième, que le mystère d'Elohîm s'accomplit.-->. Car on sonnera de la trompette, et les morts seront réveillés incorruptibles, et nous, nous serons changés.
15:53	Mais, il faut que le corruptible revête l'incorruptibilité, et que le mortel revête l'immortalité.
15:54	Or, lorsque ce corruptible aura revêtu l'incorruptibilité, et que ce mortel aura revêtu l'immortalité, alors adviendra la parole qui est écrite : La mort a été engloutie dans la victoire<!--Es. 25:8.-->.
15:55	Où est-il, mort, ton aiguillon<!--Une piqûre, comme celle des abeilles, scorpions. Ces animaux blessant et causant même la mort, Paulos compare la mort à une piqûre, une arme mortelle. L'aiguillon est une pièce métallique qu'on utilise pour diriger les bœufs et autres bêtes de trait d'où le proverbe, « ruer contre les aiguillons », c'est-à-dire offrir une vaine, périlleuse et ruineuse résistance. Os. 13:14.--> ? Hadès<!--Hadès chez les Grecs ou Pluton chez les Romains était considéré comme le dieu de l'enfer (voir commentaire en Mt. 16:18). Notre Seigneur Yéhoshoua ha Mashiah a vaincu la divinité Hadès et a récupéré les clés de l'enfer et de la mort (Ap. 1:18). Au jugement dernier, ce démon sera jeté avec Satan dans le lac de feu (Ap. 20:13-14).-->, où est ta victoire ?
15:56	Or l'aiguillon de la mort c'est le péché, et la puissance du péché, c'est la torah.
15:57	Mais grâce soit rendue à Elohîm qui nous donne la victoire par le moyen de notre Seigneur Yéhoshoua Mashiah !
15:58	C'est pourquoi, mes frères bien-aimés, soyez fermes, inébranlables, abondant<!--« Abondant » est utilisé pour une fleur qui passe de l'état de bourgeon à celui de fleur épanouie.--> toujours dans l'œuvre du Seigneur, sachant que votre travail n'est pas inutile dans le Seigneur.

## Chapitre 16

16:1	Mais en ce qui concerne la collecte pour les saints, faites, vous aussi, comme je l'ai ordonné aux assemblées de Galatie.
16:2	L'un des shabbats<!--Vient du grec « sabbaton ». Il est question ici du septième jour de chaque semaine qui était une fête sacrée, pour lequel les Israélites devaient s'abstenir de tout travail. Un shabbat, un jour de shabbat.-->, que chacun de vous mette à part chez lui quelque chose, accumulant selon qu’il aura prospéré, afin que ce ne soit pas seulement quand je serai arrivé que les collectes se fassent.
16:3	Et quand je serai présent, j'enverrai avec des lettres ceux que vous aurez éprouvés<!--Voir 1 Ti. 3:10.--> pour transporter au loin votre grâce à Yeroushalaim.
16:4	Mais si la chose mérite que j'y aille moi-même, ils iront avec moi.
16:5	Mais j'irai chez vous quand j'aurai traversé la Macédoine, car je traverserai la Macédoine.
16:6	Mais peut-être séjournerai-je auprès de vous, ou même y passerai-je l'hiver, afin que vous m'accompagniez là où j'irai.
16:7	Car je ne veux pas vous voir maintenant en passant, mais j'espère demeurer quelque temps auprès de vous, si le Seigneur le permet.
16:8	Mais je demeurerai à Éphèse jusqu'à la pentecôte,
16:9	car une porte grande et efficace m'y est ouverte, mais il y a beaucoup d’adversaires.
16:10	Mais si Timotheos arrive, voyez à ce qu'il soit sans crainte auprès de vous, car il travaille à l'œuvre du Seigneur comme moi-même.
16:11	Que personne donc ne le méprise. Accompagnez-le en paix afin qu'il vienne vers moi, car je l'attends avec les frères.
16:12	Mais pour ce qui concerne Apollos, le frère, je l'ai beaucoup exhorté à se rendre chez vous avec les frères, mais il n'a nullement eu la volonté d'y aller maintenant. Il partira quand il en aura l'occasion.
16:13	Veillez, demeurez fermes dans la foi, soyez des hommes, fortifiez-vous.
16:14	Que toutes choses parmi vous se fassent dans l’amour !
16:15	Mais je vous prie, frères, vous savez que la famille de Stéphanas est l'offrande du premier fruit<!--Prémices.--> de l'Achaïe, et qu'ils se sont désignés pour le service des saints.
16:16	Aussi, soumettez-vous à de telles personnes et à quiconque fait concourir les choses et travaille dur.
16:17	Mais je me réjouis de la parousie de Stéphanas, de Phortounatos et d'Achaikos, parce qu'ils ont suppléé à votre absence.
16:18	Car ils ont donné du repos à mon esprit et au vôtre. Reconnaissez<!--Mt. 7:16.--> donc de telles personnes !
16:19	Les assemblées d'Asie vous saluent. Akulas et Priscilla, avec l'assemblée qui est dans leur maison, vous saluent beaucoup dans le Seigneur.
16:20	Tous les frères vous saluent. Saluez-vous les uns les autres par un saint baiser.
16:21	La salutation est de ma propre main, de moi, Paulos.
16:22	Si quelqu'un n'aime pas le Seigneur Yéhoshoua Mashiah, qu'il soit anathème ! Maranatha<!--« Maranatha » signifie littéralement « Le Seigneur vient ! ».--> !
16:23	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous !
16:24	Mon amour est avec vous tous en Mashiah Yéhoshoua. Amen !
