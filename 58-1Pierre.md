# 1 Petros (1 Pierre) (1 Pi.)

Signification : Roc, pierre

Auteur : Petros (Pierre)

Thème : La victoire sur la souffrance

Date de rédaction : Env. 65 ap. J.-C.

Cette lettre semble avoir été écrite à Rome, même si Petros y parlait de « Babel ». En ces temps de persécutions, les chrétiens devaient être prudents quant à la manière dont ils parlaient du pouvoir en place, c'est pourquoi ils utilisaient souvent des codes. C'est donc durant une période difficile que fut rédigée cette lettre qui s'adressait à des assemblées d'Asie Mineure dont la plupart furent fondées par Paulos (Paul). À travers ces quelques lignes, Petros (Pierre) exhorte les frères et sœurs à tenir ferme dans la foi malgré les souffrances liées aux épreuves, et les encourage à espérer en Yéhoshoua ha Mashiah (Jésus-Christ), leur salut. Il finit cette lettre en donnant des conseils quant à l'attitude à avoir au sein de l'assemblée.

## Chapitre 1

1:1	Petros, apôtre de Yéhoshoua Mashiah, aux étrangers élus de la diaspora du Pont<!--Le Pont : province formant presque la totalité de l'Asie Mineure.-->, de la Galatie, de la Cappadoce, de l'Asie et de la Bithynie,
1:2	selon la prescience d'Elohîm le Père, par la sanctification de l'Esprit pour l’obéissance et l'aspersion du sang de Yéhoshoua Mashiah, à vous, grâce et shalôm, multipliés !
1:3	Béni soit l'Elohîm et Père de notre Seigneur Yéhoshoua Mashiah, qui par sa grande miséricorde, nous a fait naître de nouveau<!--Le mot grec « anagennao » signifie « produire de nouveau, être né de nouveau » ou « renouvelé ».--> pour une espérance vivante, par le moyen de la résurrection de Yéhoshoua Mashiah d'entre les morts,
1:4	pour un héritage incorruptible et sans souillure, qui ne se fane pas, et qui est réservé dans les cieux pour nous,
1:5	ceux qui, dans la puissance d'Elohîm, sont gardés par le moyen de la foi, pour le salut qui est prêt à être révélé dans le dernier temps !
1:6	En cela vous exultez, même si maintenant, puisqu’il le faut, vous êtes pour un peu de temps affligés par diverses épreuves,
1:7	afin que le test<!--La preuve, ce par quoi une chose est essayée, éprouvée, un test. Ja. 1:3.--> de votre foi, beaucoup plus précieuse que l'or qui périt, mais qu'on éprouve au moyen du feu, soit trouvée un sujet de louange, d'honneur et de gloire, lors de la révélation de Yéhoshoua Mashiah.
1:8	Lequel vous aimez sans l'avoir vu ; en qui, sans le voir maintenant, mais croyant, vous exultez d'une joie inexprimable et glorieuse,
1:9	obtenant le but<!--Vient du grec « telos » qui signifie « la fin », « ce par quoi se termine une chose, le but, la finalité ». Voir 1 Ti. 1:5.--> de votre foi, le salut des âmes.
1:10	Salut, au sujet duquel ont fait des investigations et cherché avec anxiété et diligemment les prophètes qui ont prophétisé au sujet de la grâce qui est en vous.
1:11	Cherchant à sonder pour quel temps et quelle circonstance indiquait l'Esprit du Mashiah qui était en eux, et qui affirmait d'avance les souffrances du Mashiah et les gloires dont elles seraient suivies.
1:12	Il leur fut révélé que ce n'était pas pour eux-mêmes, mais pour nous, qu'ils administraient ces choses qui maintenant vous ont été annoncées par le moyen de ceux qui vous ont prêché l'Évangile par le Saint-Esprit envoyé du ciel, et dans lesquelles les anges désirent regarder avec la tête penchée en avant.
1:13	C'est pourquoi, ceignez les reins de votre compréhension, soyez sobres et ayez une entière espérance dans la grâce qui vous est apportée en la révélation<!--Voir commentaire en 2 Th. 1:7.--> de Yéhoshoua Mashiah.
1:14	Comme des enfants d’obéissance, ne vous conformant pas aux convoitises d’autrefois dans votre ignorance.
1:15	Mais, comme celui qui vous a appelés est saint, vous aussi de même soyez saints dans toute conduite,
1:16	car il est écrit : Soyez saints, parce que moi je suis saint<!--Lé. 11:44.-->.
1:17	Et si vous invoquez le Père<!--Expression qui trouve son explication dans le fait que les prières adressées à Elohîm commencent en général par une invocation du nom divin. Voir Joë. 3:5 ; Ac. 15:17, 22:16 ; Ro. 10:12-14 ; 1 Co. 1:2 ; 2 Ti. 2:22.--> qui juge de façon impartiale selon l'œuvre de chacun, conduisez-vous avec crainte pendant le temps de votre demeure en terre étrangère,
1:18	sachant que ce n'est pas par des choses corruptibles - argent ou or - que vous avez été rachetés hors de votre vaine manière de vivre que vos ancêtres vous avaient transmise,
1:19	mais par un sang précieux, comme d'un agneau sans défaut et sans tache, Mashiah,
1:20	prédestiné en effet avant la fondation du monde, et manifesté dans les derniers temps à cause de vous.
1:21	Par son moyen vous croyez en Elohîm qui l'a réveillé hors des morts et lui a donné gloire, afin que votre foi et votre espérance soient en Elohîm.
1:22	Ayant purifié vos âmes par l’obéissance à la vérité par le moyen de l'Esprit pour un amour fraternel sincère, aimez-vous ardemment les uns les autres d'un cœur pur,
1:23	étant nés de nouveau<!--Voir 1 Pi. 1:3.--> non à partir d’une semence corruptible mais incorruptible, par le moyen de la parole vivante d'Elohîm et qui demeure pour l'éternité.
1:24	Parce que toute chair est comme l'herbe, et toute gloire de l'être humain comme la fleur de l'herbe. L'herbe a séché et sa fleur est tombée,
1:25	mais la parole du Seigneur demeure pour l'éternité<!--Es. 40:6-8.-->. Or c’est cette parole qui vous a été prêchée.

## Chapitre 2

2:1	Étant donc débarrassés<!--Voir Hé. 12:1.--> de toute méchanceté, et de toute espèce de tromperie, et d'hypocrisie, et d'envie et de toutes diffamations,
2:2	comme des bébés nouveau-nés, désirez le lait spirituel et pur, afin qu'en lui vous croissiez,
2:3	si en effet vous avez goûté que le Seigneur est bon<!--Petros (Pierre), l'apôtre, se réfère ici au Ps. 34:9 : « Goûtez et voyez combien YHWH est bon ! ». On peut noter que le nom propre YHWH a été remplacé par le nom commun « Seigneur ». Voir commentaire en Lu. 4:18-19.-->.
2:4	Vous approchant de lui, pierre vivante, rejetée en effet par les humains, mais choisie et précieuse devant Elohîm.
2:5	Et vous-mêmes, comme des pierres vivantes, vous êtes édifiés, maison spirituelle, sainte prêtrise, afin d'offrir des sacrifices spirituels, agréables à Elohîm par le moyen de Yéhoshoua Mashiah.
2:6	C'est pourquoi aussi, il est dit dans l'Écriture : Voici, je mets en Sion la pierre angulaire<!--Yéhoshoua Mashiah (Jésus-Christ) est la pierre rejetée par les bâtisseurs. Voir Es. 28:16 ; Ps. 118:22.-->, choisie et précieuse. Et celui qui croit en elle n'aura jamais honte.
2:7	C'est donc pour vous les croyants qu'elle a ce prix. Mais pour les rebelles<!--Ps. 118:22.-->, la pierre que ceux qui bâtissaient ont rejetée est devenue la tête de l'angle,
2:8	et une pierre d'achoppement, et un rocher de scandale. Ils se heurtent contre la parole et sont rebelles, et c'est à cela qu'ils sont destinés.
2:9	Mais vous, vous êtes une race élue, une prêtrise royale, une nation sainte, un peuple acquis, afin que vous proclamiez<!--dire clairement, déclarer à l'étranger, au loin, divulguer, publier, faire connaître par l'éloge ou la proclamation, célébrer.--> les vertus de celui qui vous a appelés de la ténèbre à sa merveilleuse lumière.
2:10	Vous qui autrefois n'étiez pas un peuple, mais qui maintenant êtes le peuple d'Elohîm. Vous qui n'aviez pas obtenu miséricorde, mais qui maintenant avez obtenu miséricorde.
2:11	Bien-aimés, je vous exhorte, comme étrangers et voyageurs, à vous abstenir des désirs<!--Voir Ga. 5:16.--> charnels qui combattent contre l'âme.
2:12	Ayant une bonne conduite parmi les nations, afin que là même où elles vous calomnient comme des malfaiteurs, elles voient vos bonnes œuvres et glorifient Elohîm au jour de la visite d'inspection<!--« Investigation, inspection, visite d'inspection ». Cet acte par lequel Elohîm visite les êtres humains, observe leurs voies, leurs caractères, pour leur accorder en partage joie ou tristesse.-->.
2:13	Soyez donc soumis à toute institution humaine, à cause du Seigneur : soit au roi, comme étant au-dessus des autres,
2:14	soit aux gouverneurs, parce qu'ils sont envoyés par lui en effet pour la punition des malfaiteurs et pour la louange de ceux qui agissent correctement.
2:15	Car telle est la volonté d'Elohîm, qu'en faisant le bien vous museliez la bouche<!--Du grec « phimoo » : « fermer la bouche par une muselière ».--> à l'ignorance des humains insensés.
2:16	Comme libres, et n’ayant pas la liberté comme une couverture de la malice, mais comme des esclaves d'Elohîm.
2:17	Honorez tout le monde, aimez la fraternité<!--La famille des frères.-->, craignez Elohîm, honorez le roi.
2:18	Les domestiques, soyez soumis en toute crainte à vos maîtres, non seulement aux bons et équitables, mais aussi aux tordus.
2:19	Car c'est une grâce, si quelqu'un, à cause de la conscience envers Elohîm, endure des douleurs en souffrant injustement.
2:20	Car quelle gloire y a-t-il si, étant frappés pour avoir commis des péchés, vous l'endurez ? Mais si vous souffrez quand vous faites le bien et que vous le supportez, c'est une grâce devant Elohîm.
2:21	Car c'est à cela que vous avez été appelés, parce que Mashiah aussi a souffert en notre faveur, nous laissant un modèle, afin que vous suiviez ses traces :
2:22	lui qui n'a pas commis de péché et dans la bouche duquel il ne s'est pas trouvé de tromperie,
2:23	lui qui, insulté, n'insultait pas en retour<!--Riposter à des railleries.-->, maltraité, ne menaçait pas mais s'en remettait à celui qui juge justement,
2:24	lui qui a lui-même porté nos péchés dans son corps sur le bois, afin qu'étant morts au péché, nous vivions pour la justice. Lui, par la meurtrissure<!--Es. 53:5.--> de qui vous avez été guéris.
2:25	Car vous étiez comme des brebis égarées, mais maintenant vous êtes retournés vers le Berger et le Surveillant<!--Job 7:20.--> de vos âmes.

## Chapitre 3

3:1	De même, les femmes, soyez soumises à vos propres maris, afin que si quelques-uns sont rebelles à la parole, ils soient gagnés sans paroles par le moyen de la conduite des femmes,
3:2	lorsqu'ils auront vu votre conduite pure, dans la crainte.
3:3	Que votre parure<!--Vient du grec « kosmos » qui signifie : « un arrangement habile et harmonieux d'une constitution, d'un ordre, d'un gouvernement », « ornement », « décoration », « parure », « le monde », « la totalité des biens terrestres, les richesses, avantages, plaisirs, etc., qui bien que creux, fragiles et fugitifs, poussent au désir, éloignent d'Elohîm, et sont des obstacles à la cause du Mashiah. »--> ne soit pas celle de l'extérieur - le tressage des cheveux et les ornements d'or ou l'ajustement des habits,
3:4	mais l'être humain caché dans le cœur, l'incorruptibilité d'un esprit de douceur<!--La douceur envers Elohîm est cette disposition d'esprit par laquelle nous acceptons tout ce qu'il nous donne comme étant un bienfait, et ceci sans discussion ni résistance. Dans le Tanakh, les doux, les débonnaires, étaient ceux qui s'en remettaient entièrement à Elohîm et non à leur propre force pour les défendre contre l'injustice. Ainsi, la douceur d'Elohîm envers les méchants est de leur permettre de connaître les préjudices infligés, qu'il utilise pour purifier ses élus, ceux qu'il délivrera en son temps (Es. 41:17 ; Lu. 18:1-8). La gentillesse ou la douceur sont opposées à l'affirmation de soi-même et au propre intérêt. Le débonnaire ne s'occupe pas du tout de soi. Cet état est une œuvre du Saint-Esprit, non de la volonté humaine.--> et de tranquillité, qui est précieux devant Elohîm.
3:5	Car c'est ainsi que se paraient<!--Vient du verbe « kosmeo » dont la racine est « kosmos » (monde) ; kosmeo signifie : « mettre en ordre », « arranger », « rendre prêt », « préparer », « orner », « ornementer ».--> aussi autrefois les saintes femmes qui espéraient en Elohîm, étant soumises à leurs propres maris,
3:6	comme Sarah, qui obéissait à Abraham, l'appelant seigneur, elle dont vous êtes devenues les enfants, en faisant le bien, sans être effrayées par aucune terreur.
3:7	Maris, également, demeurez ensemble selon la connaissance comme avec un vase<!--Petros (Pierre) utilise une métaphore connue des Grecs pour parler du corps : le vase.--> faible<!--« Infirme », « maladif ».-->, le féminin, les traitant avec honneur comme étant aussi cohéritières de la grâce de la vie, afin que vos prières ne soient pas interrompues.
3:8	Mais finalement, soyez tous d'un seul esprit, sensibles aux autres, aimez-vous<!--Vient de « philadelphos » qui signifie « aimer quelqu'un comme un frère, une sœur ».--> comme des frères et des sœurs, compatissants et amicaux.
3:9	Ne rendez pas le mal pour le mal, ou l'injure pour l'injure<!--Mt. 5:44.-->, mais au contraire, bénissez. Sachant que c'est à cela que vous êtes appelés, afin d'hériter la bénédiction.
3:10	Car celui qui veut aimer la vie et voir des jours heureux, qu’il préserve<!--« Faire cesser ou désister », « empêcher une chose ou une personne de quelque chose ».--> sa langue de ce qui est mauvais et ses lèvres de prononcer la tromperie.
3:11	Qu'il se détourne de ce qui est mauvais et fasse ce qui est bon, qu'il cherche la paix et la poursuive,
3:12	parce que les yeux<!--Voir Ps. 33:18, 34:16.--> du Seigneur sont sur les justes et ses oreilles sont tournées vers leur supplication, mais la face du Seigneur est contre ceux qui font le mal.
3:13	Et qui vous maltraitera, si vous devenez les imitateurs de celui qui est bon ?
3:14	Mais si vous souffrez aussi à cause de la justice, vous êtes bénis. Ne craignez pas leur terreur et ne soyez pas troublés.
3:15	Mais sanctifiez le Seigneur Elohîm<!--Voir Es. 8:12-13.--> dans vos cœurs, et soyez toujours prêts à la plaidoirie avec douceur et crainte, contre quiconque vous demande une parole concernant l'espérance qui est en vous,
3:16	ayant une bonne conscience, afin que là même où ils diffament votre bonne conduite en Mashiah, ils soient pris de honte de ce qu'ils vous accusent faussement comme des malfaiteurs.
3:17	Car il vaut mieux, si la volonté d'Elohîm le veut, que vous souffriez en faisant le bien qu'en faisant le mal.
3:18	Parce que Mashiah aussi a souffert une fois au sujet des péchés, lui juste en faveur des injustes, afin de nous conduire à Elohîm, ayant été en effet mis à mort selon la chair, mais il a été ramené à la vie par l'Esprit.
3:19	C'est aussi en lui qu'il est allé prêcher aux esprits en prison<!--La possibilité du salut après la mort n'a aucun fondement biblique (Hé. 9:27). Dans ce passage, il est fait mention des pécheurs qui ont vécu du temps de Noah et auxquels la parole d'Elohîm avait été annoncée. Voir aussi commentaire en Mt. 16:18.-->,
3:20	qui furent rebelles, lorsque, une fois, aux jours de Noah, la patience d'Elohîm attendait, pendant que se construisait l'arche, dans laquelle un petit nombre d'âmes, c'est-à-dire huit, furent préservées du danger à travers l'eau,
3:21	et c'est une imitation<!--Hé. 9:23.--> qui maintenant nous sauve : le baptême, lequel n'est pas la purification de la saleté de la chair, mais la demande à Elohîm d'une bonne conscience par le moyen de la résurrection de Yéhoshoua Mashiah,
3:22	lequel est à la droite d'Elohîm, étant allé au ciel, et à qui sont soumis les anges, les dominations et les puissances.

## Chapitre 4

4:1	Mashiah ayant donc souffert pour nous dans la chair, vous aussi armez-vous de la même pensée. Car celui qui a souffert dans la chair a été libéré<!--« Faire cesser ou désister », « préserver une chose ou une personne de quelque chose », « cesser », « laisser », « abandonner ».--> du péché,
4:2	afin de vivre, non plus selon les désirs des humains, mais selon la volonté d'Elohîm, pendant le temps qui lui reste à vivre dans la chair.
4:3	Car c'est assez pour nous d'avoir accompli dans le temps passé la volonté des nations, en marchant dans les luxures sans bride, les convoitises, les ivrogneries, les orgies, les beuveries et les idolâtries criminelles.
4:4	À ce propos, ils sont choqués que vous ne vous précipitiez pas avec eux dans le même débordement de libertinage, et ils vous calomnient.
4:5	Eux qui rendront compte à celui qui est prêt à juger les vivants et les morts.
4:6	Car c'est pour cela que l'Évangile a été aussi prêché aux morts, afin qu'ils soient en effet jugés selon les humains dans la chair, et qu'ils vivent selon Elohîm dans l'Esprit.
4:7	Or la fin de toutes choses est proche : maîtrisez-vous donc et soyez sobres pour les prières.
4:8	Mais avant toutes choses, ayez les uns pour les autres un amour assidu, parce que l'amour couvrira une multitude de péchés<!--Voir Pr. 10:12, 17:9 ; Ja. 5:20.-->,
4:9	étant hospitaliers les uns envers les autres, sans murmures.
4:10	Que chacun de vous rende service aux autres selon le don de grâce qu'il a reçu, comme de bons gestionnaires de la grâce diverse d'Elohîm.
4:11	Si quelqu'un parle, que ce soit comme oracles d'Elohîm. Si quelqu’un sert, que ce soit comme par la force qu'Elohîm fournit, afin qu'en toutes choses Elohîm soit glorifié par le moyen de Yéhoshoua Mashiah, auquel sont la gloire et la force souveraine, pour les âges des âges. Amen !
4:12	Bien-aimés, ne soyez pas choqués qu'il y ait au milieu de vous le feu pour votre tentation, comme s'il vous arrivait quelque chose de nouveau.
4:13	Mais réjouissez-vous de ce que vous participez aux souffrances du Mashiah, afin que lors de la révélation de sa gloire, vous vous réjouissiez et exultiez.
4:14	Si vous êtes insultés<!--Mt. 5:11.--> pour le Nom du Mashiah, vous êtes bénis, parce que l'Esprit de gloire et d'Elohîm repose sur vous. Il est en effet blasphémé par eux, mais il est glorifié par vous.
4:15	En effet, qu'aucun de vous ne souffre comme meurtrier, ou voleur, ou malfaiteur ou comme se mêlant des affaires d'autrui,
4:16	mais si c'est comme chrétien, qu'il n'en ait pas honte, mais qu'il glorifie Elohîm avec cette part.
4:17	Parce que c'est le temps où le jugement commence par la maison d'Elohîm<!--Le jugement commence par la maison d'Elohîm. Ez. 9:1-11.-->. Mais si c'est d'abord par nous, quelle sera la fin de ceux qui sont rebelles à l'Évangile d'Elohîm ?
4:18	Et si le juste est sauvé avec difficulté, celui qui est impie et pécheur, où apparaîtra-t-il ?
4:19	C'est pourquoi aussi, que ceux qui souffrent selon la volonté d'Elohîm, lui remettent leurs âmes, comme au Créateur fidèle, en faisant le bien.

## Chapitre 5

5:1	J'exhorte les anciens qui sont parmi vous, moi qui suis ancien avec eux, témoin des souffrances du Mashiah et participant de la gloire qui est sur le point d'être révélée :
5:2	paissez le troupeau d'Elohîm qui est avec vous, exerçant la surveillance non par contrainte, mais volontairement, non par empressement pour un gain, mais de bon cœur,
5:3	non comme dominant sur des lots, mais en devenant des exemples du troupeau.
5:4	Et quand le Berger<!--Yéhoshoua (Jésus) est notre Souverain Pasteur. Voir Ps. 23 ; Jn. 10.--> en chef apparaîtra, vous obtiendrez la couronne incorruptible<!--« Imputrescible », « couronne composée d'amarante, fleur qui ne se fane jamais, symbole de perpétuité et d'immortalité ».--> de gloire.
5:5	De même, vous qui êtes jeunes, soyez soumis aux anciens. Et vous soumettant tous les uns aux autres<!--Ep. 5:21.-->, revêtez-vous<!--Le verbe grec « egkomboomai » signifie « lien ou bande par laquelle deux choses sont liées ensemble, se revêtir ou se ceindre ». À l'époque, les esclaves avaient une écharpe blanche attachée à la ceinture afin de les distinguer des hommes libres. Dans ce passage, Petros (Pierre) fait allusion à cet usage, pour montrer la soumission réciproque des chrétiens. C'est aussi une référence au très humble vêtement, espèce de tablier, que les esclaves devaient porter pour ne pas se salir pendant leur travail.--> d'humilité, parce qu'Elohîm résiste aux orgueilleux, mais il fait grâce aux humbles.
5:6	Humiliez-vous donc sous la puissante main d'Elohîm, afin qu'il vous élève au temps convenable.
5:7	Jetez sur lui tout votre souci puisque lui-même prend soin de vous.
5:8	Soyez sobres, veillez ! Parce que le diable, votre adversaire, marche comme un lion rugissant, cherchant qui il dévorera.
5:9	Résistez-lui, étant fermes dans la foi, sachant que les mêmes souffrances sont imposées à votre fraternité<!--La famille des frères.--> dans le monde.
5:10	Mais l'Elohîm de toute grâce, qui nous a appelés à sa gloire éternelle en Mashiah Yéhoshoua, lorsque vous aurez souffert un peu de temps, vous équipera<!--« Réparer », « ajuster ». Voir Mt. 4:21.--> lui-même, vous affermira, vous fortifiera, vous rendra stables !
5:11	À lui la gloire et la force souveraine, pour les âges des âges ! Amen !
5:12	C'est par le moyen de Silvanos que je considère comme un frère fidèle, que je vous ai écrit en peu de mots, exhortant et rendant témoignage que cette grâce d'Elohîm dans laquelle vous êtes établis est la vraie.
5:13	Celle qui est à Babel<!--Le nom « Babel » signifie « la porte de El » ou « confusion par le mélange ». Généralement traduit par Babylone.-->, élue avec vous, vous salue, ainsi que Markos mon fils.
5:14	Saluez-vous les uns les autres par un baiser d'amour. Shalôm à vous tous qui êtes dans le Mashiah Yéhoshoua ! Amen !
