# Romains (Ro.)

Auteur : Paulos (Paul)

Thème : L'Évangile d'Elohîm

Date de rédaction : Env. 56 ap. J.-C.

Rome est une ville située dans la région du Latium, au centre de l'Italie, à la confluence de l'Aniene et du Tibre. Centre de l'Empire romain, elle domina l'Europe, l'Afrique du Nord et le Moyen-Orient du 1er siècle av. J.-C. au 5ème siècle ap. J.-C.

La lettre était destinée à l'assemblée de Rome, fondée sans doute par des chrétiens convertis grâce au service de Paulos (Paul) et d'autres apôtres itinérants. Cette assemblée comptait quelques Juifs, mais était surtout constituée des nations. La lettre de Paulos aux Romains fut rédigée au cours du troisième voyage missionnaire de Paulos, pendant les trois mois que l'apôtre passa à Corinthe.

En attendant de leur rendre visite, Paulos avait le désir de communiquer aux chrétiens de Rome les grandes lignes du principe de la grâce, dont il avait eu la révélation. Il aborde donc dans cette lettre plusieurs doctrines majeures, comme le salut par la foi et la grâce, ainsi que des enseignements pratiques sur l'amour, le devoir du chrétien et la sainteté.

## Chapitre 1

1:1	Paulos, esclave de Yéhoshoua Mashiah, appelé apôtre, mis à part pour l'Évangile d'Elohîm,
1:2	qu'il avait auparavant promis par le moyen de ses prophètes dans les saintes Écritures,
1:3	concernant son Fils qui, selon la chair, est provenu de la postérité de David,
1:4	déterminé Fils d'Elohîm avec puissance, selon l'Esprit de sainteté, par la résurrection d'entre les morts : Yéhoshoua Mashiah, notre Seigneur.
1:5	Au moyen duquel, nous avons reçu la grâce et l'apostolat pour amener en faveur de son nom toutes les nations à l'obéissance de la foi,
1:6	parmi lesquelles vous êtes aussi, vous qui êtes appelés par Yéhoshoua Mashiah.
1:7	À tous ceux qui sont à Rome, bien-aimés d'Elohîm, appelés et saints<!--En hébreu, plusieurs termes sont employés pour parler de la sanctification. Il y a par exemple le verbe « qadash » qui signifie « consacrer », « sanctifier » et le nom commun « qodesh » qui signifie « mise à part », « sainteté », « consécration », « séparation ». Ces termes sont appliqués aux humains et aux objets, quand ceux-ci sont réservés pour le service d'Elohîm. Le terme grec « hagios » signifie « consacré à Elohîm », « saint », « sacré », « pieux ». Contrairement à ce qui est enseigné par certains, on ne devient pas saint après avoir été canonisé ou béatifié, mais par le sang de Yéhoshoua ha Mashiah, lorsqu'une personne l'accepte comme son Seigneur et Sauveur (Hé. 10:10, 13:12). Ainsi, sous la nouvelle alliance, tous les disciples de Yéhoshoua sont saints ou consacrés (Hé. 10:10-14 ; Ph. 1:1). Cet état de sainteté se maintient par l'action du Saint-Esprit, par le moyen de la parole d'Elohîm (Jn. 15:3, 17:17 ; 1 Th. 5:23-24 ; Ep. 5:25-26 ; 2 Co. 3:18). Ainsi, au moment de la conversion, le chrétien s'engage dans un processus de sanctification qui durera toute sa vie. La sainteté parfaite ne sera atteinte qu'au retour du Seigneur. Alors, tous ceux qui lui appartiennent, connaîtront la rédemption totale : corps, âme et esprit (1 Co. 15:29-50 ; Ep. 5:27 ; Ph. 3:20-21 ; 1 Jn. 3:2 ; Ap. 22:12).--> : à vous, grâce et shalôm, de la part d'Elohîm notre Père et Seigneur Yéhoshoua Mashiah !
1:8	Premièrement, je rends vraiment grâce à mon Elohîm par le moyen de Yéhoshoua Mashiah, en faveur de vous tous, parce qu’on publie votre foi dans le monde entier.
1:9	Car Elohîm, que je sers en mon esprit, par l'Évangile de son Fils, m'est témoin que je fais sans cesse mention de vous.
1:10	Demandant toujours dans mes prières si je réussirai enfin, par la volonté d'Elohîm, à venir chez vous.
1:11	Car je désire ardemment vous voir pour vous communiquer quelque don de grâce spirituel, afin que vous soyez affermis,
1:12	c’est-à-dire afin de nous encourager ensemble au milieu de vous par le moyen de la foi qui nous est commune, à vous et à moi.
1:13	Or frères, je ne veux pas que vous ignoriez que je me suis souvent proposé d'aller chez vous, afin de recueillir quelque fruit aussi bien parmi vous, que parmi les autres nations, mais j'en ai été empêché jusqu'à présent.
1:14	Je suis débiteur des Grecs et des barbares, des sages et des inintelligents.
1:15	Ainsi, selon moi, je suis bien disposé à vous annoncer l'Évangile, à vous aussi qui êtes à Rome.
1:16	Car je n'ai pas honte de l'Évangile du Mashiah, car il est la puissance d'Elohîm pour le salut de quiconque croit, du Juif premièrement, mais aussi du Grec,
1:17	car la justice d'Elohîm se révèle en lui<!--L'Évangile.-->, à partir de la foi pour la foi, selon qu'il est écrit : Or le juste vivra à partir de la foi<!--Ha. 2:4.-->.
1:18	Car la colère d'Elohîm se révèle du ciel contre toute impiété et toute injustice des humains qui retiennent la vérité dans l'injustice,
1:19	parce que ce qu'on peut connaître d'Elohîm est manifeste parmi eux, car Elohîm le leur a manifesté.
1:20	Car ses choses invisibles, mais aussi sa puissance éternelle et sa divinité, se voient clairement depuis la création du monde dans ses ouvrages quand on y réfléchit, pour qu'ils soient inexcusables,
1:21	parce qu'ayant connu Elohîm, ils ne l'ont pas glorifié comme Elohîm et ils ne lui ont pas rendu grâces, mais ils sont devenus vains dans leurs raisonnements, et leur cœur destitué d'intelligence a été couvert de ténèbres<!--Ep. 4:18.-->.
1:22	Ils affirment être sages, ils sont devenus stupides<!--Le mot grecque fait allusion au sel qui a perdu sa force et sa saveur. Voir Lu. 14:34.-->,
1:23	et ils ont changé la gloire de l'Elohîm incorruptible en la ressemblance d'image<!--Ex. 20:4-5, 32:1-35 ; Mt. 22:20 ; Mc. 12:16 ; Lu. 20:24 ; Ap. 13:14-15, 14:9-11, 15:2, 16:2, 19:20, 20:4.--> de l'être humain corruptible, et des oiseaux, et des quadrupèdes, et des reptiles.
1:24	C'est pourquoi aussi l'Elohîm les a livrés, dans les désirs de leurs propres cœurs, à l'impureté pour déshonorer entre eux leurs propres corps<!--Elohîm les a livrés à l'esprit d'égarement (1 R. 22:21-24 ; 2 Th. 2:11-12).-->,
1:25	eux qui ont échangé la vérité d'Elohîm contre le mensonge et qui ont adoré et servi la créature, au lieu du Créateur, qui est béni pour les âges. Amen !
1:26	C'est pourquoi Elohîm les a livrés à leurs passions<!--Vient du grec « pathos » qui signifie « tout ce qui peut arriver à quelqu'un, que ce soit triste ou joyeux », « affliction de l'esprit », « émotion », « passion ». Voir Mt. 19:4 ; Mc. 10:6.--> déshonorantes, car même les femelles parmi eux ont échangé les rapports sexuels naturels pour des relations contre nature.
1:27	De même aussi les mâles<!--Vient du grec « arrhen » qui fait allusion au sexe masculin. Voir Mt. 19:4 ; Mc. 10:6.-->, abandonnant les rapports sexuels naturels avec le sexe féminin<!--Vient du grec « thelus » qui signifie aussi « femelle ». Voir Mt. 19:4 ; Mc. 10:6.-->, se sont embrasés dans leurs désirs<!--Désir, désir ardent, besoin impérieux, convoitise. Le mot grec « orexis » est utilisé dans un bon et un mauvais sens, aussi bien naturel et licite et même de certains désirs (appétit pour la nourriture), que pour les désirs corrompus et illicites.--> les uns pour les autres, accomplissant des choses honteuses, mâle avec mâle, et recevant en eux-mêmes la juste et correcte récompense de leur égarement.
1:28	Et comme ils n'ont pas jugé bon d'avoir la connaissance précise et correcte d'Elohîm, aussi Elohîm les a livrés<!--De. 28:28 ; 1 R. 22 ; 2 Th. 2:11.--> à une pensée réprouvée<!--Vient du grec « adokimos » qui signifie « refusé à l'épreuve », « non approuvé », « celui qui ne prouve pas ce qu'il devrait », « faux », « réprouvé ». Ce terme est utilisé pour les monnaies et les poids. Voir 2 Ti. 2:15.-->, pour pratiquer des choses qui ne sont pas convenables,
1:29	étant remplis de toute espèce d'injustice, de relation sexuelle illicite, de méchanceté, d'avarice, de malignité, pleins d'envie, de meurtre, de querelle, de tromperie et de mauvais caractère, rapporteurs,
1:30	médisants, haïssant Elohîm, insolents, orgueilleux, vains, inventeurs de choses mauvaises, rebelles à leurs parents,
1:31	sans intelligence, ne tenant pas ce qu'ils ont promis, sans affection naturelle, sans traité ou alliance, sans miséricorde<!--Impitoyable.-->.
1:32	Lesquels ayant connu l'ordonnance d'Elohîm qui déclare dignes de mort ceux qui pratiquent de telles choses, non seulement ils les font eux-mêmes, mais encore ils approuvent ceux qui les pratiquent.

## Chapitre 2

2:1	C'est pourquoi, ô humain, qui que tu sois, toi qui juges, tu es inexcusable. Car, en jugeant les autres, tu te condamnes toi-même, puisque toi qui juges, tu commets les mêmes choses.
2:2	Mais nous savons que le jugement d'Elohîm est selon la vérité, contre ceux qui pratiquent de telles choses.
2:3	Mais penses-tu ceci, ô humain, qui juges ceux qui pratiquent de telles choses et qui les fais toi-même, que tu échapperas au jugement d'Elohîm ?
2:4	Ou méprises-tu la richesse de sa bénignité, de sa tolérance et de sa patience, ne reconnaissant pas que la bonté d'Elohîm te conduit à la repentance ?
2:5	Mais, par ta dureté et par ton cœur qui n'admet aucun changement de l'esprit, tu t'amasses la colère pour le jour de la colère et de la révélation du juste jugement d'Elohîm,
2:6	qui rendra à chacun selon ses œuvres :
2:7	la vie éternelle en effet à ceux qui, par leur persévérance dans les bonnes œuvres, cherchent la gloire, et l'honneur et l'immortalité.
2:8	Mais il y aura de l'indignation en effet et de la colère contre ceux qui ont un esprit partisan<!--Voir Ph. 2:3.-->, et qui sont rebelles à la vérité et obéissent à l'injustice.
2:9	Il y aura tribulation et affreuse calamité sur toute âme humaine qui accomplit le mal, du Juif premièrement, mais aussi du Grec.
2:10	Mais gloire, honneur et paix à tout homme qui fait ce qui est bon, au Juif premièrement, mais aussi au Grec.
2:11	Car auprès d'Elohîm il n'y a pas d'égard à l'apparence des personnes<!--Ou partialité.-->.
2:12	Car tous ceux qui auront péché sans la connaissance de la torah, périront aussi sans la connaissance de la torah, et tous ceux qui auront péché avec la torah, seront jugés par le moyen de la torah.
2:13	Car ce ne sont pas les auditeurs de la torah qui sont justes devant Elohîm, mais ce sont les observateurs de la torah qui seront justifiés.
2:14	Car, quand les nations qui n'ont pas la torah pratiquent naturellement les choses de la torah, n'ayant pas la torah, elles sont une torah pour elles-mêmes.
2:15	Elles démontrent l'œuvre de la torah écrite dans leurs cœurs, leur conscience en rend témoignage, et leurs raisonnements les accusent ou même les défendent tour à tour,
2:16	au jour où Elohîm jugera les secrets des humains selon mon Évangile, par le moyen de Yéhoshoua Mashiah.
2:17	Voici, tu te nommes Juif, tu te reposes entièrement sur la torah et tu te glorifies en Elohîm,
2:18	et tu connais sa volonté, tu discernes aussi les choses qui sont importantes, étant instruit à partir de la torah.
2:19	Et tu te persuades d'être un guide des aveugles, la lumière de ceux qui sont dans la ténèbre,
2:20	un professeur des insensés, un docteur des enfants<!--métaphore enfantin, sans instruction, non qualifié.-->, ayant le modèle de la connaissance et de la vérité dans la torah.
2:21	Toi donc, qui enseignes un autre, tu ne t'enseignes pas toi-même ! Toi qui prêches de ne pas voler, tu voles !
2:22	Toi qui dis de ne pas commettre d'adultère, tu commets l'adultère ! Toi qui as en abomination les idoles, tu commets des sacrilèges !
2:23	Toi qui te glorifies de la torah, tu déshonores Elohîm par la transgression de la torah !
2:24	Car le Nom d'Elohîm est blasphémé parmi les nations à cause de vous, comme cela est écrit<!--Voir Es. 52:5 ; Ez. 36: 20-23.-->.
2:25	Car la circoncision est en effet utile si tu gardes la torah. Mais si tu es un transgresseur de la torah, ta circoncision devient incirconcision.
2:26	Si donc l'incirconcis observe les ordonnances de la torah, son incirconcision ne sera-t-elle pas tenue pour circoncision ?
2:27	Et l'incirconcis de nature qui accomplit la torah ne te jugera-t-il pas, toi qui la transgresses tout en ayant la lettre de la torah et la circoncision ?
2:28	Car le Juif, ce n'est pas celui qui en a l'apparence<!--Paulos (Paul) dénonce ici le formalisme (2 Ti. 3:5). L'apparence de la piété correspond aux vêtements des brebis : « Mais gardez-vous des faux prophètes qui viennent à vous en habits de brebis, mais au-dedans, ce sont des loups ravisseurs. » (Mt. 7:15). « Et je vis une autre bête montant de la Terre. Et elle avait deux cornes semblables à celles de l'Agneau, mais elle parlait comme le dragon. » (Ap. 13:11). Cette bête a l'apparence d'un agneau, mais sa voix est celle du dragon, il s'agit de Satan.-->, et la circoncision, ce n'est pas celle qui est apparente, dans la chair.
2:29	Mais le Juif est celui qui l'est dans le secret, et la circoncision, c'est celle du cœur, selon l'Esprit et non selon la lettre ; dont la louange ne vient pas des humains, mais d'Elohîm.

## Chapitre 3

3:1	Quel est donc l'avantage du Juif, ou quelle est l'utilité de la circoncision ?
3:2	Grande de toute manière ! Premièrement parce que les oracles d'Elohîm leur ont été confiés en effet<!--Ps. 78:5 et 147:19.-->.
3:3	Quoi ? Car si quelques-uns n'ont pas cru, leur incrédulité anéantira-t-elle la fidélité d'Elohîm ?
3:4	Que cela n’arrive jamais ! qu'Elohîm soit vrai et tout être humain menteur, selon qu'il est écrit : Pour que tu sois justifié dans tes paroles et que tu sois victorieux lorsqu’on te juge<!--Ps. 51:6.-->.
3:5	Or si notre injustice établit la justice d'Elohîm, que dirons-nous ? N’est-il pas injuste l'Elohîm qui inflige la colère ? (Je parle à la manière des humains.)
3:6	Que cela n’arrive jamais ! Autrement, comment Elohîm jugera-t-il le monde ?
3:7	Car, si par mon mensonge la vérité d'Elohîm a abondé pour sa gloire, pourquoi suis-je moi-même encore jugé comme pécheur ?
3:8	Et ne ferions-nous pas les choses mauvaises pour que viennent les choses bonnes, comme nous sommes calomniés et comme quelques-uns déclarent que nous le disons ? Le jugement de ces gens est juste !
3:9	Quoi donc ! Avons-nous la prééminence<!--« Avoir de l'avance sur un autre, avoir la prééminence sur un autre », « exceller », « surpasser », « surpasser dans l'excellence qui peut être passée à son crédit ».--> sur les autres ? Nullement ! Car nous avons déjà prouvé<!--apporter une charge contre (c'est-à-dire dans ce qui a été dit précédemment).--> que tous, tant Juifs que Grecs, sont sous le péché,
3:10	selon qu'il est écrit : Il n'y a pas de juste, pas même un seul<!--Ps. 14:3.-->.
3:11	Il n'y a personne qui comprenne, il n'y a personne qui recherche Elohîm.
3:12	Tous ont dévié, ils se sont tous ensemble rendus inutiles. Il n'y en a pas un qui pratique la bénignité, il n’y en a pas même un seul !
3:13	Leur gosier est une tombe ouverte, ils se sont servis de leurs langues pour tromper, un venin d'aspic est sous leurs lèvres,
3:14	leur bouche est pleine de malédiction et d'amertume,
3:15	leurs pieds sont rapides pour verser le sang,
3:16	destruction et malheur sont sur leurs voies,
3:17	et ils n'ont pas connu la voie de la paix,
3:18	la crainte d'Elohîm n'est pas devant leurs yeux<!--Ps. 14.-->.
3:19	Or nous savons que tout ce que la torah dit, elle l'adresse à ceux qui sont sous la torah, afin que toute bouche soit fermée, et que tout le monde devienne sujet au châtiment<!--Dans un jugement, quelqu'un qui perd son procès, le débiteur de quelqu'un.--> venant d'Elohîm.
3:20	À cause de ceci, aucune chair ne sera justifiée devant lui à partir des œuvres de la torah, car c'est par le moyen de la torah que vient la connaissance précise et correcte du péché.
3:21	Mais maintenant, sans la torah, est manifestée la justice d'Elohîm, la torah et les prophètes lui rendant témoignage :
3:22	mais la justice d'Elohîm par le moyen de la foi en Yéhoshoua Mashiah pour tous et sur tous ceux qui croient. Car il n'y a pas de différence :
3:23	car tous ont péché<!--Le mot « péché » vient du terme grec « hamartano » : « manquer la marque », « manquer le chemin de la droiture et de l'honneur », « s'éloigner de la loi d'Elohîm ». Le péché est la violation délibérée de la loi divine et l'absence de droiture.--> et n'atteignent<!--Le mot grec ici habituellement traduit par « privé » signifie plutôt : « venir en retard ou trop tardivement », « être laissé derrière dans la course et ainsi ne pas pouvoir atteindre le but » ou encore « tomber près du but ».--> pas la gloire d'Elohîm,
3:24	étant justifiés gratuitement par sa grâce, par le moyen de la rédemption<!--La rédemption est la délivrance par le paiement d'un prix. Trois termes grecs sont utilisés pour parler de la rédemption : 1 - Agorázo : acheter un objet au marché (« agora » signifiant marché). Les pécheurs sont considérés comme des esclaves vendus au marché (Ro. 7:14). 2 - Exagorázo : acheter et amener un objet hors du marché (Ga. 3:13, 4:5). L'esclave acheté et amené hors du marché, est définitivement délivré. 3 - Lutróō : détacher, rendre libre (Lu. 24:21 ; Tit. 2:14 ; 1 Pi. 1:18). Yéhoshoua ha Mashiah nous a délivrés du péché, de la puissance de Satan et de la loi mosaïque (Col. 1:12-14, 2:14-17 ; 1 Jn. 3:5).--> qui est en Yéhoshoua Mashiah,
3:25	qu'Elohîm a exposé aux regards pour être la propitiation<!--Le terme propitiation vient du grec « hilasterion » qui signifie « ce qui est expié, ce qui rend propice ou le don qui assure la propitiation ». C'est aussi le lieu où s'accomplit la propitiation (Hé. 9:5), c'est-à-dire, le couvercle de l'arche. Lors du grand jour des expiations (« Yom Kippour » en hébreu), l'aspersion du sang était faite sur le propitiatoire (Lé. 16:14). Le Seigneur Yéhoshoua ha Mashiah est notre victime propitiatoire (1 Jn. 2:2, 4:10).--> au moyen de la foi, par son sang, pour la démonstration de sa justice, parce qu'il n'avait pas été tenu compte des péchés commis antérieurement,
3:26	dans la tolérance d'Elohîm, pour la démonstration de sa justice dans le temps présent, pour être juste lui-même et justifiant celui qui est de la foi en Yéhoshoua.
3:27	Où est donc le sujet de se glorifier ? Il est exclu. Par le moyen de quelle torah ? Des œuvres ? Non, mais par le moyen de la torah de la foi.
3:28	Nous estimons donc que l'être humain est justifié par la foi, sans les œuvres de la torah.
3:29	Mais Elohîm est-il seulement celui des Juifs ? N'est-il pas aussi celui des nations ? Oui, aussi celui des nations,
3:30	puisqu'il n'y a qu'un seul Elohîm<!--1 Co. 8:5-6.--> qui justifiera la circoncision à partir de la foi et l'incirconcision par le moyen de la foi.
3:31	Abolissons-nous donc la torah par le moyen de la foi ? Que cela n’arrive jamais ! Au contraire, nous établissons la torah.

## Chapitre 4

4:1	Que dirons-nous donc qu'Abraham, notre père, a obtenu selon la chair ?
4:2	Car si Abraham a été justifié à partir des œuvres, il a de quoi se glorifier, mais non pas envers Elohîm.
4:3	Car que dit l'Écriture ? Abraham a cru en Elohîm et cela lui a été compté comme justice<!--Voir Ge. 15:6.-->.
4:4	Or à celui qui fait un travail, le salaire ne lui est pas compté comme une grâce, mais comme une chose due.
4:5	Mais à celui qui ne fait pas un travail, mais qui croit en celui qui justifie l'impie, sa foi lui est comptée comme justice.
4:6	De même aussi, David parle de la déclaration de bénédiction de l'être humain à qui Elohîm compte la justice sans les œuvres :
4:7	bénis sont ceux à qui les violations de la torah sont pardonnées, et dont les péchés sont couverts !
4:8	béni est l'homme à qui le Seigneur ne tient aucun compte de son péché<!--Ps. 32:2.--> !
4:9	Cette déclaration de bénédiction vient-elle sur la circoncision ou aussi sur l'incirconcision ? Car nous disons que la foi fut comptée comme justice à Abraham.
4:10	Comment donc lui a-t-elle été comptée ? Quand il était dans la circoncision ou dans l'incirconcision ? Non dans la circoncision, mais dans l'incirconcision.
4:11	Et il a reçu le signe de la circoncision comme sceau de la justice de la foi qu'il avait eue dans l'incirconcision, pour qu'il soit le père de tous ceux qui croient quoique dans l'incirconcision, pour que la justice leur soit aussi comptée,
4:12	et le père de la circoncision pour ceux qui, non seulement sont de la circoncision, mais aussi qui marchent sur les traces de la foi que notre père Abraham avait eue dans l'incirconcision.
4:13	Car ce n'est pas par le moyen de la torah que la promesse d'être héritier du monde a été faite à Abraham ou à sa postérité, mais par le moyen de la justice de la foi.
4:14	Car si les héritiers le sont à partir de la torah, la foi est rendue vaine et la promesse est abolie.
4:15	Car la torah produit la colère, et là où il n'y a pas de torah, il n'y a pas non plus de transgression.
4:16	Pour cette raison, c'est à partir de la foi, afin que ce soit selon la grâce, pour que la promesse soit assurée à toute la postérité, non seulement à celle qui est à partir de la torah, mais aussi à celle qui est à partir de la foi d'Abraham, qui est le père de nous tous,
4:17	selon qu'il est écrit : Je t'ai établi père de beaucoup de nations, devant l'Elohîm en qui il a cru, celui qui donne la vie aux morts et qui appelle les choses qui ne sont pas, comme celles qui sont.
4:18	Qui, contre espérance, crut avec espérance, pour devenir le père de beaucoup de nations, selon ce qui avait été dit<!--Ge. 15:1-5.--> : Ainsi sera ta postérité.
4:19	Et n'étant pas faible dans la foi, il n'a pas considéré que son corps était déjà atteint par la mort<!--Voir Hé. 11:12.-->, vu qu'il avait environ 100 ans, et que la matrice de Sarah était morte.
4:20	Et il n'a pas douté à l'égard de la promesse d'Elohîm, par incrédulité, mais il a été fortifié par la foi et il a donné gloire à Elohîm,
4:21	et étant pleinement convaincu que ce qu'il a promis, il est puissant aussi pour l'accomplir.
4:22	C'est pourquoi aussi cela lui a été compté comme justice.
4:23	Mais ce n'est pas à cause de lui seul qu'il est écrit<!--Voir Ge. 15:6.--> : Cela lui a été compté,
4:24	mais aussi à cause de nous, à qui cela est sur le point d'être compté, à nous qui croyons en celui qui a réveillé d’entre les morts Yéhoshoua notre Seigneur,
4:25	lequel a été livré à cause de nos fautes, et qui a été réveillé à cause de notre justification.

## Chapitre 5

5:1	Étant donc justifiés<!--La justification est l'œuvre par laquelle Elohîm déclare juste un pécheur (Ro. 4:3, 5:1-9 ; Ga. 2:16, 3:11). Cette justice ne s'obtient pas par les œuvres ou les efforts qu'une personne peut faire. Elle n'est donc pas un dû ou une récompense, mais une grâce (Ep. 2:5-9) qui repose uniquement sur le sacrifice de Yéhoshoua ha Mashiah (1 Pi. 2:24).--> à partir de<!--Vient de la préposition « ek » dénotant une origine (point d'où l'action ou le mouvement procède), depuis, de, hors (d'un lieu, du temps, d'une cause).--> la foi, nous avons la paix avec Elohîm, par le moyen de notre Seigneur Yéhoshoua Mashiah.
5:2	Par le moyen duquel aussi nous avons eu accès par la foi à cette grâce dans laquelle nous tenons ferme, et nous nous glorifions dans l'espérance de la gloire d'Elohîm.
5:3	Et non seulement cela, mais nous nous glorifions même dans les tribulations, sachant que la tribulation produit la persévérance,
5:4	et la persévérance l'épreuve, et l'épreuve l'espérance.
5:5	Or l'espérance ne rend pas honteux, parce que l'amour d'Elohîm est répandu dans nos cœurs par le moyen de l'Esprit Saint qui nous a été donné.
5:6	Car, lorsque nous étions encore faibles<!--« Maladifs », « infirmes », « malades ».-->, Mashiah est mort au temps convenable en faveur des impies.
5:7	Car à peine quelqu'un mourra-t-il en faveur d'un juste, mais pour ce qui est bon, peut-être quelqu’un ose même mourir -
5:8	mais Elohîm montre son amour envers nous, puisque, alors que nous étions encore des pécheurs, Mashiah est mort en notre faveur.
5:9	À bien plus forte raison donc, étant maintenant justifiés par son sang, serons-nous sauvés de la colère<!--Voir 1 Th. 1:9-10, 5:9 ; Ap. 15:5-8, 16:1-21.--> par son moyen.
5:10	Car si, étant ennemis, nous avons été réconciliés avec Elohîm au moyen de la mort de son Fils, à plus forte raison, étant réconciliés, serons-nous sauvés par sa vie.
5:11	Et non seulement cela, mais nous nous glorifions même en Elohîm par le moyen de notre Seigneur Yéhoshoua Mashiah, par le moyen duquel nous avons maintenant obtenu la réconciliation.
5:12	C'est pourquoi, comme par le moyen d'un seul être humain<!--Ge. 1:26.--> le péché est entré dans le monde, et par le moyen du péché la mort, de même aussi la mort s'est étendue sur tous les humains, par lequel tous péchèrent.
5:13	Car jusqu'à la torah, le péché était dans le monde. Or le péché n'est pas mis en compte, quand il n'y a pas de torah.
5:14	Mais la mort a régné depuis Adam jusqu'à Moshé, même sur ceux qui n'avaient pas péché par une transgression semblable à celle d'Adam, lequel est la figure<!--Vient du grec « tupos » qui signfie « type ». C'est le modèle à laquelle une chose doit être conforme ou un exemple qui doit être imité.--> de celui qui est sur le point d'arriver.
5:15	Mais, il n'en est pas du don de grâce comme de la faute. Car, si par la faute d'un seul, il en est beaucoup qui sont morts, à bien plus forte raison la grâce d'Elohîm et le don de grâce d'un seul être humain, Yéhoshoua Mashiah, ont-ils été abondamment répandus sur beaucoup.
5:16	Et le don n'est pas comme par le moyen d'un seul qui a péché, car, à partir d'une seule faute, le jugement aboutit en effet à la condamnation, tandis qu'à partir de beaucoup de fautes, le don de grâce aboutit à un acte de justice.
5:17	Car si par la faute d'un seul, la mort a régné par le moyen de ce seul, à plus forte raison ceux qui reçoivent l'abondance de la grâce et du don de la justice régneront-ils dans la vie par le moyen du seul Yéhoshoua Mashiah.
5:18	Ainsi donc, comme par le moyen d'une seule faute ce fut pour tous les humains la condamnation, de même aussi, par le moyen d'une seule justice, c'est pour tous les humains la justification qui donne la vie.
5:19	Car de même que par le moyen de la désobéissance d'un seul être humain, beaucoup ont été rendus pécheurs, de même aussi par le moyen de l'obéissance d'un seul, beaucoup seront rendus justes.
5:20	Or la torah est entrée en complément afin que la faute se multipliât, mais là où le péché s'est multiplié, la grâce a surabondé,
5:21	afin que, comme le péché a régné dans la mort, ainsi la grâce régnât au moyen de la justice pour conduire à la vie éternelle, au moyen de Yéhoshoua Mashiah notre Seigneur.

## Chapitre 6

6:1	Que dirons-nous donc ? Demeurerons-nous dans le péché afin que la grâce se multiplie ?
6:2	Que cela n'arrive jamais ! Nous qui sommes morts au péché, comment vivrons-nous encore en lui ?
6:3	Ou bien ignorez-vous que nous tous qui avons été baptisés en Mashiah Yéhoshoua, avons été baptisés en sa mort ?
6:4	Nous avons donc été ensevelis avec lui par le moyen du baptême en sa mort, afin que, comme Mashiah a été réveillé des morts par le moyen de la gloire du Père, de même nous aussi nous marchions en nouveauté de vie.
6:5	Car si nous sommes nés ensemble<!--« D'une origine commune », « parenté », « affinité », « analogie ».--> avec lui en devenant semblables à sa mort, nous le serons aussi à sa résurrection.
6:6	Sachant que notre vieil<!--Le vieil être humain représente notre ancienne nature. Ep. 4:22 ; Col. 3:9.--> être humain a été crucifié avec lui, afin que le corps du péché soit inactif et que nous ne soyons plus esclaves du péché.
6:7	Car celui qui est mort est justifié<!--Vient du grec « dikaioo » qui signifie « rendre juste ou comme il doit être », « montrer », « exposer », « démontrer sa justice », « comme on est et qu'on souhaite être considéré », « déclarer », « prononcer la justice de quelqu'un, le justifier ».--> du péché.
6:8	Or si nous sommes morts avec Mashiah, nous croyons que nous vivrons aussi avec lui.
6:9	Sachant que Mashiah réveillé des morts ne meurt plus, la mort n'a plus domination sur lui.
6:10	Car en ce qu’il mourut, il mourut pour le péché une fois pour toutes ; mais en ce qu’il vit, il vit pour Elohîm.
6:11	De même vous aussi, estimez que vous êtes vraiment morts au péché, mais vivants pour Elohîm en Yéhoshoua Mashiah notre Seigneur.
6:12	Que le péché ne règne donc pas dans votre corps mortel pour lui obéir dans ses désirs<!--Voir Ga. 5:16,24.-->.
6:13	Et n'offrez pas vos membres au péché pour être des armes de l'injustice, mais offrez-vous vous-mêmes à Elohîm, comme des morts étant devenus vivants, et offrez vos membres à Elohîm pour être des armes de justice.
6:14	Car le péché n'aura pas domination sur vous, parce que vous n'êtes plus sous la torah, mais sous la grâce.
6:15	Quoi donc ! Pécherons-nous, parce que nous ne sommes plus sous la torah, mais sous la grâce ? Que cela n'arrive jamais !
6:16	Ne savez-vous pas qu'en vous offrant à quelqu'un comme esclaves pour lui obéir, vous êtes esclaves de celui à qui vous obéissez, soit du péché pour la mort, soit de l'obéissance pour la justice ?
6:17	Mais grâce à Elohîm de ce qu'ayant été les esclaves du péché, vous avez obéi de cœur à cette forme de doctrine à laquelle vous avez été livrés !
6:18	Mais ayant été rendus libres du péché, vous êtes devenus esclaves de la justice.
6:19	Je parle à la façon des hommes, à cause de la faiblesse de votre chair. De même que vous avez offert vos membres comme esclaves à l'impureté et à la violation de la torah, pour la violation de la torah, de même offrez maintenant vos membres comme esclaves à la justice pour la sanctification.
6:20	Car lorsque vous étiez esclaves du péché, vous étiez libres à l'égard de la justice.
6:21	Quel fruit donc aviez-vous alors ? Des choses dont maintenant vous avez honte. Car la fin de ces choses, c'est la mort.
6:22	Mais maintenant, rendus libres<!--Voir Jn. 8:32,36 et Ro. 6:18, 8:2,21.--> du péché et devenus esclaves d'Elohîm, vous avez votre fruit dans la sanctification et pour fin la vie éternelle.
6:23	Car le salaire du péché, c'est la mort, mais le don de grâce d'Elohîm, c'est la vie éternelle par Yéhoshoua Mashiah notre Seigneur.

## Chapitre 7

7:1	Ou bien ignorez-vous, frères, car je parle à ceux qui connaissent la torah, que la torah exerce sa domination sur l'être humain aussi longtemps qu'il est vivant ?
7:2	Car la femme mariée est liée par la torah à son mari tant qu'il est vivant, mais si son mari meurt, elle est déliée<!--Être séparé de, déchargé de, délié de n'importe qui. Le mot signifie aussi « rompre toute relation avec quelqu'un ».--> de la torah du mari.
7:3	Ainsi donc elle sera appelée adultère si, du vivant de son mari, elle devient la femme d'un autre mari. Mais si son mari meurt, elle est affranchie<!--Né libre. Dans le sens civil : celui qui n'est pas un esclave, celui qui cesse d'être un esclave, libéré, affranchi, libre, exempt, non lié par une obligation. Dans le sens éthique : libre du joug de la Loi Mosaïque.--> de la torah, de sorte qu'elle ne sera pas adultère en devenant la femme d'un autre mari.
7:4	C’est pourquoi, mes frères, vous aussi, vous avez été, au moyen du corps du Mashiah, mis à mort en ce qui concerne la torah, pour être à un autre, à celui qui a été réveillé des morts, afin que nous portions des fruits pour Elohîm.
7:5	Car, lorsque nous étions dans la chair, les passions des péchés, à cause de la torah, agissaient dans nos membres de manière à produire des fruits pour la mort.
7:6	Mais maintenant, nous sommes déliés de la torah, étant morts à celle sous laquelle nous étions retenus, de sorte que nous sommes esclaves dans la nouveauté de l'Esprit, et non dans la vieillesse de la lettre.
7:7	Que dirons-nous donc ? La torah est-elle péché ? Que cela n'arrive jamais ! Mais je n'ai connu le péché que par le moyen de la torah. Car je n'aurais pas connu la convoitise, si la torah n'avait pas dit : Tu ne convoiteras pas<!--Voir Ex. 20:17.-->.
7:8	Mais, saisissant l'occasion, le péché produisit en moi, par le moyen du commandement, toutes sortes de convoitises, parce que sans la torah, le péché est mort.
7:9	Mais, autrefois sans torah, je vivais. Mais le commandement étant venu, le péché a repris vie, mais moi je mourus.
7:10	Et le commandement qui était pour la vie, j’ai trouvé qu’il était pour la mort.
7:11	Car le péché saisissant l'occasion, m'a trompé par le moyen du commandement, et m'a tué par son moyen.
7:12	Ainsi, la torah est vraiment sainte, et le commandement est saint, et juste et bon.
7:13	Ce qui est bon est-il donc devenu pour moi la mort ? Que cela n’arrive jamais ! Mais le péché, afin de se manifester en tant que péché, a produit en moi la mort par le moyen de ce qui est bon, afin que, par le moyen du commandement, le péché devienne excessivement pécheur.
7:14	Car nous savons que la torah est spirituelle, mais moi, je suis charnel, vendu sous le péché.
7:15	Car ce que j'accomplis, je ne le comprends pas, car ce que je veux, je ne le pratique pas, mais ce que je hais, je le fais.
7:16	Or si ce que je ne veux pas, je le fais, j'avoue que la torah est bonne.
7:17	Mais maintenant ce n'est plus moi qui accomplis cela, mais le péché qui habite en moi.
7:18	Car je sais qu'il n'y a rien de bon en moi, c'est-à-dire, dans ma chair, parce que le vouloir est à ma portée, mais je ne trouve pas le moyen d'accomplir ce qui est bon.
7:19	Car le bien que je veux, je ne le fais pas, mais le mal que je ne veux pas, je le pratique.
7:20	Or si ce que je ne veux pas, moi, je le fais, ce n'est plus moi qui accomplis cela, mais le péché habitant en moi.
7:21	Je trouve donc cette torah au-dedans de moi : quand je veux faire ce qui est bon, c'est le mal qui est à ma portée.
7:22	Car je prends plaisir à la torah d'Elohîm selon l'homme intérieur,
7:23	mais je vois dans mes membres une autre torah qui lutte contre la torah de ma pensée<!--Pensée : du grec « nous », c'est-à-dire l'esprit, l'intelligence, la pensée, le bon sens, la raison.--> et qui me rend captif de la torah du péché qui est dans mes membres.
7:24	Misérable être humain que je suis ! Qui me délivrera du corps de cette mort ?
7:25	Je rends grâce à Elohîm au moyen de Yéhoshoua Mashiah notre Seigneur ! Ainsi donc moi-même, par la pensée, je suis en effet l'esclave de la torah d'Elohîm, mais par la chair, de la torah du péché.

## Chapitre 8

8:1	Il n'y a donc maintenant aucune condamnation pour ceux qui sont en Mashiah Yéhoshoua, qui marchent, non selon la chair, mais selon l'Esprit.
8:2	Car la torah de l'Esprit de vie en Mashiah Yéhoshoua m'a rendu libre<!--Voir Jn. 8:32,36.--> de la torah du péché et de la mort.
8:3	Car ce qui était impossible à la torah parce qu'elle était faible à cause de la chair, Elohîm, en envoyant son propre Fils dans une chair semblable à celle du péché et au sujet du péché, a condamné le péché dans la chair,
8:4	afin que l'ordonnance de la torah soit accomplie en nous, qui ne marchons pas selon la chair, mais selon l'Esprit.
8:5	Car, ceux qui sont selon la chair pensent<!--Voir Col. 3:2.--> aux choses de la chair, mais ceux qui sont selon l'Esprit aux choses de l'Esprit.
8:6	Car la pensée et le but de la chair, c'est la mort, mais la pensée et le but de l'Esprit, c'est la vie et la paix.
8:7	Parce que la pensée de la chair est inimitié contre Elohîm, car elle ne se soumet pas à la torah d'Elohîm et qu'elle n'en est même pas capable.
8:8	Or ceux qui sont dans la chair ne peuvent plaire à Elohîm.
8:9	Mais vous, vous n'êtes pas dans la chair, mais dans l'Esprit, puisque l'Esprit d'Elohîm habite en vous. Mais si quelqu'un n'a pas l'Esprit du Mashiah<!--Notez que le Saint-Esprit est aussi appelé l'Esprit de Yéhoshoua (Ac. 16:7).-->, celui-là n'est pas à lui.
8:10	Et si Mashiah est en vous, le corps est vraiment mort à cause du péché, mais l'Esprit est vie à cause de la justice.
8:11	Et si l'Esprit de celui qui a réveillé Yéhoshoua d'entre les morts habite en vous, celui qui a réveillé le Mashiah d'entre les morts, rendra aussi la vie à vos corps mortels, à cause de son Esprit qui habite en vous.
8:12	Ainsi donc, frères, nous ne sommes pas débiteurs de la chair pour vivre selon la chair.
8:13	Car si vous vivez selon la chair, vous êtes sur le point de mourir. Mais si par l'Esprit vous faites mourir les actions du corps, vous vivrez.
8:14	Car tous ceux qui sont conduits par l'Esprit d'Elohîm sont fils d'Elohîm.
8:15	Car vous n'avez pas reçu un esprit d'esclavage<!--Voir Ga. 2:4 ; 2 Co. 11:20.--> pour être encore dans la crainte, mais vous avez reçu l'Esprit d'adoption par lequel nous crions : Abba ! Père !
8:16	L'Esprit lui-même rend témoignage à notre esprit, que nous sommes enfants d'Elohîm.
8:17	Or, si nous sommes enfants, nous sommes aussi héritiers : héritiers d'Elohîm en effet et cohéritiers du Mashiah, si nous souffrons avec lui, afin que nous soyons aussi glorifiés avec lui.
8:18	Car j'estime que les souffrances du temps présent ne sont pas dignes d'être comparées à la gloire à venir<!--Mt. 13:43 ; Ph. 3:21 ; Col. 3:4 ; Ap. 22:5.--> qui est sur le point d'être révélée pour nous.
8:19	Car la création attend<!--Voir Ro. 8:23-25 ; 1 Co. 1:7 ; Ga. 5:5 ; Ph. 3:20 ; Hé. 9:28.--> assidûment et patiemment, avec une ferme attente la révélation des fils d'Elohîm.
8:20	Car la création a été soumise à la perversité, non pas volontairement, mais à cause de celui qui l'y a soumise, avec l'espérance
8:21	qu'elle aussi sera rendue libre de l'esclavage de la corruption, pour avoir part à la liberté de la gloire des enfants d'Elohîm.
8:22	Car nous savons que, jusqu'à ce jour, toute la création soupire et souffre les douleurs de l'enfantement.
8:23	Et non seulement elle, mais nous aussi qui avons l'offrande du premier fruit<!--Prémices. Voir 1 Co. 15:20-23. Le Mashiah est le premier fruit.--> de l'Esprit, nous aussi, nous soupirons en nous-mêmes en attendant assidûment et patiemment l'adoption, la rédemption de notre corps<!--1 Co. 15:35-43,51-54.-->.
8:24	Car c'est en espérance que nous sommes sauvés. Or, l'espérance qu'on voit n'est plus espérance, car ce que l’on voit, pourquoi l’espérer encore ?
8:25	Mais si nous espérons ce que nous ne voyons pas, nous l'attendons assidûment et patiemment, avec persévérance.
8:26	Et de même aussi l’Esprit vient en aide à nos faiblesses, car nous ne savons pas prier comme il faut. Mais l'Esprit lui-même intercède en notre faveur par des soupirs inexprimables.
8:27	Mais celui qui sonde les cœurs connaît quelle est la pensée de l'Esprit, parce que c'est selon Elohîm qu'il intercède en faveur des saints. 
8:28	Mais nous savons aussi que toutes choses concourent au bien de ceux qui aiment Elohîm, de ceux qui sont appelés selon son dessein.
8:29	Parce que ceux qu'il a connus d'avance, il les a aussi prédestinés à être conformes à l'image de son Fils, afin qu'il soit le premier-né de beaucoup de frères.
8:30	Et ceux qu'il a prédestinés, il les a aussi appelés. Et ceux qu'il a appelés, il les a aussi justifiés, et ceux qu'il a justifiés, il les a aussi glorifiés.
8:31	Que dirons-nous donc à ces choses ? Si l'Elohîm est en notre faveur, qui sera contre nous ?
8:32	Lui qui n’a même pas épargné son propre Fils, mais qui l'a livré en faveur de nous tous, comment ne nous donnera-t-il pas aussi gracieusement toutes choses avec lui ?
8:33	Qui s'avancera en accusateur contre les élus d'Elohîm ? Elohîm est celui qui justifie !
8:34	Qui les condamnera ? Mashiah est mort, mais bien plus, il a été aussi réveillé, il est à la droite d'Elohîm et il intercède même en notre faveur !
8:35	Qui nous séparera de l'amour du Mashiah ? La tribulation, ou l'affreuse calamité, ou la persécution, ou la famine, ou la nudité, ou le péril, ou l'épée ?
8:36	Selon qu'il est écrit : À cause de toi, nous sommes mis à mort tout le jour, nous avons été estimés comme des brebis destinées à la boucherie<!--Ps. 44:23.-->.
8:37	Mais dans toutes ces choses, nous sommes plus que vainqueurs par le moyen de celui qui nous a aimés.
8:38	Car je suis persuadé que ni la mort, ni la vie, ni les anges, ni les principautés, ni les puissances, ni les choses présentes, ni les choses qui sont imminentes,
8:39	ni la hauteur, ni la profondeur, ni aucune autre créature ne pourra nous séparer de l'amour d'Elohîm dans le Mashiah Yéhoshoua notre Seigneur.

## Chapitre 9

9:1	Je dis la vérité en Mashiah, je ne mens pas, ma conscience me rendant témoignage par le Saint-Esprit,
9:2	qu'il y a une grande douleur en moi et une peine continuelle en mon cœur.
9:3	Car moi-même je souhaiterais être anathème et séparé du Mashiah en faveur de mes frères, mes parents selon la chair,
9:4	qui sont israélites, auxquels sont l'adoption, et la gloire, et les alliances, et la législation, et le service sacré et les promesses,
9:5	auxquels sont les pères, et desquels est issu selon la chair le Mashiah, lui qui est au-dessus de toutes choses Elohîm béni pour les âges. Amen !
9:6	Or ce n'est pas que la parole d'Elohîm ait échoué, car tous ceux qui sont issus d'Israël ne sont pas Israël,
9:7	ni que tous les enfants soient postérité d'Abraham. Mais en Yitzhak une postérité te sera appelée<!--Ge. 21:12.-->.
9:8	C'est-à-dire, que ce ne sont pas ceux qui sont enfants de la chair qui sont enfants d'Elohîm, mais que ce sont les enfants de la promesse qui sont comptés pour postérité.
9:9	Car voici, la parole de la promesse : En ce temps, je viendrai et Sarah aura un fils<!--Ge. 18:10.-->.
9:10	Et non seulement cela, mais il y a aussi Ribqah<!--Rébecca.-->. C’est avec un seul, Yitzhak, notre père, qu’elle a eu une relation sexuelle.
9:11	Car ils n’étaient pas encore nés et n’avaient rien pratiqué de bon ou de mauvais, afin que le dessein arrêté selon l'élection d'Elohîm demeure, non à partir des œuvres, mais à partir de celui qui appelle,
9:12	il lui fut dit : L'aîné sera l'esclave du plus petit<!--Ge. 25:23.-->, 
9:13	ainsi qu'il est écrit : J'ai aimé Yaacov et j'ai haï Ésav<!--Mal. 1:2-3.-->.
9:14	Que dirons-nous donc ? Y a-t-il de l'injustice en Elohîm ? Que cela n'arrive jamais !
9:15	Car il dit à Moshé : Je ferai miséricorde à qui je fais miséricorde, et j'aurai compassion de qui j'ai compassion<!--Ex. 33:19.-->.
9:16	Ainsi donc, ce n’est ni de celui qui veut, ni de celui qui court, mais d'Elohîm qui fait miséricorde.
9:17	Car l'Écriture dit à pharaon : C'est pour cela même que je t'ai suscité, afin de démontrer en toi ma puissance, et afin que mon nom soit publié par toute la Terre<!--Ex. 9:16.-->.
9:18	Ainsi donc, il fait miséricorde à qui il veut, et il endurcit qui il veut.
9:19	Tu me diras donc : De quoi se plaint-il encore ? Car qui a résisté à sa volonté ?
9:20	Mais toi, ô humain, qui es-tu pour contester avec Elohîm ? Le vase de terre dira-t-il à celui qui l'a modelé : Pourquoi m'as-tu fait ainsi ?
9:21	Le potier<!--Job 10:8-9.--> n'a-t-il pas autorité sur l'argile, pour faire en effet de la même masse un vase pour l'honneur et un autre pour le déshonneur ?
9:22	Mais, si en voulant montrer sa colère et faire connaître ce qu'il peut, Elohîm a supporté avec beaucoup de patience les vases de colère équipés pour la destruction,
9:23	et ceci pour faire connaître la richesse de sa gloire sur les vases de miséricorde qu'il a préparés d'avance pour la gloire,
9:24	nous aussi qu’il a appelés, non seulement hors des Juifs, mais aussi hors des nations.
9:25	Comme il dit aussi dans Hoshea<!--Généralement traduit par « Hosée » ou « Osée ».--> : Celui qu’on appelait « Pas-mon-peuple », je l’appellerai « Mon-peuple », celle qu’on appelait « Pas-Aimée », je l’appellerai « Aimée ».
9:26	Et il arrivera que, dans le lieu même où il leur avait été dit : Vous n'êtes pas mon peuple ! Là ils seront appelés les fils de l'Elohîm vivant.
9:27	Mais Yesha`yah<!--Ésaïe.--> s'écrie en faveur d'Israël : Si le nombre des fils d'Israël est comme le sable de la mer<!--Os. 2:1.-->, le reste<!--Es. 10:21-23.--> sera sauvé.
9:28	Car il accomplit et exécute rapidement la parole avec justice. Parce qu'il fera exécuter rapidement la parole sur la Terre.
9:29	Et comme Yesha`yah avait dit auparavant : Si le Seigneur Tsevaot ne nous avait laissé une postérité, nous serions devenus comme Sodome et nous aurions été semblables à Gomorrhe<!--Es. 1:9.-->.
9:30	Que dirons-nous donc ? Que les nations qui ne couraient pas après la justice ont obtenu la justice, mais la justice qui vient de la foi,
9:31	mais Israël, courant après la torah de la justice, n'est pas parvenu à cette torah.
9:32	En raison de quoi ? Parce que ce n'était pas à partir de la foi, mais comme à partir des œuvres de la torah. Car ils se sont heurtés contre la pierre d'achoppement<!--Es. 8:13-15.-->,
9:33	selon qu'il est écrit : Voici, je mets en Sion la pierre d'achoppement et le rocher de scandale, et quiconque croit en lui, ne rougira pas de honte<!--Es. 28:16.-->.

## Chapitre 10

10:1	Frères, le désir de mon cœur et ma supplication à Elohîm en faveur d'Israël est vraiment en vue du salut.
10:2	Car je leur rends témoignage qu'ils ont du zèle pour Elohîm, mais non pas selon la connaissance précise et correcte.
10:3	Car, ne connaissant pas la justice d'Elohîm et cherchant à établir leur propre justice, ils ne se sont pas soumis à la justice d'Elohîm.
10:4	Car Mashiah est la fin<!--Le mot « fin » vient du grec « telos » qui signifie « la limite à laquelle cesse une chose » ou encore « le but ». En effet, les lois cérémonielles avaient pour but d'amener les Israélites au sacrifice du Mashiah (Mt. 24:14 ; 1 Co. 10:11 ; 1 Ti. 1:5).--> de la torah<!--Il est question de la loi cérémonielle relative au culte mosaïque. Avant sa mort, Yéhoshoua, qui était né sous la torah (Ga. 4:4), demandait aux gens de l'appliquer. Ainsi, il demanda au lépreux qu'il avait guéri de présenter une offrande pour sa purification au temple (Mt. 8:1-4) et à ses disciples d'observer l'enseignement des scribes (Mt. 23:1-2). En effet, il fallait que les lois cérémonielles soient respectées jusqu'à sa mort. Quand Yéhoshoua a dit : « C'est accompli » (Jn. 19:30), toutes ces lois n'avaient plus aucune raison d'être (Col. 2:14-17 ; Hé. 7:11-22, 10:1-2).--> pour la justice de tout croyant.
10:5	Car Moshé écrit à propos de la justice qui vient de la torah : L'être humain qui aura pratiqué ces choses vivra par elles<!--Lé. 18:5.-->.
10:6	Mais ainsi parle la justice qui vient de la foi : Ne dis pas en ton cœur : Qui montera au ciel ? Cela, c’est amener vers le bas Mashiah<!--De. 30:11-14.-->.
10:7	Ou : Qui descendra dans l'abîme ? Cela, c’est emmener ailleurs Mashiah, hors des morts<!--Décédé, parti, celui dont l'âme est dans le Hadès.-->.
10:8	Mais que dit-elle ? La parole est près de toi, dans ta bouche et dans ton cœur. Cela, c’est la parole de foi que nous prêchons.
10:9	Parce que si tu confesses<!--Jn. 9:22, 12:42.--> de ta bouche le Seigneur Yéhoshoua, et si tu crois dans ton cœur qu'Elohîm l'a réveillé des morts, tu seras sauvé.
10:10	Car c'est du cœur que l'on croit à la justice, et c'est de la bouche que l'on fait profession pour le salut, 
10:11	car l'Écriture dit : Quiconque croit en lui ne rougira pas de honte<!--Es. 49:23.-->.
10:12	Car il n'y a pas de différence entre le Juif et le Grec, car le même Seigneur de tous est riche pour tous ceux qui l'invoquent<!--Voir 1 Pi. 1:17.-->.
10:13	Car quiconque invoquera le Nom du Seigneur sera sauvé<!--Paulos (Paul) se réfère ici à Joë. 3:5 : « Et il arrivera que quiconque invoquera le Nom de YHWH sera sauvé ». Comme à d'autres endroits, le nom propre YHWH a été remplacé par le générique « Seigneur ». Voir commentaire en Lu. 4:18-19.-->.
10:14	Mais comment invoqueront-ils celui en qui ils n'ont pas cru ? Et comment croiront-ils en celui dont ils n'ont pas entendu parler ? Et comment en entendront-ils parler, sans quelqu'un qui prêche ?
10:15	Mais comment prêchera-t-on, si l’on n’est pas envoyé ? Selon qu'il est écrit : Qu'ils sont beaux les pieds de ceux qui annoncent l’Évangile de la paix, de ceux qui annoncent l’Évangile des bonnes choses<!--Es. 52:7.--> !
10:16	Mais tous n'ont pas obéi à l'Évangile. Car Yesha`yah dit : Seigneur, qui est-ce qui a cru à ce qu’il a entendu de nous<!--Es. 53:1.--> ?
10:17	Ainsi la foi vient de ce qu'on entend, et l'on entend au moyen de la parole d'Elohîm.
10:18	Mais je dis : Ne l'ont-ils pas entendue ? Au contraire ! Leur voix est allée par toute la Terre, et leur parole jusqu'aux extrémités de la terre habitée<!--Ps. 19:5.-->.
10:19	Mais je dis : Israël ne l'a-t-il pas su ? Moshé le premier dit : Je vous exciterai à la jalousie par une non-nation, je provoquerai votre colère par une nation sans intelligence<!--De. 32:21.-->.
10:20	Et Yesha`yah assume avec hardiesse et dit : J'ai été trouvé par ceux qui ne me cherchaient pas, et je me suis clairement manifesté à ceux qui ne me demandaient pas<!--Es. 65:1.-->.
10:21	Mais à Israël il dit : Tout le jour j'ai tendu mes mains vers un peuple rebelle et contredisant<!--Es. 65:2.-->.

## Chapitre 11

11:1	Je dis donc : Elohîm a-t-il rejeté son peuple ? Que cela n'arrive jamais ! Car je suis aussi Israélite, de la postérité d'Abraham, de la tribu de Benyamin.
11:2	Elohîm n'a pas rejeté son peuple, qu'il a connu d'avance. Ou bien ne savez-vous pas ce que l'Écriture dit par Éliyah, comment il prie Elohîm contre Israël en disant :
11:3	Seigneur, ils ont tué tes prophètes et ils ont démoli tes autels, et je suis resté moi seul, et ils cherchent mon âme<!--1 R. 19:10.--> ?
11:4	Mais que lui dit la réponse divine ? Je me suis réservé 7 000 hommes qui n'ont pas fléchi le genou devant Baal<!--1 R. 19:18.-->.
11:5	Ainsi donc, il y a aussi dans le temps présent, un reste, selon l'élection de la grâce.
11:6	Or si c'est par la grâce, ce n'est plus à partir des œuvres, autrement la grâce n'est plus la grâce. Mais si c'est sur la base des œuvres, ce n'est plus par la grâce, autrement l'œuvre n'est plus une œuvre.
11:7	Quoi donc ? Ce qu'Israël cherche, il ne l'a pas obtenu, mais les élus l'ont obtenu, tandis que les autres ont été endurcis.
11:8	Ainsi qu'il est écrit : Elohîm leur a donné un esprit d'assoupissement, des yeux pour ne pas voir et des oreilles pour ne pas entendre<!--Es. 29:10.-->, jusqu'à ce jour. 
11:9	Et David dit : Que leur table devienne pour eux un filet et un piège, et une occasion de chute, et cela pour leur récompense !
11:10	Que leurs yeux soient obscurcis pour ne pas voir<!--Ps. 69:23-24.-->, et courbe continuellement leur dos !
11:11	Je dis donc : Ont-ils trébuché afin de tomber ? Que cela n’arrive jamais ! Mais, par leur chute<!--Le mot grec signifie aussi « tomber à côté de », « une faute » ou « une déviation par rapport à la vérité et la droiture », « un méfait ». Voir Mt. 6:14 ; Ro. 11:12.-->, il y a le salut pour les nations, pour les exciter à la jalousie.
11:12	Or, si leur chute est la richesse du monde et leur amoindrissement la richesse des nations, combien plus leur plénitude !
11:13	Car je vous le dis, à vous, les nations : pour autant que je suis, moi, en effet apôtre des nations, je glorifie mon service,
11:14	si par n'importe quel moyen je puis exciter à la jalousie ma chair et sauver quelques-uns d’entre eux.
11:15	Car si leur rejet a été la réconciliation du monde, quelle sera leur réception, sinon une vie d'entre les morts ?
11:16	Or si l'offrande du premier fruit<!--Voir 1 Co. 15:20-23.--> est sainte, la masse l'est aussi. Et si la racine est sainte, les branches le sont aussi.
11:17	Mais si quelques-unes des branches ont été retranchées, et si toi, qui étais un olivier sauvage, tu as été greffé parmi elles<!--Voir Ro. 11:24.--> et tu es devenu participant de la racine et de la sève de l'olivier,
11:18	ne te glorifie pas contre ces branches. Mais si tu te glorifies, ce n'est pas toi qui portes la racine, mais c'est la racine qui te porte.
11:19	Tu diras alors : Les branches ont été retranchées pour que moi, je sois greffé.
11:20	C'est vrai. Elles ont été retranchées à cause de leur incrédulité, et tu es debout par la foi. Ne t'élève donc pas par orgueil, mais crains.
11:21	Car si Elohîm n'a pas épargné les branches naturelles, il ne t'épargnera pas non plus.
11:22	Considère donc la bénignité et la sévérité d'Elohîm. En effet, la sévérité envers ceux qui sont tombés et la bénignité envers toi, si tu persévères dans cette bénignité ; autrement, toi aussi tu seras coupé.
11:23	Mais eux aussi, s'ils ne demeurent pas dans l'incrédulité, ils seront greffés, car Elohîm est puissant pour les greffer de nouveau.
11:24	Car si toi, tu as été coupé de l'olivier sauvage selon sa nature et, greffé contre nature sur l'olivier cultivé, combien plus eux seront-ils greffés selon leur nature sur leur propre olivier.
11:25	Car je ne veux pas, frères, que vous ignoriez ce mystère, afin que vous ne soyez pas sages à vos propres yeux : c’est qu’un endurcissement est arrivé en partie à Israël jusqu’à ce que la plénitude des nations soit entrée.
11:26	Et ainsi tout Israël sera sauvé, selon qu'il est écrit : Le Libérateur viendra de Sion et il détournera de Yaacov les impiétés.
11:27	Et c’est là l’alliance de ma part avec eux, lorsque j'ôterai leurs péchés<!--Es. 59:20-21.-->.
11:28	En effet, ils sont ennemis selon l'Évangile à cause de vous, mais selon l'élection, ils sont aimés à cause de leurs pères.
11:29	Car les dons de grâce et l'appel d'Elohîm sont sans regrets.
11:30	Car comme vous avez été vous-mêmes autrefois rebelles à Elohîm, et que maintenant vous avez obtenu miséricorde à cause de leur obstination,
11:31	de même ils sont maintenant devenus rebelles, afin qu'ils obtiennent aussi miséricorde, par la miséricorde qui vous a été faite.
11:32	Car Elohîm les a tous enfermés dans l'obstination, afin de faire miséricorde à tous.
11:33	Ô profondeur de la richesse, et de la sagesse et de la connaissance d'Elohîm ! Que ses jugements sont insondables et ses voies incompréhensibles !
11:34	Car qui a connu la pensée du Seigneur, ou qui a été son conseiller ?
11:35	Ou : Qui lui a donné le premier, et il lui sera rendu<!--Job 41:3.--> ?
11:36	Parce que c’est de lui et par lui et pour lui que sont toutes choses. À lui soit la gloire pour les âges ! Amen !

## Chapitre 12

12:1	Je vous exhorte donc, frères, par les compassions d'Elohîm, à présenter vos corps en sacrifice vivant, saint, agréable à Elohîm : c'est le service sacré spirituel, le vôtre.
12:2	Et ne vous conformez<!--Se conformer (c'est-à-dire son esprit et son caractère) au modèle d'un autre, (se façonner selon).--> pas à cet âge-ci, mais soyez transformés<!--Le verbe « transformer » est la traduction du terme grec « metamorphoo » qui a donné en français « transfigurer ». C'est le même terme qui a été utilisé en Mt. 17:2 pour parler de la transfiguration du Seigneur. Si Paulos (Paul) recommandait cela à des personnes déjà converties, c'est parce qu'Elohîm les appelait à aller plus loin. La transformation d'une chenille en papillon est un très bel exemple pour illustrer le changement qui doit s'opérer en nous. Pour atteindre ce stade, cet insecte passe par plusieurs étapes. La transformation nous permet de croître spirituellement. En effet, tout enfant d'Elohîm est appelé à devenir mature, à passer du stade de petit enfant à celui de jeune homme, et de celui de jeune homme à celui de père (1 Jn. 2:12-14).--> par le renouvellement<!--Le mot grec « anakainosis » traduit par renouvellement signifie aussi « renouveau », « rénovation » ou « changement complet vers le meilleur ». Voir Tit. 3:5.--> de votre pensée, afin que vous éprouviez quelle est la volonté d'Elohîm, ce qui est bon, agréable et parfait.
12:3	Car à travers la grâce qui m'a été donnée, je dis à chacun de vous qu'il ne faut pas penser plus de bien de soi qu'il n'est convenable de penser, mais de penser à se maîtriser soi-même, selon la mesure de foi qu'Elohîm a départie<!--« Diviser », « séparer en parts », « couper en morceaux », « être fendu en fractions ».--> à chacun.
12:4	Car, comme nous avons beaucoup de membres dans un seul corps, et que tous les membres n'ont pas la même fonction,
12:5	ainsi nous, qui sommes nombreux, sommes un seul corps en Mashiah mais, chacun individuellement, membres<!--Voir 1 Co. 12.--> les uns des autres.
12:6	Mais nous avons des dons de grâce différents selon la grâce qui nous a été donnée. Si c'est la prophétie, prophétisons selon la proportion de la foi.
12:7	Soit c'est le service, en servant. Soit celui qui enseigne, dans l'enseignement.
12:8	Soit celui qui exhorte, dans l'exhortation. Que celui qui donne le fasse dans la simplicité, celui qui dirige, avec zèle, celui qui exerce la miséricorde, avec joie.
12:9	Que l'amour soit sincère. Ayez en horreur le mal, vous tenant collés à ce qui est bon.
12:10	Quant à l'amour fraternel, ayez de la tendresse<!--Vient du grec « philostorgos » qui signifie « l'amour mutuel des parents et enfants, des maris et épouses », « amour affectueux, promptitude à aimer, aimer tendrement », « se dit surtout de la tendresse réciproque des parents et enfants ».--> les uns pour les autres. Quant à l'honneur, soyez les premiers à le rendre aux autres.
12:11	N'étant pas paresseux, mais empressés ; bouillants de chaleur de l'esprit, étant les esclaves<!--Être un esclave, servir, faire le service.--> du Seigneur.
12:12	Vous réjouissant dans l'espérance, patients dans la tribulation, persévérants dans la prière.
12:13	Prenant part aux besoins des saints, poursuivant l'hospitalité.
12:14	Bénissez ceux qui vous persécutent, bénissez et ne maudissez pas.
12:15	Réjouissez-vous avec ceux qui se réjouissent et pleurez avec ceux qui pleurent.
12:16	Ayant une même pensée les uns envers les autres, ne pensant pas à ce qui est élevé, mais vous laissant entrainer par les choses humbles. Ne soyez pas sages à votre propre jugement.
12:17	Ne rendant à personne le mal pour le mal, recherchant les choses honnêtes devant tous les humains.
12:18	S'il est possible, autant que cela dépend de vous, soyez en paix avec tous les humains.
12:19	Ne vous vengeant pas vous-mêmes, mes bien-aimés, mais donnez lieu à la colère, car il est écrit : À moi la vengeance ! Moi, je rendrai, dit le Seigneur<!--De. 32:35.-->.
12:20	Si donc ton ennemi a faim, donne-lui à manger, s'il a soif, donne-lui à boire, car en faisant cela, tu amasseras<!--Cette expression peut également être traduite par « ce sont des braises que tu enlèves de sa tête ». Cette expression est une métaphore qui tire son origine de la méthode consistant à fondre les métaux dans les anciens fours. Ces métaux étaient introduits dans un four ; ensuite, on mettait du charbon au-dessous et une couche épaisse au-dessus de leur partie supérieure. La chaleur étant augmentée, le métal fondait et se séparait des impuretés qu’il contenait. L’amas de charbons à sa partie supérieure amollissait et purifiait le minerai. Ainsi, témoigner de l’amour envers un ennemi peut l’aider à se séparer des impuretés. Voir Pr. 25:21-22.--> des charbons de feu sur sa tête.
12:21	Ne sois pas vaincu par le mal, mais vainqueur<!--Voir Jn. 16:33 ; 1 Jn. 5:4-5, 2:13-14, 4:4, 5:4-5 ; Ap. 2:7, 2:11, 2:17, 2:26, 3:5, 3:12, 3:21, 5:5, 12:11, 15:2, 17:14, 21:7.--> du mal par le bien.

## Chapitre 13

13:1	Que toute âme se soumette aux autorités qui sont au-dessus d'elle, car il n'y a pas d'autorité qui ne provienne d'Elohîm<!--Voir Jn. 19:10-11. Il existe aussi l'autorité démoniaque. Voir Lu. 22:53 ; Ac. 26:18 ; Col. 1:13 et Ap. 13:2-4.--> et les autorités qui existent ont été instituées par<!--Vient du grec « hupo » qui signifie aussi « au-dessous de ».--> Elohîm.
13:2	C'est pourquoi celui qui résiste à l'autorité s'oppose à l'ordre d'Elohîm. Et ceux qui s'y opposent attireront la condamnation sur eux-mêmes.
13:3	Car les magistrats ne sont pas une terreur pour les bonnes œuvres, mais pour les mauvaises. Or veux-tu ne pas craindre l'autorité ? Fais ce qui est bon, et tu recevras d'elle de la louange.
13:4	Car elle est ministre d'Elohîm pour ton bien. Mais si tu fais le mal, crains. Car ce n'est pas en vain qu'elle porte l'épée, car elle est ministre d'Elohîm, vengeresse pour la colère contre celui qui pratique ce qui est mauvais.
13:5	C'est pourquoi il faut être soumis, non seulement à cause de la colère, mais aussi à cause de la conscience.
13:6	Car c'est aussi pour cela que vous payez les impôts<!--Le tribut, spécialement la taxe levée sur les maisons, les terres et les personnes. Voir Mt. 17:24-25, 22:17-19 ; Mc. 12:14-17 ; Lu. 20:21-25, 23:2.-->, car ils sont ministres publics d'Elohîm, ceux qui s’appliquent constamment à cela.
13:7	Rendez donc à tous ce qui leur est dû : à qui l'impôt, l'impôt, à qui le tribut, le tribut, à qui la crainte, la crainte, à qui l'honneur, l'honneur.
13:8	Ne devez rien à personne, excepté de vous aimer les uns les autres. Car celui qui aime les autres a accompli la torah.
13:9	En effet : Tu ne commettras pas d'adultère, tu n'assassineras pas, tu ne voleras pas, tu ne diras pas de faux témoignage, tu ne convoiteras pas, (et s’il y a quelque autre commandement), se résume dans cette parole-ci : Tu aimeras ton prochain comme toi-même<!--Ex. 20:12-17 ; Lé. 19:18 ; Mt. 22:39 ; Mc. 12:31 ; Ga. 5:14.-->.
13:10	L'amour ne fait pas de mal au prochain. L'amour est donc la plénitude de la torah.
13:11	Même vu le temps, parce qu’il est déjà l'heure de nous réveiller du sommeil. Car maintenant le salut est plus près de nous que lorsque nous avons cru.
13:12	La nuit est avancée<!--Mt. 25:1-13.--> et le jour approche. Alors débarrassons-nous<!--Voir commentaire en Hé. 12:1.--> des œuvres de ténèbre et soyons revêtus des armes de lumière.
13:13	Marchons d'une manière bienséante, comme en plein jour, non dans les orgies<!--Orgies : du grec « komos ». Ce terme désigne la procession nocturne et rituelle, qui avait lieu après un souper, de gens à moitié ivres, à l'esprit folâtre, qui défilaient à travers les rues avec torches et musique en l'honneur de Bacchus ou quelque autre divinité, et chantaient et jouaient devant les maisons de leurs amis, hommes ou femmes. Ce mot est aussi utilisé pour les fêtes et beuveries de nuit qui se terminaient en orgies.--> et les ivrogneries, non dans le concubinage et la luxure sans bride<!--Voir commentaire en Mc. 7:22.-->, non dans la querelle et la jalousie.
13:14	Mais, soyez revêtus du Seigneur Yéhoshoua Mashiah et ne prenez pas soin de la chair pour accomplir ses désirs<!--Ga. 5:16-19.-->.

## Chapitre 14

14:1	Mais celui qui est faible dans la foi, recevez-le, non pas pour des jugements et des délibérations.
14:2	En effet, tel a la foi pour manger de toutes choses, mais le faible ne mange que des légumes.
14:3	Que celui qui mange ne méprise pas celui qui ne mange pas, et que celui qui ne mange pas, ne juge pas celui qui mange, car Elohîm l'a accueilli.
14:4	Qui es-tu, toi qui juges le domestique d’autrui ? C’est pour son propre Seigneur qu’il se tient ferme ou qu’il tombe. Mais il tiendra, car Elohîm a le pouvoir de le faire tenir.
14:5	Tel juge, en effet, un jour au-dessus d'un autre jour, mais tel autre juge que tous les jours sont égaux. Que chacun soit pleinement persuadé dans sa propre pensée.
14:6	Celui qui pense au jour, c'est pour le Seigneur qu'il y pense. Et celui qui ne pense pas au jour, c'est pour le Seigneur qu'il n'y pense pas. Celui qui mange, mange à cause du Seigneur, et il rend grâce à Elohîm. Et celui qui ne mange pas, ne mange pas aussi à cause du Seigneur, et il rend grâce à Elohîm.
14:7	Car aucun de nous ne vit pour lui-même, et aucun ne meurt pour lui-même.
14:8	Car si nous vivons, nous vivons pour le Seigneur, et si nous mourons, nous mourons pour le Seigneur. Soit donc que nous vivions, soit que nous mourions, nous sommes au Seigneur.
14:9	Car c'est pour cela que Mashiah est mort, qu'il s'est relevé, et qu'il a repris vie, afin de dominer sur les morts et sur les vivants.
14:10	Mais toi, pourquoi juges-tu ton frère ? Ou toi aussi, pourquoi méprises-tu ton frère ? Car nous nous présenterons tous au tribunal<!--Siège officiel d'un juge ou trône du jugement. 2 Co. 5:10.--> du Mashiah.
14:11	Car il est écrit : Moi, je vis<!--Ap. 1:18.-->, dit le Seigneur, tout genou fléchira devant moi et toute langue célébrera Elohîm<!--Es. 45:23 ; Ph. 2:10-11.-->.
14:12	Ainsi donc, chacun de nous rendra compte à Elohîm pour lui-même.
14:13	Ne nous jugeons donc plus les uns les autres, mais jugez plutôt ceci : de ne pas mettre devant un frère une pierre d'achoppement ou une occasion de chute<!--Ou scandale.-->.
14:14	Je sais et je suis persuadé par le Seigneur Yéhoshoua, que rien n'est souillé par soi-même, mais cependant si quelqu'un croit qu'une chose est souillée, elle est souillée pour lui.
14:15	Mais si ton frère est attristé à cause d'un aliment, tu ne marches plus selon l'amour. Ne détruis pas par ton aliment celui en faveur duquel Mashiah est mort.
14:16	Que ce qui est bon pour vous ne soit donc pas calomnié.
14:17	Car le Royaume d'Elohîm n'est ni aliment ni boisson, mais justice et paix et joie par l'Esprit Saint.
14:18	Car celui qui sert le Mashiah de cette manière est agréable à Elohîm et approuvé des humains.
14:19	Ainsi donc, poursuivons les choses qui tendent à la paix et à la construction mutuelle.
14:20	Ne détruis pas l'œuvre d'Elohîm pour un aliment. En effet, toutes choses sont pures, mais il est mauvais pour un homme de manger quelque chose en devenant une cause d'achoppement.
14:21	Il est bon de ne pas manger de viande, de ne pas boire de vin, pour ne pas que ton frère trébuche, ou se scandalise ou soit faible.
14:22	As-tu la foi ? Aie-la en toi-même devant Elohîm. Béni est celui qui ne se juge pas lui-même dans ce qu'il approuve !
14:23	Mais celui qui doute est condamné s'il mange, parce que cela ne vient pas de la foi. Or tout ce qui ne vient pas de la foi est péché.

## Chapitre 15

15:1	Mais nous devons, nous qui sommes forts, supporter les infirmités des faibles et ne pas nous plaire à nous-mêmes.
15:2	Que chacun de nous donc plaise au prochain pour ce qui est bon, en vue de la construction.
15:3	Car même le Mashiah n'a pas cherché à se satisfaire lui-même. Mais, selon qu'il est écrit : Les insultes de ceux qui t'insultent sont tombées sur moi<!--Ps. 69:10.-->.
15:4	Car toutes les choses qui ont été écrites auparavant, ont été écrites pour notre instruction, afin que, par le moyen de la persévérance et de la consolation des Écritures, nous ayons espérance.
15:5	Mais que l'Elohîm de persévérance et de consolation vous donne d'avoir une même pensée les uns envers les autres selon Mashiah Yéhoshoua,
15:6	afin que tous, d'un même cœur et d'une même bouche, vous glorifiiez l'Elohîm et Père de notre Seigneur Yéhoshoua Mashiah.
15:7	C'est pourquoi, accueillez-vous les uns les autres, comme aussi le Mashiah nous a accueillis, pour la gloire d'Elohîm.
15:8	Mais je dis que Yéhoshoua Mashiah a été serviteur de la circoncision en faveur de la vérité d'Elohîm, pour confirmer les promesses des pères,
15:9	et pour que les nations glorifient Elohîm en faveur de sa miséricorde, selon ce qui est écrit : À cause de cela, je te célébrerai parmi les nations et je chanterai ton Nom<!--Ps. 18:50.-->. 
15:10	Et il dit encore : Nations, réjouissez-vous avec son peuple<!--De. 32:43.--> !
15:11	Et encore : Louez le Seigneur, vous, toutes les nations, et célébrez-le, vous, tous les peuples<!--Ps. 117:1.--> ! 
15:12	Et Yesha`yah dit encore : Et ce sera la racine<!--Yéhoshoua ha Mashiah est la racine d'Isaï qui nous porte. Voir Es. 11:10 ; Ap. 5:5, 22:16.--> d'Isaï qui se lèvera pour être le chef<!--Voir Mc. 10:42. Les chefs de ce monde seront remplacés par Yéhoshoua (Jésus), le véritable chef.--> des nations. Les nations espéreront en elle<!--Es. 11:1,10.-->.
15:13	Mais que l'Elohîm de l'espérance vous remplisse de toute joie et de toute paix, dans la foi, afin que vous abondiez en espérance, par la puissance du Saint-Esprit !
15:14	Or, je suis moi-même aussi persuadé à votre sujet, mes frères, que vous êtes, vous aussi, pleins de bonté, remplis de toute connaissance, et que vous pouvez même vous avertir les uns les autres.
15:15	Mais je vous ai écrit en quelque sorte avec plus de hardiesse, frères, comme pour vous rappeler la grâce qui m'a été donnée par Elohîm,
15:16	pour être, moi, ministre de Yéhoshoua Mashiah parmi les nations, administrant selon la manière d'un prêtre l'Évangile d'Elohîm, afin que l'offrande des nations lui soit agréable, étant sanctifiée par le Saint-Esprit.
15:17	J’ai donc sujet de me glorifier en Mashiah Yéhoshoua, dans les choses qui concernent Elohîm.
15:18	Car je n’oserai rien dire que Mashiah n’ait accompli par mon moyen pour l'obéissance des nations, en parole et en œuvre,
15:19	par la puissance de signes et de prodiges, par la puissance de l'Esprit d'Elohîm, de sorte que depuis Yeroushalaim et de tous côtés, jusqu'en Illyrie, j'ai tout rempli de l'Évangile du Mashiah.
15:20	Mais je tâche sincèrement<!--Voir 2 Co. 5:9.--> de prêcher ainsi l'Évangile là où Mashiah n'avait pas encore été nommé, afin de ne pas bâtir sur le fondement étranger<!--« Appartenant à d'autres », « pas de notre famille », « un ennemi ».-->.
15:21	Mais selon qu'il est écrit : Ceux à qui il n'a pas été annoncé, le verront, et ceux qui n'en avaient pas entendu parler, l'entendront<!--Es. 52:15.-->.
15:22	Et c'est ce qui m'a souvent empêché d'aller vous voir.
15:23	Mais maintenant, comme je n'ai plus de place dans ces régions-ci, et que, depuis de nombreuses années, j'ai un grand désir d'aller vers vous,
15:24	si je me rends en Espagne, je viendrai chez vous, car j'espère vous voir en passant, et y être accompagné par vous, après que j'aurai d'abord satisfait en partie mon désir d'être avec vous.
15:25	Mais maintenant je vais à Yeroushalaim pour servir les saints.
15:26	Car la Macédoine et l’Achaïe ont décidé de faire une contribution pour les pauvres parmi les saints de Yeroushalaim.
15:27	Car elles l’ont décidé et elles sont leurs débitrices. Car si les nations ont eu part à leurs biens spirituels, elles doivent aussi leur faire part des choses charnelles.
15:28	Après donc que j'aurai achevé cela et que je leur aurai consigné<!--Ce mot vient du grec « sphragizo » et signifie aussi « confirmer l'authenticité », « mettre un sceau ». Il était question des offrandes qu'il devait remettre aux saints de la Judée.--> ce fruit, je m'en irai en Espagne par chez vous.
15:29	Et je sais qu'en allant chez vous, je viendrai dans la plénitude de bénédiction de l'Évangile du Mashiah.
15:30	Mais je vous exhorte, frères, par notre Seigneur Yéhoshoua Mashiah et par l'amour de l'Esprit, à combattre avec moi dans vos prières à Elohîm en ma faveur,
15:31	afin que je sois délivré des rebelles qui sont en Judée et que mon service<!--Service : du grec « diakonia », terme qui désigne le service ou le ministère de ceux qui répondent aux besoins des autres. Ce vocable fait aussi allusion à l'office des diacres.--> à l'égard de Yeroushalaim soit bien reçu par les saints.
15:32	Afin que, arrivé avec joie auprès de vous par la volonté d'Elohîm, je me repose avec vous.
15:33	Mais que l'Elohîm de paix soit avec vous tous ! Amen !

## Chapitre 16

16:1	Mais je vous recommande Phœbé notre sœur, qui est diaconesse de l'assemblée de Cenchrées,
16:2	afin que vous la receviez dans le Seigneur, d'une manière digne des saints, et que vous l'assistiez en toute affaire où elle aurait besoin de vous, car elle est devenue une gardienne<!--« Une femme responsable des autres », « une gardienne », « une protectrice », « une patronne », « prenant soin des affaires des autres et les aidant par ses ressources ».--> de beaucoup, et de moi-même.
16:3	Saluez Priscilla et Akulas, mes compagnons d'œuvre en Mashiah Yéhoshoua,
16:4	eux qui ont exposé leur cou en faveur de mon âme. Ce n'est pas moi seul qui leur rends grâce, mais aussi toutes les assemblées des nations,
16:5	et l'assemblée qui est dans leur maison. Saluez Épaïnetos, mon bien-aimé, qui a été pour Mashiah l'offrande du premier fruit d'Achaïe.
16:6	Saluez Myriam, qui a beaucoup travaillé pour nous.
16:7	Saluez Andronicos et Iounias, mes parents et mes compagnons de captivité, qui sont notables parmi les apôtres, qui même ont été en Mashiah avant moi.
16:8	Saluez Amplias, mon bien-aimé dans le Seigneur.
16:9	Saluez Urbanus, notre compagnon d'œuvre en Mashiah, et Stakys, mon bien-aimé.
16:10	Saluez Apellès, qui est éprouvé dans le Mashiah. Saluez ceux de chez Aristoboulos.
16:11	Saluez Hérodion, mon parent. Saluez ceux de chez Narkissos<!--Narcisse.--> qui sont dans le Seigneur.
16:12	Saluez Truphaina et Tryphosa, qui travaillent pour le Seigneur. Saluez Persis, la bien-aimée qui a beaucoup travaillé pour le Seigneur.
16:13	Saluez Rhouphos, l'élu du Seigneur, et sa mère, qui est aussi la mienne.
16:14	Saluez Asugkritos, Phlégon, Hermas, Patrobas, Hermès<!--Mercure.-->, et les frères qui sont avec eux.
16:15	Saluez Philologos et Iulia, Néreus et sa sœur, et Olympas et tous les saints qui sont avec eux.
16:16	Saluez-vous les uns les autres par un saint baiser. Les assemblées du Mashiah vous saluent.
16:17	Mais je vous exhorte, frères, à prendre garde à ceux qui causent des divisions et des scandales contre la doctrine que vous avez apprise, et éloignez-vous d'eux.
16:18	Car ces sortes de gens sont les esclaves, non pas de notre Seigneur Yéhoshoua Mashiah, mais de leur propre ventre. Et, par le moyen de douces paroles et de flatteries, ils trompent les cœurs des inoffensifs<!--« Sans artifice ou fraude », « inoffensif », « libre de culpabilité », « ne craignant pas le mal de la part des autres ».-->.
16:19	Car votre obéissance est venue aux oreilles de tous. Je me réjouis donc de vous, mais je désire que vous soyez vraiment prudents quant à ce qui est bon, et sans mélange<!--Voir Mt. 10:16.--> quant au mal.
16:20	Mais l'Elohîm de paix brisera bien vite<!--Vient du grec « tachos » qui signifie « vitesse », « rapidité », « vivacité » et « promptitude ».--> Satan sous vos pieds. Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous ! 
16:21	Timotheos, mon compagnon d'œuvre, vous salue, ainsi que Loukios, et Iason et Sosipatros, mes parents.
16:22	Moi Tertius, qui ai écrit cette lettre, je vous salue en notre Seigneur.
16:23	Gaïos, mon hôte et celui de toute l'assemblée, vous salue. Erastos, le gestionnaire de la ville, vous salue, et Quartus, notre frère.
16:24	Que la grâce de notre Seigneur Yéhoshoua Mashiah soit avec vous tous ! Amen !
16:25	Or, à celui qui est capable de vous affermir selon mon Évangile et la prédication de Yéhoshoua Mashiah, selon la révélation du mystère qui a été gardé dans le silence dans les temps éternels,
16:26	mais manifesté maintenant à travers les écritures prophétiques, selon le mandat<!--L'ordre. Voir 1 Ti. 1:1 ; Tit. 1:3.--> de l'Elohîm éternel qui l’a fait connaître ainsi à toutes les nations pour l’obéissance de la foi,
16:27	à Elohîm seul sage, soit la gloire pour les âges, par le moyen de Yéhoshoua Mashiah ! Amen !
