# Tsephanyah (Sophonie) (So.)

Signification : Yah a caché, protégé

Auteur : Tsephanyah (Sophonie)

Thème : Le jour de YHWH

Date de rédaction : 7ème siècle av. J.-C.

De lignée royale, Tsephanyah exerça son service dans le royaume de Yéhouda (Juda) au temps du roi Yoshiyah (Josias) et fut contemporain de Yirmeyah (Jérémie), Habaqqouq (Habakuk), Yehezkel (Ézéchiel) et Obadyah (Abdias). À une époque où l'iniquité s'était accrue au point où les quelques personnes fidèles à Elohîm étaient persécutées, Tsephanyah fut suscité par YHWH pour annoncer le jugement de Yéhouda, d'Israël et de quelques nations païennes.

## Chapitre 1

1:1	La parole de YHWH qui apparut à Tsephanyah, fils de Koushi<!--Leur noirceur.-->, fils de Gedalyah, fils d'Amaryah, fils d'Hizqiyah<!--Ézéchias.-->, du temps de Yoshiyah, fils d'Amon, roi de Yéhouda.
1:2	Je ferai disparaître, je ferai disparaître toutes choses sur les faces du sol, - déclaration de YHWH.
1:3	Je ferai disparaître les humains et les bêtes, je ferai disparaître les créatures volantes des cieux et les poissons de la mer, les pierres d'achoppement avec les méchants, et je retrancherai les humains sur les faces du sol, - déclaration de YHWH.
1:4	J'étendrai ma main sur Yéhouda et sur tous les habitants de Yeroushalaim. Je retrancherai de ce lieu le reste de Baal<!--Voir commentaire en Jg. 2:13.-->, les noms des prêtres idolâtres et les prêtres,
1:5	ceux qui se prosternent sur les toits devant l'armée des cieux, ceux qui se prosternent devant YHWH, qui jurent par lui, et qui jurent aussi par Malcom<!--Divinité nationale des Ammonites, dont le nom signifie « roi » ou « prince », et dont le culte idolâtre était une chose abominable aux yeux de YHWH. 1 R. 11:5-7 et 33 ; 2 R. 17:33, 23:11-13 ; Jé. 19:13.-->,
1:6	ceux qui se détournent de YHWH, ceux qui n'ont pas cherché YHWH, qui ne l'ont pas consulté.
1:7	Silence devant Adonaï YHWH ! Car le jour de YHWH est proche<!--Voir commentaire en Za. 14:1.-->, car YHWH a préparé le sacrifice, il a sanctifié ses conviés.
1:8	Il arrivera au jour du sacrifice de YHWH que je punirai les chefs, les fils du roi, et tous ceux qui portent des vêtements étrangers.
1:9	Je punirai, en ce jour-là, tous ceux qui sautent par-dessus le seuil, ceux qui remplissent de violence et de fraude la maison de leurs maîtres.
1:10	Il arrivera en ce jour-là - déclaration de YHWH - qu'on entendra des cris de détresse de la porte des poissons, des hurlements depuis la seconde et un grand fracas des collines.
1:11	Hurlez, habitants de Macthesh<!--Macthesh était un bas-quartier de Yeroushalaim où se trouvaient les marchés.--> ! Car tout le peuple de Kena'ân a été détruit, tous ces gens chargés d'argent sont éliminés.
1:12	Il arrivera en ce jour-là que je fouillerai Yeroushalaim avec des lampes, que je punirai les hommes qui sont figés sur leurs lies, et qui disent dans leurs cœurs : YHWH ne fera ni bien ni mal.
1:13	Leur richesse sera livrée au pillage, leurs maisons à la dévastation. Ils bâtiront des maisons, mais ne les habiteront pas ; ils planteront des vignes, mais n'en boiront pas le vin.
1:14	Le grand jour de YHWH<!--Voir commentaire en Za. 14:1.--> est proche, il est proche, il arrive très vite. Le jour de YHWH fait entendre sa voix, et l'homme vaillant<!--Jé. 30:7 ; Joë. 2:11 ; Am. 5:18.--> y poussera des cris amers.
1:15	Ce jour est un jour de fureur, un jour de détresse et d'angoisse, un jour de bruit éclatant et effrayant, un jour de ténèbre et d'obscurité, un jour de nuées et de profonde obscurité,
1:16	un jour de shofar et d'alarme de guerre contre les villes fortifiées, et contre les hautes tours.
1:17	Je mettrai les êtres humains dans la détresse et ils marcheront comme des aveugles, parce qu'ils ont péché contre YHWH. Leur sang sera répandu comme de la poussière et leurs intestins comme des ordures.
1:18	Ni leur argent ni leur or ne pourront les délivrer au jour de la fureur de YHWH. Toute la terre sera dévorée par le feu de sa jalousie, car il fera une entière destruction, une ruine soudaine de tous les habitants de la terre<!--Ez. 7:19 ; Pr. 11:4.-->.

## Chapitre 2

2:1	Rassemblez-vous, rassemblez-vous, nation sans désir<!--1 Th. 5:21 ; 2 Co. 13:5 ; Ep. 5:10.--> !
2:2	Avant que le décret enfante, que le jour passe comme la paille, avant que la chaleur des narines de YHWH ne vienne sur vous, avant que le jour de la colère de YHWH ne vienne sur vous !
2:3	Vous tous, les humbles de la terre, qui faites ce qu'il ordonne, cherchez YHWH ! Cherchez la justice, cherchez l'humilité ! Peut-être serez-vous protégés, le jour de la colère de YHWH<!--Am. 5:15.-->.
2:4	En effet, Gaza sera abandonnée, Askalon sera en désolation, Asdod sera chassée en plein midi, Ékron sera déracinée<!--Am. 1:6-8 ; Za. 9:5.-->.
2:5	Malheur aux habitants de la région maritime, à la nation des Kéréthiens ! La parole de YHWH est contre vous, Kena'ân, terre des Philistins : Je te détruirai, si bien que, personne n'y habitera.
2:6	La région maritime deviendra des pâturages, des demeures pour les bergers, et des bergeries pour les troupeaux.
2:7	Elle deviendra une région pour le reste de la maison de Yéhouda. Ils paîtront dans ces lieux-là, et le soir ils feront leur gîte dans les maisons d'Askalon, car YHWH, leur Elohîm, les visitera, et il ramènera leurs captifs.
2:8	J'ai entendu les insultes de Moab, et les outrages des fils d'Ammon, lorsqu'ils insultaient mon peuple et se glorifiaient de leur territoire<!--Ez. 25:3-6.-->.
2:9	C'est pourquoi, moi, le Vivant, - déclaration de YHWH Tsevaot, l'Elohîm d'Israël -, Moab deviendra comme Sodome, et les fils d'Ammon comme Gomorrhe, un lieu devenu la possession des orties, une carrière de sel et de désolation à jamais. Le reste de mon peuple les pillera et le résidu de ma nation les possédera.
2:10	Ceci leur arrivera en échange de leur orgueil, parce qu'ils ont insulté le peuple de YHWH Tsevaot<!--Es. 16:6 ; Jé. 48:29.--> et se sont glorifiés eux-mêmes contre lui.
2:11	YHWH sera redoutable contre eux, car il rendra maigres tous les elohîm de la terre. On se prosternera devant lui, chacun en son lieu, même toutes les îles des nations<!--Mal. 1:11 ; Jn. 4:21.-->.
2:12	Vous aussi, Éthiopiens, vous serez blessés mortellement par mon épée.
2:13	Il étendra sa main sur le nord, il détruira l'Assyrie, et il fera de Ninive une désolation, dans un lieu aride comme un désert.
2:14	Et les troupeaux feront leur gîte au milieu d'elle, et toutes les bêtes des nations, même le pélican et le hérisson, habiteront parmi les chapiteaux de ses colonnes. Une voix chantera à la fenêtre. La désolation sera au seuil, parce qu'il en aura abattu les cèdres<!--Es. 14:23, 34:11.-->.
2:15	C'est là cette ville remplie de joie, qui habitait en sécurité et qui disait en son cœur : Moi, et à part moi, nulle autre ! Comment est-elle devenue une horreur, un repaire de bêtes sauvages ? Quiconque passera près d'elle sifflera et secouera sa main.

## Chapitre 3

3:1	Malheur à la ville immonde et souillée et qui ne fait qu'opprimer !
3:2	Elle n'a pas écouté la voix, elle n'a pas accepté la correction, elle ne s'est pas confiée en YHWH, elle ne s'est pas approchée de son Elohîm.
3:3	Ses chefs au milieu d'elle sont des lions rugissants, et ses juges sont des loups du soir qui ne gardent pas les os pour les ronger le matin<!--Ez. 22:27 ; Pr. 28:15.-->.
3:4	Ses prophètes sont folâtres, des hommes infidèles, ses prêtres ont souillé les choses saintes, ils ont fait violence à la torah<!--Jé. 23:11-32.-->.
3:5	YHWH est juste au milieu d'elle, il ne commet pas d'injustice<!--De. 32:4.-->. Matin après matin il met en lumière son jugement, il n'y manque pas. Mais le pervers ne connaît pas la honte.
3:6	J'ai supprimé des nations, leurs tours d'angle sont ravagées. J'ai rendu désolées leurs rues : plus aucun passant. Leurs villes sont détruites : plus aucun homme, plus aucun habitant.
3:7	Je disais : Si seulement tu me craignais, si tu acceptais la correction ! - sa demeure ne serait pas retranchée. Quelle que soit la punition que je lui envoie. Mais ils se sont levés de bon matin, ils ont corrompu toutes leurs actions.
3:8	C'est pourquoi attendez-moi, - déclaration de YHWH -, le jour où je me lèverai pour le butin, car mon jugement est de rassembler<!--Mt. 25:32.--> les nations et de réunir les royaumes, pour répandre sur eux mon indignation, toute la chaleur de mes narines, car toute la Terre sera dévorée par le feu de ma jalousie.
3:9	Car alors je transformerai les langues<!--Il est question ici de la conversion des peuples issus des nations (Ap. 7:9-17).--> des nations en des langues pures, afin qu'elles invoquent toutes le Nom de YHWH, pour qu'elles le servent d'un commun accord.
3:10	De la région au-delà des fleuves de l'Éthiopie<!--Es. 18:1.-->, mes adorateurs, la fille de mes dispersés, m'apporteront des offrandes<!--Es. 19:21, 27:13 ; Ps. 68:31-32, 72:10-11.-->.
3:11	En ce jour-là, tu ne seras plus confuse à cause de toutes tes actions par lesquelles tu as péché contre moi, parce qu'alors j'aurai ôté du milieu de toi ceux qui se réjouissent de ton orgueil, et tu ne seras plus hautaine à cause de la montagne de ma sainteté.
3:12	Je laisserai au milieu de toi un peuple humble et faible qui cherchera son refuge dans le nom de YHWH.
3:13	Les restes d'Israël ne commettront plus d'injustice, ils ne proféreront plus de mensonge et il n'y aura plus dans leur bouche de langue trompeuse. Car ils paîtront et se reposeront et il n'y aura personne qui les effraye.
3:14	Pousse des cris de joie, fille de Sion ! Pousse des cris de triomphe, Israël ! Réjouis-toi et triomphe de tout ton cœur, fille de Yeroushalaim !
3:15	YHWH a aboli ta condamnation, il a éloigné ton ennemi. Le Roi d'Israël, YHWH, est au milieu de toi : tu ne verras plus de mal<!--Ps. 46:5-6 ; Col. 2:14.-->.
3:16	En ce jour-là, on dira à Yeroushalaim : N'aie pas peur Sion, que tes mains ne défaillent pas !
3:17	YHWH, ton Elohîm, est au milieu de toi comme le Puissant qui sauve. Il se réjouira à cause de toi d'une grande joie. Il se taira à cause de son amour, et exultera à ton sujet avec des cris de joie.
3:18	Je rassemblerai ceux qui sont affligés, loin des assemblées solennelles, ceux qui sont loin de toi, leur fardeau est l'insulte.
3:19	Voici, je détruirai en ce temps-là tous ceux qui t'auront affligé, je sauverai ce qui boite, je rassemblerai ce qui était banni. Je ferai d’eux une louange et un nom dans toutes les terres où ils auront été couverts de honte.
3:20	En ce temps-là, je vous ferai venir, en ce temps-là je vous rassemblerai. En effet, je vous donnerai en nom, en louange, parmi tous les peuples de la Terre, quand je ramènerai vos captifs sous vos yeux, dit YHWH.
