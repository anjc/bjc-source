# Loukas (Luc) (Lu.)

Signification : Qui donne la lumière

Auteur : Loukas (Luc)

Thème : Yéhoshoua, le Fils d'humain

Date de rédaction : Env. 60 ap. J.-C.

D'origine grecque, Loukas (Luc) fut l'auteur de l'Évangile éponyme et du livre des « Actes des apôtres ». Celui que Paulos (Paul) appelait le « médecin bien-aimé », et qui fut son compagnon d'œuvre, avait entrepris des investigations visant à narrer avec exactitude la vie terrestre de Yéhoshoua ha Mashiah (Jésus-Christ) dont il était devenu le disciple, probablement à la suite d'une prédication de Paulos. Adressés initialement à Theophilos (Théophile), Loukas était loin de penser que ses écrits constitueraient avec le temps une véritable richesse pour l'Assemblée et pour le monde.

L'évangile de Loukas présente l'humanité parfaite de Yéhoshoua, sa compassion et sa miséricorde à l'égard des plus faibles. Rédigé avec rigueur et soin, il retrace le parcours du Fils d'humain, de sa naissance à son adolescence, puis de sa mort à sa résurrection, et enfin son ascension. Il souligne aussi sa vie de prière et son fardeau pour le salut de l'humanité. Par ailleurs, il met en exergue la manière dont les femmes ont assisté Yéhoshoua par leurs biens durant son service.

Fruit de recherches minutieuses, le récit de Loukas présente certaines similitudes avec ceux de Mattithyah (Matthieu) et de Markos (Marc). Il est toutefois le seul à relater la célèbre parabole du fils dépensier (fils prodigue), profonde représentation de l'amour du Père.

## Chapitre 1

### Introduction

1:1	Puisque beaucoup ont entrepris de composer un récit des événements qui ont été accomplis parmi nous,
1:2	selon que nous les ont transmis ceux qui, dès le commencement, ont été des témoins oculaires<!--voir de ses propres yeux.--> et qui sont devenus des serviteurs de la parole,
1:3	il m'a semblé bon, à moi aussi, qui ai suivi exactement toutes ces choses dès l'origine, de te les écrire dans leur ordre, très excellent Theophilos,
1:4	afin que tu reconnaisses la sûreté des paroles que l'on t'a enseignées oralement.

### Un ange annonce la naissance de Yohanan le Baptiste (Jean-Baptiste)

1:5	Aux jours d'Hérode, roi de Judée, il y avait un prêtre du nom de Zekaryah, de la classe d'Abiyah, et sa femme était d'entre les filles d'Aaron, et s'appelait Éliysheba<!--« El du serment », généralement traduit par « Élisabeth ». C'était le nom de la femme d'Aaron. Voir Ex. 6:23.-->.
1:6	Et ils étaient tous deux justes devant Elohîm, marchant dans tous les commandements et dans toutes les ordonnances du Seigneur, sans reproche.
1:7	Et ils n'avaient pas d'enfant, parce qu'Éliysheba était stérile, et tous deux étaient très avancés en âge.
1:8	Or il arriva, pendant qu’il exerçait la prêtrise en face d’Elohîm dans l'ordre de sa classe<!--1 Ch. 24.-->,
1:9	que, selon la coutume de la prêtrise, il fut désigné par le sort à brûler l'encens en entrant dans le temple du Seigneur.
1:10	Et toute la multitude du peuple était dehors en prière, à l'heure de l'encens.
1:11	Et un ange du Seigneur lui apparut, se tenant debout à partir des droites de l'autel de l'encens.
1:12	Et Zekaryah fut troublé en le voyant, et la terreur tomba sur lui.
1:13	Mais l'ange lui dit : Zekaryah, n'aie pas peur, car ta supplication a été entendue. Et ta femme Éliysheba t'enfantera un fils et tu l'appelleras du nom de Yohanan.
1:14	Et il sera pour toi le sujet d'une grande joie et d'allégresse<!--Aux fêtes, le peuple était oint avec « l'huile de joie ». Dans Hébreux 1:9, l'auteur fait allusion à cette cérémonie, et l'utilise comme emblème de la puissance divine et de la majesté à laquelle Yéhoshoua a été élevé.-->, et beaucoup se réjouiront de sa naissance,
1:15	car il sera grand devant le Seigneur, et il ne boira jamais de vin, ni de boisson forte, et il sera rempli du Saint-Esprit dès le ventre de sa mère.
1:16	Et il ramènera beaucoup des fils d’Israël au Seigneur, leur Elohîm.
1:17	Et il marchera devant lui dans l'esprit et la puissance d'Éliyah pour ramener les cœurs des pères vers les enfants<!--Mal. 3:24.--> et les rebelles à la sagesse des justes, afin de préparer un peuple pour le Seigneur rendu prêt.
1:18	Et Zekaryah dit à l'ange : À quoi connaîtrai-je cela ? Car moi, je suis un vieillard et ma femme est avancée dans ses jours.
1:19	Et l'ange répondant, lui dit : Moi, je suis Gabriy'el, je me tiens devant Elohîm et j'ai été envoyé pour te parler et pour t’annoncer ces choses.
1:20	Et voici, tu seras muet, et tu ne pourras pas parler jusqu'au jour où ces choses arriveront, parce que tu n'as pas cru à mes paroles qui s'accompliront en leur temps.
1:21	Or le peuple attendait Zekaryah et on s'étonnait de ce qu’il s’attardait dans le temple.
1:22	Mais quand il fut sorti, il ne pouvait pas leur parler et ils comprirent qu'il avait vu une apparition dans le temple, car il leur faisait des signes et il resta muet d'une façon permanente.
1:23	Et il arriva que, quand les jours de son service furent achevés, il retourna dans sa maison.
1:24	Et après ces jours-là, Éliysheba, sa femme, conçut, et elle se cacha l'espace de cinq mois, en disant :
1:25	C'est là ce qu'a fait pour moi le Seigneur aux jours où il m'a regardée pour ôter ma honte parmi les humains.

### L'ange Gabriy'el annonce la naissance de Yéhoshoua ha Mashiah (Jésus-Christ)

1:26	Or dans le sixième mois, l'ange Gabriy'el fut envoyé par Elohîm dans une ville de Galilée, appelée Nazareth,
1:27	vers une vierge fiancée à un homme nommé Yossef, qui était de la maison de David. Et le nom de la vierge était Myriam.
1:28	Et l'ange étant entré dans le lieu où elle était, lui dit : Réjouis-toi, toi qui a été rendue gracieuse, le Seigneur est avec toi. Tu es bénie parmi les femmes.
1:29	Et en le voyant, elle fut grandement agitée par sa parole et elle raisonnait sur ce que pouvait signifier une telle salutation.
1:30	Et l'ange lui dit : Myriam, n'aie pas peur, car tu as trouvé grâce devant l'Elohîm.
1:31	Et voici, tu concevras dans ton ventre, et tu enfanteras un fils, et tu l'appelleras du nom de Yéhoshoua.
1:32	Il sera grand, et sera appelé Fils du Très-Haut, et le Seigneur Elohîm lui donnera le trône de David, son père.
1:33	Et il régnera sur la maison de Yaacov pour l’éternité et son royaume n'aura pas de fin.

### Naissance miraculeuse de Yéhoshoua ha Mashiah (Jésus-Christ)

1:34	Mais Myriam dit à l'ange : Comment cela sera-t-il, puisque je ne connais pas d'homme ?
1:35	Et l'ange lui répondit et dit : Le Saint-Esprit viendra sur toi, et la puissance du Très-Haut te couvrira de son ombre. C'est pourquoi aussi le Saint<!--Yéhoshoua est le Saint dont Iyov (Job) n'avait pas transgressé la loi (Job 6:10). Voir Es. 43:15 ; Ha. 1:12 ; Mc. 1:24 ; Lu. 4:34. Voir commentaire en Ac. 3:14.--> qui naîtra de toi sera appelé le Fils d'Elohîm.
1:36	Et voici qu'Éliysheba, ta parente, vient, elle aussi, de concevoir un fils dans sa vieillesse. Et celle que l'on appelle la stérile est dans son sixième mois.
1:37	Parce qu’aucune parole<!--Il est question du « rhema », c'est-à-dire « ce qui est ou a été émis par une voix humaine », « la chose dite », « la parole ». « Tout son produit par la voix et ayant un sens défini ».--> ne sera impossible à Elohîm.
1:38	Et Myriam dit : Voici l'esclave du Seigneur. Qu'il me soit fait selon ta parole ! Et l'ange la quitta.

### Myriam (Marie) se rend chez Éliysheba (Élisabeth)

1:39	Or en ces jours-là, Myriam se leva et s'en alla en hâte vers la région montagneuse, dans une ville de Yéhouda.
1:40	Et elle entra dans la maison de Zekaryah, et salua Éliysheba.
1:41	Et il arriva, comme Éliysheba entendait la salutation de Myriam, que le bébé tressaillit dans son ventre. Et Éliysheba fut remplie de l'Esprit Saint,
1:42	et elle s'écria d'une grande voix, et dit : Tu es bénie entre les femmes, et béni est le fruit de ton ventre.
1:43	Et d'où me vient ceci, que la mère de mon Seigneur vienne vers moi ?
1:44	Car voici, dès que la voix de ta salutation est parvenue à mes oreilles, le bébé a tressailli d'allégresse dans mon ventre.
1:45	Et bénie est celle qui a cru, parce que les choses qui lui ont été dites par le Seigneur auront leur accomplissement.

### Cantique de Myriam<!--Cp. 1 S. 2:1-10.-->

1:46	Et Myriam dit : Mon âme magnifie le Seigneur,
1:47	et mon esprit a exulté en Elohîm, mon Sauveur.
1:48	Car il a tourné les yeux sur la bassesse de son esclave. Car voici, désormais toutes les générations m'appelleront bénie,
1:49	parce que le Puissant a fait pour moi de grandes choses et son nom est Saint,
1:50	et sa miséricorde est de génération en génération en faveur de ceux qui le craignent.
1:51	Il a exercé la force souveraine avec son bras, il a dispersé ceux qui avaient dans le cœur des pensées orgueilleuses.
1:52	Il a renversé les princes de leurs trônes et il a élevé les humbles.
1:53	Il a rassasié de biens les affamés et il a renvoyé les riches les mains vides.
1:54	Il a pris sous sa protection Israël, son serviteur, et il s'est souvenu de la miséricorde,
1:55	comme il avait dit à nos pères, à Abraham et à sa postérité pour toujours.
1:56	Et Myriam demeura avec elle environ trois mois. Et elle retourna dans sa maison.

### Naissance de Yohanan (Jean)

1:57	Or le temps où Éliysheba devait accoucher s'accomplit, et elle enfanta un fils.
1:58	Et ses voisins et ses parents ayant appris que le Seigneur avait magnifié sa miséricorde envers elle, s'en réjouissaient avec elle.
1:59	Et il arriva qu'au huitième jour, ils vinrent pour circoncire l'enfant, et ils l'appelaient Zekaryah, du nom de son père.
1:60	Mais sa mère répondit et dit : Non, mais il sera appelé Yohanan.
1:61	Et ils lui dirent : Il n'y a personne dans ta parenté qui soit appelé de ce nom.
1:62	Mais ils demandèrent par signes à son père comment il voulait qu'on l'appelle.
1:63	Et ayant demandé une tablette, il écrivit ces mots : Yohanan est son nom. Et tous furent dans l'étonnement.
1:64	Et au même instant, sa bouche s'ouvrit et sa langue se délia, et il parlait, bénissant Elohîm.
1:65	Et la terreur vint sur tous leurs voisins et, dans toute la région montagneuse de la Judée, on parlait de toutes ces choses.
1:66	Et tous ceux qui les apprirent les gardèrent dans leur cœur, disant : Que sera donc cet enfant ? Et la main du Seigneur était avec lui.

### Cantique de Zekaryah (Zacharie)

1:67	Et Zekaryah son père fut rempli du Saint-Esprit et prophétisa en disant :
1:68	Béni soit le Seigneur, l'Elohîm d'Israël, de ce qu'il a visité et accompli la rédemption de son peuple,
1:69	et de ce qu'il a réveillé pour nous la corne du salut<!--Yéhoshoua est YHWH, voir 2 S. 22:3 ; Ez. 29:21 ; Ps. 18:3.--> dans la maison de David, son serviteur,
1:70	comme il l'avait dit à travers la bouche de ses saints prophètes pour toujours<!--Du grec « aion » qui signifie « période », « âge », etc.-->.
1:71	Un salut hors de nos ennemis et hors de la main de tous ceux qui nous haïssent !
1:72	C'est ainsi qu'il manifeste sa miséricorde envers nos pères, et se souvient de sa sainte alliance,
1:73	le serment par lequel il avait juré à Abraham, notre père,
1:74	de nous permettre, étant délivrés hors de la main de nos ennemis, de le servir sans crainte,
1:75	dans la sainteté et dans la justice devant lui, tous les jours de notre vie.
1:76	Et toi, enfant, tu seras appelé prophète du Très-Haut, car tu marcheras en face du Seigneur pour préparer ses voies,
1:77	afin de donner à son peuple la connaissance du salut, par le pardon de leurs péchés,
1:78	grâce aux entrailles de la miséricorde de notre Elohîm, en vertu de laquelle le Soleil Levant nous a visités d'en haut,
1:79	pour apparaître sur ceux qui sont assis dans la ténèbre et dans l'ombre de la mort et pour conduire nos pas dans la voie de la paix.
1:80	Or l'enfant croissait et se fortifiait en esprit. Et il demeura dans les déserts jusqu'au jour où il se présenta à Israël.

## Chapitre 2

### Naissance de Yéhoshoua à Bethléhem<!--Mt. 1:18-25, 2:1 ; Jn. 1:14.-->

2:1	Or il arriva en ces jours-là, qu'une ordonnance fut publiée par César Augustus<!--Kaisar Augustus (César Auguste), de son vrai nom Octave, est le fils d'un notable romain et de la nièce de Jules César, lequel en a fait son fils adoptif faute d'héritier direct. Il a seulement 19 ans à la mort de celui-ci, en 44 av. J.-C. Il décède à 76 ans, le 19 août de l'an 14 après J.-C.--> pour que toute la terre habitée<!--Vient du grec « oikoumene » qui signifie « terre habitée », « l'univers », « territoire habité par les Grecs », « tous les sujets de l'empire romain ».--> se fasse enregistrer.
2:2	Ce premier enregistrement eut lieu pendant que Quirinius était gouverneur de Syrie.
2:3	Et tous allaient se faire enregistrer, chacun dans sa propre ville.
2:4	Et Yossef aussi monta de la Galilée, de la ville de Nazareth, pour se rendre en Judée, dans la ville de David appelée Bethléhem, parce qu'il était de la maison et de la famille de David,
2:5	afin de se faire enregistrer avec Myriam, la femme qui lui était fiancée, laquelle était enceinte.
2:6	Et il arriva pendant qu'ils étaient là, que le temps où Myriam devait accoucher arriva,
2:7	et elle enfanta son fils premier-né. Et elle l’enveloppa de langes et le coucha dans une crèche, parce qu'il n'y avait pas de place pour eux dans l'auberge.

### L'Ange du Seigneur annonce la naissance de Yéhoshoua

2:8	Et il y avait dans cette même contrée des bergers qui vivaient dans les champs et qui gardaient leur troupeau pendant les veilles de la nuit.
2:9	Et voici qu'un ange du Seigneur se tint au-dessus d'eux, et la gloire du Seigneur resplendit autour d'eux. Et ils furent saisis d'une grande peur.
2:10	Mais l'ange leur dit : N'ayez pas peur, car voici, je vous prêche une bonne nouvelle qui sera pour tout le peuple une grande joie :
2:11	C'est qu'aujourd'hui, dans la ville de David, il vous est né un Sauveur qui est Mashiah Seigneur.
2:12	Et ceci est le signe pour vous : Vous trouverez un bébé enveloppé de langes et couché dans une crèche.
2:13	Et soudain il y eut avec l'ange une multitude de l'armée céleste, louant l'Elohîm et disant :
2:14	Gloire à Elohîm dans les lieux très hauts, et, sur la Terre, paix et bienveillance dans les humains !

### Les bergers de Bethléhem

2:15	Et il arriva, lorsque les anges les eurent quittés pour le ciel, que ces hommes, les bergers, se dirent les uns aux autres : Allons donc jusqu'à Bethléhem, et voyons cette chose qui est arrivée, ce que le Seigneur nous a fait connaître.
2:16	Et ils y allèrent donc en hâte, et ils trouvèrent Myriam et Yossef, et le bébé couché dans une crèche.
2:17	Et l’ayant vu, ils publièrent autour d'eux ce qui leur avait été dit au sujet de cet enfant.
2:18	Mais tous ceux qui les entendirent furent dans l'étonnement de ce qui leur avait été dit par les bergers.
2:19	Mais Myriam gardait toutes ces paroles, les repassant dans son cœur.
2:20	Et les bergers s'en retournèrent, glorifiant et louant Elohîm pour tout ce qu'ils avaient entendu et vu, et qui était conforme à ce qui leur avait été annoncé.

### Yéhoshoua circoncis et présenté au temple de Yeroushalaim (Jérusalem)<!--Ex. 13:12,15.-->

2:21	Et quand les huit jours furent accomplis pour circoncire l'enfant, il fut appelé du Nom de Yéhoshoua, comme l'ange l'avait aussi appelé avant qu'il ne soit conçu dans le ventre.
2:22	Et, quand les jours de leur purification<!--Lé. 12:2-6.--> furent accomplis selon la torah de Moshé, ils l’emmenèrent à Yeroushalaim pour le présenter au Seigneur,
2:23	selon ce qui est écrit dans la torah du Seigneur : Tout mâle qui ouvre complètement une matrice sera appelé « consacré au Seigneur<!--Ex. 13:2,12 ; No. 3:13, 8:17.--> »,
2:24	et pour donner en sacrifice, ainsi qu'il est dit dans la torah du Seigneur<!--Lé. 12:8.-->, deux tourterelles ou deux jeunes pigeons.

### Prophétie de Shim’ôn (Siméon)

2:25	Et voici, il y avait à Yeroushalaim un homme appelé Shim’ôn. Et cet homme était juste et pieux, il attendait la consolation d'Israël, et le Saint-Esprit était sur lui.
2:26	Et il avait été averti divinement par le Saint-Esprit qu'il ne verrait pas la mort avant d'avoir vu le Mashiah du Seigneur.
2:27	Et il vint dans le temple, poussé par l'Esprit. Et, comme les parents apportaient dans le temple l'enfant Yéhoshoua, pour accomplir à son égard ce qu'ordonnait la torah,
2:28	il le prit dans ses bras, et bénit Elohîm et dit :
2:29	Maître<!--Vient du grec « despotes ». Voir Ac. 4:24 ; 2 Pi. 2:1 ; Jud. 1:4 ; Ap. 6:10. Yéhoshoua est le seul Maître.-->, tu laisses maintenant ton esclave s'en aller en paix, selon ta parole.
2:30	Parce que mes yeux ont vu ton salut,
2:31	que tu as préparé face à tous les peuples,
2:32	la lumière pour la révélation des nations et la gloire de ton peuple d'Israël.
2:33	Et Yossef et sa mère étaient dans l'admiration des choses qui étaient dites de lui.
2:34	Et Shim’ôn les bénit et dit à Myriam, sa mère : Voici, celui-ci est établi pour la chute et la résurrection de beaucoup en Israël, et pour être un signe qui provoquera la contradiction,
2:35	et toi-même, une grande épée te transpercera l’âme, afin que les pensées sortant de beaucoup de cœurs soient découvertes.

### Channah (Anne) témoigne du Mashiah

2:36	Il y avait aussi Channah, la prophétesse, fille de Penouel, de la tribu d'Asher. Elle était déjà avancée en âge. Et elle avait vécu avec son mari 7 ans depuis sa virginité.
2:37	Restée veuve, et âgée d'environ 84 ans, elle ne quittait pas le temple, servant par des jeûnes et des supplications, nuit et jour.
2:38	Elle aussi, survenant en cette même heure, louait le Seigneur et parlait de lui à tous ceux qui, dans Yeroushalaim attendaient la rédemption.

### Retour à Nazareth<!--Suite aux évènements de Mt. 2.-->

2:39	Et quand ils eurent tout accompli selon la torah du Seigneur, ils retournèrent en Galilée, à Nazareth, leur ville.
2:40	Et l'enfant croissait et se fortifiait en esprit. Il était rempli de sagesse, et la grâce d'Elohîm était sur lui.

### Yéhoshoua dans le temple de Yeroushalaim (Jérusalem), assis au milieu des docteurs

2:41	Et ses parents allaient chaque année à Yeroushalaim, à la fête de Pâque.
2:42	Et lorsqu’il eut 12 ans, ils montèrent à Yeroushalaim selon la coutume de la fête.
2:43	Et s'en retournant après avoir accompli les jours, l'enfant Yéhoshoua resta à Yeroushalaim. Et Yossef et sa mère ne s'en aperçurent pas.
2:44	Mais pensant qu'il était avec leurs compagnons de voyage, ils marchèrent le chemin d’un jour et le cherchèrent parmi les parents et parmi les connaissances.
2:45	Et ne le trouvant pas, ils retournèrent à Yeroushalaim pour le chercher.
2:46	Or il arriva que trois jours après, ils le trouvèrent dans le temple, assis au milieu des docteurs, les écoutant et les interrogeant.
2:47	Mais tous ceux qui l'entendaient s'étonnaient de sa connaissance et de ses réponses.
2:48	Et en le voyant, ils furent choqués et sa mère lui dit : Mon enfant, pourquoi as-tu agi ainsi avec nous ? Voici, ton père et moi te cherchions avec angoisse.
2:49	Et il leur dit : Pourquoi me cherchiez-vous ? Ne saviez-vous pas qu’il me faut être avec mon Père ?
2:50	Mais ils ne comprirent pas la parole qu'il leur disait.
2:51	Et il descendit avec eux et vint à Nazareth, et il leur était soumis. Et sa mère gardait continuellement toutes ces paroles dans son cœur.
2:52	Et Yéhoshoua progressait en sagesse et en stature, et en grâce auprès d'Elohîm et auprès des humains.

## Chapitre 3

### Yohanan le Baptiste, l'envoyé d'Elohîm<!--Mt. 3:1-12 ; Mc. 1:1-8 ; Jn. 1:6-8,15-37.-->

3:1	Or, en la quinzième année du règne de l'empereur romain Tibère César, Ponce Pilate étant gouverneur de la Judée, Hérode, tétrarque de la Galilée, et son frère Philippos, tétrarque de Yethour<!--Iturée.--> et du territoire de la Trachonite, et Lysanias, tétrarque de l'Abilène,
3:2	du temps des grands-prêtres Chananyah et Kaïaphas, la parole d'Elohîm vint à Yohanan, fils de Zekaryah, dans le désert.
3:3	Et il alla dans toute la région environnante du Yarden<!--Jourdain.-->, prêchant le baptême de repentance, pour le pardon des péchés,
3:4	comme il est écrit dans le livre des paroles de Yesha`yah, le prophète, disant : Voix de celui qui crie : Dans le désert, préparez la voie du Seigneur, aplanissez ses sentiers<!--Es. 40:3 ; Mal. 3:1.-->.
3:5	Toute vallée sera remplie, et toute montagne et toute colline seront abaissées, et les choses tortueuses deviendront droites, et les voies rocheuses seront nivelées.
3:6	Et toute chair verra le salut d'Elohîm<!--Es. 40:3-5.-->.
3:7	Il disait donc aux foules qui sortaient pour être baptisées par lui : Progénitures de vipères, qui vous a appris à fuir la colère qui est imminente ?
3:8	Produisez donc des fruits dignes de la repentance, et ne vous mettez pas à dire en vous-mêmes : Nous avons Abraham pour père ! Car je vous dis qu'Elohîm peut réveiller, à partir de ces pierres, des enfants à Abraham.
3:9	Or la hache est déjà mise à la racine des arbres : tout arbre donc qui ne produit pas de beau fruit est coupé et jeté au feu.
3:10	Et les foules l'interrogeaient, disant : Que ferons-nous donc ?
3:11	Et il répondit, et leur dit : Que celui qui a deux tuniques partage avec celui qui n'en a pas, et que celui qui a de quoi manger en fasse de même.
3:12	Mais il vint aussi à lui des publicains pour être baptisés, et ils lui dirent : Docteur, que ferons-nous ?
3:13	Et il leur dit : N'exigez rien au-delà de ce qui vous a été ordonné.
3:14	Et des soldats l'interrogèrent aussi, en disant : Et nous, que ferons-nous ? Et il leur dit : Ne commettez ni extorsion ni fraude envers personne, mais contentez-vous de votre salaire.
3:15	Et le peuple était dans l’attente et tous raisonnaient dans leurs cœurs au sujet de Yohanan, s’il n’était pas lui-même le Mashiah.
3:16	Yohanan répondit et dit à tous : En effet, moi, je vous baptise dans l'eau. Mais il vient, celui qui est plus puissant que moi, et je ne suis pas digne de délier le lacet de ses sandales. C'est lui qui vous baptisera dans le Saint-Esprit et le feu.
3:17	Il a la pelle à vanner dans sa main et il nettoiera complètement son aire, et assemblera le blé dans son grenier, mais il brûlera la paille dans un feu qui ne s'éteint pas.
3:18	Et faisant en effet beaucoup d'autres exhortations, il évangélisait le peuple.
3:19	Mais Hérode, le tétrarque, étant repris par lui au sujet d'Hérodias, femme de Philippos, son frère, et à cause de toutes les choses méchantes qu'Hérode faisait,
3:20	ajouta encore à toutes les autres celle de mettre Yohanan en prison.

### Le baptême de Yéhoshoua ha Mashiah (Jésus-Christ)<!--Mt. 3:13-17 ; Mc. 1:9-11 ; Jn. 1:31-34.-->

3:21	Et il arriva que comme tout le peuple était baptisé, Yéhoshoua aussi étant baptisé et étant en prière, le ciel s'ouvrit,
3:22	et le Saint-Esprit descendit sur lui sous une forme corporelle, comme celle d'une colombe. Et une voix vint du ciel : Tu es mon Fils bien-aimé ! C'est en toi que j'ai pris plaisir.

### Genèse de Yéhoshoua<!--V. 31 ; Mt. 1:1-16.-->

3:23	Et Yéhoshoua avait environ 30 ans, lorsqu'il commença, étant comme on pensait, fils de Yossef, d'Éli,
3:24	de Matthat, de Lévi, de Melchi, de Yanah, de Yossef<!--Joseph.-->,
3:25	de Mattithyah, d'Amos, de Nahoum, d'Esli, de Naggaï,
3:26	de Maath, de Mattithyah, de Shimeï, de Yosseh, de Yéhoyada,
3:27	de Yohanan, de Rhésa, de Zerubbabel<!--Zorobabel.-->, de Shealthiel, de Neriyah,
3:28	de Melchi, d'Addi, de Kosam, d'Elmadam, d'Er,
3:29	de Yéhoshoua, d'Éliy`ezer, de Yorim, de Matthat, de Lévi,
3:30	de Shim’ôn, de Yéhouda, de Yossef, de Yonam, d'Élyakim,
3:31	de Meleas, de Mainan, de Mattattah, de Nathan, de David,
3:32	d'Isaï, d'Obed, de Boaz, de Salmon, de Nahshôn,
3:33	d'Amminadab, d'Aram, de Hetsron, de Pérets, de Yéhouda,
3:34	de Yaacov<!--Jacob.-->, de Yitzhak<!--Isaac.-->, d'Abraham, de Térach, de Nachor,
3:35	de Seroug, de Réou, de Péleg, de Héber, de Shélach,
3:36	de Qeynan, d'Arpacshad, de Shem, de Noah<!--Noé.-->, de Lémek,
3:37	de Metoushèlah, d'Hanowk<!--Hénoc.-->, de Yered, de Mahalal'el, de Qeynan,
3:38	d'Enowsh, de Sheth, d'Adam, d'Elohîm.

## Chapitre 4

### La tentation de Yéhoshoua ha Mashiah<!--Mt. 4:1 ; Mc. 1:12-13 ; cp. Ge. 3:6 ; 1 Jn. 2:16.-->

4:1	Et Yéhoshoua, rempli du Saint-Esprit, revint du Yarden et il fut conduit par l'Esprit dans le désert,
4:2	où il fut tenté par le diable pendant 40 jours. Et il ne mangea rien durant ces jours-là et, après qu'ils furent écoulés, il eut faim.
4:3	Et le diable lui dit : Si tu es le Fils d'Elohîm, ordonne à cette pierre qu'elle devienne du pain.
4:4	Et Yéhoshoua lui répondit, en disant : Il est écrit que l'être humain ne vivra pas seulement de pain, mais de toute parole d'Elohîm<!--De. 8:3.-->.
4:5	Et le diable l'ayant emmené sur une haute montagne, lui montra en un instant tous les royaumes de la terre habitée,
4:6	et le diable lui dit : Je te donnerai toute cette autorité et leur gloire, car elle m'est livrée et je la donne à qui je veux.
4:7	Si donc tu te prosternes devant moi, elle sera toute à toi.
4:8	Et Yéhoshoua répondant lui dit : Va-t'en, arrière de moi, Satan ! Car il est écrit : C'est devant le Seigneur, ton Elohîm, que tu te prosterneras, et tu le serviras lui seul<!--De. 6:13.-->.
4:9	Et il le mena à Yeroushalaim et le plaça au sommet du temple, et lui dit : Si tu es le Fils d'Elohîm, jette-toi d'ici en bas,
4:10	car il est écrit qu'il ordonnera à ses anges à ton sujet de te garder.
4:11	Et ils te porteront sur leurs mains, de peur que tu ne heurtes du pied une pierre<!--Ps. 91:11-12.-->.
4:12	Mais Yéhoshoua répondant, lui dit : Il est dit : Tu ne tenteras pas le Seigneur, ton Elohîm<!--De. 6:16.-->.
4:13	Et ayant achevé toute tentation, le diable se retira de lui jusqu’au temps<!--Kaïros « le temps opportun et favorable ».--> convenable.

### Yéhoshoua ha Mashiah retourne en Galilée<!--Mt. 4:12-17 ; Mc. 1:14-15.-->

4:14	Et Yéhoshoua retourna en Galilée dans la puissance de l'Esprit, et sa renommée se répandit dans toute la région environnante.
4:15	Et il enseignait dans leurs synagogues, et il était glorifié par tous.

### Yéhoshoua lit le rouleau de Yesha`yah (Ésaïe)<!--Cp. Mt. 13:54-58 ; Mc. 6:1-6.-->

4:16	Et il se rendit à Nazareth, où il avait été élevé, et selon sa coutume, il entra dans la synagogue le jour du shabbat. Et il se leva pour faire la lecture,
4:17	et on lui donna le livre du prophète Yesha`yah. Et ayant déroulé le livre, il trouva le passage où il est écrit :
4:18	L'Esprit du Seigneur<!--Yéhoshoua lit précisément le passage de Yesha`yah (Ésaïe), Es. 61:1-2 : « L'Esprit d'Adonaï YHWH est sur moi, car YHWH m'a oint pour évangéliser les malheureux (...) pour publier une année de grâce de YHWH (...) ». Alors que Yesha`yah nomme le Seigneur par son Nom, à savoir YHWH, traduit par « l'Éternel » dans la plupart des bibles modernes, celui-ci n'a pas été restitué dans Lu. 4:18. En effet, le tétragramme, qui apparaît plus de 5 000 fois dans le Tanakh, ne figure pas dans les textes grecs. Ainsi, tous les passages des évangiles et des lettres (épîtres) citant des extraits du Tanakh rendent le tétragramme par les termes grecs « Kurios » ou « Theos », qui signifient respectivement « Seigneur » et « Dieu ».--> est sur moi, parce qu'il m'a oint pour évangéliser les pauvres ; il m'a envoyé pour guérir ceux qui ont le cœur brisé, pour proclamer aux captifs la délivrance et aux aveugles le recouvrement de la vue, pour mettre en liberté les opprimés,
4:19	pour publier une année favorable du Seigneur<!--Es. 61:1-2.-->.
4:20	Et ayant enroulé le livre, et l'ayant rendu au serviteur, il s'assit. Les yeux de tous ceux qui étaient dans la synagogue étaient fixés sur lui.
4:21	Alors il commença à leur dire : Aujourd'hui cette écriture est accomplie à vos oreilles !
4:22	Et tous lui rendaient témoignage, et s'étonnaient des paroles de grâce qui sortaient de sa bouche, et ils disaient : Celui-ci n'est-il pas le fils de Yossef ?
4:23	Et il leur dit : Sûrement vous me direz ce proverbe : Médecin, guéris-toi toi-même. Tout ce qu’on nous a dit être arrivé à Capernaüm, fais-le de même ici dans ton pays natal.
4:24	Mais, il leur dit : Amen, je vous dis qu'aucun prophète n'est accepté dans son pays natal.
4:25	Mais en vérité<!--Vient du grec « aletheia » qui signifie « objectivement », « ce qui est vrai quelle que soit la considération ».-->, je vous le dis : Il y avait beaucoup de veuves en Israël du temps d'Éliyah, lorsque le ciel fut fermé 3 ans et 6 mois, de sorte qu’est survenue une grande famine sur toute la terre.
4:26	Et Éliyah ne fut envoyé vers aucune d'elles, sauf à Sarepta de Sidon, vers une femme veuve.
4:27	Et il y avait aussi beaucoup de lépreux en Israël du temps d'Éliysha, le prophète, et aucun d'eux ne fut purifié, sauf Naaman, le Syrien.
4:28	Et ils furent tous remplis de colère dans la synagogue lorsqu'ils entendirent ces choses.
4:29	Et s'étant levés, ils le chassèrent hors de la ville et le menèrent jusqu'au bord de la montagne sur laquelle leur ville était bâtie, pour le jeter du haut en bas.
4:30	Mais il passa au milieu d'eux et s'en alla.

### Guérison d'un démoniaque<!--Mc. 1:21-28.-->

4:31	Et il descendit à Capernaüm, ville de Galilée, et il les enseignait les jours de shabbat.
4:32	Et ils étaient choqués par sa doctrine, car il parlait avec autorité.
4:33	Et il y avait dans la synagogue un homme qui avait un esprit de démon impur, et il s’écria d'une grande voix,
4:34	en disant : Ah ! Qu'y a-t-il entre nous et toi, Yéhoshoua de Nazareth ? Es-tu venu pour nous détruire ? Je sais qui tu es ! Le Saint<!--Job 6:10. Voir Mc. 1:24 ; Lu. 1:35. Voir commentaire en Ac. 3:14.--> d'Elohîm.
4:35	Et Yéhoshoua le réprimanda d'une manière tranchante, en lui disant : Sois muselé, et sors de cet homme ! Et le démon, après l’avoir jeté au milieu, sortit de lui sans lui avoir fait aucun mal.
4:36	Et tous furent saisis de stupéfaction, et ils parlaient entre eux, et disaient : Quelle est cette parole ? Car il commande avec autorité et puissance aux esprits impurs, et ils sortent !
4:37	Et la rumeur à son sujet se répandait en tout lieu dans la région environnante.

### Guérison de la belle-mère de Petros (Pierre) et de beaucoup de malades<!--Mt. 8:14-17 ; Mc. 1:29-34.-->

4:38	Et s'étant levé, il sortit de la synagogue et entra dans la maison de Shim’ôn. Et la belle-mère de Shim’ôn avait une violente fièvre, et ils le prièrent en sa faveur.
4:39	Et s'étant placé au-dessus d'elle, il réprimanda d'une manière tranchante la fièvre, et elle la quitta. À l'instant elle se leva, et les servit.
4:40	Et au coucher du soleil, tous ceux qui avaient des malades atteints de diverses maladies les lui amenèrent. Et il imposa les mains à chacun d'eux, et il les guérit.
4:41	Les démons aussi sortirent de beaucoup de personnes, en criant et en disant : Tu es le Mashiah, le Fils d'Elohîm. Mais il les réprimandait d'une manière tranchante et ne leur permettait pas de dire qu'ils savaient qu'il était le Mashiah.
4:42	Mais dès que le jour parut, il sortit et alla dans un lieu désert. Et les foules le cherchaient et, étant venues jusqu'à lui, elles le retenaient afin qu'il ne les quittât pas.
4:43	Mais il leur dit : Il faut que je prêche aux autres villes l'Évangile du Royaume d'Elohîm, car c'est pour cela que j'ai été envoyé.
4:44	Et il prêchait dans les synagogues de la Galilée.

## Chapitre 5

### L'appel des premiers disciples<!--Mt. 4:18-22 ; Mc. 1:16-20 ; cp. Jn. 1:35-51, 21:1-8.-->

5:1	Or il arriva, comme la foule se pressait contre lui pour entendre la parole d'Elohîm, qu'il se tenait sur le bord du lac de Génésareth.
5:2	Il vit deux bateaux qui étaient au bord du lac, et dont les pêcheurs étaient descendus et lavaient leurs filets.
5:3	Or étant monté dans l'un de ces bateaux qui était à Shim’ôn, il le pria de s'éloigner un peu de terre. Et s'étant assis, du bateau il enseignait les foules.
5:4	Et quand il eut cessé de parler, il dit à Shim’ôn : Avance vers la profondeur, et relâchez vos filets pour la pêche.
5:5	Et Shim’ôn répondant, lui dit : Maître, nous avons travaillé dur toute la nuit et nous n'avons rien pris, mais sur ta parole<!--Vient du grec « rhema » qui signifie « ce qui est ou a été émis par une voix humaine », « la chose dite », « la parole ».--> je relâcherai les filets.
5:6	Et l’ayant fait, ils enfermèrent ensemble une grande multitude de poissons, et leur filet se rompait.
5:7	Et ils firent signe à leurs compagnons qui étaient dans l'autre bateau de venir les aider. Et étant venus, ils remplirent les deux bateaux, de sorte qu'ils s'enfonçaient.
5:8	Or Shim’ôn Petros, ayant vu cela, tomba aux genoux de Yéhoshoua, en lui disant : Seigneur, retire-toi de moi, car je suis un homme pécheur.
5:9	Car la stupéfaction l'avait saisi, lui et tous ceux qui étaient avec lui, pour la pêche des poissons qu’ils avaient pris. 
5:10	Et de même que Yaacov et Yohanan, fils de Zabdi<!--Généralement traduit par Zébédée.-->, qui étaient les compagnons de Shim’ôn. Et Yéhoshoua dit à Shim’ôn : N'aie pas peur ! Dès maintenant tu seras un captureur d'êtres humains.
5:11	Et quand ils eurent ramené les bateaux à terre, ils abandonnèrent tout et le suivirent.

### Guérison d'un lépreux<!--Mt. 8:2-4 ; Mc. 1:40-45.-->

5:12	Et il arriva que, comme il était dans une des villes, voici un homme plein de lèpre, qui voyant Yéhoshoua et tombant sur sa face, le suppliait en disant : Seigneur, si tu veux, tu peux me rendre pur.
5:13	Et ayant étendu la main, il le toucha en disant : Je le veux, sois pur. Immédiatement la lèpre le quitta.
5:14	Et il lui ordonna de n'en parler à personne. Mais va et montre-toi au prêtre, et offre pour ta purification ce que Moshé a prescrit<!--Lé. 13 et 14.--> pour leur témoignage.
5:15	Mais la parole le concernant se répandait de plus en plus, et des foules nombreuses se rassemblaient pour l'entendre, et pour être guéries par lui de leurs maladies.
5:16	Mais il se tenait retiré dans les déserts, et priait.

### Guérison d'un paralytique<!--Mt. 9:2-8 ; Mc. 2:3-12.-->

5:17	Et il arriva, un de ces jours, qu'il enseignait. Et des pharisiens et des docteurs de la torah, qui étaient venus de chaque village de Galilée, et de Judée, et de Yeroushalaim, étaient assis là. Et la puissance du Seigneur était là pour les guérir.
5:18	Et voici des hommes portant sur un lit un homme qui était paralytique, et ils cherchaient à le faire entrer et à le mettre devant lui.
5:19	Et ne trouvant pas par quel moyen le faire entrer, à cause de la foule, ils montèrent sur le toit<!--Les toits orientaux, en terrasses, étaient (et sont encore) utilisés pour se reposer, mais aussi pour la méditation et la prière.-->, et ils le descendirent à travers les tuiles de toiture<!--La phrase « à travers les tuiles de toiture », veut dire par la porte du toit à laquelle arrivait une échelle ou un escalier venant de la rue. Les Juifs distinguaient deux manières pour entrer dans une maison, « le moyen par la porte » et « le moyen par le toit ».-->, avec son lit, au milieu, devant Yéhoshoua.
5:20	Et voyant leur foi, il lui dit : Homme, tes péchés te sont remis.
5:21	Et les scribes et les pharisiens commencèrent à raisonner en disant : Qui est celui-ci qui profère des blasphèmes ? Qui peut remettre les péchés, excepté Elohîm seul ?
5:22	Mais Yéhoshoua, connaissant leurs raisonnements, répondant, leur dit : Pourquoi raisonnez-vous ainsi dans vos cœurs ?
5:23	Qu'est-ce qui est plus facile ? Dire : Tes péchés te sont remis, ou dire : Réveille-toi et marche ?
5:24	Or afin que vous sachiez que le Fils d'humain a l'autorité sur la Terre de remettre les péchés - il dit au paralytique : Je te dis, Réveille-toi ! Prends ton lit et va dans ta maison ! 
5:25	Et immédiatement, se levant devant eux, et prenant ce sur quoi il était couché, il s'en alla dans sa maison, glorifiant Elohîm.
5:26	Ils furent tous saisis d'étonnement et ils glorifiaient Elohîm. Et étant remplis de crainte, ils disaient : Nous avons vu aujourd'hui des choses incroyables.

### Appel de Lévi<!--Mt. 9:9 ; Mc. 2:13-14.-->

5:27	Et après ces choses il sortit, et vit un publicain du nom de Lévi, assis au bureau des péages et il lui dit : Suis-moi.
5:28	Et, laissant tout, il se leva et le suivit.

### Yéhoshoua appelle les pécheurs à la repentance<!--Mt. 9:10-15 ; Mc. 2:13-14.-->

5:29	Et Lévi lui fit un grand festin dans sa maison, et il y avait une grande foule de publicains et d'autres gens qui étaient avec eux à table.
5:30	Et les pharisiens et leurs scribes murmuraient contre ses disciples, en disant : En raison de quoi mangez-vous et buvez-vous avec les publicains et les pécheurs ?
5:31	Et Yéhoshoua, répondant, leur dit : Ceux qui sont en bonne santé n'ont pas besoin de médecin, mais ceux qui sont malades.
5:32	Je ne suis pas venu appeler à la repentance les justes, mais les pécheurs.
5:33	Mais ils lui dirent : Pourquoi les disciples de Yohanan, tout comme ceux des pharisiens, jeûnent-ils souvent et font-ils des supplications, tandis que les tiens mangent et boivent ?
5:34	Mais il leur dit : Pouvez-vous faire jeûner les fils de la chambre nuptiale pendant que l'Époux est avec eux ?
5:35	Mais les jours viendront où l'Époux leur sera enlevé, alors ils jeûneront en ces jours-là.

### Parabole de l'habit neuf et des outres neuves<!--Mt. 9:16-17 ; Mc. 2:21-22.-->

5:36	Mais il leur disait aussi une parabole : Personne ne met une pièce d'un vêtement neuf à un vêtement vieux ; autrement, le neuf déchire le vieux, et la pièce issue du neuf ne s'accorde pas avec le vieux.
5:37	Et personne ne met du vin nouveau dans de vieilles outres, autrement, le vin nouveau fait rompre les outres, et il se répand, et les outres sont perdues.
5:38	Mais le vin nouveau doit être mis dans des outres neuves<!--Mt. 9:16-17 ; Mc. 2:21-22. Voir Job. 32:19.-->, et les deux se conservent ensemble.
5:39	Et personne, après avoir bu du vin vieux, ne veut du nouveau, car il dit : Le vieux est bon.

## Chapitre 6

### Yéhoshoua, le Seigneur du shabbat<!--Mt. 12:1-8 ; Mc. 2:23-28.-->

6:1	Or il arriva, au shabbat second-premier<!--Certains pensent qu'il s'agit du second jour des azymes, tandis que d'autres estiment que c'est le dernier. Il y en a aussi qui l'associent à la pentecôte puisque la fête des pains sans levain correspond au premier shabbat.--> qu'il passait au travers les champs ensemencés. Et ses disciples arrachaient les épis et, les froissant dans leurs mains, ils les mangeaient.
6:2	Et quelques pharisiens leur dirent : Pourquoi faites-vous ce qui n'est pas légal de faire pendant les shabbats ?
6:3	Et Yéhoshoua répondant, leur dit : N'avez-vous même pas lu ce que fit David quand il eut faim, lui et ceux qui étaient avec lui ?
6:4	Comment il entra dans la maison d'Elohîm, et prit les pains des faces, et en mangea, et en donna aussi à ceux qui étaient avec lui, bien qu'il ne soit légal que pour les prêtres seuls d'en manger<!--1 S. 21:1-7.--> ?
6:5	Et il leur dit : Le Fils d'humain est Seigneur même du shabbat.

### Guérison de l'homme à la main sèche<!--Mt. 12:9-13 ; Mc. 3:1-5.-->

6:6	Or il arriva aussi, un autre shabbat, qu'il entra dans la synagogue pour enseigner. Et il s'y trouvait là un homme dont la main droite était sèche.
6:7	Or les scribes et les pharisiens l'observaient, s'il guérirait durant le shabbat, afin de trouver de quoi l'accuser.
6:8	Mais il connaissait leurs pensées, et il dit à l'homme qui avait la main sèche : Réveille-toi et tiens-toi debout au milieu ! Et il se leva et se tint debout.
6:9	Yéhoshoua donc leur dit : Je vous demanderai : Qu'est-il légal durant les shabbats ? De faire du bien ou de faire du mal ? De sauver une âme ou de la perdre ?
6:10	Et ayant regardé tous ceux qui étaient autour de lui, il dit à l'homme : Étends ta main ! Il fit ainsi, et sa main fut restaurée à l'état initial comme l'autre.
6:11	Et ils furent remplis de fureur, et ils s'entretenaient ensemble de ce qu'ils feraient à Yéhoshoua.

### Choix des douze apôtres<!--Mt. 10:2-4 ; Mc. 3:13-19.-->

6:12	Or il arriva en ces jours-là, qu'il s'en alla sur une montagne pour prier, et il passa toute la nuit à prier Elohîm.
6:13	Et quand le jour fut venu, il appela ses disciples et en choisit parmi eux douze, auxquels il donna aussi le nom d'apôtres :
6:14	Shim’ôn, qu'il nomma aussi Petros, et Andreas, son frère, et Yaacov et Yohanan, Philippos et Bar-Talmaï ;
6:15	Mattithyah et Thomas, Yaacov, celui d'Alphaios, et Shim’ôn, surnommé zélote<!--Le mot zélote signifie « celui qui est zélé ». Les zélotes avaient parmi leurs modèles Phinées, le fils zélé d'Èl’azar, fils d'Aaron. Ce dernier s'était illustré en tuant un prince d'une tribu d'Israël qui avait provoqué la colère d'Elohîm en se débauchant ouvertement avec une Madianite (No. 25:6-11). Issus des milieux populaires, les zélotes étaient à la fois des adeptes d'une secte religieuse espérant hâter l'accomplissement des promesses divines et des militants politiques qui s'opposaient à l'occupant romain. Pour eux, leur pays ne pouvait être gouverné que par un descendant du roi David. Ils furent à l'origine de la Grande Révolte Juive (66-70), en déclenchant en août 66 un soulèvement sanglant au cours duquel ils s'emparèrent de Yeroushalaim (Jérusalem) et décimèrent les grands-prêtres qui collaboraient avec l'occupant. En représailles, Vespasien (9-79) puis Titus (39-81) organisèrent un siège de la ville qui aboutit au saccage de la cité et à la destruction du temple en 70.--> ;
6:16	Yéhouda de Yaacov, et Yéhouda Iskariote, qui devint traître.

### L'enseignement de Yéhoshoua sur la montagne<!--Mt. 5-7.-->

6:17	Et descendant avec eux, il s'arrêta sur une plaine avec une foule de ses disciples et une grande multitude du peuple de toute la Judée, et de Yeroushalaim, et de la contrée maritime de Tyr et de Sidon, qui étaient venus pour l'entendre et pour être guéris de leurs maladies,
6:18	et ceux qui étaient tourmentés par des esprits impurs, et ils étaient guéris.
6:19	Et toute la foule cherchait à le toucher, parce qu'une force sortait de lui et guérissait tout le monde.
6:20	Et lui, levant les yeux vers ses disciples, disait : Bénis êtes-vous, vous qui êtes pauvres, car le Royaume d'Elohîm est à vous !
6:21	Bénis êtes-vous, vous qui avez faim maintenant, car vous serez rassasiés ! Bénis êtes-vous, vous qui pleurez maintenant, parce que vous rirez !
6:22	Bénis êtes-vous quand les humains vous auront haïs, et quand ils vous auront chassés, et vous auront outragés, et qu’ils auront rejeté votre nom comme mauvais, à cause du Fils d'humain !
6:23	Réjouissez-vous en ce jour-là, et tressaillez d'allégresse, car voici que votre récompense sera grande dans le ciel. Car leurs pères en faisaient de même aux prophètes.
6:24	Mais malheur à vous riches, parce que vous avez votre consolation !
6:25	Malheur à vous qui êtes rassasiés, parce que vous aurez faim ! Malheur à vous qui riez maintenant, parce que vous vous lamenterez et vous pleurerez !
6:26	Malheur à vous quand tous les humains auront dit du bien de vous, car leurs pères en faisaient de même aux faux prophètes !
6:27	Mais à vous qui entendez, je vous dis : Aimez vos ennemis, faites du bien à ceux qui vous haïssent,
6:28	bénissez ceux qui vous maudissent, et priez en faveur de ceux qui vous accusent faussement.
6:29	À celui qui te frappe sur une joue, présente aussi l'autre. Et à celui qui prend ton manteau, ne refuse pas aussi ta tunique.
6:30	Mais donne à quiconque te demande, et ne réclame pas ton bien à celui qui s'en empare.
6:31	Et ce que vous voulez que les humains fassent pour vous, faites-le de même pour eux.
6:32	Et si vous aimez ceux qui vous aiment, quelle sorte de grâce est la votre ? Car les pécheurs aussi aiment ceux qui les aiment.
6:33	Et si vous faites du bien à ceux qui vous font du bien, quelle sorte de grâce est la votre ? Car les pécheurs aussi font de même.
6:34	Et si vous prêtez à ceux de qui vous espérez recevoir, quelle sorte de grâce est la votre ? Car les pécheurs aussi prêtent aux pécheurs, afin de recevoir la pareille.
6:35	Néanmoins, aimez vos ennemis, et faites du bien, et prêtez sans rien espérer, et votre récompense sera grande, et vous serez les fils du Très-Haut, car il est bon envers les ingrats et les méchants.
6:36	Devenez donc miséricordieux, comme aussi votre Père est miséricordieux.
6:37	Et ne jugez pas, et vous ne serez jamais jugés. Ne condamnez pas, et vous ne serez jamais condamnés. Acquittez et vous serez acquittés.
6:38	Donnez et il vous sera donné : on versera dans votre sein une bonne mesure, serrée, et secouée et qui déborde, car de la même mesure dont vous mesurez, il vous sera mesuré en retour.
6:39	Mais il leur disait aussi une parabole : Un aveugle peut-il conduire un aveugle ? Ne tomberont-ils pas tous deux dans une fosse ?
6:40	Le disciple n'est pas au-dessus de son docteur, mais entièrement équipé<!--Vient du grec « katartizo » qui signifie « redresser », « ajuster », « compléter ». Voir Ga. 6:1.-->, il sera comme son docteur.
6:41	Mais pourquoi regardes-tu la paille qui est dans l'œil de ton frère, et n'aperçois-tu pas la poutre qui est dans ton propre œil ?
6:42	Ou comment peux-tu dire à ton frère : Frère, laisse-moi enlever la paille qui est dans ton œil, toi qui ne vois pas la poutre qui est dans ton œil ? Hypocrite ! Ôte premièrement la poutre de ton œil, et alors tu verras clairement pour ôter la paille qui est dans l'œil de ton frère.
6:43	Car ce n'est pas un bon arbre qui porte un fruit pourri, ni un arbre pourri qui porte un bon fruit.
6:44	Car chaque arbre se reconnaît à son propre fruit. Car on ne cueille pas des figues sur des épines, et l'on ne vendange pas des raisins sur des ronces.
6:45	L'être humain bon tire de bonnes choses du bon trésor de son cœur, et l'être humain mauvais tire de mauvaises choses du mauvais trésor de son cœur. Car c'est de l'abondance du cœur que sa bouche parle.

### Parabole des deux maisons<!--Mt. 7:24-27.-->

6:46	Mais pourquoi m'appelez-vous Seigneur, Seigneur ! et ne faites-vous pas ce que je dis ?
6:47	Quiconque vient à moi, et écoute mes paroles et les met en pratique, je vous montrerai à qui il est semblable.
6:48	Il est semblable à un être humain qui bâtissant une maison, a creusé, creusé profondément et a mis le fondement sur le rocher. Mais une inondation est venue, et le fleuve s'est jeté contre cette maison, sans pouvoir l'ébranler, parce qu'elle était bâtie sur le rocher.
6:49	Mais celui qui entend et ne met pas en pratique est semblable à un être humain qui a bâti une maison sur la terre, sans fondement. Le fleuve s'est jeté contre elle, et immédiatement elle est tombée, et la ruine de cette maison a été grande.

## Chapitre 7

### Guérison de l'esclave d'un officier de l'armée romaine<!--Mt. 8:5-13.-->

7:1	Or après qu'il eut achevé de faire entendre toutes ses paroles au peuple, il entra dans Capernaüm.
7:2	Or, l'esclave d'un certain officier de l'armée romaine, à qui il était précieux, se trouvant malade, sur le point d'arriver à sa fin,
7:3	et ayant entendu parler de Yéhoshoua, il envoya vers lui des anciens des Juifs pour le prier de venir préserver du danger son esclave.
7:4	Et étant venu à Yéhoshoua, ils le supplièrent instamment, en disant : Il mérite que tu lui accordes cela,
7:5	car il aime notre nation et c'est lui qui a bâti notre synagogue.
7:6	Et Yéhoshoua s'en alla donc avec eux. Mais comme déjà il n'était plus loin de la maison, l'officier de l'armée romaine envoya des amis au-devant de lui pour lui dire : Seigneur, ne prends pas tant de peine, car je ne suis pas digne que tu entres sous mon toit.
7:7	C'est pourquoi aussi je ne me suis pas jugé digne d'aller vers toi. Mais dis une parole et mon serviteur sera guéri.
7:8	Car, moi-même je suis un homme placé sous autorité, ayant des soldats sous mes ordres, et je dis à l'un : Va ! et il va, et à un autre : Viens ! et il vient, et à mon esclave : Fais cela ! et il le fait.
7:9	Or ayant entendu ces choses, Yéhoshoua fut dans l'admiration pour lui et, se tournant vers la foule qui le suivait, il dit : Je vous le dis, je n'ai pas trouvé, même en Israël, une si grande foi.
7:10	Et quand ceux qui avaient été envoyés furent de retour à la maison, ils trouvèrent en bonne santé l'esclave qui avait été malade.

### Résurrection du fils de la veuve de Naïn

7:11	Et le lendemain, il arriva qu'il allait dans une ville appelée Naïn. Et beaucoup de ses disciples et une grande foule allaient avec lui.
7:12	Mais comme il approchait de la porte de la ville, voici, on portait en terre un mort, fils unique de sa mère, et elle était veuve. Et une assez grande foule de la ville était avec elle.
7:13	Et le Seigneur, l'ayant vue, fut ému de compassion pour elle, et il lui dit : Ne pleure pas !
7:14	Et s'étant approché, il toucha le cercueil. Et ceux qui le portaient s'arrêtèrent. Et il dit : Jeune homme, je te dis, réveille-toi !
7:15	Et le mort s'assit et se mit à parler. Et il le donna à sa mère.
7:16	Et ils furent tous saisis de crainte, et ils glorifiaient Elohîm, en disant : Un grand prophète a été suscité parmi nous et Elohîm a visité son peuple.
7:17	Et cette parole le concernant se répandit dans toute la Judée et dans toute la région environnante.

### Yohanan le Baptiste, le messager envoyé pour préparer la voie du Seigneur<!--Mt. 11:1-19.-->

7:18	Et les disciples de Yohanan lui rapportèrent toutes ces choses.
7:19	Et Yohanan, ayant appelé à lui deux de ses disciples, les envoya vers Yéhoshoua pour lui dire : Es-tu celui qui vient ou devons-nous en attendre un autre ?
7:20	Et étant venus vers lui, ces hommes dirent : Yohanan le Baptiste nous a envoyés vers toi pour te dire : Es-tu celui qui vient ou devons-nous en attendre un autre ?
7:21	Or en cette même heure, il guérit beaucoup de personnes de maladies, et de fléaux, et d'esprits malins, et il accorda à beaucoup d'aveugles la faveur de voir.
7:22	Et Yéhoshoua répondant, leur dit : Allez rapporter à Yohanan ce que vous avez vu et entendu : les aveugles recouvrent la vue, les boiteux marchent, les lépreux sont purifiés, les sourds entendent, les morts ressuscitent, l'Évangile est prêché aux pauvres.
7:23	Et béni est celui qui n'aura pas été scandalisé en moi !
7:24	Mais lorsque les messagers de Yohanan furent partis, il commença à parler aux foules au sujet de Yohanan : Qu'êtes-vous allés voir dans le désert ? Un roseau agité par le vent ?
7:25	Mais, qu'êtes-vous allés voir ? Un homme vêtu d'habits précieux ? Voici, ceux qui portent des habits magnifiques et qui vivent dans le luxe sont dans les maisons des rois.
7:26	Mais qu'êtes-vous donc allés voir ? Un prophète ? Oui, vous dis-je, et plus qu'un prophète.
7:27	C'est de lui qu'il est écrit : Voici, j'envoie mon messager devant ta face, et il préparera ta voie devant toi<!--Mal. 3:1.-->.
7:28	Car je vous le dis, parmi ceux qui sont nés de femmes, il n'y a aucun prophète plus grand que Yohanan le Baptiste. Cependant, le plus petit dans le Royaume d'Elohîm est plus grand que lui.
7:29	Et tout le peuple qui entendait cela, et les publicains justifiaient Elohîm, ayant été baptisés du baptême de Yohanan.
7:30	Mais les pharisiens et les docteurs de la torah, en ne se faisant pas baptiser par lui, ont rejeté le dessein d'Elohîm à leur égard.
7:31	Et le Seigneur dit : À qui donc comparerai-je les gens de cette génération, et à qui sont-ils semblables ?
7:32	Ils sont semblables à des enfants qui sont assis sur la place du marché, qui s'appellent les uns les autres pour dire : Nous vous avons joué de la flûte et vous n'avez pas dansé. Nous vous avons chanté des chants funèbres et vous n'avez pas pleuré.
7:33	Car Yohanan le Baptiste est venu ne mangeant pas de pain et ne buvant pas de vin, et vous dites : Il a un démon.
7:34	Le Fils d'humain est venu, mangeant et buvant, et vous dites : Voici un homme vorace et un buveur de vin, un ami des publicains et des pécheurs.
7:35	Et la sagesse a été justifiée par tous ses enfants.

### Une femme pécheresse pardonnée par Yéhoshoua

7:36	Or un des pharisiens le pria de manger avec lui. Et étant entré dans la maison du pharisien, il se mit à table.
7:37	Et voici une femme, qui dans la ville était une pécheresse, ayant su qu'il était à table dans la maison du pharisien, elle apporta un vase d'albâtre plein de baume.
7:38	Et se tenant derrière à ses pieds en pleurant, elle commença à les mouiller de ses larmes, et elle les essuyait avec les cheveux de sa tête, et embrassait tendrement ses pieds et les oignait avec ce baume.
7:39	Mais le pharisien qui l'avait invité, voyant cela, se dit en lui-même : Si celui-ci était prophète, il saurait qui et de quelle race est la femme qui le touche, car c'est une pécheresse.

### Parabole des deux débiteurs

7:40	Et Yéhoshoua répondant lui dit : Shim’ôn, j'ai quelque chose à te dire. Docteur, parle, dit-il.
7:41	Un certain créancier avait deux débiteurs : L'un lui devait 500 deniers et l'autre 50.
7:42	Mais n’ayant pas, eux, de quoi payer, il leur accorda à tous les deux une faveur. Dis-moi donc, lequel l'aimera le plus ?
7:43	Mais Shim’ôn répondant, lui dit : Celui, je pense, à qui il a accordé la plus grande faveur. Et il lui dit : Tu as droitement jugé.
7:44	Et se tournant vers la femme, il dit à Shim’ôn : Vois-tu cette femme ? Je suis entré dans ta maison, et tu ne m'as pas donné d'eau pour mes pieds, mais elle, elle les a mouillés de ses larmes et elle les a essuyés avec ses propres cheveux.
7:45	Tu ne m'as pas donné un baiser, mais elle, depuis que je suis entré, n'a cessé d'embrasser tendrement mes pieds.
7:46	Tu n'as pas oint ma tête d'huile, mais elle, elle a oint mes pieds avec un baume.
7:47	À cause de cela, je te le dis, ses nombreux péchés sont remis, car elle a beaucoup aimé. Or celui à qui on remet peu aime peu.
7:48	Et il dit à la femme : Tes péchés sont remis.
7:49	Et ceux qui étaient avec lui à table commençaient à se dire entre eux : Qui est celui-ci qui remet même les péchés ?
7:50	Mais il dit à la femme : Ta foi t'a sauvée. Va en paix.

## Chapitre 8

8:1	Et il arriva, dans la suite, qu’il allait de ville en village, prêchant et annonçant l'Évangile du Royaume d'Elohîm.
8:2	Et les douze disciples étaient auprès de lui avec quelques femmes aussi qu'il avait délivrées d'esprits malins et de maladies : Myriam, celle qu’on appelle Magdeleine, de laquelle étaient sortis sept démons,
8:3	et Yohana, femme de Chouzas, intendant d'Hérode, Shoushan et beaucoup d'autres qui le servaient de leurs biens.

### Parabole du semeur<!--Mt. 13:1-23 ; Mc. 4:1-20.-->

8:4	Or, une grande foule venant ensemble et allant vers lui de chaque ville, il parla à travers une parabole :
8:5	Celui qui sème sortit pour semer sa semence. Et en semant, il en tomba en effet à côté de la voie, et elle fut foulée aux pieds et les oiseaux du ciel la mangèrent.
8:6	Et une autre tomba sur le rocher<!--Rocher escarpé, terrain rocheux.--> et, ayant poussé, elle sécha parce qu'elle n'avait pas d'humidité.
8:7	Et une autre tomba au milieu des épines, et les épines poussèrent avec elle et l'étouffèrent.
8:8	Et une autre tomba dans la bonne terre et, ayant poussé, elle produisit du fruit au centuple. En disant ces choses, il dit à grande voix : Que celui qui a des oreilles pour entendre, entende !
8:9	Mais ses disciples l'interrogèrent, disant : Qu’est-ce que cette parabole ?
8:10	Mais il dit : Il vous a été donné de connaître les mystères du Royaume d'Elohîm, mais pour les autres, cela leur est dit en paraboles, afin qu'en voyant ils ne voient pas, et qu'en entendant ils ne comprennent pas.
8:11	Or voici ce qu’est la parabole : la semence, c'est la parole d'Elohîm.
8:12	Et ceux qui sont à côté de la voie, ce sont ceux qui entendent, ensuite le diable vient et enlève la parole de leur cœur, de peur qu'ils ne croient et ne soient sauvés.
8:13	Et ceux qui sont sur le rocher, ce sont ceux qui, lorsqu'ils entendent la parole, la reçoivent avec joie, mais ils n'ont pas de racine, ils croient pour un temps, mais au temps de l'épreuve ils se retirent.
8:14	Et ce qui est tombé parmi les épines, ce sont ceux qui ayant entendu et s'en allant, sont étouffés par les soucis et par la richesse, et par les plaisirs de la vie, et n'amènent rien à maturité<!--Amener à la perfection.-->.
8:15	Mais ce qui est dans la bonne terre, ce sont ceux qui ayant entendu la parole, la retiennent dans un cœur convenable et bon, et portent du fruit avec persévérance<!--« Constance, persévérance, endurance ». C'est le trait caractéristique d'une personne qui ne dévie pas de son but délibéré et de sa loyauté à la foi et la piété malgré les plus grandes épreuves et souffrances.-->.

### Parabole du chandelier<!--Mt. 5:15-16 ; Mc. 4:21-23 ; Lu. 11:33-36.-->

8:16	Mais personne, après avoir allumé une lampe, ne la couvre d'un vase, ni ne la met sous un lit, mais il la met sur un chandelier afin que ceux qui entrent voient la lumière.
8:17	Car il n'est rien de secret qui ne deviendra manifeste, rien de caché qui ne sera connu et ne viendra en évidence.
8:18	Discernez donc la manière dont vous écoutez, car on donnera à celui qui a, mais à celui qui n'a pas, on ôtera même ce qu'il pense avoir.

### La famille spirituelle<!--Mt. 12:46-50 ; Mc. 3:31-35.-->

8:19	Or sa mère et ses frères survinrent près de lui, mais ils ne pouvaient pas arriver jusqu'à lui à cause de la foule.
8:20	Et on le lui rapporta en disant : Ta mère et tes frères sont dehors, et ils désirent te voir.
8:21	Mais répondant, il leur dit : Ma mère et mes frères, ce sont ceux qui écoutent la parole d'Elohîm, et qui la pratiquent.

### Yéhoshoua calme la tempête<!--Mt. 8:23-27 ; Mc. 4:35-41.-->

8:22	Or il arriva l'un de ces jours, qu'il monta dans un bateau avec ses disciples et il leur dit : Passons de l'autre côté du lac ! Et ils firent voile.
8:23	Mais comme ils naviguaient, il s'endormit. Un tourbillon de vent descendit sur le lac, et ils se remplissaient complètement, et ils étaient en péril.
8:24	Et s'approchant, ils le réveillèrent, en disant : Maître, Maître ! Nous périssons ! Et lui, s'étant réveillé, réprimanda d'une manière tranchante le vent et les flots d'eau, et ils s'apaisèrent, et le calme revint.
8:25	Mais il leur dit : Où est votre foi ? Saisis de frayeur et d'étonnement, ils se dirent les uns aux autres : Qui est donc celui-ci, qui commande même aux vents et à l'eau, et ils lui obéissent ?

### Le démoniaque de Gadara délivré<!--Mt. 8:28-34 ; Mc. 5:1-20.-->

8:26	Et ils abordèrent dans le pays des Gadaréniens qui est vis-à-vis de la Galilée.
8:27	Mais, comme il sortait à terre, vint à sa rencontre un certain homme hors de la ville qui avait des démons depuis longtemps. Il ne portait pas de vêtements et ne demeurait pas dans une maison, mais dans les sépulcres.
8:28	Et voyant Yéhoshoua, il s’écria et tomba devant lui, et d’une grande voix il dit : Qu'y a-t-il entre moi et toi, Yéhoshoua, Fils d'Elohîm, du Très-Haut ? Je te prie, ne me tourmente pas.
8:29	Car il commandait à l'esprit impur de sortir de cet homme. Car il s'était emparé de lui depuis longtemps. Et on le gardait lié de chaînes et de fers. Mais, rompant les liens, il était entraîné par le démon dans les déserts.
8:30	Mais Yéhoshoua lui demanda, en disant : Quel est ton nom ? Et il dit : Légion<!--Voir commentaire en Mc. 5:9.-->. Car beaucoup de démons étaient entrés en lui.
8:31	Et ils le suppliaient de ne pas leur ordonner de s'en aller dans l'abîme<!--Voir Ap. 20:1-3.-->.
8:32	Mais il y avait là un grand troupeau de pourceaux paissant sur la montagne, et ils le suppliaient de leur permettre d'entrer en eux. Il le leur permit.
8:33	Et les démons étant sortis de cet homme, entrèrent dans les pourceaux, et le troupeau se précipita des pentes escarpées dans le lac et se noya.
8:34	Mais ceux qui les faisaient paître, voyant ce qui était arrivé, s'enfuirent et allèrent le raconter dans la ville et dans les campagnes.
8:35	Et ils sortirent pour voir ce qui était arrivé. Ils vinrent auprès de Yéhoshoua et ils trouvèrent l'homme de qui étaient sortis les démons assis aux pieds de Yéhoshoua, vêtu et dans son bon sens, et ils eurent peur.
8:36	Et ceux qui avaient vu ces choses leur racontèrent comment le démoniaque avait été sauvé.
8:37	Et toute la multitude de la région environnante des Gadaréniens le supplia de s’en aller de chez eux, parce qu'ils étaient saisis d'une grande crainte. Et lui, étant monté dans le bateau, s'en retourna.
8:38	Et l'homme de qui étaient sortis les démons le supplia de lui permettre d’être avec lui, mais Yéhoshoua le renvoya en disant :
8:39	Retourne dans ta maison et raconte quelles grandes choses l'Elohîm a faites pour toi. Et il s'en alla, proclamant par toute la ville quelles grandes choses Yéhoshoua avait faites pour lui.
8:40	Or il arriva, comme Yéhoshoua revenait, que la foule le reçut, car ils étaient tous à l'attendre.

### Résurrection de la fille de Yaïr (Jaïrus) et guérison de la femme à la perte de sang<!--Mt. 9:18-26 ; Mc. 5:21-43.-->

8:41	Et voici qu'arriva un homme du nom de Yaïr, qui était chef de la synagogue. Et, tombant aux pieds de Yéhoshoua, il le suppliait d'entrer dans sa maison,
8:42	parce qu'il avait une fille unique, âgée d'environ 12 ans, qui se mourait. Mais comme il y allait, les foules l'étouffaient.
8:43	Et une femme ayant une perte de sang depuis 12 ans, et qui, ayant dépensé tout son bien pour les médecins, n'avait pu être guérie par aucun,
8:44	s'approcha par derrière et toucha le bord de son vêtement. Immédiatement la perte de sang s'arrêta.
8:45	Et Yéhoshoua dit : Qui m'a touché ? Comme tous le niaient, Petros et ceux qui étaient avec lui, dirent : Maître, la foule qui t'entoure te presse, et tu dis : Qui m'a touché ?
8:46	Mais Yéhoshoua dit : Quelqu'un m'a touché, car je sais qu'une force est sortie de moi.
8:47	Et la femme, voyant qu'elle n'était pas restée cachée, vint en tremblant et, tombant devant lui, lui déclara devant tout le peuple pour quelle raison elle l'avait touché, et comment elle avait été guérie instantanément.
8:48	Mais il lui dit : Prends courage, ma fille. Ta foi t'a sauvée. Va en paix.
8:49	Et tandis qu'il parlait encore, quelqu'un vient de chez le chef de la synagogue et lui dit : Ta fille est morte, n’importune pas le Docteur.
8:50	Mais Yéhoshoua ayant entendu cela, lui répondit en disant : N'aie pas peur, crois seulement, et elle sera sauvée.
8:51	Or, étant entré dans la maison, il ne permit à personne d’entrer avec lui, excepté Petros, et Yaacov et Yohanan, et le père et la mère de la fille.
8:52	Or tous pleuraient et se frappaient la poitrine de chagrin pour elle. Mais il leur dit : Ne pleurez pas, elle n'est pas morte, mais elle dort.
8:53	Et ils le tournaient en dérision, sachant bien qu'elle était morte.
8:54	Mais lui, les ayant tous mis dehors, et lui prenant la main, dit à grande voix : Enfant, réveille-toi !
8:55	Et son esprit revint et elle se leva immédiatement. Et il ordonna qu'on lui donne à manger.
8:56	Et les parents de la fille furent dans l'étonnement, et il leur ordonna de ne dire à personne ce qui était arrivé.

## Chapitre 9

### Mission des douze apôtres<!--Mt. 10:1-42 ; Mc. 6:7-13.-->

9:1	Mais ayant convoqué les douze disciples, il leur donna puissance et autorité sur tous les démons, et pour guérir les maladies.
9:2	Et il les envoya prêcher le Royaume d'Elohîm et guérir les malades.
9:3	Et il leur dit : Ne prenez rien pour la route, ni bâton, ni sac, ni pain, ni argent, et n'ayez pas deux tuniques chacun.
9:4	Et dans quelque maison que vous entriez, demeurez-y et partez de là.
9:5	Et quant à ceux qui ne vous recevront pas, en sortant de cette ville, secouez la poussière de vos pieds en témoignage contre eux.
9:6	Et étant sortis, ils allaient de village en village, prêchant l’Evangile et opérant des guérisons partout.
9:7	Or Hérode, le tétrarque, entendit parler de toutes les choses qui sont faites par lui, et il était en perplexité, parce que quelques-uns disent que Yohanan était réveillé des morts,
9:8	mais d’autres, qu'Éliyah était apparu, mais d’autres encore, qu'un des anciens prophètes s'était relevé.
9:9	Mais Hérode dit : J'ai décapité Yohanan. Qui est donc celui-ci de qui j'entends dire de telles choses ? Et il cherchait à le voir.
9:10	Et les apôtres étant de retour lui racontèrent toutes les choses qu'ils avaient faites. Et les prenant alors avec lui, il se retira dans un lieu désert d'une ville appelée Bethsaïda.
9:11	Mais l'ayant su, les foules le suivirent. Et les ayant accueillies, il leur parlait du Royaume d'Elohîm et guérissait ceux qui avaient besoin de guérison.

### Multiplication des pains pour 5 000 hommes<!--Mt. 14:15-21 ; Mc. 6:32-44 ; Jn. 6:1-14.-->

9:12	Mais comme le jour commençait à baisser, les douze disciples s'approchèrent et lui dirent : Renvoie la foule, afin qu'ils aillent dans les villages et dans les campagnes des environs pour se loger et pour trouver à manger, car nous sommes ici dans un pays désert.
9:13	Et il leur dit : Donnez-leur vous-mêmes à manger ! Et ils dirent : Nous n'avons pas plus de cinq pains et de deux poissons, à moins que nous n'allions acheter des vivres pour tout ce peuple.
9:14	Car ils étaient environ 5 000 hommes. Mais il dit aux disciples : Faites-les asseoir par rangées de 50 chacune.
9:15	Et ils le firent ainsi et les firent tous asseoir.
9:16	Et, prenant les cinq pains et les deux poissons, levant les yeux au ciel, il prononça la prière de bénédiction. Et il les rompit et les donna aux disciples afin qu'ils les mettent devant la foule.
9:17	Et ils mangèrent et furent tous rassasiés, et l'on emporta le surplus des morceaux, douze paniers.

### Petros (Pierre) reconnaît Yéhoshoua (Jésus) comme étant le Mashiah (Christ)<!--Mt. 16:13-16 ; Mc. 8:27-30 ; Jn. 6:66-71.-->

9:18	Or il arriva que, comme il était dans un lieu retiré pour prier et que les disciples étaient avec lui, il les interrogea, disant : Qui suis-je aux dires des foules ?
9:19	Et répondant, ils dirent : Yohanan le Baptiste ; et d'autres, Éliyah ; et d'autres encore, qu'un des anciens prophètes est réveillé.
9:20	Mais il leur dit : Et vous, qui dites-vous que je suis ? Et Petros répondit en disant : Le Mashiah d'Elohîm.
9:21	Il les réprimanda d'une manière tranchante et leur ordonna de ne parler de cela à personne.

### Yéhoshoua annonce sa mort et sa résurrection<!--Mt. 16:21-23 ; Mc. 8:31-33.-->

9:22	Et il dit : Il faut que le Fils d'humain souffre beaucoup, et qu'il soit rejeté par les anciens, et par les principaux prêtres, et par les scribes, qu'il soit mis à mort et soit réveillé le troisième jour.

### Le renoncement à soi-même<!--Mt. 16:24-28 ; Mc. 8:34-38.-->

9:23	Mais il disait à tous : Si quelqu'un veut venir après moi, qu'il renonce à lui-même, qu'il se charge chaque jour de sa croix et qu'il me suive.
9:24	Car celui qui voudra sauver son âme la perdra, mais celui qui perdra son âme à cause de moi la sauvera.
9:25	Car que profite un être humain en gagnant tout le monde, mais en se détruisant lui-même ou en subissant un dommage ?
9:26	Car quiconque aura honte de moi et de mes paroles, le Fils d'humain aura honte de lui quand il sera venu dans sa gloire et celle du Père et des saints anges.
9:27	Et je vous le dis en vérité, quelques-uns de ceux qui sont ici présents ne goûteront jamais la mort, non, jusqu'à ce qu'ils aient vu le Royaume d'Elohîm<!--Voir commentaire en Mt. 16:28.-->.

### La transfiguration<!--Mt. 17:1-8 ; Mc. 9:1-8.-->

9:28	Or il arriva, environ huit jours après ces paroles, qu'il prit avec lui Petros, Yohanan, et Yaacov, et qu'il monta sur une montagne pour prier.
9:29	Et il arriva qu'en priant, l'aspect de son visage changea, et son habit devint blanc et resplendissant comme un éclair.
9:30	Et voici, deux hommes, Moshé et Éliyah, parlaient avec lui,
9:31	apparaissant dans la gloire, ils parlaient de son départ<!--Départ : du grec « exodos » qui signifie « sortie », « décès », « destin final », « fin de carrière ». Yéhoshoua (Jésus) est le prophète de l'Exode dont Moshé (Moïse) a parlé dans De. 18:15.--> qu'il était sur le point d'accomplir à Yeroushalaim.
9:32	Or Petros et ceux qui étaient avec lui étaient accablés de sommeil mais, restant éveillés, ils virent sa gloire et les deux hommes qui se tenaient avec lui.
9:33	Et il arriva, comme ceux-ci se séparaient de lui, que Petros dit à Yéhoshoua : Maître, il est bon que nous soyons ici. Et faisons trois tabernacles : un pour toi, et un pour Moshé, et un pour Éliyah. Il ne savait pas ce qu'il disait.
9:34	Mais pendant qu'il disait ces choses, une nuée vint les couvrir de son ombre. Et ils eurent peur comme ils entraient dans la nuée.
9:35	Et une voix vint de la nuée, disant : Celui-ci est mon Fils bien-aimé : écoutez-le !
9:36	Et quand la voix vint, Yéhoshoua se trouva seul. Et ils gardèrent le silence, et ne rapportèrent rien à personne en ces jours-là de ce qu'ils avaient vu.

### Une génération incrédule et perverse

9:37	Or il arriva le jour suivant, lorsqu'ils furent descendus de la montagne, qu'une grande foule vint à sa rencontre.
9:38	Et voici, du milieu de la foule, un homme s'écria en disant : Docteur, je t'en prie, tourne les yeux vers mon fils, car c'est mon fils unique.
9:39	Et voilà qu'un esprit le saisit, et soudain il pousse des cris, il le fait se convulser et écumer, et il se retire de lui avec difficulté après l'avoir brisé.
9:40	Et j'ai supplié tes disciples de le chasser, mais ils n'ont pas pu.
9:41	Mais Yéhoshoua répondit en disant : Ô génération incrédule et déformée, jusqu'à quand serai-je avec vous et vous supporterai-je ? Amène ici ton fils !
9:42	Mais en s'approchant, le démon le déchira et l’agita de convulsions. Mais Yéhoshoua réprimanda d'une manière tranchante l'esprit impur, et guérit l'enfant, et le rendit à son père.
9:43	Et tous furent choqués à cause de la grandeur d'Elohîm. Mais comme ils étaient tous dans l'admiration de tout ce que Yéhoshoua faisait, il dit à ses disciples :
9:44	Pour vous, mettez bien ces paroles dans vos oreilles, car le Fils d'humain est sur le point d'être livré entre les mains des hommes.
9:45	Mais ils ne comprirent pas cette parole. Et elle était voilée pour eux, afin qu'ils n'en aient pas le sens. Et ils craignaient de l'interroger à ce sujet.

### L'humilité, le secret de la véritable grandeur<!--Mt. 18:1-6 ; Mc. 9:33-37.-->

9:46	Et il survint entre eux une pensée : lequel d'entre eux était le plus grand.
9:47	Mais Yéhoshoua, voyant la pensée de leur cœur, prit un enfant et le mit auprès de lui.
9:48	Et il leur dit : Quiconque reçoit cet enfant en mon nom me reçoit, et quiconque me reçoit, reçoit celui qui m'a envoyé. Car celui qui est le plus petit d'entre vous tous, c'est celui-là qui est grand.

### Yéhoshoua condamne le sectarisme de Yaacov et Yohanan (Jacques et Jean)<!--Mc. 9:38-40.-->

9:49	Et Yohanan, répondant, dit : Maître, nous avons vu quelqu'un qui chasse les démons en ton nom et nous l'en avons empêché, parce qu'il ne suit pas avec nous.
9:50	Mais Yéhoshoua lui dit : Ne l'en empêchez pas, car celui qui n'est pas contre nous est en notre faveur.

### La mission de Yéhoshoua : Sauver les âmes

9:51	Or il arriva, comme les jours où il devait être enlevé s'accomplissaient, qu'il affermit sa face pour aller à Yeroushalaim.
9:52	Et il envoya devant lui des messagers, qui se mirent en route, et entrèrent dans un village des Samaritains, afin de préparer pour lui.
9:53	Mais ils ne le reçurent pas, parce que sa face était dirigée vers Yeroushalaim.
9:54	Et voyant cela, les disciples Yaacov et Yohanan dirent : Seigneur, veux-tu que nous disions que le feu descende du ciel et les consume comme l'a fait aussi Éliyah ?
9:55	Mais se tournant, il les réprimanda d'une manière tranchante, et disant : Vous ne savez pas de quel esprit vous êtes ! 
9:56	Car le Fils d'humain n'est pas venu pour perdre les âmes des hommes, mais pour les sauver. Ainsi, ils allèrent dans un autre village.

### Le prix à payer pour suivre Yéhoshoua<!--Mt. 8:19-22.-->

9:57	Et en allant par la route, il arriva que quelqu'un lui dit : Seigneur, je te suivrai où que tu ailles.
9:58	Et Yéhoshoua lui dit : Les renards ont des tanières, et les oiseaux du ciel ont des nids, mais le Fils d'humain n'a pas où reposer sa tête.
9:59	Et il dit à un autre : Suis-moi ! Mais il dit : Seigneur, permets-moi d'aller d'abord ensevelir mon père.
9:60	Mais Yéhoshoua lui dit : Laisse les morts ensevelir leurs morts mais toi, va, et annonce le Royaume d'Elohîm !
9:61	Et un autre aussi dit : Seigneur, je te suivrai, mais permets-moi de prendre d'abord congé de ceux de ma maison.
9:62	Mais Yéhoshoua lui répondit : Aucun homme qui a mis la main à la charrue et qui regarde en arrière, n'est utile au Royaume d'Elohîm.

## Chapitre 10

### 70 disciples envoyés en mission

10:1	Or après ces choses, le Seigneur en désigna aussi 70 autres et il les envoya deux à deux devant sa face dans toutes les villes et dans tous les lieux où lui-même était sur le point d'aller.
10:2	Il leur disait donc : En effet, la moisson est grande, mais il y a peu d'ouvriers. Priez donc le Maître de la moisson d'envoyer des ouvriers dans sa moisson.
10:3	Allez ! Voici, je vous envoie comme des agneaux au milieu des loups.
10:4	Ne portez ni bourse, ni sac, ni sandales et ne saluez personne en route.
10:5	En toute maison où vous entrez, dites premièrement : Shalôm à cette maison !
10:6	Et s’il y a là un fils de shalôm, votre shalôm reposera en effet sur lui, sinon, il retournera à vous.
10:7	Et demeurez dans cette maison, mangeant et buvant ce qu’on vous donnera, car l'ouvrier est digne de son salaire. N'allez pas de maison en maison.
10:8	Et dans toute ville où vous entrez et où ils vous reçoivent, mangez ce qui sera mis devant vous,
10:9	et guérissez les malades qui s'y trouveront, et dites-leur : Le Royaume d'Elohîm s'est approché de vous !
10:10	Mais dans toute ville où vous entrez et où ils ne vous reçoivent pas, sortez dans ses rues, et dites :
10:11	Nous secouons contre vous-mêmes la poussière de votre ville qui s'est attachée à nous. Toutefois, sachez que le Royaume d'Elohîm s'est approché de vous !
10:12	Mais je vous dis qu'en ce jour-là, le sort de Sodome sera plus supportable que celui de cette ville-là.

### Yéhoshoua dénonce les indifférents<!--Mt. 11:20-24.-->

10:13	Malheur à toi, Chorazin ! Malheur à toi, Bethsaïda ! Car si les miracles qui ont été faits au milieu de vous avaient été faits dans Tyr et dans Sidon, il y a longtemps qu'elles se seraient repenties, couvertes d'un sac et assises sur la cendre.
10:14	C'est pourquoi le sort de Tyr et de Sidon sera plus supportable au Jugement que le vôtre.
10:15	Et toi, Capernaüm, qui as été élevée jusqu'au ciel, on te fera descendre jusqu'à l'Hadès<!--Voir commentaire en Mt. 16:18.-->.
10:16	Celui qui vous écoute, m'écoute, et celui qui vous rejette me rejette. Or celui qui me rejette, rejette celui qui m'a envoyé.
10:17	Or les 70 revinrent avec joie, disant : Seigneur, même les démons nous sont soumis en ton Nom.
10:18	Mais il leur dit : Je voyais Satan tombant du ciel comme un éclair.
10:19	Voici, je vous donne l'autorité de fouler aux pieds<!--Piétiner, écraser avec les pieds ; avancer en posant le pied, marcher sur : vaincre heureusement les plus grands périls des machinations et persécutions que Satan mettrait volontiers pour contrarier la prédication de l'Évangile ; fouler aux pieds, piétiner, traiter avec insulte et mépris : profaner la cité sainte par dévastation et outrage.--> serpents et scorpions, et toute la force de l'ennemi et rien ne vous fera du mal en aucune façon.
10:20	Toutefois, ne vous réjouissez pas de ce que les esprits vous sont soumis, mais réjouissez-vous plutôt de ce que vos noms sont écrits dans les cieux.
10:21	En cette heure même, Yéhoshoua exulte en esprit, et dit : Je te célèbre, Père, Seigneur du ciel et de la Terre, de ce que tu as caché ces choses aux sages et aux intelligents, et que tu les as révélées aux enfants. Oui, Père, parce que devant toi, telle a été ta volonté.
10:22	Et se tournant vers les disciples, il dit : Toutes choses m'ont été livrées par mon Père, et personne ne connaît qui est le Fils, excepté le Père, ni qui est le Père, excepté le Fils et celui à qui le Fils veut le révéler.
10:23	Et se tournant vers ses disciples, il leur dit en particulier : Bénis sont les yeux qui voient ce que vous voyez !
10:24	Car je vous dis que beaucoup de prophètes et de rois ont désiré voir ce que vous voyez, et ne l'ont pas vu, et entendre ce que vous entendez, et ne l'ont pas entendu.

### Un docteur de la torah tente d'éprouver Yéhoshoua<!--Mt. 22:34-40 ; Mc. 12:28-34.-->

10:25	Et voici, un certain docteur de la torah s'étant levé pour l'éprouver, lui dit : Docteur, que dois-je faire pour avoir la vie éternelle ?
10:26	Et il lui dit : Qu'est-il écrit dans la torah ? Comment lis-tu ?
10:27	Et répondant, il dit : Tu aimeras le Seigneur, ton Elohîm, de tout ton cœur, et de toute ton âme, et de toute ta force, et de toute ta pensée, et ton prochain comme toi-même<!--De. 6:4-5 et Lé. 19:18.-->.
10:28	Et il lui dit : Tu as bien répondu. Fais cela, et tu vivras.
10:29	Mais lui, voulant se justifier lui-même, dit à Yéhoshoua : Et qui est mon prochain ?

### Parabole du Samaritain

10:30	Mais Yéhoshoua répondant, dit : Un homme descendait de Yeroushalaim à Yeriycho. Il tomba au milieu des brigands qui, l'ayant dépouillé et couvert de blessures, s'en allèrent, le laissant à demi mort.
10:31	Or, un certain prêtre, qui par hasard descendait par la même route et, ayant vu cet homme, passa du côté opposé.
10:32	Et de même un Lévite arriva à cet endroit, il le vit et passa du côté opposé.
10:33	Mais un certain Samaritain, qui voyageait, étant venu là, fut ému de compassion lorsqu'il le vit.
10:34	Et il s'approcha, et banda ses plaies en y versant de l'huile et du vin et, l'ayant fait monter sur sa propre bête, il le conduisit dans une auberge et prit soin de lui.
10:35	Et le lendemain, en partant il tira de sa bourse deux deniers, et les donna à l'hôte, en lui disant : Aie soin de lui, et tout ce que tu dépenseras de plus, je te le rendrai à mon retour.
10:36	Lequel donc de ces trois te semble avoir été le prochain de celui qui était tombé au milieu des brigands ?
10:37	Et il dit : C'est celui qui a usé de miséricorde envers lui. Yéhoshoua donc lui dit : Va, et toi aussi, fais de même.

### Martha et Myriam

10:38	Et en s'en allant, il arriva qu'il entra dans un village, et une certaine femme du nom de Martha, le reçut dans sa maison.
10:39	Et elle avait une sœur appelée Myriam, et qui, s'étant assise aux pieds de Yéhoshoua, écoutait sa parole.
10:40	Mais Martha était distraite par beaucoup de soucis du service. Étant survenue, elle dit : Seigneur, ne te soucies-tu pas de ce que ma sœur m'a laissée servir toute seule ? Dis-lui donc de m'aider !
10:41	Mais répondant, Yéhoshoua lui dit : Martha, Martha, tu t'inquiètes et tu t'agites pour beaucoup de choses.
10:42	Mais une seule est nécessaire. Et Myriam a choisi la bonne part, qui ne lui sera pas ôtée.

## Chapitre 11

### Enseignement de Yéhoshoua sur la prière<!--Mt. 6:9-15.-->

11:1	Et il arriva, comme il était en prière en un certain lieu, que l'un de ses disciples lui dit, dès qu'il eut cessé : Seigneur, enseigne-nous à prier, comme Yohanan l'a enseigné à ses disciples.
11:2	Et il leur dit : Quand vous priez, dites : Notre Père qui es dans les cieux ! Que ton Nom soit sanctifié, que ton royaume vienne, que ta volonté soit faite, comme dans le ciel, aussi sur la Terre.
11:3	Donne-nous selon le jour, notre pain qui nous suffit chaque jour<!--Le mot a comme sens premier : la survie physique demandée à YHWH, jour après jour. Soit « le pain qui nous est nécessaire » ou « le pain qui nous suffit chaque jour ». Voir également Ex. 16:16-22 ; Mt. 6:11,34.-->.
11:4	Et remets-nous nos péchés, car nous-mêmes aussi nous remettons à chacun qui nous doit. Et ne nous fais pas entrer dans la tentation, mais délivre-nous du mauvais.

### Parabole des trois amis et de la prière importune

11:5	Et il leur dit : Qui d'entre vous aura un ami et ira chez lui à minuit pour lui dire : Ami, prête-moi trois pains,
11:6	car mon ami est arrivé de voyage chez moi, et je n'ai rien à lui offrir,
11:7	et que celui-ci répondant de l’intérieur dise : Ne m'importune pas, la porte est déjà fermée, et mes enfants sont au lit avec moi, je ne puis me lever pour t'en donner.
11:8	Je vous le dis, même s'il ne se réveille pas pour lui donner parce que c'est son ami, il se réveillera à cause de son impudence et lui donnera tout ce dont il a besoin.
11:9	Ainsi je vous dis : Demandez, et il vous sera donné. Cherchez, et vous trouverez. Frappez, et l'on vous ouvrira.
11:10	Car quiconque demande reçoit, et celui qui cherche trouve, et l’on ouvrira à celui qui frappe.

### Parabole du père

11:11	Mais quel est parmi vous le père à qui son fils demandera un pain ? Il ne lui donnera pas une pierre. Et s'il demande un poisson, il ne lui donnera pas un serpent à la place d'un poisson !
11:12	Ou aussi, s'il demande un œuf, il ne lui donnera pas un scorpion !
11:13	Si donc, mauvais comme vous l'êtes, vous savez donner des dons qui sont bons à vos enfants, à combien plus forte raison le Père depuis le ciel donnera-t-il l'Esprit Saint à ceux qui le lui demandent.

### Yéhoshoua guérit un démoniaque

11:14	Et il chassa un démon qui était muet. Et il arriva que le démon étant sorti, le muet parla et la foule fut dans l'admiration.

### Le blasphème contre le Saint-Esprit<!--Mt. 12:24-32 ; Mc. 3:22-30.-->

11:15	Mais quelques-uns d'entre eux dirent : C'est par Béelzéboul<!--Béelzéboul, chef des démons, est connu dans le Tanakh sous le nom de Baal-Zeboub, dieu d'Ékron (2 R. 1:2-16). Son nom signifie « Seigneur des mouches ».-->, le chef des démons, qu'il chasse les démons.
11:16	Mais les autres, pour l'éprouver, lui demandaient un signe venant du ciel.
11:17	Mais lui, connaissant leurs pensées, leur dit : Tout royaume divisé contre lui-même sera dévasté. Et maison, contre maison, tombe.
11:18	Mais si Satan aussi est divisé contre lui-même, comment son royaume tiendra-t-il debout, puisque vous dites que je chasse les démons par Béelzéboul ?
11:19	Mais si moi, je chasse les démons par Béelzéboul, vos fils, par qui les chassent-ils ? À cause de cela, ils seront eux-mêmes vos juges !
11:20	Mais si je chasse les démons par le doigt d'Elohîm, alors le Royaume d'Elohîm est parvenu jusqu'à vous.
11:21	Quand un homme fort et fourni en armes garde sa bergerie<!--Chez les Grecs, du temps d'Homère, la bergerie était un espace découvert autour de la maison, fermé par un mur, tandis que chez les orientaux, il s'agissait d'un espace dans la campagne, entouré d'un mur, où les troupeaux passaient la nuit. La bergerie désigne aussi la partie non couverte d'une maison. Dans la première alliance, il s'agit particulièrement du « parvis » du tabernacle et du temple à Yeroushalaim (Jérusalem). Les demeures des gens de la haute société possédaient généralement deux de ces « cours » : une entre la porte et la rue, l'autre entourant l'immeuble lui-même. C'est ce qui est mentionné en Mt. 26:69. Enfin, ce terme fait allusion à la maison elle-même, un palais.-->, ce qu'il possède est en paix.
11:22	Mais quand un plus fort que lui, étant survenu, l'a vaincu, il lui enlève toute son armure<!--Vient du grec « panoplia » qui signifie « l'armement complet avec bouclier, épée, lance, casque, jambières et pectoral ». Voir Ep. 6:11 et 13.--> dans laquelle il se confiait, et il partage ses dépouilles.
11:23	Celui qui n'est pas avec moi est contre moi et celui qui n'assemble pas avec moi disperse.

### Le retour de l'esprit impur<!--Mt. 12:43-45.-->

11:24	Lorsque l'esprit impur est sorti de l’être humain, il passe par des lieux sans eau<!--Voir 2 Pi. 2:17 ; Jud. 1:12.-->, cherchant du repos. N'en trouvant pas, il dit : Je retournerai dans ma maison, d'où je suis sorti.
11:25	Et, en arrivant, il la trouve balayée et ornée.
11:26	Alors il s'en va et prend avec lui, sept autres esprits plus méchants que lui, et ils y entrent et s'y établissent, de sorte que la dernière condition de cet être humain est pire que la première.
11:27	Or il arriva comme il disait ces choses, qu'une femme élevant sa voix du milieu de la foule, lui dit : Béni est le ventre qui t'a porté, et les mamelles que tu as tétées !
11:28	Mais il dit : Bénis sont plutôt ceux qui écoutent la parole d'Elohîm et qui la gardent !

### Le miracle de Yonah (Jonas), le prophète<!--Mt. 12:38-41.-->

11:29	Et comme les foules se rassemblaient ensemble, il commença à dire : Cette génération est méchante. Elle demande un signe, mais il ne lui sera pas donné de signe, excepté le signe de Yonah, le prophète.
11:30	Car, de même que Yonah devint un signe pour les Ninivites, de même le Fils d'humain en sera un pour cette génération.
11:31	La reine du midi se réveillera au jugement contre les hommes de cette génération et les condamnera, parce qu'elle vint des extrémités de la Terre pour entendre la sagesse de Shelomoh, et voici, il y a ici plus que Shelomoh.
11:32	Les hommes de Ninive se relèveront au jugement contre cette génération et la condamneront, parce qu'ils se sont repentis à la prédication de Yonah, et voici, il y a ici plus que Yonah.

### Parabole de la lampe<!--Mt. 5:14-16 ; Mc. 4:21-23 ; Lu. 8:16-18.-->

11:33	Personne, après avoir allumé une lampe, ne la met dans un lieu caché ou sous le boisseau<!--Mesure de volume pour matière sèche d'environ 9 litres.-->, mais sur le chandelier, afin que ceux qui entrent voient la lumière.
11:34	L'œil est la lampe du corps. Lorsque donc ton œil est simple, tout ton corps aussi sera lumineux. Mais lorsqu'il est mauvais, ton corps sera dans l'obscurité.
11:35	Prends donc garde que la lumière qui est en toi ne soit ténèbre.
11:36	Si donc tout ton corps est lumineux, n'ayant aucune partie dans l'obscurité, il sera entièrement lumineux, comme lorsque la lampe t'éclaire de sa lumière.

### Yéhoshoua censure les pharisiens et les docteurs de la torah<!--Cp. Mt. 12:38-41.-->

11:37	Et comme il parlait, un pharisien le pria de dîner chez lui. Il entra et se mit à table.
11:38	Mais le pharisien vit avec étonnement qu'il ne s'était pas premièrement baptisé<!--Vient du grec « baptizo » qui signifie « plonger », « immerger », « submerger », « purifier en plongeant ou submergeant », « laver », « rendre pur avec de l'eau », « se laver », « se baigner ». Le mot « baptizo » ne doit pas être confondu avec « bapto ». L'exemple le plus simple a été donné par Nicander, un physicien et poète grec, vers 200 av. J.-C. Il parle de la recette pour faire des conserves au vinaigre, et pour ceci utilise les deux mots grecs visés ci-avant : le légume doit d'abord être plongé (« bapto ») dans de l'eau bouillante, puis immergé (« baptizo ») dans le vinaigre. Les deux verbes concernent une immersion, mais la première « bapto » est temporaire et brève, la seconde « baptizo » est une action de longue durée, considérée comme définitive.--> avant le dîner.
11:39	Mais le Seigneur lui dit : Maintenant, vous, pharisiens, vous nettoyez le dehors de la coupe et du plat, mais votre intérieur est rempli de pillage et de méchanceté.
11:40	Insensés ! Celui qui a fait le dehors, n'a-t-il pas fait aussi le dedans ?
11:41	Donnez plutôt en aumône ce qui est dedans, et voici, toutes choses sont pures pour vous.
11:42	Mais malheur à vous, pharisiens, parce que vous payez la dîme de la menthe, de la rue<!--Il s'agit d'un arbuste ayant des propriétés médicinales. Les pharisiens poussaient leur zèle jusqu'à payer la dîme sur certaines herbes. Toutefois, en négligeant la justice et l'amour d'Elohîm, ils passaient à côté de l'essentiel. Toutes leurs œuvres étaient par conséquent vaines.-->, et de chaque légume et que vous négligez la justice et l'amour d'Elohîm. C'est là ce qu'il fallait pratiquer, sans négliger les autres choses.
11:43	Malheur à vous, pharisiens ! Parce que vous aimez le premier siège dans les synagogues et les salutations sur les places du marché !
11:44	Malheur à vous, scribes et pharisiens hypocrites, parce que vous êtes comme les sépulcres qui ne paraissent pas, et sur lesquels les gens marchent sans le voir.
11:45	Mais un des docteurs de la torah répondit et lui dit : Docteur, en disant ces choses, tu nous outrages, nous aussi !
11:46	Et il dit : Malheur à vous aussi, docteurs de la torah ! Car vous chargez les gens de fardeaux difficiles à porter, et vous-mêmes vous ne touchez pas ces fardeaux d'un seul de vos doigts.
11:47	Malheur à vous, parce que vous bâtissez les sépulcres des prophètes, que vos pères ont tués.
11:48	Vous rendez donc témoignage aux œuvres de vos pères, et vous y prenez plaisir, car eux, ils les ont vraiment tués, et vous, vous bâtissez leurs sépulcres.
11:49	À cause de cela, la sagesse d'Elohîm a dit : Je leur enverrai des prophètes et des apôtres, et ils en tueront, et ils en persécuteront,
11:50	afin que le sang de tous les prophètes qui a été répandu depuis la fondation du monde soit redemandé à cette génération,
11:51	depuis le sang d'Abel jusqu'au sang de Zekaryah, qui fut tué entre l'autel et la maison. Oui, je vous le dis qu'il sera redemandé à cette génération.
11:52	Malheur à vous docteurs de la torah ! Parce que vous avez enlevé la clé de la connaissance ! Vous n'êtes pas entrés vous-mêmes, et ceux qui entrent, vous les empêchez !
11:53	Et comme il leur disait ces choses, les scribes et les pharisiens commencèrent à le presser violemment, et à le faire parler sur beaucoup de choses,
11:54	lui tendant des pièges et cherchant à attraper par artifice quelque chose sortie de sa bouche, afin de l'accuser.

## Chapitre 12

### Enseignements divers de Yéhoshoua<!--Mt. 16:6-12 ; Mc. 8:14-21.-->

12:1	Sur quoi, la foule s'étant rassemblée par milliers, au point de marcher les uns sur les autres, il se mit à dire à ses disciples : En premier lieu, gardez-vous du levain des pharisiens, qui est l'hypocrisie.
12:2	Mais il n'y a rien de caché entièrement qui ne sera révélé, rien de secret qui ne sera connu.
12:3	C'est pourquoi tout ce que vous aurez dit dans la ténèbre sera entendu dans la lumière et ce que vous aurez dit à l'oreille dans les chambres sera prêché sur les toits.
12:4	Mais je vous le dis à vous mes amis : Ne craignez pas ceux qui tuent le corps, et qui après cela ne peuvent rien faire de plus.
12:5	Mais je vous montrerai qui vous devez craindre. Craignez celui qui, après avoir tué, a le pouvoir de jeter dans la géhenne. Oui, vous dis-je, craignez celui-là.
12:6	Ne vend-on pas cinq petits passereaux pour deux assarius<!--Voir commentaire en Mt. 10:29.--> ? Cependant, aucun d'eux n'est oublié devant Elohîm.
12:7	Et les cheveux même de votre tête sont tous comptés. Ne craignez donc pas : vous valez plus que beaucoup de passereaux.
12:8	Mais je vous le dis, quiconque m'aura confessé devant les humains, le Fils d'humain le confessera aussi devant les anges d'Elohîm.
12:9	Mais quiconque m'aura renié devant les humains sera renié devant les anges d'Elohîm.
12:10	Et quiconque dira une parole contre le Fils d'humain, cela lui sera remis, mais à qui aura blasphémé<!--Voir le dictionnaire en annexe.--> contre le Saint Esprit, cela ne sera pas remis.
12:11	Mais quand ils vous mèneront devant les synagogues, les magistrats et les autorités, ne vous inquiétez pas de la manière dont vous vous défendrez ni de ce que vous aurez à dire,
12:12	car le Saint-Esprit vous enseignera à l'heure même ce qu'il faut dire.

### Parabole du riche insensé

12:13	Et quelqu'un de la foule lui dit : Docteur, dis à mon frère qu'il partage avec moi l'héritage !
12:14	Mais il lui dit : Homme ! Qui m'a établi juge ou faiseur de partages sur vous ?
12:15	Mais il leur dit : Faites attention ! Gardez-vous de toute cupidité, car même si quelqu'un est dans l'abondance, sa vie ne provient pas de ses biens.
12:16	Et il leur dit une parabole, en disant : Les champs d'un certain homme riche avaient beaucoup rapporté.
12:17	Et il raisonnait en lui-même, en disant : Que ferai-je, car je n'ai pas de lieu où je recueillerai mes fruits ?
12:18	Et il dit : Voici ce que je ferai : J'abattrai mes greniers, j'en bâtirai de plus grands, j'y recueillerai tous mes fruits de la terre et toutes mes bonnes choses.
12:19	Et je dirai à mon âme : Âme, tu as beaucoup de bonnes choses en réserve pour de nombreuses années, repose-toi, mange, bois et réjouis-toi !
12:20	Mais l'Elohîm lui dit : Insensé ! Cette nuit même on réclame ton âme loin de toi ! Et ces choses que tu as préparées, à qui seront-elles ?
12:21	Il en est ainsi de celui qui accumule une richesse<!--Vient du grec « thesaurizo » qui signifie « amasser de l'argent pour le garder, sans le faire circuler ni le placer », « rassembler et déposer », « entasser », « emmagasiner ».--> pour lui-même, et qui n'est pas riche pour Elohîm.

### Exhortation à se confier en Elohîm

12:22	Et il dit à ses disciples : C'est pourquoi je vous dis : Ne vous inquiétez pas pour votre âme, de ce que vous mangerez, ni pour le corps, de quoi vous serez vêtus.
12:23	L'âme est plus que la nourriture, et le corps, plus que le vêtement.
12:24	Observez les corbeaux, parce qu'ils ne sèment ni ne moissonnent, et ils n'ont pas de cellier, ni de grenier, et cependant Elohîm les nourrit. Combien ne valez-vous pas plus que les oiseaux ?
12:25	Et qui de vous, par ses inquiétudes, peut ajouter une coudée à sa stature ?
12:26	Si donc vous ne pouvez pas même la moindre chose, pourquoi êtes-vous inquiets du reste ?
12:27	Observez comment croissent les lis, ils ne travaillent, ni ne filent, et cependant je vous dis que Shelomoh même, dans toute sa gloire, n'a pas été vêtu comme l'un d'eux.
12:28	Mais si Elohîm revêt ainsi l'herbe qui est aujourd'hui dans les champs, et qui demain est jetée dans le four, à combien plus forte raison vous, gens de petite foi !
12:29	Et vous, ne cherchez pas ce que vous mangerez et ce que vous boirez et ne soyez pas inquiets,
12:30	car toutes ces choses, ce sont les nations du monde qui les recherchent. Mais votre Père sait que vous en avez besoin.
12:31	Mais cherchez plutôt le Royaume d'Elohîm, et toutes ces choses vous seront ajoutées.
12:32	N'aie pas peur, petit troupeau, car il a plu à votre Père de vous donner le Royaume.
12:33	Vendez vos biens et donnez l'aumône ! Faites-vous des bourses qui ne s'usent pas, un trésor dans les cieux qui ne défaille jamais, et où le voleur n'approche pas, et où la teigne ne détruit rien.
12:34	Car là où est votre trésor, là aussi sera votre cœur.

### Veiller en attendant le Seigneur<!--Mt. 24:36-25:30.-->

12:35	Que vos reins soient ceints, et les lampes allumées.
12:36	Et vous, soyez semblables à des humains qui attendent leur seigneur, quand il reviendra des noces, afin que quand il arrivera et qu'il frappera, ils lui ouvrent aussitôt. 
12:37	Bénis sont ces esclaves que le seigneur, en arrivant, trouvera veillant ! Amen, je vous dis qu’il se ceindra et qu’il les fera mettre à table, et que, passant de l’un à l’autre, il les servira.
12:38	Et s'il arrive à la seconde veille, et s'il arrive à la troisième veille, et qu'il les trouve ainsi, bénis sont ces esclaves !
12:39	Or sachez ceci : que si le seigneur de la maison savait à quelle heure le voleur vient, il veillerait et ne laisserait pas percer sa maison.
12:40	Vous donc aussi, soyez prêts, parce que le Fils d'humain vient à l'heure que vous ne pensez pas.

### Parabole des deux serviteurs

12:41	Mais Petros lui dit : Seigneur, dis-tu cette parabole pour nous, ou aussi pour tous ?
12:42	Mais le Seigneur dit : Quel est donc le gestionnaire fidèle et prudent, que le seigneur établira sur ses domestiques pour donner la nourriture au temps convenable ?
12:43	Béni est l'esclave, celui que son seigneur, en arrivant, trouvera agissant ainsi !
12:44	Je vous le dis en vérité, il l'établira sur tous ses biens.
12:45	Mais si cet esclave dit en son cœur : Mon seigneur tarde à venir, s'il se met à battre les serviteurs et les servantes, à manger, à boire et à s'enivrer,
12:46	le seigneur de cet esclave viendra le jour où il ne s'y attend pas et à l'heure qu'il ne connaît pas, et il le coupera en deux parts<!--Le mot grec « dichotomeo » signifie « couper en deux parts », « couper quelqu'un en deux », « châtiant en coupant », « fléau sévère ». Certains peuples, dont les Hébreux, employaient cette méthode comme châtiment corporel.-->, et lui donnera sa part avec les incrédules.
12:47	Or cet esclave qui a connu la volonté de son seigneur, et qui ne s'est pas préparé et n'a pas agi selon sa volonté, sera battu de beaucoup de coups.
12:48	Mais celui qui ne l'a pas connue et qui a fait des choses dignes de coups sera battu un peu. Et on demandera beaucoup à qui l'on a beaucoup donné, et on exigera davantage de celui à qui l'on a beaucoup confié.

### Yéhoshoua suscite la division

12:49	Je suis venu jeter un feu sur la Terre, et qu'ai-je à désirer, s'il est déjà allumé ?
12:50	Mais j'ai à être baptisé d'un baptême, et combien suis-je pressé jusqu'à ce qu'il soit accompli.
12:51	Pensez-vous que je sois venu donner la paix sur la Terre ? Non, vous dis-je, mais plutôt la division.
12:52	Car désormais, cinq dans une maison seront divisés, trois contre deux, et deux contre trois.
12:53	Le père sera divisé contre le fils et le fils contre le père, la mère contre la fille et la fille contre la mère, la belle-mère contre sa belle-fille et la belle-fille contre sa belle-mère.
12:54	Mais il disait aussi aux foules : Quand vous voyez une nuée se lever à l'occident, vous dites immédiatement : L'averse vient ! Et cela arrive ainsi.
12:55	Et quand c'est le vent du midi qui souffle, vous dites qu'il fera chaud. Et cela arrive.
12:56	Hypocrites ! Vous savez examiner l'aspect du ciel et de la Terre, mais comment donc n'examinez-vous pas ce temps-ci ?
12:57	Et pourquoi ne jugez-vous pas aussi par vous-mêmes de ce qui est juste ?
12:58	Car quand tu vas avec ton adversaire devant le magistrat, tâche en route de t'en délivrer, de peur qu'il ne te traîne devant le juge, et que le juge ne te livre à l'officier de justice, et que l'officier de justice ne te jette en prison.
12:59	Je te le dis, tu ne sortiras jamais de là que tu n'aies payé jusqu'à la dernière petite pièce<!--Petite pièce de monnaie en laiton. Voir le tableau « Monnaies au temps de Yéhoshoua ha Mashiah » en annexe.-->.

## Chapitre 13

### Exhortation à la repentance

13:1	Et en ce même temps, quelques-uns qui se trouvaient là présents lui apportèrent des nouvelles concernant les Galiléens dont Pilate avait mêlé le sang à celui de leurs sacrifices.
13:2	Et Yéhoshoua répondant, leur dit : Pensez-vous que ces Galiléens étaient plus pécheurs que tous les Galiléens, parce qu'ils ont souffert de la sorte ?
13:3	Non, vous dis-je. Mais si vous ne vous repentez pas, vous périrez tous de la même manière.
13:4	Ou bien, ces 18 sur qui est tombée la tour de Siloé et qu'elle a tués, pensez-vous qu'ils étaient plus débiteurs que tous les humains qui habitent Yeroushalaim ?
13:5	Non, vous dis-je. Mais si vous ne vous repentez pas, vous périrez tous de la même manière.

### Parabole du figuier stérile<!--Cp. Mt. 21:18-21.-->

13:6	Or il disait cette parabole : Quelqu'un avait un figuier planté dans sa vigne et il vint pour y chercher du fruit, mais il n'en trouva pas.
13:7	Et il dit au vigneron : Voilà trois ans que je viens chercher du fruit à ce figuier, et je n'en trouve pas. Coupe-le : pourquoi donc rend-il la terre inutile ?
13:8	Et répondant, il lui dit : Seigneur, laisse-le encore pour cette année, jusqu’à ce que j’aie creusé et mis du fumier tout autour.
13:9	Et s'il faisait du fruit en effet ? Sinon, tu le couperas dans l'imminence.

### Guérison de la femme courbée le jour du shabbat

13:10	Or il était en train d’enseigner dans une synagogue un jour de shabbat,
13:11	et voici, il y avait là une femme ayant un esprit d'infirmité depuis 18 ans : elle était toute courbée et ne pouvait pas se redresser complètement.
13:12	Et quand Yéhoshoua la vit, il l'appela et lui dit : Femme, tu es libérée de ton infirmité !
13:13	Et il lui imposa les mains et, immédiatement, elle se redressa et glorifia Elohîm.
13:14	Mais le chef de la synagogue, indigné de ce que Yéhoshoua avait guéri un jour de shabbat, répondit et dit à la foule : Il y a six jours pour travailler, venez donc vous faire guérir ces jours-là et non pas le jour du shabbat.
13:15	Mais le Seigneur lui répondit et dit : Hypocrite ! Le jour du shabbat, chacun de vous ne délie-t-il pas son bœuf ou son âne de la crèche et ne les mène-t-il pas boire ?
13:16	Et celle-ci, qui est fille d'Abraham, que Satan avait liée voici 18 ans, ne fallait-il pas la délier des liens le jour du shabbat ?
13:17	Et en parlant de ces choses, tous ses adversaires étaient couverts de honte, mais la foule entière se réjouissait de toutes les choses glorieuses qui étaient faites par lui.

### Parabole du grain de sénevé et du levain<!--Voir Mt. 13:31,33.-->

13:18	Mais il disait : À quoi est semblable le Royaume d'Elohîm, et à quoi le comparerai-je ?
13:19	Il est semblable au grain de sénevé qu'un être humain a pris et jeté dans son jardin. Et il a poussé, il est devenu un grand arbre, et les oiseaux du ciel ont fait leurs nids dans ses branches.
13:20	Et il dit encore : À quoi comparerai-je le Royaume d'Elohîm ?
13:21	Il est semblable au levain qu'une femme a pris et mis dans trois mesures<!--Un type de mesure de produits secs (farine, grain, ...), d'environ 12 litres.--> de farine, jusqu'à ce que le tout soit levé.

### Enseignements de Yéhoshoua sur le chemin de Yeroushalaim (Jérusalem)

13:22	Et il traversait les villes et les villages, enseignant, et faisant un voyage vers Yeroushalaim.
13:23	Et quelqu'un lui dit : Seigneur, est-ce que peu, sont les sauvés ? Et il leur dit :
13:24	Combattez<!--Col. 1:29 ; 4:12 ; 1 Tim. 6:12 ; 2 TiM 4:7.--> pour entrer à travers l’étroite porte. Car je vous le dis que beaucoup chercheront à y entrer, et ne le pourront pas.
13:25	Dès que le maître de la maison se sera réveillé et aura fermé la porte, et que vous, étant dehors, vous vous mettrez à frapper à la porte, en disant : Seigneur, Seigneur ! ouvre-nous ! Et que, répondant, il vous dira : Vous, je ne sais pas d’où vous êtes !
13:26	Alors vous vous mettrez à dire : Nous avons mangé et bu en ta présence, et tu as enseigné dans nos rues.
13:27	Et il dira : Je vous le dis, je ne sais pas d’où vous êtes. Retirez-vous de moi, vous tous ouvriers d'injustice.
13:28	Là sera le pleur et le grincement des dents, quand vous verrez Abraham, et Yitzhak, et Yaacov, et tous les prophètes dans le Royaume d'Elohîm, - mais vous, jetés dehors !
13:29	Et ils viendront de l'orient et de l'occident, du nord et du sud, et ils se mettront à table dans le Royaume d'Elohîm.
13:30	Et voici, ceux qui sont les derniers seront les premiers, et ceux qui sont les premiers seront les derniers.
13:31	En ce même jour, quelques pharisiens s'approchèrent de lui en disant : Sors et va-t'en d'ici, parce qu'Hérode veut te tuer.
13:32	Et il leur dit : Allez et dites à ce renard : Voici, je chasse les démons et j'accomplis des guérisons aujourd'hui et demain, et le troisième jour, je suis rendu parfait.
13:33	Mais il me faut marcher aujourd'hui et demain, et le jour suivant, car il n’est pas admissible qu’un prophète meure hors de Yeroushalaim.

### Lamentations de Yéhoshoua sur Yeroushalaim (Jérusalem)<!--Mt. 23:37-39 ; Lu. 19:41-44 ; cp. Jé. 22:5.-->

13:34	Yeroushalaim ! Yeroushalaim ! Qui tues les prophètes et qui lapides ceux qui te sont envoyés, combien de fois j'ai voulu rassembler tes enfants comme une poule sa couvée sous ses ailes<!--Ps. 17:8 ; Mt. 23:37-39.--> ! Et vous ne l'avez pas voulu !
13:35	Voici, votre maison vous est laissée déserte. Mais, je vous le dis, amen, vous ne me verrez plus jamais, jusqu’à ce qu’il arrive que vous disiez : Béni soit celui qui vient au Nom du Seigneur<!--Ps. 118:26.--> !

## Chapitre 14

### Yéhoshoua guérit un hydropique le jour du shabbat<!--Cp. Mt. 12:9-13.-->

14:1	Et il arriva, comme il entrait un shabbat dans la maison d'un des chefs des pharisiens pour prendre un repas, qu'ils étaient là et l'observaient.
14:2	Et voici, un homme hydropique<!--L'hydropisie est une maladie qui se caractérise par une accumulation anormale de fluide séreux dans les tissus cellulaires. Elle est synonyme d'« œdème ».--> était là devant lui.
14:3	Et Yéhoshoua, répondant, parla aux docteurs de la torah et aux pharisiens, disant : Est-il légal de guérir pendant le shabbat ?
14:4	Mais ils gardèrent le silence. Et l'ayant pris, il le guérit et le renvoya.
14:5	Et s'adressant à eux, il dit : Lequel de vous, si son âne ou son bœuf tombe dans un puits, ne l'en retirera pas aussitôt, le jour du shabbat ?
14:6	Et ils ne pouvaient répondre par la contradiction à ces choses.

### Parabole de l'invité

14:7	Mais il disait aux invités une parabole, en remarquant comment ils choisissaient les premières places. Il leur dit :
14:8	Quand tu seras invité par quelqu'un à des noces, ne te mets pas à la première place à table, de peur qu'un plus considéré que toi n'ait été invité par lui,
14:9	et que celui qui vous aura invités, toi et lui, ne vienne te dire : Donne ta place à celui-ci ! Et alors tu commenceras avec honte à aller occuper la dernière place.
14:10	Mais quand tu seras invité, va te mettre à la dernière place, afin que quand viendra celui qui t'a invité, il te dise : Mon ami, monte plus haut ! Alors ce sera une gloire pour toi en présence de tous ceux qui seront à table avec toi.
14:11	Car quiconque s'élève sera abaissé, et quiconque s'abaisse sera élevé.
14:12	Et il dit aussi à celui qui l'avait invité : Quand tu fais un dîner ou un souper, n'invite pas tes amis, ni tes frères, ni tes parents, ni tes riches voisins, de peur qu'eux aussi ne t'invitent à leur tour et que la pareille ne te soit rendue.
14:13	Mais quand tu fais un souper, invite les pauvres, les mutilés, les boiteux et les aveugles.
14:14	Et tu seras béni parce qu'ils n’ont pas le moyen de te le rendre, car cela te sera rendu à la résurrection des justes.

### Parabole du grand souper<!--Mt. 22:1-14.-->

14:15	Mais un de ceux qui étaient à table, ayant entendu ces choses, lui dit : Béni est celui qui mangera du pain dans le Royaume d'Elohîm !
14:16	Mais il lui dit : Un homme fit un grand souper, et il invita beaucoup de gens.
14:17	Et à l'heure du souper, il envoya son esclave pour dire aux invités : Venez, car tout est déjà prêt.
14:18	Mais ils commencèrent tous unanimement à s'excuser. Le premier lui dit : J'ai acheté un champ, et il faut que j’aille le voir, je te prie, tiens-moi pour excusé.
14:19	Et un autre dit : J'ai acheté cinq paires de bœufs, et je vais les éprouver, je te prie, tiens-moi pour excusé.
14:20	Et un autre dit : J'ai épousé une femme, c'est pourquoi je ne peux pas venir.
14:21	Et l'esclave, de retour, rapporta ces choses à son seigneur. Alors le maître de la maison, irrité, dit à son esclave : Va rapidement sur les places et dans les rues de la ville, et amène ici les pauvres, les mutilés, les boiteux et les aveugles.
14:22	Et l'esclave dit : Seigneur, ce que tu as commandé a été fait, et il y a encore de la place.
14:23	Et le seigneur dit à l'esclave : Va dans les routes et le long des clôtures, et ceux que tu trouveras, contrains-les d'entrer, afin que ma maison soit remplie.
14:24	Car je vous dis qu'aucun de ces hommes qui avaient été invités ne goûtera de mon souper.

### Le renoncement à soi-même

14:25	Or de grandes foules faisaient route avec lui. Il se retourna et leur dit :
14:26	Si quelqu'un vient à moi, et ne hait pas son père et sa mère, sa femme et ses enfants, ses frères et ses sœurs, et même son âme propre, il ne peut être mon disciple.
14:27	Et quiconque ne porte pas sa croix en venant après moi ne peut être mon disciple.
14:28	Car lequel d'entre vous, voulant bâtir une tour, ne s'assied premièrement et ne calcule la dépense, pour voir s'il a de quoi l'achever,
14:29	de peur qu'après avoir posé le fondement, il ne puisse pas l'achever, et que tous ceux qui le verront ne commencent à se jouer de lui,
14:30	en disant : Cet homme a commencé à bâtir, et il n'a pas pu achever ?
14:31	Ou, quel roi, s'il va faire la guerre à un autre roi, ne s'assied pas premièrement pour examiner s'il peut, avec 10 000, aller à la rencontre de celui qui vient contre lui avec 20 000 ?
14:32	Autrement, il lui envoie une ambassade, pendant qu’il est encore loin et demande la paix.
14:33	Ainsi donc, quiconque d'entre vous ne renonce pas à tout ce qu'il possède ne peut être mon disciple.
14:34	Le sel est bon, mais si le sel perd sa force et sa saveur<!--Le mot grec signifie littéralement « être stupide », « agir follement, bêtement », « faire des stupidités ». Voir Ro. 1:22 ; 1 Co. 1:20.-->, avec quoi l'assaisonnera-t-on ?
14:35	Il n'est utile ni pour la terre, ni pour le fumier, mais on le jette dehors. Que celui qui a des oreilles pour entendre, qu'il entende !

## Chapitre 15

### Trois paraboles sur la repentance

15:1	Or tous les publicains et les pécheurs s'approchaient de lui pour l'entendre.
15:2	Et les pharisiens et les scribes murmuraient, en disant : Celui-ci reçoit les pécheurs et mange avec eux.

### Parabole de la brebis perdue<!--Mt. 18:12-14.-->

15:3	Mais il leur proposa cette parabole, en disant :
15:4	Quel homme parmi vous, s'il a 100 brebis et qu'il en perde une, ne laisse pas les 99 dans le désert, pour aller à la recherche de celle qui est perdue, jusqu'à ce qu'il la trouve ?
15:5	Et lorsqu'il l'a trouvée, il la met avec joie sur ses épaules,
15:6	et, de retour à la maison, il appelle ses amis et ses voisins et il leur dit : Réjouissez-vous avec moi, parce que j'ai trouvé ma brebis qui était perdue.
15:7	Je vous dis, qu’il y aura de même plus de joie dans le ciel pour un seul pécheur qui se repent, que pour les 99 justes qui n'ont pas besoin de repentance.

### Parabole de la drachme perdue

15:8	Ou quelle femme, si elle a 10 drachmes et qu'elle en perde une, n'allume pas une lampe, ne balaie la maison, et ne cherche avec soin, jusqu'à ce qu'elle la trouve ?
15:9	Et lorsqu'elle l'a trouvée, elle appelle amies et voisines, en leur disant : Réjouissez-vous avec moi, car j'ai trouvé la drachme que j'avais perdue.
15:10	C'est ainsi, je vous le dis, qu'il y a de la joie devant les anges d'Elohîm pour un seul pécheur qui se repent.

### Parabole du fils dépensier

15:11	Il dit aussi : Un homme avait deux fils.
15:12	Et le plus jeune d’entre eux dit au père : Père, donne-moi la part de bien qui m'appartient. Et il leur partagea son bien.
15:13	Et peu de jours après, le plus jeune fils, ayant tout ramassé, partit pour un pays éloigné, où il dissipa son bien en vivant d'une façon libertine<!--Pr. 29:3.-->.
15:14	Mais après qu'il eut tout gaspillé<!--Le verbe « gaspiller » vient du grec « dapanao » qui veut dire aussi « éparpiller, consumer, dépenser ». Voir Mc. 5:26 ; Ja. 4:3.-->, une grande famine survint dans ce pays-là, et il commença à se trouver dans le besoin.
15:15	Et s'en étant allé, il se colla à l'un des citoyens de cette contrée. Celui-ci l'envoya dans ses champs pour paître les pourceaux.
15:16	Et il désirait se remplir le ventre des caroubes<!--Le nom d'un fruit, ceratonia siliqua, ou caroube, fruit du caroubier, appelé aussi le pain de Jean, car ses cosses ressemblent aux sauterelles qui constituaient la nourriture de Yohanan le Baptiste (Jean-Baptiste). Ce fruit avait une forme de corne et un goût doucereux. Il était utilisé pour nourrir les cochons, et était également un élément de nourriture pour les gens de condition très modeste.--> que les pourceaux mangeaient, mais personne ne lui en donnait.
15:17	Mais étant rentré en lui-même, il dit : Combien d’employés chez mon père ont du pain en abondance, et moi je meurs de faim !
15:18	Je me lèverai, j'irai vers mon père et je lui dirai : Père, j'ai péché contre le ciel et devant toi,
15:19	et je ne suis plus digne d'être appelé ton fils, traite-moi comme l'un de tes employés.
15:20	Et s'étant levé, il alla vers son père. Or comme il était encore loin, son père le vit et fut ému de compassion, et il courut se jeter à son cou et l'embrassa tendrement.
15:21	Mais le fils lui dit : Mon père, j'ai péché contre le ciel et devant toi. Et je ne suis plus digne d'être appelé ton fils.
15:22	Et le père dit à ses esclaves : Apportez la robe d'honneur<!--Vient du grec « protos » qui signifie « premier », « principal », « chef ».--> et revêtez-le, et mettez-lui un anneau au doigt et des sandales aux pieds.
15:23	Amenez-moi le veau gras, et tuez-le ! Mangeons et réjouissons-nous,
15:24	parce que mon fils que voici était mort et il a repris vie, il était perdu et il est retrouvé. Et ils commencèrent à se réjouir.
15:25	Or son fils aîné était dans le champ. Lorsqu'il revint et approcha de la maison, il entendit une symphonie et des danses.
15:26	Et il appela un des serviteurs, il s'informa de ce que c'était.
15:27	Mais celui-ci lui dit : Parce que ton frère est de retour, et ton père a tué le veau gras, parce qu'il l'a retrouvé<!--« Recevoir », « recouvrer », « reprendre ».--> en bonne santé.
15:28	Mais il se mit en colère, et ne voulut pas entrer. Son père étant donc sorti, l'exhortait.
15:29	Mais répondant, il dit au père : Voilà tant d'années que je suis ton esclave, et je ne me suis jamais éloigné de ton commandement, et jamais tu ne m’as donné un chevreau pour me réjouir avec mes amis.
15:30	Mais quand ton fils est arrivé, celui qui a dévoré ton bien avec des prostituées, c'est pour lui que tu as tué le veau gras !
15:31	Et le père lui dit : Enfant, tu es toujours avec moi, et tout ce qui est à moi est à toi.
15:32	Mais il fallait bien s'égayer et se réjouir, parce que ton frère que voici était mort et il a repris vie, parce qu'il était perdu et qu'il est retrouvé.

## Chapitre 16

### Parabole du gestionnaire injuste

16:1	Mais il disait aussi à ses disciples : Il y avait un certain homme riche qui avait un gestionnaire, qui fut accusé devant lui comme dissipant ses biens.
16:2	Et il l'appela et lui dit : Qu'est-ce que j'entends dire de toi ? Rends compte de ta gestion<!--la gestion d'un ménage ou des affaires du ménage, spécifiquement, la gestion, la surveillance, l'administration de propriétés des autres.-->, car tu n'auras plus le pouvoir de gérer mes biens.
16:3	Et le gestionnaire dit en lui-même : Que ferai-je, puisque mon seigneur m'ôte ta gestion ? Travailler à la terre ? Je n’en ai pas la force. Mendier ? J'en ai honte !
16:4	Je sais ce que je ferai, afin que, lorsque j’aurai été destitué de la gestion, il y en ait qui me reçoivent dans leurs maisons.
16:5	Et il appela chacun des débiteurs de son seigneur, et il dit au premier : Combien dois-tu à mon seigneur ?
16:6	Et il dit : 100 mesures d'huile. Et il lui dit : Prends ton billet<!--Vient du grec gramma qui signifie « une lettre », « tout écrit, un document ou un enregistrement », « une note de sa main », etc.--> et assieds-toi rapidement, et écris 50.
16:7	Et il dit à un autre : Et toi, combien dois-tu ? Il dit : 100 mesures de blé. Et il lui dit : Prends ton billet, et écris 80.
16:8	Et le seigneur loua le gestionnaire injuste de ce qu'il avait agi prudemment. Ainsi les fils de cet âge<!--Du grec « aion » qui signifie « période », « temps », etc.--> sont plus prudents dans leur génération, que les fils de lumière.
16:9	Et moi je vous dis : Faites-vous des amis hors de<!--Ce mot vient du terme grec « ek » qui signifie « loin de, hors de ». Le Seigneur s'est servi de ce terme comme le préfixe de « ekklesia » afin de désigner l'Assemblée (ou l'Église). Voir Mt. 16:18.--> Mamon<!--Voir Mt. 6:24. Mamon signifie « richesses ».--> de l'injustice, afin que quand vous partirez<!--« Manquer », « laisser », « omettre », « passer outre », « quitter », « cesser », « arrêter », « manque de lumière causé par une éclipse de soleil et de lune ».-->, ils vous reçoivent dans les tabernacles éternels.
16:10	Celui qui est fidèle dans les petites choses est aussi fidèle dans les grandes choses, et celui qui est injuste dans les petites choses est aussi injuste dans les grandes choses.
16:11	Si donc dans l'injuste Mamon<!--Mamon trompeur ou richesses trompeuses.--> vous n'êtes pas devenus fidèles, qui vous confiera<!--Qui croira en vous.--> le véritable ?
16:12	Et si vous n'êtes pas devenus fidèles dans ce qui vous est étranger, ce qui est à vous, qui vous le donnera ?
16:13	Aucun domestique ne peut servir deux seigneurs. Car, ou il haïra l'un et aimera l'autre : ou il s'attachera à l'un et méprisera l'autre. Vous ne pouvez servir Elohîm et Mamon<!--Voir commentaire en Mt. 6:24.-->.

### L'avarice condamnée par Yéhoshoua

16:14	Mais les pharisiens, qui sont amis de l'argent, entendaient aussi toutes ces choses et ils se moquaient de lui.
16:15	Et il leur dit : Vous êtes ceux qui se justifient eux-mêmes devant les humains, mais Elohîm connaît vos cœurs. C'est pourquoi, ce qui est élevé parmi les humains est une abomination devant Elohîm.
16:16	La torah et les prophètes, jusqu'à Yohanan ; depuis lors, le Royaume d'Elohîm est prêché et chacun y fait violence.
16:17	Or il est plus aisé au ciel et à la Terre de passer que pour un point de la torah de tomber.

### Enseignement de Yéhoshoua sur le divorce<!--Mt. 5:31-32, 19:1-9 ; Mc. 10:2-12.-->

16:18	Quiconque répudie sa femme et en épouse une autre commet un adultère, et quiconque épouse celle qui a été répudiée par son mari commet un adultère.

### Histoire de l'homme riche et d'Èl’azar (Lazare)

16:19	Or il y avait un certain humain riche, qui s’habillait de pourpre et de fin lin, se réjouissant même chaque jour splendidement.
16:20	Et il y avait un certain pauvre, du nom de Èl’azar, qui était couché à sa porte, couvert d’ulcères,
16:21	et désirant se rassasier des miettes, tombant de la table du riche. Mais même les chiens en venant, léchaient ses ulcères.
16:22	Et il arriva que le pauvre mourut et il fut porté par les anges dans le sein d'Abraham<!--Le sein d'Abraham n'est pas le ciel, car Yéhoshoua ha Mashiah (Jésus-Christ) a dit que personne n'était monté au ciel, excepté celui qui est descendu du ciel, le Fils d'humain (c'est-à-dire lui-même) qui est dans le ciel (Jn. 3:13). En effet, le chemin vers le Saint des saints n'a pas été manifesté (ouvert) avant la croix. C'est donc Yéhoshoua ha Mashiah qui l'a inauguré (Hé. 10:19-22). De plus, selon Ep. 4:8-10, le Mashiah est « descendu dans les parties inférieures de la Terre » et en est remonté avec des captifs, à savoir tous les croyants ayant vécu avant la croix. Le sein d'Abraham était une partie du shéol (également appelé enfer, séjour des morts ou Hadès ; voir commentaire en Mt. 16:18). L'histoire d'Èl’azar et de l'homme riche démontre que le séjour des morts était divisé en deux parties séparées par un abîme infranchissable. L'une était réservée aux impies qui y subissaient des tourments, et l'autre aux croyants qui se reposaient en attendant leur délivrance. Ce récit est confirmé par l'histoire de Shaoul (Saül), un homme rejeté par Elohîm, qui avait rejoint dans le shéol : Shemouél (Samuel), un prophète intègre (1 S. 28:16-19).-->. Le riche mourut aussi et il fut enseveli.
16:23	Et dans l'Hadès<!--Voir commentaire en Mt. 16:18.-->, levant les yeux, tandis qu'il est dans les tourments, il voit de loin Abraham et Èl’azar dans son sein.
16:24	Et s'écriant il dit : Père Abraham, aie pitié de moi, et envoie Èl’azar pour qu'il trempe le bout de son doigt dans l'eau et me rafraîchisse la langue, parce que je suis tourmenté dans cette flamme.
16:25	Mais Abraham dit : Enfant, souviens-toi que pendant ta vie tu as reçu de bonnes choses, et Èl’azar pareillement les choses mauvaises. Mais maintenant, il est ici consolé, et toi, tu es tourmenté.
16:26	Et en plus de tout cela, un grand abîme a été fermement placé entre nous et vous, afin que ceux qui veulent passer d'ici vers vous ne puissent le faire, et qu'on ne traverse pas non plus de là-bas vers nous.
16:27	Mais il dit : Je te prie donc, père, que tu l'envoies dans la maison de mon père, 
16:28	car j'ai cinq frères, afin qu'il leur rende témoignage, de peur qu'eux aussi ne viennent dans ce lieu de tourment.
16:29	Abraham lui dit : Ils ont Moshé et les prophètes, qu'ils les écoutent !
16:30	Mais il dit : Non, père Abraham, mais si quelqu'un des morts va vers eux, ils se repentiront.
16:31	Mais il lui dit : S'ils n'écoutent pas Moshé et les prophètes, ils ne se laisseront pas persuader même si quelqu'un des morts ressuscitait.

## Chapitre 17

### Enseignements de Yéhoshoua au sujet des scandales, du pardon et de la foi<!--Mt. 5:31-32, 19:1-9 ; Mc. 10:2-12.-->

17:1	Or il dit à ses disciples : Il est impossible<!--« Ce qui ne peut être admis », « inadmissible », « incroyable », « malséant ».--> qu'il n'arrive pas de scandales, mais malheur à celui par le moyen duquel cela arrive !
17:2	Il est avantageux pour lui qu’une pierre de meule soit mise autour de son cou et qu’il soit jeté à la mer, que de scandaliser un seul de ces petits.
17:3	Discernez vous-mêmes ! Mais si ton frère a péché contre toi, réprimande-le d'une manière tranchante et, s'il se repent, remets-lui.
17:4	Et s'il a péché contre toi 7 fois le jour et que 7 fois le jour il retourne à toi en disant : Je me repens, tu lui remettras.
17:5	Et les apôtres dirent au Seigneur : Ajoute-nous de la foi.
17:6	Et le Seigneur dit : Si vous aviez de la foi comme un grain de sénevé, vous diriez à ce sycomore : Sois déraciné et planté dans la mer ! Et il vous obéirait !

### Les esclaves inutiles

17:7	Mais qui de vous, ayant un esclave qui laboure ou garde les troupeaux, lui dira aussitôt qu'il rentre des champs : Avance-toi de suite et mets-toi à table ?
17:8	Ne lui dira-t-il pas plutôt : Prépare-moi à souper, ceins-toi, et sers-moi jusqu'à ce que j'aie mangé et bu, et après cela tu mangeras et tu boiras ?
17:9	A-t-il à rendre grâce à cet esclave parce qu'il a fait ce qui lui était ordonné ? Je ne pense pas.
17:10	Vous aussi, de même, quand vous aurez fait toutes les choses qui vous ont été ordonnées, dites : Nous sommes des esclaves inutiles, et nous avons fait ce que nous devions faire.

### Guérison des dix lépreux

17:11	Et il arriva qu'en allant à Yeroushalaim, il passait par le milieu de la Samarie et de la Galilée.
17:12	Et en entrant dans un village, dix hommes lépreux vinrent à sa rencontre. Ils se tenaient au loin,
17:13	et ils élevèrent la voix, disant : Yéhoshoua, Maître, aie pitié de nous !
17:14	Et en les voyant, il leur dit : Allez vous montrer aux prêtres<!--Lé. 13.--> ! Et il arriva, comme ils s'en allaient, qu'ils furent purifiés.
17:15	Mais l'un d'eux, se voyant guéri, revint sur ses pas en glorifiant Elohîm à grande voix.
17:16	Et il tomba sur sa face à ses pieds en lui rendant grâce. Or c'était un Samaritain.
17:17	Mais Yéhoshoua, répondant, dit : Les dix n'ont-ils pas été purifiés ? Mais les neuf, où sont-ils ?
17:18	Il ne s'en est pas trouvé qui soit revenu pour donner gloire à Elohîm, si ce n'est cet étranger !
17:19	Et il lui dit : Lève-toi, va, ta foi t'a sauvé.
17:20	Mais étant interrogé par les pharisiens quand viendrait le Royaume d'Elohîm, il leur répondit et dit : Le Royaume d'Elohîm ne vient pas de manière à être observé.
17:21	Et on ne dira pas : Voici, il est ici ! ou : Voilà, il est là ! Car voici, le Royaume d'Elohîm est à l'intérieur de vous.

### Le jour du Fils d'humain

17:22	Mais il dit aux disciples : Des jours viendront où vous désirerez voir un des jours du Fils d'humain, mais vous ne le verrez pas. 
17:23	Et on vous dira : Voici, il est ici ! Voici, il est là ! N'y allez pas, et ne suivez pas.
17:24	Car comme l'éclair brille, resplendissant d’une région sous le ciel à une autre région sous le ciel, ainsi sera le Fils d'humain en son jour.
17:25	Mais il faut premièrement qu'il souffre beaucoup et qu'il soit rejeté par cette génération.
17:26	Et comme il arriva dans les jours de Noah, il en sera aussi de même dans les jours du Fils d'humain.
17:27	Ils mangeaient, buvaient, se mariaient, donnaient en mariage, jusqu'au jour où Noah entra dans l'arche, alors le déluge vint et les fit tous périr.
17:28	De même aussi, comme il arriva dans les jours de Lot : ils mangeaient, buvaient, achetaient, vendaient, plantaient et bâtissaient.
17:29	Mais le jour où Lot sortit de Sodome, une pluie de feu et de soufre tomba du ciel, et les fit tous périr.
17:30	Il en sera de même le jour où le Fils d'humain sera révélé.
17:31	En ce jour-là, que celui qui sera sur le toit et qui aura ses effets dans la maison ne descende pas pour les prendre, et que celui qui sera dans le champ ne retourne pas non plus à ce qui est resté en arrière.
17:32	Rappelez-vous la femme de Lot<!--Voir Ge. 19:26.-->.
17:33	Quiconque cherchera à sauver son âme la perdra, et quiconque la perdra la retrouvera.
17:34	Je vous le dis : En cette nuit-là, il y en aura deux sur un seul lit : l'un sera pris et l'autre sera laissé.
17:35	Elles seront deux en train de moudre<!--C'était la coutume d'envoyer les femmes et les esclaves féminines au moulin pour tourner la meule à main.--> ensemble : l'une sera prise et l'autre laissée.
17:36	Deux seront aux champs : l'un sera pris et l'autre sera laissé.
17:37	Et répondant, ils lui disent : Où, Seigneur ? Et il leur dit : Là où est le corps, là aussi seront rassemblés les aigles<!--Un aigle, en tant qu'étendard de l'armée impériale romaine. Voir Job 39:30 ; Mt. 24:28 et Ap. 19:17-21.-->.

## Chapitre 18

### Parabole du juge inique

18:1	Mais il leur disait aussi une parabole pour montrer qu'il faut toujours prier et ne pas se décourager,
18:2	disant : Il y avait dans une ville un juge qui ne craignait pas Elohîm et qui ne respectait aucun humain.
18:3	Or il y avait aussi dans cette ville une veuve, qui venait auprès de lui, disant : Fais-moi justice de ma partie adverse !
18:4	Et pendant un temps il ne voulut pas. Mais après cela il dit en lui-même : Quoique je ne craigne pas Elohîm et que je ne respecte aucun humain,
18:5	néanmoins, parce que cette veuve m'importune, je lui ferai justice, de peur qu'elle ne vienne à la fin me traiter rudement.
18:6	Et le Seigneur dit : Écoutez ce que dit le juge injuste.
18:7	Et Elohîm n'exécutera-t-il pas la vengeance de ses élus qui crient à lui jour et nuit, quoiqu'il use de patience envers eux ?
18:8	Je vous dis qu'il exécutera la vengeance pour eux rapidement. Mais quand le Fils d'humain viendra, trouvera-t-il la foi sur la Terre ?

### Parabole du pharisien et du publicain

18:9	Mais il dit aussi cette parabole à quelques-uns qui se persuadaient en eux-mêmes d'être justes et qui méprisaient totalement les autres :
18:10	Deux hommes montèrent au temple pour prier, l'un pharisien, et l'autre publicain.
18:11	Le pharisien, se tenant debout, priait ainsi en lui-même : Elohîm, je te rends grâce de ce que je ne suis pas comme le reste des humains : ravisseurs, injustes, adultères, ni même comme ce publicain.
18:12	Je jeûne deux fois le shabbat<!--Le septième jour de chaque semaine qui était une fête sacrée, pour lequel les Israélites devaient s'abstenir de tout travail.-->, et je donne la dîme de tout ce que je possède.
18:13	Mais le publicain, se tenant debout, éloigné, n'osait même pas lever les yeux vers le ciel, mais il se frappait la poitrine, en disant : Elohîm, sois apaisé envers moi qui suis pécheur !
18:14	Je vous dis que celui-ci descendit dans sa maison justifié, plutôt que l'autre. Car quiconque s'élève sera abaissé, et quiconque s'abaisse sera élevé.

### Yéhoshoua et les bébés<!--Mt. 19:13-15 ; Mc. 10:13-16.-->

18:15	Or ils lui présentèrent aussi des bébés afin qu'il les touche, mais les disciples voyant cela, les réprimandaient d'une manière tranchante.
18:16	Mais Yéhoshoua les appela et dit : Laissez venir à moi les enfants, et ne les en empêchez pas, car le Royaume d'Elohîm est pour ceux qui leur ressemblent.
18:17	Amen, je vous le dis, quiconque ne recevra pas comme un enfant le Royaume d'Elohîm n'y entrera jamais.

### Yéhoshoua dénonce l'attachement aux richesses<!--Mt. 19:16-30 ; Mc. 10:17-31 ; cp. Lu. 10:25-37.-->

18:18	Et un chef interrogea Yéhoshoua et dit : Bon Docteur ! Que dois-je faire pour hériter la vie éternelle ?
18:19	Mais Yéhoshoua lui dit : Pourquoi m'appelles-tu bon ? Nul n'est bon, excepté un seul : Elohîm<!--Voir commentaire Mc. 10:18.-->.
18:20	Tu connais les commandements : Tu ne commettras pas d'adultère. Tu n'assassineras pas. Tu ne voleras pas. Tu ne diras pas de faux témoignage. Honore ton père et ta mère.
18:21	Et il dit : J'ai observé toutes ces choses dès ma jeunesse.
18:22	Mais Yéhoshoua ayant entendu cela, lui dit : Il te manque encore une chose : Vends tout ce que tu as, et distribue-le aux pauvres, et tu auras un trésor dans les cieux. Et viens, suis-moi !
18:23	Mais lorsqu'il entendit ces choses, il devint très triste, car il était extrêmement riche.
18:24	Mais Yéhoshoua, voyant qu'il était devenu très triste, dit : Qu'il est difficile à ceux qui ont des richesses d'entrer dans le Royaume d'Elohîm !
18:25	Car il est plus facile à un chameau de passer par le trou d'une aiguille, qu'à un riche d'entrer dans le Royaume d'Elohîm<!--Voir commentaire Mt. 19:24.-->.
18:26	Mais ceux qui entendirent cela, dirent : Et qui peut donc être sauvé ?
18:27	Mais il leur dit : Ce qui est impossible pour les humains est possible pour Elohîm.

### La récompense des disciples de Yéhoshoua

18:28	Mais Petros dit : Voici, nous avons tout quitté, et nous t'avons suivi.
18:29	Et il leur dit : Amen, je vous le dis, il n'est personne qui, ayant quitté pour l'amour du Royaume d'Elohîm, sa maison, ou ses parents, ou ses frères, ou sa femme, ou ses enfants,
18:30	ne reçoive beaucoup plus en ce temps-ci, et dans l'âge à venir, la vie éternelle.

### Yéhoshoua annonce à nouveau sa mort et sa résurrection<!--Mt. 20:17-19 ; Mc. 10:32-34.-->

18:31	Mais prenant avec lui les douze il leur dit : Voici, nous montons à Yeroushalaim, et tout ce qui est écrit par les prophètes au sujet du Fils d'humain s'accomplira.
18:32	Car il sera livré aux nations et on se jouera de lui, on l'outragera et on crachera sur lui,
18:33	et, après l'avoir châtié avec un fouet, on le fera mourir mais, le troisième jour, il ressuscitera.
18:34	Et ils ne comprirent rien à ces choses : et cette parole leur était cachée, et ils ne percevaient pas ce qui a été dit.

### Guérison de Bartimaios (Bartimée)<!--Cp. Mt. 20:29-34 ; Mc. 10:46-52.-->

18:35	Or il arriva, comme il approchait de Yeriycho, qu'un aveugle était assis près du chemin, demandant l'aumône.
18:36	Et entendant la foule qui passait, il s'informa de ce que c'était.
18:37	Et on lui déclara : C'est Yéhoshoua, le Nazaréen, qui passe !
18:38	Et il cria, disant : Yéhoshoua, Fils de David, aie pitié de moi !
18:39	Et ceux qui allaient devant le réprimandaient d'une manière tranchante pour le faire taire, mais il criait beaucoup plus fort : Fils de David, aie pitié de moi !
18:40	Et Yéhoshoua s'étant arrêté, ordonna qu'on le lui amène. Et quand il se fut approché, il l'interrogea,
18:41	disant : Que veux-tu que je fasse pour toi ? Et il dit : Seigneur, que je recouvre la vue !
18:42	Et Yéhoshoua lui dit : Recouvre la vue, ta foi t'a sauvé.
18:43	Et immédiatement il recouvra la vue et il le suivait en glorifiant Elohîm. Et tout le peuple voyant cela, donna louange à Elohîm.

## Chapitre 19

### Conversion de Zakkay (Zachée)

19:1	Et il entra dans Yeriycho et la traversa.
19:2	Et voici, un homme appelé du nom de Zakkay<!--Zachée.-->, et qui était chef des publicains, et qui était riche,
19:3	et il cherchait à voir qui était Yéhoshoua, mais il ne le pouvait pas à cause de la foule, parce qu'il était de petite stature.
19:4	Et courant en avant, il monta sur un sycomore pour le voir, parce qu'il était sur le point de passer par là.
19:5	Et quand il fut arrivé à cet endroit-là, Yéhoshoua levant les yeux, le vit et lui dit : Zakkay, hâte-toi de descendre, car il faut que je demeure aujourd'hui dans ta maison.
19:6	Et il se hâta de descendre et le reçut avec joie.
19:7	Et tous, voyant cela, murmuraient en disant : Il est entré chez un homme pécheur pour y loger.
19:8	Mais Zakkay, se tenant debout, dit au Seigneur : Voici, Seigneur, je donne la moitié de mes biens aux pauvres et, si j'ai extorqué<!--Le mot grec signifie : « accuser à tort, calomnier, attaquer avec des desseins méchants, extorquer de l'argent ou frauder ». À Athènes, ceux qui étaient « sukophantia » avaient le rôle d'informer contre ceux qui faisaient de l'exportation illicite de figues ; et ils pouvaient quelquefois extorquer de l'argent des contrevenants. Le mot « sukophantes » était un terme général d'opprobre, pour désigner un malin et un vulgaire accusateur par amour du gain.--> de l'argent à quelqu'un, je lui rends le quadruple<!--Lé. 5:20-24 ; 2 S. 12:6.-->.
19:9	Et Yéhoshoua lui dit : Aujourd'hui, le salut est arrivé pour cette maison, parce que celui-ci aussi est fils d'Abraham.
19:10	Car le Fils d'humain est venu chercher et sauver ce qui était perdu.

### Parabole des dix mines<!--Lu. 17:21.-->

19:11	Et comme ils entendaient ces choses, il ajouta et leur dit une parabole, parce qu'il était près de Yeroushalaim, et qu'ils pensaient que le Royaume d'Elohîm allait immédiatement paraître.
19:12	Il dit donc : Un homme noble s'en alla dans une contrée éloignée pour recevoir un royaume et revenir.
19:13	Et ayant appelé dix de ses esclaves, il leur donna 10 mines et leur dit : Continuez les affaires jusqu'à ce que je vienne.
19:14	Or ses concitoyens le haïssaient, et ils envoyèrent une ambassade après lui pour dire : Nous ne voulons pas que celui-ci règne sur nous.
19:15	Et il arriva donc, après qu'il fut de retour, et après avoir reçu le royaume, qu'il fit appeler auprès de lui ses esclaves auxquels il avait donné l'argent, afin de savoir ce que chacun avait gagné en affaires.
19:16	Et le premier vint et dit : Seigneur, ta mine a produit 10 autres mines.
19:17	Et il lui dit : C'est bien, bon esclave ! Parce que tu as été fidèle dans une petite chose, reçois autorité sur 10 villes.
19:18	Et le second vint, et dit : Seigneur, ta mine a produit 5 mines.
19:19	Et il dit aussi à celui-ci : Toi aussi, sois établi sur 5 villes.
19:20	Et un autre vint et dit : Seigneur, voici ta mine que j'ai gardée enveloppée dans un mouchoir.
19:21	Car j'avais peur de toi, parce que tu es un homme sévère : tu prends ce que tu n'as pas déposé, et tu moissonnes ce que tu n'as pas semé.
19:22	Et il lui dit : C’est à partir de ta propre bouche que je te jugerai, toi, méchant esclave : tu savais que je suis un homme sévère, prenant ce que je n'ai pas déposé, et moissonnant ce que je n'ai pas semé.
19:23	Et en raison de quoi n'as-tu pas mis mon argent dans une banque<!--La table ou comptoir d'un changeur de monnaie, où il se tenait pour l'échange de différentes sortes d'argent en prenant un pourcentage (un agio) et en versant un intérêt sur ses emprunts ou les dépôts.-->, afin qu'à mon retour je le retire avec un intérêt ?
19:24	Et il dit à ceux qui étaient présents : Ôtez-lui la mine et donnez-la à celui qui a les 10 mines !
19:25	Et ils lui dirent : Seigneur, il a 10 mines !
19:26	Car je vous dis qu’on donnera à quiconque a, mais à celui qui n'a rien, on ôtera même ce qu'il a.
19:27	Au reste, amenez ici mes ennemis qui n'ont pas voulu que je règne sur eux, et tuez-les devant moi.

### Yéhoshoua fait son entrée à Yeroushalaim (Jérusalem)<!--Za. 9:9 ; Mt. 21:1-11 ; Mc. 11:1-11 ; Jn. 12:12-19.-->

19:28	Et après avoir dit ces choses, il allait devant, montant à Yeroushalaim.
19:29	Et il arriva qu'en approchant de Bethphagé et de Béthanie, près de la montagne appelée des Oliviers, il envoya deux des disciples,
19:30	en disant : Allez au village qui est en face et, en y entrant, vous trouverez un ânon attaché, sur lequel aucun être humain n'est monté. Détachez-le et amenez-le-moi.
19:31	Et si quelqu'un vous demande : En raison de quoi le détachez-vous ? Vous lui direz : Parce que le Seigneur en a besoin.
19:32	Et ceux qui étaient envoyés s'en allèrent et le trouvèrent comme il leur avait dit.
19:33	Et comme ils le détachaient, ses seigneurs leur dirent : Pourquoi détachez-vous cet ânon ?
19:34	Et ils dirent : Parce que le Seigneur en a besoin.
19:35	Et ils emmenèrent à Yéhoshoua l'ânon, sur lequel ils jetèrent leurs vêtements, et firent monter Yéhoshoua dessus.
19:36	Or, pendant qu'il avançait, ils étendaient leurs vêtements sur la route.
19:37	Et il approchait déjà de la descente de la montagne des Oliviers lorsque toute la multitude des disciples, se réjouissant, se mit à louer Elohîm à grande voix pour tous les miracles qu'ils avaient vus,
19:38	disant : Béni soit le Roi qui vient au Nom du Seigneur<!--Ps. 118:26.--> ! Paix dans le ciel, et gloire dans les lieux très hauts.
19:39	Et quelques pharisiens, du milieu de la foule, lui dirent : Docteur, réprimande d'une manière tranchante tes disciples !
19:40	Et répondant, il leur dit : Je vous le dis, s'ils se taisent, les pierres crieront.

### Nouvelles lamentations de Yéhoshoua sur Yeroushalaim<!--Cp. Mt. 23:37-39 ; Lu. 13:34-35.-->

19:41	Et comme il approchait, voyant la ville, il pleura sur elle, 
19:42	en disant : Si tu avais su toi aussi, du moins en ce jour qui est le tien, les choses qui appartiennent à ta paix ! Mais maintenant elles sont cachées à tes yeux.
19:43	Car des jours viendront sur toi, où tes ennemis t'entoureront de tranchées, et t'environneront, et te serreront de tous côtés,
19:44	et ils te jetteront à terre, toi et tes enfants qui sont au milieu de toi, et ils ne laisseront pas en toi pierre sur pierre, parce que tu n'as pas connu le temps de ta visite d'inspection<!--« C'est l'acte par lequel Elohîm visite les hommes, observe leurs voies, leurs caractères, pour leur accorder en partage joie ou tristesse », « contrôle », « la fonction d'un ancien ». 1 Pi. 2:12.-->.

### Yéhoshoua chasse les marchands du temple

19:45	Et, étant entré dans le temple, il se mit à chasser dehors ceux qui y vendaient et qui y achetaient,
19:46	en leur disant : Il est écrit : Ma maison est la maison de prière. Mais vous, vous en avez fait une caverne de brigands<!--Es. 56:7 ; Jé. 7:11.--> !
19:47	Et il enseignait tous les jours dans le temple. Et les principaux prêtres et les scribes, et les principaux du peuple cherchaient à le faire mourir.
19:48	Mais ils ne savaient comment s'y prendre, car tout le peuple s'attachait à lui pour l'entendre.

## Chapitre 20

### L'autorité de Yéhoshoua et celle de Yohanan le Baptiste<!--Mt. 21:23-27 ; Mc. 11:27-33.-->

20:1	Et il arriva qu'un de ces jours-là, comme il enseignait le peuple dans le temple et évangélisait, les principaux prêtres et les scribes avec les anciens, survinrent
20:2	et lui parlèrent en disant : Dis-nous par quelle autorité fais-tu ces choses, ou qui est celui qui t'a donné cette autorité ?
20:3	Mais répondant il leur dit : Je vous interrogerai, moi aussi sur une parole. Dites-moi :
20:4	Le baptême de Yohanan était-il issu du ciel ou issu des humains ?
20:5	Mais ils raisonnaient entre eux, en disant : Si nous répondons : Issu du ciel, il dira : En raison de quoi ne l'avez-vous pas cru ?
20:6	Mais si nous répondons : Issu des humains, tout le peuple nous lapidera, car il est persuadé que Yohanan est un prophète.
20:7	Et ils répondirent qu’ils ne savaient pas d’où il venait.
20:8	Et Yéhoshoua leur dit : Moi non plus, je ne vous dirai pas par quelle autorité je fais ces choses.

### Parabole des vignerons<!--Es. 5:1-7 ; Mt. 21:33-46 ; Mc. 12:1-12.-->

20:9	Mais il se mit à dire au peuple cette parabole : Un homme planta une vigne, et la laissa en location à des vignerons, et s'en alla hors du pays pour longtemps.
20:10	Et au temps favorable et opportun, il envoya un esclave vers les vignerons, afin qu'ils lui donnent du fruit de la vigne. Mais les vignerons le battirent et le renvoyèrent à vide.
20:11	Et il leur envoya encore un autre esclave, mais l'ayant aussi battu et déshonoré, ils le renvoyèrent à vide.
20:12	Et il en envoya encore un troisième, mais ils le blessèrent aussi, et le jetèrent dehors.
20:13	Mais le maître de la vigne dit : Que ferai-je ? J'enverrai mon fils bien-aimé, peut-être que quand ils le verront, ils le respecteront.
20:14	Mais quand les vignerons le virent, ils raisonnèrent entre eux, et dirent : Voici l'héritier. Venez, tuons-le, afin que l'héritage soit à nous.
20:15	Et ils le jetèrent hors de la vigne, et le tuèrent. Que leur fera donc le maître de la vigne ?
20:16	Et il viendra, et fera périr ces vignerons-là, et il donnera la vigne à d'autres. Lorsqu'ils entendirent cela, ils dirent : Que cela n'arrive jamais !
20:17	Et, fixant sur eux son regard, il dit : Que veut donc dire ce qui est écrit : La pierre qu'ont rejetée ceux qui bâtissaient est devenue la tête de l'angle<!--Ps. 118:22.--> ?
20:18	Quiconque tombera sur cette pierre-ci sera brisé ; et quant à celui sur qui elle tombera, elle l'écrasera en morceaux.
20:19	Et les principaux prêtres et les scribes cherchèrent à mettre la main sur lui à l'heure même, mais ils craignirent le peuple. Car ils comprirent que c'était pour eux qu'il avait dit cette parabole.

### Le tribut à César<!--Mt. 22:15-22 ; Mc. 12:13-17.-->

20:20	Et l'observant attentivement, ils envoyèrent des espions qui faisaient semblant<!--« Prendre à son compte les déclarations d'un autre », « répliquer », « donner une réplique (parler) sur une scène », « personnifier », « représenter un personnage », « jouer un rôle ».--> d'être justes, afin de saisir de lui quelque parole pour le livrer au magistrat et à l'autorité du gouverneur.
20:21	Et ils l'interrogèrent, en disant : Docteur, nous savons que tu parles et enseignes droitement, et que tu ne regardes pas à l'apparence, mais que tu enseignes la voie d'Elohîm selon la vérité.
20:22	Est-il légal pour nous de payer l'impôt à César, ou non ?
20:23	Mais comprenant leur ruse, il leur dit : Pourquoi me tentez-vous ?
20:24	Montrez-moi un denier. De qui a-t-il l'image et l'inscription ? Et répondant, ils dirent : De César.
20:25	Et il leur dit : Rendez donc à César ce qui est à César, et à Elohîm ce qui est à Elohîm.
20:26	Et ils ne purent le prendre en défaut sur aucune parole devant le peuple et, étonnés de sa réponse, ils gardèrent le silence.

### Enseignement sur la résurrection<!--Mt. 22:23-33 ; Mc. 12:18-27.-->

20:27	Mais quelques-uns des sadducéens, qui contestent qu'il y ait une résurrection, s'approchèrent et l'interrogèrent,
20:28	disant : Docteur, Moshé a écrit pour nous : Si le frère de quelqu'un meurt, ayant une femme et pas d'enfants, son frère épousera la femme et suscitera une postérité à son frère.
20:29	Or il y avait sept frères. Et le premier ayant pris une femme, mourut sans enfants.
20:30	Et le second prit la femme, et mourut aussi sans enfants.
20:31	Et le troisième la prit, et de même aussi les sept. Et ils ne laissèrent pas d'enfants et moururent.
20:32	Mais après eux tous la femme mourut aussi.
20:33	À la résurrection, duquel d'entre eux donc devient-elle la femme ? Car les sept l'ont eue pour femme.
20:34	Et Yéhoshoua répondant leur dit : Les fils de cet âge<!--Du grec « aion » qui signifie « période », « temps », etc.--> se marient et donnent en mariage.
20:35	Mais ceux qui ont été jugés dignes d'avoir part à cet âge-là et à la résurrection d’entre les morts, ne se marient ni ne donnent en mariage.
20:36	Car ils ne peuvent même pas mourir, parce qu'ils sont semblables aux anges, et qu'ils sont fils d'Elohîm, étant fils de la résurrection.
20:37	Mais que les morts sont réveillés, Moshé lui-même l'a révélé<!--Faire connaître une chose secrète.--> au buisson, quand il appelle le Seigneur l'Elohîm d'Abraham, et l'Elohîm de Yitzhak, et l'Elohîm de Yaacov.
20:38	Or il n'est pas l'Elohîm des morts, mais des vivants, car tous vivent en lui.

### Yéhoshoua dénonce les scribes<!--Cp. Mt. 22:41-23:36 ; Mc. 12:35-40.-->

20:39	Et quelques-uns des scribes, répondant, dirent : Docteur, tu as bien parlé.
20:40	Et ils n'osaient plus l'interroger sur rien.
20:41	Mais il leur dit : Comment dit-on que le Mashiah est Fils de David ?
20:42	Et David lui-même dit dans le livre des psaumes : Le Seigneur a dit à mon Seigneur : Assieds-toi à ma droite,
20:43	jusqu'à ce que j'aie mis tes ennemis pour le marchepied de tes pieds<!--Ps. 110:1.-->.
20:44	David donc l'appelle son Seigneur, comment est-il son Fils ?
20:45	Mais comme tout le peuple écoutait, il dit à ses disciples :
20:46	Gardez-vous des scribes, qui prennent plaisir à se promener en robes longues, qui aiment les salutations sur les places du marché, et les premiers sièges dans les synagogues, et les premières places dans les soupers,
20:47	qui dévorent entièrement les maisons des veuves, et qui font pour l'apparence de longues prières. Ils en recevront une plus grande condamnation.

## Chapitre 21

### Offrande de la pauvre veuve<!--Mc. 12:41-44.-->

21:1	Or, levant les yeux, il vit des riches jetant leurs offrandes dans le trésor.
21:2	Mais il vit aussi une pauvre veuve qui y jetait deux petites pièces de monnaie.
21:3	Et il dit : Je vous le dis en vérité, cette pauvre veuve a jeté plus que tous.
21:4	Car tous ceux-là ont jeté dans les offrandes d'Elohîm de leur abondance, mais elle, de son insuffisance, elle a jeté tout ce qu'elle avait : son bien.

### Enseignement sur le Mont des Oliviers<!--Mt. 24-25 ; Mc. 13.-->

21:5	Et comme quelques-uns parlaient du temple, qui était orné de belles pierres et d'offrandes, il dit :
21:6	Est-ce cela que vous regardez ? Les jours viendront où il ne restera pas pierre sur pierre qui ne soit détruite.

### Les disciples posent deux questions à Yéhoshoua<!--Mt. 24:3 ; Mc. 13:3-4.-->

21:7	Et ils l'interrogèrent en disant : Docteur, quand donc cela aura-t-il lieu, et quel sera le signe que cela est sur le point d’arriver ?

### Les temps de la fin<!--Mt. 24:4-14 ; Mc. 13:5-13.-->

21:8	Et il dit : Discernez ! Ne soyez pas égarés, car beaucoup viendront en mon nom, en disant que « moi je suis », et le temps approche. Ne les suivez pas.
21:9	Mais quand vous entendrez parler de guerres et de désordres<!--1 Co. 14:33 ; Ja. 3:16.-->, ne soyez pas terrifiés, car il faut que ces choses arrivent premièrement. Mais ce ne sera pas encore la fin.
21:10	Alors il leur dit : Nation se réveillera contre nation et royaume contre royaume.
21:11	Il y aura de grands tremblements de terre en divers lieux, des famines et des pestes, et il y aura des choses qui frappent de terreur et de grands signes dans le ciel.

### La souffrance des croyants

21:12	Mais, avant toutes ces choses, ils mettront les mains sur vous et vous persécuteront, vous livrant aux synagogues et aux prisons, et vous serez menés devant les rois et les gouverneurs, à cause de mon nom.
21:13	Et cela se tournera pour vous en témoignage.
21:14	Mettez donc dans vos cœurs de ne pas préméditer votre défense.
21:15	Car je vous donnerai une bouche et une sagesse à laquelle aucun de vos adversaires ne pourra contredire ni résister.
21:16	Mais vous serez livrés même par les parents, par les frères, par les proches et par vos amis, et ils feront mourir beaucoup d'entre vous.
21:17	Et vous serez haïs de tous à cause de mon Nom.
21:18	Mais il ne se perdra jamais un cheveu de votre tête.
21:19	Possédez vos âmes par votre persévérance.

### La destruction de Yeroushalaim (Jérusalem) prophétisée

21:20	Mais quand vous verrez Yeroushalaim encerclée par des armées, sachez alors que sa désolation est proche.
21:21	Alors, que ceux qui seront en Judée fuient dans les montagnes, et que ceux qui seront au milieu d'elle en sortent, et que ceux qui seront dans les champs n'entrent pas en elle.
21:22	Car ce sont là des jours de vengeance, afin que soient accomplies toutes les choses qui sont écrites.
21:23	Mais malheur aux femmes qui seront enceintes et à celles qui allaiteront en ces jours-là ! Car il y aura une grande calamité sur le pays, et de la colère contre ce peuple.
21:24	Et ils tomberont à bouche d’épée, ils seront emmenés captifs<!--Les Romains, installés à Kena'ân (Canaan) depuis 65 av. J.-C., essuyèrent plusieurs révoltes juives. L'une des plus connues est celle organisée par les zélotes (voir commentaire en Lu. 6:15) en août 66 et qui aboutit en 70 à la ruine de Yeroushalaim (Jérusalem) et à la destruction du temple par l'armée de Titus (39-81). Soixante-deux ans plus tard, en 132, Bar Kokhba organisa une armée après avoir établi un état Juif en Judée et déclencha une rébellion qui deviendra la seconde guerre judéo-romaine. En 135, les légionnaires de l'empereur Hadrien (76-138) matèrent définitivement l'insurrection dans un bain de sang qui coûta la vie à quelques 580 000 Judéens. Yeroushalaim fut alors totalement détruite et interdite aux Juifs. Ces événements marquèrent le début de la diaspora juive dans le monde entier.--> parmi toutes les nations, et Yeroushalaim sera foulée aux pieds par les nations, jusqu'à ce que les temps<!--Du grec « kairos », qui signifie mesure de temps, portion de temps, ou encore une période limitée de temps.--> des nations soient accomplis<!--Du grec « pleroo », remplir jusqu'au bord, faire abonder, remplir au sommet, à ras bord, etc. Voir Ge. 15:16.-->.
### Retour du Mashiah (Christ) sur la Terre<!--Mt. 24:29-31 ; Mc. 13:24-27.-->

21:25	Et il y aura des signes dans le soleil, et dans la lune et dans les étoiles<!--Es. 13:10 ; Mt. 24:29.-->, et sur la Terre, l’oppression<!--« La partie resserrée » d'une racine qui signifie « comprimer », « presser ensemble avec la main », « presser de chaque côté », « une cité assiégée. ».--> des nations en perplexité à cause du bruit de la mer et de son agitation<!--l'agitation ou gonflement de la mer.-->,
21:26	et les humains expirant de frayeur dans l'attente des choses qui surviendront sur toute la terre habitée, car les puissances des cieux seront ébranlées.
21:27	Et alors, ils verront le Fils d'humain venant sur une nuée<!--Es. 19:1 ; Ap. 1:7.--> avec puissance et grande gloire.
21:28	Mais quand ces choses commenceront à arriver, regardez en haut et levez vos têtes, parce que votre rédemption approche.

### Parabole du figuier<!--Mt. 24:29-31 ; Mc. 13:24-27.-->

21:29	Et il leur proposa une parabole : Voyez le figuier et tous les autres arbres.
21:30	Dès qu'ils ont poussé, vous savez de vous-mêmes, en regardant, que l'été est déjà proche.
21:31	Vous aussi de même, quand vous verrez arriver ces choses, sachez que le Royaume d'Elohîm est proche.
21:32	Amen, je vous le dis, cette génération ne passera jamais, que toutes ces choses ne soient arrivées.
21:33	Le ciel et la Terre passeront, mais mes paroles ne passeront jamais.

### Exhortation à veiller<!--Mt. 24:36-51 ; Mc. 13:32-37.-->

21:34	Mais faites attention à vous-mêmes, de peur que vos cœurs ne soient surchargés par le vertige et mal de tête causés en buvant du vin à l'excès ou l'ivrognerie, et par les soucis de cette vie, et que ce jour-là ne vous surprenne subitement.
21:35	Car il surprendra comme un piège tous ceux qui habitent sur la surface de toute la Terre.
21:36	Veillez donc, priant en tout temps, afin que vous soyez jugés dignes d'échapper à toutes ces choses qui sont sur le point d'arriver, et de vous tenir debout devant le Fils d'humain.
21:37	Et pendant le jour, il enseignait dans le temple, et il sortait pour passer la nuit en plein air à la montagne appelée des Oliviers.
21:38	Et dès le point du jour, tout le peuple venait vers lui au temple pour l'entendre.

## Chapitre 22

### Trahison de Yéhouda Iskariote<!--Mt. 26:14-16 ; Mc. 14:1-2,10-11.-->

22:1	Or la fête des pains sans levain, qu'on appelle Pâque, approchait.
22:2	Et les principaux prêtres et les scribes cherchaient comment le tuer, car ils avaient peur du peuple.
22:3	Mais Satan entra dans Yéhouda, surnommé Iskariote, qui était du nombre des douze.
22:4	Et il s'en alla et parla avec les principaux prêtres et les chefs des strategos<!--« Le commandant d'une armée », « un commandant civil », « un gouverneur ». C'était le nom du plus haut magistrat dans les colonies romaines. Ils avaient le pouvoir d'administrer la justice pour les cas importants. C'était aussi le nom du « commandant du temple », c'est-à-dire le commandant des Lévites qui montaient la garde dans et autour du temple.-->, sur la manière de le leur livrer.
22:5	Et ils se réjouirent et convinrent de lui donner de l'argent.
22:6	Et il s'y engagea, et il cherchait une occasion favorable pour le leur livrer à l'insu de la foule.

### La dernière Pâque<!--Mt. 26:17-25 ; Mc. 14:12-21 ; Jn. 13:1-12.-->

22:7	Or le jour des pains sans levain où l'on devait immoler la Pâque arriva.
22:8	Et il envoya Petros et Yohanan en disant : Allez nous préparer la Pâque, pour que nous la mangions.
22:9	Mais ils lui dirent : Où veux-tu que nous la préparions ?
22:10	Et il leur dit : Voici, quand vous serez entrés dans la ville, vous rencontrerez un homme portant une cruche d'eau, suivez-le dans la maison où il entrera.
22:11	Et dites au maître de la maison : Le Docteur te dit : Où est l'auberge où je mangerai la Pâque avec mes disciples ?
22:12	Et il vous montrera une grande chambre haute, meublée : c'est là que vous préparerez.
22:13	Et ils partirent et trouvèrent les choses comme il leur avait dit, et ils préparèrent la Pâque.
22:14	Et quand l'heure fut venue, il se mit à table, et les douze apôtres avec lui.
22:15	Et il leur dit : J’ai désiré<!--Ou « J'ai convoité de convoitise ».--> d’un désir de manger cette Pâque avec vous avant de souffrir.
22:16	Car, je vous le dis, je n'en mangerai plus jamais jusqu'à ce qu'elle soit accomplie dans le Royaume d'Elohîm.
22:17	Et, ayant pris la coupe, il rendit grâce, et il dit : Prenez-la, et distribuez-la entre vous.
22:18	Car, je vous le dis, je ne boirai plus du fruit de la vigne jusqu'à ce que le Royaume d'Elohîm soit venu.

### Le repas de la Pâque<!--Mt. 26:26-29 ; Mc. 14:22-25 ; cp. Jn. 13:12-30 ; 1 Co. 11:23-26.-->

22:19	Et prenant du pain, et ayant rendu grâce, il le rompit et le leur donna en disant : Ceci est mon corps qui est donné en votre faveur. Faites ceci en mémoire de moi.
22:20	De même aussi, il prit la coupe après le souper, et la leur donna, en disant : Cette coupe est la nouvelle alliance en mon sang, qui est répandu en votre faveur.

### Yéhoshoua annonce qu'il sera livré<!--Mt. 26:21-25 ; Mc. 14:18-21 ; Jn. 13:18-30.-->

22:21	Cependant, voici que la main de celui qui me livre est avec moi à cette table.
22:22	En effet, le Fils d'humain s'en va selon ce qui est décrété, mais malheur à cet humain par le moyen duquel il est livré !
22:23	Et ils commencèrent à discuter entre eux : qui donc parmi eux était sur le point de commettre cela ?

### Leçon d'humilité<!--Mt. 20:20-28 ; Mc. 9:33-37, 10:35-45 ; Jn. 13:1-17.-->

22:24	Or il s'éleva aussi parmi eux une dispute : Lequel d'entre eux semble être le plus grand ?
22:25	Mais il leur dit : Les rois des nations dominent en seigneurs<!--1 Ti. 6:15.--> sur elles, et ceux qui exercent l'autorité sur elles sont appelés bienfaiteurs<!--Du grec « yoo-erg-et'-ace ». C'est un titre d'honneur, décerné à celui qui a rendu des services dans sa région, et aux princes, équivalent à Sôter, Pater Patriae (Père de la Patrie).-->.
22:26	Mais il n'en sera pas ainsi de vous : Au contraire, que le plus grand parmi vous devienne comme le plus jeune, et celui qui dirige comme celui qui sert.
22:27	Car lequel est le plus grand, celui qui est à table, ou celui qui sert ? N'est-ce pas celui qui est à table ? Or je suis au milieu de vous comme celui qui sert.
22:28	Mais vous, vous êtes ceux qui sont restés avec moi d'une façon permanente dans mes tentations.
22:29	Et moi, je vous fais un testament<!--Ac. 3:25 ; Hé. 8:10, 9:16-17.--> pour le royaume, comme mon Père m'a fait un testament,
22:30	afin que vous mangiez et que vous buviez à ma table dans mon Royaume, et que vous soyez assis sur des trônes, pour juger les douze tribus d'Israël.

### Yéhoshoua prophétise le triple reniement de Petros (Pierre)<!--Mt. 26:30-35 ; Mc. 14:26-31 ; Jn. 13:36-38.-->

22:31	Mais le Seigneur dit : Shim’ôn, Shim’ôn, voici, Satan vous a réclamés pour vous cribler comme le blé.
22:32	Mais moi, j'ai prié pour toi, afin que la foi ne vienne pas à te manquer. Toi donc, quand tu seras converti<!--« Se tourner vers », « retourner », « revenir », « ramener ».-->, rends tes frères stables<!--« Rendre stable », « placer fermement », « fixer », « fortifier », « renforcer », « rendre ferme », « rendre constant », « confirmer son esprit ». Pr. 4:26.-->.
22:33	Mais il lui dit : Seigneur, avec toi, je suis prêt à aller, et en prison et à la mort.
22:34	Mais il lui dit : Petros, je te dis que le coq ne chantera pas aujourd'hui, que premièrement tu n'aies nié trois fois de me connaître.

### Recommandations aux disciples<!--Cp. Jn. 14-16 ; contraste Mt. 10:9-13.-->

22:35	Et il leur dit : Quand je vous ai envoyés sans bourse, sans sac et sans sandales, avez-vous manqué de quelque chose ? Ils répondirent : De rien.
22:36	Et il leur dit : Mais maintenant, que celui qui a une bourse la prenne, de même aussi un sac, et que celui qui n'a pas d'épée vende son vêtement et en achète une.
22:37	Car je vous dis qu'il faut que ceci encore qui est écrit, s’accomplisse en moi : Et il a été compté parmi les violeurs de la torah<!--Du grec « anomos » qui veut dire « sans loi », « impie », etc. Es. 53:12.-->. Car aussi les choses qui me concernent ont une fin.
22:38	Et ils dirent : Seigneur, voici ici deux épées. Et il leur dit : C'est assez.

### Gethsémané<!--Mt. 26:36-46 ; Mc. 14:32-42 ; Jn. 18:1 ; cp. Hé. 5:7-8.-->

22:39	Et étant sorti, il s’en alla, selon sa coutume, à la montagne des Oliviers. Et ses disciples aussi le suivirent.
22:40	Et lorsqu'il fut arrivé à cet endroit, il leur dit : Priez afin que vous n'entriez pas en tentation.
22:41	Et s'étant éloigné d'eux à la distance d'environ un jet de pierre, et s'étant mis à genoux, il priait,
22:42	en disant : Père, si tu voulais éloigner cette coupe loin de moi ! Toutefois, que ma volonté ne soit pas faite, mais la tienne.
22:43	Et un ange venu du ciel lui apparut, le fortifiant.
22:44	Et étant en agonie, il priait plus attentivement, et sa sueur devint comme des grumeaux de sang qui tombaient à terre.
22:45	Et s'étant levé de sa prière, il vint vers ses disciples et les trouva endormis de douleur.
22:46	Et il leur dit : Pourquoi dormez-vous ? Levez-vous, et priez, afin que vous n'entriez pas en tentation.

### Trahison de Yéhouda (Judas) Iskariote<!--Mt. 26:47-54 ; Mc. 14:43-47 ; Jn. 18:2-11.-->

22:47	Mais comme il parlait encore, voici une foule. Et celui qu’on appelle Yéhouda, l'un des douze, marchait devant elle. Il s'approcha de Yéhoshoua pour lui donner un baiser.
22:48	Mais Yéhoshoua lui dit : Yéhouda, c'est par un baiser que tu livres le Fils d'humain ?
22:49	Mais ceux qui étaient autour de lui, voyant ce qui allait arriver, lui dirent : Seigneur, frapperons-nous de l'épée ?
22:50	Et l'un d'eux frappa l'esclave du grand-prêtre et lui emporta l'oreille droite.
22:51	Mais Yéhoshoua répondant, dit : Laissez ! Jusque-là ! Et, ayant touché son oreille, il le guérit.
22:52	Et Yéhoshoua dit aux principaux prêtres, aux chefs des strategos<!--Voir commentaire en Lu. 22:4.--> du temple et aux anciens qui étaient venus contre lui : Vous êtes venus comme contre un brigand, avec des épées et des bâtons.
22:53	J'étais tous les jours avec vous dans le temple et vous n'avez pas mis la main sur moi. Mais c'est ici votre heure et l'autorité de la ténèbre<!--Voir Ac. 26:18 et Col. 1:13.-->.

### Le triple reniement de Petros (Pierre)<!--Mt. 26:55-58,69-75 ; Mc. 14:48-54,66-72 ; Jn. 18:15-18,25-27.-->

22:54	Et l'ayant pris, ils l'emmenèrent et le conduisirent dans la maison du grand-prêtre. Or Petros suivait de loin.
22:55	Mais comme ils avaient allumé un feu au milieu de la cour et s'étaient assis ensemble, Petros s'assit au milieu d'eux.
22:56	Et une servante, le voyant assis près de la lumière, fixa sur lui les yeux, et dit : Celui-ci aussi était avec lui.
22:57	Mais il le nia, en disant : Femme, je ne le connais pas.
22:58	Et peu après, un autre le voyant, dit : Toi aussi tu fais partie de ces gens-là. Mais Petros dit : Homme ! Je n'en suis pas.
22:59	Et environ une heure plus tard, un autre l'affirmait vaillamment en disant : Certainement, celui-ci aussi était avec lui, car il est galiléen.
22:60	Mais Petros dit : Homme ! Je ne sais pas ce que tu dis. Et immédiatement, comme il parlait encore, le coq chanta.
22:61	Et le Seigneur, s'étant retourné, regarda Petros. Et Petros se souvint de la parole que le Seigneur lui avait dite : Avant que le coq chante, tu me renieras trois fois.
22:62	Et Petros, étant sorti dehors, pleura amèrement.

### Yéhoshoua est outragé<!--Mt. 26:67-68 ; Mc. 14:65 ; Jn. 18:22-23.-->

22:63	Et les hommes qui tenaient Yéhoshoua se jouaient de lui en le frappant.
22:64	Et l'ayant voilé, ils lui donnaient des coups sur le visage, et l'interrogeaient, en disant : Devine qui est celui qui t'a frappé ?
22:65	Et ils proféraient contre lui beaucoup d'autres blasphèmes.

### Yéhoshoua déclare qu'il est le Fils d'Elohîm<!--Mt. 26:59-68, 27:1 ; Mc. 14:55-65, 15:1 ; Jn. 18:19-24.-->

22:66	Et quand le jour fut venu, le corps des anciens du peuple, principaux prêtres et scribes, s’assembla, et l’amenèrent dans leur sanhédrin, disant :
22:67	Si tu es le Mashiah, dis-le-nous. Mais il leur dit : Si je vous le dis, vous ne le croirez jamais.
22:68	Et si je vous interroge, vous ne me répondrez jamais, et vous ne me laisserez pas aller.
22:69	Désormais le Fils d'humain sera assis à la droite de la puissance d'Elohîm.
22:70	Et ils dirent tous : Tu es donc le Fils d'Elohîm ? Et il leur dit : Vous le dites, je suis<!--Voir commentaire en Ex. 3:14.-->.
22:71	Et ils dirent : Qu'avons-nous besoin encore de témoignage ? Nous l'avons entendu nous-mêmes de sa bouche.

## Chapitre 23

### Yéhoshoua comparaît devant Pilate<!--Mt. 27:2,11-14 ; Mc. 15:1-5 ; Jn. 18:28-38.-->

23:1	Et toute leur multitude s’étant levée, ils le conduisirent devant Pilate.
23:2	Et ils se mirent à l'accuser, disant : Nous avons trouvé celui-ci pervertissant la nation et empêchant de payer les impôts à César, disant qu'il est lui-même Mashiah, Roi.
23:3	Et Pilate l'interrogea, en disant : Es-tu le Roi des Juifs ? Et répondant il lui dit : Tu le dis.
23:4	Et Pilate dit aux principaux prêtres et aux foules : Je ne trouve aucune faute en cet homme.
23:5	Mais ils insistaient, en disant : Il soulève le peuple, enseignant par toute la Judée, depuis la Galilée où il a commencé, jusqu'ici.

### Yéhoshoua est envoyé à Hérode

23:6	Or Pilate, ayant entendu parler de la Galilée, demanda si cet homme était galiléen,
23:7	et, ayant appris qu'il vient de la juridiction<!--Vient du grec « exousia » qui signifie « pouvoir ou règle ou gouvernement (pouvoir auquel un commandement exige que nous soyons soumis) », « autorité ».--> d'Hérode, il l'envoya à Hérode qui se trouvait aussi à Yeroushalaim en ces jours-là.
23:8	Et lorsque Hérode vit Yéhoshoua, il en eut une grande joie, car depuis longtemps il désirait le voir, à cause de beaucoup de choses qu'il entendait le concernant, et il espérait qu'il le verrait produire quelque signe.
23:9	Et il l'interrogea avec beaucoup de discours, mais il ne lui répondit rien.
23:10	Et les principaux prêtres et les scribes se tenaient là, l'accusant avec véhémence.
23:11	Mais Hérode, avec ses bandes de soldats, le traita avec mépris et, après s'être joué de lui et l'avoir revêtu d'un vêtement éclatant, il le renvoya à Pilate.
23:12	Et ce même jour, Pilate et Hérode devinrent amis, car auparavant ils étaient en inimitié l'un avec l'autre.

### Hérode renvoie Yéhoshoua à Pilate<!--Mt. 27:15-26 ; Mc. 15:6-15 ; Jn. 18:39-19:15.-->

23:13	Mais Pilate, ayant rassemblé les principaux prêtres, les magistrats et le peuple, 
23:14	leur dit : Vous m'avez présenté cet homme comme détournant le peuple. Et voilà que, l'ayant moi-même examiné devant vous, je n’ai trouvé en cet homme aucune faute dont vous l'accusez.
23:15	Mais Hérode non plus, car je vous ai renvoyés à lui, et voici, rien n’a été fait par lui qui soit digne de mort.
23:16	L’ayant donc châtié, je le relâcherai.
23:17	Or il était obligé de leur relâcher quelqu'un à la fête.
23:18	Mais ils s'écrièrent tous ensemble, en disant : Ôte celui-ci et relâche-nous Barabbas !
23:19	Cet homme, à cause d'une insurrection qui s'était faite dans la ville et d'un meurtre, avait été jeté en prison.
23:20	Pilate donc leur adressa de nouveau la parole, voulant relâcher Yéhoshoua.
23:21	Mais ils criaient, en disant : Crucifie, crucifie-le !
23:22	Et il leur dit pour la troisième fois : Mais quel mal a fait cet homme ? Je ne trouve en lui aucune faute qui soit digne de mort. Après l’avoir châtié, je le relâcherai.
23:23	Mais ils insistèrent à grands cris, demandant qu'il soit crucifié. Et leurs cris et ceux des principaux prêtres l'emportèrent.
23:24	Et Pilate décréta qu'il serait fait selon leur requête.
23:25	Et il leur relâcha celui qui avait été jeté en prison à cause d'une insurrection et d'un meurtre, et qu'ils demandaient, et il livra Yéhoshoua à leur volonté.

### Sur le chemin de Golgotha<!--Mt. 27:31-32 ; Mc. 15:20-21 ; Jn. 19:16-17.-->

23:26	Et comme ils l'emmenaient, ils prirent un certain Shim’ôn de Cyrène, qui revenait des champs, et le chargèrent de la croix pour qu'il la porte derrière Yéhoshoua.
23:27	Et il était suivi d'une grande multitude du peuple et de femmes qui se frappaient la poitrine de chagrin et se lamentaient sur lui.
23:28	Mais Yéhoshoua se tournant vers elles, dit : Filles de Yeroushalaim, ne pleurez pas sur moi, mais pleurez sur vous-mêmes et sur vos enfants.
23:29	Parce que, voici, des jours viennent où l’on dira : Heureuses les stériles, les entrailles qui n'ont pas enfanté, et les mamelles qui n'ont pas allaité !
23:30	Alors ils se mettront à dire aux montagnes : Tombez sur nous. Et aux collines : Couvrez-nous<!--Es. 2:19-21 ; Ap. 6:16.--> !
23:31	Parce que s'ils font ces choses au bois vert, que deviendra le sec ?
23:32	Et deux autres aussi, qui étaient des malfaiteurs, furent menés pour les faire mourir avec lui.

### Crucifixion de Yéhoshoua<!--Mt. 27:33-43 ; Mc. 15:24-32 ; Jn. 19:17-37.-->

23:33	Et quand ils furent arrivés au lieu appelé le Crâne, ils le crucifièrent là ainsi que les malfaiteurs, l'un en effet à droite et l'autre à gauche.
23:34	Mais Yéhoshoua disait : Père, pardonne-leur, car ils ne savent pas ce qu'ils font. Ils se partagèrent ensuite ses vêtements, en tirant au sort.
23:35	Et le peuple se tenait là, et regardait. Et même les magistrats se moquaient de lui avec eux, en disant : Il a sauvé les autres, qu'il se sauve lui-même, s'il est le Mashiah, l'Élu d'Elohîm !
23:36	Mais les soldats aussi se jouaient de lui, s'approchant et lui présentant du vinaigre,
23:37	et disant : Si tu es le Roi des Juifs, sauve-toi toi-même !
23:38	Or il y avait aussi au-dessus de lui cette inscription en lettres grecques, et romaines, et hébraïques : CELUI-CI EST LE ROI DES JUIFS.

### Repentance du malfaiteur à la croix<!--Cp. Mt. 27:44 ; Mc. 15:32.-->

23:39	Et l'un des malfaiteurs qui étaient pendus blasphémait contre lui en disant : Si tu es le Mashiah, sauve-toi toi-même et sauve-nous !
23:40	Mais l'autre répondant, le réprimandait d'une manière tranchante en disant : Ne crains-tu pas Elohîm, toi, puisque tu es sous le même jugement ?
23:41	Et pour nous, en effet c'est juste, car nous recevons ce que méritent les choses que nous avons commises, mais celui-ci n'a rien fait de nuisible.
23:42	Et il disait à Yéhoshoua : Seigneur, souviens-toi de moi quand tu viendras dans ton royaume !
23:43	Et Yéhoshoua lui dit : Amen, je te le dis, aujourd'hui tu seras avec moi dans le paradis.

### Yéhoshoua remet son esprit à son Père<!--Mt. 27:45-56 ; Mc. 15:33-41 ; Jn. 19:30-37.-->

23:44	Or il était déjà environ la sixième heure, et la ténèbre<!--Le mot est au singulier.--> survint sur toute la Terre jusqu'à la neuvième heure.
23:45	Et le soleil s'obscurcit, et le voile du temple se déchira par le milieu.
23:46	Et Yéhoshoua cria d'une grande voix et dit : Père, je dépose mon esprit entre tes mains ! Et ayant dit ces choses, il expira<!--C'est la fin de la première alliance. Voir commentaire Jn. 19:30.-->.

### FIN DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE<!--Hé. 9:16-18.-->

23:47	Or l'officier de l'armée romaine, voyant ce qui était arrivé, glorifia Elohîm en disant : Cet homme était vraiment juste.
23:48	Et toutes les foules qui s'étaient assemblées pour ce spectacle, voyant ce qui était arrivé, s'en retournaient en se frappant la poitrine.
23:49	Et tous ceux qui l'avaient connu se tenaient loin, ainsi que les femmes qui l'avaient accompagné de la Galilée, regardant ces choses.

### Sépulture de Yéhoshoua<!--Mt. 27:57-61 ; Mc. 15:42-47 ; Jn. 19:38-42.-->

23:50	Et voici un homme du nom de Yossef, conseiller, homme bon et juste,
23:51	qui n'était pas d'accord avec leur conseil et leur mode d'action. Il était d'Arimathée, ville des Juifs, et il attendait lui aussi le Royaume d'Elohîm.
23:52	Celui-ci, étant venu à Pilate, lui demanda le corps de Yéhoshoua.
23:53	Et l'ayant descendu, il l'enveloppa d'un linceul et le mit dans un sépulcre taillé dans le roc, où personne n'avait jamais été mis.
23:54	Or c'était le jour de la préparation, et le shabbat allait commencer.
23:55	Mais les femmes qui étaient venues de Galilée avec lui, ayant suivi, regardèrent le sépulcre, et comment son corps y était déposé.
23:56	Et s'en étant retournées, elles préparèrent des aromates et des baumes. Et le jour du shabbat, elles se reposèrent en effet selon le commandement.

## Chapitre 24

### La résurrection de Yéhoshoua<!--Mt. 28:1-15 ; Mc. 16:1-11 ; Jn. 20:1-18.-->

24:1	Mais l'un des shabbats<!--Il est question du shabbat hebdomadaire. Voir commentaire en Mt. 28:1.-->, elles se rendirent au sépulcre à l’aube profonde, apportant les aromates qu'elles avaient préparés.
24:2	Et elles trouvèrent la pierre roulée à côté du sépulcre.
24:3	Et, étant entrées, elles ne trouvèrent pas le corps du Seigneur Yéhoshoua.
24:4	Et il arriva que comme elles étaient en perplexité à ce sujet, voici, deux hommes se présentèrent à elles, en vêtements resplendissants.
24:5	Mais comme elles étaient effrayées, et qu’elles baissaient le visage contre terre, ils leur dirent : Pourquoi cherchez-vous le Vivant<!--Ap. 1:18.--> parmi les morts ?
24:6	Il n'est pas ici, mais il est réveillé. Souvenez-vous comment il vous a parlé quand il était encore en Galilée,
24:7	et qu'il disait : Il faut que le Fils d'humain soit livré aux mains d'hommes pécheurs, et qu'il soit crucifié, et qu'il ressuscite le troisième jour.
24:8	Et elles se souvinrent de ses paroles.
24:9	Et s'en étant retournées du sépulcre, elles annoncèrent toutes ces choses aux onze disciples et à tous les autres.
24:10	Or c'étaient Myriam-Magdeleine, Yohana, Myriam, mère de Yaacov, et les autres qui étaient avec elles, qui dirent ces choses aux apôtres.
24:11	Mais leurs paroles leur semblèrent comme une absurdité et ils ne les crurent pas.
24:12	Mais Petros s'étant levé, courut au sépulcre. Et ayant regardé avec la tête penchée en avant, il ne voit que les petites étoffes de lin posées là, seules. Et il s'en alla chez lui, dans l'admiration de ce qui était arrivé.

### Yéhoshoua et les deux disciples sur le chemin d'Emmaüs<!--Mc. 16:12-13.-->

24:13	Or voici, deux d'entre eux étaient ce jour-là en chemin pour aller à un village nommé Emmaüs, éloigné de Yeroushalaim de soixante stades<!--Une douzaine de kilomètres.-->.
24:14	Et ils s'entretenaient ensemble de toutes ces choses qui étaient arrivées.
24:15	Et il arriva que, comme ils s'entretenaient et discutaient entre eux, Yéhoshoua lui-même s'étant approché, se mit à marcher avec eux.
24:16	Mais leurs yeux étaient retenus de sorte qu'ils ne le reconnaissaient pas.
24:17	Et il leur dit : Quels sont ces discours que vous tenez ensemble en marchant ? Et pourquoi êtes-vous tout tristes ?
24:18	Et l'un d'eux, du nom de Cléopas, répondant, lui dit : Es-tu le seul étranger dans Yeroushalaim qui ne sache pas les choses qui s'y sont passées ces jours-ci ?
24:19	Et il leur dit : Lesquelles ? Ils lui dirent : Celles concernant Yéhoshoua, le Nazaréen, qui était devenu un prophète puissant en œuvres et en paroles devant Elohîm et devant tout le peuple,
24:20	et comment les principaux prêtres et nos magistrats l'ont livré pour être condamné à mort, et l'ont crucifié.
24:21	Or nous, nous espérions que lui, il était sur le point de racheter Israël, mais avec tout cela, c'est aujourd'hui le troisième jour que ces choses sont arrivées.
24:22	Mais aussi quelques femmes d'entre nous nous ont beaucoup étonnés. Ayant été de grand matin au sépulcre,
24:23	et n'ayant pas trouvé son corps, elles sont venues en disant avoir vu même une apparition d'anges qui disent qu'il est vivant.
24:24	Et quelques-uns des nôtres sont allés au sépulcre, et ont trouvé les choses comme les femmes l'avaient dit, mais lui, ils ne l'ont pas vu.
24:25	Et il leur dit : Ô gens inintelligents<!--Ga. 3:1.-->, et dont le cœur est lent à croire tout ce que les prophètes ont annoncé !
24:26	Ne fallait-il pas que le Mashiah souffrît ces choses et qu'il entrât dans sa gloire ?
24:27	Et commençant par Moshé et par tous les prophètes, il leur interpréta dans toutes les Écritures ce qui le concernait.
24:28	Et comme ils furent près du village où ils allaient, il faisait comme s'il voulait aller plus loin.
24:29	Mais ils le contraignirent par d'instantes supplications<!--utiliser une force contraire à la nature et au droit, contraindre en utilisant la force. Voir Ac. 16:15.-->, en disant : Demeure avec nous, car le soir approche et le jour commence à baisser. Et il entra donc pour rester avec eux.
24:30	Et il arriva que comme il était à table avec eux, il prit le pain et prononça une bénédiction, et l'ayant rompu, il leur donna.
24:31	Et leurs yeux s'ouvrirent et ils le reconnurent, mais il leur devint invisible.
24:32	Et ils se dirent l'un à l'autre : Notre cœur ne brûlait-il pas au-dedans de nous lorsqu'il nous parlait en route, et qu'il nous ouvrait<!--Le verbe « ouvrir » vient du grec « dianoigo » qui signifie « ouvrir complètement ce qui a été fermé » ; « un enfant ouvrant la matrice, le premier-né » ; « ouvrir les yeux et les oreilles » ; « ouvrir l'esprit de quelqu'un, lui faire comprendre une chose » ou encore « donner la faculté de comprendre ou le désir d'apprendre ».--> les Écritures ?
24:33	Et ils se levèrent à ce moment même et retournèrent à Yeroushalaim, où ils trouvèrent les onze et les autres qui étaient rassemblés,
24:34	qui disaient : Le Seigneur est vraiment réveillé, et il est apparu à Shim’ôn.
24:35	Eux aussi racontèrent ce qui leur était arrivé en route, et comment ils l'avaient reconnu lorsqu'il avait rompu le pain.
24:36	Mais comme ils tenaient ces discours, Yéhoshoua se présenta lui-même au milieu d'eux, et leur dit : Shalôm à vous !
24:37	Mais eux, tout terrifiés et effrayés croyaient voir un esprit.
24:38	Et il leur dit : Pourquoi êtes-vous troublés, et en raison de quoi monte-t-il des pensées dans vos cœurs ?
24:39	Voyez mes mains et mes pieds, c'est bien moi. Touchez-moi et voyez car un esprit n'a ni chair ni os, comme vous voyez que j'ai.
24:40	Et en disant cela, il leur montra ses mains et ses pieds.
24:41	Mais comme dans leur joie ils ne croyaient pas encore et qu'ils s'étonnaient, il leur dit : Avez-vous ici quelque chose à manger ?
24:42	Et ils lui présentèrent un morceau de poisson rôti et un rayon de miel.
24:43	Et l'ayant pris, il mangea devant eux.

### La nouvelle mission des onze<!--Mt. 28:18-20 ; Mc. 16:15-18 ; Jn. 17:18, 20:21 ; Ac. 1:8.-->

24:44	Et il leur dit : Ce sont ici les paroles que je vous disais lorsque j'étais encore avec vous, qu'il fallait que fût accompli tout ce qui est écrit de moi dans la torah de Moshé, dans les prophètes<!--Les Nevi'im.-->, et dans les psaumes<!--Les Ketouvim.-->.
24:45	Alors il leur ouvrit la pensée<!--Pour comprendre les Écritures, nous avons besoin de l'aide de l'Esprit d'Elohîm. La vraie connaissance ne vient pas des humains, mais d'Elohîm (Da. 9:22).--> afin qu'ils comprennent les Écritures.
24:46	Et il leur dit : Il est ainsi écrit, et ainsi il fallait que le Mashiah souffre, et qu'il ressuscite des morts le troisième jour,
24:47	et que la repentance et le pardon des péchés seraient prêchés en son Nom à toutes les nations, à commencer par Yeroushalaim<!--Es. 53.-->.
24:48	Et vous êtes témoins de ces choses.
24:49	Et voici, j'envoie sur vous la promesse de mon Père. Mais vous, restez dans la ville de Yeroushalaim, jusqu'à ce que vous soyez revêtus de la puissance d'en haut.

### Yéhoshoua enlevé au ciel<!--Mc. 16:19-20 ; Ac. 1:9-11.-->

24:50	Et il les conduisit dehors jusque vers Béthanie, et levant ses mains en haut, il les bénit.
24:51	Et il arriva qu'en les bénissant, il se sépara d'eux et fut élevé dans le ciel.
24:52	Et pour eux, après l'avoir adoré, ils retournèrent à Yeroushalaim avec une grande joie.
24:53	Et ils étaient toujours dans le temple, louant et bénissant Elohîm. Amen !
