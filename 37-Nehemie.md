# Nehemyah (Néhémie) (Né.)

Signification : Yah a consolé

Auteur : Nehemyah (Néhémie)

Thème : Reconstruction des murailles de Yeroushalaim (Jérusalem)

Date de rédaction : 5ème siècle av. J.-C.

En apprenant l'état de ruine dans lequel se trouvait Yeroushalaim, Nehemyah, échanson du roi perse Artaxerxés Ier, fut profondément affecté. Après plusieurs jours dans la désolation et l'humiliation, le Seigneur toucha le cœur du roi qui lui donna l'autorisation et le matériel nécessaire pour rebâtir la muraille de Yeroushalaim. Malgré les nombreuses oppositions dont il fit l'objet au cours de son entreprise, Nehemyah acheva l'œuvre qui lui avait été confiée. Dans le même temps, il mit en place de profondes réformes dans le cadre du retour à la torah de YHWH.

Complément du livre d'Ezra (Esdras) avec lequel il ne formait initialement qu'un ouvrage, le livre de Nehemyah (Néhémie) présente un homme de prière, un serviteur œuvrant pour, avec, et au Nom de YHWH.

## Chapitre 1

### La détresse du peuple resté à Yeroushalaim est racontée à Nehemyah

1:1	Paroles de Nehemyah, fils de Chakalyah. Il arriva au mois de Kisleu, la vingtième année, comme j'étais à Suse, la capitale,
1:2	que Chananiy, l'un de mes frères, et quelques hommes arrivèrent de Yéhouda. Je les questionnai au sujet des Juifs rescapés qui étaient restés de la captivité et au sujet de Yeroushalaim.
1:3	Et ils me dirent : Les restants qui sont restés de la captivité, là-bas dans la province, sont dans une grande misère et dans l'insulte. Il y a des brèches dans la muraille de Yeroushalaim et ses portes sont brûlées par le feu.

### Nehemyah prie YHWH et implore sa grâce

1:4	Or il arriva que, dès que j'entendis ces paroles, je m'assis, je pleurai et je fus dans le deuil plusieurs jours. Je jeûnai et je priai devant l'Elohîm des cieux,
1:5	et je dis : Oh ! Je t'en prie, YHWH ! Elohîm des cieux, El grand et redoutable, qui garde l'alliance et la miséricorde de ceux qui t'aiment et qui observent tes commandements !
1:6	S'il te plaît, que ton oreille soit attentive et que tes yeux soient ouverts pour écouter la prière de ton serviteur. Je prie devant toi en ce temps-ci, jour et nuit, pour tes serviteurs les fils d'Israël, en confessant les péchés des fils d'Israël, que nous avons commis contre toi. Moi et la maison de mon père, nous avons péché.
1:7	Nous sommes coupables, nous sommes coupables devant toi, nous n'avons pas gardé les commandements, les lois et les ordonnances que tu prescrivis à Moshé, ton serviteur.
1:8	Souviens-toi, s’il te plaît, de la parole que tu as ordonnée à Moshé, ton serviteur, en disant : Lorsque vous commettrez des délits, je vous disperserai parmi les peuples<!--De. 28:63-67.-->.
1:9	Mais si, revenant à moi, vous gardez mes commandements et les observez, vos bannis seraient-ils à l’extrémité des cieux, de là je les rassemblerai et je les ferai venir au lieu que j'ai choisi pour y faire habiter mon nom<!--De. 30:1-10.-->.
1:10	Ils sont tes serviteurs et ton peuple, que tu as rachetés par ta grande puissance et par ta main forte.
1:11	Oh ! Adonaï, s'il te plaît, que ton oreille soit maintenant attentive à la prière de ton serviteur, et à la supplication de tes serviteurs qui prennent plaisir à craindre ton nom ! S'il te plaît, donne aujourd'hui du succès à ton serviteur, et donne-le aux matrices, face à cet homme ! J’étais échanson du roi.

## Chapitre 2

### YHWH exauce Nehemyah et lui donne la faveur du roi

2:1	Il arriva, au mois de Nisan, la vingtième année du roi Artaxerxès, comme le vin était devant lui, que je pris le vin et le donnai au roi. Or je n'avais jamais paru triste devant lui<!--Pr. 15:13.-->.
2:2	Le roi me dit : Pourquoi tes faces sont-elles tristes, puisque tu n'es pas malade ? Cela ne peut être qu'une tristesse de cœur. J'eus une très grande peur,
2:3	et je dis au roi : Que le roi vive éternellement ! Pourquoi mes faces ne seraient-elles pas tristes, quand la ville, la maison des sépulcres de mes pères est en désolation et que ses portes ont été dévorées par le feu ?
2:4	Le roi me dit : En cela, que demandes-tu ? Je priai l'Elohîm des cieux,
2:5	et je dis au roi : Si le roi le trouve bon, et si ton serviteur est agréable devant toi, envoie-moi en Yéhouda, vers la ville des sépulcres de mes pères, pour la rebâtir<!--La reconstruction de la ville de Yeroushalaim (Jérusalem) sous Nehemyah (Néhémie) date, selon certains, de l'an 445 av. J.-C., suite au décret d'Artaxerxès. Cette date marquerait le point de départ des 70 semaines d'années annoncées par Daniye'l (Da. 9:24-27).-->.
2:6	Le roi me dit, et sa femme aussi qui était assise auprès de lui : Combien de temps ton voyage durera-t-il, et quand seras-tu de retour ? Je lui fixai le temps, et le roi trouva bon de m'envoyer.
2:7	Je dis au roi : Si le roi le trouve bon, qu'on me donne des lettres pour les gouverneurs de l'autre côté du fleuve, afin qu'ils me laissent passer, jusqu'à ce que j'arrive en Yéhouda,
2:8	et des lettres pour Asaph, le garde de la forêt du roi, afin qu'il me donne du bois pour la charpente des portes de la forteresse près de la maison, pour les murailles de la ville, et pour la maison dans laquelle j'entrerai. Et le roi me l'accorda, car la main de mon Elohîm était bonne sur moi.

### Arrivée à Yeroushalaim (Jérusalem), constat des murailles en ruine

2:9	J'allai vers les gouverneurs qui sont de l'autre côté du fleuve et je leur donnai les lettres du roi. Le roi avait aussi envoyé avec moi des chefs de l'armée et des cavaliers.
2:10	Sanballat<!--Sin (elohîm lunaire) a donné la vie. Un Samaritain qui s'opposait à la reconstruction des murs de Yeroushalaim par Nehemyah.-->, le Horonite et Tobiyah, le serviteur Ammonite, l'ayant appris, prirent très mal le fait qu'un homme soit venu pour chercher le bien des fils d'Israël.
2:11	J'arrivai à Yeroushalaim et j’y fus trois jours.
2:12	Je me levai de nuit, moi et le peu d'hommes qui étaient avec moi, mais je ne déclarai à personne ce qu'Elohîm avait mis dans mon cœur de faire pour Yeroushalaim. Il n'y avait pas d'autre bête avec moi que la bête sur laquelle j'étais monté.
2:13	Je sortis de nuit par la porte de la vallée, en face de la source<!--Ou en face de l'œil du Dragon.--> du Dragon, vers la porte du fumier, en inspectant les murailles de Yeroushalaim où il y avait des brèches<!--Jé. 39:8.--> et ses portes dévorées par le feu.
2:14	Je passai près de la porte de la source et vers l'étang du roi, et il n’y avait pas de place par où pût passer la bête qui était sous moi.
2:15	Je montai de nuit par le torrent et j'inspectai la muraille. Puis en revenant, je rentrai par la porte de la vallée et ainsi je fus de retour.
2:16	Or les dirigeants ne savaient pas où j’étais allé et ce que je faisais. Jusque-là je n’avais rien déclaré aux Juifs : ni aux prêtres, ni aux nobles, ni aux dirigeants, ni aux autres qui faisaient l’œuvre.

### Mobilisation du peuple derrière Nehemyah

2:17	Je leur dis : Vous voyez la misère dans laquelle nous sommes ! Comment Yeroushalaim est en désolation et ses portes brûlées par le feu ! Venez et nous bâtirons les murailles de Yeroushalaim et nous ne serons plus dans l'insulte.
2:18	Je leur déclarai comment la main de mon Elohîm avait été bonne sur moi, et quelles paroles le roi m'avait dites. Ils dirent : Nous nous lèverons et nous bâtirons ! Ils fortifièrent leurs mains pour le bien.

### Montée des opposants à la vision d'Elohîm

2:19	Sanballat, le Horonite, Tobiyah, le serviteur Ammonite, et Guéshem, l'Arabe, l'ayant appris, se moquèrent de nous et nous méprisèrent. Ils dirent : Qu'est-ce que vous faites ? Ne vous rebellez-vous pas contre le roi ?
2:20	Je leur retournai la parole et leur dis : L'Elohîm des cieux lui-même nous fera réussir ! Nous, qui sommes ses serviteurs, nous nous lèverons et nous bâtirons. Mais vous, vous n'avez ni part, ni droit, ni souvenir, à Yeroushalaim.

## Chapitre 3

### Les participants à la reconstruction de la muraille

3:1	Élyashiyb, le grand-prêtre, se leva avec ses frères, les prêtres, et ils bâtirent la porte des brebis<!--La première porte reconstruite fut la porte des brebis. Cette porte est très proche du temple, c'est par elle que l'on faisait entrer les brebis destinées aux sacrifices dans la cour du temple. Cette porte est la préfiguration de Yéhoshoua ha Mashiah qui s'est lui-même présenté comme étant la « Porte des brebis » (Jn. 10:7).-->. Ils la consacrèrent et en posèrent les battants. Ils la consacrèrent depuis la tour de Méah jusqu'à la tour de Hananeel.
3:2	Les hommes de Yeriycho bâtirent à son côté, et Zakkour, fils d'Imri, bâtit à côté d'eux.
3:3	Les fils de Senaa bâtirent la porte des poissons. Ils en firent la charpente et y mirent ses portes, ses serrures et ses barres.
3:4	À côté d’eux fortifiait Merémoth, fils d'Ouriyah, fils d'Hakkots, et à côté d'eux fortifiait Meshoullam, fils de Berekyah, fils de Meshézabeel, et à côté d'eux fortifiait Tsadok, fils de Ba`ana.
3:5	À côté d’eux fortifiaient les Tekoïtes, mais les chefs d'entre eux n'introduisirent pas leur cou au service de leur Seigneur.
3:6	Et Yoyada, fils de Paséach, et Meshoullam, fils de Besodeyah, fortifièrent la vieille porte. Ils en firent la charpente, y mirent ses battants, ses serrures et ses barres.
3:7	À côté d’eux fortifiaient Melatyah, le Gabaonite, Yadon, le Méronothite, et les hommes de Gabaon et de Mitspah, vers le siège du gouverneur de ce côté du fleuve.
3:8	À côté d'eux fortifiait Ouzziel, fils de Charhayah, d'entre les orfèvres, et à côté de lui fortifiait Chananyah, fils d'un parfumeur. Et ainsi ils relevèrent Yeroushalaim jusqu'à la muraille large.
3:9	À côté d’eux fortifiait Rephayah, fils de Hour, chef d'un demi-quartier de Yeroushalaim.
3:10	À côté d’eux fortifiait Yedayah, fils de Haroumaph, en face de sa maison, et à côté de lui fortifiait Hattoush, fils de Chashabneyah.
3:11	Malkiyah, fils de Harim, et Hashoub, fils de Pachath-Moab, fortifiaient une seconde section et la tour des fours.
3:12	À côté d’eux fortifiait, avec ses filles, Shalloum, fils d'Hallochesh, chef de la moitié du quartier de Yeroushalaim.
3:13	Hanoun et les habitants de Zanoach fortifièrent la porte de la vallée. Ils la bâtirent et mirent ses battants, ses serrures, et ses barres : 1 000 coudées de muraille jusqu'à la porte du fumier.
3:14	Malkiyah, fils de Rékab, chef du quartier de Beth-Hakkérem, fortifia la porte du fumier. Il la bâtit et mit ses battants, ses serrures et ses barres.
3:15	Shalloun, fils de Kol-Hozeh, chef du quartier de Mitspah, fortifiait la porte de la source. Il la bâtit et la couvrit, et mit ses portes, ses serrures, ses barres, ainsi que la muraille de l'étang de Siloé, vers le jardin du roi, jusqu'aux marches qui descendent de la cité de David.
3:16	Après lui fortifiait Nehemyah, fils d'Azbouq, chef de la moitié du quartier de Beth-Tsour, jusqu'à l'endroit des sépulcres de David, et jusqu'à l'étang qui avait été refait, et jusqu'à la maison des hommes vaillants.
3:17	Après lui, fortifiaient les Lévites, Rehoum, fils de Bani, et à son côté, fortifiait Chashabyah, chef de la moitié du quartier de Qe'iylah, pour ceux de son quartier.
3:18	Après lui fortifiaient leurs frères, Bavvaï, fils de Hénadad, chef de la moitié du quartier de Qe'iylah.
3:19	À son côté, Ézer, fils de Yéshoua, chef de Mitspah, en fortifiait autant, à l'endroit où l'on monte à l'arsenal, à l'angle.
3:20	Après lui Baroukh, fils de Zabbaï, fortifiait avec ardeur une seconde section, depuis l'angle jusqu'à la porte de la maison d'Élyashiyb, le grand-prêtre.
3:21	Après lui Merémoth, fils d'Ouriyah, fils d'Hakkots, fortifiait une seconde section, depuis l'entrée de la maison d'Élyashiyb, jusqu'à l'extrémité de la maison d'Élyashiyb.
3:22	Après lui fortifiaient les prêtres, habitants des environs.
3:23	Après eux, Benyamin et Hashoub fortifiaient devant leur maison. Après eux, Azaryah, fils de Ma`aseyah, fils d'Ananyah, fortifiait auprès de sa maison.
3:24	Après lui, Binnouï, fils de Hénadad, fortifiait une seconde section, depuis la maison d'Azaryah jusqu'à l'angle et jusqu'au coin.
3:25	Palal, fils d'Ouzaï, fortifiait vis-à-vis de l'angle, et de la tour qui sort de la tour supérieure du roi, qui est auprès de la cour de la prison. Après lui fortifiait Pedayah, fils de Pareosh.
3:26	Les Néthiniens demeuraient sur la colline, vers l'est, jusqu'à l'endroit de la porte des eaux et vers la tour qui sort.
3:27	Après eux, les Tekoïtes fortifiaient une seconde section, depuis l'endroit de la grande tour qui sort en dehors, jusqu'à la muraille de la colline.
3:28	Au-dessus de la porte des chevaux, les prêtres fortifiaient, chacun devant de sa maison.
3:29	Après eux, Tsadok, fils d'Immer, restaurait devant sa maison. Après lui fortifiait Shema’yah, fils de Shekanyah, gardien de la porte de l'est.
3:30	Après lui, Chananyah, fils de Shelemyah et Hanoun le sixième fils de Tsalaph, en fortifiaient une seconde section. Après eux, Meshoullam, fils de Berekyah, restaurait vis-à-vis de sa chambre.
3:31	Après lui, Malkiyah, fils de l'orfèvre, restaurait jusqu'à la maison des Néthiniens et des marchands, vis-à-vis de la porte de Miphkad, et jusqu'à la chambre haute de l’Angle.
3:32	Entre la chambre haute de l’Angle et la porte des brebis, les orfèvres et les marchands la fortifièrent.

### La prière, solution pour faire face aux attaques et moqueries

3:33	Or il arriva lorsque Sanballat apprit que nous bâtissions la muraille, qu'il s'enflamma et fut très fâché. Il se moqua des Juifs.
3:34	Il parla en face de ses frères et de l’armée de Samarie en disant : Que font ces faibles Juifs ? Abandonneront-ils ? Sacrifieront-ils ? Finiront-ils en un jour ? Feront-ils vivre des pierres avec des monceaux de poussière ? Elles sont brûlées !
3:35	Et Tobiyah, l'Ammonite, qui était auprès de lui, dit : Même s’ils bâtissaient, un renard montera et fera une brèche dans leur muraille de pierres !
3:36	Entends, notre Elohîm ! Oui, nous sommes méprisés ! Retourne leurs insultes sur leur tête, et livre-les au pillage sur une terre de captivité.
3:37	Ne couvre pas leur iniquité, leurs péchés, ne les efface pas en face de toi, car ils ont vexé ceux qui bâtissent !
3:38	Nous avons bâti la muraille. Toute la muraille fut reliée jusqu'à sa moitié, et le peuple avait le cœur au travail.

## Chapitre 4

4:1	Il arriva que lorsque Sanballat, Tobiyah, les Arabes, les Ammonites et les Asdodiens apprirent que la restauration des murailles de Yeroushalaim progressait et que les brèches commençaient à se fermer, ils furent très fâchés.
4:2	Et ils se liguèrent<!--Lier, attacher, nouer, conspirer.--> tous ensemble pour venir faire la guerre contre Yeroushalaim et y mettre la confusion<!--Erreur (dans la morale et la religion), égarement, impiété, perversions, dérangement.-->.

### Persévérance du peuple prêt à se battre à tout moment

4:3	Nous avons prié notre Elohîm, et nous avons établi une garde contre eux, jour et nuit, en face d’eux.
4:4	Yéhouda disait : La force des ouvriers a trébuché, et il y a beaucoup de débris, en sorte que nous ne pourrons pas bâtir la muraille.
4:5	Nos ennemis disaient : Qu'ils n'en sachent rien et qu'ils ne voient rien, jusqu'à ce que nous entrions au milieu d'eux. Nous les tuerons et nous ferons ainsi cesser l'ouvrage.
4:6	Il arriva que les Juifs qui habitaient près d’eux vinrent et nous le dirent dix fois, de tous les lieux d’où ils revenaient vers nous.
4:7	Je plaçai le peuple dans les parties inférieures, derrière la muraille et dans des lieux découverts. Je le plaçai selon leurs familles, avec leurs épées, leurs lances et leurs arcs.
4:8	Je regardai, et je me levai en disant aux nobles, aux dirigeants et au reste du peuple : N'ayez pas peur devant eux ! Souvenez-vous d'Adonaï, qui est grand et redoutable, et combattez pour vos frères, pour vos fils et pour vos filles, pour vos femmes et pour vos maisons !
4:9	Il arriva que lorsque nos ennemis apprirent que cela nous était connu, Elohîm fit échouer leur projet et nous sommes tous retournés aux murailles, chaque homme à son ouvrage.
4:10	Depuis ce jour-là, il arriva que la moitié de mes serviteurs faisait l'ouvrage, et l'autre moitié avait des lances, des boucliers, des arcs et des cuirasses, avec les chefs derrière toute la maison de Yéhouda.
4:11	Ceux qui bâtissaient la muraille et ceux qui portaient la charge ou chargeaient, d’une main faisaient l'ouvrage et de l'autre tenaient une arme.
4:12	Et ceux qui bâtissaient avaient, chaque homme son épée ceinte sur les reins tout en bâtissant. Et celui qui sonnait du shofar se tenait près de moi.
4:13	Je dis aux nobles, aux dirigeants et au reste du peuple : L'ouvrage est grand et étendu, et nous sommes séparés sur la muraille, éloignés chaque homme de son frère.
4:14	À l'endroit où vous entendrez le son du shofar, c'est là que vous vous rassemblerez vers nous. Notre Elohîm combattra pour nous<!--Ex. 14:14 ; De. 1:30 ; 2 Ch. 20:29.-->.
4:15	Nous faisions l’ouvrage - la moitié tenant des lances - depuis le lever de l’aurore jusqu’à l’apparition des étoiles.
4:16	En ce temps-là aussi, je dis au peuple : Que chaque homme passe la nuit dans Yeroushalaim avec son serviteur, afin de faire la garde la nuit et de travailler le jour.
4:17	Nous ne quittions pas nos vêtements, ni moi, ni mes frères, ni mes serviteurs, ni les hommes de garde qui me suivaient. Chaque homme n'avait que ses armes et de l'eau.

## Chapitre 5

### Cupidité des chefs dévoilée ; rétablissement de la justice

5:1	Il y eut un grand cri de détresse du peuple et de leurs femmes contre leurs frères les Juifs.
5:2	Il y en avait qui disaient : Nous, nos fils et nos filles, nous sommes nombreux, nous prendrons du blé, nous mangerons et nous vivrons.
5:3	Et il y en avait qui disaient : Nous engageons nos champs, nos vignes et nos maisons, pour avoir du blé pendant la famine.
5:4	Il y en avait qui disaient : Nous avons emprunté de l'argent sur nos champs et sur nos vignes pour le tribut du roi.
5:5	Maintenant, notre chair est comme la chair de nos frères, nos fils comme leurs fils, et voici, nous forçons nos fils et nos filles à être esclaves : certaines de nos filles y sont forcées. Nos mains sont impuissantes, nos champs et nos vignes appartiennent à d'autres.
5:6	Je fus très fâché quand j'entendis leur cri et ces paroles-là.
5:7	Je résolus dans mon cœur contester contre les nobles et les dirigeants, et je leur dis : Vous prêtez en exigeant un intérêt chaque homme de son frère ! Je convoquai une grande assemblée contre eux.
5:8	Je leur dis : Nous avons racheté selon notre pouvoir nos frères juifs, qui avaient été vendus aux nations, et vous vendriez vous-mêmes vos frères, ou nous seraient-ils vendus ? Ils gardèrent le silence et ne trouvèrent pas de parole.
5:9	Je dis : Cette manière d'agir n'est pas bonne. Ne voulez-vous pas marcher dans la crainte de notre Elohîm, à cause des insultes des nations qui sont nos ennemies ?
5:10	Moi aussi, mes frères et mes serviteurs, nous leur avons prêté de l'argent et du blé. Abandonnons, s'il vous plaît, cette dette !
5:11	S’il vous plaît, rendez-leur en ce jour leurs champs, leurs vignes, leurs oliviers et leurs maisons, et le centième de l'argent, du blé, du vin nouveau et de l'huile que vous leur avez prêtés !
5:12	Ils dirent : Nous les rendrons et nous ne leur demanderons rien. Nous ferons ce que tu dis. J'appelai les prêtres et je les fis jurer d'agir selon cette parole.
5:13	Je secouai aussi mon bras et je dis : Ainsi Elohîm secouera tout homme qui ne confirmera pas cette parole, hors de sa maison et de son travail, ainsi il sera secoué et vidé ! Toute l'assemblée dit : Amen ! Ils louèrent YHWH. Et le peuple agit selon cette parole.

### Nehemyah, modèle de dévouement

5:14	Aussi, depuis le jour où l'on m'a donné l'ordre de devenir leur gouverneur en terre de Yéhouda, depuis la vingtième année jusqu'à la trente-deuxième année du roi Artaxerxès, pendant 12 ans, ni moi, ni mes frères, n'avons mangé le pain du gouverneur.
5:15	Les premiers gouverneurs qui étaient avant moi pesaient sur le peuple et prenaient d'eux du pain et du vin, ainsi que 40 sicles d'argent ; leurs serviteurs aussi dominaient sur le peuple. Mais moi, je n'ai pas agi de la sorte face à la crainte d'Elohîm.
5:16	J'ai même renforcé l’ouvrage de cette muraille, et nous n'avons pas acheté de champ, et tous mes serviteurs rassemblés étaient à l'ouvrage.
5:17	Les Juifs et les dirigeants, 150 hommes, et ceux qui venaient à nous des nations autour de nous étaient à ma table.
5:18	Ce qui était préparé pour un jour - un bœuf, six moutons choisis et des volailles - était préparé pour moi et, tous les dix jours, toutes sortes de vins en abondance. Et avec cela, je n’ai pas exigé le pain du gouverneur, parce que le service pesait sur ce peuple.
5:19	Mon Elohîm, souviens-toi pour mon bonheur de tout ce que j'ai fait pour ce peuple !

## Chapitre 6

### Complot et mensonge contre Nehemyah ; fermeté et confiance en Elohîm

6:1	Or il arriva que Sanballat, Tobiyah, et Guéshem, l'Arabe, et le reste de nos ennemis apprirent que j'avais bâti la muraille, et qu'il n'y restait aucune brèche, bien que jusqu'à ce temps-là, je n'avais pas encore fixé les battants aux portes.
6:2	Sanballat et Guéshem envoyèrent vers moi, en disant: Viens ! Nous nous rencontrerons ensemble dans les villages de la vallée d'Ono. Ils projetaient de me faire du mal.
6:3	J’envoyai vers eux des messagers pour leur dire : Je fais un grand ouvrage, et je ne peux pas descendre. Pourquoi l’ouvrage cesserait-il quand je le relâcherai pour descendre vers vous ?
6:4	Ils m'envoyèrent quatre fois la même parole et je leur retournai la même parole.
6:5	Sanballat m'envoya cette même parole une cinquième fois par son jeune serviteur, qui tenait à la main une lettre ouverte.
6:6	Il y était écrit : On entend dire parmi les nations, et Guéshem le dit, que vous pensez, toi et les Juifs, à vous révolter, et que c'est pour cela que tu rebâtis la muraille et, selon ces paroles, c’est toi qui deviendrais leur roi.
6:7	Tu aurais même établi des prophètes pour te proclamer à Yeroushalaim et pour dire : Il est roi de Yéhouda. Maintenant, le roi entendra ces paroles. Maintenant viens ! Nous nous consulterons ensemble.
6:8	Je lui envoyai dire : Il n'existe aucune des choses dont tu parles, mais c'est toi qui l'inventes dans ton propre cœur !
6:9	Car tous voulaient nous faire peur, se disant : Leurs mains se relâcheront à l’ouvrage et il ne se fera jamais. Maintenant, fortifie mes mains !
6:10	Et moi, j’entrai dans la maison de Shema’yah, fils de Delayah, fils de Mehétabeel. Il s'était enfermé et il me dit : Nous nous rencontrerons dans la maison d'Elohîm, au milieu du temple et nous fermerons les portes du temple, car ils viennent pour te tuer dans la nuit, ils viennent pour te tuer.
6:11	Je dis : Un homme tel que moi fuirait-il ? Et quel homme tel que moi entrerait dans le temple et vivrait ? Je n'y entrerai pas.
6:12	Je reconnus que, voici, ce n’était pas Elohîm qui l’avait envoyé, mais il avait prononcé cette prophétie contre moi parce que Sanballat et Tobiyah l'avaient engagé.
6:13	Ils l'avaient engagé pour que j'aie peur, que j'agisse ainsi, pour que je pèche. Cela deviendrait pour eux une mauvaise réputation afin de m'insulter.
6:14	Souviens-toi, Elohîm, de Tobiyah, de Sanballat, de leurs actions et aussi de No`adyah, la prophétesse et du reste des prophètes qui ont cherché à me faire peur !

### Achèvement de la muraille

6:15	La muraille fut achevée le vingt-cinquième jour du mois d'Éloul, en 52 jours.
6:16	Il arriva que quand tous nos ennemis l’apprirent et que toutes les nations autour de nous l’eurent vu, ils tombèrent de haut à leurs propres yeux et surent que cet ouvrage s’était fait avec notre Elohîm.
6:17	En ces jours-là aussi les nobles de Yéhouda envoyaient beaucoup de lettres à Tobiyah et celles de Tobiyah venaient à eux.
6:18	Car par serment, il était le possesseur<!--Vient de l'hébreu « ba`al » qui signifie « mari », « seigneur », « maîtres ».--> de beaucoup en Yéhouda, parce qu'il était le gendre de Shekanyah, fils d'Arach, et que son fils Yehohanan avait pris la fille de Meshoullam, fils de Berekyah.
6:19	Il arriva qu'ils disaient même du bien de lui en face de moi et lui rapportaient mes paroles. Il arriva que Tobiyah envoyait des lettres pour me faire peur.

## Chapitre 7

### Instructions spécifiques à Chananiy et Chananyah

7:1	Il arriva que lorsque la muraille fut bâtie et que j’eus fixé les portes, les portiers, les chanteurs et les Lévites furent chargés de la surveillance.
7:2	Je donnai cet ordre à Chananiy, mon frère, et à Chananyah, chef de la forteresse de Yeroushalaim. C'était en effet un homme fidèle et craignant Elohîm plus que beaucoup d'autres.
7:3	Je leur dis : Les portes de Yeroushalaim ne seront pas ouvertes jusqu’à la chaleur du soleil et, quand ceux qui se tiendront debout auront fermé les portes, verrouillez-les ! On fera tenir debout les gardes parmi les habitants de Yeroushalaim, chaque homme à son poste, chaque homme devant sa maison.
7:4	C'était une ville aux mains larges et grande, mais le peuple peu nombreux au milieu d'elle, et ses maisons n'étaient pas bâties<!--De. 4:27.-->.

### Liste des familles revenues de captivité avec Zerubbabel (Zorobabel)

7:5	Et mon Elohîm me mit à cœur de rassembler les nobles, les dirigeants et le peuple, pour en faire le dénombrement selon leurs généalogies. Je trouvai le registre du dénombrement selon les généalogies de ceux qui étaient montés la première fois, et j’y trouvai écrit :
7:6	Ceux-ci sont les fils de la province qui montèrent de la captivité, que Neboukadnetsar, roi de Babel<!--Babylone.-->, avait emmenés en exil et qui retournèrent à Yeroushalaim et en Yéhouda, chaque homme dans sa ville.
7:7	Ils vinrent avec Zerubbabel<!--Esd. 5:2.-->, Yéshoua, Nehemyah, Azaryah, Ra`amyah, Nachamani, Mordekay<!--Mardochée.-->, Bilshan, Mispéreth, Bigvaï, Nehoum, et Ba`anah. Nombre des hommes du peuple d'Israël :
7:8	Les fils de Pareosh, 2 172.
7:9	Les fils de Shephatyah, 372.
7:10	Les fils d'Arach, 652.
7:11	Les fils de Pachath-Moab, des fils de Yéshoua et de Yoab, 2 818.
7:12	Les fils d'Éylam, 1 254.
7:13	Les fils de Zatthou, 845.
7:14	Les fils de Zakkay, 760.
7:15	Les fils de Binnouï, 648
7:16	Les fils de Bébaï, 628.
7:17	Les fils d'Azgad, 2 322.
7:18	Les fils d'Adonikam, 667.
7:19	Les fils de Bigvaï, 2 067.
7:20	Les fils d'Adin, 655.
7:21	Les fils d'Ather, d'Hizqiyah, 98.
7:22	Les fils de Hashoum, 328.
7:23	Les fils de Betsaï, 324.
7:24	Les fils de Hariph, 112.
7:25	Les fils de Gabaon, 95.
7:26	Les hommes de Bethléhem et de Netophah, 188.
7:27	Les hommes d'Anathoth, 128.
7:28	Les hommes de Beth-Azmaveth, 42.
7:29	Les hommes de Qiryath-Yéarim, de Kephiyrah et de Beéroth, 743.
7:30	Les hommes de Ramah et de Guéba, 621.
7:31	Les hommes de Micmas, 122.
7:32	Les hommes de Béth-El et de Aï, 123.
7:33	Les hommes de l'autre Nebo, 52.
7:34	Les fils d'un autre Éylam, 1 254.
7:35	Les fils de Harim, 320.
7:36	Les fils de Yeriycho, 345.
7:37	Les fils de Lod, de Hadid et d'Ono, 721.
7:38	Les fils de Senaa, 3 930.

### Liste des prêtres revenus de captivité

7:39	Prêtres : Les fils de Yekda`yah, de la maison de Yéshoua, 973.
7:40	Les fils d'Immer, 1 052.
7:41	Les fils de Pashhour, 1 247.
7:42	Les fils de Harim, 1 017.

### Liste des Lévites revenus de captivité

7:43	Lévites : Les fils de Yéshoua et de Qadmiy'el, d'entre les fils de Howdevah, 74.
7:44	Chanteurs : Les fils d'Asaph, 148.
7:45	Portiers : Les fils de Shalloum, les fils d'Ather, les fils de Thalmon, les fils d'Aqqoub, les fils de Hathitha, les fils de Shobaï, 138.

### Liste des Néthiniens revenus de captivité

7:46	Néthiniens : Les fils de Tsicha, les fils de Hasoupha, les fils de Thabbaoth,
7:47	les fils de Kéros, les fils de Sia, les fils de Padon,
7:48	les fils de Lebana, les fils de Hagaba, les fils de Salmaï,
7:49	les fils de Hanan, les fils de Guiddel, les fils de Gachar,
7:50	les fils de Reayah, les fils de Retsin, les fils de Nekoda,
7:51	les fils de Gazzam, les fils d'Ouzza, les fils de Paséach,
7:52	les fils de Bésaï, les fils de Mehounim, les fils de Nephishsim,
7:53	les fils de Baqbouq, les fils de Haqoupha, les fils de Har-hour,
7:54	les fils de Batslith, les fils de Mehida, les fils de Harsha,
7:55	les fils de Barkos, les fils de Sisera, les fils de Thamach,
7:56	les fils de Netsiach, les fils de Hathipha.

### Liste des fils des serviteurs de Shelomoh revenus de captivité

7:57	Fils des serviteurs de Shelomoh : Les fils de Sothaï, les fils de Sophéreth, les fils de Perida,
7:58	les fils de Ya`ala, les fils de Darkon, les fils de Guiddel,
7:59	les fils de Shephatyah, les fils de Hatthil, les fils de Pokéreth-Hatsebaïm, les fils d'Amon.
7:60	Tous les Néthiniens, et les fils des serviteurs de Shelomoh, étaient 392.
7:61	Voici ceux qui montèrent de Thel-Mélach, de Thel-Harsha, de Keroub-Addôn et d'Immer, lesquels ne purent montrer la maison de leurs pères, ni leur postérité, pour prouver qu'ils étaient d'Israël.
7:62	Les fils de Delayah, les fils de Tobiyah, les fils de Nekoda, 642.

### Liste des prêtres exclus de la prêtrise

7:63	Et d’entre les prêtres : les fils de Chabayah, les fils d'Hakkots, les fils de Barzillaï, qui prit pour femme une des filles de Barzillaï<!--2 S. 17:27 ; Esd. 2:61.-->, le Galaadite, et qui fut appelé de leur nom.
7:64	Ils cherchèrent leur registre généalogique, mais ils n'y furent pas trouvés, et on les déclara souillés et hors de la prêtrise.
7:65	Le gouverneur leur dit de ne pas manger du saint des saints, jusqu'à ce que le prêtre eût consulté l'ourim et le thoummim<!--Ex. 28:30 ; Esd. 2:63.-->.

### Somme des Israélites revenus de captivité

7:66	Toute l'assemblée réunie était de 42 360,
7:67	outre leurs serviteurs et leurs servantes, ceux-là : 7 337 ; avec des chanteurs et des chanteuses : 245.
7:68	Leurs chevaux : 736 ; leurs mulets : 245 ; 435 chameaux et 6 720 ânes.
7:69	Une partie des têtes des pères donnèrent pour l'ouvrage. Le gouverneur donna au trésor 1 000 drachmes d'or, 50 cuvettes, 530 tuniques de prêtres.
7:70	Certains des têtes de pères donnèrent pour le trésor de l'ouvrage 20 000 drachmes d'or et 2 200 mines d'argent.
7:71	Le reste du peuple donna 20 000 drachmes d'or, 2 000 mines d'argent et 67 tuniques de prêtres.
7:72	Les prêtres, les Lévites, les portiers, les chanteurs, quelques-uns du peuple, les Néthiniens et tout Israël, habitèrent dans leurs villes. Le septième mois arriva, et les fils d'Israël étaient dans leurs villes.

## Chapitre 8

### Lecture du livre de la torah, conviction de péché du peuple

8:1	Or tout le peuple se rassembla comme un seul homme sur la place qui est face à la porte des eaux. Et ils dirent à Ezra, le scribe, d'apporter le livre de la torah de Moshé, que YHWH avait ordonné à Israël.
8:2	Le premier jour du septième mois, Ezra, le prêtre, apporta la torah face à l'assemblée, aux hommes, jusqu'aux femmes et à tous ceux qui avaient du discernement pour entendre.
8:3	Il la lit, face à la place qui est face à la porte des eaux, dès la lumière jusqu’au milieu du jour, en présence des hommes, des femmes et de ceux qui avaient du discernement. Les oreilles de tout le peuple étaient attentives au livre de la torah.
8:4	Ezra, le scribe, se tenait debout sur une estrade de bois qu'on avait faite pour le discours. Mattithyah, Shéma, Anayah, Ouriyah, Chilqiyah et Ma`aseyah, se tenaient debout à côté de lui, à sa droite et, à sa gauche, Pedayah, Miyshael, Malkiyah, Hashoum, Hashbaddanah, Zekaryah et Meshoullam.
8:5	Ezra ouvrit le livre aux yeux de tout le peuple, car il était au-dessus de tout le peuple, et lorsqu'il l'eut ouvert, tout le peuple se tint debout.
8:6	Ezra bénit YHWH, le grand Elohîm, et tout le peuple répondit en élevant leurs mains : Amen ! Amen ! Et ils s'inclinèrent et se prosternèrent devant YHWH, le visage contre terre.
8:7	Yéshoua, Bani, Sherebyah, Yamin, Aqqoub, Shabbethaï, Hodiyah, Ma`aseyah, Qeliyta, Azaryah, Yozabad, Hanan, Pelayah et les Lévites, faisaient discerner la torah au peuple, et le peuple se tenait à sa place.
8:8	Ils lisaient distinctement dans le livre de la torah d'Elohîm, ils en donnaient le sens et faisaient discerner la lecture.
8:9	Nehemyah, le gouverneur, Ezra, le prêtre-scribe et les Lévites qui faisaient discerner le peuple, dirent à tout le peuple : Ce jour est sacré pour YHWH notre Elohîm. Ne soyez pas dans les lamentations et ne pleurez pas ! Car tout le peuple pleurait en entendant les paroles de la torah.
8:10	Il leur dit : Allez, mangez des viandes grasses, buvez du vin doux et envoyez-en des portions à ceux qui n'ont rien de prêt, car ce jour est sacré pour notre Seigneur. Ne soyez pas tristes, car la joie de YHWH est votre force !
8:11	Les Lévites calmaient tout le peuple, en disant : Taisez-vous car ce jour est sacré, et ne vous affligez pas.
8:12	Tout le peuple s'en alla pour manger et boire, pour envoyer des portions, et pour faire une grande réjouissance, parce qu'ils avaient discerné les paroles qu'on leur avait fait connaître.

### Célébration de la fête des cabanes (ou des tabernacles)

8:13	Le second jour, les têtes des pères de tout le peuple, les prêtres et les Lévites, se rassemblèrent auprès d'Ezra, le scribe, pour sagement comprendre les paroles de la torah.
8:14	Et ils trouvèrent écrit dans la torah que YHWH avait ordonnée par la main de Moshé, que les fils d'Israël devaient habiter sous des cabanes<!--Voir les sept fêtes de YHWH en Lé. 23.--> pendant la fête au septième mois,
8:15	et qu’ils devaient faire entendre et faire passer une voix dans toutes leurs villes et à Yeroushalaim, en disant : Sortez dans la montagne, et apportez des feuilles d'oliviers, des feuilles de l’arbre à huile, des feuilles de myrte, des feuilles de palmier et des feuilles d'arbres touffus, afin de faire des cabanes, selon ce qui est écrit.
8:16	Le peuple sortit et rapporta de quoi se faire des cabanes, chaque homme sur son toit, dans les cours de leurs maisons, et dans les parvis de la maison d'Elohîm, sur la place de la porte des eaux, et sur la place de la porte d'Éphraïm.
8:17	Toute l'assemblée de ceux qui étaient revenus de la captivité fit des cabanes, et ils habitèrent sous ces cabanes. Or les fils d'Israël n'en avaient pas fait de telles depuis les jours de Yéshoua<!--Forme contractée de Yéhoshoua (Josué).-->, fils de Noun, jusqu'à ce jour. Et il y eut une très grande joie.
8:18	On lut dans le livre de la torah d'Elohîm jour après jour, depuis le premier jour jusqu'au dernier jour. Ils firent la fête pendant 7 jours, et il y eut une assemblée solennelle au huitième jour, selon l’ordonnance.

## Chapitre 9

### Confession, jeûne et prière du peuple

9:1	Le vingt-quatrième jour du même mois, les fils d'Israël se rassemblèrent pour un jeûne, couverts de sacs et de terre.
9:2	Et la postérité d'Israël se sépara de tous les fils d'étrangers et ils se présentèrent pour confesser leurs péchés et les iniquités de leurs pères.
9:3	Ils se levèrent à leur place, et on lut dans le livre de la torah de YHWH, leur Elohîm, pendant un quart de la journée. Et pendant un autre quart, ils confessèrent et se prosternèrent devant YHWH, leur Elohîm.

### Prière des Lévites, alliance avec YHWH

9:4	Yéshoua, Bani, Qadmiy'el, Shebanyah, Bounni, Sherebyah, Bani et Kenani se levèrent sur la tribune des Lévites, et crièrent à grande voix à YHWH, leur Elohîm.
9:5	Et les Lévites, Yéshoua, Qadmiy'el, Bani, Chashabneyah, Sherebyah, Hodiyah, Shebanyah et Pethachyah, dirent : Levez-vous, bénissez YHWH votre Elohîm, d'éternité en éternité ! Que l'on bénisse ton Nom glorieux, qui est au-dessus de toute bénédiction et de toute louange !
9:6	Toi, lui, YHWH, toi seul, toi, tu as fait les cieux, les cieux des cieux et toute leur armée, la Terre et tout ce qui y est, les mers et toutes les choses qui y vivent. Tu donnes la vie à toutes ces choses et l'armée des cieux se prosterne devant toi.
9:7	Toi, lui, YHWH, l'Elohîm, qui as choisi Abram, tu l’as fait sortir d'Our en Chaldée, tu lui as donné le nom d'Abraham<!--Ge. 11:31, 17:5.-->.
9:8	Tu as trouvé son cœur fidèle devant toi et tu as conclu l'alliance avec lui pour donner la terre des Kena'ânéens, des Héthiens, des Amoréens, des Phéréziens, des Yebousiens et des Guirgasiens, pour la donner à sa postérité. Et tu as accompli ce que tu as promis parce que tu es juste.
9:9	Tu as vu l'affliction de nos pères en Égypte et tu as entendu leurs cris près de la Mer Rouge<!--Ex. 2:23-25.-->.
9:10	Tu as donné des signes et des miracles contre pharaon et contre tous ses serviteurs, et contre tout le peuple de sa terre, parce que tu savais avec quelle arrogance ils les avaient maltraités et tu t’es fait un nom comme en ce jour.
9:11	Tu as fendu la mer devant eux, ils sont passés à sec au milieu de la mer mais tu as jeté dans les profondeurs ceux qui les poursuivaient, comme une pierre dans les eaux violentes.
9:12	Tu les as conduits le jour par la colonne de nuée, et la nuit par la colonne de feu qui était leur lumière dans le chemin où ils marchaient<!--Ex. 13:21.-->.
9:13	Tu es descendu sur la montagne de Sinaï, tu leur as parlé du haut des cieux et leur as donné des ordonnances justes et la torah de vérité, des statuts et des commandements bons.
9:14	Tu leur as fait connaître ton saint shabbat<!--Ge. 2:1-3 ; Ex. 20:8-11.--> et tu leur as donné les commandements, les statuts, et la torah par la main de Moshé, ton serviteur.
9:15	Tu leur as donné aussi, du haut des cieux, du pain quand ils avaient faim, et tu as fait sortir de l'eau du rocher quand ils avaient soif<!--Ex. 16:13-36 ; No. 20:8 ; 1 Co. 10:4.-->. Et tu leur as dit d'entrer et de prendre possession de la terre que tu avais juré, à main levée, de leur donner.
9:16	Mais eux, nos pères, se sont montrés arrogants et ont raidi leur cou. Ils n'ont pas écouté tes commandements.
9:17	Ils ont refusé d'écouter et ils ne se sont pas souvenus des merveilles que tu avais faites en leur faveur. Mais ils ont raidi leur cou et, dans leur rébellion, ils se sont donnés une tête pour retourner à leur esclavage. Mais toi, tu es l'Éloah qui pardonne, miséricordieux, compatissant, lent à la colère des narines et grand en bonté, et tu ne les as pas abandonnés.
9:18	Même quand ils se sont fait un veau en métal fondu, en disant : Voici ton Elohîm qui t'a fait sortir hors d'Égypte, et qu'ils ont commis contre toi de grands blasphèmes<!--Ex. 32:1-14.--> ;
9:19	et toi, dans ton immense miséricorde, tu ne les as pas abandonnés dans le désert. Et la colonne de nuée ne s'est pas écartée d'eux pendant le jour pour les conduire sur ce chemin, ni la colonne de feu pendant la nuit qui était la lumière du chemin sur lequel ils marchaient.
9:20	Tu leur as donné ton bon Esprit pour les rendre sages. Tu n'as pas refusé ta manne à leur bouche et tu leur as donné de l'eau quand ils avaient soif.
9:21	Pendant 40 ans, tu les as nourris dans le désert, et ils n’ont manqué de rien. Leurs vêtements ne se sont pas usés et leurs pieds n'ont pas enflé.
9:22	Tu leur as donné des royaumes et des peuples et tu les leur as répartis comme territoires frontaliers, et ils ont pris possession de la terre de Sihon, c'est la terre du roi de Hesbon, et de la terre de Og, roi de Bashân.
9:23	Et tu as multiplié leurs fils comme les étoiles des cieux et tu les as fait entrer sur la terre dont tu avais dit à leurs pères qu'ils y entreraient pour en prendre possession.
9:24	Leurs fils sont entrés sur la terre et en ont pris possession. Tu as humilié devant eux les habitants de la terre, les Kena'ânéens, tu les as livrés entre leurs mains avec leurs rois et les peuples de la terre, pour qu'ils les traitent selon leur volonté.
9:25	Ils ont pris des villes fortifiées et un sol fertile. Ils ont pris possession de maisons remplies de toutes sortes de biens, de puits qu'on avait creusés, de vignes, d'oliviers et d'arbres fruitiers en abondance. Ils ont mangé, ils se sont rassasiés, ils se sont engraissés et ils ont vécu dans les délices, par ta grande bonté.
9:26	Ils furent désobéissants et se révoltèrent contre toi. Ils ont rejeté ta torah derrière leur dos, ils ont tué tes prophètes, qui les avertissaient pour qu'ils reviennent à toi, et ils ont commis contre toi de grands blasphèmes.
9:27	Tu les as livrés entre les mains de leurs ennemis, qui les ont opprimés. Mais au temps de leur détresse, ils ont crié vers toi, et toi, tu les as entendus des cieux, et selon tes grandes matrices, tu leur as donné des sauveurs qui les ont sauvés de la main de leurs ennemis.
9:28	Mais dès qu'ils ont eu du repos, ils ont recommencé à faire le mal devant toi, et tu les as abandonnés entre les mains de leurs ennemis, qui dominèrent sur eux. Mais, de nouveau, ils ont crié vers toi, et toi, tu les as entendus des cieux, et, dans tes grandes matrices, tu les as délivrés plusieurs fois et en divers temps.
9:29	Tu as témoigné contre eux pour les faire retourner vers ta torah, mais ils se sont montrés arrogants et n'ont pas écouté tes commandements. Ils ont péché contre tes ordonnances qui font vivre l'être humain qui les observe. Ils ont rendu rebelle leur épaule et ont raidi leur cou, ils n'ont pas écouté.
9:30	Tu les as tirés pendant de nombreuses années et tu les as avertis par ton Esprit, par la main de tes prophètes, mais ils n'ont pas prêté l'oreille. C'est pourquoi tu les as livrés entre les mains des peuples des terres.
9:31	Dans tes grandes matrices, tu ne les as pas livrés à la destruction et tu ne les as pas abandonnés, car tu es le El compatissant et miséricordieux.
9:32	Et maintenant, notre Elohîm, le El Gadowl<!--Le El Grand.-->, le Gibbor et le redoutable, qui garde ton alliance et la miséricorde, qu'elle ne soit pas peu de chose devant toi toute cette détresse qui nous a atteints, nous, nos rois, nos chefs, nos prêtres, nos prophètes, nos pères et tout ton peuple, depuis le temps des rois d'Assyrie jusqu'à aujourd'hui.
9:33	Tu as été juste dans toutes les choses qui nous sont arrivées, car tu as agi avec fidélité, mais nous, nous avons agi méchamment.
9:34	Nos rois, nos chefs, nos prêtres et nos pères n'ont pas pratiqué ta torah et n'ont pas été attentifs à tes commandements ni à tes témoignages par lesquels tu les as avertis.
9:35	Ils ne t'ont pas servi durant leur règne, au milieu des grands biens que tu leur accordais, sur la terre vaste et riche que tu avais mise devant eux, et ils ne se sont pas détournés de leurs mauvaises œuvres.
9:36	Voici, nous sommes aujourd'hui esclaves sur la terre que tu as donnée à nos pères pour en manger le fruit et les biens. Voici, nous y sommes esclaves !
9:37	Elle rapporte ses produits en abondance pour les rois que tu as établis sur nous à cause de nos péchés, et qui dominent sur nos corps et sur nos bêtes, à leur volonté, de sorte que nous sommes dans une grande angoisse !

## Chapitre 10

10:1	À cause de tout cela, nous avons conclu une ferme alliance que nous avons mise par écrit. Nos chefs, nos Lévites et nos prêtres y ont apposé leur sceau.

### Liste des contractants et termes de l'alliance

10:2	Voici ceux qui apposèrent leur sceau : Nehemyah, le gouverneur, fils de Chakalyah, et Tsidqiyah<!--Sédécias.-->.
10:3	Serayah, Azaryah, Yirmeyah,
10:4	Pashhour, Amaryah, Malkiyah,
10:5	Hattoush, Shebanyah, Mallouk,
10:6	Harim, Merémoth, Obadyah,
10:7	Daniye'l, Guinnethon, Baroukh,
10:8	Meshoullam, Abiyah, Miyamin,
10:9	Ma`azyah, Bilgaï et Shema’yah. Ce sont les prêtres.
10:10	Des Lévites : Yéshoua, fils d'Azanyah, Binnouï d'entre les fils de Hénadad, et Qadmiy'el.
10:11	Et leurs frères : Shebanyah, Hodiyah, Qeliyta, Pelayah, Hanan,
10:12	Miyka<!--Michée.-->, Rehob, Chashabyah.
10:13	Zakkour, Sherebyah, Shebanyah,
10:14	Hodiyah, Bani et Beninou.
10:15	Des têtes du peuple : Pareosh, Pachath-Moab, Éylam, Zatthou, Bani,
10:16	Bounni, Azgad, Bébaï,
10:17	Adoniyah, Bigvaï, Adin,
10:18	Ather, Hizqiyah, Azzour,
10:19	Hodiyah, Hashoum, Betsaï,
10:20	Hariph, Anathoth, Nébaï,
10:21	Magpiash, Meshoullam, Hézir,
10:22	Meshézabeel, Tsadok, Yaddoua,
10:23	Pelatyah, Hanan, Anayah,
10:24	Hoshea, Chananyah, Hashoub,
10:25	Hallochesh, Pilcha, Shobek,
10:26	Rehoum, Hashabnah, Ma`aseyah,
10:27	Achiyah, Hanan, Anan,
10:28	Mallouk, Harim et Ba`anah.
10:29	Quant au reste du peuple, les prêtres, les Lévites, les portiers, les chanteurs, les Néthiniens et tous ceux qui s'étaient séparés des peuples de ces terres pour suivre la torah d'Elohîm, leurs femmes, leurs fils et leurs filles, tous ceux qui avaient de la connaissance et du discernement,
10:30	se joignirent à leurs frères, les grands, et s'engagèrent par imprécation et serment, de marcher dans la torah d'Elohîm, qui avait été donnée par la main de Moshé, serviteur d'Elohîm, de garder et de mettre en pratique tous les commandements de YHWH, notre Adonaï, ses jugements et ses ordonnances,
10:31	de ne pas donner nos filles aux peuples de la terre et de ne pas prendre leurs filles pour nos fils.
10:32	Quant aux peuples de la terre qui font venir des marchandises et toutes sortes de grains le jour du shabbat, pour les vendre, nous ne les prendrons pas d’eux, le shabbat, ni un jour sacré. Nous abandonnerons la septième année, ainsi que la charge de toute main.
10:33	Nous nous sommes imposés des commandements pour donner un tiers de sicle par année pour le service de la maison de notre Elohîm,
10:34	pour les rangées de pain, pour l'offrande perpétuelle et pour l'holocauste perpétuel, pour ceux des shabbats, des nouvelles lunes et des fêtes, pour les choses consacrées, pour les sacrifices pour le péché afin de faire la propitiation pour Israël et pour toute l'œuvre de la maison de notre Elohîm.
10:35	Nous avons fait tomber les sorts au sujet de l'offrande du bois, nous, les prêtres, les Lévites et le peuple, pour les faire venir à la maison de notre Elohîm, selon les maisons de nos pères et dans les temps fixés, d'année en année, pour le brûler sur l'autel de YHWH, notre Elohîm, ainsi qu'il est écrit dans la torah,
10:36	et pour faire venir les prémices de notre sol et les prémices de tous les fruits de tous les arbres, à la maison de YHWH, d'année en année,
10:37	les premiers-nés de nos fils et de nos bêtes, comme il est écrit dans la torah, et de faire venir à la maison de notre Elohîm, aux prêtres qui font le service dans la maison de notre Elohîm, les premiers-nés de nos bœufs et de notre petit bétail,
10:38	et les prémices de notre pâte, nos offrandes, les fruits de tous les arbres, le vin nouveau et l'huile, aux prêtres, dans les chambres de la maison de notre Elohîm, et la dîme de notre sol aux Lévites. Et eux, les Lévites, prendront les dîmes dans toutes les villes agricoles.
10:39	Le prêtre, fils d'Aaron, sera avec les Lévites lorsque les Lévites prendront la dîme<!--Il est question de la dîme des Lévites (No. 18:24 ; De. 14:28-29).-->. Et les Lévites monteront la dîme de la dîme<!--Il s'agit ici de la dîme de la dîme que les Lévites donnaient aux prêtres. Elle était apportée aux magasins du temple. Voir commentaires en No. 18:21 et Mal. 3:10.--> à la maison de notre Elohîm, dans les chambres de la maison où sont les magasins<!--Le mot hébreu « owtsar » (trésor) signifie aussi magasin (Né. 12:44, 13:12-13).-->.
10:40	Car les fils d'Israël et les fils de Lévi feront venir dans ces chambres les offrandes du blé, du vin nouveau et de l'huile, là sont les ustensiles du sanctuaire, et les prêtres qui font le service, les portiers et les chanteurs. Ainsi, nous n'abandonnerons pas la maison de notre Elohîm.

## Chapitre 11

### Les habitants de Yeroushalaim (Jérusalem)

11:1	Les chefs du peuple habitèrent à Yeroushalaim. Le reste du peuple fit tomber les sorts pour faire venir un sur dix habiter Yeroushalaim, la ville sainte. Les neuf parties étant pour les villes.
11:2	Le peuple bénit tous ceux qui se présentèrent volontairement pour habiter à Yeroushalaim.
11:3	Voici les têtes de la province qui habitèrent à Yeroushalaim et dans les villes de Yéhouda. Chaque homme dans sa propriété, selon sa ville, Israël, prêtres, Lévites, Néthiniens, et les fils des serviteurs de Shelomoh.
11:4	À Yeroushalaim habitèrent des fils de Yéhouda et des fils de Benyamin. Des fils de Yéhouda : Athayah, fils d'Ouzyah, fils de Zekaryah, fils d'Amaryah, fils de Shephatyah, fils de Mahalal'el, d'entre les fils de Pérets,
11:5	et Ma`aseyah, fils de Baroukh, fils de Kol-Hozeh, fils de Chazayah, fils d'Adayah, fils de Yoyariyb, fils de Zekaryah, fils de Shiloni.
11:6	Total des fils de Pérets, qui s'établirent à Yeroushalaim : 468 hommes talentueux.
11:7	Voici les fils de Benyamin : Sallou, fils de Meshoullam, fils de Yoed, fils de Pedayah, fils de Qolayah, fils de Ma`aseyah, fils d'Ithiel, fils de Yesha`yah,
11:8	et après lui, Gabbaï et Sallaï : 928.
11:9	Yoel, fils de Zicri, était leur chef, et Yéhouda, fils de Hassenoua, était le second chef de la ville.
11:10	Des prêtres : Yekda`yah, fils de Yoyariyb, Yakiyn,
11:11	Serayah, fils de Chilqiyah, fils de Meshoullam, fils de Tsadok, fils de Merayoth, fils d'Ahitoub, prince de la maison d'Elohîm,
11:12	et leurs frères, faisant le service de la maison : 822. Adayah, fils de Yeroham, fils de Pelalyah, fils d'Amtsi, fils de Zekaryah, fils de Pashhour, fils de Malkiyah,
11:13	et ses frères, têtes des pères : 242 ; et Amashsaï, fils d'Azareel, fils d'Achzaï, fils de Meshillémoth, fils d'Immer,
11:14	et leurs frères, vaillants et talentueux : 128. Zabdiel, fils de Guedolim, leur chef.
11:15	Des Lévites : Shema’yah, fils de Hashoub, fils d'Azrikam, fils de Chashabyah, fils de Bounni,
11:16	Shabbethaï et Yozabad chargés des ouvrages extérieurs de la maison d'Elohîm, d'entre les têtes des Lévites.
11:17	Mattanyah, fils de Miyka, fils de Zabdi, fils d'Asaph, était la tête qui commençait le premier à chanter les louanges dans la prière, et Baqboukyah, le second parmi ses frères, puis Abda, fils de Shammoua, fils de Galal, fils de Yedoutoun.
11:18	Total des Lévites dans la ville sainte : 284.
11:19	Et les portiers : Aqqoub, Thalmon, et leurs frères qui gardaient les portes : 172.

### Les habitants des autres villes

11:20	Le reste d'Israël, des prêtres et des Lévites, s'établit dans toutes les villes de Yéhouda, chacun dans son héritage.
11:21	Mais les Néthiniens habitèrent sur la colline. Tsicha et Guishpa étaient préposés sur les Néthiniens.
11:22	Celui qui avait la charge des Lévites à Yeroushalaim était Ouzzi, fils de Bani, fils de Chashabyah, fils de Mattanyah, fils de Miyka, d'entre les fils d'Asaph, chanteurs, pour l'ouvrage de la maison d'Elohîm,
11:23	car il y avait un commandement du roi à leur égard, et il y avait un salaire fixe pour les chanteurs, parole du jour en son jour.
11:24	Pethachyah, fils de Meshézabeel, d'entre les fils de Zérach, fils de Yéhouda, était sous la main du roi pour toutes les affaires du peuple.
11:25	Dans les villages et leurs territoires, quelques-uns des fils de Yéhouda habitèrent à Qiryath-Arba et ses filles, à Dibon et ses filles, à Yeqabtseel et dans ses villages,
11:26	à Yéshoua, à Moladah, à Beth-Paleth,
11:27	à Hatsar-Shoual, à Beer-Shéba et ses filles,
11:28	à Tsiklag, à Mekonah et ses filles,
11:29	à En-Rimmon, à Tsor`ah, à Yarmouth,
11:30	à Zanoach, à Adoullam, et dans leurs villages, à Lakis et dans ses territoires, à Azéqah et ses filles. Ils habitèrent depuis Beer-Shéba jusqu'à la vallée de Hinnom.
11:31	Les fils de Benyamin : depuis Guéba, Micmash, Ayyah, Béth-El et ses filles,
11:32	Anathoth, Nob, Ananyah,
11:33	Hatsor, Ramah, Guitthaïm,
11:34	Hadid, Tseboïm, Neballath,
11:35	Lod et Ono, la vallée des ouvriers.
11:36	D'entre les Lévites, des classes de Yéhouda se rattachèrent à Benyamin.

## Chapitre 12

### Les prêtres et les Lévites montés avec Zerubbabel (Zorobabel)

12:1	Voici les prêtres et les Lévites qui montèrent avec Zerubbabel, fils de Shealthiel, et avec Yéshoua : Serayah, Yirmeyah, Ezra,
12:2	Amaryah, Mallouk, Hattoush,
12:3	Shekanyah, Rehoum, Merémoth,
12:4	Iddo, Guinnethoï, Abiyah,
12:5	Miyamin, Ma`adyah, Bilgah,
12:6	Shema’yah, Yoyariyb, Yekda`yah,
12:7	Sallou, Amok, Chilqiyah, Yekda`yah. Ce furent là les têtes des prêtres, et de leurs frères, du temps de Yéshoua.
12:8	Lévites : Yéshoua, Binnouï, Qadmiy'el, Sherebyah, Yéhouda, Mattanyah, qui présidait au chant des louanges, lui et ses frères.
12:9	Baqboukyah et Ounni, leurs frères, qui étaient en fonction vis-à-vis d'eux.

### Les fils des prêtres

12:10	Yéshoua engendra Yoyaqiym, Yoyaqiym engendra Élyashiyb, Élyashiyb engendra Yoyada,
12:11	Yoyada engendra Yonathan, et Yonathan engendra Yaddoua.
12:12	Au temps de Yoyaqiym, les prêtres têtes des pères étaient : pour Serayah, Merayah ; pour Yirmeyah, Chananyah ;
12:13	pour Ezra, Meshoullam ; pour Amaryah, Yehohanan ;
12:14	pour Melouki, Yonathan ; pour Shebanyah, Yossef ;
12:15	pour Harim, Adna ; pour Merayoth, Helkaï ;
12:16	pour Iddo, Zekaryah ; pour Guinnethon, Meshoullam ;
12:17	pour Abiyah, Zicri ; pour Minyamin et Mo`adyah, Pilthaï ;
12:18	pour Bilgah, Shammoua ; pour Shema’yah, Yehonathan ;
12:19	pour Yoyariyb, Matthnaï ; pour Yekda`yah, Ouzzi ;
12:20	pour Sallaï, Kallaï ; pour Amok, Héber ;
12:21	pour Chilqiyah, Chashabyah ; pour Yekda`yah, Netanél.

### Les chefs des fils de Lévi

12:22	Aux jours d'Élyashiyb, de Yoyada, de Yohanan et de Yaddoua, les Lévites, têtes de famille et les prêtres, furent inscrits sous le règne de Darius, le Perse.
12:23	Les fils de Lévi, têtes des pères, furent enregistrés dans le livre de « Discours des jours », jusqu’aux jours de Yohanan, fils d'Élyashiyb.
12:24	Les têtes des Lévites : Chashabyah, Sherebyah et Yéshoua, fils de Qadmiy'el, et leurs frères, pour louer et célébrer, selon l'ordre de David, homme d'Elohîm, une garde juxtaposée à une garde.
12:25	Mattanyah, Baqboukyah, Obadyah, Meshoullam, Thalmon, et Aqqoub, les portiers, faisaient la garde au seuil des portes.
12:26	Ce fut aux jours de Yoyaqiym, fils de Yéshoua, fils de Yotsadak, et aux jours de Nehemyah, le gouverneur, et d'Ezra, prêtre et scribe.

### La dédicace de la muraille de Yeroushalaim (Jérusalem)

12:27	Lors de la dédicace de la muraille de Yeroushalaim, on envoya chercher les Lévites de tous les lieux où ils étaient, pour les faire venir à Yeroushalaim, afin de faire la dédicace avec joie, avec des louanges, des chants, des cymbales, des luths et des harpes.
12:28	Les fils des chanteurs se rassemblèrent du district des alentours de Yeroushalaim, des villages des Netophathiens,
12:29	de Beth-Guilgal, et des territoires de Guéba et d'Azmaveth, car les chanteurs s'étaient bâtis des villages aux alentours de Yeroushalaim.
12:30	Les prêtres et les Lévites se purifièrent, et ils purifièrent le peuple, les portes et la muraille.
12:31	Je fis monter sur la muraille les chefs de Yéhouda, et j'établis deux grandes processions de louange, à droite sur la muraille, à la porte du fumier.
12:32	Après eux marchait Hosha`yah, avec la moitié des chefs de Yéhouda,
12:33	Azaryah, Ezra, Meshoullam,
12:34	Yéhouda, Benyamin, Shema’yah et Yirmeyah,
12:35	des fils des prêtres avec les trompettes, Zekaryah, fils de Yonathan, fils de Shema’yah, fils de Mattanyah, fils de Miykayah, fils de Zakkour, fils d'Asaph,
12:36	et ses frères, Shema’yah, Azareel, Milalaï, Guilalaï, Maaï, Netanél, Yéhouda, et Chananiy, avec les instruments des cantiques de David, homme d'Elohîm. Ezra, le scribe, marchait devant eux.
12:37	À la porte de la source, qui était vis-à-vis d'eux, ils montèrent les marches de la cité de David, par la montée de la muraille, depuis la maison de David, jusqu'à la porte des eaux, vers l'est.
12:38	La seconde louange marchait en avant, et moi-même derrière elle, ainsi que la moitié du peuple, sur la muraille, au-dessus de la tour des fours, jusqu'à la muraille large,
12:39	et au-dessus de la porte d'Éphraïm, vers la vieille porte, vers la porte des poissons, la tour de Hananeel, et la tour de Méah, jusqu'à la porte des brebis. Et l'on s'arrêta à la porte de la prison.
12:40	Les deux louanges se tinrent debout dans la maison d'Elohîm, moi aussi, avec les dirigeants qui étaient avec moi,
12:41	et les prêtres Élyakim, Ma`aseyah, Minyamin, Miykayah, Élyehow`eynay, Zekaryah, Chananyah, avec les trompettes,
12:42	et Ma`aseyah, Shema’yah, Èl’azar, Ouzzi, Yehohanan, Malkiyah, Éylam et Ézer. Puis les chanteurs, desquels Yizrachyah avait la charge, se firent entendre.
12:43	Ils sacrifièrent ce jour-là de nombreux sacrifices et ils se réjouirent, car Elohîm les avait réjouis d’une grande joie. Les femmes et les enfants se réjouirent aussi et la joie de Yeroushalaim fut entendue au loin.

### Les prêtres et les Lévites à leur poste

12:44	En ce jour, on désigna des hommes sur les chambres des magasins pour les offrandes, pour les prémices et pour les dîmes, afin d’y recueillir, du territoire des villes, les portions ordonnées par la torah aux prêtres et aux Lévites. Car Yéhouda se réjouissait de ce que les prêtres et de ce que les Lévites étaient à leur poste,
12:45	et parce qu'ils avaient gardé la charge qui leur avait été donnée de la part de leur Elohîm, et la charge de la purification. Les chanteurs et les portiers remplissaient aussi leurs fonctions, selon le commandement de David, et de Shelomoh, son fils.
12:46	Car aux jours de David et d’Asaph, dans l’ancien temps, il y avait des têtes de chanteurs et des chants de louange pour louer Elohîm.
12:47	Tout Israël, aux jours de Zerubbabel et de Nehemyah, donna les portions des chanteurs et des portiers, jour par jour ; on donna aux Lévites les choses consacrées, et les Lévites donnèrent aux fils d'Aaron les choses consacrées.

## Chapitre 13

### Lecture du livre de Moshé, séparation d'avec les étrangers

13:1	En ce jour-là, on a lu dans le livre de Moshé, aux oreilles du peuple et l'on y a trouvé écrit que les Ammonites et les Moabites n'entreraient jamais dans l'assemblée d'Elohîm<!--De. 23:4.-->,
13:2	parce qu'ils n'étaient pas venus au-devant des fils d'Israël avec du pain et de l'eau et qu'ils avaient engagé Balaam<!--Balaam : voir No. 22-24.--> contre eux pour les maudire. Mais notre Elohîm avait changé la malédiction en bénédiction.
13:3	Il arriva que lorsqu’ils entendirent la torah, on sépara d'Israël toutes les races mélangées.

### Purification des chambres du temple

13:4	Or avant cela, Élyashiyb, prêtre établi sur les chambres de la maison de notre Elohîm, un proche de Tobiyah,
13:5	avait fait pour lui une grande chambre où l'on mettait auparavant les offrandes, l'encens, les ustensiles, les dîmes du blé, du vin nouveau et de l'huile, c'était un commandement pour les Lévites, pour les chanteurs et pour les portiers, avec les contributions pour les prêtres.
13:6	Quand tout cela eut lieu, je n’étais pas à Yeroushalaim, car j'étais retourné vers le roi la trente-deuxième année d'Artaxerxès, roi de Babel. Et à la fin de l'année, j'ai demandé au roi la permission
13:7	d'aller à Yeroushalaim et j'ai discerné le mal fait par Élyashiyb en faveur de Tobiyah, en lui faisant une chambre dans le parvis de la maison d'Elohîm.
13:8	Cela me déplut beaucoup et j'ai jeté tous les objets de la maison de Tobiyah hors de la chambre.
13:9	J'ai ordonné qu'on purifie les chambres avant d'y faire revenir les objets de la maison d'Elohîm, les offrandes et l'encens.

### Sur les portions des Lévites

13:10	J'ai su que les portions des Lévites ne leur avaient pas été données, et que les Lévites et les chanteurs qui faisaient le service s'étaient enfuis chaque homme vers son champ.
13:11	J'ai fait des reproches aux dirigeants en leur disant : Pourquoi la maison d'Elohîm a-t-elle été abandonnée ? Je les ai rassemblés et les ai placés à leur poste.
13:12	Tout Yéhouda apporta les dîmes du blé, du vin nouveau et de l'huile dans les magasins.
13:13	J'ai établi des surveillants pour les magasins, Shelemyah le prêtre, Tsadok le scribe, et Pedayah l'un des Lévites, avec sous leur main, Hanan, fils de Zakkour, fils de Mattanyah, parce qu'ils étaient considérés comme fidèles. Et je les ai chargés de faire les distributions à leurs frères.
13:14	Souviens-toi de moi, mon Elohîm, à cause de cela et n'efface pas ce que j'ai fait avec fidélité pour la maison de mon Elohîm, et pour ce qu'il est ordonné d'y faire !

### Avertissement pour le respect du shabbat

13:15	En ces jours-là, le shabbat, j'ai vu en Yéhouda des gens fouler aux pressoirs, rentrer des tas et charger même sur des ânes du vin, des raisins, des figues et toutes sortes de fardeaux pour les introduire à Yeroushalaim le jour du shabbat. Je les ai avertis, le jour où ils vendaient leurs provisions.
13:16	Et les Tyriens qui y habitaient, faisaient venir du poisson et toutes sortes de marchandises. Ils les vendaient, le shabbat, aux fils de Yéhouda et dans Yeroushalaim.
13:17	J'ai fait des reproches aux nobles de Yéhouda en leur disant : Que signifie cette action mauvaise que vous faites, en profanant le jour du shabbat ?
13:18	Vos pères n'ont-ils pas fait la même chose, et n'est-ce pas pour cela que notre Elohîm a fait venir tout ce mal sur nous et sur cette ville ? Et vous, vous ajoutez la chaleur contre Israël en violant le shabbat !
13:19	Il arriva que, lorsque les portes de Yeroushalaim commencèrent à être dans l’ombre, avant le shabbat, j'ai dit que les portes soient fermées, et j'ai dit qu’on ne les ouvre pas jusqu’après le shabbat. J'ai placé quelques-uns de mes serviteurs aux portes, afin d'empêcher l'entrée des fardeaux le jour du shabbat.
13:20	Les marchands et les vendeurs de toutes sortes de marchandises ont passé la nuit hors de Yeroushalaim, une ou deux fois.
13:21	J'ai témoigné contre eux en leur disant : Pourquoi passez-vous la nuit devant la muraille ? Si vous le faites encore, je mettrai la main sur vous. Depuis ce temps-là, ils ne sont plus venus le shabbat.
13:22	J'ai dit aux Lévites d'être purs et de venir garder les portes pour sanctifier le jour du shabbat. Souviens-toi de moi, mon Elohîm, à cause de cela, et aie compassion de moi selon la grandeur de ta miséricorde !

### Condamnation des unions mixtes ; rétablissement des fonctions des prêtres et des Lévites

13:23	En ces jours-là aussi, je vis que les Juifs avaient fait habiter chez eux des femmes asdodiennes, ammonites et moabites.
13:24	Leurs fils parlaient à moitié l'asdodien et ne savaient pas parler la langue judaïque, mais ils parlaient la langue d'un peuple ou d'un autre peuple.
13:25	Je leur ai fait des reproches et les ai maudits. J'ai frappé quelques-uns d'entre eux, je leur ai arraché des cheveux et leur ai fait jurer par Elohîm en disant : Vous ne donnerez pas vos filles à leurs fils et vous ne prendrez leurs filles ni pour vos fils ni pour vous.
13:26	Shelomoh, le roi d'Israël, n'avait-il pas péché par ce moyen<!--1 R. 11.--> ? Il n'y avait pas de roi semblable à lui parmi un grand nombre de nations, il était aimé de son Elohîm, et Elohîm l’avait donné pour roi de tout Israël. Mais lui aussi, les femmes étrangères l’ont fait pécher.
13:27	Vous écouterions-nous pour faire tout ce grand mal, pour commettre un délit contre notre Elohîm en faisant habiter chez nous des femmes étrangères ?
13:28	Un des fils de Yoyada, fils d'Élyashiyb, le grand-prêtre, était gendre de Sanballat, le Horonite. Je l'ai chassé loin de moi.
13:29	Souviens-toi d'eux, mon Elohîm ! à cause de la souillure de la prêtrise, de l’alliance de la prêtrise et des Lévites !
13:30	Je les ai purifiés de tous les étrangers, j'ai fait tenir debout<!--Hé. 10:11.--> les prêtres et les Lévites dans leur fonction, chaque homme dans son service,
13:31	pour l'offrande de bois aux temps fixés et pour les prémices. Souviens-toi de moi en bien, mon Elohîm !
