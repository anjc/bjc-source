# Bereshit (Genèse) (Ge.)

Signification : Au commencement

Auteur : Probablement Moshé (Moïse)

Thème : La création de l'être humain

Date de rédaction : Env. 1450 - 1410 av. J.-C.

Premier livre du Tanakh, Bereshit est le livre du commencement. Il relate l'histoire des origines de l'humanité, la création des cieux, de la Terre et de tout ce qui s'y trouve par YHWH, l'Elohîm créateur.

Il y est décrit le péché de l'être humain et sa séparation d'avec Elohîm, ainsi que la décadence de l'univers qui en résulta. En réponse à la méchanceté du cœur de l'humain, YHWH exerça sa justice en détruisant la Terre par le déluge. Dans sa prescience, YHWH avait cependant résolu de se réconcilier avec l'être humain. Il se révéla donc comme Sauveur en accordant sa grâce à Noah (Noé) et à sa famille. Après cet événement, les êtres humains se tournèrent une fois de plus vers le mal en tentant Elohîm par la construction de la tour de Babel, œuvre à l'origine de la dispersion des nations.

Ce livre présente aussi l'élection d'Abraham, originaire d'Our en Chaldée (Mésopotamie antique, dans l'actuel Irak), qui reçut la promesse divine de devenir une grande nation, en qui toutes les familles de la Terre seraient bénies. Le récit se poursuit par l'histoire de ses descendants : Yitzhak (Isaac), Yaacov (Jacob) et ses douze fils, qui formèrent par la suite la nation d'Israël.

## Chapitre 1

### La Terre devint tohu et bohu

1:1	<w lemma="strong:H07225">Au commencement</w><!--Bereshit bara elohîm èth hashamaïm vehèth ah aretz. Dans ce passage le mot « èth » est composé de la première et la dernière lettre de l'alphabet hébraïque « aleph » et « tav ». Aleph Tav apparaît plus de 7 000 fois dans le Tanakh. Nous notons la présence d'Aleph Tav : au commencement (Ge. 1:1), avec la Lumière (Ge. 1:4), Adam (Ge. 4:1), Hanowk (Hénoc, Ge. 5:22), Yossef (Joseph, Ge. 37:23), Moshé (Moïse, Ex. 18:23), Aaron et le pectoral (Ex. 28:28-30), les Hébreux (No. 14:22), Shema Israël (De. 6:4). Aleph Tav est le symbole du caractère hébreu considéré comme la signature du Mashiah (Messie) : Et les Juifs regarderont vers Aleph Tav, lequel ils ont percé (Za. 12:10).--> <w lemma="strong:H0430">Elohîm</w> <w morph="strongMorph:TH8804" lemma="strong:H0853 strong:H01254">créa</w> <w lemma="strong:H08064">les cieux</w> <w lemma="strong:H0853">et</w> <w lemma="strong:H0776">la Terre</w>.
1:2	La Terre devint<!--Voir Es. 45:18.--> tohu et bohu<!--Les mots hébreux « tohuw » et « bohuw » désignent la confusion, le chaos, la vanité. Voir Jé. 4:23.-->. La ténèbre<!--Le mot hébreu est au singulier.--> était sur les faces de l'abîme et l'Esprit d'Elohîm planait<!--De. 32:11.--> sur les faces des eaux.

### Jour « un » : Apparition de la lumière

1:3	Elohîm avait dit : La lumière apparaîtra<!--Es. 9:1 ; Mt. 4:16 ; Jn. 1:1-5. Cette lumière n'est autre que YHWH lui-même qui va s'incarner en la personne de Yéhoshoua Mashiah (Jésus-Christ) pour chasser la ténèbre (2 S. 22:9-12 ; Es. 60:1,19-20 ; Jn. 1:1-5, 8:12-14 ; 2 Co. 4:6).--> ! Et la lumière apparut.
1:4	Elohîm vit que la lumière était bonne, et Elohîm sépara la lumière de la ténèbre<!--Ou « Elohîm mit une séparation entre la lumière et la ténèbre ».-->.
1:5	Elohîm appela la lumière jour, et il appela la ténèbre nuit<!--La lumière et la ténèbre, ainsi que leurs champs lexicaux respectifs, personnifient souvent Yéhoshoua (Jésus) et Satan. Ainsi, Yéhoshoua est la Lumière du monde (Jn. 9:5), l'Étoile brillante du matin (Ap. 22:16), le Soleil levant ou le Soleil de la justice (Mal. 3:20 ; Ps. 19:6-7 ; Lu. 1:78). Il est associé au jour (Jn. 9:4) d'où les expressions « Jour du Seigneur » (1 Th. 5:2) ou « Jour de YHWH » (Joë. 1:15). À l'inverse, la Bible associe Satan aux ténèbres (Es. 8:23 ; Ps. 143:3 ; Ep. 6:12 ; Col. 1:13) et à la nuit (Jn. 9:4 ; Ro. 13:12).-->. Le soir apparut, et le matin apparut<!--Contrairement au calendrier grégorien où le jour commence à minuit, selon Elohîm et le calendrier hébraïque, le jour commence le soir à 18 heures pour se terminer le lendemain à la même heure. Voir commentaire en Mc. 16:9.--> : un jour<!--L'hébreu utilise le terme « ehad » qui signifie « un », au sens de l'indivisible. Elohîm se révèle aux Hébreux comme étant un. De. 6:4: « Shema Yisrael YHWH elohénou YHWH ehad » (« Écoute Israël, YHWH [est] notre Elohîm, YHWH [est] un »). YHWH est un et indivisible.-->.

### Second jour : Un firmament entre les eaux

1:6	Elohîm avait dit : Le firmament apparaîtra au milieu des eaux, et il apparut pour séparer les eaux d'avec les eaux.
1:7	Elohîm fit le firmament et sépara les eaux qui sont au-dessous du firmament d’avec les eaux qui sont au-dessus du firmament. Il en fut ainsi.
1:8	Elohîm appela le firmament cieux. Le soir apparut, et le matin apparut : second jour.

### Troisième jour : Les mers, la terre et la végétation

1:9	Elohîm avait dit : Les eaux qui sont au-dessous des cieux seront rassemblées en un lieu et le sec apparaîtra. Il en fut ainsi.
1:10	Elohîm appela le sec terre, et il appela la masse des eaux mers. Et Elohîm vit que cela était bon.
1:11	Elohîm avait dit : La terre fera sortir de la végétation, de l'herbe semant de la semence, l'arbre à fruit faisant du fruit selon son espèce, dont la semence était en lui sur la terre ! Il en fut ainsi.
1:12	La terre fit sortir de la végétation, de l'herbe semant de la semence, selon son espèce, et de l'arbre faisant du fruit, dont la semence était en lui, selon son espèce. Elohîm vit que cela était bon.
1:13	Le soir apparut, et le matin apparut : troisième jour.

### Quatrième jour : Les luminaires des cieux

1:14	Elohîm avait dit : Des luminaires apparaîtront dans le firmament des cieux pour séparer la nuit d'avec le jour. Ils deviendront des signes pour les temps fixés<!--Voir les fêtes de YHWH en annexe. Ex. 23:15.-->, pour les jours et pour les années,
1:15	et ils deviendront des luminaires dans le firmament des cieux et la lumière sur la Terre. Il en fut ainsi.
1:16	Elohîm fit les deux grands luminaires, le grand luminaire pour dominer le jour et le petit luminaire pour dominer la nuit, ainsi que les étoiles.
1:17	Elohîm les plaça dans le firmament des cieux pour être la lumière sur la Terre,
1:18	pour gouverner le jour et la nuit, pour séparer la lumière d'avec la ténèbre. Elohîm vit que cela était bon.
1:19	Le soir apparut, et le matin apparut : quatrième jour.

### Cinquième jour : Les animaux marins et volants<!--Ge. 2:19.-->

1:20	Elohîm avait dit : Les eaux grouilleront de choses grouillantes, d'âmes vivantes et des créatures volantes voleront sur la terre, sur les faces du firmament des cieux.
1:21	Elohîm créa les grands monstres marins et toutes les âmes vivantes qui se meuvent, dont grouillèrent les eaux, selon leur espèce, et toute créature volante ayant des ailes selon son espèce. Elohîm vit que cela était bon.
1:22	Elohîm les bénit, en disant : Portez du fruit, multipliez-vous, remplissez les eaux des mers et que les créatures volantes se multiplient sur la Terre !
1:23	Le soir apparut, et le matin apparut : cinquième jour.

### Sixième jour : Les animaux terrestres

1:24	Elohîm avait dit : La terre fera sortir des âmes vivantes selon leurs espèces, le bétail, les choses rampantes, le vivant de la terre, chacun selon son espèce. Il en fut ainsi.
1:25	Elohîm fit le vivant de la terre selon son espèce, le bétail selon son espèce, et toutes les choses rampantes du sol selon leur espèce. Elohîm vit que cela était bon.

### Mission confiée à l'être humain (Adam) ; son autorité sur la création

1:26	Elohîm avait dit : Nous ferons<!--Le verbe faire ici en hébreu est à l'imparfait (ou « futur », ou non accompli). Il exprime une action, un processus ou une condition non accompli, inachevé, prolongé ou répété. YHWH est le Seul Créateur Ge. 6:7. En 2 S. 24:14, David, en parlant de lui-même, utilise un pluriel (tombons).--> l'être humain<!--Il existe plusieurs termes hébreux qui ont été traduits en français par « homme », mot qui peut avoir plusieurs sens. Dans cette traduction, nous avons fait le choix de mettre en évidence ces subtilités pour la bonne compréhension du lecteur. Le terme hébreu [’âdâm] signifie littéralement « être humain », « de la terre ». Ce mot, qui est le plus fréquemment employé dans le Tanakh, désigne le genre humain dans sa globalité. Son équivalent grec est [ánthrōpos] (voir commentaire dans Mt. 4:4). On le retrouve notamment dans Ge. 1:26-27, 2:7,15-25, 3:8-9,12,20-24, 4:1. Lire également les autres termes [’Âdâm] rendus par « Adam » (voir commentaire dans Ge. 3:17) et [’îysh] en rapport avec le mâle ou le mari (voir commentaire dans Ge. 2:23).--> à notre image, selon notre ressemblance<!--L'image d'Elohîm n'est autre que Yéhoshoua ha Mashiah lui-même (Col. 1:15). Le dernier Adam (1 Co. 15:40-49) qui est venu comme Fils, afin de nous montrer le modèle de fils et de filles qu'Elohîm souhaite (Ro. 8:29). Nous avons ici une autre image de l'incarnation d'Elohîm en la personne de Yéhoshoua ha Mashiah. Ainsi, avant que l'homme ne pèche, le projet de la rédemption était déjà là (1 Pi. 1:19-21).-->. Ils domineront<!--Le verbe est au pluriel dans les sources hébraïques.--> sur les poissons de la mer, sur les créatures volantes des cieux, sur le bétail, sur toute la Terre et sur toute chose rampante qui rampe sur la terre.
1:27	Elohîm créa l'être humain à son image, il l’a créé à l'image d'Elohîm. Mâle et femelle il les a créés.
1:28	Elohîm les bénit, et Elohîm leur dit : Portez du fruit, multipliez-vous, remplissez la Terre et assujettissez-la. Dominez sur les poissons de la mer, sur les créatures volantes des cieux et sur tout vivant qui rampe sur la terre.
1:29	Elohîm avait dit : Voici, je vous ai donné toute herbe semant de la semence qui est sur les faces de toute la terre, et tout arbre ayant en lui du fruit d'arbre et semant de la semence, cela deviendra votre nourriture.
1:30	Pour tout vivant de la terre, pour toute créature volante des cieux, pour tout ce qui rampe sur la terre, ayant en soi une âme vivante, toute herbe verte sera la nourriture. Il en fut ainsi.
1:31	Elohîm vit tout ce qu'il avait fait, et voici cela était très bon. Le soir apparut, et le matin apparut : sixième jour.

## Chapitre 2

### Septième jour : Le repos (shabbat)

2:1	Les cieux et la Terre furent accomplis avec toute leur armée.
2:2	Elohîm accomplit au septième jour son œuvre qu'il avait faite, et il se reposa au septième jour de toute son œuvre qu'il avait faite.
2:3	Elohîm bénit le septième jour et le sanctifia, parce qu'en ce jour-là il s'était reposé de toute son œuvre qu'Elohîm avait créée en la faisant.
2:4	Telles sont les généalogies des cieux et de la Terre, lorsqu'ils furent créés, au temps où YHWH Elohîm fit la Terre et les cieux.
2:5	Il n'existait encore sur terre aucun arbrisseau des champs, aucune herbe des champs n'avait encore germé, car YHWH Elohîm n'avait pas fait pleuvoir sur la terre et il n'y avait pas d'être humain pour travailler<!--Ou « servir ».--> le sol.
2:6	Mais une vapeur montait de la terre et arrosait toutes les faces du sol.
2:7	YHWH Elohîm forma l'être humain à partir de la poussière du sol<!--Vient de l'hébreu « adamah ».-->. Il souffla dans ses narines le souffle de vies<!--Nishmat hayim. Ap. 1:18.--> et l'être humain devint une âme vivante.
2:8	Et YHWH Elohîm planta un jardin en Éden, à l'orient, et il y mit l'être humain qu'il avait formé.
2:9	YHWH Elohîm fit germer du sol tout arbre désirable à la vue et bon pour la nourriture, et l'arbre de vies<!--Dans ce passage, le terme hébreu traduit par « arbre » est au singulier, tandis que le mot « vie » est au pluriel : « chayyîym ». Voir Gn. 3:22.--> au milieu du jardin<!--Ap. 2:7.-->, et l'arbre de la connaissance de ce qui est bon ou mauvais.
2:10	Un fleuve sortait d'Éden pour arroser le jardin, et de là il se divisait et devenait quatre têtes.
2:11	Le nom de l'un est Pishon : c'est le fleuve qui coule en entourant toute la terre de Haviylah où il y a de l’or.
2:12	L'or de cette terre est bon et là se trouvent le bdellium et la pierre d'onyx.
2:13	Le nom du second fleuve est Guihon. C'est celui qui coule en entourant toute la terre de Koush<!--Le mot hébreu « kuwsh » traduit en français par « Koush » signifie Éthiopie (« noir, brûlé par le soleil »). Il est prophétiquement question de l'Afrique.-->.
2:14	Le nom du troisième fleuve est Hiddéqel, c'est celui qui coule à l'est de l'Assyrie. Le quatrième fleuve est l'Euphrate.
2:15	YHWH Elohîm prit l'être humain et le fit se reposer<!--« Se reposer » est la traduction de l'hébreu « yanach ». La vie chrétienne commence par le repos. Voir Ep. 2:6.--> dans le jardin d'Éden pour qu’il le travaille et le garde.
2:16	YHWH Elohîm donna cet ordre à l'être humain en disant : Manger, tu mangeras<!--Répétition du mot « mangeras » du mot hébreu « akal ». À l'origine, dans les écrits hébraïques, la répétition de mots était utilisée pour accentuer quelque chose dans le texte. Elle pouvait appuyer un fait précis, traduire une urgence, un danger ou indiquer de l'émotion et la renforcer.--> de tout arbre du jardin.
2:17	Mais quant à l'arbre de la connaissance de ce qui est bon ou mauvais, tu n'en mangeras pas, car le jour où tu en mangeras, mourir, tu mourras<!--L'hébreu « muwth » est utilisé deux fois de suite. Le premier est à l'infinitif et le deuxième à l'inaccompli (le futur). Le Seigneur met en garde contre ce qui arrive à tout être humain pécheur : la mort spirituelle puis la mort physique et enfin « la seconde mort » qui est la damnation éternelle dans le lac de feu (Ap. 2:11, 20:12-15).-->.
2:18	YHWH Elohîm avait dit : Il n'est pas bon que l'être humain soit seul. Je lui ferai un secours qui soit son vis-à-vis<!--Vient de l'hébreu « neged » qui signifie « ce qui est visible », « ce qui est en face de », « en face de ».-->.
2:19	YHWH Elohîm forma du sol tout vivant des champs et toutes les créatures volantes des cieux, puis il les fit venir vers l'être humain pour voir comment il les appellerait, afin que toute âme vivante porte le nom dont l'être humain l'appellerait.
2:20	L'être humain appela de leurs noms tout le bétail, et les créatures volantes des cieux et tout vivant des champs, mais pour l'être humain, il ne trouva pas de secours qui fût son vis-à-vis.
2:21	YHWH Elohîm fit tomber un profond sommeil sur l'être humain, qui s'endormit. Il prit l'un de ses côtés<!--Voir la note en Ge. 2:22.-->, et ferma la chair dessous<!--Voir Ge. 2:22.-->.
2:22	YHWH Elohîm bâtit une femme du côté<!--Le mot hébreu « tsela » généralement traduit en français par « côte » en Ge. 2:21-22 signifie d'abord « côté ». Partout ailleurs, ce mot a été traduit non par « côte » mais par « côté » : voir Ex. 25:12,14, 26:20,26-27,35, 27:7, 30:4, 36:25,31-32, 37:3,5,27, 38:7 et Job 18:12. Ce mot a été traduit par « flanc » en 2 S. 16:13. Voir Ge. 1:27.--> qu'il avait pris de l'être humain, et la fit venir vers l'être humain<!--1 Co. 11:8.-->.
2:23	L'être humain dit : Celle-ci pour le coup est l'os de mes os et la chair de ma chair<!--Voir Ge. 29:14 ; 2 S. 5:1, 19:12-14 ; 1 Ch. 11:1.-->. Celle-ci sera appelée femme, parce que de l'homme<!--Ce terme est [’îysh] en hébreu et se rapporte au genre masculin. Il désigne le mâle ou le mari. Il est notamment utilisé dans Ge. 2:23-24, 3:6,16, 4:1,23. Lire également les autres termes [’âdâm] rendus par « être humain » (voir commentaire dans Ge. 1:26) et [’Âdâm] le nom propre du premier être humain (voir commentaire dans Ge. 3:17).--> celle-ci a été prise.
2:24	C'est pourquoi l'homme<!--« Ish ».--> quittera son père et sa mère et se joindra à sa femme<!--« Ishshah ».-->, et ils deviendront une seule chair<!--Mt. 19:5 ; Mc. 10:7 ; 1 Co. 6:16 ; Ep. 5:30-31.-->.
2:25	Et ils étaient tous les deux nus, l'être humain et sa femme, et ils n'en avaient pas honte.

## Chapitre 3

3:1	Or le serpent<!--Satan ou le serpent ancien (Ap. 12:9, 20:2).--> devint plus rusé que tout vivant des champs que YHWH Elohîm avait fait. Il dit à la femme : Sûrement Elohîm a dit : Vous ne mangerez pas de tous les arbres du jardin !
3:2	La femme dit au serpent : Nous mangeons du fruit des arbres du jardin.
3:3	Mais quant au fruit de l'arbre qui est au milieu du jardin, Elohîm a dit : Vous n'en mangerez pas, vous n’y toucherez pas, de peur que vous ne mouriez !
3:4	Le serpent dit à la femme : Non, mourir, vous ne mourrez pas !
3:5	Mais Elohîm sait qu'à partir<!--Vient de l'hébreu « min » qui signifie : de, hors de, à cause, depuis. Ce mot fait aussi allusion à un matériau à partir duquel quelque chose est fabriqué, de sa source ou de son origine. Son équivalent grec est « ek ».--> du jour où vous en mangerez, vos yeux seront ouverts, et vous deviendrez comme Elohîm, connaissant ce qui est bon ou mauvais.
3:6	La femme vit que l'arbre était bon pour la nourriture, qu'il était appétissant pour les yeux et que l'arbre était désirable pour prospérer<!--« Être prudent », « être circonspect », « agir sagement », « comprendre ». De. 29:8 ; Jos. 1:8.-->. Elle prit de son fruit et en mangea. Elle en donna aussi à son homme qui était avec elle et il en mangea.
3:7	Les yeux de tous les deux s'ouvrirent et ils surent qu'ils étaient nus. Ils cousirent ensemble des feuilles de figuier pour se faire des ceintures.
3:8	Ils entendirent la voix de YHWH Elohîm marchant dans le jardin au vent du jour. Et l'être humain et sa femme se cachèrent, face à YHWH Elohîm, au milieu de l'arbre du jardin.
3:9	YHWH Elohîm appela Adam et lui dit : Où es-tu ?
3:10	Il dit : J'ai entendu ta voix dans le jardin, et j'ai eu peur parce que je suis nu, et je me suis caché.
3:11	YHWH dit : Qui t'a informé que tu es nu ? L’arbre dont je t’avais ordonné de ne pas manger, en as-tu mangé ?
3:12	Adam dit : La femme qu’avec moi tu as donnée, elle, m’a donné de l’arbre, et j’ai mangé.
3:13	YHWH Elohîm dit à la femme : Pourquoi as-tu fait cela ? La femme dit : Le serpent m'a trompée et j'ai mangé.
3:14	YHWH Elohîm dit au serpent : Parce que tu as fait cela, tu es maudit parmi toute bête, parmi tout vivant des champs. Tu marcheras sur ton ventre et tu mangeras de la poussière<!--La poussière dont il est question n'est autre que l'être humain (Ge. 3:19). Satan ne peut rien contre les véritables enfants d'Elohîm (Mt. 16:18 ; Lu. 10:19).--> tous les jours de ta vie.
3:15	Je mettrai inimitié entre toi et entre la femme<!--La femme représente en premier lieu Chavvah (Ève), la mère de tous les vivants. Ici, elle représente aussi Israël, l'épouse de YHWH selon Ge. 37:5-11 et Ap. 12:1.-->, entre ta postérité<!--La postérité du serpent regroupe l'homme impie (2 Th. 2:3-4 ; 1 Jn. 2:18-22) et tous ceux qui n'ont pas reçu Yéhoshoua ha Mashiah (Jésus-Christ) comme Seigneur et Sauveur. En effet, seuls ceux qui ont reçu Yéhoshoua dans leur vie sont appelés enfants d'Elohîm (Jn. 1:12 ; 1 Jn. 3:8-10, 5:19).--> et entre sa postérité<!--La postérité de la femme regroupe Yéhoshoua ha Mashiah homme (Es. 7:14 ; Lu. 2:4-7) et l'Assemblée (Église), le corps du Mashiah (Col. 1:24).-->. Lui<!--Ici l'hébreu utilise un pronom masculin. Il est question du Mashiah. Voir Ps. 108:14 ; 2 Th. 2:8 ; Ro. 16:20.-->, il t'écrasera la tête et toi tu lui écraseras le talon.
3:16	À la femme, il dit : Augmenter, j'augmenterai la douleur de tes grossesses. Tu enfanteras dans la douleur tes enfants. Tes désirs seront vers ton homme, mais lui, il dominera sur toi.
3:17	À Adam<!--Lorsque le mot adam prend une majuscule [’Âdâm], il désigne le nom propre du premier être humain. On trouve la première occurrence dans Ge. 3:17, puis dans Ge. 4:25, 5:1,3,4,5 ; mais aussi dans Jos. 3:16 ; 1 Ch. 1:1 ; Job 31:33. Lire également les autres termes [’âdâm] rendus par « être humain » (voir commentaire dans Ge. 1:26) et [’îysh] en rapport avec le mâle ou le mari (voir commentaire dans Ge. 2:23).-->, il dit : Parce que tu as écouté la voix de ta femme, et que tu as mangé de l'arbre au sujet duquel je t'avais donné cet ordre en disant : Tu n'en mangeras pas ! Le sol est maudit à cause de toi ! C'est dans la douleur que tu en mangeras tous les jours de ta vie.
3:18	Il fera germer pour toi des épines et des chardons et tu mangeras l'herbe des champs.
3:19	À la sueur de ton visage tu mangeras du pain jusqu’à ce que tu retournes au sol, car c’est de lui que tu as été pris. Car tu es poussière et tu retourneras à la poussière.
3:20	Adam appela sa femme du nom de Chavvah<!--« Vie », « vivant ». Généralement traduit par Ève.-->, car elle est devenue la mère de tous les vivants.
3:21	YHWH Elohîm habilla Adam et sa femme avec les tuniques de peaux qu'il leur avait faites.
3:22	YHWH Elohîm dit : Voici l'être humain devenu<!--ou était devenu.--> comme l'un de nous en connaissant le bon et le mauvais ! Maintenant, de peur qu'il n'avance sa main et ne prenne aussi de l’arbre de vies<!--Le mot hébreu est au pluriel. Voir Gn. 2:9.-->, n'en mange et ne vive à perpétuité...
3:23	YHWH Elohîm le renvoya du jardin d'Éden pour travailler le sol d'où il avait été pris.
3:24	Il chassa l'être humain et fit demeurer à l'orient du jardin d'Éden des chérubins, ainsi que la flamme<!--Voir Ex. 7:11.--> de l'épée qui tournait çà et là pour garder la voie de l'arbre de vies<!--Le mot hébreu est au pluriel. Voir Gn. 2:9.-->.

## Chapitre 4

4:1	Et Adam connut Chavvah<!--« Vie », « vivant ». Généralement traduit par Ève.--> sa femme. Elle devint enceinte et enfanta Qayin. Elle dit : J'ai acquis un homme avec YHWH.
4:2	Elle enfanta encore Abel, son frère. Abel devint berger de petit bétail et Qayin devint travailleur du sol<!--« cultivateur ».-->.
4:3	Il arriva au bout de quelques jours que Qayin fit venir des fruits du sol en offrande à YHWH.
4:4	Abel, lui aussi, fit venir des premiers-nés de son troupeau et leur graisse<!--Abel était juste et pieux, aussi il sut instinctivement apporter une offrande agréable à Elohîm (Mt. 23:35 ; Lu. 11:51 ; Hé. 11:4). En l'occurrence, son offrande préfigurait le sacrifice du Seigneur.-->. YHWH porta son regard sur Abel et sur son offrande,
4:5	mais il ne porta pas son regard sur Qayin ni sur son offrande. Qayin fut très fâché et ses faces tombèrent.
4:6	YHWH dit à Qayin : Pourquoi es-tu fâché et pourquoi tes faces sont-elles tombées<!--« Tomber », « être couché ».--> ?
4:7	Si tu agis bien, n’y aura-t-il pas élévation ? Si tu n’agis pas bien, le péché est couché à la porte et son désir se porte vers toi, mais toi, domine sur lui.
4:8	Qayin parla à Abel, son frère. Il arriva que comme ils étaient dans les champs, Qayin s'éleva contre Abel, son frère et le tua.
4:9	YHWH dit à Qayin : Où est Abel ton frère ? Il dit : Je ne sais pas. Suis-je le gardien de mon frère, moi ?
4:10	Il dit : Qu'as-tu fait ? La voix des sangs de ton frère crie du sol à moi.
4:11	Maintenant tu es maudit loin du sol qui a ouvert sa bouche pour recevoir de ta main les sangs de ton frère.
4:12	Quand tu travailleras le sol, il ne te donnera plus son fruit, et tu deviendras vagabond et fugitif sur la Terre.
4:13	Qayin dit à YHWH : Le châtiment de mon iniquité est trop grand pour être porté.
4:14	Voici que tu me chasses aujourd'hui sur les faces du sol. Je serai caché à tes faces, je deviendrai vagabond et fugitif sur la Terre, et il arrivera que quiconque me trouvera me tuera.
4:15	YHWH lui dit : Alors tout tueur de Qayin subira sept fois la vengeance. Et YHWH mit un signe sur Qayin afin que quiconque le trouverait ne le tue pas.
4:16	Qayin sortit de devant les faces de YHWH et habita en terre de Nod, à l'orient d'Éden.
4:17	Qayin connut sa femme. Elle devint enceinte et enfanta Hanowk<!--Hénoc.-->. Il devint un bâtisseur de ville, et appela le nom de la ville d'après le nom de son fils Hanowk.
4:18	Hanowk engendra Irad, Irad engendra Mehouyaël, Mehouyaël engendra Metoushaël, et Metoushaël engendra Lémek.
4:19	Lémek prit deux femmes. Le nom de l'une était Adah, et le nom de l'autre Tsillah.
4:20	Adah enfanta Yabal. Il devint le père de ceux qui habitent dans les tentes et près des troupeaux.
4:21	Le nom de son frère était Youbal. Il devint le père de tous ceux qui manient la harpe et la flûte.
4:22	Tsillah, elle aussi, enfanta Toubal-Qayin. Il forgeait toutes sortes d'instruments en cuivre et en fer. Et la sœur de Toubal-Qayin était Na`amah.
4:23	Lémek dit à Adah et à Tsillah ses femmes : Écoutez ma voix femmes de Lémek, prêtez l'oreille à ma parole ! Oui, j'ai tué un homme pour ma blessure et un enfant pour ma meurtrissure !
4:24	Oui, Qayin est vengé 7 fois, et Lémek, 77 fois !
4:25	Adam connut encore sa femme. Elle enfanta un fils et il l'appela du nom de Sheth, car dit-elle, Elohîm a mis une autre postérité pour moi à la place d'Abel que Qayin a tué.
4:26	Il naquit aussi un fils à Sheth, et il l'appela du nom d'Enowsh. C'est alors que l'on commença à faire appel<!--« Réciter », « lire », « s'écrier », « proclamer ».--> au nom de YHWH.

## Chapitre 5

5:1	Voici le livre de la généalogie d'Adam : Le jour où Elohîm créa l'être humain, il le fit à la ressemblance d'Elohîm.
5:2	Mâle et femelle, il les créa et les bénit. Il les appela du nom d'êtres humains<!--Littéralement « Adam ».--> le jour où ils furent créés.
5:3	Adam, ayant vécu 130 ans, engendra un fils à sa ressemblance, selon son image<!--Désormais, les hommes naissent à la ressemblance d'Adam, c'est-à-dire pécheurs (Ro. 3:23, 5:14-17).-->, et il appela son nom Sheth.
5:4	Les jours d'Adam, après qu'il eut engendré Sheth, furent de 800 ans, et il engendra des fils et des filles.
5:5	Tous les jours qu'Adam vécut furent de 930 ans, et il mourut<!--Gn. 2:16-17.-->.
5:6	Sheth, ayant vécu 105 ans, engendra Enowsh.
5:7	Sheth, après qu'il eut engendré Enowsh, vécut 807 ans et il engendra des fils et des filles.
5:8	Tous les jours que Sheth vécut furent de 912 ans, et il mourut.
5:9	Enowsh, ayant vécu 90 ans, engendra Qeynan.
5:10	Enowsh, après qu'il eut engendré Qeynan, vécut 815 ans et il engendra des fils et des filles.
5:11	Tous les jours qu'Enowsh vécut furent de 905 ans, et il mourut.
5:12	Qeynan, ayant vécu 70 ans, engendra Mahalal'el.
5:13	Qeynan, après qu'il eut engendré Mahalal'el, vécut 840 ans et il engendra des fils et des filles.
5:14	Tous les jours que Qeynan vécut furent de 910 ans, et il mourut.
5:15	Mahalal'el, ayant vécu 65 ans, engendra Yered.
5:16	Mahalal'el, après qu'il eut engendré Yered, vécut 830 ans et il engendra des fils et des filles.
5:17	Tous les jours que Mahalal'el vécut furent de 895 ans, et il mourut.
5:18	Yered, ayant vécu 162 ans, engendra Hanowk.
5:19	Yered, après avoir engendré Hanowk, vécut 800 ans et il engendra des fils et des filles.
5:20	Tous les jours que Yered vécut furent de 962 ans, et il mourut.
5:21	Hanowk, ayant vécu 65 ans, engendra Metoushèlah.
5:22	Hanowk, après qu'il eut engendré Metoushèlah, marcha avec Elohîm 300 ans et il engendra des fils et des filles.
5:23	Tous les jours que Hanowk vécut furent de 365 ans.
5:24	Hanowk marcha avec Elohîm, et il ne fut plus, parce qu'Elohîm l'avait pris.
5:25	Metoushèlah, ayant vécu 187 ans, engendra Lémek.
5:26	Metoushèlah, après qu'il eut engendré Lémek, vécut 782 ans et il engendra des fils et des filles.
5:27	Tous les jours que Metoushèlah vécut furent de 969 ans, et il mourut.
5:28	Lémek, ayant vécu 182 ans, engendra un fils.
5:29	Il l'appela du nom de Noah en disant : Celui-ci nous consolera de notre travail et de la douleur de nos mains provenant du sol que YHWH a maudit.
5:30	Lémek, après qu'il eut engendré Noah, vécut 595 ans et il engendra des fils et des filles.
5:31	Tous les jours que Lémek vécut furent de 777 ans, et il mourut.
5:32	Noah était fils de 500 ans, Noah engendra Shem, Cham et Yepheth.

## Chapitre 6

6:1	Or il arriva quand les êtres humains commencèrent à se multiplier sur les faces du sol et qu'ils eurent engendré des filles,
6:2	que les fils d'Elohîm<!--Ici, les fils d'Elohîm sont des anges qui ont quitté leur demeure (Jud. 1:5-7).--> voyant que les filles des êtres humains<!--Le mot « être humain » vient de l'hébreu « adam ». Donc ces filles étaient les descendantes du premier humain, c'est-à-dire d'Adam. Il y a un contraste entre les fils d'Elohîm qui n'étaient pas les descendants d'Adam et les filles des humains ou d'Adam.--> étaient belles, ils en prirent pour femmes parmi toutes celles qu'ils choisirent.
6:3	YHWH dit : Mon Esprit ne contestera pas à perpétuité avec l'être humain<!--C'est le Saint-Esprit qui nous convainc de péché, de justice et de jugement (Jn. 16:8). Lorsqu'il constate que le cœur d'une personne est définitivement endurci au point de refuser la repentance, il renonce à la convaincre de péché et il se retire. La génération antédiluvienne avait définitivement rejeté Elohîm en choisissant de faire le mal (Ge. 6:5). Elle était allée si loin dans l'abomination qu'elle s'accoupla avec des anges déchus (Ge. 6:4), ce qui laisse supposer un culte volontaire aux démons. Lorsque le Saint-Esprit est retiré d'une personne, il est remplacé par l'esprit d'égarement qui enferme le pécheur dans l'erreur et l'entraîne ainsi à sa condamnation éternelle (Mt. 12:31 ; 2 Th. 2:11).-->, car il n'est que chair et ses jours seront de 120 ans.
6:4	Les Nephilim<!--Vient de l'hébreu « naphal » qui signifie « tomber ».--> apparurent sur la Terre en ces jours-là, et aussi après que les fils d'Elohîm furent venus vers les filles des êtres humains et qu'elles eurent enfanté pour eux. Ce sont ces hommes puissants qui, dès les temps anciens furent des hommes du nom.
6:5	YHWH vit que la méchanceté de l'être humain était très grande sur la Terre et que toute la structure<!--Vient de l'hébreu « yetser » qui signifie « forme », « but », « charpente », « imagination », « dessein », (charpente intellectuelle). Yetser vient de « yatsar » qui signifie « former », « façonner », « structurer ».--> des pensées de son cœur n'était que mal tout le jour.
6:6	YHWH se repentit d'avoir fait l'être humain sur la Terre, et il fut affligé en son cœur.
6:7	YHWH dit : J’effacerai l'être humain que j'ai créé des faces du sol, depuis l'être humain jusqu'au bétail, jusqu'aux choses rampantes, et même jusqu'aux créatures volantes des cieux, car je me repens de les avoir faits.
6:8	Mais Noah trouva grâce aux yeux de YHWH.
6:9	Voici la généalogie de Noah. Noah devint un homme juste et intègre en son temps. Noah marchait avec Elohîm.
6:10	Noah engendra trois fils : Shem, Cham et Yepheth.
6:11	La Terre était corrompue en face d'Elohîm, la Terre était remplie de violence.
6:12	Elohîm regarda la Terre, et voici qu’elle était corrompue, car toute chair avait corrompu sa voie sur la Terre.
6:13	Elohîm dit à Noah : La fin de toute chair est venue en face de moi, car la Terre est remplie de violence face à eux et voici, je les détruirai avec la Terre.
6:14	Fais-toi une arche<!--Type du Mashiah, l'arche est l'image du refuge et du salut que l'on peut trouver en Yéhoshoua Mashiah (Jésus-Christ) (Col. 1:12-13, 3:3). Les différents types d'animaux purs et impurs qui sont entrés dans l'arche préfigurent l'Assemblée (Église) bâtie par le Seigneur Yéhoshoua (Mt. 16:18), composée de Juifs convertis (animaux purs) qui furent les premiers à recevoir l'adoption, les alliances, le culte, et les promesses d'Elohîm (Ro. 9:4), et d'hommes et de femmes issus des nations païennes (animaux impurs ; Ac. 10). Noah a pu disposer d'une centaine d'années pour mener à terme le projet de construction de l'Arche. Voir Ge. 5:32 et 7:11. Il avait 500 ans quand ses fils sont nés (Ge. 5 :32) et 600 ans lorsque se produit le Déluge (Ge. 7:6).--> de bois de gopher ; tu feras cette arche en cellules, et tu la couvriras<!--Faire une expiation. Ex. 29:33. Vient de l'hébreu « kaphar » qui signifie aussi: « propitiation », « réconciliation », « pardon », « miséricordieux », « apaiser ».--> de poix<!--Prix d'une vie, rançon, un présent. Es. 43:3.--> à l'intérieur<!--Vient de l'hébreu « bayith » qui signifie : « maison ».--> et à l'extérieur.
6:15	Voici comment tu la feras : la longueur de l'arche sera de 300 coudées, sa largeur de 50 coudées et sa hauteur de 30 coudées.
6:16	Tu feras une fenêtre à l'arche et tu l'achèveras une coudée plus haut. Tu mettras la porte de l'arche sur le côté, et tu feras un étage inférieur, un deuxième et un troisième.
6:17	Et moi, voici, je ferai venir le déluge, les eaux, sur la Terre, pour détruire toute chair dans laquelle il y a souffle de vies sous les cieux. Tout ce qui est sur la Terre expirera.
6:18	J'établirai mon alliance avec toi et tu entreras dans l'arche, toi, tes fils, ta femme et les femmes de tes fils avec toi.
6:19	De tout vivant, de toute chair, tu feras entrer dans l’arche deux de chaque pour qu'ils survivent avec toi : ils seront mâle et femelle.
6:20	Des créatures volantes, selon leur espèce, du bétail selon son espèce, de toute chose rampante du sol selon son espèce, tous par deux entreront vers toi pour qu'ils survivent.
6:21	Et toi, prends pour toi de toute nourriture dont on se nourrit, et tu la recueilleras près de toi, et cela deviendra une nourriture pour toi et pour eux.
6:22	Noah fit selon tout ce qu'Elohîm lui avait ordonné. Ainsi fit-il.

## Chapitre 7

7:1	YHWH dit à Noah : Entre, toi et toute ta maison dans l'arche, car je t'ai vu juste face à moi parmi cette génération.
7:2	Tu prendras de tout le bétail pur, sept par sept, le mâle et sa femelle, mais du bétail qui n'est pas pur, deux, le mâle et sa femelle.
7:3	Et aussi des créatures volantes des cieux, sept par sept, le mâle et sa femelle afin de faire survivre leur semence sur les faces de toute la Terre.
7:4	Car dans sept jours, je ferai pleuvoir sur la Terre pendant 40 jours et 40 nuits. J’effacerai sur les faces du sol toute existence que j’ai faite.
7:5	Et Noah fit selon tout ce que YHWH lui avait ordonné.
7:6	Noah était fils de 600 ans quand arriva le déluge, les eaux sur la Terre.
7:7	Noah entra dans l'arche avec ses fils, sa femme et les femmes de ses fils, face aux eaux du déluge.
7:8	Du bétail pur, du bétail qui n'est pas pur, des créatures volantes, et tout ce qui rampe sur le sol,
7:9	ils entrèrent deux à deux vers Noah dans l'arche, le mâle et la femelle, comme Elohîm l'avait ordonné à Noah.
7:10	Il arriva qu'au septième jour les eaux du déluge arrivèrent sur la Terre.
7:11	En l'an 600 de la vie de Noah, au second mois, le dix-septième jour du mois, en ce jour-là toutes les sources du grand abîme furent rompues, et les écluses des cieux furent ouvertes.
7:12	Il y eut de la pluie sur la Terre pendant 40 jours et 40 nuits.
7:13	En ce même<!--Littéralement « dans l'os de ce jour ».--> jour, Noah, Shem, Cham et Yepheth, fils de Noah, entrèrent dans l'arche, avec la femme de Noah et les trois femmes de ses fils avec eux.
7:14	Eux et tout vivant selon son espèce, tout le bétail selon son espèce, toutes les choses rampantes qui rampent sur la Terre selon leur espèce, toutes les créatures volantes selon leur espèce, tout petit oiseau et tout ce qui a des ailes.
7:15	Ils entrèrent vers Noah dans l’arche, deux par deux, de toute chair ayant en elle le souffle de vies<!--le mot est au pluriel.-->.
7:16	Et les entrants, mâle et femelle de toute chair entraient comme Elohîm le lui avait ordonné. Ensuite YHWH ferma la porte derrière lui.
7:17	Il y eut le déluge pendant 40 jours sur la Terre. Les eaux crûrent et soulevèrent l'arche, et elle s'éleva au-dessus de la terre.
7:18	Et les eaux grossirent et s'accrurent beaucoup sur la Terre, et l'arche allait sur les faces des eaux.
7:19	Les eaux grossirent de plus en plus sur la Terre et recouvrirent toutes les hautes montagnes qui sont sous tous les cieux.
7:20	Les eaux s'élevèrent de 15 coudées au-dessus des montagnes qui furent couvertes.
7:21	Toute chair qui rampait sur la Terre expira : créatures volantes, bétail, vivant, toutes les choses grouillantes qui grouillaient sur la Terre, et tous les êtres humains.
7:22	Tout ce qui avait dans ses narines le souffle d'esprit de vies, tout ce qui était sur le sol sec, mourut.
7:23	Toute existence sur les faces du sol fut effacée, depuis l'être humain jusqu'au bétail, jusqu'aux choses rampantes et jusqu'aux créatures volantes des cieux : ils furent effacés de la Terre. Il ne resta seulement que Noah, et ce qui était avec lui dans l'arche.
7:24	Les eaux prévalurent sur la Terre pendant 150 jours.

## Chapitre 8

8:1	Elohîm se souvint de Noah, de tout vivant et de tout le bétail qui étaient avec lui dans l'arche. Elohîm fit passer un vent sur la Terre, et les eaux se calmèrent.
8:2	Les sources de l'abîme et les écluses des cieux furent fermées et la pluie des cieux fut retenue.
8:3	Les eaux s'éloignèrent de dessus la Terre, s'en allant et s'éloignant. Au bout de 150 jours les eaux diminuèrent.
8:4	Le dix-septième jour du septième mois, l'arche s'arrêta sur les montagnes d'Ararat.
8:5	Les eaux continuèrent de s'en aller et de diminuer jusqu'au dixième mois et, au premier du dixième mois, les sommets des montagnes apparurent.
8:6	Il arriva qu'au bout de 40 jours, Noah ouvrit la fenêtre qu'il avait faite à l'arche.
8:7	Il envoya le corbeau : il sortit, il sortit et retourna, jusqu'à ce que les eaux aient séché sur la Terre.
8:8	Il envoya une colombe d’auprès de lui, pour voir si les eaux avaient diminué sur les faces du sol.
8:9	La colombe, ne trouvant pas de lieu de repos pour la plante de son pied, retourna à lui dans l'arche car les eaux étaient sur les faces de terre. Il étendit la main et la prit pour la faire rentrer dans l’arche.
8:10	Il attendit anxieusement encore sept autres jours et envoya de nouveau la colombe hors de l'arche.
8:11	La colombe vint à lui au temps du soir, et voici, une feuille d'olivier fraîchement arrachée était dans son bec. Noah sut ainsi que les eaux avaient diminué sur la Terre.
8:12	Il attendit encore sept autres jours, puis il envoya la colombe qui ne retourna plus à lui.
8:13	Il arriva qu'en l'an 601, le premier, le un du mois, les eaux avaient séché sur la terre. Noah ôta la couverture de l'arche, regarda, et voici, les faces du sol avaient séché.
8:14	Et au vingt-septième jour du second mois la terre était devenue sèche.
8:15	Elohîm parla à Noah, en disant :
8:16	Sors de l'arche, toi et ta femme, tes fils et les femmes de tes fils avec toi.
8:17	Fais sortir avec toi tout vivant qui est avec toi, de toute chair, tant les créatures volantes que le bétail, et toutes les choses rampantes qui rampent sur la terre. Qu'ils grouillent sur la terre, qu'ils portent du fruit et se multiplient sur la terre.
8:18	Noah sortit avec ses fils, sa femme et les femmes de ses fils.
8:19	Tout vivant, toutes les choses rampantes, toutes les créatures volantes, tout ce qui rampe sur la terre, selon leurs espèces, sortirent de l'arche.
8:20	Noah bâtit un autel à YHWH, il prit de toutes les bêtes pures, et de toute créature volante pure, et il fit monter des holocaustes sur l'autel.
8:21	YHWH respira un parfum tranquillisant et YHWH dit en son cœur : Je n’ajouterai pas à maudire encore le sol à cause de l'être humain, car la structure du cœur de l'être humain est mauvaise dès sa jeunesse, et je ne frapperai plus de nouveau tout vivant, comme je l'ai fait.
8:22	Durant tous les jours de la Terre, semence et moisson, froid et chaleur, été et hiver, jour et nuit ne cesseront pas.

## Chapitre 9

9:1	Elohîm bénit Noah et ses fils et leur dit : Portez du fruit, multipliez-vous et remplissez la Terre.
9:2	La crainte et la terreur de vous seront sur tout vivant de la terre, sur toutes les créatures volantes des cieux, sur tout ce qui rampe sur le sol, et sur tous les poissons de la mer : ils sont livrés entre vos mains.
9:3	Toute chose rampante qui est vivante deviendra pour vous de la nourriture comme l'herbe verte. Je vous donne tout,
9:4	mais vous ne mangerez pas de chair avec son âme, son sang.
9:5	En effet, je redemanderai votre sang pour vos âmes, je le redemanderai de la main de tout vivant. Je redemanderai l'âme de l'être humain de la main de l'être humain, de la main de l'homme qui est son frère.
9:6	Quiconque versera le sang de l'être humain, par l'être humain son sang sera versé, car Elohîm a fait l'être humain à son image<!--Ge. 1:26.-->.
9:7	Quant à vous, portez du fruit et multipliez-vous, grouillez et multipliez-vous sur la Terre.
9:8	Elohîm parla à Noah et à ses fils avec lui, en disant :
9:9	Et moi, voici que j'établis mon alliance avec vous et avec votre postérité après vous,
9:10	avec toute âme vivante qui est avec vous, tant les créatures volantes que le bétail, et tout vivant de la terre qui sont avec vous, avec tous ceux qui sont sortis de l'arche jusqu'à tous les vivants de la terre.
9:11	J'établis mon alliance avec vous : aucune chair ne sera plus retranchée par les eaux du déluge, il n'y aura plus de déluge pour détruire la Terre.
9:12	Elohîm dit : Voici le signe de l'alliance que je mets entre moi, entre vous et entre toutes les âmes vivantes qui sont avec vous, pour les générations à perpétuité :
9:13	j'ai mis mon arc dans les nuages et il devient le signe de l'alliance entre moi et entre la Terre.
9:14	Il arrivera que quand j'aurai rassemblé des nuages au-dessus de la terre, l'arc apparaîtra parmi les nuages,
9:15	et je me souviendrai de mon alliance entre moi, entre vous et entre toutes les âmes vivantes de toute chair, et les eaux ne deviendront plus un déluge pour détruire toute chair.
9:16	L'arc apparaîtra parmi les nuages et je le regarderai pour me souvenir de l'alliance perpétuelle entre Elohîm et entre toute âme vivante parmi toute chair qui est sur la Terre.
9:17	Elohîm dit à Noah : C'est là le signe de l'alliance que j'ai levé entre moi et entre toute chair qui est sur la Terre.
9:18	Les fils de Noah qui sortirent de l'arche étaient Shem, Cham, et Yepheth. Cham fut le père de Kena'ân<!--Canaan.-->.
9:19	Ces trois-là sont les fils de Noah, et c'est à partir d'eux qu'on se dispersa sur toute la Terre.
9:20	Noah, homme du sol, commença à planter de la vigne.
9:21	Il but du vin, s’enivra et se découvrit au milieu de sa tente.
9:22	Cham, père de Kena'ân, vit la nudité de son père<!--Lé. 18:6-19, 20:11-21.--> et il le raconta à ses deux frères dehors.
9:23	Shem et Yepheth prirent un manteau qu'ils mirent sur leurs deux épaules, et marchant en arrière, ils couvrirent la nudité de leur père. Leurs faces étant en arrière, ils ne virent pas la nudité de leur père.
9:24	Noah se réveilla de son vin et sut ce que lui avait fait son fils cadet.
9:25	Il dit : Maudit soit Kena'ân<!--Une idée erronée selon laquelle les noirs auraient été maudits par Elohîm au travers de la malédiction de Kena'ân s'est répandue pendant des siècles. Ainsi, certains hommes ont pris pour prétexte cette malédiction pour légitimer l'asservissement des populations africaines, de même que d'autres populations indigènes, comme les Amérindiens. Il faut préciser que les descendants de Cham furent Koush (Éthiopie), Mitsraïm (Égypte), Pouth (Libye) et Kena'ân (la terre qu'Elohîm a donnée aux descendants de Shem, selon Ge. 15). Cham est le fils cadet de Noah. Mais c'est à Kena'ân, le fils de Cham, donc petit-fils de Noah, que s'adresse la malédiction. Selon la Bible, les peuples africains sont des descendants de Cham, mais par son fils Koush et non par Kena'ân. La prétendue malédiction des noirs n'a donc aucun fondement.--> ! Qu’il devienne l'esclave des esclaves de ses frères.
9:26	Il dit : Béni soit YHWH, l'Elohîm de Shem, et que Kena'ân devienne leur esclave !
9:27	Qu'Elohîm mette Yepheth au large, qu'il habite dans les tentes de Shem et que Kena'ân devienne leur esclave !
9:28	Noah vécut après le déluge 350 ans.
9:29	Tous les jours de Noah furent de 950 ans, puis il mourut.

## Chapitre 10

10:1	Voici la généalogie des fils de Noah : Shem, Cham et Yepheth. Il leur naquit des fils après le déluge.
10:2	Les fils de Yepheth furent : Gomer, Magog, Madaï, Yavan, Toubal, Méshek, et Tiras.
10:3	Et les fils de Gomer : Ashkenaz, Riphat, et Togarmah.
10:4	Les fils de Yavan : Éliyshah, Tarsis, Kittim, et Dodanim.
10:5	De ceux-là furent divisées les îles des nations selon leurs terres, chaque homme selon sa langue, selon leurs familles, entre leurs nations.
10:6	Les fils de Cham furent : Koush, Mitsraïm, Pouth, et Kena'ân.
10:7	Les fils de Koush : Saba, Haviylah, Sabta, Ra`mah, et Sabteca. Les fils de Ra`mah : Séba et Dedan.
10:8	Koush engendra aussi Nimrod<!--Nimrod ou Nemrod, dont le nom signifie « rebelle », fut le premier roi de l'histoire biblique. Fils de Koush (Éthiopie), lui-même premier-né de Cham, fils de Noah (Ge. 10:8-10), il fut à la tête du premier Empire après le déluge. Il se distingua en qualité de puissant chasseur « devant YHWH » ou « contre YHWH ». Le contexte du chapitre 10 nous démontre qu'il provoquait Elohîm. Fondateur de Ninive, il est surtout connu pour avoir été à l'origine du projet de la tour de Babel.-->. C'est lui qui commença à devenir puissant sur la Terre.
10:9	Il devint un puissant chasseur devant YHWH, c'est pourquoi l'on a dit : Comme Nimrod, le puissant chasseur devant YHWH.
10:10	Et le commencement de son royaume fut Babel<!--Le nom Babel signifie la porte de El. Babylone : c'est la confusion par le mélange.-->, Érec, Accad, et Kalneh, en terre de Shinear.
10:11	De cette terre-là sortit Assour, et il bâtit Ninive, et la ville de Rehoboth, et Kélach,
10:12	et Résen, entre Ninive et Kélach, qui est une grande ville.
10:13	Mitsraïm engendra les Loudim, les Anamim, les Lehabim, les Naphtouhim,
10:14	les Patrousim, les Kaslouhim, d'où sont sortis les Philistins, et les Kaphtorim.
10:15	Kena'ân engendra Sidon, son premier-né, et Heth,
10:16	et les Yebousiens, les Amoréens, les Guirgasiens,
10:17	les Héviens, les Arkiens, les Siniens,
10:18	les Arvadiens, les Tsemariens, les Hamathiens. Ensuite, les familles des Kena'ânéens<!--Cananéens.--> se sont dispersées.
10:19	Les limites des Kena'ânéens furent depuis Sidon, quand on vient vers Guérar, jusqu'à Gaza, en allant vers Sodome et Gomorrhe, Admah et Tseboïm, jusqu'à Lésha.
10:20	Ce sont là les fils de Cham selon leurs familles et leurs langues, selon leurs terres, et selon leurs nations.
10:21	Il naquit aussi des fils à Shem, lui, le père de tous les fils d'Héber, le frère de Yepheth, le grand.
10:22	Les fils de Shem furent : Éylam, Assour, Arpacshad, Loud et Aram.
10:23	Les fils d'Aram : Outs, Houl, Guéter et Mash.
10:24	Arpacshad engendra Shélach et Shélach engendra Héber.
10:25	Il naquit à Héber deux fils. Le nom de l'un était Péleg, parce que de son temps la Terre fut partagée, et le nom de son frère était Yoqtan. 
10:26	Yoqtan engendra Almodad, Shéleph, Hatsarmaveth, Yerach,
10:27	Hadoram, Ouzal, Diqlah,
10:28	Obal, Abimaël, Séba,
10:29	Ophir, Haviylah, et Yobab. Tous ceux-là sont les fils de Yoqtan.
10:30	Leur demeure était depuis Mésha, du côté de Sephar, jusqu'à la montagne de l'orient.
10:31	Ce sont là les fils de Shem, selon leurs familles, selon leurs langues, selon leurs terres, et selon leurs nations.
10:32	Telles sont les familles des fils de Noah, selon leurs généalogies, selon leurs nations. Et de ceux-là ont été divisées les nations sur la Terre après le déluge.

## Chapitre 11

11:1	Toute la Terre était d'un seul langage et d'une seule parole.
11:2	Et il arriva, comme ils se déplaçaient à l'orient, qu'ils trouvèrent une vallée en terre de Shinear et y habitèrent.
11:3	Ils se dirent, l’homme à son compagnon : Venez ! Faisons des briques et cuisons-les au feu ! La brique devint pour eux de la pierre, et le bitume devint pour eux de l'argile.
11:4	Ils dirent : Venez ! Bâtissons-nous une ville et une tour dont la tête soit jusqu'aux cieux. Faisons-nous un nom, de peur que nous ne soyons dispersés sur les faces de toute la Terre.
11:5	YHWH descendit pour voir la ville et la tour que les fils des humains bâtissaient.
11:6	YHWH dit : Voici, ce peuple est un, et ils ont tous un seul langage ! Cela, ils commencent à le faire. Maintenant, rien de ce qu'ils ont projeté d'accomplir ne leur sera inaccessible !
11:7	Allons ! Descendons et là confondons leur langage, afin que l’homme n’entende plus le langage de son compagnon !
11:8	YHWH les dispersa de là sur les faces de toute la Terre et ils cessèrent de bâtir la ville.
11:9	C'est pourquoi on l'appela du nom de Babel<!--Porte de El. Babylone.-->, car c'est là que YHWH confondit le langage de toute la Terre, et c'est de là que YHWH les dispersa sur les faces de toute la Terre.
11:10	Voici la généalogie de Shem : Shem, fils de 100 ans, engendra Arpacshad, 2 ans après le déluge.
11:11	Shem, après qu'il eut engendré Arpacshad, vécut 500 ans et engendra des fils et des filles.
11:12	Arpacshad vécut 35 ans et engendra Shélach.
11:13	Arpacshad, après qu'il eut engendré Shélach, vécut 403 ans et engendra des fils et des filles.
11:14	Shélach, ayant vécu 30 ans, engendra Héber.
11:15	Shélach, après qu'il eut engendré Héber, vécut 403 ans et engendra des fils et des filles.
11:16	Héber, ayant vécu 34 ans, engendra Péleg.
11:17	Héber, après qu'il eut engendré Péleg, vécut 430 ans et engendra des fils et des filles.
11:18	Péleg, ayant vécu 30 ans, engendra Réou.
11:19	Péleg, après qu'il eut engendré Réou, vécut 209 ans et engendra des fils et des filles.
11:20	Réou, ayant vécu 32 ans, engendra Seroug.
11:21	Réou, après qu'il eut engendré Seroug, vécut 207 ans et engendra des fils et des filles.
11:22	Seroug, ayant vécu 30 ans, engendra Nachor.
11:23	Seroug, après qu'il eut engendré Nachor, vécut 200 ans et engendra des fils et des filles.
11:24	Nachor, ayant vécu 29 ans, engendra Térach.
11:25	Nachor, après qu'il eut engendré Térach, vécut 119 ans et engendra des fils et des filles.
11:26	Térach, ayant vécu 70 ans, engendra Abram, Nachor et Haran.
11:27	Voici la généalogie de Térach : Térach engendra Abram, Nachor et Haran. Haran engendra Lot.
11:28	Haran mourut en présence de Térach, son père, en terre de sa naissance, à Our en Chaldée.
11:29	Abram et Nachor prirent pour eux des femmes. Le nom de la femme d'Abram était Saraï et le nom de la femme de Nachor était Milkah, fille d'Haran, père de Milkah et père de Yiskah.
11:30	Saraï était devenue stérile, elle n'avait pas de progéniture.
11:31	Térach prit Abram, son fils, Lot, fils d'Haran, fils de son fils, et Saraï, sa belle-fille, femme d'Abram, son fils. Ils sortirent ensemble d'Our en Chaldée pour aller vers la terre de Kena'ân. Ils vinrent jusqu'à Charan et ils y habitèrent.
11:32	Les jours de Térach furent de 205 ans, et il mourut à Charan.

## Chapitre 12

12:1	YHWH avait dit à Abram : Va pour toi<!--L’expression « lekh-lekha » est composé de deux mots. Le premier est : « Lekh » du verbe : « qal », impératif, actif, deuxième personne du singulier, masculin, de la racine « halakh » qui signifie : « aller » ou « va ! » Le deuxième est : « Lekha » qui est composé de deux éléments : « le », une préposition qui signifie : « à », « pour », « vers » et « kha », un suffixe, deuxième personne du singulier, masculin qui signifie : « toi ».-->, hors de ta terre, de ta patrie, et de la maison de ton père, vers la terre que je te montrerai<!--Ac. 7:3 ; Hé. 11:8.-->.
12:2	Je te ferai devenir une grande nation, et je te bénirai, je rendrai ton nom grand, et tu deviendras une bénédiction.
12:3	Je bénirai ceux qui te béniront, et je maudirai ceux qui te maudiront, et toutes les familles du sol seront bénies en toi<!--Ac. 3:25 ; Ga. 3:8.-->.
12:4	Abram s’en alla, comme lui avait dit YHWH, et Lot s’en alla avec lui. Abram était fils de 75 ans quand il sortit de Charan.
12:5	Abram prit aussi Saraï, sa femme, et Lot, fils de son frère, avec tous les biens qu'ils avaient réunis, et toutes les âmes qu'ils avaient acquises à Charan. Ils sortirent pour aller vers la terre de Kena'ân et ils arrivèrent en terre de Kena'ân<!--Ac. 7:4.-->.
12:6	Abram passa par la terre jusqu'au lieu de Shekem<!--Généralement traduit par Sichem.-->, jusqu'aux grands arbres de Moré. Les Kena'ânéens étaient alors sur la terre.
12:7	YHWH se fit voir à Abram et lui dit : Je donnerai cette terre à ta postérité. Il bâtit là un autel pour YHWH qui s’est fait voir à lui.
12:8	Il se transporta de là vers la montagne à l'orient de Béth-El et y dressa ses tentes, ayant Béth-El à l'occident, et Aï à l'orient. Là, il bâtit un autel à YHWH et invoqua le nom de YHWH.
12:9	Et Abram partit, marchant et allant vers le midi.
12:10	Il y eut une famine sur la terre et Abram descendit en Égypte pour y séjourner, car la famine était grande sur la terre.
12:11	Il arriva que comme il était près d'entrer en Égypte, il dit à Saraï, sa femme : S'il te plaît, je sais que tu es une femme de belle apparence.
12:12	Il arrivera que lorsque les Égyptiens te verront, ils diront : C'est sa femme ! Ils me tueront, et toi, ils te laisseront vivre.
12:13	S’il te plaît, dis que tu es ma sœur, afin que je sois bien traité à cause de toi, et que mon âme vive grâce à toi.
12:14	Il arriva que lorsque Abram fut arrivé en Égypte, les Égyptiens virent que cette femme était très belle.
12:15	Les princes de pharaon la virent et la vantèrent à pharaon et cette femme fut enlevée pour la maison de pharaon.
12:16	À cause d'elle, on traita bien Abram : il eut des brebis, des bœufs, des ânes, des esclaves et des servantes, des ânesses et des chameaux.
12:17	Mais YHWH frappa de grandes plaies pharaon et sa maison, à cause de Saraï, femme d'Abram.
12:18	Pharaon appela Abram et lui dit : Qu'est-ce que tu m'as fait ? Pourquoi ne m'as-tu pas déclaré que c'était ta femme ?
12:19	Pourquoi as-tu dit : C'est ma sœur ? Aussi je l’ai prise pour femme. Maintenant, voici ta femme, prends-la et va-t'en !
12:20	Et pharaon ayant donné ordre à ses hommes, ils le renvoyèrent, lui, sa femme, et tout ce qui était à lui.

## Chapitre 13

13:1	Abram monta d'Égypte, lui, sa femme, tout ce qui est à lui et Lot avec lui, vers le midi.
13:2	Abram était très riche en bétail, en argent et en or.
13:3	Il alla par étapes du midi jusqu’à Béth-El, jusqu'au lieu où il avait dressé ses tentes au commencement, entre Béth-El et Aï,
13:4	au lieu où était l'autel qu'il avait bâti au commencement. Là, Abram invoqua le nom de YHWH.
13:5	Lot aussi, qui marchait avec Abram, avait des brebis, des bœufs et des tentes.
13:6	Et la terre ne les portait plus pour habiter ensemble. Car leurs biens étaient si grands qu'ils ne pouvaient habiter ensemble.
13:7	Il y eut dispute entre les bergers du bétail d'Abram et les bergers du bétail de Lot. Les Kena'ânéens et les Phéréziens habitaient alors la terre.
13:8	Abram dit à Lot : S’il te plaît, qu'il n'y ait pas de querelle entre moi et toi, ni entre mes bergers et tes bergers, car nous sommes des hommes, des frères.
13:9	Toute la terre n'est-elle pas devant toi ? S’il te plaît, sépare-toi de moi. Si tu vas à gauche, j'irai à droite, et si tu vas à droite, j'irai à gauche.
13:10	Lot leva les yeux et vit que toute la plaine du Yarden était entièrement arrosée. Avant que YHWH ait détruit Sodome et Gomorrhe, c'était, jusqu'à Tsoar, comme le jardin de YHWH, comme la terre d'Égypte.
13:11	Lot choisit pour lui toute la plaine du Yarden, et Lot se déplaça vers l’est. Et l'homme se sépara de son frère.
13:12	Abram habita sur la terre de Kena'ân, et Lot habita dans les villes de la plaine, et dressa ses tentes jusqu'à Sodome.
13:13	Or les hommes de Sodome étaient mauvais et extrêmement pécheurs contre YHWH.
13:14	YHWH dit à Abram, après que Lot se fut séparé de lui : Lève les yeux, s’il te plaît, et regarde, du lieu où tu es, vers le nord et le sud, vers l’est et l’ouest.
13:15	Car je te donnerai, à toi et à ta postérité pour toujours, toute la terre que tu vois.
13:16	Je rendrai ta postérité comme la poussière de la terre, de sorte que, si un homme pouvait compter la poussière de la terre, ta postérité<!--Ro. 4:18 ; Hé. 11:12.--> aussi serait comptée.
13:17	Lève-toi, traverse la terre dans sa longueur et dans sa largeur, car je te la donnerai.
13:18	Abram déplaça ses tentes et vint habiter parmi les grands arbres de Mamré, qui sont près d'Hébron et là, il bâtit un autel à YHWH.

## Chapitre 14

14:1	Il arriva aux jours d'Amraphel, roi de Shinear, d'Aryok, roi d'Ellasar, de Kedorlaomer, roi d'Éylam, et de Tideal, roi de Goyim<!--Mot hébreu signifiant « peuple, nation (non-hébreu) ».-->,
14:2	qu'ils firent la guerre à Béra, roi de Sodome, à Birsha, roi de Gomorrhe, à Shineab, roi d'Admah, à Shémeéber, roi de Tseboïm, et au roi de Béla, qui est Tsoar.
14:3	Tous ceux-ci se joignirent dans la vallée de Siddim, qui est la Mer Salée.
14:4	Ils avaient été asservis 12 années à Kedorlaomer, et la treizième année, ils s'étaient révoltés.
14:5	À la quatorzième année, Kedorlaomer et les rois qui étaient avec lui vinrent et ils battirent les géants<!--Ou « Réphaïm ». Anciens géants de la terre de Canaan. Ils étaient descendants de Rapha.--> à Ashteroth-Karnaïm, les Zouzim à Ham, et les Émim à Shaveh-Qiryathayim,
14:6	et les Horiens dans leur montagne de Séir, jusqu'au chêne de Paran, qui est près du désert.
14:7	Ils retournèrent et vinrent à En-Mishpath, qui est Qadesh, et ils frappèrent les Amalécites sur toute la terre et aussi les Amoréens qui habitaient dans Hatsatson-Thamar.
14:8	Le roi de Sodome, le roi de Gomorrhe, le roi d'Admah, le roi de Tseboïm, et le roi de Béla, qui est Tsoar, sortirent et se rangèrent en bataille contre eux dans la vallée de Siddim,
14:9	contre Kedorlaomer, roi d'Éylam, Tideal, roi de Goyim, Amraphel, roi de Shinear, et Aryok, roi d'Ellasar : 4 rois contre 5.
14:10	La vallée de Siddim avait des puits, des puits de bitume. Les rois de Sodome et de Gomorrhe s'enfuirent et y tombèrent, et le reste s'enfuit dans la montagne.
14:11	Ils prirent tous les biens de Sodome et de Gomorrhe et toute leur nourriture, puis ils s’en allèrent.
14:12	Ils prirent aussi Lot, fils du frère d'Abram, qui habitait dans Sodome et tous ses biens, puis ils s'en allèrent.
14:13	Un fugitif vint l'annoncer à Abram l'Hébreu qui demeurait parmi les grands arbres de Mamré, l'Amoréen, frère d'Eshcol et frère d'Aner, lesquels étaient maîtres à l'alliance d'Abram.
14:14	Abram, ayant appris que son frère avait été emmené captif, arma 318 hommes bien entraînés, nés dans sa maison, et les poursuivit jusqu'à Dan.
14:15	Il se partagea avec ses serviteurs pendant la nuit, les frappa et les poursuivit jusqu'à Choba, qui est au nord de Damas.
14:16	Il ramena tous les biens. Il ramena aussi Lot, son frère, ses biens, les femmes et le peuple.
14:17	Le roi de Sodome sortit à la rencontre d'Abram qui revenait vainqueur de Kedorlaomer, et des rois qui étaient avec lui, dans la vallée de la plaine, qui est la vallée royale.
14:18	Malkiy-Tsédeq<!--Malkiy-Tsédeq est un type du Mashiah (Ps. 110:4 ; Hé. 5:5-6, 6:20, 7:1-2). Ce personnage nous montre l'aspect du Mashiah en tant que Roi de Shalem, ce qui signifie « roi de paix », et Grand-Prêtre possédant une prêtrise non transmissible (Hé. 7:24).-->, roi de Shalem<!--« Paix, paisible ». Voir Ge. 33:18 ; Ps. 76:3. Voir Jg. 6:24 ; Es. 9:5.-->, fit sortir le pain et le vin, lui, le prêtre de El Élyon<!--El Très-Haut.-->.
14:19	Il le bénit et dit : Béni soit Abram par El Élyon, Possesseur des cieux et de la Terre.
14:20	Béni soit El Élyon qui a livré tes ennemis entre tes mains ! Il lui donna la dîme<!--Voir commentaire sur la dîme en No. 18:21 et Mal. 3:10.--> de tout.
14:21	Le roi de Sodome dit à Abram : Donne-moi les âmes, et prends pour toi les biens.
14:22	Abram dit au roi de Sodome : Je lève ma main vers YHWH, El Élyon, Possesseur des cieux et de la Terre :
14:23	Je ne prendrai rien de tout ce qui est à toi, pas même un fil, ni un cordon de sandale, afin que tu ne dises pas : J'ai enrichi Abram.
14:24	Rien pour moi, seulement ce qu'ont mangé mes serviteurs et la part des hommes qui sont venus avec moi, Aner, Eshcol, et Mamré, eux, ils prendront leur part.

## Chapitre 15

15:1	Après ces choses, la parole de YHWH apparut à Abram dans une vision en disant : Abram, n'aie pas peur, moi, je suis ton bouclier et ta très-grande récompense.
15:2	Abram dit : Adonaï YHWH, que me donneras-tu ? Je m'en vais sans enfants et le fils qui aura l'acquisition de ma maison c'est Éliy`ezer de Damas.
15:3	Abram dit : Voici, tu ne m'as pas donné de postérité. Voilà, le fils de ma maison sera mon héritier.
15:4	Et voici, la parole de YHWH lui est apparue en disant : Celui-ci ne sera pas ton héritier, mais celui qui sortira de tes entrailles sera ton héritier.
15:5	Il le fit sortir dehors et lui dit : S’il te plaît, regarde vers les cieux et compte les étoiles, si tu peux les compter ! Il lui dit : Ainsi deviendra ta postérité.
15:6	Il crut en YHWH, qui le lui compta comme justice<!--Ja. 2:23 ; Ga. 3:6 ; Ro. 4:3.-->.
15:7	Et il lui dit : Moi, YHWH, je t'ai fait sortir d'Our en Chaldée, afin de te donner cette terre pour en prendre possession.
15:8	Il dit : Adonaï YHWH, comment saurais-je que j'en prendrai possession ?
15:9	Il lui dit : Prends une génisse de 3 ans, une chèvre de 3 ans, un bélier de 3 ans, une tourterelle, et un jeune pigeon.
15:10	Il prit tous ces animaux, les coupa par le milieu et mit chaque morceau l'un vis-à-vis de l'autre<!--Littéralement : L'homme rencontrant son compagnon.-->, mais il ne partagea pas les oiseaux.
15:11	Et les oiseaux de proie descendirent sur les cadavres, mais Abram les chassa.
15:12	Et il arriva, comme le soleil se couchait, qu'un profond sommeil tomba sur Abram, et voici, une terreur d'une grande obscurité tomba sur lui.
15:13	Il dit à Abram : Tu le sauras, tu le sauras : oui, ta postérité sera étrangère sur une terre qui n’est pas la sienne. Elle y sera asservie et on l'opprimera pendant 400 ans<!--Ac. 7:6 ; Ga. 3:17.-->.
15:14	Mais je jugerai aussi la nation à laquelle elle sera asservie, et après cela elle sortira avec de grands biens<!--Ex. 3:22.-->.
15:15	Et toi tu iras vers tes pères en paix, et tu seras enterré après une heureuse vieillesse.
15:16	La quatrième génération reviendra ici, car l'iniquité des Amoréens n'est pas complète jusqu’ici.
15:17	Et il arriva que le soleil s’étant couché, il y eut une obscurité profonde, et voici, une fournaise fumante et une torche de feu passèrent entre les animaux qui avaient été partagés.
15:18	En ce jour-là, YHWH traita alliance avec Abram en disant : J'ai donné cette terre à ta postérité, depuis le fleuve d'Égypte jusqu'au grand fleuve, le fleuve d'Euphrate,
15:19	la terre des Qeyniens, des Qenizziens, des Qadmoniens,
15:20	des Héthiens, des Phéréziens, des géants<!--Ou « Rephaïm ».-->,
15:21	des Amoréens, des Kena'ânéens, des Guirgasiens, et des Yebousiens.

## Chapitre 16

16:1	Saraï, femme d'Abram, n’avait pas enfanté pour lui. Elle avait une servante égyptienne du nom d'Agar.
16:2	Et Saraï dit à Abram : Voici, YHWH m’a retenue d’enfanter. Viens, s’il te plaît vers ma servante, peut-être serai-je bâtie<!--Vient de l'hébreu « banah » qui signifie « bâtir », « rebâtir », « établir », « assurer une suite », « construire ». C'est YHWH qui bâtit avec perfection et non les humains. Ge. 2:22 ; Ps. 127:1 ; Mt. 16:18.--> à partir d’elle, s’il te plaît. Et Abram écouta la voix de Saraï.
16:3	Saraï, femme d'Abram, prit Agar, sa servante égyptienne, au bout de 10 ans d’habitation d’Abram en terre de Kena'ân, et elle la donna pour femme à Abram, son homme.
16:4	Il alla vers Agar et elle devint enceinte. Voyant qu’elle était devenue enceinte, sa maîtresse devint insignifiante à ses yeux.
16:5	Et Saraï dit à Abram : Ma violence est contre toi. Moi-même j’ai donné ma servante à ton sein, mais quand elle a vu qu'elle était enceinte, je suis devenue insignifiante à ses yeux. YHWH jugera entre moi et entre toi !
16:6	Abram dit à Saraï : Voici, ta servante est entre tes mains, traite-la comme il sera bon à tes yeux. Saraï l'opprima et elle s'enfuit devant ses faces.
16:7	L'Ange de YHWH la trouva près d'une source d'eau dans le désert, près de la source qui est sur le chemin de Shour.
16:8	Il lui dit : Agar, servante de Saraï, d'où viens-tu ? Et où vas-tu ? Elle dit : En face de Saraï, ma maîtresse, je m'enfuis.
16:9	Et l'Ange de YHWH lui dit : Retourne vers ta maîtresse et humilie-toi sous sa main.
16:10	L'Ange de YHWH lui dit : Multiplier, je multiplierai ta postérité, on ne la comptera pas à cause de sa multitude.
16:11	L'Ange de YHWH lui dit : Voici, tu es enceinte, tu enfanteras un fils et tu l'appelleras du nom de Yishmael<!--Ismaël.-->, car YHWH a entendu ton affliction.
16:12	Il deviendra un âne sauvage humain. Sa main sera contre tous et la main de tous contre lui. Et il habitera en face de tous ses frères.
16:13	Elle appela le Nom de YHWH qui lui avait parlé : « Atta-El-roï<!--Toi, le El mon voyant.--> », car elle dit : N'ai-je pas vu ici même celui qui me voyait ?
16:14	C'est pourquoi on a appelé ce puits : « Le puits de Lachaï-roï<!--Puits pour le vivant, mon voyant.--> ». Voici, il est entre Qadesh et Bared.
16:15	Agar enfanta un fils à Abram, et Abram appela le nom de son fils, qu’Agar avait enfanté, Yishmael<!--Ga. 4:22.-->.
16:16	Abram était fils de 86 ans quand Agar enfanta Yishmael à Abram.

## Chapitre 17

17:1	Il arriva qu'Abram étant fils de 99 ans, YHWH se fit voir à Abram et lui dit : C'est moi El Shaddaï<!--Elohîm se révèle ici à Abraham comme l'Elohîm Tout-Puissant. Or le Mashiah (Christ) s'est présenté à Yohanan (Jean) comme l'Elohîm Tout-Puissant (Ap. 1:8). Plus loin en Ap. 5:6, le Seigneur apparaît au milieu du trône céleste sous la forme d'un Agneau ayant sept cornes qui représentent sa toute puissance. Yéhoshoua est bien l'Elohîm Tout-Puissant qui s'était révélé à Abraham.-->. Marche en face de moi et deviens intègre.
17:2	Je mettrai mon alliance entre moi et entre toi, et je te multiplierai extrêmement, extrêmement.
17:3	Abram tomba sur ses faces, et Elohîm parla avec lui, disant :
17:4	Quant à moi, voici mon alliance avec toi : tu deviendras le père d'une multitude de nations<!--Ro. 4:17.-->.
17:5	On ne t'appellera plus du nom d'Abram<!--Né. 9:7.-->, mais ton nom deviendra Abraham, car je t’ai donné en père d’une multitude de nations.
17:6	Je te ferai extrêmement, extrêmement porter du fruit : je te ferai devenir des nations, et des rois sortiront de toi<!--Mt. 1:6.-->.
17:7	Je lève mon alliance entre moi, entre toi, et entre ta postérité après toi pour leurs générations, une alliance perpétuelle, pour devenir ton Elohîm et celui de ta postérité après toi.
17:8	Je te donnerai, à toi et à ta postérité après toi, la terre où tu demeures comme étranger, toute la terre de Kena'ân, en propriété perpétuelle, et je deviendrai leur Elohîm.
17:9	Elohîm dit à Abraham : Tu garderas mon alliance, toi et ta postérité après toi, selon leurs générations.
17:10	Voici mon alliance entre moi, entre toi et entre ta postérité après toi que vous garderez : Tout mâle parmi vous sera circoncis.
17:11	Vous circoncirez la chair de votre prépuce, cela deviendra le signe de l'alliance entre moi et vous<!--Ac. 7:8 ; Ro. 4:11.-->.
17:12	Tout mâle parmi vous, fils de 8 jours, sera circoncis dans vos générations, celui qui est né dans la maison et celui qui est acheté à prix d’argent, tout fils d’étranger qui n’est pas de ta postérité<!--Lé. 12:3 ; Lu. 2:21.-->.
17:13	Celui qui est né dans ta maison et celui qui est acheté à prix d'argent seront circoncis, ils seront circoncis et mon alliance sera dans votre chair pour être une alliance perpétuelle.
17:14	Et le mâle incirconcis qui n'aura pas été circoncis dans la chair de son prépuce, cette âme sera retranchée du milieu de son peuple. Il aura rompu mon alliance.
17:15	Elohîm dit à Abraham : Saraï, ta femme, tu ne l'appelleras plus du nom de Saraï, mais Sarah sera son nom.
17:16	Je la bénirai, et même je te donnerai un fils d'elle. Je la bénirai et elle deviendra des nations : des rois de peuples viendront d'elle.
17:17	Abraham tomba sur ses faces, il rit et dit dans son cœur : Un fils de 100 ans engendrera-t-il ? Ou Sarah, une fille de 90 ans, enfantera-t-elle ?
17:18	Abraham dit à Elohîm : Si seulement Yishmael vivait en face de toi ! 
17:19	Et Elohîm dit : Sarah, ta femme, t'enfantera vraiment un fils et tu l'appelleras du nom de Yitzhak<!--Isaac.-->. J'établirai mon alliance avec lui pour être une alliance perpétuelle pour sa postérité après lui.
17:20	Quant à Yishmael, je t’ai entendu : Voici, je le bénirai, je le ferai porter du fruit et je le multiplierai extrêmement, extrêmement. Il engendrera douze princes, et je ferai de lui une grande nation.
17:21	Mais j'établirai mon alliance avec Yitzhak, que Sarah t'enfantera l'année qui vient, en ce temps fixé.
17:22	Ayant achevé de parler avec lui, Elohîm s'éleva au-dessus d'Abraham.
17:23	Abraham prit son fils Yishmael, avec tous ceux qui étaient nés dans sa maison, et tous ceux qu'il avait achetés à prix d'argent, tous les mâles parmi les hommes de la maison d'Abraham, et il circoncit la chair de leur prépuce, ce jour-là même, comme Elohîm le lui avait dit.
17:24	Abraham était fils de 99 ans quand il circoncit la chair de son prépuce,
17:25	et Yishmael, son fils, était fils de 13 ans lorsque fut circoncise la chair de son prépuce.
17:26	Ce jour même furent circoncis Abraham et son fils Yishmael.
17:27	Tous les hommes de sa maison, ceux qui étaient nés dans la maison, et ceux qui avaient été achetés à prix d’argent d’entre les fils de l’étranger, furent circoncis avec lui.

## Chapitre 18

18:1	YHWH se fit voir à lui parmi les grands arbres de Mamré, tandis qu'il était assis à la porte de la tente, pendant la chaleur du jour.
18:2	Il leva les yeux et regarda, et voici, trois hommes se tenaient debout devant lui. En les voyant, il courut de la porte de sa tente à leur rencontre et se prosterna à terre<!--Hé. 13:2.-->.
18:3	Il dit : Seigneur, s’il te plaît, si j'ai trouvé grâce à tes yeux, ne passe pas, s’il te plaît, loin de ton serviteur.
18:4	Qu'on prenne, s’il vous plaît, un peu d’eau pour vous laver les pieds, et reposez-vous sous cet arbre.
18:5	Je prendrai un morceau de pain pour fortifier votre cœur. Après quoi vous passerez outre, car c’est pour cela que vous êtes passés près de votre serviteur. Et ils dirent : Fais ce que tu as dit.
18:6	Abraham s'en alla en hâte dans la tente vers Sarah et lui dit : Vite ! Pétris 3 mesures de fleur de farine et fais des gâteaux !
18:7	Abraham courut au troupeau, prit un jeune bœuf, tendre et bon, et le donna à un serviteur qui se hâta de le préparer.
18:8	Il prit du beurre et du lait, et le jeune bœuf qu'on avait préparé, et le mit devant eux. Il se tint debout auprès d'eux sous l'arbre et ils mangèrent.
18:9	Ils lui dirent : Où est Sarah, ta femme ? Et il dit : La voilà dans la tente.
18:10	Il dit : Je reviendrai, je reviendrai vers toi, comme en ce temps vivant, et voici, Sarah ta femme aura un fils. Et Sarah écoutait à la porte de la tente qui était derrière lui<!--Ro. 9:9.-->.
18:11	Abraham et Sarah étaient vieux, avancés en jours, et ce qui arrive d’ordinaire<!--La voie.--> aux femmes avait cessé chez Sarah<!--Sarah avait cessé d'avoir ses règles. Ro. 4:19 ; Hé. 11:11.-->.
18:12	Sarah rit en elle-même, en se disant : Maintenant que je suis usée, aurai-je encore des désirs ? Mon seigneur aussi est vieux.
18:13	Et YHWH dit à Abraham : Pourquoi Sarah a-t-elle ri en disant : Enfanterai-je vraiment et réellement, moi qui suis vieille ?
18:14	Y a-t-il quelque chose qui soit difficile pour YHWH ? Je reviendrai vers toi au temps fixé, comme en ce temps vivant, et Sarah aura un fils<!--Mt. 19:26 ; Lu. 1:37.-->.
18:15	Et Sarah le nia en disant : Je n'ai pas ri, car elle avait peur. Il dit : Non, car tu as ri.
18:16	Ces hommes se levèrent de là et regardèrent en face de Sodome. Abraham alla avec eux pour les envoyer.
18:17	Et YHWH dit : Cacherai-je à Abraham ce que je fais, moi ?
18:18	Abraham deviendra, il deviendra une nation grande et puissante, et toutes les nations de la Terre seront bénies en lui<!--Ac. 3:25 ; Ga. 3:8.-->.
18:19	Car je l'ai connu, afin qu'il ordonne à ses fils et à sa maison après lui, de garder la voie de YHWH en pratiquant la justice et la droiture, afin que YHWH fasse venir sur Abraham tout ce qu'il lui a déclaré.
18:20	Et YHWH dit : Le cri de détresse contre Sodome et Gomorrhe est grand, et leur péché est extrêmement pesant.
18:21	Je descendrai maintenant et je verrai s'ils ont agi tout à fait selon le cri de détresse qui est venu jusqu'à moi. S'il n'en est pas ainsi, je le saurai.
18:22	Les hommes se détournèrent de là et allèrent vers Sodome. Abraham se tenait encore debout en face de YHWH.
18:23	Abraham s'approcha et dit : Feras-tu périr le juste avec le méchant ?
18:24	Peut-être y a-t-il 50 justes dans la ville, les feras-tu périr aussi ? Ne pardonneras-tu pas à la ville à cause des 50 justes qui sont au milieu d'elle ?
18:25	Loin de toi de faire cette chose-là ! De faire mourir le juste avec le méchant, en sorte que le juste devienne comme le méchant. Loin de toi ! Celui qui juge toute la Terre ne fera-t-il pas justice<!--Ro. 3:5-6.--> ?
18:26	YHWH dit : Si je trouve dans Sodome 50 justes au milieu de la ville, je pardonnerai à cause d’eux à tout le lieu.
18:27	Abraham répondit en disant : Voici, s’il te plaît, je me suis résolu à parler à Adonaï, moi qui ne suis que poussière et cendres.
18:28	Peut-être en manquera-t-il 5 des 50 justes. Détruiras-tu toute la ville pour ces 5 là ? Il dit : Je ne la détruirai pas si j'y trouve 45 justes.
18:29	Il continua encore de lui parler en disant : Peut-être s'y trouvera-t-il 40 ? Et il dit : Je ne lui ferai rien à cause de ces 40.
18:30	Abraham dit : S’il te plaît, qu'Adonaï ne se fâche pas si je parle. Peut-être s'y trouvera-t-il 30 ? Et il dit : Je ne lui ferai rien si j'y trouve 30 justes.
18:31	Abraham dit : Voici, s’il te plaît, je me suis résolu à parler à Adonaï : Peut-être s'y trouvera-t-il 20 ? Et il dit : Je ne la détruirai pas à cause de ces 20.
18:32	Il dit : S'il te plaît, qu'Adonaï ne se fâche pas, je parlerai seulement cette fois : Peut-être s'y trouvera-t-il 10 ? Il dit : Je ne la détruirai pas à cause de ces 10.
18:33	YHWH s'en alla quand il eut achevé de parler avec Abraham, et Abraham retourna en son lieu.

## Chapitre 19

19:1	Or sur le soir, les deux anges arrivèrent à Sodome. Lot était assis à la porte de Sodome. Quand Lot les vit, il se leva pour aller au-devant d'eux, et se prosterna le visage contre terre.
19:2	Il dit : Voici, s’il vous plaît, mes seigneurs, détournez-vous, s’il vous plaît, vers la maison de votre serviteur pour y passer la nuit. Vous vous laverez les pieds, vous vous lèverez de bonne heure et vous irez votre chemin. Ils dirent : Non, mais nous passerons la nuit sur la place.
19:3	Il les pressa beaucoup et ils se détournèrent vers lui et entrèrent dans sa maison. Il leur fit un festin et fit cuire au four des pains sans levain, et ils mangèrent.
19:4	Ils n'étaient pas encore couchés que les hommes de la ville, les hommes de Sodome, environnèrent la maison, depuis les jeunes hommes jusqu'aux vieillards, tout le peuple, de partout.
19:5	Ils appelèrent Lot et ils lui dirent : Où sont les hommes qui sont venus cette nuit chez toi ? Fais-les sortir afin que nous les connaissions.
19:6	Lot sortit vers eux à l'entrée et, ayant fermé la porte derrière lui,
19:7	il leur dit : S’il vous plaît, mes frères, ne faites pas le mal.
19:8	Voici, s’il vous plaît, j'ai deux filles qui n'ont pas connu d'homme. Je les ferai sortir vers vous, s’il vous plaît, et vous leur ferez ce qui est bon à vos yeux. Seulement, ne faites pas de mal à ces hommes, car ils sont venus à l'ombre de ma poutre.
19:9	Ils lui dirent : Va-t-en loin d'ici. Ils dirent aussi : En voilà un qui est venu comme étranger, et il veut nous gouverner, nous gouverner ! Maintenant nous te ferons plus de mal qu'à eux. Ils poussèrent fortement l'homme, Lot, et s'approchèrent pour briser la porte<!--2 Pi. 2:7-8.-->.
19:10	Mais ces hommes étendirent leurs mains, firent rentrer Lot vers eux dans la maison, et fermèrent la porte.
19:11	Et ils frappèrent d'aveuglement les hommes qui étaient à la porte de la maison, depuis le plus petit jusqu'au plus grand, de sorte qu'ils se lassèrent à chercher la porte.
19:12	Les hommes dirent à Lot : Qui est encore avec toi ici ? Gendre, tes fils, tes filles, tout ce qui est à toi dans la ville, fais-les sortir du lieu !
19:13	Car nous allons détruire ce lieu, car leur cri est devenu grand en face de YHWH. YHWH nous a envoyés pour le détruire.
19:14	Lot sortit et parla à ses gendres qui avaient pris ses filles, et dit : Levez-vous, sortez de ce lieu, car YHWH va détruire la ville. Mais aux yeux de ses gendres, il parut plaisanter.
19:15	Quand l'aube se leva, les anges pressèrent Lot en disant : Lève-toi, prends ta femme et tes deux filles qui se trouvent ici, de peur que tu ne périsses dans le châtiment de la ville.
19:16	Et comme il tardait, ces hommes saisirent sa main, la main de sa femme et les mains de ses deux filles, à cause de la miséricorde de YHWH à son égard. Ils le firent sortir et le firent se reposer en dehors de la ville.
19:17	Et il arriva, quand ils les eurent fait sortir dehors, qu'ils dirent : Échappe-toi pour ton âme ! Ne regarde pas derrière toi et ne t’arrête pas dans toute la plaine ! Échappe-toi vers la montagne, de peur que tu ne périsses !
19:18	Lot leur dit : Non, s’il te plaît, Adonaï !
19:19	Voici, s’il te plaît, ton serviteur a trouvé grâce à tes yeux, et tu as fait croître ta bonté à mon égard en faisant vivre mon âme. Mais moi, je ne pourrai pas me sauver vers la montagne sans être rattrapé par le mal et mourir.
19:20	Voici, s’il te plaît, cette ville est assez proche pour que je m'y enfuie, et elle est petite. S'il te plaît, que je m'y sauve ! N'est-elle pas petite ? Et mon âme vivra.
19:21	Il lui dit : Voici, j’ai porté tes faces en cette parole aussi de ne pas renverser la ville dont tu as parlé.
19:22	Hâte-toi de t'y sauver, car je ne pourrai rien faire jusqu'à ce que tu y sois entré. C'est pourquoi on a appelé cette ville du nom de Tsoar.
19:23	Comme le soleil se levait sur la Terre, Lot entra dans Tsoar.
19:24	YHWH fit pleuvoir sur Sodome et sur Gomorrhe du soufre et du feu venant de YHWH, des cieux<!--De. 29:22 ; Lu. 17:29 ; Jud. 1:7.-->.
19:25	Il renversa ces villes-là, toute la plaine, et tous les habitants des villes et les herbes du sol.
19:26	Sa femme regarda derrière elle, et elle devint un pilier de sel<!--Lu. 17:31-33.-->.
19:27	Abraham se leva tôt le matin et vint au lieu où il s'était tenu face à YHWH.
19:28	Et regardant en face de Sodome et de Gomorrhe, et en face de tout le sol de cette plaine-là, il vit monter du sol une fumée comme la fumée d'une fournaise.
19:29	Il arriva, lorsqu'Elohîm détruisit les villes de la plaine, qu'Elohîm se souvînt d'Abraham. Il envoya Lot hors du milieu du renversement par lequel il renversa les villes où Lot habitait.
19:30	Lot monta de Tsoar et habita sur la montagne avec ses deux filles, car il craignait de demeurer dans Tsoar, et il se retira dans une caverne avec ses deux filles.
19:31	L'aînée dit à la plus jeune : Notre père est vieux, et il n'y a pas d'homme sur terre pour venir vers nous, selon la coutume de toutes les terres.
19:32	Allons ! Faisons boire du vin à notre père et couchons avec lui, afin que la semence de notre père vive ! 
19:33	Elles firent boire du vin à leur père cette nuit-là, et l'aînée vint et coucha avec son père, mais il ne s'aperçut ni quand elle se coucha ni quand elle se leva.
19:34	Et il arriva le lendemain que l'aînée dit à la plus jeune : Voici, j'ai couché la nuit dernière avec mon père. Faisons-lui boire du vin cette nuit aussi et va coucher avec lui afin que la semence de notre père vive !
19:35	Cette nuit-là aussi, elles firent boire du vin à leur père, et la plus jeune se leva et coucha avec lui. Mais il ne s'aperçut ni quand elle se coucha ni quand elle se leva.
19:36	Les deux filles de Lot conçurent de leur père.
19:37	L'aînée enfanta un fils qu'elle appela du nom de Moab. C'est le père des Moabites jusqu'à ce jour.
19:38	La plus jeune aussi enfanta un fils qu'elle appela du nom de Ben-Ammi. C'est le père des fils d'Ammon jusqu'à ce jour.

## Chapitre 20

20:1	Abraham s'en alla de là vers la terre du midi. Il demeura entre Qadesh et Shour et habita comme étranger à Guérar.
20:2	Abraham disait de Sarah, sa femme : C'est ma sœur<!--Le. 18:9.-->. Abiymélek, roi de Guérar, envoya enlever Sarah.
20:3	Elohîm vint vers Abiymélek dans un rêve pendant la nuit et lui dit : Voici, tu vas mourir à cause de la femme que tu as enlevée, car elle est mariée à un mari !
20:4	Abiymélek, qui ne s'était pas approché d'elle, dit : Adonaï, feras-tu mourir une nation juste ?
20:5	Ne m'a-t-il pas dit : C'est ma sœur ? Et elle-même aussi n'a-t-elle pas dit : C'est mon frère ? C'est dans l'intégrité de mon cœur et dans l'innocence de mes paumes que j'ai fait cela.
20:6	Et Elohîm lui dit en rêve : Je sais que tu l'as fait dans l'intégrité de ton cœur, aussi ai-je empêché que tu ne pèches contre moi. C'est pourquoi je n'ai pas permis que tu la touches.
20:7	Maintenant rends la femme de cet homme, car il est prophète. Il intercédera pour toi et tu vivras. Mais si tu ne la rends pas, sache que, mourir, tu mourras toi et tout ce qui est à toi.
20:8	Abiymélek se leva tôt le matin, appela tous ses serviteurs et dit toutes ces paroles à leurs oreilles. Ces hommes eurent très peur.
20:9	Abiymélek appela Abraham et lui dit : Que nous as-tu fait ? En quoi ai-je péché contre toi, pour que tu fasses venir sur moi et sur mon royaume un si grand péché ? Tu as fait à mon égard une action qui ne se fait pas !
20:10	Abiymélek dit aussi à Abraham : Qu'as-tu vu pour avoir fait cette chose ?
20:11	Abraham dit : C'est parce que je disais : Il n'y a sûrement aucune crainte d'Elohîm dans cet endroit, et ils me tueront à cause de ma femme.
20:12	D'ailleurs elle est vraiment ma sœur, fille de mon père, mais elle n'est pas fille de ma mère et elle est devenue ma femme.
20:13	Et il est arrivé, lorsque les Elohîm m'ont fait errer<!--Le verbe est au pluriel.--> loin de la maison de mon père, que je lui ai dit : Aie la bonté de dire, dans tous les endroits où nous irons, que je suis ton frère.
20:14	Abiymélek prit des brebis, des bœufs, des serviteurs et des servantes, et les donna à Abraham, et lui rendit Sarah, sa femme.
20:15	Abiymélek lui dit : Voici ma terre en face de toi, demeure où il sera bon à tes yeux.
20:16	Il dit à Sarah : Voici, je donne à ton frère 1 000 pièces d'argent. Ce sera pour toi un voile sur les yeux pour tous ceux qui sont avec toi et cela te justifiera devant tous.
20:17	Abraham intercéda auprès d'Elohîm, et Elohîm guérit Abiymélek, sa femme et ses servantes, et elles enfantèrent.
20:18	Car YHWH avait fermé, fermé toute matrice de la maison d'Abiymélek, à cause de Sarah, femme d'Abraham.

## Chapitre 21

21:1	YHWH visita Sarah comme il avait dit, YHWH accomplit pour Sarah ce qu'il avait déclaré.
21:2	Sarah devint enceinte et enfanta un fils à Abraham dans sa vieillesse, au temps fixé qu'Elohîm lui avait déclaré.
21:3	Abraham appela son fils qui lui était né, que lui avait enfanté Sarah, du nom de Yitzhak.
21:4	Abraham circoncit son fils Yitzhak fils de 8 jours, comme Elohîm le lui avait ordonné.
21:5	Abraham était fils de 100 ans quand lui fut enfanté Yitzhak, son fils.
21:6	Et Sarah dit : Elohîm m’a fait rire ! Tous ceux qui l'apprendront riront de moi.
21:7	Elle dit : Qui aurait dit à Abraham que Sarah allaiterait des fils ? Car je lui ai enfanté un fils dans sa vieillesse.
21:8	L'enfant grandit et fut sevré. Abraham fit un grand festin le jour où Yitzhak fut sevré.
21:9	Sarah vit rire le fils qu'Agar l'Égyptienne avait enfanté à Abraham.
21:10	Et elle dit à Abraham : Chasse cette servante et son fils, car le fils de cette servante n'héritera pas avec mon fils, avec Yitzhak<!--Ga. 4:30.-->.
21:11	Cette parole déplut beaucoup aux yeux d'Abraham à cause de son fils.
21:12	Mais Elohîm dit à Abraham : Que cela ne déplaise pas à tes yeux, à cause du garçon et de ta servante. Écoute la voix de Sarah dans toutes les choses qu'elle te dira, car c'est en Yitzhak que ta postérité sera appelée de ton nom<!--Ro. 9:7.-->.
21:13	Toutefois, je ferai aussi devenir le fils de la servante une nation, parce qu'il est ta postérité.
21:14	Abraham se leva tôt le matin, prit du pain et une outre d'eau, les donna à Agar, lui mit l'enfant sur le dos et la renvoya. Elle s’en alla et erra dans le désert de Beer-Shéba.
21:15	Quand l'eau de l'outre fut épuisée, elle jeta l'enfant sous un arbrisseau,
21:16	et elle alla s'asseoir vis-à-vis, à la distance d’une portée d’arc, car elle dit : Que je ne voie pas mourir mon enfant ! Elle s'assit vis-à-vis de lui, éleva la voix et pleura.
21:17	Elohîm entendit la voix du garçon. L'Ange d'Elohîm appela des cieux Agar et lui dit : Qu'as-tu Agar ? N'aie pas peur, car Elohîm a entendu la voix du garçon là où il est.
21:18	Lève-toi ! Porte le garçon et soutiens-le de la main, car je le ferai devenir une grande nation.
21:19	Elohîm lui ouvrit les yeux et elle vit un puits d'eau. Elle alla remplir d'eau l'outre et donna à boire au garçon.
21:20	Elohîm fut avec le garçon qui devint grand et demeura dans le désert. Il devint un tireur d'arc.
21:21	Il habita dans le désert de Paran et sa mère lui prit une femme de la terre d'Égypte.
21:22	Il arriva en ce temps-là qu'Abiymélek, avec Picol, chef de son armée, parla à Abraham en disant : Elohîm est avec toi dans tout ce que tu fais.
21:23	Maintenant, jure-moi ici par Elohîm que tu ne me tromperas pas, ni ma descendance, ni ma progéniture. Comme j’ai agi avec bonté envers toi, tu agiras de même envers moi et envers la terre où tu séjournes.
21:24	Abraham dit : Je le jurerai.
21:25	Abraham réprimanda Abiymélek au sujet d'un puits d'eau que les serviteurs d'Abiymélek avaient volé.
21:26	Abiymélek dit : Je ne sais pas qui a fait cette chose-là ! Toi-même tu ne m'en avais pas informé et moi-même je n'en avais rien entendu jusqu'à ce jour.
21:27	Abraham prit des brebis et des bœufs et les donna à Abiymélek, et ils firent alliance ensemble.
21:28	Abraham mit à part sept jeunes brebis de son troupeau.
21:29	Abiymélek dit à Abraham : Qu’est-ce que ces sept jeunes brebis que tu as placées à part ?
21:30	Il dit : C'est pour que tu acceptes de ma main ces sept jeunes brebis. Elles deviendront pour moi un témoignage que j'ai creusé ce puits.
21:31	C'est pourquoi on a appelé ce lieu Beer-Shéba, parce qu’ils y jurèrent eux deux.
21:32	Ils traitèrent alliance à Beer-Shéba, puis Abiymélek se leva avec Picol, chef de son armée, et ils retournèrent vers la terre des Philistins.
21:33	Abraham planta des tamaris à Beer-Shéba, et là il invoqua le nom de YHWH, El-Olam<!--El d'éternité. Voir Es. 40:28.-->.
21:34	Abraham séjourna beaucoup de jours comme étranger en terre des Philistins.

## Chapitre 22

22:1	Or il arriva après ces choses qu'Elohîm éprouva Abraham et lui dit : Abraham ! Et il dit : Me voici.
22:2	Il dit : S’il te plaît, prends ton fils, ton unique, celui que tu aimes, Yitzhak. Va pour toi<!--Gn. 12:1.--> en terre de Moriyah et là, fais-le monter en holocauste sur l'une des montagnes que je te dirai.
22:3	Abraham s'étant levé tôt le matin, sella son âne et prit deux de ses serviteurs avec lui et Yitzhak son fils. Ayant fendu le bois pour l'holocauste, il se leva et s'en alla vers le lieu qu'Elohîm lui avait dit.
22:4	Le troisième jour, Abraham levant ses yeux, vit le lieu de loin.
22:5	Abraham dit à ses serviteurs : Pour vous, demeurez-là avec l'âne. Le garçon et moi, nous irons jusque là-bas pour adorer, puis nous reviendrons auprès de vous.
22:6	Abraham prit le bois de l'holocauste et le mit sur Yitzhak, son fils, et il prit le feu dans sa main et un couteau. Ils s'en allèrent tous les deux ensemble.
22:7	Yitzhak parla à Abraham, son père, et dit : Mon père ! Abraham dit : Me voici mon fils. Et il dit : Voici le feu et le bois, mais où est l'agneau pour l'holocauste<!--Yitzhak (Isaac) est un autre type du Mashiah qui s'offre en sacrifice pour l'expiation de nos péchés. La réponse à sa question au v. 7, « Voici le feu et le bois, mais où est l'agneau pour l'holocauste ? », a été apportée bien des siècles plus tard par Yohanan le Baptiste : « Voici l'Agneau d'Elohîm, qui ôte le péché du monde » (Jn. 1:29).--> ?
22:8	Abraham dit : Mon fils, Elohîm verra lui-même l'agneau pour l'holocauste. Et ils marchèrent tous les deux ensemble.
22:9	Étant arrivés au lieu qu'Elohîm lui avait dit, Abraham bâtit là un autel et rangea le bois. Il lia Yitzhak son fils et le mit sur l'autel par-dessus le bois<!--Ja. 2:21.-->.
22:10	Abraham étendit sa main et prit le couteau pour tuer son fils.
22:11	L'Ange de YHWH l'appela des cieux et dit : Abraham, Abraham ! Il dit : Me voici.
22:12	L'Ange lui dit : Ne porte pas ta main sur le garçon et ne lui fais rien, car maintenant je sais que tu crains Elohîm, puisque tu ne m'as pas refusé ton fils, ton unique.
22:13	Abraham leva les yeux et regarda, et voici qu'un bélier était pris par les cornes dans un buisson. Abraham alla prendre le bélier et le fit monter en holocauste à la place de son fils.
22:14	Abraham appela le nom de ce lieu : YHWH-Yireh<!--« YHWH voit ».-->. C'est pourquoi l'on dit aujourd'hui : Sur la montagne de YHWH-Yèraé<!--YHWH verra.-->.
22:15	L'Ange de YHWH appela des cieux Abraham pour la seconde fois,
22:16	et dit : Je le jure par moi-même<!--Hé. 6:13-15.-->, - déclaration de YHWH ! Parce que tu as fait cela, et que tu n'as pas refusé ton fils, ton unique,
22:17	oui, bénir, je te bénirai. Multiplier, je multiplierai ta postérité comme les étoiles des cieux et comme le sable qui est sur le bord de la mer. Ta postérité prendra possession de la porte de ses ennemis.
22:18	Toutes les nations de la Terre seront bénies en ta postérité<!--Ga. 3:16.-->, parce que tu as obéi à ma voix.
22:19	Abraham retourna vers ses serviteurs. Ils se levèrent et s'en allèrent ensemble à Beer-Shéba. Et Abraham demeura à Beer-Shéba.
22:20	Il arriva après ces choses qu'on informa Abraham en disant : Voici, Milkah a aussi enfanté des fils à Nachor, ton frère.
22:21	Outs, son premier-né, et Bouz son frère, Kemouel, père d'Aram,
22:22	Késed, Hazo, Pildash, Yidlaph et Betouel.
22:23	Betouel a engendré Ribqah<!--Rébecca.-->. Milkah enfanta ces huit fils à Nachor, frère d'Abraham.
22:24	Sa concubine, nommée Réoumah, enfanta aussi Thébach, Gaham, Tahash et Ma'akah.

## Chapitre 23

23:1	La vie de Sarah fut de 127 ans : ce sont là les années de la vie de Sarah.
23:2	Sarah mourut à Qiryath-Arba, qui est Hébron, en terre de Kena'ân. Abraham vint se lamenter sur Sarah et la pleurer.
23:3	Abraham se leva sur les faces de sa morte et parla aux fils de Heth en disant :
23:4	Je suis moi-même un étranger, un habitant avec vous. Donnez-moi une propriété sépulcrale avec vous afin que j'enterre ma morte en face de moi.
23:5	Les fils de Heth répondirent à Abraham et lui dirent :
23:6	Mon seigneur, écoute-nous ! Tu es un prince d'Elohîm parmi nous, enterre ta morte dans le meilleur de nos sépulcres. Aucun homme parmi nous ne fermera son sépulcre pour enterrer ta morte.
23:7	Abraham se leva et se prosterna devant le peuple de la terre, devant les Héthiens.
23:8	Il parla avec eux et dit : Si c’est votre désir que j’enterre ma morte de devant moi, écoutez-moi et intercédez en ma faveur auprès d'Éphron, fils de Tsochar,
23:9	et il me donnera sa caverne de Makpelah qui est à l'extrémité de son champ. Il me la donnera contre plein argent pour propriété sépulcrale au milieu de vous.
23:10	Éphron était assis parmi les fils de Heth. Et Éphron, le Héthien, répondit à Abraham, aux oreilles des fils de Heth, devant tous ceux qui entraient par la porte de sa ville et dit :
23:11	Non, mon seigneur, écoute-moi ! Je te donne le champ, je te donne aussi la caverne qui s'y trouve. Je te la donne aux yeux des fils de mon peuple. Enterre ta morte !
23:12	Abraham se prosterna devant le peuple de la terre.
23:13	Il parla à Éphron aux oreilles du peuple de la terre en disant : Ah, si seulement tu m’écoutais ! Je donne l'argent du champ. Reçois-le de moi et j'y enterrerai ma morte.
23:14	Et Éphron répondit à Abraham en disant :
23:15	Mon seigneur, écoute-moi ! Une terre de 400 sicles d'argent, qu'est-ce que cela entre moi et toi ? Enterre ta morte.
23:16	Abraham ayant écouté Éphron, et Abraham pesa pour Éphron l'argent dont il avait parlé aux oreilles des fils de Heth : 400 sicles d'argent ayant cours chez les marchands<!--Ac. 7:16.-->.
23:17	Le champ d'Éphron à Makpelah, en face de Mamré, le champ et la caverne qui s'y trouve et tous les arbres qui sont dans le champ et dans toutes ses limites alentour,
23:18	Abraham les acheta sous les yeux des fils de Heth et de tous ceux qui entraient par la porte de la ville.
23:19	Après cela, Abraham enterra Sarah, sa femme, dans la caverne du champ de Makpelah, en face de Mamré, qui est Hébron, en terre de Kena'ân.
23:20	Le champ, avec la caverne qui s’y trouve resta à Abraham comme propriété sépulcrale, par les fils de Heth.

## Chapitre 24

24:1	Or Abraham devint vieux et très avancé en âge. YHWH avait béni Abraham en toutes choses.
24:2	Abraham dit à son serviteur, l’ancien de sa maison, qui gouvernait tout ce qui était à lui : S’il te plaît, mets ta main sous ma cuisse,
24:3	et je te ferai jurer par YHWH, l'Elohîm des cieux et l'Elohîm de la Terre, que tu ne prendras pas de femme pour mon fils parmi les filles des Kena'ânéens, au milieu desquels j'habite.
24:4	Mais tu iras vers ma terre et vers mes parents, et tu y prendras une femme pour mon fils Yitzhak.
24:5	Le serviteur lui dit : Peut-être la femme ne voudra-t-elle pas venir après moi vers cette terre. Ferais-je retourner, retourner ton fils vers la terre d'où tu es sorti ?
24:6	Abraham lui dit : Garde-toi ! de peur d'y faire retourner mon fils !
24:7	YHWH, l'Elohîm des cieux, qui m'a fait sortir de la maison de mon père et de ma patrie, qui m'a parlé et qui m'a juré en disant : Je donnerai cette terre à ta postérité<!--Ge. 13:14-18.-->, enverra lui-même son ange en face de toi. C'est là que tu prendras une femme pour mon fils.
24:8	Si la femme ne veut pas venir après toi, tu seras dégagé de ce serment. Seulement ne fais pas retourner mon fils là-bas.
24:9	Le serviteur mit la main sous la cuisse d'Abraham, son seigneur, et lui jura d'observer ces choses.
24:10	Le serviteur prit 10 chameaux parmi les chameaux de son seigneur et s'en alla ayant en mains tous les biens de son seigneur. Il partit et s'en alla en Mésopotamie, à la ville de Nachor.
24:11	Il fit reposer les chameaux sur leurs genoux hors de la ville, près d'un puits d'eau, au temps du soir, au temps où sortent celles qui vont puiser de l'eau.
24:12	Il dit : YHWH, l'Elohîm de mon seigneur Abraham, s’il te plaît, fais que cela arrive en face de moi en ce jour et use de bonté envers mon seigneur Abraham.
24:13	Voici, je me tiens près de la source d'eau, et les filles des hommes de la ville sortent pour puiser de l’eau.
24:14	Qu’il arrive que la jeune fille à laquelle je dirai : S’il te plaît, incline ta cruche afin que je boive et qui dira : Bois, et je donnerai aussi à boire à tes chameaux, soit celle que tu as destinée à ton serviteur Yitzhak ! Par là je saurai que tu agis avec bonté envers mon seigneur.
24:15	Et il arriva, avant qu’il eût fini de parler, que voici sortir Ribqah, sa cruche sur l’épaule. Elle était née de Betouel, fils de Milkah, femme de Nachor, frère d'Abraham.
24:16	Et la jeune fille était très belle de figure. Elle était vierge et aucun homme ne l'avait connue. Elle descendit à la source, et comme elle montait après avoir rempli sa cruche,
24:17	le serviteur courut au-devant d'elle et lui dit : Laisse-moi boire, s’il te plaît, un peu d'eau de ta cruche.
24:18	Elle dit : Mon seigneur, bois. Elle s'empressa d'abaisser sa cruche sur sa main, et elle lui donna à boire.
24:19	Quand elle eut fini de lui donner à boire, elle dit : Je puiserai aussi pour tes chameaux jusqu'à ce qu'ils aient achevé de boire.
24:20	Elle s'empressa de vider sa cruche dans l'abreuvoir et courut encore au puits pour puiser. Elle puisa pour tous ses chameaux.
24:21	L'homme la regardait fixement en silence, pour savoir si YHWH faisait réussir son voyage ou non.
24:22	Et il arriva, quand les chameaux eurent fini de boire, que l'homme prit un anneau d'or du poids d'un demi-sicle et deux bracelets pesant 10 sicles d'or pour ses mains.
24:23	Et il lui dit : De qui es-tu fille ? S’il te plaît, fais-le-moi savoir. Y a-t-il dans la maison de ton père de la place pour nous loger ?
24:24	Elle lui dit : Je suis fille de Betouel, fils de Milkah et de Nachor.
24:25	Elle lui dit aussi : Il y a chez nous de la paille et du fourrage en abondance, et de la place pour loger.
24:26	L'homme s'inclina et adora YHWH,
24:27	et dit : Béni soit YHWH, l'Elohîm de mon seigneur Abraham, qui n'a pas cessé d'exercer sa bonté et sa fidélité envers mon seigneur ! Lorsque j'étais en chemin, YHWH m'a conduit dans la maison des frères de mon seigneur.
24:28	La jeune fille courut et rapporta toutes ces choses à la maison de sa mère.
24:29	Ribqah avait un frère du nom de Laban. Laban courut dehors vers l'homme près de la source.
24:30	Il arriva que lorsqu’il vit l’anneau et les bracelets aux mains de sa sœur, et qu’il entendit les paroles de Ribqah sa sœur, disant : Ainsi m'a parlé cet homme, il vint vers l'homme, et voici, il se tenait près des chameaux, vers la source.
24:31	Il lui dit : Entre, béni de YHWH ! Pourquoi te tiens-tu dehors ? J'ai préparé la maison et une place pour tes chameaux.
24:32	L'homme entra dans la maison. On délia les chameaux. On donna de la paille et du fourrage aux chameaux, avec de l'eau pour laver les pieds de l'homme et les pieds de ceux qui étaient avec lui.
24:33	Il mit en face de lui de quoi manger, mais il dit : Je ne mangerai pas jusqu’à ce que j’aie parlé de mon affaire. On lui dit : Parle !
24:34	Il dit : Je suis serviteur d'Abraham.
24:35	YHWH a extrêmement béni mon seigneur et l'a rendu puissant. Il lui a donné des brebis, des bœufs, de l'argent, de l'or, des serviteurs, des servantes, des chameaux et des ânes.
24:36	Sarah, la femme de mon seigneur, a enfanté dans sa vieillesse un fils à mon seigneur et il lui a donné tout ce qu'il possède.
24:37	Mon seigneur m'a fait jurer en disant : Tu ne prendras pas de femme pour mon fils parmi les filles des Kena'ânéens en terre desquels j'habite,
24:38	non, mais tu iras dans la maison de mon père et de ma famille prendre une femme pour mon fils.
24:39	J'ai dit à mon seigneur : Peut-être la femme ne viendra-t-elle pas après moi.
24:40	Il m'a dit : YHWH, en face de qui j'ai marché, enverra son ange avec toi et fera réussir ta voie. Tu prendras pour mon fils une femme de ma famille et de la maison de mon père.
24:41	Tu seras alors dégagé de ma malédiction quand tu seras arrivé dans ma famille. Si on ne te la donne pas, tu seras dégagé de ma malédiction.
24:42	Je suis arrivé aujourd'hui à la source et j'ai dit : YHWH ! Elohîm de mon seigneur Abraham, je te prie, s’il existe en toi de faire réussir la voie sur laquelle je marche,
24:43	me voici debout près de la source d'eau : qu'il arrive que la vierge qui sortira pour puiser et à qui je dirai : S’il te plaît, laisse-moi boire un peu d'eau de ta cruche,
24:44	et qui me dira : « Bois, toi aussi, puis je puiserai aussi pour tes chameaux », qu'elle soit la femme que YHWH a destinée au fils de mon seigneur.
24:45	Avant que j'aie fini de parler en mon cœur, voici, Ribqah est sortie, ayant sa cruche sur son épaule. Elle est descendue à la source et a puisé de l'eau. Je lui ai dit : Donne-moi à boire, s’il te plaît.
24:46	Elle s’est hâtée de descendre sa cruche de dessus elle et m'a dit : Bois, et je donnerai aussi à boire à tes chameaux. J'ai bu, et elle a aussi donné à boire aux chameaux.
24:47	Je l'ai interrogée en disant : De qui es-tu fille ? Elle a dit : Je suis fille de Betouel, fils de Nachor et de Milkah. J'ai mis un anneau à son nez et les bracelets à ses mains.
24:48	Je me suis incliné, j'ai adoré YHWH, et j'ai béni YHWH, l'Elohîm de mon seigneur Abraham, qui m'a conduit sur la voie de vérité pour que je prenne la fille du frère de mon seigneur pour son fils.
24:49	Maintenant, si vous voulez user de bonté et de fidélité envers mon seigneur, déclarez-le-moi. Sinon, déclarez-le-moi aussi et je me tournerai à droite ou à gauche.
24:50	Laban et Betouel répondirent et dirent : Cette affaire vient de YHWH, nous ne pouvons te parler ni en bien ni en mal.
24:51	Voici Ribqah en face de toi. Prends-la et va ! Qu’elle devienne la femme du fils de ton seigneur, comme YHWH l'a dit.
24:52	Et il arriva, lorsque le serviteur d’Abraham entendit leurs paroles, qu’il se prosterna à terre devant YHWH.
24:53	Le serviteur sortit des objets d'argent, des objets d'or et des vêtements qu'il donna à Ribqah. Il donna aussi de riches présents à son frère et à sa mère.
24:54	Ils mangèrent et burent, lui et les hommes qui étaient avec lui, et ils passèrent la nuit là. Le matin, quand ils furent levés, le serviteur dit : Laissez-moi retourner vers mon seigneur.
24:55	Le frère et la mère lui dirent : Que la jeune fille reste avec nous quelques jours encore, une dizaine de jours ! Après quoi, elle s'en ira.
24:56	Il leur dit : Ne me retardez pas puisque YHWH a fait réussir ma voie. Laissez-moi partir afin que je m'en aille vers mon seigneur.
24:57	Ils dirent : Appelons la jeune fille et interrogeons sa bouche.
24:58	Ils appelèrent Ribqah et lui dirent : Iras-tu avec cet homme ? Elle dit : J'irai !
24:59	Ils laissèrent aller Ribqah leur sœur, sa nourrice, le serviteur d'Abraham et ses hommes.
24:60	Ils bénirent Ribqah et lui dirent : Toi, notre sœur, deviens des milliers de myriades, que ta postérité prenne possession de la porte de ses ennemis !
24:61	Ribqah se leva avec ses servantes. Elles montèrent sur les chameaux et s’en allèrent après l'homme. Le serviteur prit Ribqah et s'en alla.
24:62	Or Yitzhak était venu, il était venu du puits de Lachaï-roï et habitait sur la terre du midi.
24:63	Vers le soir, Yitzhak sortit pour méditer dans les champs et, levant les yeux, il regarda, et voici que des chameaux arrivaient.
24:64	Ribqah leva aussi les yeux, vit Yitzhak et descendit de son chameau.
24:65	Elle dit au serviteur : Qui est cet homme qui marche dans les champs à notre rencontre ? Le serviteur dit : C'est mon seigneur. Elle prit son voile et se couvrit.
24:66	Le serviteur relata à Yitzhak toutes les choses qu'il avait faites.
24:67	Yitzhak la fit entrer dans la tente de Sarah, sa mère. Il prit Ribqah pour sa femme<!--Pr. 18:22, 31:10-31.--> et l'aima. Yitzhak fut consolé après sa mère.

## Chapitre 25

25:1	Or Abraham prit une autre femme du nom de Qetourah.
25:2	Elle lui enfanta Zimran, Yoqshan, Medan, Madian, Yishbaq et Shouah.
25:3	Yoqshan engendra Séba et Dedan. Les fils de Dedan furent Ashourim, Letoushim et Leoummim.
25:4	Les fils de Madian furent Eyphah, Épher, Hanowk, Abida, Elda`ah. Ce sont là tous les fils de Qetourah.
25:5	Abraham donna tout ce qui était à lui à Yitzhak.
25:6	Aux fils des concubines d'Abraham, Abraham donna des dons et, pendant qu’il était encore vivant, il les envoya loin de son fils Yitzhak, du côté de l'orient, vers la terre d'orient.
25:7	Voici les jours des années de la vie d'Abraham : Il vécut 175 ans.
25:8	Abraham expira, il mourut dans une bonne vieillesse, vieux et rassasié, et il fut recueilli auprès de son peuple.
25:9	Yitzhak et Yishmael, ses fils, l'enterrèrent dans la caverne de Makpelah, dans le champ d'Éphron, fils de Tsochar, le Héthien, qui est vis-à-vis de Mamré.
25:10	C'est le champ qu'Abraham avait acheté des fils de Heth. Là furent enterrés Abraham et Sarah, sa femme.
25:11	Et il arriva, après la mort d’Abraham, qu'Elohîm bénit Yitzhak, son fils. Yitzhak habitait près du puits de Lachaï-roï.
25:12	Voici la généalogie de Yishmael, fils d'Abraham, qu'Agar l'Égyptienne, servante de Sarah, avait enfanté à Abraham.
25:13	Voici les noms des fils de Yishmael, par leurs noms, selon leurs généalogies. Le premier-né de Yishmael fut Nebayoth, puis Qedar, Adbeel, Mibsam,
25:14	Mishma, Doumah, Massa,
25:15	Hadad, Théma, Yetour, Naphish, et Qedemah.
25:16	Ce sont là les fils de Yishmael et tels sont leurs noms, selon leurs villages et selon leurs campements : douze princes de leurs peuples.
25:17	Voici les années de vies de Yishmael : 137 ans. Il expira et mourut, et il fut recueilli auprès de ses peuples.
25:18	Il habita depuis Haviylah jusqu'à Shour, qui est en face de l'Égypte, en allant vers l'Assyrie. Il s'était établi en face de tous ses frères.
25:19	Voici la généalogie de Yitzhak, fils d'Abraham. Abraham engendra Yitzhak. 
25:20	Yitzhak était fils de 40 ans quand il épousa Ribqah, fille de Betouel, le Syrien, de Paddan-Aram, sœur de Laban, le Syrien.
25:21	Yitzhak intercéda auprès de YHWH au sujet de sa femme parce qu'elle était stérile, et YHWH intercéda pour lui, et Ribqah sa femme devint enceinte.
25:22	Les fils s'écrasaient à l’intérieur d’elle et elle dit : S’il en est ainsi, pourquoi en suis-je là ? Et elle alla consulter YHWH.
25:23	YHWH lui dit : Il y a deux nations dans ton ventre, et deux peuples de tes entrailles se sépareront. Un peuple sera plus fort que l’autre peuple, et le plus grand servira le plus petit<!--Ro. 9:12.-->.
25:24	Ses jours pour accoucher étant accomplis, et voici des jumeaux dans son ventre.
25:25	Le premier qui sortit était entièrement roux comme un manteau de poil et on l'appela du nom d'Ésav<!--Ésaü.-->.
25:26	Après cela sortit son frère et sa main tenait le talon d'Ésav. On l'appela du nom de Yaacov<!--Yaacov (Jacob) : « celui qui prend par le talon » ou « qui supplante ».-->. Yitzhak était fils de 60 ans quand ils naquirent.
25:27	Les garçons devinrent grands. Ésav devint un homme connaissant la chasse, un homme des champs, et Yaacov était un homme parfait, habitant sous les tentes.
25:28	Yitzhak aimait Ésav car la chasse était pour sa bouche. Mais Ribqah aimait Yaacov.
25:29	Comme Yaacov faisait cuire du potage, Ésav arriva des champs et il était épuisé.
25:30	Et Ésav dit à Yaacov : Laisse-moi, s’il te plaît, avaler<!--Dévorer.--> de ce rouge, de ce rouge-là<!--Probablement un plat de lentilles.-->, car je suis épuisé. C'est pourquoi on appela son nom Édom<!--Édom : « rouge, de couleur rousse ».-->.
25:31	Yaacov lui dit : Vends-moi aujourd'hui ton droit d'aînesse.
25:32	Ésav dit : Voici, moi, je vais mourir. Peu importe pour moi ce droit d'aînesse !
25:33	Yaacov dit : Jure-moi aujourd'hui. Il le lui jura, et il vendit son droit d'aînesse à Yaacov<!--Hé. 12:16.-->.
25:34	Et Yaacov donna à Ésav du pain et du potage de lentilles. Il mangea et but, il se leva et s'en alla. Ésav méprisa son droit d'aînesse.

## Chapitre 26

26:1	Il y eut une famine sur la terre, outre la première famine qu’il y avait aux jours d'Abraham, et Yitzhak s'en alla vers Abiymélek, roi des Philistins, à Guérar.
26:2	YHWH se fit voir à lui et lui dit : Ne descends pas en Égypte ! Demeure sur la terre que je te dirai,
26:3	séjourne sur cette terre, je serai avec toi et je te bénirai. Oui, à toi et à ta postérité je donnerai toutes ces terres. J'accomplirai le serment que j'ai juré à ton père Abraham.
26:4	Je multiplierai ta postérité comme les étoiles des cieux, je donnerai toutes ces terres à ta postérité et toutes les nations de la Terre seront bénies en ta postérité,
26:5	parce qu'Abraham a obéi à ma voix, et qu'il a gardé mon dépôt<!--1 Ti. 6:20.-->, mes commandements, mes statuts et ma torah.
26:6	Yitzhak demeura à Guérar.
26:7	Les hommes du lieu l'interrogèrent sur sa femme et il dit : C'est ma sœur. Il craignait, en effet, de dire : C'est ma femme, de peur que les habitants du lieu ne le tuent à cause de Ribqah, car elle est belle de figure.
26:8	Il arriva quand les jours se prolongeaient là pour lui qu'Abiymélek, roi des Philistins, regardant par la fenêtre, et voilà que Yitzhak riait avec Ribqah, sa femme.
26:9	Abiymélek appela Yitzhak et lui dit : Voici, c'est véritablement ta femme ! Comment as-tu dit : C'est ma sœur ? Et Yitzhak lui dit : Parce que je disais : De peur que je ne meure à cause d’elle.
26:10	Abiymélek dit : Que nous as-tu fait ? Encore un peu et quelqu'un du peuple couchait avec ta femme, et tu aurais fait venir la culpabilité sur nous.
26:11	Abiymélek fit une ordonnance à tout le peuple en disant : Celui qui touchera à cet homme ou à sa femme, mourir, il mourra.
26:12	Yitzhak sema dans cette terre et il recueillit cette année-là le centuple, car YHWH le bénit.
26:13	Cet homme devint grand et alla toujours en augmentant, jusqu'à ce qu'il devint extrêmement grand.
26:14	Il avait des troupeaux de brebis et des troupeaux de bœufs, et beaucoup de serviteurs. Les Philistins furent jaloux de lui.
26:15	Tous les puits que les serviteurs de son père avaient creusés, aux jours de son père Abraham, les Philistins les bouchèrent et les remplirent de terre.
26:16	Abiymélek dit à Yitzhak : Va-t'en de chez nous car tu es devenu beaucoup plus puissant que nous.
26:17	Yitzhak s’en alla de là, campa dans la vallée de Guérar et y habita.
26:18	Yitzhak retourna et creusa les puits d'eau qu'on avait creusés aux jours d'Abraham, son père, et que les Philistins avaient bouchés après la mort d'Abraham. Il les appela des mêmes noms dont son père les avait appelés.
26:19	Les serviteurs de Yitzhak creusèrent dans cette vallée et y trouvèrent un puits d'eau vive.
26:20	Les bergers de Guérar contestèrent avec les bergers de Yitzhak, disant : L'eau est à nous. Il appela le puits du nom d'Eseq, parce qu'on s'était disputé avec lui.
26:21	Ils creusèrent un autre puits, pour lequel ils contestèrent aussi, et il l'appela du nom de Sitna.
26:22	Il se transporta de là et creusa un autre puits pour lequel ils ne contestèrent pas. Il l'appela du nom de Rehoboth, en disant : C'est parce que YHWH nous a maintenant mis au large, et nous porterons du fruit sur la terre.
26:23	Et de là, il monta à Beer-Shéba.
26:24	YHWH se fit voir à lui cette nuit-là et lui dit : Je suis l'Elohîm d'Abraham, ton père. N'aie pas peur, car je suis avec toi, je te bénirai et je multiplierai ta postérité à cause d'Abraham, mon serviteur.
26:25	Là, il bâtit un autel et fit appel au Nom de YHWH. Là, il dressa ses tentes et là les serviteurs de Yitzhak creusèrent un puits.
26:26	Abiymélek vint vers lui de Guérar avec Ahouzath, son ami, et Picol, chef de son armée.
26:27	Yitzhak leur dit : Pourquoi venez-vous vers moi puisque vous me haïssez et que vous m'avez renvoyé de chez vous ?
26:28	Ils dirent : Nous avons vu, nous avons vu que YHWH est avec toi, et nous nous sommes dit : S’il te plaît, qu'il y ait un serment entre nous, entre nous et toi. Traitons alliance avec toi :
26:29	que tu ne nous feras aucun mal, de même que nous ne t'avons pas maltraité, que nous t'avons fait seulement du bien, et que nous t'avons laissé partir en paix. Toi qui es maintenant béni de YHWH.
26:30	Il leur fit un festin et ils mangèrent et burent.
26:31	Ils se levèrent tôt le matin et se lièrent l'homme à son frère par un serment. Yitzhak les renvoya et ils s'en allèrent en paix.
26:32	Il arriva, en ce même jour, que les serviteurs de Yitzhak vinrent l’informer au sujet du puits qu'ils avaient creusé et lui dirent : Nous avons trouvé de l'eau.
26:33	Et il l'appela Shib`ah. C'est pourquoi le nom de la ville a été Beer-Shéba jusqu'à ce jour.
26:34	Ésav, fils de 40 ans, prit pour femmes Yehoudith, fille de Beéri, le Héthien, et Basmath, fille d'Eylon, le Héthien.
26:35	Elles devinrent une amertume pour l'esprit de Yitzhak et de Ribqah.

## Chapitre 27

27:1	Il arriva, quand Yitzhak fut devenu vieux, et que ses yeux affaiblis ne voyaient plus, qu’il appela Ésav, son fils aîné, et lui dit : Mon fils ! Celui-ci lui dit : Me voici.
27:2	Yitzhak lui dit : Voici, s’il te plaît, je suis vieux, et je ne connais pas le jour de ma mort.
27:3	Maintenant, s’il te plaît, prends tes armes, ton carquois et ton arc, va dans les champs et chasse-moi du gibier.
27:4	Prépare-moi une nourriture de choix comme j'aime et apporte-la-moi, afin que je mange et que mon âme te bénisse avant que je meure.
27:5	Or Ribqah écoutait pendant que Yitzhak parlait à Ésav, son fils. Ésav s'en alla dans les champs pour chasser et apporter du gibier.
27:6	Et Ribqah parla à Yaacov, son fils, et lui dit : Voici, j'ai entendu parler ton père à Ésav, ton frère, en disant :
27:7	Apporte-moi du gibier et prépare-moi une nourriture de choix afin que je la mange, et je te bénirai, face à YHWH, face à ma mort.
27:8	Maintenant, mon fils, obéis à ma parole et fais ce que je vais t'ordonner.
27:9	Va, s’il te plaît, vers le troupeau et prends-moi là deux bons chevreaux parmi les chèvres, et j'en ferai une nourriture de choix pour ton père comme il aime.
27:10	Tu l’apporteras à ton père afin qu'il la mange et qu'il te bénisse, face à sa mort.
27:11	Yaacov dit à Ribqah, sa mère : Voici, Ésav, mon frère, est un homme velu, et je suis un homme sans poils.
27:12	Peut-être que mon père me palpera-t-il ? Je deviendrai à ses yeux un trompeur et ferai venir sur moi la malédiction et non la bénédiction.
27:13	Sa mère lui dit : Que ta malédiction soit sur moi, mon fils ! Écoute seulement ma voix, et va me les prendre.
27:14	Yaacov alla les prendre et les apporta à sa mère et sa mère prépara une nourriture de choix comme son père aimait.
27:15	Ribqah prit les plus précieux habits d'Ésav, son fils aîné, qu'elle avait dans la maison, et elle les fit mettre à Yaacov, son fils cadet.
27:16	Elle couvrit ses mains et son cou, qui étaient sans poils, des peaux des chevreaux.
27:17	Elle mit entre les mains de son fils Yaacov la nourriture de choix et le pain qu'elle avait préparés.
27:18	Il vint vers son père et dit : Mon père ! Il dit : Me voici ! Qui es-tu, mon fils ?
27:19	Yaacov dit à son père : Je suis Ésav, ton fils aîné. J'ai fait ce que tu m'as dit. Lève-toi s’il te plaît, assieds-toi et mange de mon gibier, afin que ton âme me bénisse.
27:20	Yitzhak dit à son fils : Eh quoi ! Tu en as déjà trouvé mon fils ! Et il dit : YHWH, ton Elohîm, l'a fait venir devant moi.
27:21	Yitzhak dit à Yaacov : Approche-toi s’il te plaît mon fils et que je te tâte. Es-tu mon fils Ésav ou non ?
27:22	Yaacov s'approcha de son père Yitzhak qui le palpa et dit : Cette voix est la voix de Yaacov, mais ces mains sont les mains d'Ésav.
27:23	Il ne le reconnut pas, car ses mains étaient devenues velues comme les mains de son frère Ésav et il le bénit.
27:24	Il dit : C'est toi, mon fils Ésav ? Il dit : C'est moi.
27:25	Yitzhak lui dit : Approche-le de moi pour que je mange du gibier de mon fils, afin que mon âme te bénisse. Yaacov l'apporta et il mangea. Il lui apporta aussi du vin et il but.
27:26	Yitzhak, son père, lui dit : Approche-toi s’il te plaît et embrasse-moi mon fils.
27:27	Il s'approcha et l'embrassa. Il sentit l'odeur de ses habits et le bénit en disant : Voyez ! L'odeur de mon fils est comme l'odeur d'un champ que YHWH a béni.
27:28	Qu'Elohîm te donne de la rosée des cieux et de la graisse de la Terre, du blé et du vin nouveau en abondance !
27:29	Que des peuples te servent, et que des nations se prosternent devant toi ! Sois le maître de tes frères, et que les fils de ta mère se prosternent devant toi ! Maudit soit quiconque te maudira, et béni soit quiconque te bénira.
27:30	Il arriva que, quand Yitzhak eut fini de bénir Yaacov, et comme Yaacov venait de sortir de devant Yitzhak, son père, Ésav, son frère, revint de la chasse.
27:31	Il prépara aussi une nourriture de choix, l'apporta à son père et lui dit : Que mon père se lève et mange du gibier de son fils, afin que ton âme me bénisse !
27:32	Yitzhak, son père, lui dit : Qui es-tu ? Et il dit : Je suis ton fils, ton premier-né, Ésav.
27:33	Yitzhak trembla d’un très grand tremblement et il dit : Alors qui est celui qui a chassé du gibier et me l'a apporté ? J'ai mangé de tout avant que tu ne viennes, et je l'ai béni. Aussi sera-t-il béni !
27:34	Dès qu'Ésav entendit les paroles de son père, il cria d'un cri grand et extrêmement amer et il dit à son père : Bénis-moi aussi, bénis-moi, mon père !
27:35	Il dit : Ton frère est venu avec tromperie, et il a enlevé ta bénédiction.
27:36	Il dit : Est-ce parce qu'on l'a appelé du nom de Yaacov qu’il m’a supplanté ces deux fois ? Il a pris mon droit d'aînesse, et voici maintenant qu’il a pris ma bénédiction. Il dit : Ne m'as-tu pas réservé une bénédiction ?
27:37	Yitzhak répondit à Ésav en disant : Voici, je l'ai établi ton maître, je lui ai donné tous ses frères pour serviteurs et je l'ai soutenu de blé et de vin nouveau. Que ferai-je maintenant pour toi, mon fils ?
27:38	Ésav dit à son père : N'as-tu qu'une bénédiction, mon père ? Bénis-moi aussi, bénis-moi, mon père ! Et Ésav éleva la voix et pleura<!--Hé. 12:17.-->.
27:39	Yitzhak, son père, répondit et dit : Voici, ta demeure sera privée de la graisse de la terre, et de la rosée des cieux, d'en haut.
27:40	Tu vivras par ton épée, et tu seras asservi à ton frère. Mais il arrivera que lorsque tu seras devenu errant, tu briseras son joug de dessus ton cou.
27:41	Ésav haïssait Yaacov à cause de la bénédiction dont son père l'avait béni. Ésav dit en son cœur : Les jours du deuil de mon père approchent, et je tuerai Yaacov, mon frère.
27:42	On rapporta à Ribqah les paroles d'Ésav, son fils aîné. Elle envoya appeler Yaacov son fils cadet et lui dit : Voici que ton frère Ésav veut se consoler à ton égard en te tuant.
27:43	Maintenant, mon fils, obéis à ma voix : lève-toi ! Fuis pour toi chez Laban, mon frère, à Charan.
27:44	Habite avec lui quelques jours, jusqu'à ce que le courroux de ton frère se détourne,
27:45	jusqu'à ce que les narines de ton frère se détournent de toi et qu'il oublie ce que tu lui as fait. Pourquoi serais-je privée de vous deux en un seul jour ?
27:46	Ribqah dit à Yitzhak : J'ai une aversion pour ma vie en face des filles de Heth. Si Yaacov prend une femme comme celles-ci, parmi les filles de Heth, parmi les filles de la terre, pourquoi vivrais-je ?

## Chapitre 28

28:1	Yitzhak ayant appelé Yaacov, le bénit et lui donna cet ordre, disant : Tu ne prendras pas de femme parmi les filles de Kena'ân.
28:2	Lève-toi, va à Paddan-Aram, à la maison de Betouel, père de ta mère, et prends pour toi une femme de là, parmi les filles de Laban, frère de ta mère.
28:3	El Shaddaï te bénira, il te fera porter du fruit et te multipliera, et tu deviendras une assemblée de peuples.
28:4	Il te donnera la bénédiction d'Abraham, à toi et à ta postérité avec toi, afin que tu prennes possession de la terre où tu as été étranger, qu'Elohîm a donnée à Abraham.
28:5	Yitzhak fit partir Yaacov, qui s'en alla à Paddan-Aram, vers Laban, fils de Betouel, le Syrien, frère de Ribqah, mère de Yaacov et d'Ésav.
28:6	Ésav vit que Yitzhak avait béni Yaacov, et qu'il l'avait envoyé à Paddan-Aram afin qu'il prenne une femme de cette terre-là pour lui, et qu'il lui avait donné cet ordre, quand il le bénissait, en disant : Ne prends pas de femme parmi les filles de Kena'ân,
28:7	et que Yaacov avait obéi à son père et à sa mère, et qu'il était parti à Paddan-Aram.
28:8	Ésav voyant que les filles de Kena'ân déplaisaient aux yeux de Yitzhak, son père,
28:9	Ésav s'en alla vers Yishmael et prit pour femme - en plus des femmes qu'il avait - Mahalath, fille de Yishmael, fils d'Abraham, sœur de Nebayoth.
28:10	Yaacov partit de Beer-Shéba et s'en alla à Charan.
28:11	Il arriva dans un lieu où il passa la nuit, parce que le soleil était couché. Il y prit une pierre<!--1 Pi. 2:4. Voir commentaire en Es. 8:13-15.--> et en fit son chevet, et il se coucha dans ce lieu-là.
28:12	Il rêva. Et voici, une échelle était placée sur la terre et son sommet touchait les cieux. Et voici, les anges d'Elohîm montaient et descendaient sur elle<!--Jn. 1:51.-->.
28:13	Voici que YHWH était placé au-dessus d’elle, et il dit : Je suis YHWH, l'Elohîm d'Abraham, ton père, et l'Elohîm de Yitzhak. Je te donnerai à toi et à ta postérité la terre sur laquelle tu es couché.
28:14	Ta postérité deviendra comme la poussière de la terre : tu t’étendras à l’ouest et à l’est, au nord et au sud, et toutes les familles du sol seront bénies en toi et en ta postérité<!--Ga. 3:16.-->.
28:15	Voici, je suis avec toi, je te garderai partout où tu iras et je te ramènerai vers ce sol, car je ne t’abandonnerai pas jusqu’à ce que j’aie fait ce que je t’ai dit.
28:16	Et Yaacov se réveilla de son sommeil et dit : Vraiment, YHWH est en ce lieu-ci, et moi, je ne le savais pas !
28:17	Il eut peur et dit : Que ce lieu est effrayant ! C'est ici la maison d'Elohîm, et c'est ici la porte des cieux !
28:18	Yaacov se leva tôt le matin, prit la pierre dont il avait fait son chevet, il la dressa pour monument, et versa de l'huile sur son sommet.
28:19	Il appela ce lieu du nom de Béth-El - mais Louz était d’abord le nom de cette ville.
28:20	Yaacov fit un vœu, en disant : Si Elohîm est avec moi et me garde dans cette voie où je marche, et qu’il me donne du pain à manger et un vêtement pour me vêtir,
28:21	et que je retourne en paix à la maison de mon père, YHWH deviendra mon Elohîm.
28:22	Cette pierre que j'ai dressée pour monument deviendra la maison d'Elohîm et je te donnerai la dîme, la dîme de tout ce que tu me donneras<!--Voir commentaire sur la dîme en No. 18:21 et Mal. 3:10.-->.

## Chapitre 29

29:1	Yaacov leva les pieds et s'en alla vers la terre des fils de l'orient.
29:2	Il regarda, et voici, un puits dans un champ. Et voici là trois troupeaux de brebis couchés, car c'était à ce puits qu'on abreuvait les troupeaux. Et il y avait une grosse pierre sur l'ouverture du puits.
29:3	Tous les troupeaux se rassemblaient là, on roulait la pierre de dessus l'ouverture du puits, on abreuvait les troupeaux et on remettait la pierre à sa place, sur l'ouverture du puits.
29:4	Yaacov leur dit : Mes frères, d'où êtes-vous ? Ils dirent : Nous sommes de Charan.
29:5	Il leur dit : Connaissez-vous Laban, fils de Nachor ? Ils dirent : Nous le connaissons.
29:6	Il leur dit : Est-il en paix ? Ils lui dirent : Il est en paix, et voici Rachel, sa fille, qui vient avec le troupeau.
29:7	Il dit : Voici, il est encore grand jour, ce n'est pas le temps de rassembler les troupeaux. Donnez à boire aux brebis, puis allez les faire paître.
29:8	Ils dirent : Nous ne le pouvons pas, jusqu'à ce que tous les troupeaux soient rassemblés et qu'on ait ôté la pierre de dessus l'ouverture du puits, afin d'abreuver les troupeaux.
29:9	Comme il parlait encore avec eux, Rachel arriva avec le troupeau de son père, car elle était bergère.
29:10	Et il arriva que quand Yaacov vit Rachel, fille de Laban, frère de sa mère, et le troupeau de Laban, frère de sa mère, il s'approcha et roula la pierre de dessus l'ouverture du puits, et abreuva le troupeau de Laban, frère de sa mère.
29:11	Yaacov embrassa Rachel, et il éleva sa voix et pleura.
29:12	Yaacov informa Rachel qu'il était frère de son père, et qu'il était fils de Ribqah. Elle courut en informer son père.
29:13	Il arriva que lorsque Laban entendit parler de Yaacov, fils de sa sœur, il courut au-devant de lui, le prit dans ses bras et l'embrassa. Il le conduisit dans sa maison, et il raconta à Laban toutes ces choses.
29:14	Laban lui dit : En effet, tu es mon os et ma chair. Yaacov demeura un mois de jours chez Laban.
29:15	Laban dit à Yaacov : Me serviras-tu pour rien parce que tu es mon frère ? Dis-moi quel sera ton salaire ?
29:16	Laban avait deux filles. Le nom de l’aînée était Léah et le nom de la cadette Rachel.
29:17	Léah avait les yeux faibles, mais Rachel était belle de taille et belle de figure.
29:18	Yaacov aimait Rachel, et il dit : Je te servirai 7 ans pour Rachel, ta cadette.
29:19	Laban dit : Il vaut mieux que je te la donne que de la donner à un autre homme. Demeure avec moi !
29:20	Yaacov servit sept années pour Rachel, mais elles furent à ses yeux comme quelques jours à cause de son amour pour elle.
29:21	Yaacov dit à Laban : Donne-moi ma femme, car mes jours sont accomplis, et j'irai vers elle.
29:22	Laban réunit tous les hommes du lieu et fit un festin.
29:23	Il arriva, le soir, qu'il prit Léah sa fille et la fit venir vers Yaacov, qui vint vers elle.
29:24	Laban donna Zilpah, sa servante, à Léah, sa fille, pour servante.
29:25	Il arriva, au matin, que voici : c’était Léah ! Yaacov dit à Laban : Qu'est-ce que tu m'as fait ? N'ai-je pas servi chez toi pour Rachel ? Pourquoi m'as-tu trompé ?
29:26	Laban dit : cela ne se fait pas ainsi dans notre endroit, de donner la plus jeune avant l'aînée.
29:27	Accomplis la semaine avec celle-ci, et nous te donnerons aussi l'autre, pour le service que tu feras encore chez moi sept autres années.
29:28	Yaacov fit ainsi : il accomplit la semaine avec celle-ci et Laban lui donna sa fille Rachel pour femme.
29:29	Laban donna Bilhah, sa servante, à Rachel, sa fille, pour servante.
29:30	Yaacov alla aussi vers Rachel, et il aima Rachel plus que Léah, et il servit encore chez Laban sept autres années.
29:31	YHWH vit que Léah était haïe, et il ouvrit sa matrice, tandis que Rachel était stérile.
29:32	Léah devint enceinte et enfanta un fils, et elle appela son nom Reouben. Oui, elle dit : Oui, YHWH a vu mon affliction, maintenant, oui, mon homme m’aimera.
29:33	Elle devint encore enceinte et enfanta un fils, et elle dit : Parce que YHWH a entendu que j'étais haïe, il m'a aussi donné celui-ci. Et elle appela son nom Shim’ôn.
29:34	Elle devint encore enceinte et enfanta un fils, et elle dit : Maintenant mon homme s'attachera à moi, car je lui ai enfanté trois fils. C'est pourquoi on appela son nom Lévi.
29:35	Elle devint encore enceinte et enfanta un fils, et elle dit : Cette fois je louerai YHWH. C'est pourquoi elle appela son nom Yéhouda. Et elle cessa d’enfanter.

## Chapitre 30

30:1	Voyant qu'elle n'enfantait pas pour Yaacov, Rachel devint jalouse de sa sœur. Rachel dit à Yaacov : Donne-moi des fils, sinon je vais mourir !
30:2	Les narines de Yaacov s'enflammèrent contre Rachel et il dit : Suis-je à la place d'Elohîm qui t’a refusé le fruit du ventre ?
30:3	Elle dit : Voici ma servante Bilhah. Viens vers elle ! Qu'elle enfante sur mes genoux et que j'aie des fils par elle.
30:4	Elle lui donna pour femme Bilhah, sa servante, et Yaacov alla vers elle.
30:5	Bilhah devint enceinte et enfanta un fils à Yaacov.
30:6	Rachel dit : Elohîm m’a jugée, et il a écouté ma voix aussi, en me donnant un fils. C'est pourquoi elle l'appela du nom de Dan.
30:7	Bilhah, servante de Rachel, devint encore enceinte et enfanta un second fils à Yaacov.
30:8	Rachel dit : J'ai lutté contre ma sœur les luttes d'Elohîm et j'ai vaincu. C'est pourquoi elle l'appela du nom de Nephthali.
30:9	Léah voyant qu'elle avait cessé d'enfanter, prit Zilpah, sa servante, et la donna pour femme à Yaacov.
30:10	Zilpah, servante de Léah, enfanta un fils à Yaacov.
30:11	Léah dit : La bonne fortune est arrivée ! Elle l'appela du nom de Gad.
30:12	Zilpah, servante de Léah, enfanta un second fils à Yaacov.
30:13	Léah dit : Je suis heureuse, car les filles me déclareront bénie ! C'est pourquoi elle l'appela du nom d'Asher.
30:14	Reouben sortit au temps de la moisson des blés, trouva des mandragores<!--La mandragore, appelée pomme d'amour ou pomme du diable, est une plante qui est employée en médecine comme antidouleur et lors de contrôle ophtalmique pour dilater la pupille. Il lui est aussi attribué des effets prétendument aphrodisiaques et fertilisants. À forte dose, c'est un puissant hallucinogène, utilisé lors de rituels magiques, pour favoriser la sortie astrale des consommateurs.--> aux champs, et les apporta à Léah, sa mère. Et Rachel dit à Léah : Donne-moi, s’il te plaît, des mandragores de ton fils.
30:15	Elle lui dit : Est-ce peu que tu aies pris mon homme, pour que tu prennes aussi les mandragores de mon fils ? Et Rachel dit : Qu'il couche cette nuit avec toi pour les mandragores de ton fils.
30:16	Le soir, comme Yaacov revenait des champs, Léah sortit au-devant de lui et lui dit : Tu viendras vers moi, car je t'ai acheté pour les mandragores de mon fils. Et il coucha avec elle cette nuit-là.
30:17	Elohîm écouta Léah, et elle devint enceinte et enfanta à Yaacov un cinquième fils.
30:18	Léah dit : Elohîm m'a récompensée, parce que j'ai donné ma servante à mon homme. C'est pourquoi elle l'appela du nom de Yissakar.
30:19	Léah devint encore enceinte et enfanta un sixième fils à Yaacov.
30:20	Léah dit : Elohîm m'a donné un beau don. Maintenant mon homme habitera avec moi, car je lui ai enfanté six fils. C'est pourquoi elle l'appela du nom de Zebouloun.
30:21	Ensuite, elle enfanta une fille et l'appela du nom de Diynah.
30:22	Elohîm se souvint de Rachel : Elohîm l'écouta et ouvrit sa matrice.
30:23	Elle devint enceinte et enfanta un fils, et elle dit : Elohîm a ôté mon insulte.
30:24	Elle l'appela du nom de Yossef, en disant : Que YHWH m'ajoute un autre fils !
30:25	Il arriva qu'après que Rachel eut enfanté Yossef, Yaacov dit à Laban : Laisse-moi partir, pour que je m'en aille chez moi, vers ma terre.
30:26	Donne-moi mes femmes et mes enfants, pour lesquels je t'ai servi, et je m'en irai, car tu sais de quelle manière je t'ai servi.
30:27	Laban lui dit : S’il te plaît, si j'ai trouvé grâce à tes yeux... J'ai appris par divination que YHWH m'a béni à cause de toi.
30:28	Il lui dit : Fixe-moi le salaire que tu veux, et je te le donnerai.
30:29	Yaacov lui dit : Tu sais comment je t'ai servi et ce qu'est devenu ton bétail avec moi.
30:30	Car le peu que tu avais avant moi a beaucoup augmenté et YHWH t'a béni depuis que j'ai mis les pieds chez toi. Maintenant, quand m'occuperai-je aussi de ma maison ?
30:31	Il lui dit : Que te donnerai-je ? Yaacov dit : Tu ne me donneras rien si tu fais pour moi ce que je vais dire. Je ferai paître encore ton petit bétail et je le garderai.
30:32	Je passerai parmi tout ton petit bétail aujourd'hui. J’ôterai de là tout agneau tacheté et marqueté et tout agneau noir, parmi les moutons, de même, parmi les chèvres, tout ce qui est marqueté et tacheté. Cela deviendra mon salaire.
30:33	Ma justice rendra témoignage pour moi aujourd'hui ou demain, quand elle viendra devant toi pour mon salaire : tout ce qui ne sera pas marqueté ou tacheté parmi les chèvres et noir parmi les agneaux, ce sera de ma part un vol.
30:34	Laban dit : Voici, qu'il te soit fait comme tu l'as dit.
30:35	Et en ce même jour-là, il sépara les boucs rayés et marquetés, et toutes les chèvres tachetées et marquetées, toutes celles où il y avait du blanc, et tous les agneaux noirs. Il les remit entre les mains de ses fils.
30:36	Il mit trois jours de route entre lui et entre Yaacov. Et Yaacov fit paître le reste du petit bétail de Laban.
30:37	Mais Yaacov prit des branches vertes de peuplier, d'amandier et de platane. Il y pela des bandes blanches, mettant à nu le blanc qui était sur les branches.
30:38	Il mit les branches qu'il avait pelées dans les auges, dans les abreuvoirs, sous les yeux du petit bétail qui venait boire, et il entrait en chaleur quand il venait boire.
30:39	Le petit bétail entrait en chaleur près des branches et le petit bétail engendrait des rayés, des tachetés et des marquetés.
30:40	Yaacov séparait les agneaux et il mettait les faces des bêtes vers les rayés et tout ce qui était noir dans le petit bétail de Laban. Il mit à part ses troupeaux et ne les mit pas près du petit bétail de Laban.
30:41	Il arrivait que toutes les fois que le petit bétail vigoureux entrait en chaleur, Yaacov mettait les branches dans les auges sous les yeux du petit bétail, afin qu'il entre en chaleur près des branches.
30:42	Mais quand le petit bétail devenait chétif, il ne les mettait pas, de sorte que les chétifs appartenaient à Laban et les vigoureux à Yaacov.
30:43	Cet homme devint extrêmement, extrêmement riche, et il eut de nombreux troupeaux, des servantes et des serviteurs, des chameaux et des ânes.

## Chapitre 31

31:1	Or il entendit les discours des fils de Laban qui disaient : Yaacov a pris tout ce qui appartenait à notre père et c'est avec ce qui appartenait à notre père qu'il s'est fait toute cette gloire.
31:2	Yaacov regarda les faces de Laban, et voici, il n’était plus avec lui comme hier ou avant-hier.
31:3	YHWH dit à Yaacov : Retourne vers la terre de tes pères et vers ta parenté, et je serai avec toi.
31:4	Yaacov envoya appeler Rachel et Léah aux champs vers son petit bétail,
31:5	et leur dit : Je vois sur les faces de votre père qu'il n’est plus avec moi comme hier ou avant-hier. Mais l'Elohîm de mon père a été avec moi.
31:6	Vous savez que j'ai servi votre père de tout mon pouvoir.
31:7	Mais votre père s'est moqué de moi et a changé dix fois mon salaire. Mais Elohîm ne lui a pas permis de me faire du mal.
31:8	S’il disait ainsi : Les tachetées deviendront ton salaire, tout le petit bétail faisait des tachetés. Et s’il disait ainsi : Les marquetées deviendront ton salaire, tout le petit bétail faisait des marquetés.
31:9	Elohîm a dépouillé votre père de son troupeau, et il me l'a donné.
31:10	Il arriva au temps où le petit bétail entrait en chaleur, que je levai mes yeux et je vis en rêve, et voici, les boucs qui couvraient les brebis étaient rayés, tachetés et marquetés.
31:11	L'Ange d'Elohîm me dit en rêve : Yaacov ! Et je dis : Me voici !
31:12	Il dit : Lève tes yeux, s’il te plaît et regarde : Tous les boucs qui couvrent les brebis sont rayés, tachetés et marquetés, car j'ai vu tout ce que te fait Laban.
31:13	Je suis le El de Béth-El, où tu oignis la pierre que tu dressas pour monument, où tu me fis un vœu. Maintenant lève-toi, sors de cette terre, et retourne vers la terre de ta naissance.
31:14	Rachel et Léah lui répondirent et dirent : Avons-nous encore quelque portion et quelque héritage dans la maison de notre père ?
31:15	Ne nous a-t-il pas tenues pour des étrangères ? Oui, il nous a vendues, et même il a mangé, il a mangé notre argent.
31:16	Oui, toute la richesse dont Elohîm a dépouillé notre père, elle est à nous et à nos fils. Maintenant fais tout ce qu'Elohîm t'a dit.
31:17	Yaacov se leva et fit monter ses fils et ses femmes sur des chameaux.
31:18	Il emmena tout son troupeau, tous les biens qu'il avait réunis, tout ce qu'il possédait et qu'il avait acquis à Paddan-Aram, pour aller vers Yitzhak, son père, en terre de Kena'ân.
31:19	Laban était allé tondre ses brebis, et Rachel vola les théraphim<!--Les théraphim étaient des idoles utilisées dans un sanctuaire de maison ou dans un lieu de culte. Voir Jg. 18:14 ; 2 R. 23:24.--> de son père.
31:20	Et Yaacov trompa l'esprit de Laban, le Syrien, en ne l'avertissant pas de sa fuite.
31:21	Il s’enfuit, lui et tout ce qui était à lui. Il se leva, passa le fleuve et mit ses faces vers la montagne de Galaad.
31:22	Le troisième jour, on informa Laban que Yaacov s'était enfui.
31:23	Il prit avec lui ses frères, le poursuivit pendant sept jours de route et le rattrapa à la montagne de Galaad.
31:24	Elohîm vint vers Laban, le Syrien, en rêve la nuit, et lui dit : Garde-toi de parler à Yaacov ni en bien ni en mal.
31:25	Laban atteignit Yaacov. Yaacov avait dressé ses tentes sur la montagne. Laban dressa aussi les siennes avec ses frères sur la montagne de Galaad.
31:26	Et Laban dit à Yaacov : Qu'as-tu fait ? Tu as trompé mon esprit, tu as emmené mes filles comme des prisonnières de guerre.
31:27	Pourquoi as-tu pris la fuite secrètement, m'as-tu trompé et ne m'as-tu pas averti ? Je t'aurais laissé aller avec joie et avec des chansons, au son des tambours et des harpes.
31:28	Tu ne m'as pas laissé embrasser mes fils et mes filles. C'est en insensé que tu as agi.
31:29	J'ai en main le pouvoir de vous faire du mal, mais l'Elohîm de votre père m'a parlé la nuit passée et m'a dit : Garde-toi de ne parler à Yaacov ni en bien ni en mal.
31:30	Maintenant que tu es parti, parce que tu languissais après la maison de ton père, pourquoi as-tu volé mes elohîm ?
31:31	Yaacov répondit et dit à Laban : Oui, j'avais peur. Oui, je me disais que tu me déroberais peut-être tes filles.
31:32	Qui tu trouveras avec tes elohîm ne vivra pas. En présence de nos frères, reconnais ce que j’ai avec moi et prends-le pour toi ! Or Yaacov ne savait pas que Rachel les avait volés.
31:33	Laban entra dans la tente de Yaacov, dans la tente de Léah, et dans la tente des deux servantes, et il ne les trouva pas. Et étant sorti de la tente de Léah, il entra dans la tente de Rachel.
31:34	Rachel avait pris les théraphim et les avait mis dans le bât d'un chameau, et s'était assise dessus. Laban palpa toute la tente et ne les trouva pas.
31:35	Elle dit à son père : Que mon seigneur ne se fâche pas de ce que je ne puis me lever devant lui, car j'ai ce que les femmes ont coutume d'avoir. Il chercha et ne trouva pas les théraphim.
31:36	Yaacov se fâcha et querella Laban. Yaacov répondit et dit à Laban : Quelle est ma transgression ? Quel est mon péché pour que tu t’allumes derrière moi ?
31:37	Quand tu as palpé tous mes objets, qu'as-tu trouvé de tous les objets de ta maison ? Mets-les ici en présence de mes frères et de tes frères, et qu'ils jugent entre nous deux.
31:38	Voilà 20 ans que j'ai passés chez toi. Tes brebis et tes chèvres n'ont pas avorté, je n'ai pas mangé les moutons de tes troupeaux.
31:39	Je ne t'ai pas rapporté de bêtes déchirées, j'en ai moi-même subi la perte. Et tu redemandais de ma main ce qui avait été dérobé de jour et ce qui avait été dérobé de nuit.
31:40	Le jour la chaleur me consumait, et la nuit le froid, et le sommeil fuyait de mes yeux.
31:41	Voilà 20 ans que j'ai passés dans ta maison, 14 ans pour tes deux filles, et 6 ans pour tes troupeaux, et tu m'as changé dix fois mon salaire.
31:42	Si je n'avais pas eu pour moi l'Elohîm de mon père, l'Elohîm d'Abraham, et celui que craint Yitzhak, certes tu m'aurais maintenant laissé aller à vide. Elohîm a regardé mon affliction et le travail de mes paumes, et il t'a repris la nuit passée.
31:43	Laban répondit à Yaacov et dit : Ces filles sont mes filles, et ces fils sont mes fils, et ces troupeaux sont mes troupeaux, et tout ce que tu vois est à moi. Que ferais-je aujourd'hui à mes filles et aux fils qu'elles ont enfantés ?
31:44	Maintenant, viens, faisons ensemble une alliance, et qu'elle serve de témoignage entre moi et entre toi.
31:45	Yaacov prit une pierre et il la dressa pour monument.
31:46	Yaacov dit à ses frères : Ramassez des pierres. Ils prirent des pierres et ils en firent un monceau, et ils mangèrent là sur ce monceau.
31:47	Laban l'appela Yegar-Sahadoutha<!--Monceau du témoignage.-->, et Yaacov l'appela Galed<!--Monceau servant de témoin.-->.
31:48	Laban dit : Ce monceau sera aujourd'hui témoin entre moi et entre toi. C'est pourquoi on l’appela du nom de Galed,
31:49	et de Mitspah<!--Tour de gué, d'observation.-->, parce qu'il avait dit : Que YHWH veille sur moi et sur toi, quand nous serons cachés, l’homme loin de son compagnon.
31:50	Si tu maltraites mes filles et si tu prends une autre femme que mes filles, ce n'est pas un homme qui sera avec nous, fais-y bien attention ! C'est Elohîm qui sera témoin entre moi et entre toi.
31:51	Laban dit à Yaacov : Regarde ce monceau, et considère le monument que j'ai dressé entre moi et entre toi.
31:52	Ce monceau est témoin, ce monument est le témoignage que je n'irai pas vers toi au-delà de ce monceau, et que tu ne viendras pas vers moi au-delà de ce monceau et de ce monument pour me faire du mal !
31:53	Les Elohîm d'Abraham et les Elohîm de Nachor jugeront<!--Le verbe est au pluriel.--> entre nous, les Elohîm de leur père ! Yaacov jura par celui que craignait Yitzhak, son père.
31:54	Yaacov sacrifia un sacrifice sur la montagne et invita ses frères à manger du pain. Ils mangèrent du pain et passèrent la nuit sur la montagne.

## Chapitre 32

32:1	Laban se leva tôt le matin, embrassa ses fils et ses filles, et les bénit. Laban s’en alla et retourna en son lieu.
32:2	Yaacov alla son chemin et des anges d'Elohîm le rencontrèrent.
32:3	En les voyant, Yaacov dit : C'est ici le camp d'Elohîm ! Et il appela ce lieu du nom de Mahanaïm.
32:4	Yaacov envoya devant lui des messagers vers Ésav, son frère, en terre de Séir, dans le territoire d'Édom.
32:5	Il leur donna cet ordre en disant : Vous parlerez ainsi à mon seigneur Ésav : Ainsi parle ton serviteur Yaacov : J'ai séjourné comme étranger chez Laban, et j'y ai habité jusqu'à présent.
32:6	J’ai eu des bœufs, des ânes, des brebis, des serviteurs et des servantes, et j'envoie l'annoncer à mon seigneur, afin de trouver grâce à tes yeux.
32:7	Et les messagers revinrent auprès de Yaacov et lui dirent : Nous sommes allés vers ton frère Ésav, et il marche aussi à ta rencontre avec 400 hommes.
32:8	Yaacov eut très peur et l’angoisse le saisit. Il partagea le peuple qui était avec lui, les brebis, les bœufs et les chameaux en deux camps.
32:9	Il se dit : Si Ésav vient vers un camp et le frappe, le camp restant sera en fuite.
32:10	Yaacov dit aussi : Elohîm de mon père Abraham, Elohîm de mon père Yitzhak, ô YHWH qui m'as dit : Retourne vers ta terre, vers ta parenté, et je te ferai du bien.
32:11	Je suis trop petit pour toutes les faveurs et pour toute la fidélité dont tu as usé envers ton serviteur, car j'ai passé ce Yarden avec mon bâton, et maintenant je forme deux camps.
32:12	Délivre-moi, s’il te plaît, de la main de mon frère, de la main d'Ésav ! Car je crains qu'il ne vienne, et qu'il ne me frappe, avec la mère et les fils.
32:13	Et toi, tu as dit : Faire du bien, je te ferai du bien et je rendrai ta postérité comme le sable de la mer, dont la multitude ne sera pas comptée.
32:14	C'est là que Yaacov passa la nuit. Il prit de ce qui venait à sa main, un présent pour Ésav, son frère :
32:15	200 chèvres, 20 boucs, 200 brebis et 20 béliers.
32:16	30 femelles de chameaux qui allaitaient, et leurs petits, 40 jeunes vaches, 10 jeunes taureaux, 20 ânesses et 10 ânes.
32:17	Il les mit entre les mains de ses serviteurs, troupeau par troupeau à part, et il dit à ses serviteurs : Passez en face de moi et mettez un intervalle entre les troupeaux, parmi les troupeaux.
32:18	Il donna cet ordre au premier, disant : Quand Ésav, mon frère, te rencontrera et te demandera, disant : À qui es-tu et où vas-tu ? Et à qui sont ces choses qui sont devant toi ?
32:19	Tu diras : Je suis à ton serviteur Yaacov, c'est un présent qu'il envoie à mon seigneur Ésav. Et voici, il vient lui-même derrière nous.
32:20	Il donna ordre aussi au deuxième, au troisième aussi, et aussi à tous ceux qui marchaient derrière les troupeaux, disant : C’est selon cette parole que vous parlerez à Ésav quand vous le rencontrerez.
32:21	Vous direz : Voici aussi ton serviteur Yaacov derrière nous. Car il se disait : Je rendrai propice ses faces par ce présent qui va en face de moi. Après cela, je verrai ses faces et peut-être supportera-t-il mes faces.
32:22	Le présent passa en face de lui, mais il resta cette nuit-là dans le camp.
32:23	Il se leva cette nuit, et prit ses deux femmes, ses deux servantes, et ses onze enfants, et passa le gué de Yabboq.
32:24	Il les prit, et leur fit passer le torrent, il fit aussi passer tout ce qu'il avait.
32:25	Yaacov demeura seul. Un homme lutta avec lui jusqu'au lever de l'aurore.
32:26	Voyant qu'il ne pouvait pas le vaincre, il le frappa au creux de la cuisse. Ainsi le creux de la cuisse de Yaacov se disloqua pendant qu'il luttait avec lui.
32:27	Il lui dit : Laisse-moi, car l'aurore se lève. Mais il dit : Je ne te laisserai pas partir sans que tu m'aies béni.
32:28	Il lui dit : Quel est ton nom ? Il dit : Yaacov.
32:29	Il dit : Ton nom ne se dira plus Yaacov, mais Israël, car tu as lutté avec Elohîm et avec les hommes, et tu as vaincu.
32:30	Yaacov l'interrogea, en disant : S’il te plaît, fais-moi connaître ton nom. Et il dit : Pourquoi demandes-tu mon nom ? Et il le bénit là<!--Jg. 13:18.-->.
32:31	Yaacov appela ce lieu du nom de Peniel : car j'ai vu Elohîm faces à faces, et mon âme a été délivrée.
32:32	Le soleil se levait lorsqu'il passa Peniel. Yaacov boitait à cause de sa cuisse.
32:33	C'est pourquoi, jusqu'à ce jour, les fils d'Israël ne mangent pas le tendon qui est au creux de la cuisse, parce qu'il avait frappé Yaacov au creux de la cuisse, au tendon.

## Chapitre 33

33:1	Yaacov leva ses yeux et regarda. Et voici, Ésav arrivait avec 400 hommes. Il partagea les enfants de Léah, de Rachel et des deux servantes.
33:2	Il plaça en tête les servantes avec leurs enfants, puis Léah et ses enfants, enfin Rachel et Yossef au dernier rang.
33:3	Quant à lui, il passa devant eux et se prosterna à terre sept fois, jusqu'à ce qu'il soit près de son frère.
33:4	Ésav courut à sa rencontre, le prit dans ses bras, se jeta à son cou et l'embrassa. Et ils pleurèrent.
33:5	Ésav leva ses yeux, vit les femmes et les enfants, et dit : Qui sont ceux-là ? Sont-ils à toi ? Il lui dit : Ce sont les enfants qu'Elohîm, par sa grâce, a donnés à ton serviteur.
33:6	Les servantes s'approchèrent, elles et leurs enfants, et se prosternèrent.
33:7	Léah aussi s'approcha avec ses enfants et ils se prosternèrent. Ensuite Yossef et Rachel s'approchèrent et se prosternèrent aussi.
33:8	Ésav dit : Que veux-tu faire avec tout ce camp que j'ai rencontré ? Et Yaacov dit : C'est pour trouver grâce aux yeux de mon seigneur.
33:9	Ésav dit : Je suis dans l'abondance, mon frère. Garde ce qui est à toi.
33:10	Et Yaacov dit : Non, s’il te plaît, si j'ai trouvé grâce à tes yeux, reçois s’il te plaît ce présent de ma main ! Parce que j'ai vu tes faces comme si j'avais vu les faces d'Elohîm, et parce que tu m'as accueilli favorablement.
33:11	Accepte, s’il te plaît, mon présent qui t'a été offert, car Elohîm m'a comblé de grâce, et je ne manque de rien. Il le pressa tellement qu'il le prit.
33:12	Il dit : Partons et marchons, et je marcherai devant toi.
33:13	Mais il lui dit : Mon seigneur sait que ces enfants sont jeunes et que j'ai des brebis et des vaches qui allaitent. Si l'on forçait leur marche un seul jour, tout le troupeau mourra.
33:14	S’il te plaît, que mon seigneur passe devant son serviteur, et je m'avancerai tout doucement, au pas de ce bétail qui est devant moi, et au pas de ces enfants, jusqu'à ce que j'arrive chez mon seigneur à Séir.
33:15	Ésav dit : S’il te plaît, que je place avec toi quelques-uns de ce peuple qui est avec moi. Et il dit : Pourquoi cela ? Que je trouve grâce aux yeux de mon seigneur !
33:16	Ésav retourna ce jour-là par son chemin à Séir.

### Yaacov dresse un autel à El-Élohé-Israël

33:17	Yaacov partit pour Soukkoth. Il bâtit une maison pour lui, et il fit des cabanes pour son bétail. C'est pourquoi il appela ce lieu du nom de Soukkoth.
33:18	Yaacov vint à Shalem<!--« Paix, paisible ». Voir Ge. 14:18 ; Ps. 76:3.-->, ville de Shekem, en terre de Kena'ân, venant de Paddan-Aram. Il campa en face de la ville.
33:19	Il acheta une portion du champ où il avait dressé sa tente de la main des fils de Hamor, père de Shekem, pour 100 pièces d'argent.
33:20	Et là, il dressa un autel qu'il appela El-Élohé-Israël<!--Le El Fort, le El d'Israël.-->.

## Chapitre 34

34:1	Diynah, la fille que Léah avait enfantée à Yaacov, sortit pour voir les filles de la terre.
34:2	Shekem, fils de Hamor, le Hévien, prince de la terre, la vit, la prit, coucha avec elle et l’humilia.
34:3	Son âme s’attacha à Diynah, fille de Yaacov. Il aima la jeune fille et parla au cœur de la jeune fille.
34:4	Et Shekem parla à Hamor, son père, en disant : Prends-moi cette fille pour femme.
34:5	Yaacov entendit qu'il avait souillé Diynah, sa fille. Or ses fils étaient avec son bétail aux champs. Yaacov garda le silence jusqu'à leur retour.
34:6	Hamor, père de Shekem, sortit vers Yaacov pour lui parler.
34:7	Les fils de Yaacov venaient des champs lorsqu'ils apprirent cela. Ces hommes furent affligés et très fâchés parce qu'il avait fait une infamie en Israël, en couchant avec la fille de Yaacov. Cela ne se fait pas.
34:8	Hamor parla avec eux, en disant : L'âme de Shekem, mon fils, s'est attachée à votre fille. S’il vous plaît, donnez-la-lui pour femme.
34:9	Alliez-vous par mariage avec nous ! Vous nous donnerez vos filles et vous prendrez pour vous nos filles.
34:10	Vous habiterez avec nous, la terre sera en face de vous. Demeurez-y, faites-y du commerce et acquérez-y des possessions.
34:11	Shekem dit au père et aux frères de la fille : Que je trouve grâce à vos yeux, et je donnerai tout ce que vous me direz.
34:12	Exigez de moi une forte dot et beaucoup de présents, et je donnerai ce que vous me direz, mais donnez-moi la jeune fille pour femme.
34:13	Les fils de Yaacov répondirent avec ruse à Shekem et à Hamor, son père. Ils parlèrent ainsi parce que Shekem avait souillé Diynah, leur sœur.
34:14	Ils leur dirent : C'est une chose que nous ne pouvons pas faire, que de donner notre sœur à un homme incirconcis, car ce serait une insulte pour nous.
34:15	Mais nous ne consentirons à ce que vous demandez que si vous deveniez semblables à nous en circoncisant tous les mâles parmi vous.
34:16	Nous vous donnerons nos filles, et nous prendrons vos filles pour nous, et nous habiterons avec vous, et nous ne serons qu'un seul peuple.
34:17	Mais si vous ne voulez pas nous écouter et vous circoncire, nous prendrons notre fille et nous nous en irons.
34:18	Leurs discours parurent bons aux yeux de Hamor et aux yeux de Shekem, fils de Hamor.
34:19	Le jeune homme n'hésita pas à faire la chose, car il désirait la fille de Yaacov. Il était le plus honoré de toute la maison de son père.
34:20	Hamor et Shekem son fils, allèrent à la porte de leur ville et parlèrent aux hommes de leur ville en leur disant :
34:21	Ces hommes sont paisibles à notre égard. Qu'ils habitent sur la terre et qu'ils y fassent commerce ! Voici la terre aux mains larges en face d’eux ! Nous prendrons pour femmes leurs filles et nous leur donnerons nos filles.
34:22	Mais ces hommes ne consentiront à habiter avec nous pour former un seul peuple, que si tout mâle qui est parmi nous est circoncis, comme ils sont eux-mêmes circoncis.
34:23	Leur bétail, leurs biens et toutes leurs bêtes ne seront-ils pas à nous ? Donnons-leur seulement notre consentement, pour qu’ils habitent avec nous.
34:24	Tous ceux qui sortaient par la porte de leur ville obéirent à Hamor et à Shekem son fils. Et tout mâle d'entre tous ceux qui sortaient par la porte de leur ville fut circoncis.
34:25	Mais il arriva, au troisième jour, quand ils étaient dans la douleur, que deux des fils de Yaacov, Shim’ôn et Lévi, frères de Diynah, prirent leurs épées, entrèrent hardiment dans la ville et tuèrent tous les mâles.
34:26	Et Hamor et Shekem son fils, ils les tuèrent à bouche d’épée. Ils enlevèrent Diynah de la maison de Shekem et sortirent.
34:27	Les fils de Yaacov se jetèrent sur les tués et pillèrent la ville, parce qu'on avait souillé leur sœur.
34:28	Ils prirent leurs troupeaux, leurs bœufs, leurs ânes et ce qui était dans la ville et dans les champs.
34:29	Ils emmenèrent et pillèrent toutes leurs richesses, tous leurs petits enfants et leurs femmes, ainsi que tout ce qui était dans les maisons.
34:30	Yaacov dit à Shim’ôn et Lévi : Vous m'avez troublé en me rendant odieux aux habitants de la terre, aux Kena'ânéens et aux Phéréziens, et je n'ai qu'un petit nombre d'hommes. Ils se rassembleront contre moi, me frapperont et me détruiront, moi et ma maison.
34:31	Ils dirent : Traitera-t-on notre sœur comme une prostituée ?

## Chapitre 35

35:1	Elohîm dit à Yaacov : Lève-toi, monte à Béth-El et demeures-y. Tu y dresseras un autel au El qui s’est fait voir à toi lorsque tu fuyais en face d'Ésav, ton frère.
35:2	Yaacov dit à sa famille et à tous ceux qui étaient avec lui : Ôtez les elohîm étrangers qui sont au milieu de vous, purifiez-vous et changez de vêtements<!--Jos. 24:23.-->.
35:3	Nous nous lèverons et nous monterons à Béth-El. Là, je ferai un autel au El qui m’a répondu au jour de ma détresse et qui a été avec moi dans la voie où j'ai marché.
35:4	Ils donnèrent à Yaacov tous les elohîm étrangers qui étaient entre leurs mains et les anneaux qui étaient à leurs oreilles. Yaacov les cacha sous un térébinthe qui est près de Shekem.
35:5	Ils partirent et la terreur d'Elohîm vint sur les villes qui les entouraient, et l'on ne poursuivit pas les fils de Yaacov.
35:6	Yaacov arriva, lui et tout le peuple qui était avec lui, à Louz qui est Béth-El, en terre de Kena'ân.
35:7	Il bâtit là un autel et il appela ce lieu El-Béth-El<!--El de la maison de El.-->, car c'est là que les Elohîm s'étaient découverts<!--Le verbe est au pluriel.--> à lui lorsqu'il fuyait en face de son frère.
35:8	Déborah, nourrice de Ribqah, mourut. Elle fut enterrée au-dessous de Béth-El, sous le chêne auquel on a donné le nom de Chêne des pleurs.
35:9	Elohîm se fit voir encore à Yaacov, lorsqu’il venait de Paddan-Aram, et il le bénit<!--Os. 12:5.-->.
35:10	Elohîm lui dit : Ton nom est Yaacov. On ne t’appellera plus du nom de Yaacov, car ton nom est Israël. Et il l'appela du nom d'Israël.
35:11	Elohîm lui dit : Moi, El Shaddaï ! Porte du fruit et multiplie-toi ! Une nation et une assemblée de nations seront issues de toi et des rois sortiront de tes reins.
35:12	Je te donnerai la terre que j'ai donnée à Abraham et à Yitzhak, et je la donnerai à ta postérité après toi.
35:13	Elohîm monta au-dessus de lui du lieu où il avait parlé avec lui.
35:14	Et Yaacov dressa un monument au lieu où il avait parlé avec lui, un monument de pierres sur lequel il fit une libation et versa de l'huile.
35:15	Yaacov appela le nom du lieu où Elohîm avait parlé avec lui Béth-El.
35:16	Ils partirent de Béth-El. Il y avait encore une certaine distance jusqu'à Éphrata<!--Éphrata : « lieu de la fécondité ».--> lorsque Rachel accoucha. Elle eut un accouchement difficile.
35:17	Il arriva que comme elle avait beaucoup de peine à accoucher, la sage-femme lui dit : N'aie pas peur, car tu as encore un fils.
35:18	Il arriva, comme son âme sortait, car elle était mourante, qu'elle appela son nom Ben-Oni<!--Ben-Oni : « fils de ma douleur ».-->, mais son père l'appela Benyamin<!--Benyamin : « fils de ma main droite », « fils de félicité ».-->.
35:19	Rachel mourut et fut enterrée sur le chemin d'Éphrata, qui est Bethléhem.
35:20	Yaacov dressa un monument sur son sépulcre. C'est le monument du sépulcre de Rachel jusqu’à ce jour.
35:21	Israël partit et dressa ses tentes au-delà de Migdal-Éder.
35:22	Il arriva que, quand Israël habitait en cette terre-là, Reouben vint et coucha avec Bilhah, concubine de son père. Et Israël l'apprit. Or Yaacov avait 12 fils.
35:23	Les fils de Léah étaient Reouben, premier-né de Yaacov, Shim’ôn, Lévi, Yéhouda, Yissakar et Zebouloun.
35:24	Les fils de Rachel : Yossef et Benyamin.
35:25	Les fils de Bilhah, servante de Rachel : Dan et Nephthali.
35:26	Les fils de Zilpah, servante de Léah : Gad et Asher. Ce sont là les fils de Yaacov qui lui naquirent à Paddan-Aram.
35:27	Yaacov arriva auprès de Yitzhak, son père, à Mamré, à Qiryath-Arba qui est Hébron, où Abraham et Yitzhak avaient séjourné.
35:28	Les jours de Yitzhak furent de 180 ans.
35:29	Yitzhak expira et mourut. Il fut recueilli auprès de son peuple, vieux et rassasié de jours, et Ésav et Yaacov ses fils l'enterrèrent.

## Chapitre 36

36:1	Voici la généalogie d'Ésav, qui est Édom.
36:2	Ésav prit ses femmes parmi les filles de Kena'ân : Adah, fille d'Eylon, le Héthien, Oholiybamah fille d'Anah, fille de Tsibeon, le Hévien.
36:3	Il prit aussi Basmath, fille de Yishmael, sœur de Nebayoth.
36:4	Adah enfanta à Ésav Éliyphaz, et Basmath enfanta Reouel.
36:5	Et Oholiybamah enfanta Yéoush, Ya`lam et Koré. Ce sont là les fils d'Ésav qui lui naquirent en terre de Kena'ân.
36:6	Ésav prit ses femmes, ses fils et ses filles, et toutes les âmes de sa maison, tous ses troupeaux, ses bêtes et tout le bien qu'il avait réunis en terre de Kena'ân, et il s'en alla vers une terre face à Yaacov, son frère.
36:7	Car leurs biens étaient devenus trop grands pour qu’ils habitent ensemble. La terre où ils séjournaient ne pouvait plus les porter en face de leurs troupeaux.
36:8	Ésav habita dans la montagne de Séir. Ésav, c'est Édom.
36:9	Voici la généalogie d'Ésav, père d'Édom, dans la montagne de Séir.
36:10	Voici les noms des fils d'Ésav : Éliyphaz fils d'Adah, femme d'Ésav ; Reouel, fils de Basmath, femme d'Ésav.
36:11	Les fils d'Éliyphaz furent : Théman, Omar, Tsepho, Gaetham et Qenaz.
36:12	Timna devint la concubine d'Éliyphaz, fils d'Ésav, et elle enfanta à Éliyphaz Amalek. Ce sont là les fils d'Adah, femme d'Ésav.
36:13	Voici les fils de Reouel : Nahath, Zérach, Shammah et Mizzah. Ce sont là les fils de Basmath, femme d'Ésav.
36:14	Voici les fils d'Oholiybamah, fille d'Anah, fille de Tsibeon, femme d'Ésav : elle enfanta à Ésav Yéoush, Ya`lam et Koré.
36:15	Voici les chefs des fils d'Ésav. Voici les fils d'Éliyphaz, premier-né d'Ésav, le chef Théman, le chef Omar, le chef Tsepho, le chef Qenaz,
36:16	le chef Koré, le chef Gaetham, le chef Amalek. Ce sont là les chefs d'Éliyphaz en terre d'Édom. Ce sont les fils d'Adah.
36:17	Voici les fils de Reouel, fils d'Ésav : le chef Nahath, le chef Zérach, le chef Shammah et le chef Mizzah. Ce sont là les chefs sortis de Reouel, en terre d'Édom. Ce sont là les fils de Basmath, femme d'Ésav.
36:18	Voici les fils d'Oholiybamah, femme d'Ésav : Le chef Yéoush, le chef Ya`lam, le chef Koré. Ce sont là les chefs sortis d'Oholiybamah, fille d'Anah, femme d'Ésav.
36:19	Ce sont là les fils d'Ésav, qui est Édom, et ce sont là leurs chefs.
36:20	Voici les fils de Séir, le Horien, habitants de la terre : Lothan, Shobal, Tsibeon, Anah,
36:21	Dishon, Etser et Dishan. Ce sont là les chefs des Horiens, fils de Séir, en terre d'Édom.
36:22	Les fils de Lothan furent Hori et Héman. Et Timna était sœur de Lothan.
36:23	Voici les fils de Shobal : Alvan, Manahath, Ébal, Shepho et Onam.
36:24	Voici les fils de Tsibeon : Ayah et Anah. C'est cet Anah qui trouva les sources chaudes dans le désert, quand il faisait paître les ânes de Tsibeon, son père.
36:25	Voici les fils d'Anah : Dishon et Oholiybamah, fille d'Anah.
36:26	Voici les fils de Dishon : Hemdan, Eshban, Yithran et Keran.
36:27	Voici les fils d'Etser : Bilhan, Zaavan et Aqan.
36:28	Voici les fils de Dishan : Outs et Aran.
36:29	Voici les chefs des Horiens : Le chef Lothan, le chef Shobal, le chef Tsibeon, le chef Anah,
36:30	le chef Dishon, le chef Etser, le chef Dishan. Ce sont là les chefs des Horiens, selon leurs chefs, en terre de Séir.
36:31	Voici les rois qui ont régné en terre d'Édom avant qu'un roi règne sur les fils d'Israël.
36:32	Béla, fils de Béor, régna sur Édom, et le nom de sa ville était Dinhabah.
36:33	Béla mourut et Yobab, fils de Zérach de Botsrah, régna à sa place.
36:34	Yobab mourut et Housham, de la terre des Thémanites, régna à sa place.
36:35	Housham mourut et Hadad, fils de Bédad, régna à sa place. C'est lui qui frappa Madian dans le territoire de Moab. Le nom de sa ville était Avith.
36:36	Hadad mourut et Samlah, de Masreqah, régna à sa place.
36:37	Samlah mourut et Shaoul de Réhoboth sur le fleuve, régna à sa place.
36:38	Shaoul mourut et Baal-Hanan, fils d'Acbor, régna à sa place.
36:39	Baal-Hanan, fils d'Acbor mourut et Hadar régna à sa place. Le nom de sa ville était Paou, et le nom de sa femme Mehétabeel. Elle était la fille de Mathred et la petite-fille de Mézahab.
36:40	Voici les noms des chefs d'Ésav selon leurs familles, selon leurs territoires et d'après leurs noms : le chef Timna, le chef Alvah, le chef Yetheyh,
36:41	le chef Oholiybamah, le chef Élah, le chef Pinon,
36:42	le chef Qenaz, le chef Théman, le chef Mibtsar,
36:43	le chef Magdiel et le chef Iram. Ce sont là les chefs d'Édom, selon leurs habitations, en terre de leur propriété. C'est Ésav le père d'Édom.

## Chapitre 37

37:1	Yaacov demeura en terre de Kena'ân, terre où avait séjourné son père.
37:2	Voici la généalogie de Yaacov. Yossef, fils de 17 ans, devint berger de troupeaux avec ses frères, - il était un jeune homme, - avec les fils de Bilhah et les fils de Zilpah, femmes de son père. Et Yossef rapportait à leur père leurs méchantes diffamations.
37:3	Israël aimait Yossef plus que tous ses fils, parce qu'il était le fils de sa vieillesse, et il lui fit une tunique de plusieurs couleurs.
37:4	Ses frères voyant que leur père l’aimait plus que ses frères, le haïssaient et ne pouvaient lui parler en paix.
37:5	Yossef rêva un rêve et le raconta à ses frères, qui le haïrent encore plus.
37:6	Il leur dit : Écoutez, s’il vous plaît, ce rêve que j'ai rêvé.
37:7	Voici, nous étions à lier des gerbes au milieu d'un champ, et voici que ma gerbe se leva et se tint droite. Et voici, vos gerbes l'entourèrent et se prosternèrent devant elle.
37:8	Ses frères lui dirent : Régner, régneras-tu sur nous ? Ou gouverner, nous gouverneras-tu ? Ils le haïrent encore plus pour ses rêves et pour ses paroles.
37:9	Il rêva encore un autre rêve et il le raconta à ses frères, en disant : Voici, j'ai encore rêvé un rêve. Et voici, le soleil, la lune et onze étoiles se prosternaient devant moi<!--Ap. 12:1.-->.
37:10	Il le raconta à son père et à ses frères. Son père le réprimanda et lui dit : Qu’est-ce que ce rêve que tu as rêvé ? Venir, viendrons-nous, moi, ta mère, tes frères, nous prosterner à terre devant toi ?
37:11	Ses frères devinrent jaloux de lui, mais son père retenait ses discours<!--Ac. 7:9.-->.
37:12	Les frères de Yossef s'en allèrent paître les troupeaux de leur père à Shekem.
37:13	Israël dit à Yossef : Tes frères ne paissent-ils pas à Shekem ? Viens, et je t’enverrai vers eux. Et il lui dit : Me voici !
37:14	Israël lui dit : Va, s’il te plaît, et vois si tes frères sont en paix et si le troupeau est en paix, et rapporte-le-moi. Il l'envoya ainsi de la vallée d'Hébron, et il alla à Shekem.
37:15	Un homme le rencontra errant dans les champs et cet homme lui demanda en disant : Que cherches-tu ?
37:16	Il dit : Je cherche mes frères. S’il te plaît, dis-moi où ils font paître.
37:17	L'homme dit : Ils sont partis d'ici, car j’ai entendu qu’ils disaient : Allons à Dothan. Yossef alla après ses frères et les trouva à Dothan.
37:18	Ils le virent de loin et, avant qu'il soit près d'eux, ils complotèrent contre lui pour le tuer.
37:19	Et chaque homme dit à son frère : Voici le maître des rêves qui vient !
37:20	Maintenant, venez, tuons-le et jetons-le dans l'une de ces citernes. Nous dirons qu'une bête féroce l'a dévoré, et nous verrons ce que deviendront ses rêves.
37:21	Mais Reouben entendit cela et le délivra de leurs mains, en disant : Ne tuons pas son âme.
37:22	Reouben leur dit encore : Ne répandez pas le sang ! Jetez-le dans cette citerne qui est dans le désert, mais ne mettez pas la main sur lui. C'était pour le délivrer de leurs mains et le renvoyer à son père.
37:23	Il arriva que lorsque Yossef arriva auprès de ses frères, ils dépouillèrent Yossef de sa tunique, de la tunique de plusieurs couleurs qui était sur lui.
37:24	Ils le prirent et le jetèrent dans la citerne. Cette citerne était vide, il n'y avait pas d'eau.
37:25	Ils s'assirent ensuite pour manger du pain. Et comme ils levaient les yeux, voici qu'ils aperçurent une caravane de Yishmaélites qui venait de Galaad. Leurs chameaux étaient chargés d'aromates, de baume et de la myrrhe, qu'ils allaient livrer en Égypte.
37:26	Et Yéhouda dit à ses frères : Que gagnerons-nous à tuer notre frère et à cacher son sang ?
37:27	Venez ! Nous le vendrons à ces Yishmaélites et notre main ne sera pas sur lui, car il est notre frère, c'est notre chair. Et ses frères lui obéirent.
37:28	Et comme les marchands madianites passaient, ils tirèrent et firent monter Yossef de la citerne, et le vendirent pour 20 pièces d'argent aux Yishmaélites, qui emmenèrent Yossef en Égypte<!--Ps. 105:17.-->.
37:29	Reouben revint à la citerne, et voici, Yossef n'était plus dans la citerne. Alors il déchira ses vêtements.
37:30	Il retourna vers ses frères et leur dit : L'enfant n'y est plus ! Et moi, moi, où irai-je ?
37:31	Ils prirent la tunique de Yossef, et ayant tué un bouc d'entre les chèvres, ils plongèrent la tunique dans le sang.
37:32	Ils envoyèrent et firent porter à leur père la tunique de plusieurs couleurs, en lui disant : Voici ce que nous avons trouvé ! Reconnais, s’il te plaît, si c'est la tunique de ton fils ou non.
37:33	Yaacov la reconnut et dit : C'est la tunique de mon fils ! Une bête féroce l'a dévoré ! Déchirer, Yossef a été déchiré !
37:34	Et Yaacov déchira ses vêtements, il mit un sac sur ses reins et il porta le deuil de son fils durant plusieurs jours.
37:35	Tous ses fils et toutes ses filles vinrent pour le consoler, mais il rejeta toute consolation. Il disait : C'est en pleurant que je descendrai vers mon fils dans le shéol ! C'est ainsi que son père le pleurait.
37:36	Les Madianites le vendirent en Égypte à Potiphar, eunuque de pharaon, le grand bourreau.

## Chapitre 38

38:1	Il arriva qu'en ce temps-là, Yéhouda descendit de chez ses frères et se détourna jusque chez un homme d'Adoullam, du nom de Chiyrah.
38:2	Là, Yéhouda vit la fille d'un homme, un Kena'ânéen, du nom de Shoua. Il la prit pour femme et alla vers elle.
38:3	Elle devint enceinte et enfanta un fils qu'elle appela du nom d'Er.
38:4	Elle devint encore enceinte et enfanta un fils qu'elle appela du nom d'Onan.
38:5	Elle enfanta de nouveau un fils qu'elle appela du nom de Shélah. Il était à Czib quand elle l'enfanta.
38:6	Yéhouda prit une femme pour Er, son premier-né, une femme du nom de Tamar.
38:7	Mais Er, le premier-né de Yéhouda, était méchant devant YHWH, et YHWH le fit mourir<!--No. 26:19.-->.
38:8	Yéhouda dit à Onan : Va avec la femme de ton frère, exerce envers elle ton droit de beau-frère et suscite une postérité à ton frère<!--Lé. 25:25,48 ; De. 25:5-7. Voir commentaire en Ru. 2:20.-->.
38:9	Onan, sachant que cette postérité ne serait pas pour lui, et il arriva que quand il allait vers la femme de son frère, il se corrompait à terre afin de ne pas donner de postérité à son frère.
38:10	Ce qu’il faisait fut mauvais aux yeux de YHWH, et il le fit aussi mourir.
38:11	Et Yéhouda dit à Tamar, sa belle-fille : Demeure veuve dans la maison de ton père, jusqu'à ce que Shélah, mon fils, soit devenu grand. Car il se disait : Il faut prendre garde qu'il ne meure comme ses frères. Ainsi, Tamar s'en alla et demeura dans la maison de son père.
38:12	Après beaucoup de jours, la fille de Shoua, femme de Yéhouda, mourut. Lorsque Yéhouda fut consolé, il monta vers ceux qui tondaient ses brebis à Timnah, avec Chiyrah, l'Adoullamite, son ami intime.
38:13	On en informa Tamar et on lui dit : Voici, ton beau-père monte à Timnah pour tondre ses brebis.
38:14	Elle ôta ses habits de veuve, se couvrit d'un voile et s'enveloppa, et elle s'assit à l'entrée d'Énaïm, sur le chemin de Timnah, car elle voyait que Shélah était devenu grand et qu'elle ne lui était pas donnée pour femme.
38:15	Yéhouda l’ayant vue, s’imagina que c'était une prostituée, car elle avait couvert ses faces.
38:16	Il s'inclina vers elle, dans le chemin et dit : Permets, s’il te plaît, que je vienne vers toi ! Car il ne savait pas que c'était sa belle-fille. Elle dit : Que me donneras-tu pour venir vers moi ?
38:17	Il dit : Je t'enverrai un chevreau d'entre les chèvres du troupeau. Elle dit : Me donneras-tu un gage jusqu'à ce que tu l'envoies ?
38:18	Il dit : Quel gage te donnerai-je ? Elle dit : Ton sceau, ton cordon et ton bâton que tu as à la main. Et il les lui donna. Il alla vers elle et elle devint enceinte de lui.
38:19	Elle se leva et s'en alla. Elle ôta son voile et remit ses habits de veuve.
38:20	Yéhouda envoya un chevreau d'entre ses chèvres par la main de son ami, l'Adoullamite, pour qu'il retire le gage de la main de la femme, mais il ne la trouva pas.
38:21	Il interrogea les hommes du lieu où elle avait été, en disant : Où est cette prostituée qui était à Énaïm, sur le chemin ? Ils dirent : Il n'y a pas eu ici de prostituée.
38:22	Il retourna auprès de Yéhouda et lui dit : Je ne l'ai pas trouvée, et même les hommes du lieu m'ont dit : Il n'y a pas eu ici de prostituée.
38:23	Yéhouda dit : Qu'elle garde ce qu'elle a ! Il ne faut pas nous faire mépriser. Voici, j'ai envoyé ce chevreau, mais tu ne l'as pas trouvée.
38:24	Et il arriva, environ trois mois après, qu’on informa Yéhouda, en disant : Tamar, ta belle-fille, s’est prostituée, et voici elle est même enceinte. Et Yéhouda dit : Faites-la sortir et qu'elle soit brûlée !
38:25	Comme on la faisait sortir, elle envoya dire à son beau-père : Je suis enceinte de l'homme à qui ces choses appartiennent. Elle dit aussi : Reconnais, s’il te plaît, à qui sont cet anneau à cacheter, ce cordon et ce bâton.
38:26	Yéhouda les reconnut et il dit : Elle est plus juste que moi, parce que je ne l'ai pas donnée à Shélah, mon fils. Et il ne la connut plus.
38:27	Quand elle fut au moment d'accoucher, voici, il y avait des jumeaux dans son ventre.
38:28	Il arriva, comme elle accouchait, que l'un d'eux donna la main. La sage-femme la prit et attacha sur la main un fil cramoisi en disant : Celui-ci sort le premier.
38:29	Il arriva, comme il retira la main, que voici, son frère sortit. Elle dit : Quelle brèche tu as faite ! Et on appela son nom Pérets.
38:30	Ensuite, sortit son frère qui avait à la main le fil cramoisi, et on appela son nom Zérach.

## Chapitre 39

39:1	On fit descendre Yossef en Égypte, et Potiphar, eunuque de pharaon, le grand bourreau, homme égyptien, l'acheta de la main des Yishmaélites qui l’avaient fait descendre là.
39:2	YHWH était avec Yossef et il devint un homme qui prospérait. Il était dans la maison de son seigneur<!--Seigneur vient de l'hébreu « adhonaw » qui est le pluriel de « adhôn ». L'auteur veut exprimer l'excellence, la majesté. Voir Ge. 39:3,7,8,16,19,20.--> égyptien.
39:3	Son seigneur vit que YHWH était avec lui et que YHWH faisait prospérer entre ses mains tout ce qu'il faisait.
39:4	Yossef trouva grâce à ses yeux et il le servait. Il l'établit sur sa maison et lui remit entre les mains tout ce qui est à lui.
39:5	Il arriva que depuis qu’il l’avait établi sur sa maison et sur tout ce qu'il possédait, YHWH bénit la maison de l'Égyptien à cause de Yossef, et la bénédiction de YHWH vint sur tout ce qu'il avait à la maison et aux champs.
39:6	Il remit tout ce qui était à lui entre les mains de Yossef et ne s'occupa plus de rien, excepté de la nourriture qu'il mangeait. Or Yossef était beau de taille et beau de figure.
39:7	Après ces choses, il arriva que la femme de son seigneur porta les yeux sur Yossef et elle lui dit : Couche avec moi<!--Pr. 7:9-13.--> !
39:8	Mais il le refusa et dit à la femme de son seigneur : Voici, mon seigneur ne prend connaissance avec moi de quoi que ce soit dans la maison, et il a remis en ma main tout ce qui est à lui.
39:9	Il n'y a personne dans cette maison qui soit plus grand que moi, et il ne m'a rien refusé, sauf toi, parce que tu es sa femme. Comment ferai-je ce grand mal ? Pécherais-je contre Elohîm ?
39:10	Il arriva, comme elle parlait à Yossef, jour après jour, qu’il ne l’écouta pas pour coucher à côté d’elle, pour être avec elle.
39:11	Or il arriva un jour qu’il entra dans la maison pour faire son travail, et il n’y avait là, dans la maison, aucun homme des hommes de la maison.
39:12	Elle le saisit par son vêtement et lui dit : Couche avec moi ! Mais il laissa son vêtement entre ses mains, s'enfuit et sortit dehors<!--1 Co. 6:18.-->.
39:13	Il arriva, quand elle vit qu’il lui avait laissé son vêtement entre les mains et s’était enfui dehors,
39:14	qu’elle appela les hommes de sa maison et leur parla, en disant : Voyez ! On nous a amené un homme, un Hébreu, pour se moquer de nous. Il est venu vers moi pour coucher avec moi, et j’ai appelé à grands cris.
39:15	Et il est arrivé, quand il a entendu que j’élevais ma voix et que je criais, qu’il a laissé son vêtement à côté de moi et s’est enfui, et est sorti dehors.
39:16	Et elle garda le vêtement de Yossef jusqu'à ce que son seigneur rentre à la maison.
39:17	Elle lui parla selon ces paroles, en disant : Le serviteur hébreu que tu nous as amené est venu vers moi pour se moquer de moi.
39:18	Il est arrivé, comme j’élevais ma voix et que je criais, qu’il a laissé son vêtement à côté de moi et s’est enfui dehors.
39:19	Il arriva, dès que son seigneur entendit les paroles de sa femme, qui lui parlait, disant : Ton serviteur a agi envers moi selon ces paroles, ses narines s'enflammèrent.
39:20	Et le seigneur de Yossef le prit et le mit en maison d’arrêt, le lieu où les prisonniers du roi étaient enfermés. Il fut là, en maison d’arrêt.
39:21	Mais YHWH fut avec Yossef. Il étendit la miséricorde sur lui et lui donna de la grâce aux yeux du chef de la maison d’arrêt.
39:22	Le chef de la maison d'arrêt mit entre les mains de Yossef tous les prisonniers de la maison d’arrêt, et tout ce qu'il y avait à faire, il le faisait.
39:23	Le chef de la maison d'arrêt ne prêtait attention à rien de tout de ce que Yossef avait en main, parce que YHWH était avec lui. Et YHWH faisait prospérer tout ce qu'il faisait.

## Chapitre 40

40:1	Après ces choses, il arriva que l'échanson et le panetier du roi d'Égypte péchèrent contre leur seigneur, le roi d'Égypte.
40:2	Pharaon fut très fâché contre ces deux eunuques, contre le chef des échansons et contre le chef des panetiers.
40:3	Et il les mit en prison dans la maison du chef des bourreaux, dans la maison de prison, lieu où Yossef était emprisonné.
40:4	Le grand bourreau les plaça sous la surveillance de Yossef qui les servait. Ils restèrent des jours en prison.
40:5	Pendant une nuit, l'échanson et le panetier du pharaon enfermés dans la maison d'arrêt rêvèrent tous les deux un rêve, chacun son rêve, chacun selon l'interprétation de son rêve.
40:6	Yossef, étant venu le matin vers eux, les regarda, et voici, ils étaient tristes.
40:7	Et il interrogea ces deux eunuques de pharaon, qui étaient avec lui dans la prison de son maître, et leur dit : Pourquoi avez-vous mauvaises faces aujourd'hui ?
40:8	Ils lui dirent : Nous avons rêvé un rêve et il n'y a personne pour l'interpréter. Et Yossef leur dit : Les interprétations n'appartiennent-elles pas à Elohîm ? S’il vous plaît, racontez-le moi<!--Job 33:15 ; 1 Co. 12:8-10.-->.
40:9	Le chef des échansons raconta son rêve à Yossef et lui dit : Dans mon rêve, voici, il y avait un cep devant moi.
40:10	Ce cep avait trois sarments. Quand il a poussé, sa fleur est montée et ses grappes ont donné des raisins mûrs.
40:11	La coupe de pharaon était dans ma main. J'ai pris les raisins, je les ai pressés dans la coupe de pharaon et j'ai mis la coupe dans la paume de pharaon.
40:12	Yossef lui dit : Voici son interprétation : Les trois sarments sont trois jours.
40:13	Dans trois jours, pharaon élèvera ta tête et te rétablira dans ta charge, et tu mettras la coupe de pharaon dans sa main, comme tu le faisais auparavant lorsque tu étais son échanson.
40:14	Mais si tu te souviens de moi, quand tu seras heureux, use de bonté envers moi s’il te plaît : fais mention de moi à pharaon, afin qu'il me fasse sortir de cette maison.
40:15	Car j'ai été enlevé de la terre des Hébreux et ici non plus je n'ai rien fait pour être mis en prison.
40:16	Le chef des panetiers voyant que Yossef avait bien interprété, lui dit : Voici, il y avait aussi dans mon rêve trois corbeilles de pain blanc sur ma tête.
40:17	Dans la corbeille la plus élevée, il y avait pour pharaon, des mets de toute espèce, cuits au four. Les créatures volantes les mangeaient dans la corbeille au-dessus de ma tête.
40:18	Yossef répondit, et dit : Voici son interprétation : Les trois corbeilles sont trois jours.
40:19	Dans trois jours, pharaon enlèvera ta tête de dessus toi et te fera pendre à un bois, et les créatures volantes mangeront ta chair sur toi.
40:20	Il arriva au troisième jour, le jour de la naissance de pharaon, qu'il fit un festin pour tous ses serviteurs. Il éleva la tête du chef des échansons et la tête du chef des panetiers, au milieu de ses serviteurs.
40:21	Il rétablit le chef des échansons dans sa charge d'échanson, pour qu'il mette la coupe dans la paume de pharaon.
40:22	Mais il fit pendre le chef des panetiers, selon l'explication que Yossef leur avait donnée.
40:23	Le chef des échansons ne se souvint pas de Yossef. Il l'oublia.

## Chapitre 41

41:1	Mais il arriva qu'au bout de 2 ans de jours, pharaon eut un rêve, et voici qu’il se tenait près du fleuve.
41:2	Et voici que du fleuve montaient sept vaches de belle apparence et grasses de chair, et elles paissaient dans la prairie.
41:3	Et voici que sept autres vaches montaient du fleuve derrière elles, d'apparence mauvaise et maigres en chair. Elles se tinrent à côté des vaches qui étaient sur le bord du fleuve.
41:4	Les vaches d'apparence mauvaise et maigres en chair, dévoraient les sept vaches de belle apparence et grasses. Et pharaon se réveilla.
41:5	Il s'endormit et il eut un second rêve. Et voici que sept épis montaient sur une seule tige, gras et bons.
41:6	Et voici que sept épis maigres et brûlés par le vent d'orient poussaient après eux.
41:7	Les épis maigres avalaient les sept épis gras et pleins. Puis pharaon se réveilla, et voici que c'était un rêve !
41:8	Et il arriva au matin que son esprit était troublé. Il envoya appeler tous les magiciens et tous les sages d'Égypte, et pharaon leur raconta ses rêves. Mais personne ne put les interpréter à pharaon.
41:9	Le chef des échansons parla à pharaon, en disant : Je rappelle aujourd’hui mes péchés !
41:10	Lorsque pharaon fut fâché contre ses serviteurs et m’avait mis en prison dans la maison du chef des bourreaux, le chef des panetiers et moi,
41:11	nous avons rêvé un rêve, une même nuit, moi et lui. Nous rêvions chacun selon l'interprétation de son rêve.
41:12	Or il y avait là avec nous un jeune homme hébreu, esclave du chef des bourreaux. Nous lui avons raconté nos rêves et il nous les a interprétés. Il a interprété à chacun selon son rêve.
41:13	Il arriva que, comme il nous avait interprété, ainsi en fut-il. Moi, on me fit retourner à ma fonction, et lui, on le pendit.
41:14	Pharaon envoya appeler Yossef. On le fit courir hors de la prison. Il se rasa, changea son manteau et entra chez pharaon.
41:15	Et pharaon dit à Yossef : J’ai rêvé un rêve, mais il n’y a personne pour l’interpréter. Or j'ai entendu dire de toi que, quand tu entends un rêve, tu l'interprètes.
41:16	Et Yossef répondit à pharaon, en disant : Ce n'est pas moi ! C'est Elohîm qui donnera à pharaon une réponse de paix.
41:17	Pharaon dit à Yossef : Dans mon rêve, voici, je me tenais sur le bord du fleuve,
41:18	et voici que du fleuve montaient sept vaches grasses de chair et de belle apparence, et elles paissaient dans la prairie.
41:19	Et voici que montaient derrière elles sept autres vaches, pauvres, d'apparence très mauvaise et maigres de chair. Je n'en ai pas vu d'aussi mauvaises sur toute la terre d'Égypte.
41:20	Mais les vaches maigres et mauvaises dévoraient les sept premières vaches qui étaient grasses.
41:21	Celles-ci entraient dans leur ventre, sans qu’on puisse savoir qu’elles étaient entrées dans leur ventre, car leur apparence était mauvaise comme auparavant. Puis je me suis réveillé.
41:22	J’ai vu dans mon rêve, et, voici, sept épis montaient sur une seule tige, pleins et bons.
41:23	Mais voici que sept épis desséchés, maigres, brûlés par le vent d'orient, poussaient après eux.
41:24	Les épis maigres avalaient les sept bons épis. Je l'ai dit aux magiciens, mais personne ne me l'a fait connaître.
41:25	Yossef dit à pharaon : Le rêve de pharaon est un. Elohîm a fait connaître à pharaon ce qu’il va faire.
41:26	Les sept vaches belles sont sept années, et les sept bons épis sont sept années : c'est un seul rêve.
41:27	Les sept vaches maigres et mauvaises qui montaient derrière les premières sont sept années, tout comme les sept épis vides brûlés par le vent d'orient. Ce sont sept années de famine.
41:28	C’est là la parole que j’ai dite à pharaon : Elohîm a fait voir à pharaon ce qu'il va faire.
41:29	Voici, il y aura sept années de grande abondance sur toute la terre d'Égypte.
41:30	Sept années de famine viendront après elles, et l'on oubliera toute cette abondance sur la terre d'Égypte. La famine consumera la terre.
41:31	On ne s'apercevra plus de l'abondance sur la terre, face à cette famine qui suivra après, car elle sera vraiment très grande.
41:32	Si le rêve a été deux fois répété à pharaon, c'est que la parole est fermement établie de la part d'Elohîm, et qu'Elohîm se hâte de l'accomplir.
41:33	Maintenant, que pharaon choisisse un homme intelligent et sage, et qu'il l'établisse sur la terre d'Égypte.
41:34	Que pharaon agisse et qu’il prépose des surveillants sur la terre, et qu'il prenne le cinquième de la terre d'Égypte durant les sept années d'abondance.
41:35	Qu’on rassemble toutes les céréales de ces bonnes années qui viennent, qu’on entasse, sous la main de pharaon, du blé, des céréales dans les villes et qu’on les garde.
41:36	Ces céréales seront en réserve pour la terre durant les sept années de famine qui seront sur la terre d'Égypte, afin que la terre ne soit pas consumée par la famine.
41:37	Cette parole fut bonne aux yeux du pharaon et aux yeux de tous ses serviteurs<!--Ac. 7:10.-->.
41:38	Pharaon dit à ses serviteurs : Trouverons-nous un homme en qui soit comme en celui-ci l'Esprit d'Elohîm ?
41:39	Et pharaon dit à Yossef : Puisqu'Elohîm t'a fait connaître tout cela, il n'y a personne qui soit aussi intelligent et aussi sage que toi.
41:40	Tu seras sur ma maison, et tout mon peuple obéira à ta bouche. Je serai seulement plus grand que toi par le trône.
41:41	Pharaon dit encore à Yossef : Regarde, je t'établis sur toute la terre d'Égypte.
41:42	Pharaon ôta son anneau de sa main et le mit à la main de Yossef. Il le fit revêtir d'habits de fin lin et lui mit un collier d'or au cou.
41:43	Il le fit monter sur le char qui suivait le sien et on criait devant lui : À genoux ! Et il l'établit sur toute la terre d'Égypte.
41:44	Pharaon dit à Yossef : Je suis pharaon ! Et sans toi, personne ne lèvera la main ni le pied sur toute la terre d'Égypte.
41:45	Pharaon appela Yossef du nom de Tsaphnath-Paenéach, et il lui donna pour femme Asnath, fille de Poti-Phéra, prêtre d'On. Et Yossef alla visiter la terre d'Égypte.
41:46	Yossef était fils de 30 ans quand il se tint en face de pharaon, roi d'Égypte. Il quitta pharaon et parcourut toute la terre d'Égypte.
41:47	Et la terre rapporta très abondamment pendant les sept années de fertilité.
41:48	Il rassembla toutes les céréales de ces sept années sur la terre d'Égypte, et mit ces céréales dans les villes. Il mit dans chaque ville les céréales des champs qui étaient autour d’elle.
41:49	Yossef amassa du blé comme le sable de la mer, en si grande quantité qu'on cessa de le compter, parce qu'il était sans nombre.
41:50	Avant les années de famine, il naquit à Yossef deux fils, que lui enfanta Asnath, fille de Poti-Phéra, prêtre d'On.
41:51	Yossef appela le premier-né du nom de Menashè<!--Manassé.-->. Oui, Elohîm m'a fait oublier toute ma peine et toute la maison de mon père.
41:52	Et il appela le second du nom d'Éphraïm. Oui, Elohîm m'a fait porter du fruit en terre de mon affliction.
41:53	Alors s’achevèrent les sept années de l'abondance qui avaient été en terre d'Égypte.
41:54	Et les sept années de famine commencèrent à venir, comme Yossef l'avait dit. Et la famine survint sur toutes les terres, mais il y avait du pain sur toute la terre d'Égypte.
41:55	Toute la terre d'Égypte fut affamée, et le peuple cria vers pharaon pour du pain. Et pharaon dit à toute l’Égypte : Allez vers Yossef et faites ce qu'il vous dira.
41:56	La famine étant survenue sur toutes les faces de la terre, Yossef ouvrit tout ce qui y était et vendit du blé à l’Égypte. La famine augmentait sur la terre d'Égypte.
41:57	On venait de toutes les terres jusqu'en Égypte pour acheter du blé auprès de Yossef, car la famine était très grande sur toute la Terre.

## Chapitre 42

42:1	Yaacov, voyant qu'il y avait du blé en Égypte, Yaacov dit à ses fils : Pourquoi vous regardez-vous l’un l’autre ?
42:2	Il leur dit aussi : Voici, j'ai entendu qu'il y a du blé en Égypte, descendez-y pour nous en acheter là. Nous vivrons, nous ne mourrons pas.
42:3	Les frères de Yossef descendirent pour acheter du blé en Égypte.
42:4	Mais Yaacov n'envoya pas Benyamin, frère de Yossef, avec ses frères. Car il se disait : De peur qu’un malheur ne lui arrive !
42:5	Les fils d'Israël allèrent pour acheter du blé parmi ceux qui y allaient, car la famine était sur la terre de Kena'ân.
42:6	Yossef était le dominateur de la terre, et c'était lui qui vendait le blé à tous les peuples de la Terre. Les frères de Yossef vinrent et se prosternèrent<!--Ge. 37:6-10.--> devant lui, le visage contre terre.
42:7	Yossef vit ses frères et les reconnut, mais il feignit d'être un étranger pour eux, et il leur parla durement, en leur disant : D'où venez-vous ? Et ils dirent : De la terre de Kena'ân, pour acheter des céréales.
42:8	Yossef reconnut ses frères, mais eux ne le reconnurent pas.
42:9	Yossef se souvint des rêves qu’il avait rêvés à leur sujet<!--Ge. 37.-->. Il leur dit : Vous êtes des espions, vous êtes venus pour observer les lieux faibles de la terre.
42:10	Ils lui dirent : Non, mon seigneur, tes serviteurs sont venus pour acheter des céréales.
42:11	Nous sommes tous fils d’un seul homme, nous sommes sincères ! Tes serviteurs ne sont pas des espions.
42:12	Et il leur dit : Non ! Vous êtes venus pour inspecter la nudité<!--« Exposé », « non défendu », « honte », « organes génitaux féminins ».--> de la terre.
42:13	Ils dirent : Nous, tes serviteurs, étions douze frères, fils d'un même homme, en terre de Kena'ân. Et voici, le plus jeune est aujourd'hui avec notre père, et l'un n'est plus.
42:14	Yossef leur dit : C'est ce que je vous disais, vous êtes des espions !
42:15	Voici comment vous serez éprouvés : Par la vie de pharaon ! vous ne sortirez pas d'ici jusqu'à ce que votre jeune frère ne soit venu ici.
42:16	Envoyez l'un de vous et qu'il amène votre frère pendant que vous, vous resterez prisonniers. Vos paroles seront éprouvées et je saurai si vous avez dit la vérité. Autrement, par la vie de pharaon, vous êtes des espions !
42:17	Et il les mit tous ensemble en prison pendant trois jours.
42:18	Le troisième jour, Yossef leur dit : Faites ceci et vous vivrez. Je crains Elohîm !
42:19	Si vous êtes sincères, que l'un de vos frères reste en prison dans la maison d'arrêt. Et vous, partez et emportez du blé pour la famine de vos maisons.
42:20	Puis amenez-moi votre jeune frère, afin que vos paroles soient éprouvées et vous ne mourrez pas. Et ils firent ainsi.
42:21	Chaque homme dit à son frère : Nous sommes vraiment coupables à l'égard de notre frère. Car nous avons vu l'angoisse de son âme quand il nous demandait grâce et nous ne l'avons pas écouté. C'est pour cela que cette détresse nous est arrivée.
42:22	Reouben leur répondit, en disant : Ne vous ai-je pas parlé, en disant : Ne péchez pas contre l’enfant ? Mais vous n'avez pas écouté, et voici que son sang nous est redemandé.
42:23	Ils ne savaient pas que Yossef les comprenait, parce qu'il se servait d'un interprète pour leur parler.
42:24	Il s'éloigna d'eux pour pleurer. Et il revint et leur parla, puis il prit parmi eux Shim’ôn et le fit enchaîner sous leurs yeux.
42:25	Et Yossef ordonna qu'on remplisse leurs sacs de blé, qu'on remette l'argent de chacun d'eux dans son sac, et qu'on leur donne de la provision pour la route. C'est ainsi qu'il agit envers eux.
42:26	Ils chargèrent leur blé sur leurs ânes et s'en allèrent.
42:27	L'un d'eux ouvrit son sac pour donner du fourrage à son âne dans le lieu de logement, et il vit son argent qui était à l'ouverture de son sac.
42:28	Il dit à ses frères : Mon argent m'a été rendu et le voici dans mon sac. Alors leur cœur fut en défaillance, ils furent saisis de peur et se dirent chaque homme à son frère : Qu'est-ce qu'Elohîm nous a fait ?
42:29	Étant arrivés vers Yaacov, leur père, en terre de Kena'ân, ils lui racontèrent toutes les choses qui leur étaient arrivées, en disant :
42:30	Cet homme, le seigneur de la terre, nous a parlé durement et nous a pris pour des espions de la terre.
42:31	Nous lui avons dit : Nous sommes sincères, nous ne sommes pas des espions.
42:32	Nous étions douze frères, fils de notre père. L'un n'est plus, et le plus jeune est aujourd'hui avec notre père en terre de Kena'ân.
42:33	Et cet homme, le seigneur de la terre, nous a dit : Voici comment je saurai si vous êtes sincères : Laissez-moi l'un de vos frères et prenez quelque chose pour la famine de vos maisons et partez.
42:34	Faites venir votre jeune frère vers moi. Je saurai que vous n'êtes pas des espions mais que vous êtes sincères. Je vous rendrai votre frère et vous irez faire du commerce sur la terre.
42:35	Il arriva que comme ils vidaient leurs sacs, voici, le paquet d'argent de chacun était dans son sac. Ils virent, eux et leur père, leurs paquets d'argent, et ils eurent peur.
42:36	Yaacov leur père leur dit : Vous me privez de mes enfants ! Yossef n'est plus, et Shim’ôn n'est plus, et vous prendriez Benyamin ! C'est sur moi que tout cela tombe.
42:37	Reouben parla à son père et lui dit : Fais mourir deux de mes fils si je ne te ramène pas Benyamin ! Donne-le moi en main, moi, je te le retournerai. 
42:38	Yaacov dit : Mon fils ne descendra pas avec vous, car son frère est mort et il reste seul. S'il lui arrivait un malheur sur la route où vous irez, vous feriez descendre mes cheveux gris avec douleur dans le shéol.

## Chapitre 43

43:1	Or la famine devint très grande sur la terre.
43:2	Et il arriva, lorsqu'ils eurent fini de manger le blé qu'ils avaient fait venir d'Égypte, que leur père leur dit : Retournez, achetez-nous un peu de céréales.
43:3	Yéhouda lui répondit et lui dit : Cet homme nous a répété, il nous a répété, en disant : Vous ne verrez pas mes faces, à moins que votre frère ne soit avec vous.
43:4	Si tu envoies notre frère avec nous, nous descendrons et nous t'achèterons des céréales.
43:5	Mais si tu ne l'envoies pas, nous n'y descendrons pas. Car cet homme nous a dit : Vous ne verrez pas mes faces, à moins que votre frère ne soit avec vous.
43:6	Et Israël dit : Pourquoi avez-vous mal agi à mon égard en disant à cet homme que vous aviez encore un frère ?
43:7	Ils dirent : Cet homme nous a interrogés sur nous et sur notre famille, en disant : Votre père vit-il encore ? N'avez-vous pas de frère ? Et nous lui avons déclaré selon ce qu'il nous avait demandé. Pouvions-nous savoir qu'il dirait : Faites descendre votre frère ?
43:8	Yéhouda dit à Israël, son père : Laisse venir le garçon avec moi, afin que nous nous levions et que nous partions. Nous vivrons et nous ne mourrons pas, nous, toi et nos enfants.
43:9	C’est moi qui réponds de lui, tu le demanderas de ma main. Si je ne le fais pas venir à toi et ne le mets pas en face de toi, je serai coupable envers toi, tous les jours.
43:10	Car si nous n'avions pas tardé, certainement nous serions déjà de retour deux fois.
43:11	Israël, leur père, leur dit : S’il en est ainsi, faites donc ceci : prenez dans vos vases les meilleures productions de la terre et portez-les en offrande à cet homme : un peu de baume, un peu de miel, des épices, de la myrrhe, des noix de pistaches et des amandes.
43:12	Prenez en mains le double de l’argent, l’argent retourné sur l'ouverture de vos sacs, retournez-le de vos mains. Peut-être était-ce une erreur.
43:13	Prenez votre frère et levez-vous, retournez vers cet homme.
43:14	El Shaddaï vous donnera des matrices en face de cet homme. Il laissera aller votre autre frère et Benyamin. Quant à moi, privé d’enfants, je suis privé d’enfants !
43:15	Ces hommes prirent le présent. Ils prirent aussi en leur main le double d’argent, ainsi que Benyamin. Puis ils se levèrent, descendirent en Égypte et se présentèrent en face de Yossef.
43:16	Dès que Yossef vit Benyamin avec eux, il dit à l’homme qui était au-dessus de sa maison : Fais entrer ces hommes dans la maison, tue une bête et prépare-la, car ces hommes mangeront avec moi à midi.
43:17	Cet homme fit ce que Yossef lui avait dit. L'homme conduisit ces hommes dans la maison de Yossef.
43:18	Ils eurent peur lorsqu'ils furent conduits dans la maison de Yossef, et ils dirent : Nous sommes emmenés à cause de l'argent remis l'autre fois dans nos sacs. C'est pour se jeter sur nous, se précipiter sur nous. C'est pour nous prendre comme esclaves et s'emparer de nos ânes.
43:19	Ils s'approchèrent vers l’homme qui était au-dessus de la maison de Yossef et lui parlèrent à l'entrée de la maison,
43:20	en disant : Excuse-nous, mon seigneur, nous sommes déjà descendus une fois pour acheter des céréales.
43:21	Et il est arrivé que, lorsque nous sommes venus au lieu de logement et que nous avons voulu ouvrir nos sacs, voici, l’argent de chacun était à l'entrée de son sac, notre argent selon son poids, et nous l’avons rapporté dans notre main.
43:22	Nous descendons avec un autre argent en nos mains pour acheter des céréales. Nous ne savons pas qui a remis notre argent dans nos sacs.
43:23	L'intendant leur dit : Shalôm à vous ! N'ayez pas peur ! c'est votre Elohîm, l'Elohîm de votre père qui vous a donné un trésor dans vos sacs. Votre argent est parvenu jusqu'à moi. Et il leur amena Shim’ôn.
43:24	Cet homme les fit entrer dans la maison de Yossef. Il leur donna de l'eau et ils lavèrent leurs pieds ; il donna aussi à manger à leurs ânes.
43:25	Ils préparèrent leur présent jusqu’à la venue de Yossef à midi, car ils avaient appris qu'ils mangeraient là le pain.
43:26	Et Yossef vint à la maison, et ils lui firent venir dans la maison le présent qu’ils avaient entre les mains et se prosternèrent<!--Ge. 37:5-10.--> à terre devant lui.
43:27	Il les questionna sur la paix et leur dit : Est-il en paix votre vieux père dont vous aviez parlé ? Est-il encore vivant ?
43:28	Ils dirent : Ton serviteur, notre père, est en paix, il vit encore. Et ils s'inclinèrent et se prosternèrent.
43:29	Yossef leva les yeux, il vit Benyamin, son frère, fils de sa mère, et il dit : Est-ce là votre jeune frère dont vous m'avez parlé ? Et il ajouta : Mon fils, qu'Elohîm te fasse grâce !
43:30	Yossef se hâta, car ses matrices étaient brûlantes pour son frère et il cherchait où pleurer. Il entra dans sa chambre et il y pleura.
43:31	S’étant lavé les faces, il sortit et, se contenant, il dit : Mettez le pain !
43:32	Ils le lui mirent pour lui à part, pour eux à part et pour les Égyptiens qui mangeaient avec lui, à part, car les Égyptiens ne pouvaient pas manger du pain avec les Hébreux, parce que c'est une abomination pour les Égyptiens.
43:33	Les frères de Yossef s'assirent en face de lui, le premier-né selon son droit d'aînesse, et le plus jeune selon sa jeunesse. Ces hommes s’étonnaient, l’homme avec son compagnon.
43:34	On leur fit porter des portions de ses faces. La portion de Benyamin était cinq fois plus grande que les parts de tous les autres. Ils burent et s'enivrèrent avec lui.

## Chapitre 44

44:1	Il donna cet ordre à celui qui était au-dessus de sa maison, en disant : Remplis de céréales les sacs de ces hommes, autant qu'ils en pourront porter, et remets l'argent de chaque homme à la bouche de son sac.
44:2	Tu mettras aussi ma coupe, la coupe d'argent, à l'entrée du sac du plus petit avec l'argent de son blé. Et il fit comme Yossef lui avait dit.
44:3	Le matin, dès que la lumière parut, on renvoya ces hommes avec leurs ânes.
44:4	Ils étaient sortis de la ville et n'en étaient pas loin lorsque Yossef dit à celui qui était au-dessus de sa maison : Lève-toi ! Poursuis ces hommes, rattrape-les et dis-leur : Pourquoi avez-vous rendu le mal pour le bien ?
44:5	N’est-ce pas celle dans laquelle mon seigneur boit et par laquelle il pratique la divination, il pratique la divination ? Vous avez mal fait d'agir ainsi.
44:6	Il les rattrapa et leur dit ces paroles.
44:7	Ils lui dirent : Pourquoi mon seigneur parle-t-il ainsi ? Loin de tes serviteurs de faire une chose pareille !
44:8	Voici, nous t'avons rapporté de la terre de Kena'ân l'argent que nous avions trouvé à la bouche de nos sacs, et comment aurions-nous dérobé de l'argent ou de l'or de la maison de ton seigneur ?
44:9	Parmi tes serviteurs, qui sera trouvé avec mourra ! Et nous-mêmes, nous deviendrons esclaves de mon seigneur !
44:10	Il leur dit : Qu'il soit fait maintenant selon vos paroles ! Qu'il en soit ainsi ! Qui sera trouvé avec devienne mon esclave, et vous, vous serez innocents.
44:11	Et ils se hâtèrent de déposer chacun son sac à terre, et chacun ouvrit son sac.
44:12	Il les fouilla, en commençant par le plus grand et finissant par le plus petit. Et la coupe fut trouvée dans le sac de Benyamin.
44:13	Ils déchirèrent leurs vêtements. Chacun rechargea son âne et ils retournèrent à la ville.
44:14	Yéhouda et ses frères arrivèrent à la maison de Yossef, qui était encore là, et ils se jetèrent à terre devant lui.
44:15	Yossef leur dit : Quelle action avez-vous faite ? Ne savez-vous pas qu'un homme tel que moi pratique la divination, pratique la divination ?
44:16	Yéhouda lui dit : Que dirons-nous à mon seigneur ? Comment parlerons-nous ? Et comment nous justifierons-nous ? Elohîm a trouvé l'iniquité de tes serviteurs ; voici, nous sommes esclaves de mon seigneur, nous et celui entre les mains de qui la coupe a été trouvée.
44:17	Mais il dit : Loin de moi d'agir ainsi ! L'homme dans la main duquel la coupe a été trouvée sera mon esclave. Mais vous, montez en paix vers votre père.
44:18	Yéhouda s'approcha de lui, en disant : Excuse-moi, mon seigneur ! Je te prie, que ton serviteur dise un mot, s’il te plaît, aux oreilles de mon seigneur, et que tes narines ne s'enflamment pas contre ton serviteur, car tu es comme pharaon.
44:19	Mon seigneur interrogea ses serviteurs, en disant : Avez-vous un père ou un frère ?
44:20	Nous avons dit à mon seigneur : Nous avons un vieux père et l'enfant qu'il a eu dans sa vieillesse est encore jeune. Son frère est mort, il est resté le seul de sa mère et son père l'aime.
44:21	Tu as dit à tes serviteurs : Faites-le descendre vers moi et que je le voie de mes yeux.
44:22	Nous avons dit à mon seigneur : Ce garçon ne peut quitter son père, car s'il le quitte, son père mourra.
44:23	Tu as dit à tes serviteurs : Si votre petit frère ne descend avec vous, vous ne verrez plus mes faces.
44:24	Il est arrivé, quand nous sommes montés vers ton serviteur, mon père, que nous lui avons rapporté les paroles de mon seigneur.
44:25	Notre père nous a dit : Retournez et achetez-nous un peu de céréales.
44:26	Nous lui avons dit : Nous ne pouvons pas descendre. Mais si notre petit frère est avec nous, nous descendrons, car nous ne pouvons pas voir les faces de cet homme, à moins que notre jeune frère ne soit avec nous.
44:27	Ton serviteur, mon père, nous a dit : Vous savez que ma femme m'a enfanté deux fils.
44:28	L'un étant sorti de chez moi, et j'ai dit : il a été déchiré, déchiré, car je ne l'ai pas vu jusqu’ici !
44:29	Si vous enleviez encore celui-ci loin de mes faces et qu'il lui arrive un malheur, vous ferez descendre mes cheveux gris avec douleur dans le shéol.
44:30	Et maintenant, quand je viendrai vers ton serviteur, mon père, le garçon n’étant pas avec nous, lui à l’âme duquel son âme est attachée,
44:31	il arrivera que, dès qu'il verra que le garçon n'y est pas, il mourra. Et tes serviteurs feront descendre avec douleur dans le shéol les cheveux gris de ton serviteur notre père.
44:32	Car moi, ton serviteur, je me suis porté garant pour le garçon devant mon père en disant : Si je ne te le ramène pas, je serai pour toujours coupable envers mon père.
44:33	Maintenant, s’il te plaît, que ton serviteur reste à la place du garçon, en serviteur de mon seigneur et que le garçon monte avec ses frères.
44:34	Car comment monterai-je vers mon père le garçon n’étant pas avec moi ? Que je ne voie pas le mal qui atteindrait mon père !

## Chapitre 45

45:1	Yossef ne pouvant plus se contenir devant tous ceux qui se tenaient près de lui, cria : Sortez ! Tout homme loin de moi ! Et pas un homme ne se tenait près de lui quand Yossef se fit connaître à ses frères.
45:2	Il éleva la voix en pleurant, et les Égyptiens l'entendirent, et la maison du pharaon l'entendit.
45:3	Et Yossef dit à ses frères : Je suis Yossef ! Mon père vit-il encore ? Mais ses frères ne pouvaient lui répondre, car ils étaient terrifiés en face de lui.
45:4	Yossef dit encore à ses frères : S’il vous plaît, approchez-vous de moi. Ils s'approchèrent, et il leur dit : Je suis Yossef, votre frère, que vous avez vendu pour être mené en Égypte<!--Ac. 7:13.-->.
45:5	Mais maintenant ne vous affligez pas, que cela ne brûle pas vos yeux de m’avoir vendu ici, car Elohîm m'a envoyé devant vous pour la préservation de la vie.
45:6	Car voici, il y a déjà 2 ans que la famine est sur la Terre, et il y aura encore 5 ans pendant lesquels il n'y aura ni labour ni moisson.
45:7	Mais Elohîm m'a envoyé devant vous pour vous assurer un reste sur la Terre et vous faire vivre par une grande délivrance.
45:8	Maintenant ce n'est pas vous qui m'avez envoyé ici, mais l'Elohîm. Il m'a établi père de pharaon, seigneur sur toute sa maison et gouverneur de toute la terre d'Égypte.
45:9	Hâtez-vous de monter vers mon père et dites-lui : Ainsi a dit ton fils Yossef : Elohîm m'a établi seigneur sur toute l'Égypte, descends vers moi, ne t'arrête pas !
45:10	Tu habiteras dans la région de Goshen, et tu seras près de moi avec tes fils et les fils de tes fils, tes brebis et tes bœufs, ainsi que tout ce qui est à toi.
45:11	Là, je te nourrirai, car il y aura encore 5 années de famine. Ainsi tu ne tomberas pas dans la pauvreté, toi, ta maison et tout ce qui est à toi.
45:12	Et voici, vous voyez de vos yeux, et Benyamin mon frère voit aussi de ses yeux, que c'est moi qui vous parle de ma propre bouche.
45:13	Rapportez à mon père quelle est ma gloire en Égypte et tout ce que vous avez vu. Hâtez-vous et faites descendre ici mon père.
45:14	Il se jeta au cou de Benyamin, son frère, et pleura. Benyamin pleura aussi sur son cou.
45:15	Il embrassa tous ses frères en pleurant. Après cela, ses frères parlèrent avec lui.
45:16	Le bruit se répandit dans la maison de pharaon que les frères de Yossef étaient venus : cela fut bon aux yeux du pharaon et aux yeux de ses serviteurs.
45:17	Pharaon dit à Yossef : Dis à tes frères : Faites ceci : chargez vos bêtes et allez, retournez vers la terre de Kena'ân,
45:18	et prenez votre père et vos familles et revenez vers moi. Je vous donnerai le meilleur de la terre d'Égypte, et vous mangerez la graisse de la terre.
45:19	Et toi, donne-leur cet ordre : Faites ceci : prenez avec vous de la terre d'Égypte, des chariots pour vos plus petits enfants et pour vos femmes. Amenez votre père et venez.
45:20	Ne regrettez pas vos objets, car le meilleur de toute la terre d'Égypte sera à vous.
45:21	Les fils d'Israël firent ainsi. Yossef leur donna des chariots, de la bouche de pharaon ; il leur donna aussi de la provision pour le chemin.
45:22	Il donna à chacun d’eux tous des vêtements de rechange, et il donna à Benyamin 300 pièces d'argent et 5 vêtements de rechange.
45:23	Il envoya aussi à son père 10 ânes transportant des bonnes choses d’Égypte et 10 ânesses transportant du blé, du pain et de la nourriture pour son père, pour la route.
45:24	Il renvoya ses frères et ils partirent. Il leur dit : Ne vous querellez pas en chemin.
45:25	Ils montèrent d'Égypte et vinrent vers Yaacov leur père, en terre de Kena'ân.
45:26	Et ils lui racontèrent, disant : Yossef est encore vivant ! Oui, c'est lui qui gouverne toute la terre d'Égypte ! Mais son cœur s'engourdit, car il ne les croyait pas.
45:27	Ils lui dirent toutes les paroles que Yossef leur avait dites, et lorsqu'il vit les chariots que Yossef avait envoyés pour le transporter, alors l'esprit de Yaacov leur père reprit vie.
45:28	Israël dit : C'est assez ! Yossef, mon fils, est encore vivant ! J'irai et je le verrai avant de mourir !

## Chapitre 46

46:1	Israël partit avec tout ce qui lui appartenait. Arrivé à Beer-Shéba, il sacrifia des sacrifices à l'Elohîm de son père Yitzhak.
46:2	Elohîm parla à Israël dans les visions de la nuit, en disant : Yaacov, Yaacov ! Et il dit : Me voici !
46:3	Il lui dit : Je suis le El, l'Elohîm de ton père. N'aie pas peur de descendre en Égypte, je t'y ferai devenir une grande nation.
46:4	Je descendrai avec toi en Égypte et je t'en ferai moi-même monter, monter. C'est Yossef qui posera sa main sur tes yeux.
46:5	Yaacov se leva de Beer-Shéba, et les fils d'Israël mirent Yaacov leur père, leurs petits enfants et leurs femmes sur les chariots que pharaon avait envoyés pour les porter.
46:6	Ils prirent aussi leur bétail et leurs biens qu'ils avaient réunis en terre de Kena'ân. Yaacov et toute sa famille avec lui vinrent en Égypte.
46:7	Ses fils, les fils de ses fils avec lui, ses filles, les filles de ses fils, toute sa postérité, il les fit venir avec lui en Égypte.
46:8	Voici les noms des fils d'Israël qui vinrent en Égypte : Yaacov et ses fils. Le premier-né de Yaacov fut Reouben.
46:9	Les fils de Reouben : Hanowk, Pallou, Hetsron et Karmiy.
46:10	Les fils de Shim’ôn : Yemouel, Yamin, Ohad, Yakiyn, Tsochar et Shaoul, fils d'une Kena'ânéenne.
46:11	Les fils de Lévi : Guershon, Qehath et Merari.
46:12	Les fils de Yéhouda : Er, Onan, Shélah, Pérets et Zérach. Mais Er et Onan moururent en terre de Kena'ân. Les fils de Pérets furent Hetsron et Hamoul.
46:13	Les fils de Yissakar : Thola, Pouvah, Yob et Shimron.
46:14	Les fils de Zebouloun : Séred, Eylon et Yahleel.
46:15	Ce sont là les fils de Léah qu'elle enfanta à Yaacov à Paddan-Aram, avec Diynah sa fille. Ses fils et ses filles formaient en tout 33 personnes.
46:16	Les fils de Gad : Tsiphyon, Haggi, Shouni, Etsbon, Éri, Arodi et Areéli.
46:17	Les fils d'Asher : Yimnah, Yishvah, Yishviy, Beriy`ah et Sérach, leur sœur. Les fils de Beriy`ah : Héber et Malkiyel.
46:18	Ce sont là les fils de Zilpah que Laban donna à Léah, sa fille ; elle les enfanta à Yaacov : 16 personnes en tout.
46:19	Les fils de Rachel, femme de Yaacov : furent Yossef et Benyamin.
46:20	Et il naquit à Yossef en terre d'Égypte, Menashè et Éphraïm, qu'Asnath, fille de Poti-Phéra, prêtre d'On, lui enfanta.
46:21	Les fils de Benyamin étaient Béla, Béker, Ashbel, Guéra, Naaman, Éhi, Rosh, Mouppim, Houppim et Ard.
46:22	Ce sont là les fils de Rachel qu'elle enfanta à Yaacov. En tout 14 personnes.
46:23	Les fils de Dan : Houshim.
46:24	Les fils de Nephthali : Yahtseel, Gouni, Yetser et Shillem.
46:25	Ce sont là les fils de Bilhah que Laban donna à Rachel, sa fille, et elle les enfanta à Yaacov. En tout 7 personnes.
46:26	Toutes les personnes appartenant à Yaacov qui vinrent en Égypte et qui étaient issues de lui, sans les femmes des fils de Yaacov, furent en tout 66.
46:27	Les fils de Yossef qui lui étaient nés en Égypte furent 2 personnes. Toutes les personnes de la maison de Yaacov qui vinrent en Égypte furent 70.
46:28	Il envoya Yéhouda devant lui vers Yossef pour le faire venir devant lui en Goshen. Ils vinrent en terre de Goshen.
46:29	Et Yossef fit atteler son char et y monta pour aller à la rencontre d'Israël, son père, en Goshen. Dès qu'il le vit, il se jeta à son cou et pleura longtemps sur son cou.
46:30	Israël dit à Yossef : Je peux mourir maintenant, puisque j'ai vu ton visage et que tu es encore en vie !
46:31	Yossef dit à ses frères et à la famille de son père : Je monterai pour informer pharaon, et je lui dirai : Mes frères et la famille de mon père qui étaient en terre de Kena'ân sont venus vers moi.
46:32	Ces hommes sont des bergers : oui, ce sont des hommes à bétail. Ils ont fait venir leurs brebis, leurs bœufs et tout ce qui était à eux.
46:33	Il arrivera que, quand pharaon vous appellera et dira : Quel est votre métier ?
46:34	Vous direz : Tes serviteurs sont des hommes à bétail, depuis notre jeunesse jusqu’à maintenant, nous et nos pères ; afin que vous habitiez en terre de Goshen, car les Égyptiens ont en abomination les bergers.

## Chapitre 47

47:1	Yossef alla avertir pharaon et lui dit : Mon père et mes frères sont arrivés de la terre de Kena'ân avec leurs troupeaux et leurs bœufs, et tout ce qui est à eux, et voici, ils sont en terre de Goshen.
47:2	Il prit cinq de ses frères et les mit en face de pharaon.
47:3	Pharaon dit aux frères de Yossef : Quelle est votre occupation ? Ils dirent à pharaon : Tes serviteurs sont bergers comme nos pères.
47:4	Ils dirent à pharaon : Nous sommes venus séjourner sur cette terre parce qu'il n'y a plus de pâturages pour les troupeaux de tes serviteurs. En effet, la famine pèse lourdement sur la terre de Kena'ân. Maintenant, s'il te plaît, que tes serviteurs demeurent en terre de Goshen.
47:5	Pharaon parla à Yossef et lui dit : Ton père et tes frères sont arrivés auprès de toi.
47:6	La terre d'Égypte est devant toi. Fais habiter ton père et tes frères dans le meilleur endroit de la terre. Qu'ils demeurent en terre de Goshen. Si tu connais parmi eux des hommes habiles, tu les établiras chefs de tous mes troupeaux.
47:7	Yossef fit venir Yaacov son père et le fit se tenir debout en face de pharaon. Yaacov bénit pharaon.
47:8	Pharaon dit à Yaacov : Quel est le nombre de jours des années de tes vies ?
47:9	Yaacov dit à pharaon : Les jours des années de mes pèlerinages sont de 130 ans. Les jours des années de ma vie ont été courts et mauvais et n'ont pas atteint les jours des années de la vie de mes pères, du temps de leurs pèlerinages.
47:10	Yaacov bénit pharaon et sortit loin des faces de pharaon.
47:11	Yossef fit habiter son père et ses frères en leur donnant une propriété en terre d'Égypte, au meilleur endroit de la terre, en terre de Ramsès, comme pharaon l'avait ordonné.
47:12	Yossef nourrit son père, ses frères et toute la maison de son père de pain, selon le nombre des bouches de leurs enfants.
47:13	Or il n'y avait pas de pain sur toute la Terre, car la famine était très grande. La terre d'Égypte et la terre de Kena'ân languissaient face à la famine.
47:14	Yossef recueillit tout l'argent qui se trouvait en terre d'Égypte et en terre de Kena'ân contre le blé qu'on achetait, et il apporta l'argent à la maison de pharaon.
47:15	Quand l'argent de la terre d'Égypte et de la terre de Kena'ân fut épuisé, tous les Égyptiens vinrent auprès de Yossef, en disant : Donne-nous du pain ! Faut-il que nous mourions devant toi parce que l'argent manque ?
47:16	Yossef dit : Donnez votre bétail et je vous en donnerai pour votre bétail, puisque l'argent manque.
47:17	Ils firent venir à Yossef leur bétail et Yossef leur donna du pain contre les chevaux, contre les troupeaux de brebis, contre les troupeaux de bœufs et contre les ânes. Ainsi il leur fournit du pain contre leurs troupeaux cette année-là.
47:18	Cette année étant finie, ils vinrent à lui la seconde année et lui dirent : Nous ne le cacherons pas à mon seigneur, mais l’argent est fini, et les troupeaux de bétail sont à mon seigneur. Il ne reste rien devant mon seigneur excepté nos corps et notre sol.
47:19	Pourquoi mourrions-nous sous tes yeux, nous et nos sols ? Achète-nous avec nos sols contre du pain et nous serons esclaves de pharaon, et nos sols seront à lui. Donne-nous aussi de quoi semer afin que nous vivions et ne mourrions pas, et que nos sols ne soient pas ruinés.
47:20	Yossef acheta tous les sols de l'Égypte pour pharaon. En effet, les Égyptiens vendirent chacun son champ parce que la famine les pressait. Et la terre devint la propriété de pharaon.
47:21	Et il fit passer le peuple dans les villes, d'un bout à l'autre des frontières de l'Égypte.
47:22	Seulement, il n'acheta pas les sols des prêtres, parce qu'il y avait un décret de pharaon en faveur des prêtres. Ils mangeaient la portion prescrite que pharaon leur donnait. C'est pourquoi ils ne vendirent pas leurs sols.
47:23	Et Yossef dit au peuple : Voici, je vous ai achetés aujourd'hui, vous et vos sols pour pharaon, voilà de la semence pour ensemencer le sol.
47:24	Quand sera apparu son produit, vous donnerez un cinquième à pharaon, mais quatre parts seront à vous, comme semence pour les champs, comme nourriture pour vous et pour ceux qui sont dans vos maisons, et pour qu’en mangent vos petits enfants.
47:25	Ils dirent : Tu nous sauves la vie ! Que nous trouvions grâce aux yeux de mon seigneur et nous serons esclaves de pharaon.
47:26	Et Yossef en fit un décret jusqu'à ce jour : à pharaon, le cinquième des terres de l'Égypte. Il n'y eut que les sols des prêtres qui ne furent pas à pharaon.
47:27	Israël habita en terre d'Égypte, en terre de Goshen. Ils eurent des possessions, ils portèrent du fruit et multiplièrent beaucoup.
47:28	Yaacov vécut 17 ans en terre d'Égypte. Les jours des années de la vie de Yaacov furent de 147 ans.
47:29	Quand les jours d'Israël approchèrent de la mort, il appela Yossef son fils et lui dit : S’il te plaît, si j'ai trouvé grâce à tes yeux, mets ta main sous ma cuisse et use s’il te plaît envers moi de bonté et de fidélité : S’il te plaît, ne m'enterre pas en Égypte !
47:30	Quand je serai couché avec mes pères, tu me transporteras hors de l'Égypte et m'enterreras dans leur sépulcre. Il dit : Je ferai selon ta parole.
47:31	Il lui dit : Jure-le-moi ! Il le lui jura, et Israël se prosterna à la tête du lit.

## Chapitre 48

48:1	Il arriva après ces choses que l'on vint dire à Yossef : Voici, ton père est malade ! Et il prit avec lui ses deux fils, Menashè et Éphraïm.
48:2	On informa Yaacov en disant : Voici Yossef ton fils qui vient vers toi. Israël se fortifia et s'assit sur son lit.
48:3	Yaacov dit à Yossef : El Shaddaï m'est apparu à Louz, en terre de Kena'ân et m'a béni.
48:4	Il m'a dit : Voici, je te ferai porter du fruit et je te multiplierai, je te ferai devenir une assemblée de peuples et je donnerai cette terre en propriété perpétuelle à ta postérité après toi.
48:5	Maintenant, tes deux fils qui te sont nés en terre d'Égypte avant mon arrivée vers toi seront à moi : Éphraïm et Menashè seront à moi comme Reouben et Shim’ôn.
48:6	Mais les enfants que tu auras engendrés après eux, seront à toi, ils seront appelés selon le nom de leurs frères dans leur héritage.
48:7	À mon retour de Paddan, Rachel mourut en route auprès de moi, en terre de Kena'ân, à quelque distance d'Éphrata. C'est là que je l'ai enterrée, sur le chemin d'Éphrata, qui est Bethléhem.
48:8	Israël vit les fils de Yossef et il dit : Ceux-ci, qui sont-ils ?
48:9	Et Yossef dit à son père : Ce sont mes fils qu'Elohîm m'a donnés ici. Et il dit : Amène-les-moi, s’il te plaît, afin que je les bénisse.
48:10	Or les yeux d'Israël étaient appesantis par la vieillesse et il ne pouvait plus voir. Et il les fit approcher de lui, les embrassa et les prit dans ses bras.
48:11	Israël dit à Yossef : Je n'intercédais pas pour voir tes faces et voici qu'Elohîm m’a fait voir même ta postérité !
48:12	Et Yossef les retira de ses genoux et se prosterna le visage contre terre.
48:13	Yossef les prit tous les deux, Éphraïm de sa main droite à la gauche d'Israël, et Menashè de sa main gauche à la droite d'Israël, et il les fit approcher de lui.
48:14	Israël étendit sa main droite et la posa sur la tête d'Éphraïm qui était le plus jeune, et il posa sa main gauche sur la tête de Menashè. Il croisa ses mains, car Menashè était le premier-né.
48:15	Il bénit Yossef et dit : Que l'Elohîm en face de qui ont marché mes pères Abraham et Yitzhak, que l'Elohîm qui a été mon berger depuis que j'existe jusqu'à ce jour<!--Hé. 11:21.-->,
48:16	que l'Ange qui m'a racheté de tout mal, bénisse ces garçons ! Qu'ils soient appelés de mon nom et du nom de mes pères, Abraham et Yitzhak, et qu'ils se multiplient en abondance comme les poissons au milieu de la terre.
48:17	Yossef vit que son père avait posé sa main droite sur la tête d'Éphraïm et cela fut mauvais à ses yeux. Il saisit la main de son père pour la détourner de la tête d'Éphraïm vers la tête de Menashè.
48:18	Et Yossef dit à son père : Ce n'est pas juste, mon père, car celui-ci est l'aîné. Mets ta main droite sur sa tête !
48:19	Mais son père le refusa, en disant : Je le sais, mon fils, je le sais. Lui aussi deviendra un peuple, lui aussi sera grand. Toutefois, son frère qui est plus jeune, sera plus grand que lui et sa postérité deviendra une multitude de nations.
48:20	Il les bénit ce jour-là et dit : C'est par toi qu'Israël bénira en disant : Qu'Elohîm t'établisse comme Éphraïm et comme Menashè ! Et il mit Éphraïm avant Menashè.
48:21	Israël dit à Yossef : Voici, je vais mourir, mais Elohîm sera avec vous et vous fera retourner vers la terre de vos pères.
48:22	Je te donne une épaule de plus qu'à tes frères, celle que j'ai prise de la main de l'Amoréen, avec mon épée et mon arc.

## Chapitre 49

49:1	Yaacov appela ses fils et leur dit : Rassemblez-vous et je vous annoncerai ce qui vous arrivera dans les derniers<!--L'adjectif « derniers » vient de l'hébreu « achariyth ». Son équivalent grec est « eschatos » : « dernier », « extrémité ». Yaacov est le premier homme à avoir utilisé l'expression « derniers jours ». Cette promesse de Yaacov devait arriver à Israël dans les derniers jours, selon leurs tribus. Ainsi, les promesses du droit d'aînesse de Ge. 49 étaient pour l'âge messianique, lequel est associé aux derniers jours, et a commencé à la fête de la pentecôte (Ac. 2:14-21). Ces jours impliquent :\\- L'effusion de l'Esprit, le réveil de l'Assemblée (Église) du Mashiah (Mt. 25:1-13 ; Ac. 2).\\- Le réveil des faux prophètes ou l'apostasie (2 Pi. 3:3 ; 1 Jn. 2).\\- La dégradation de la moralité (2 Ti. 3).\\- Le fait qu'Elohîm nous parle par le Fils (Hé. 1:2).\\- La future résurrection des saints lors du retour du Mashiah (Jn. 6:39-54 ; 1 Th. 4:13-17).\\- Le temps des nations (fin des temps) s'achèvera lors du retour visible de Yéhoshoua ha Mashiah pour établir son règne sur toute la terre. Le temps des nations a commencé lorsque, à la suite de l'infidélité d'Israël, la gloire d'Elohîm a quitté le temple et la ville de Yeroushalaim (Ez. 11) ; la puissance fut confiée aux nations en la personne de Neboukadnetsar qui s'empara de Yeroushalaim (2 R. 24-25 ; Jé. 39 ; Da. 1 ; 2 Ch. 36:6-21). Ces temps dureront jusqu'à la destruction finale du dernier empire des nations représenté par la Bête romaine ressuscitée (Ap. 13:3). Cette destruction n'aura lieu que lorsque Yéhoshoua ha Mashiah, la Pierre détachée sans le secours d'aucune main, deviendra une grande montagne qui remplira toute la terre (Mi. 4 ; Da. 2:34). Yeroushalaim ne sera délivrée du joug des nations qu'à ce moment-là. Le temps des nations ne sera accompli que lorsque le trône d'Elohîm sera de nouveau établi à Yeroushalaim.--> jours.
49:2	Rassemblez-vous et écoutez, fils de Yaacov ! Écoutez Israël<!--Le verbe écouter vient de l'hébreu « shama ». C'est sur cette interpellation solennelle de Yaacov que se base le texte principal de la liturgie juive. Récité matin et soir avec des bénédictions, le « Shema » se compose de trois extraits de la torah : De. 6:4, 11:13-21 et No. 15:37-41. Véritable profession de foi, le « Shema » commence par affirmer et rappeler le fondement de la piété juive : Elohîm est un.-->, votre père !
49:3	Reouben, toi, mon premier-né, ma force et le premier de ma vigueur, l'excellence en dignité et l'excellence en force,
49:4	impétueux comme les eaux, tu n’excelleras pas, car tu es monté sur la couche de ton père et tu as souillé mon lit en y montant.
49:5	Shim’ôn et Lévi, sont frères, leurs glaives sont des instruments de violence.
49:6	À leur conseil secret tu ne viendras pas mon âme ! À leur assemblée tu ne t’uniras pas, ma gloire ! Car, dans leur narine, ils ont tué un homme, et pour leur plaisir, ils ont coupé les jarrets des bœufs.
49:7	Maudite soit leur narine, car elle a été violente, et leur fureur, car elle a été cruelle ! Je les diviserai dans Yaacov et les disperserai dans Israël.
49:8	Yéhouda, quant à toi, tes frères te loueront. Ta main sera sur la nuque de tes ennemis. Les fils de ton père se prosterneront devant toi.
49:9	Yéhouda est un jeune lion. Tu es monté d’auprès de la proie, mon fils ! Il s'est agenouillé, il s'est couché comme un lion, comme une lionne : Qui le fera lever ?
49:10	Le sceptre ne s'éloignera pas de Yéhouda, ni le bâton de législateur d'entre ses pieds, jusqu'à ce que vienne le Shiyloh<!--« Le Pacifique », « le Repos » ou « celui à qui c'est », « ce qui lui appartient ».-->. À lui, l’obéissance des peuples !
49:11	Il attache à la vigne son ânon, à la vigne de choix, le petit de son ânesse. Il lavera ses habits dans le vin et son vêtement dans le sang des raisins.
49:12	Il a les yeux brillants de vin et les dents blanches de lait.
49:13	Zebouloun habitera sur la côte des mers, il sera un port des navires et ses côtés seront vers Sidon.
49:14	Yissakar est un âne robuste, couché entre les barres des étables.
49:15	Il voit que le lieu où il repose est agréable et que la contrée est magnifique, il courbe son épaule sous le fardeau, il s'est réduit à la servitude.
49:16	Dan jugera son peuple, comme l'une des tribus d'Israël.
49:17	Dan deviendra un serpent sur le chemin, une vipère sur le sentier, mordant les talons du cheval, pour que le cavalier tombe à la renverse.
49:18	YHWH ! J'espère en ton salut<!--Le mot salut se dit « yeshuw'ah » en hébreu, terme qui signifie également « délivrance ». Avant de mourir, Yaacov a donc placé son espérance en Yéhoshoua ha Mashiah qui est la résurrection et la vie (Jn. 11:25). Voir commentaire en Es. 26:1.--> !
49:19	Quant à Gad, des troupes viendront l'attaquer, mais il ravagera leur arrière-garde.
49:20	D'Asher le pain sera excellent et il fournira les mets délicats des rois.
49:21	Nephtali est une biche en liberté. Il donne des paroles de bonté.
49:22	Yossef est le fils qui porte du fruit<!--Voir Jn. 15:1-6.-->, le fils qui porte du fruit près de la source<!--Es. 12:3, 55:1 ; Jé. 2:13, 17:13 ; Ps. 1:3, 87:7 ; Jn. 7:37 ; Ap. 21:6, 22:17.-->. Les filles enjambent le mur<!--Ou encore : les filles courent sur le mur pour voir Yossef.-->.
49:23	Ils le rendent amer et lui tirent dessus, ils le haïssent, les maîtres des flèches.
49:24	Mais son arc reste ferme, ses bras et ses mains sont rendus agiles, par les mains du Puissant de Yaacov. De là est le berger, la pierre d’Israël -
49:25	par le El de ton père qui t'aidera, avec Shaddaï qui te bénira des bénédictions des cieux en haut, des bénédictions de l'abîme qui repose en bas, des bénédictions des seins et de la matrice !
49:26	Les bénédictions de ton père sont plus fortes que les bénédictions de ceux qui m'ont engendré, jusqu'à la cime des antiques collines. Elles seront sur la tête de Yossef et sur le sommet de la tête du Nazaréen d'entre ses frères.
49:27	Benyamin est un loup qui déchirera. Le matin il dévorera la proie et sur le soir il partagera le butin.
49:28	Tous ceux-là sont les 12 tribus d’Israël. Et c'est là ce que leur père leur dit en les bénissant. Il les bénit, chaque homme selon sa propre bénédiction.
49:29	Il leur donna des ordres en leur disant : Je vais être recueilli auprès de mon peuple, enterrez-moi avec mes pères dans la caverne qui est au champ d'Éphron, le Héthien,
49:30	dans la caverne du champ de Makpelah, vis-à-vis de Mamré, en terre de Kena'ân. C'est le champ qu'Abraham a acheté à Éphron, le Héthien, comme propriété sépulcrale.
49:31	C'est là qu'on a enterré Abraham avec Sarah, sa femme, c'est là qu'on a enterré Yitzhak et Ribqah, sa femme, et c'est là que j'ai enterré Léah.
49:32	Le champ et la caverne qui s'y trouve ont été achetés aux fils de Heth.
49:33	Lorsque Yaacov eut achevé de donner ses ordres à ses fils, il retira ses pieds dans le lit, il expira et fut recueilli auprès de son peuple.

## Chapitre 50

50:1	Yossef se jeta sur les faces de son père, pleura sur lui et l'embrassa.
50:2	Yossef ordonna à ses serviteurs, les médecins, d'embaumer son père. Les médecins embaumèrent Israël.
50:3	Et 40 jours s’accomplirent pour lui, car ainsi s’accomplissaient les jours de l’embaumement. Les Égyptiens le pleurèrent 70 jours.
50:4	Quand les jours du deuil furent passés, Yossef parla à la maison de pharaon et dit : S’il vous plaît, si j'ai trouvé grâce à vos yeux, parlez, s’il vous plaît, aux oreilles de pharaon, en disant :
50:5	Mon père m'a fait jurer, en disant : Voici, je vais mourir ! Tu m'enterreras dans le sépulcre que je me suis acheté en terre de Kena'ân. Maintenant, s’il te plaît, que j'y monte et que j'y enterre mon père, puis je reviendrai.
50:6	Pharaon dit : Monte et enterre ton père comme il t'a fait jurer.
50:7	Yossef monta pour enterrer son père. Les serviteurs de pharaon, les anciens de la maison de pharaon et tous les anciens de la terre d'Égypte montèrent avec lui,
50:8	ainsi que toute la maison de Yossef, ses frères et la maison de son père. Ils ne laissèrent en terre de Goshen que les enfants, les brebis et les bœufs.
50:9	Avec lui montèrent aussi des chars et des cavaliers, en sorte que le camp devint très nombreux.
50:10	Arrivés à l'aire d'Athad qui est au-delà du Yarden. Là, ils se lamentèrent, une lamentation grande et très lourde. Il fit pour son père un deuil de 7 jours.
50:11	Et les Kena'ânéens, habitants de la terre, voyant ce deuil dans l'aire d'Athad, dirent : Ce deuil est grand pour les Égyptiens. C'est pourquoi on a appelé du nom d'Abel-Mitsraïm<!--Deuil de l'Égypte.--> cet endroit qui est au-delà du Yarden.
50:12	Ses fils firent donc ce qu'il leur avait ordonné.
50:13	Ils le transportèrent en terre de Kena'ân et l'enterrèrent dans la caverne du champ de Makpelah, qu'Abraham avait achetée d'Éphron, le Héthien, comme propriété sépulcrale, et qui est vis-à-vis de Mamré.
50:14	Et après que Yossef eut enseveli son père, il retourna en Égypte avec ses frères et tous ceux qui étaient montés avec lui pour ensevelir son père.
50:15	Et les frères de Yossef, voyant que leur père était mort, dirent : Peut-être que Yossef nous haïra, et nous rendra, nous rendra tout le mal que nous lui avons fait.
50:16	Ils firent parvenir un ordre à Yossef en disant : Ton père a donné cet ordre avant de mourir, en disant :
50:17	Vous parlerez ainsi à Yossef : Oh ! Pardonne, s'il te plaît, l'iniquité de tes frères et leur péché, car ils t'ont fait du mal. Maintenant, s’il te plaît, pardonne la transgression des serviteurs de l'Elohîm de ton père. Et Yossef pleura quand on lui parla.
50:18	Ses frères vinrent eux-mêmes se prosterner<!--Ge. 37:5-10.--> devant lui et ils dirent : Nous sommes tes serviteurs.
50:19	Yossef leur dit : N'ayez pas peur ! Car suis-je à la place d'Elohîm ?
50:20	Le mal que vous comptiez me faire, Elohîm comptait en faire une bonne chose, pour accomplir ce qui arrive aujourd'hui, pour faire vivre un peuple nombreux.
50:21	Maintenant, n’ayez plus peur : je vous nourrirai, vous et vos familles. Et il les consola en parlant à leur cœur.
50:22	Yossef demeura en Égypte, lui et la maison de son père, et vécut 110 ans.
50:23	Yossef vit les fils d'Éphraïm jusqu'à la troisième génération. Makir aussi, fils de Menashè, eut des fils qui furent élevés sur les genoux de Yossef.
50:24	Yossef dit à ses frères : Je vais mourir ! Mais Elohîm vous visitera<!--Ex. 3:16.-->, il vous visitera et vous fera monter de cette terre-ci vers la terre qu'il a juré de donner à Abraham, à Yitzhak et à Yaacov.
50:25	Yossef fit jurer les fils d'Israël et leur dit : Elohîm vous visitera, il vous visitera et alors vous transporterez mes os d'ici<!--Ex. 13:19 ; Hé. 11:22.-->.
50:26	Yossef mourut, fils de 110 ans. On l'embauma et on le mit dans un cercueil en Égypte.
