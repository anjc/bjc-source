# Yohanan (Jean) (Jn.)

Signification : YHWH a fait grâce

Auteur : Yohanan (Jean)

(Gr. : Ioannes)

Thème : Yéhoshoua, Elohîm

Date de rédaction : Env. 85 - 90 ap. J.-C.

Auteur d'un des quatre évangiles, des trois lettres éponymes et de l'Apocalypse, Yohanan, fils de Zabdi (Zébédée), fut l'un des douze apôtres. Témoin oculaire du service terrestre de Yéhoshoua ha Mashiah (Jésus-Christ), il atteste par l'essence de ses écrits le caractère divin de ce dernier.

Fidèle au livre de Shemot (Exode) où YHWH se révéla comme étant « JE SUIS », Yohanan reprend les propos de Yéhoshoua et le présente comme le « Logos » (la Parole) incarné, le Pain de vie, la Lumière du monde, la Porte des brebis, le Bon Berger, la Résurrection, la Vie... Proche du Maître, Yohanan fut à même de relater les événements marquants de sa vie comme la gloire de la transfiguration, l'angoisse de la passion exprimée à Gethsémané, ou encore les déclarations solennelles précédées de l'expression « Amen, amen » (en vérité, en vérité). Il met également en évidence la controverse suscitée par le Mashiah (Christ) et l'opposition dont il fit l'objet de la part de certains pharisiens qui souhaitaient sa mort.

L'évangile de Yohanan exprime la nécessité de la naissance d'en haut et dévoile les attributs du Fils d'Elohîm, le Mashiah tant attendu.

## Chapitre 1

### La divinité de Yéhoshoua ha Mashiah (Jésus-Christ)<!--Jn. 10:30 ; Hé. 1:5-13.-->

1:1	Au commencement était le Logos<!--Le philosophe Grec Héraclite d'Éphèse, a le premier utilisé le terme Logos vers 600 av. J.-C. pour désigner la raison divine ou le plan qui coordonne un univers changeant.-->, et le Logos était vers<!--Le mot grec « pros » signifie aussi « à l'avantage de », « à », « par », « envers », « auprès de », « vers », « avec ».--> l'Elohîm<!--Ici, le grec « theós » est précédé de l'article défini « τό » (le).-->, et Elohîm était le Logos.
1:2	Il était au commencement vers l'Elohîm<!--Ici également, le grec « theós » est précédé de l'article défini « τό » (le).-->.

### L'œuvre de Yéhoshoua avant son incarnation

1:3	Toutes choses ont été faites par son moyen, et rien de ce qui a été fait n'a été fait sans lui.
1:4	En lui était la vie, et la vie était la lumière<!--Yéhoshoua ha Mashiah (Jésus-Christ) notre lumière (Es. 60:19-20).--> des humains.
1:5	Et la lumière brille dans la ténèbre, mais la ténèbre ne l'a pas saisie<!--Ce mot vient du grec « katalambano » qui signifie « se saisir, saisir quelque chose comme pour en faire son propre bien, s'approprier » ; « faire une saisie ou prendre possession de ».-->.

### Yohanan le Baptiste (Jean-Baptiste), l'envoyé d'Elohîm

1:6	Il parut un homme, envoyé d'Elohîm. Son nom, Yohanan.
1:7	Celui-ci vint en témoignage, pour rendre témoignage au sujet de la lumière, afin que tous croient au travers de lui.
1:8	Il n'était pas la lumière, mais celui qui témoigne au sujet de la lumière.

### Yéhoshoua ha Mashiah (Jésus-Christ), la véritable lumière<!--Jn. 3:19-21, 8:12, 9:5, 12:46.-->

1:9	La lumière était la véritable qui illumine<!--« Éclairer spirituellement », « imprégner de la connaissance qui sauve ».--> tout être humain venant dans le monde. 
1:10	Il<!--Il est question du « Logos » (mot grec masculin signifiant la parole) d'où l'emploi du pronom personnel « il ». C'est le Logos qui a fait le monde. Voir Jn. 1:3.--> était dans le monde, et le monde a été fait par son moyen, mais le monde ne l'a pas connu.
1:11	Il<!--Il est également question ici du « Logos » (mot grec masculin signifiant la parole).--> est venu chez les siens, et les siens ne l'ont pas reçu.
1:12	Mais à tous ceux qui l'ont reçu<!--Ce mot vient du grec « lambano » qui signifie « prendre, prendre avec la main, saisir une personne ou une chose pour l'emmener » ; « recevoir ce qui est offert ».-->, à ceux qui croient en son Nom, il leur a donné le pouvoir de devenir enfants d'Elohîm,
1:13	lesquels ne sont pas engendrés du sang, ni de la volonté de la chair, ni de la volonté de l'homme, mais d'Elohîm.

### Le Logos (la Parole) devenu chair<!--Mt. 1:18-23 ; Lu. 1:30-35, 2:11 ; Jn. 14:9 ; 1 Ti. 3:16.-->

1:14	Et le Logos est devenu chair, il a dressé sa tente<!--Vient du grec « skenoo » qui signifie « fixer sa tente », « avoir son tabernacle », « demeurer dans un tabernacle » ou « habiter ». Voir Ap. 7:15, 12:12, 13:6, 21:3.--> parmi nous, et nous avons contemplé sa gloire, une gloire comme celle du Fils unique du Père, plein de grâce et de vérité.

### Témoignage de Yohanan le Baptiste<!--Mt. 3 ; Mc. 1:1-11 ; Lu. 3:1-22.-->

1:15	Yohanan rend témoignage à son sujet, et il a crié en disant : C'est de celui-ci que je disais : Celui qui vient derrière moi est passé devant moi, parce qu'il était avant moi.
1:16	Et nous avons tous reçu de sa plénitude, et grâce pour grâce.
1:17	Car la torah a été donnée par le moyen de Moshé, la grâce et la vérité sont venues par le moyen de Yéhoshoua Mashiah.
1:18	Elohîm, personne ne l'a jamais vu : le Fils unique, qui est dans le sein du Père, lui, l'a fait connaître.
1:19	Et c'est ici le témoignage de Yohanan, lorsque les Juifs envoyèrent de Yeroushalaim des prêtres et des Lévites pour l'interroger, et lui dire : Toi qui es-tu ?
1:20	Et il confessa et ne le nia pas, et il confessa : Moi, je ne suis pas le Mashiah.
1:21	Et ils lui demandèrent : Quoi donc ? Es-tu Éliyah<!--Élie.--> ? Et il dit : Je ne le suis pas<!--En Mt. 11:14, Yéhoshoua confirme pourtant que Yohanan le Baptiste est bien l'Éliyah (Élie) qui devait venir. Comment expliquer qu'il nia l'être lorsqu'il fut interrogé par les pharisiens ? La seule explication plausible, c'est qu'il l'ignorait. Toutefois, il avait conscience qu'il était « la voix » prophétisée par Yesha`yah (Ésaïe). Remarquez que lorsqu'il fut emprisonné, il avait envoyé quelques-uns de ses disciples pour demander à Yéhoshoua s'il était bien le Mashiah (Christ) (Mt. 11:13 ; Lu. 7:19-20) alors qu'il fut le premier à rendre témoignage du Seigneur. Ces éléments ne sont pas contradictoires, ils ne font que révéler les failles liées à la nature humaine de Yohanan.-->. Es-tu le Prophète ? Et il répondit : Non.
1:22	Ils lui dirent donc : Qui es-tu ? Afin que nous donnions une réponse à ceux qui nous ont envoyés. Que dis-tu de toi-même ?
1:23	Il déclara : Moi, je suis la voix de celui qui crie : Dans le désert, aplanissez la voie du Seigneur<!--Yohanan le Baptiste se réfère ici à la prophétie de Yesha`yah (Ésaïe) : « Préparez la voie de YHWH, aplanissez parmi les lieux arides un chemin pour notre Elohîm » (Es. 40:3). Cependant, on note que le texte ne mentionne pas le nom propre YHWH, mais utilise le nom commun « Seigneur ». Voir commentaire en Lu. 4:18-19.-->, comme a dit Yesha`yah<!--Ésaïe.-->, le prophète.
1:24	Et ceux qui avaient été envoyés étaient de chez les pharisiens.
1:25	Et ils l'interrogèrent encore et lui dirent : Pourquoi donc baptises-tu si tu n'es pas le Mashiah, ni Éliyah, ni le Prophète ?
1:26	Yohanan leur répondit en disant : Pour moi, je baptise dans l'eau, mais il y a quelqu'un au milieu de vous que vous ne connaissez pas,
1:27	c'est celui qui vient derrière moi, il est passé devant moi, et je ne suis pas digne de délier les lacets de ses sandales.
1:28	Ces choses se passèrent à Béthabara, au-delà du Yarden<!--Jourdain.-->, où Yohanan baptisait.
1:29	Le lendemain, Yohanan voit Yéhoshoua venant à lui, et il dit : Voici l'Agneau d'Elohîm qui ôte le péché du monde.
1:30	C'est lui, au sujet duquel je disais : Derrière moi vient un homme qui est passé devant moi, parce qu'il était avant moi.
1:31	Et pour moi, je ne le connaissais pas, mais c'est afin qu'il soit manifesté à Israël que je suis venu baptisant dans l'eau.
1:32	Yohanan rendit aussi témoignage, en disant : J'ai vu l'Esprit descendant du ciel comme une colombe et il est demeuré sur lui.
1:33	Et pour moi, je ne le connaissais pas, mais celui qui m'a envoyé baptiser dans l'eau m'a dit : Celui sur qui tu verras l'Esprit descendre et demeurer, c'est celui qui baptise dans le Saint-Esprit.
1:34	Et pour moi, j'ai vu et témoigné que celui-là est le Fils d'Elohîm.

### Les premiers disciples de Yéhoshoua ha Mashiah<!--Mt. 4:18-22 ; Mc. 1:16-20 ; Lu. 5:1-11.-->

1:35	Le lendemain, Yohanan était encore là avec deux de ses disciples.
1:36	Et, ayant regardé Yéhoshoua qui marchait, il dit : Voici l'Agneau d'Elohîm !
1:37	Et les deux disciples l'entendirent parler et ils suivirent Yéhoshoua.
1:38	Et Yéhoshoua se retourna et, voyant qu'ils le suivaient, il leur dit : Que cherchez-vous ? Ils lui dirent : Rabbi, (ce qui, interprété, signifie Docteur), où demeures-tu ?
1:39	Il leur dit : Venez et voyez. Ils y allèrent, et ils virent où il demeurait et ils demeurèrent auprès de lui ce jour-là. C'était environ la dixième heure.
1:40	Andreas, le frère de Shim’ôn Petros<!--Simon Pierre.-->, était l'un des deux qui avaient entendu ce que Yohanan avait dit et qui l'avaient suivi.
1:41	Celui-ci trouve le premier Shim’ôn, son propre frère, et il lui dit : Nous avons trouvé le Mashiah, ce qui, interprété, est : l'Oint<!--Ou Christ.-->.
1:42	Et il le conduisit vers Yéhoshoua. Et Yéhoshoua l'ayant regardé, dit : Tu es Shim’ôn, fils de Yonah, tu seras appelé Kephas<!--Céphas.--> (qui est interprété Petros<!--Pierre.-->).
1:43	Le lendemain, Yéhoshoua voulut aller en Galilée, et il trouve Philippos et lui dit : Suis-moi !
1:44	Or Philippos était de Bethsaïda, la ville d'Andreas et de Petros.
1:45	Philippos trouve Netanél<!--Nathanaël.--> et lui dit : Celui au sujet duquel ont écrit Moshé, dans la torah, et les prophètes, nous l'avons trouvé : c'est Yéhoshoua de Nazareth, fils de Yossef.
1:46	Et Netanél lui dit : Peut-il venir quelque chose de bon de Nazareth ? Philippos lui dit : Viens et vois.
1:47	Yéhoshoua aperçut Netanél venir vers lui, et il dit de lui : Voici vraiment un Israélite dans lequel il n'y a pas de tromperie.
1:48	Netanél lui dit : D'où me connais-tu ? Yéhoshoua répondit et lui dit : Avant que Philippos t'appelle, quand tu étais sous le figuier, je t'ai vu.
1:49	Netanél répondit et lui dit : Rabbi, tu es le Fils d'Elohîm, tu es le Roi d'Israël.
1:50	Yéhoshoua répondit et lui dit : Parce que je t'ai dit que je t'ai vu sous le figuier, tu crois ? Tu verras des choses plus grandes encore !
1:51	Il lui dit aussi : Amen, amen, je vous le dis : Désormais, vous verrez le ciel ouvert, et les anges d'Elohîm montant et descendant sur le Fils d'humain.

## Chapitre 2

### Miracle à Qanah (Cana)

2:1	Et le troisième jour, il y eut une noce à Qanah en Galilée, et la mère de Yéhoshoua était là.
2:2	Or Yéhoshoua fut aussi invité à la noce avec ses disciples.
2:3	Et le vin ayant manqué, la mère de Yéhoshoua lui dit : Ils n'ont plus de vin.
2:4	Yéhoshoua lui dit : Qu'y a-t-il entre moi et toi, femme ? Mon heure n'est pas encore venue.
2:5	Sa mère dit aux serviteurs : Quoi qu'il vous dise, faites-le.
2:6	Or six jarres à eau de pierre étaient posées là selon la purification des Juifs, contenant chacune deux ou trois mesures<!--Un ustensile pour mesurer, sorte d'amphore, servant à mesurer des liquides, et d'une contenance d'environ 40 litres.-->.
2:7	Yéhoshoua leur dit : Remplissez d'eau ces jarres à eau. Et ils les remplirent jusqu'en haut.
2:8	Et il leur dit : Puisez-en maintenant et apportez-en au maître de table. Et ils lui en apportèrent.
2:9	Mais quand le maître de table eut goûté l'eau devenue du vin (or il ne savait pas d'où cela venait, mais les serviteurs qui avaient puisé l'eau le savaient bien), le maître de table appelle l'époux
2:10	et lui dit : Tout être humain sert d'abord le bon vin, ensuite le moindre après qu'on s'est enivré. Toi, tu as gardé le bon vin jusqu'à maintenant.
2:11	Yéhoshoua fit ce premier de ses signes à Qanah de Galilée. Il manifesta sa gloire, et ses disciples crurent en lui.
2:12	Après cela, il descendit à Capernaüm avec sa mère et ses frères, et ses disciples, mais ils n'y demeurèrent que peu de jours.

### La première Pâque<!--Jn. 6:4, 11:55.-->

2:13	Et la Pâque des Juifs était proche, et Yéhoshoua monta à Yeroushalaim.
2:14	Et il trouva dans le temple les vendeurs de bœufs, de brebis et de colombes, et les changeurs qui y étaient assis.
2:15	Et ayant fait un fouet avec des petites cordes, il les chassa tous du temple, avec les brebis et les bœufs. Il dispersa la monnaie des changeurs et renversa les tables.
2:16	Et il dit aux vendeurs de colombes : Ôtez ces choses d'ici ! Ne faites pas de la maison de mon Père une maison de marché.
2:17	Mais ses disciples se souvinrent qu'il est écrit : Le zèle de ta maison m'a dévoré<!--Ps. 69:10.-->.
2:18	Les Juifs donc répondirent et lui dirent : Quel signe nous montres-tu, puisque tu fais ces choses ?
2:19	Yéhoshoua répondit et leur dit : Détruisez ce temple, et en 3 jours je le ressusciterai.
2:20	Alors les Juifs dirent : Ce temple a été bâti en 46 ans, et toi, tu le ressusciteras en 3 jours !
2:21	Mais lui parlait du temple de son corps.
2:22	Quand donc il fut réveillé d'entre les morts, ses disciples se souvinrent qu'il leur avait dit cela, et ils crurent l'Écriture et la parole que Yéhoshoua avait dite.
2:23	Et comme il était à Yeroushalaim, à la Pâque, pendant la fête, beaucoup crurent en son Nom, voyant les signes qu'il faisait.
2:24	Mais Yéhoshoua, lui, ne croyait pas en eux, parce qu'il les connaissait tous,
2:25	et parce qu'il n'avait pas besoin qu'on lui rende témoignage au sujet de l'être humain, car il savait lui-même ce qui était dans l'être humain.

## Chapitre 3

### La naissance d'en haut

3:1	Mais il y avait un homme d'entre les pharisiens, du nom de Nikodemos<!--Victorieux du peuple, généralement traduit par « Nicodème ».-->, un des chefs des Juifs.
3:2	Celui-ci vint de nuit vers Yéhoshoua et lui dit : Rabbi, nous savons que tu es un Docteur venu d'Elohîm, car personne ne peut produire les signes que, toi, tu produis, à moins qu'Elohîm ne soit avec lui.
3:3	Yéhoshoua répondit et lui dit : Amen, amen, je te le dis : À moins que quelqu'un ne soit engendré d'en haut<!--L'adverbe « d'en haut » vient du mot grec « anothen » qui signifie « depuis le haut », « depuis un endroit plus élevé », « ce qui vient des cieux ou d'Elohîm », « depuis le début », « l'origine ». Ce mot se retrouve dans Mt. 27:51 ; Mc. 15:38 ; Lu. 1:3 ; Jn. 3:31, 19:11,23 ; Ja. 1:17, 3:15,17. « Anothen » vient de « ano » qui signifie : « choses d'en haut ». En Ga. 4:26, « ano » peut se référer au lieu ou au temps. Le lieu : (le ciel). Le mot « ano » a été traduit par « en haut » dans Jn. 8:23, 11:41 ; Ac. 2:19 ; Ga. 4:26 ; Col. 3:1-2 ; et par « céleste » dans Ph. 3:14.-->, il ne peut voir le Royaume d'Elohîm.
3:4	Nikodemos lui dit : Comment un être humain peut-il être engendré quand il est vieux ? Peut-il une seconde fois entrer dans le ventre de sa mère et être engendré ?
3:5	Yéhoshoua répondit : Amen, amen, je te le dis : À moins que quelqu'un ne soit engendré d'eau et d'Esprit, il ne peut entrer dans le Royaume d'Elohîm.
3:6	Ce qui a été engendré de la chair est chair, et ce qui a été engendré de l'Esprit est esprit.
3:7	Ne t'étonne pas de ce que je t'ai dit : Il faut que vous soyez engendrés d'en haut.
3:8	Le vent souffle où il veut et tu en entends le bruit, mais tu ne sais pas d'où il vient, ni où il va. Ainsi en est-il de quiconque a été engendré de l'Esprit.
3:9	Nikodemos répondit et lui dit : Comment ces choses peuvent-elles se faire ?
3:10	Yéhoshoua répondit et lui dit : Tu es le docteur d'Israël et tu ne connais pas ces choses !
3:11	Amen, amen, je te dis que ce que nous savons, nous le disons et ce que nous avons vu, nous le témoignons, et vous ne recevez pas notre témoignage.
3:12	Si je vous ai parlé des choses terrestres et que vous ne croyiez pas, comment croirez-vous si je vous parle des choses célestes ?
3:13	Et personne n'est monté au ciel, excepté celui qui est descendu du ciel, le Fils d'humain qui est dans le ciel.
3:14	Or comme Moshé éleva le serpent<!--Le serpent d'airain ou de cuivre : No. 21:9.--> dans le désert, ainsi faut-il que soit élevé le Fils d'humain,
3:15	afin que quiconque croit en lui ne périsse pas, mais qu'il ait la vie éternelle.
3:16	Car Elohîm a tant aimé le monde qu'il a donné son Fils unique, afin que quiconque croit en lui ne périsse pas, mais qu'il ait la vie éternelle.
3:17	Car Elohîm n'a pas envoyé son Fils dans le monde pour qu'il juge le monde, mais pour que le monde soit sauvé par son moyen.
3:18	Celui qui croit en lui n'est pas jugé, mais celui qui ne croit pas est déjà jugé parce qu'il n'a pas cru au Nom du Fils unique d'Elohîm.
3:19	Et voici le jugement : C'est que la lumière est venue dans le monde et que les humains ont aimé la ténèbre plus que la lumière, car leurs œuvres étaient mauvaises.
3:20	Car quiconque pratique le mal, hait la lumière, et ne vient pas à la lumière, de peur que ses œuvres ne soient exposées<!--Condamner, réfuter. Généralement avec une suggestion de honte pour la personne accusée. Amener avec conviction à la lumière, exposer.-->.
3:21	Mais celui qui agit selon la vérité vient à la lumière, afin que ses œuvres soient manifestées, parce qu'elles sont faites en Elohîm.

### Nouveau témoignage de Yohanan le Baptiste

3:22	Après ces choses, Yéhoshoua vint avec ses disciples dans le pays de Judée. Là, il séjournait avec eux et il baptisait.
3:23	Or Yohanan baptisait aussi à Énon, près de Salim, parce qu'il y avait là beaucoup d'eau, et on y venait pour être baptisé.
3:24	Car Yohanan n'avait pas encore été mis en prison.
3:25	Or il y eut un débat entre les disciples de Yohanan et les Juifs concernant la purification.
3:26	Et ils vinrent près de Yohanan et lui dirent : Rabbi, celui qui était avec toi au-delà du Yarden et à qui tu as rendu témoignage, le voilà qui baptise, et tous vont vers lui.
3:27	Yohanan répondit et dit : Un être humain ne peut rien prendre, si cela ne lui a pas été donné à partir du ciel.
3:28	Vous-mêmes m'êtes témoins que j'ai dit : Ce n'est pas moi qui suis le Mashiah, mais je suis envoyé devant lui.
3:29	Celui qui a l'Épouse, c'est l'Époux, mais l'ami de l'Époux qui se tient debout et qui l'entend, se réjouit de joie à cause de la voix de l'Époux. Ainsi donc, cette joie qui est la mienne est accomplie.
3:30	Lui, il doit croître, mais moi, diminuer en popularité<!--Être moindre ou inférieur : en dignité, être rendu inférieur, diminuer en autorité ou popularité.-->.
3:31	Celui qui vient d'en haut est au-dessus de tous. Celui qui est de la Terre est de la Terre et, de la Terre, il parle. Celui qui vient du ciel est au-dessus de tous,
3:32	et ce qu'il a vu et entendu, il le témoigne, mais personne ne reçoit son témoignage.
3:33	Celui qui a reçu son témoignage a mis un sceau dessus qu'Elohîm est vrai.
3:34	Car celui qu'Elohîm a envoyé prononce les paroles d'Elohîm, car ce n'est pas par mesure qu'Elohîm donne l'Esprit.
3:35	Le Père aime le Fils et a donné toutes choses en sa main.
3:36	Celui qui croit au Fils a la vie éternelle, mais celui qui est rebelle au Fils ne verra pas la vie, mais la colère d'Elohîm demeure sur lui.

## Chapitre 4

### Yéhoshoua se rend en Galilée

4:1	Quand donc le Seigneur sut que les pharisiens avaient entendu dire : Yéhoshoua fait et baptise plus de disciples que Yohanan,
4:2	toutefois Yéhoshoua ne baptisait pas lui-même, mais c'étaient ses disciples<!--Parmi les disciples du Seigneur, il y avait aussi des femmes. Voir Ac. 9:36.-->,
4:3	il quitta la Judée et retourna encore en Galilée.

### Yéhoshoua et la femme samaritaine

4:4	Or, il lui fallait passer par la Samarie.
4:5	Il arriva dans une ville de Samarie, nommée Sychar, près de la terre que Yaacov avait donnée à Yossef, son fils<!--Ge. 48:22.-->.
4:6	Or, là était la source de Yaacov. Yéhoshoua donc, fatigué du voyage, s'était ainsi assis sur la source. C'était environ la sixième heure.
4:7	Et une femme samaritaine vient pour puiser de l'eau. Yéhoshoua lui dit : Donne-moi à boire.
4:8	Car ses disciples étaient allés à la ville pour acheter des vivres.
4:9	La femme, la samaritaine, lui dit donc : Comment toi qui es Juif, me demandes-tu à boire, à moi qui suis une femme samaritaine ? Car les Juifs n'ont pas de relations avec les Samaritains.
4:10	Yéhoshoua répondit et lui dit : Si tu connaissais le don d'Elohîm et qui est celui qui te dit : Donne-moi à boire, tu lui aurais toi-même demandé à boire, et il t'aurait donné de l'eau vive<!--Ap. 7:17.-->.
4:11	La femme lui dit : Seigneur, tu n'as rien pour puiser et le puits est profond. D'où aurais-tu donc cette eau vive ?
4:12	Es-tu plus grand que Yaacov, notre père, qui nous a donné ce puits et qui en a bu lui-même, ainsi que ses fils et son bétail ?
4:13	Yéhoshoua répondit et lui dit : Quiconque boit de cette eau-ci aura encore soif.
4:14	Mais celui qui boira de l'eau que je lui donnerai n'aura jamais soif, mais l'eau que je lui donnerai deviendra en lui une source d'eau qui jaillira jusque dans la vie éternelle.
4:15	La femme lui dit : Seigneur, donne-moi de cette eau, afin que je n'aie plus soif et que je ne vienne plus puiser ici.
4:16	Yéhoshoua lui dit : Va, appelle ton mari, et viens ici.
4:17	La femme répondit et lui dit : Je n'ai pas de mari. Yéhoshoua lui dit : Tu as bien dit : Je n'ai pas de mari.
4:18	Car tu as eu cinq maris et celui que tu as maintenant n'est pas ton mari. En cela tu as dit vrai.
4:19	La femme lui dit : Seigneur, je vois que tu es prophète.
4:20	Nos pères ont adoré sur cette montagne<!--La Samaritaine faisait allusion au Mont Garizim, également appelé Montagne de Shekem (Sichem). Les Samaritains y avaient construit leur propre temple, mais celui-ci fut détruit vers 129 av. J.-C. par Yohanan Girhan (Jean Hyrcan), fils de Shim’ôn Makabi (Simon Maccabée).-->, et vous, vous dites que le lieu où il faut adorer est à Yeroushalaim.
4:21	Yéhoshoua lui dit : Femme, crois-moi, parce que l’heure vient où vous n’adorerez le Père, ni sur cette montagne, ni à Yeroushalaim.
4:22	Vous, vous adorez ce que vous ne connaissez pas ; nous, nous adorons ce que nous connaissons, car le salut est à partir des Juifs.
4:23	Mais l'heure vient et elle est déjà venue, où les véritables adorateurs adoreront le Père en esprit et en vérité. Car ce sont des tels adorateurs que le Père cherche.
4:24	Elohîm est Esprit, et il faut que ceux qui l'adorent, l'adorent en esprit et en vérité.
4:25	La femme lui dit : Je sais que le Mashiah vient, celui qu'on appelle l'Oint<!--Christ.-->. Quand donc il sera venu, il nous annoncera toutes choses.
4:26	Yéhoshoua lui dit : Moi, je suis<!--Voir commentaire en Ex. 3:14 ; Jn. 8:28,58, 18:5-6.-->, qui te parle.
4:27	Et là-dessus, ses disciples vinrent et ils s'étonnèrent de ce qu'il parlait avec une femme. Toutefois aucun ne dit : Que demandes-tu ? Ou : Pourquoi parles-tu avec elle ?
4:28	La femme ayant laissé sa jarre à eau, s'en alla dans la ville et elle dit aux gens :
4:29	Venez, voyez un homme qui m'a dit tout ce que j'ai fait. Celui-ci n’est-il pas le Mashiah ?
4:30	Ils sortirent donc de la ville et vinrent vers lui.
4:31	Et pendant ce temps-là, les disciples le priaient en disant : Rabbi, mange !
4:32	Mais il leur dit : Moi, j'ai à manger une nourriture que vous, vous ne connaissez pas.
4:33	Les disciples se disaient donc l'un à l'autre : Quelqu'un lui a-t-il apporté à manger ?
4:34	Yéhoshoua leur dit : Mon aliment est de faire la volonté de celui qui m'a envoyé et d'accomplir son œuvre.
4:35	Ne dites-vous pas : Encore quatre mois et la moisson vient ? Voyez, vous dis-je, levez les yeux et regardez les champs, parce qu'ils sont déjà blancs pour la moisson.
4:36	Or celui qui moissonne reçoit le salaire et amasse le fruit pour la vie éternelle, afin que celui qui sème et celui qui moissonne se réjouissent ensemble.
4:37	Car en ceci la parole est véritable : Autre est celui qui sème et autre, celui qui moissonne.
4:38	Pour moi, je vous ai envoyés moissonner là où vous ne vous êtes pas fatigués. D'autres se sont fatigués et vous, vous êtes entrés dans leur travail.

### Yéhoshoua et les Samaritains

4:39	Or beaucoup de Samaritains de cette ville-là crurent en lui à cause de la parole de la femme témoignant qu'il m’a dit toutes les choses que j’ai faites.
4:40	Quand donc les Samaritains vinrent le trouver, ils le prièrent de demeurer avec eux. Et il demeura là deux jours.
4:41	Et un beaucoup plus grand nombre crurent à cause de sa parole,
4:42	et ils disaient à la femme : Ce n'est plus à cause de ton discours que nous croyons, car nous l'avons entendu nous-mêmes et nous savons qu'il est véritablement le Mashiah, le Sauveur du monde<!--Yéhoshoua est le Sauveur du monde. Le mot « monde » vient du grec « kosmos » qui signifie : Le cercle de la Terre, la Terre. Les habitants de la Terre, les humains, la race humaine. La multitude sans Elohîm, la masse des humains séparés d'Elohîm. Voir Jean 3:17 ; 12:47 et 1 Jean 4:14.-->.

### Yéhoshoua guérit le fils d'un officier

4:43	Or après les deux jours, il partit de là et s'en alla en Galilée.
4:44	Car Yéhoshoua avait rendu témoignage qu'un prophète n’a pas de valeur dans son propre pays natal.
4:45	Quand donc il vint en Galilée, les Galiléens le reçurent, ayant vu toutes les choses qu'il avait faites à Yeroushalaim le jour de la fête, car eux aussi étaient allés à la fête.
4:46	Yéhoshoua donc vint de nouveau à Qanah de Galilée, où il avait fait du vin avec de l'eau. Or il y avait à Capernaüm un certain serviteur du roi dont le fils était malade,
4:47	qui, ayant entendu que Yéhoshoua était venu de Judée en Galilée, s'en alla vers lui et le pria de descendre pour guérir son fils qui était sur le point de mourir.
4:48	Mais Yéhoshoua lui dit : Si vous ne voyez pas des signes et des prodiges, vous ne croirez donc jamais !
4:49	Le serviteur du roi lui dit : Seigneur, descends avant que mon enfant meure.
4:50	Yéhoshoua lui dit : Va, ton fils vit. Et cet homme crut à la parole que Yéhoshoua lui avait dite et il s'en alla.
4:51	Et comme il descendait déjà, ses esclaves vinrent à sa rencontre et lui apportèrent des nouvelles, en disant : Ton garçon vit.
4:52	Et il leur demanda à quelle heure il s'était trouvé mieux et ils lui dirent : Hier, à la septième heure, la fièvre l'a quitté.
4:53	Le père reconnut que c'était à cette heure-là que Yéhoshoua lui avait dit : Ton fils vit. Et il crut avec toute sa maison.
4:54	Yéhoshoua produisit encore ce second signe après être venu de Judée en Galilée.

## Chapitre 5

### Guérison d'un paralytique à la piscine de Béthesda

5:1	Après ces choses, il y eut une fête des Juifs, et Yéhoshoua monta à Yeroushalaim.
5:2	Or il existe dans Yeroushalaim, près de la porte des brebis, un réservoir appelé en hébreu Béthesda, ayant cinq portiques.
5:3	Là étaient couchés un grand nombre de malades : des aveugles, des boiteux, des paralytiques, attendant le mouvement de l'eau.
5:4	Car un ange descendait en un certain temps dans le réservoir et agitait l'eau, et alors le premier qui y descendait après que l'eau avait été agitée était guéri, de quelque maladie qu'il fût détenu.
5:5	Or un certain homme était là, étant dans sa maladie depuis 38 ans.
5:6	Yéhoshoua le voyant couché et sachant que depuis longtemps déjà il l'était, lui dit : Veux-tu être guéri ?
5:7	Le malade lui répondit : Seigneur, je n'ai personne pour me jeter dans le réservoir quand l'eau est agitée, et pendant que j'y vais, un autre y descend avant moi.
5:8	Yéhoshoua lui dit : Réveille-toi ! Prends ton lit de camp et marche !
5:9	Et immédiatement l'homme fut guéri. Il prit son lit de camp et marcha. Or c'était le shabbat, ce jour-là.
5:10	Les Juifs dirent donc à celui qui avait été guéri : C'est le shabbat ! Il n'est pas légal pour toi de prendre ce lit.
5:11	Il leur répondit : Celui qui m'a guéri m'a dit : Prends ton lit de camp et marche !
5:12	Alors ils lui demandèrent : Qui est celui qui t'a dit : Prends ton lit de camp et marche ?
5:13	Mais celui qui avait été guéri ne savait pas qui c'était, car Yéhoshoua s'était échappé<!--« S'en aller au loin », « éviter une chose », « disparaître furtivement ».--> du milieu de la foule qui était en ce lieu.
5:14	Après cela, Yéhoshoua le trouve dans le temple et lui dit : Vois, tu as été guéri. Ne pèche plus, pour qu’il ne t’arrive rien de pire.
5:15	Cet homme s'en alla et rapporta aux Juifs que c'était Yéhoshoua qui l'avait guéri.
5:16	Et à cause de cela, les Juifs persécutaient Yéhoshoua et cherchaient à le faire mourir, parce qu'il faisait ces choses pendant le shabbat.

### Égalité de Yéhoshoua avec le Père

5:17	Mais Yéhoshoua leur répondit : Mon Père travaille jusqu'à présent, et moi aussi je travaille.
5:18	Et à cause de cela, les Juifs cherchaient encore plus à le faire mourir, non seulement parce qu'il annulait le shabbat, mais aussi parce qu'il disait qu'Elohîm était son propre Père, se faisant égal à Elohîm.
5:19	Mais Yéhoshoua répondit et leur dit : Amen, amen, je vous le dis, le Fils ne peut rien faire de lui-même, à moins qu'il ne le voit faire au Père. Car les choses que celui-ci fait, le Fils aussi les fait pareillement.
5:20	Car le Père aime le Fils et lui montre toutes les choses qu'il fait, et il lui montrera de plus grandes œuvres que celles-ci, afin que vous soyez dans l'admiration.
5:21	Car, comme le Père ressuscite les morts et donne la vie, de même aussi le Fils donne la vie à ceux qu'il veut.
5:22	Car le Père ne juge personne, mais il a donné tout jugement au Fils,
5:23	afin que tous honorent le Fils comme ils honorent le Père. Celui qui n'honore pas le Fils, n'honore pas le Père qui l'a envoyé.
5:24	Amen, amen, je vous le dis : Celui qui entend ma parole et croit à celui qui m'a envoyé, a la vie éternelle et ne vient pas en jugement, mais il est passé hors de la mort à la vie.

### La résurrection

5:25	Amen, amen, je vous le dis : L'heure vient, et elle est même déjà venue, où les morts entendront la voix du Fils d'Elohîm, et ceux qui l'auront entendue, vivront.
5:26	Car comme le Père a la vie en lui-même, ainsi il a donné au Fils d'avoir la vie en lui-même.
5:27	Et il lui a donné aussi le pouvoir de juger parce qu'il est le Fils d'humain.
5:28	Ne vous étonnez pas de cela, car l'heure vient où tous ceux qui sont dans les sépulcres entendront sa voix,
5:29	et ils en sortiront : ceux qui auront fait des choses bonnes, pour la résurrection de la vie, mais ceux qui auront pratiqué des choses mauvaises, pour la résurrection de condamnation<!--Le tribunal. Voir 2 Co. 5:10.-->.

### Témoignages en accord avec celui de Yéhoshoua

5:30	Je ne peux, moi, rien faire de moi-même : je juge selon ce que j'entends, et mon jugement est juste, parce que je ne cherche pas ma volonté, mais la volonté du Père qui m'a envoyé.
5:31	Si je rends témoignage de moi-même, mon témoignage n'est pas vrai.
5:32	C'est un autre qui rend témoignage de moi, et je sais que le témoignage qu'il rend de moi est vrai.

### Le témoignage de Yohanan le Baptiste

5:33	Vous avez envoyé des hommes vers Yohanan, et il a rendu témoignage à la vérité.
5:34	Mais moi, je ne reçois pas le témoignage d'un humain, mais je dis ces choses afin que vous soyez sauvés.
5:35	Il était la lampe qui brûle et qui brille<!--Ps. 18:29 ; Jn. 1:8.-->, et vous avez voulu exulter une heure en sa lumière.

### Le témoignage des œuvres de Yéhoshoua

5:36	Mais moi, j'ai un témoignage plus grand que celui de Yohanan, car les œuvres que mon Père m'a donné d'accomplir, ces œuvres mêmes que je fais, témoignent de moi que c'est mon Père qui m'a envoyé.

### Le témoignage du Père<!--Mt. 3:17.-->

5:37	Et le Père qui m'a envoyé, a lui-même rendu témoignage de moi. Vous n'avez jamais entendu sa voix, vous n'avez jamais vu sa face,
5:38	et sa parole ne demeure pas en vous, puisque vous ne croyez pas en celui qu'il a envoyé.

### Le témoignage de l'Écriture<!--Lu. 24:27,44.-->

5:39	Vous sondez les Écritures parce que vous pensez avoir en elles la vie éternelle, et ce sont elles qui rendent témoignage de moi.
5:40	Mais vous ne voulez pas venir à moi pour avoir la vie.
5:41	Je ne reçois pas ma gloire des humains.
5:42	Mais je sais que vous n'avez pas l'amour d'Elohîm en vous.
5:43	Moi, je suis venu au Nom de mon Père et vous ne me recevez pas, si un autre vient en son propre nom<!--Ap. 13:17-18.-->, vous le recevrez.
5:44	Comment pouvez-vous croire, vous qui recevez votre gloire les uns des autres et qui ne cherchez pas la gloire qui vient d'Elohîm seul ?
5:45	Ne pensez pas que ce soit moi qui vous accuserai devant le Père. Celui qui vous accuse, c’est Moshé, en qui vous espérez.
5:46	Car si vous croyiez Moshé, vous me croiriez aussi, car il a écrit sur moi.
5:47	Mais si vous ne croyez pas à ses écrits, comment croirez-vous à mes paroles ?

## Chapitre 6

### Multiplication des pains<!--Mt. 14:15-21 ; Mc. 6:32-44 ; Lu. 9:12-17.-->

6:1	Après ces choses, Yéhoshoua s'en alla au-delà de la Mer de Galilée, de Tibériade.
6:2	Et une grande foule le suivait, parce qu'elle voyait les signes qu'il produisait sur les malades.
6:3	Mais Yéhoshoua monta sur une montagne, et il s'assit là avec ses disciples.
6:4	Or la Pâque, la fête des Juifs, était proche.
6:5	Yéhoshoua donc ayant levé ses yeux et, voyant qu'une grande foule venait à lui, dit à Philippos : Où achèterons-nous des pains, afin que ceux-ci mangent ?
6:6	Or il disait cela pour l'éprouver, car il savait bien ce qu'il était sur le point de faire.
6:7	Philippos lui répondit : 200 deniers de pain ne leur suffisent pas pour que chacun d’eux en reçoive un peu.
6:8	Un de ses disciples, Andreas, le frère de Shim’ôn Petros, lui dit :
6:9	Il y a ici un petit garçon qui a cinq pains d'orge et deux poissons, mais qu'est-ce que cela pour tant de monde ?
6:10	Mais Yéhoshoua dit : Faites asseoir les gens. Or il y avait beaucoup d'herbe dans ce lieu. Les hommes donc s’assirent, au nombre d’environ 5 000.
6:11	Et Yéhoshoua prit les pains, et après avoir rendu grâce, il les distribua aux disciples, et les disciples à ceux qui étaient assis, de même aussi des poissons, autant qu'ils en voulaient.
6:12	Et après qu'ils furent rassasiés, il dit à ses disciples : Rassemblez les morceaux qui restent, afin que rien ne soit perdu.
6:13	Ils les rassemblèrent donc et ils remplirent douze paniers avec les morceaux qui restèrent des cinq pains d'orge, après que tous eurent mangé.
6:14	Les gens donc ayant vu le signe que Yéhoshoua avait produit, disaient : Celui-ci est vraiment le Prophète qui vient dans le monde.
6:15	Mais Yéhoshoua, sachant qu'ils étaient sur le point de venir l'enlever de force pour le faire roi, se retira encore, lui seul, sur la montagne.

### Yéhoshoua marche sur les eaux<!--Mt. 14:22-33 ; Mc. 6:45-52.-->

6:16	Et quand le soir fut venu, ses disciples descendirent à la mer.
6:17	Et étant montés dans le bateau, ils traversaient la mer pour se rendre à Capernaüm. Déjà l’obscurité s’était faite, et Yéhoshoua n'était pas encore venu vers eux.
6:18	Il soufflait un grand vent et la mer était agitée.
6:19	Après avoir donc ramé environ vingt-cinq ou trente stades<!--C'est-à-dire environ cinq mille mètres.-->, ils voient Yéhoshoua marchant sur la mer et arrivant près du bateau, et ils eurent peur.
6:20	Mais il leur dit : Moi, je suis, n'ayez pas peur !
6:21	Ils le reçurent donc avec plaisir dans le bateau, et immédiatement le bateau toucha la terre, là où ils allaient.

### Yéhoshoua, le Pain de vie

6:22	Le lendemain, la foule qui se tenait de l'autre côté de la mer vit qu'il ne se trouvait là qu'un seul petit bateau et que Yéhoshoua n'y était pas monté avec ses disciples, mais qu'ils étaient partis seuls.
6:23	Mais d'autres petits bateaux étaient arrivés de Tibériade près du lieu où ils avaient mangé le pain, après que le Seigneur eut rendu grâce.
6:24	Quand la foule vit que ni Yéhoshoua ni ses disciples n'étaient là, ils montèrent eux-mêmes dans ces bateaux et allèrent à Capernaüm, cherchant Yéhoshoua.
6:25	Et l'ayant trouvé au-delà de la mer, ils lui dirent : Rabbi, quand es-tu arrivé ici ?
6:26	Yéhoshoua répondit et leur dit : Amen, amen, je vous le dis : Vous me cherchez, non parce que vous avez vu des signes, mais parce que vous avez mangé des pains et avez été rassasiés.
6:27	Travaillez, non pour la nourriture qui périt, mais pour la nourriture qui demeure pour la vie éternelle et que le Fils d'humain vous donnera. Car le Père, l'Elohîm, l'a marqué de son sceau.
6:28	Ils lui dirent donc : Que devons-nous faire pour accomplir les œuvres d'Elohîm ?
6:29	Yéhoshoua répondit et leur dit : C'est ici l'œuvre d'Elohîm, que vous croyiez en celui qu'il a envoyé.
6:30	Alors ils lui dirent : Quel signe produis-tu donc, afin que nous le voyions et que nous croyions en toi ? Quelle œuvre fais-tu ?
6:31	Nos pères ont mangé la manne dans le désert, selon ce qui est écrit : Il leur a donné à manger le pain issu du ciel<!--Ex. 16.-->.
6:32	Mais Yéhoshoua leur dit : Amen, amen, je vous le dis : Moshé ne vous a pas donné le pain issu du ciel. Mais mon Père vous donne le vrai pain issu du ciel.
6:33	Car le pain d'Elohîm est celui qui descend du ciel et qui donne la vie au monde.
6:34	Ils lui dirent donc : Seigneur, donne-nous toujours ce pain-là.
6:35	Et Yéhoshoua leur dit : Moi, JE SUIS le Pain de vie. Celui qui vient à moi n'aura jamais faim, et celui qui croit en moi n'aura jamais soif.
6:36	Mais je vous ai dit que vous m'avez vu, et cependant vous ne croyez pas.
6:37	Tout ce que me donne le Père, viendra à moi et, celui qui vient à moi, je ne le jetterai jamais dehors.
6:38	Car je suis descendu du ciel, non pas pour faire ma volonté, mais la volonté de celui qui m'a envoyé.
6:39	Et c'est ici la volonté du Père qui m'a envoyé, que je ne perde rien de tout ce qu'il m'a donné, mais que je le ressuscite dans le dernier jour.
6:40	Et c'est ici la volonté de celui qui m'a envoyé : que quiconque pose son regard sur le Fils et croit en lui ait la vie éternelle, et moi, je le ressusciterai le dernier jour.
6:41	Les Juifs murmuraient donc à son sujet, parce qu'il avait dit : Moi, je suis le pain descendu du ciel.
6:42	Et ils disaient : Celui-ci n'est-il pas Yéhoshoua, le fils de Yossef, dont nous connaissons le père et la mère ? Comment donc celui-ci dit-il : Je suis descendu du ciel ?
6:43	Yéhoshoua donc répondit et leur dit : Ne murmurez pas entre vous.
6:44	Personne ne peut venir à moi, à moins que le Père qui m'a envoyé ne le tire<!--Traîner. Voir Jn. 12:32.-->, et moi, je le ressusciterai le dernier jour.
6:45	Il est écrit dans les prophètes : Et ils seront tous enseignés d'Elohîm<!--Jé. 31:31-34.-->. C'est pourquoi quiconque a entendu le Père et a été enseigné par lui, vient à moi.
6:46	C'est que personne n'a vu le Père, sinon celui qui vient d'Elohîm, celui-là a vu le Père.
6:47	Amen, amen, je vous le dis : Celui qui croit en moi a la vie éternelle.
6:48	Moi, je suis le Pain de vie.
6:49	Vos pères ont mangé la manne dans le désert et ils sont morts.
6:50	C'est ici le pain qui descend du ciel, afin que si quelqu'un en mange ne meure pas.
6:51	Moi, je suis le Pain, le Vivant descendu du ciel. Si quelqu'un mange de ce pain, il vivra pour l’éternité, et le pain que je donnerai, c'est ma chair que je donnerai en faveur de la vie du monde.
6:52	Les Juifs donc discutaient entre eux et disaient : Comment peut-il nous donner sa chair à manger ?
6:53	Et Yéhoshoua leur dit : Amen, amen, je vous le dis : Si vous ne mangez pas la chair du Fils d'humain et ne buvez pas son sang, vous n'avez pas la vie en vous-mêmes.
6:54	Celui qui mange ma chair et qui boit mon sang a la vie éternelle, et je le ressusciterai le dernier jour.
6:55	Car ma chair est vraiment un aliment, et mon sang est vraiment une boisson.
6:56	Celui qui mange ma chair et qui boit mon sang demeure en moi, et moi en lui.
6:57	Comme il m’a envoyé, le Vivant Père, et que moi, je vis à travers le Père, aussi celui qui me mange vivra également à travers moi.
6:58	C'est ici le pain qui est descendu du ciel. Il n'en est pas comme de vos pères qui mangèrent la manne et qui moururent. Celui qui mangera ce pain vivra pour l’éternité.
6:59	Il dit ces choses dans la synagogue, enseignant à Capernaüm.
6:60	Beaucoup de ses disciples ayant donc entendu cela, dirent : Cette parole est dure ! qui peut l'entendre ? 
6:61	Mais Yéhoshoua, sachant en lui-même que ses disciples murmuraient à ce sujet, leur dit : Cela vous scandalise-t-il ?
6:62	Si donc vous voyez le Fils d'humain montant où il était auparavant ?
6:63	C'est l'Esprit qui donne la vie, la chair ne sert à rien. Les paroles que je vous ai dites sont esprit et sont vie.
6:64	Mais il en est certains parmi vous qui ne croient pas. Car Yéhoshoua savait depuis le commencement qui étaient ceux qui ne croiraient pas, et qui était celui qui le livrerait.
6:65	Et il disait : C’est à cause de cela que je vous ai dit que personne ne peut venir à moi, à moins qu'il ne lui soit donné de mon Père.
6:66	Dès lors, beaucoup de ses disciples s’en allèrent vers les choses qui étaient derrière et ne marchèrent plus avec lui.

### Petros (Pierre) reconnaît Yéhoshoua comme étant le Mashiah (Christ)<!--Mt. 16:13-16 ; Mc. 8:27-30 ; Lu. 9:18-21.-->

6:67	Et Yéhoshoua dit aux douze : Et vous, ne voulez-vous pas aussi vous en aller ?
6:68	Alors Shim’ôn Petros lui répondit : Seigneur, vers qui irions-nous ? Tu as les paroles de la vie éternelle.
6:69	Et nous avons cru et nous avons connu que tu es le Mashiah, le Fils d'Elohîm, du Vivant.
6:70	Yéhoshoua leur répondit : Ne vous ai-je pas choisis, vous les douze ? Et l'un de vous est un diable<!--Vient du grec « diabolos » qui veut dire « calomniateur », « prompt à la calomnie », « accusant faussement ». Voir Mt. 4:1.-->.
6:71	Or il parlait de Yéhouda, de Shim’ôn Iskariote, car c’était lui qui était sur le point de le livrer, quoiqu'il fût l'un des douze.

## Chapitre 7

### L'incrédulité des frères de Yéhoshoua

7:1	Et après ces choses, Yéhoshoua marchait dans la Galilée, car il ne voulait pas marcher dans la Judée, parce que les Juifs cherchaient à le faire mourir.
7:2	Or la fête des Juifs, la construction d'un tabernacle<!--La fête des cabanes (ou des tabernacles) était la dernière des trois grandes fêtes annuelles auxquelles tout Israélite devait assister, dans le lieu qu'Elohîm avait choisi. Cette fête emprunta son nom à la coutume de séjourner dans des cabanes de feuillage pendant sa célébration. Cette fête se déroulait le septième mois de l'année religieuse (Tishri, soit octobre) quand les récoltes et vendanges étaient terminées. Elle devait rappeler le séjour au désert, passé sous des tentes. La fête qui commençait le 15 du mois, durait sept jours. De nombreux holocaustes étaient offerts.-->, était proche.
7:3	Et ses frères lui dirent : Pars d'ici et va en Judée, afin que tes disciples aussi voient les œuvres que tu fais.
7:4	Car on ne fait rien en secret quand on cherche à être remarqué en faisant sa propre publicité<!--Le maintien par lequel on se met en évidence et que l'on assure sa publicité.-->. Si tu fais ces choses, montre-toi toi-même au monde.
7:5	Car ses frères non plus ne croyaient pas en lui.
7:6	Et Yéhoshoua leur dit : Mon temps n'est pas encore venu, mais votre temps est toujours prêt.
7:7	Le monde ne peut pas vous haïr, mais moi, il me hait parce que moi, je témoigne à son sujet que ses œuvres sont mauvaises.
7:8	Vous, montez à cette fête ! Pour moi, je n'y monte pas encore, parce que mon temps n'est pas encore accompli.
7:9	Et après leur avoir dit ces choses, il resta en Galilée.

### Yéhoshoua à la fête des cabanes (ou des tabernacles)

7:10	Mais lorsque ses frères furent montés, alors il y monta aussi lui-même, non pas manifestement, mais comme en secret.
7:11	Les Juifs le cherchaient pendant la fête, et ils disaient : Où est-il ?
7:12	Et il y avait un grand murmure à son sujet dans les foules. Les uns disaient : C'est un homme bon. Mais d'autres disaient : Non, il égare la foule.
7:13	Toutefois personne ne parlait franchement de lui, à cause de la crainte qu'on avait des Juifs.
7:14	Mais, comme on était déjà au milieu de la fête, Yéhoshoua monta au temple et il enseignait.
7:15	Et les Juifs s'étonnaient, disant : Comment celui-ci connaît-il les Lettres<!--2 Ti. 3:15.--> sans avoir été enseigné ?
7:16	Yéhoshoua leur répondit et dit : Ma doctrine n'est pas de moi, mais de celui qui m'a envoyé.
7:17	Si quelqu'un veut faire sa volonté, il saura si ma doctrine est d'Elohîm ou si je parle de moi-même.
7:18	Celui qui parle de lui-même cherche sa propre gloire. Mais celui qui cherche la gloire de celui qui l'a envoyé est vrai, et il n'y a pas d'injustice en lui.
7:19	Moshé ne vous a-t-il pas donné la torah ? Cependant, aucun de vous n'observe la torah. Pourquoi cherchez-vous à me faire mourir ?
7:20	La foule répondit : Tu as un démon. Qui est-ce qui cherche à te faire mourir ?
7:21	Yéhoshoua répondit et leur dit : J'ai fait une œuvre et vous en êtes tous étonnés.
7:22	C’est en raison de ceci que Moshé vous a donné la circoncision, non qu’elle soit issue de Moshé, mais elle est issue des pères et, dans un shabbat, vous circoncisez un homme.
7:23	Si un homme reçoit la circoncision dans un shabbat, pour que la torah de Moshé ne soit pas renversée, pourquoi êtes-vous fâchés contre moi parce que j'ai rendu toute sa santé à un homme dans un shabbat ?
7:24	Ne jugez pas sur l'apparence, mais jugez d'un juste jugement.
7:25	Alors quelques-uns de ceux de Yeroushalaim disaient : N'est-ce pas celui qu'ils cherchent à faire mourir ?
7:26	Et cependant voici, il parle librement, et ils ne lui disent rien ! Est-ce que vraiment les chefs auraient reconnu qu'il est véritablement le Mashiah ?
7:27	Or nous savons d'où est celui-ci, mais quand le Mashiah viendra, personne ne saura d'où il est.
7:28	Yéhoshoua donc criait dans le temple, enseignant et disant : Vous me connaissez et vous savez d'où je suis ! Je ne suis pas venu de moi-même, mais celui qui m'a envoyé est véritable, et vous ne le connaissez pas.
7:29	Mais moi, je le connais, car je suis de lui et c'est lui qui m'a envoyé.
7:30	Ils cherchaient donc à se saisir de lui, mais personne ne mit la main sur lui, parce que son heure n'était pas encore venue.
7:31	Et beaucoup parmi la foule crurent en lui, et ils disaient : Le Mashiah, quand il viendra, produira-t-il plus de signes que celui-ci n'en a produit ?
7:32	Les pharisiens entendirent la foule murmurant ces choses à son sujet. Et les pharisiens avec les principaux prêtres envoyèrent des huissiers pour se saisir de lui.
7:33	Alors Yéhoshoua leur dit : Je suis encore pour un peu de temps avec vous, et je m'en vais vers celui qui m'a envoyé.
7:34	Vous me chercherez mais vous ne me trouverez pas, et vous ne pouvez pas venir là où je suis.
7:35	Les Juifs dirent donc entre eux : Où celui-ci est sur le point d'aller que nous ne le trouverons pas ? N'est-il pas sur le point de se rendre auprès de la diaspora des Grecs et d’enseigner les Grecs ?
7:36	Quelle est cette parole qu'il a dite : Vous me chercherez, mais vous ne me trouverez pas, vous ne pouvez pas venir là où je suis ?
7:37	Mais le dernier jour, le grand, de la fête, Yéhoshoua, se tenait debout, et il cria disant : Si quelqu'un a soif, qu'il vienne à moi et qu'il boive.
7:38	Celui qui croit en moi, selon ce que l'Écriture a dit, des fleuves d'eau vive couleront de son ventre<!--Es. 44:3-4.-->.
7:39	Or il dit cela de l'Esprit qu’étaient sur le point de recevoir ceux qui croient en lui. Car le Saint-Esprit n'était pas encore, parce que Yéhoshoua n'était pas encore glorifié.

### La foule divisée à cause de Yéhoshoua

7:40	Dans la foule, beaucoup de ceux qui avaient entendu ces paroles disaient : Celui-ci est vraiment le Prophète.
7:41	D'autres disaient : Celui-ci est le Mashiah. Mais d'autres disaient : Non, le Mashiah vient-il donc de la Galilée ?
7:42	L'Écriture ne dit-elle pas que le Mashiah vient de la postérité de David et du village de Bethléhem, d'où était David ?
7:43	Il y eut donc de la division parmi la foule à cause de lui.
7:44	Et quelques-uns d'entre eux voulaient le saisir, mais personne ne mit la main sur lui.
7:45	Alors les huissiers vinrent vers les principaux prêtres et les pharisiens, et ceux-ci leur dirent : En raison de quoi ne l'avez-vous pas amené ?
7:46	Les huissiers répondirent : Jamais un humain n'a parlé comme cet humain !
7:47	Alors les pharisiens leur répondirent : Avez-vous été égarés, vous aussi ?
7:48	Y a-t-il quelqu'un des chefs ou des pharisiens qui ait cru en lui ?
7:49	Mais cette foule qui ne connaît pas la torah, ce sont des maudits.
7:50	Nikodemos, celui qui était venu vers Yéhoshoua de nuit et qui était l'un d'entre eux, leur dit :
7:51	Notre torah juge-t-elle un humain sans qu'on l'ait entendu auparavant, et qu'on ait pris connaissance de ce qu'il a fait ?
7:52	Ils répondirent et lui dirent : N’es-tu pas aussi de la Galilée ? Examine et tu verras qu'aucun prophète n'a été suscité de Galilée.
7:53	Et chacun s'en alla dans sa maison.

## Chapitre 8

### La femme adultère

8:1	Mais Yéhoshoua se rendit à la Montagne des Oliviers.
8:2	Mais à l’aube, il alla de nouveau dans le temple, et tout le peuple vint à lui. Et s'étant assis, il les enseignait.
8:3	Or les scribes et les pharisiens lui amènent une femme surprise en adultère et, l'ayant placée au milieu,
8:4	ils lui disent : Docteur, cette femme a été prise sur le fait, commettant un adultère.
8:5	Or Moshé nous a ordonné dans la torah de lapider de telles personnes. Toi donc, qu'en dis-tu ?
8:6	Or ils disaient cela pour le tenter, afin d'avoir de quoi l'accuser. Mais Yéhoshoua, s'étant penché en bas, écrivait avec le doigt sur la terre.
8:7	Et comme ils continuaient à l'interroger, s'étant relevé, il leur dit : Que celui de vous qui est sans péché jette le premier la pierre contre elle.
8:8	Et s'étant encore baissé, il écrivait sur la terre.
8:9	Mais eux, ayant entendu cela, et étant condamnés par leur conscience, ils se retirèrent un à un, en commençant depuis les plus anciens jusqu'aux derniers, et Yéhoshoua fut laissé seul, la femme se tenant au milieu.
8:10	Mais Yéhoshoua s'étant relevé, et ne voyant plus que la femme, il lui dit : Femme, où sont tes accusateurs ? Personne ne t'a-t-il condamnée ?
8:11	Et elle dit : Personne, Seigneur. Et Yéhoshoua lui dit : Je ne te condamne pas non plus : va, et ne pèche plus.

### Le témoignage de Yéhoshoua<!--Jn. 1:9.-->

8:12	Alors Yéhoshoua leur parla encore, en disant : Moi, JE SUIS la Lumière<!--Es. 60:20 ; Ps. 27:1.--> du monde. Celui qui me suit ne marchera jamais dans la ténèbre, mais il aura la lumière de la vie.
8:13	Alors les pharisiens lui dirent : Tu rends témoignage de toi-même, ton témoignage n'est pas vrai.
8:14	Yéhoshoua répondit et leur dit : Même si moi, je rends témoignage au sujet de moi-même, mon témoignage est vrai, car je sais d'où je suis venu et où je vais. Mais vous, vous ne savez pas d'où je viens ni où je vais.
8:15	Vous jugez selon la chair, mais moi, je ne juge personne.
8:16	Et si je juge, mon jugement est vrai, parce que je ne suis pas seul, mais avec moi est le Père qui m'a envoyé.
8:17	Mais il est aussi écrit dans votre torah que le témoignage de deux hommes est vrai<!--De. 19:15.-->.
8:18	C'est moi qui rends témoignage de moi-même, et le Père qui m'a envoyé rend aussi témoignage de moi.
8:19	Alors ils lui dirent : Où est ton Père ? Yéhoshoua répondit : Vous ne connaissez ni moi, ni mon Père. Si vous me connaissiez, vous connaîtriez aussi mon Père.
8:20	Yéhoshoua dit ces paroles dans le trésor, enseignant dans le temple, mais personne ne le saisit, parce que son heure n'était pas encore venue.
8:21	Alors Yéhoshoua leur dit encore : Je m'en vais et vous me chercherez et vous mourrez dans votre péché. Vous ne pouvez pas venir là où je vais.
8:22	Les Juifs disaient donc : Se tuera-t-il lui-même, puisqu'il dit : Vous ne pouvez pas venir là où je vais ?
8:23	Et il leur dit : Vous êtes d'en bas, mais moi, je suis d'en haut. Vous êtes de ce monde, mais moi, je ne suis pas de ce monde.
8:24	C'est pourquoi je vous ai dit que vous mourrez dans vos péchés. Car si vous ne croyez pas que moi je suis<!--Ici le Seigneur affirme sa divinité. Voir commentaire en Ex. 3:14 ; Jn. 8:58, 18:5-6.-->, vous mourrez dans vos péchés.
8:25	Alors ils lui dirent : Toi, qui es-tu ? Et Yéhoshoua leur dit : Le commencement<!--Vient du grec « arche » : La personne ou chose qui commence, la première personne ou chose d'une série, le premier, le chef, la cause. Jn. 1:1-2 ; Col. 1:18 ; Ap. 3:14, 21:6 et 22:13.-->, ce que je vous dis aussi.
8:26	J’ai beaucoup à dire sur vous et à juger, mais celui qui m'a envoyé est vrai. Et moi, et les choses que j'ai entendues de lui, je les dis pour le monde.
8:27	Ils ne comprirent pas qu'il leur parlait du Père.
8:28	Yéhoshoua leur dit donc : Quand vous aurez élevé le Fils d'humain, vous connaîtrez alors que je suis<!--Voir commentaire en Ex. 3:14 ; Jn. 8:58, 18:5-6.--> et que je ne fais rien de moi-même, mais que je dis ces choses selon ce que mon Père m'a enseigné.
8:29	Et celui qui m'a envoyé est avec moi. Le Père ne m'a pas laissé seul, parce que je fais toujours les choses qui lui plaisent.
8:30	Comme il disait ces choses, beaucoup crurent en lui.
8:31	Yéhoshoua donc disait aux Juifs qui avaient cru en lui : Si vous demeurez dans ma parole, vous serez vraiment mes disciples.
8:32	Et vous connaîtrez la vérité et la vérité vous rendra libres<!--Voir Jn. 8:36 ; Ga. 5:1 ; Ro. 6:18, 8:2,21.-->.
8:33	Ils lui répondirent : Nous sommes la postérité d'Abraham et nous n'avons jamais été esclaves de personne, comment donc dis-tu : Vous deviendrez libres ?
8:34	Yéhoshoua leur répondit : Amen, amen, je vous dis que quiconque pratique le péché est esclave du péché.
8:35	Or l'esclave ne demeure pas toujours dans la maison, le fils y demeure pour l'éternité.
8:36	Si donc le Fils vous rend libres, vous serez vraiment libres<!--Voir Jn. 8:32.-->.
8:37	Je sais que vous êtes la postérité d'Abraham, mais vous cherchez à me faire mourir, parce que ma parole ne trouve pas d'espace en vous.
8:38	Moi, je dis ce que j’ai vu chez mon Père et vous, par conséquent, vous faites ce que vous avez vu chez votre père.
8:39	Ils répondirent et lui dirent : Notre père c'est Abraham. Yéhoshoua leur dit : Si vous étiez enfants d'Abraham, vous feriez les œuvres d'Abraham.
8:40	Mais maintenant vous cherchez à me faire mourir, moi, un homme qui vous ai annoncé la vérité que j’ai entendue d'Elohîm. Cela, Abraham ne l'a pas fait.
8:41	Vous faites les œuvres de votre père. Alors ils lui dirent : Nous, nous ne sommes pas nés d'une relation sexuelle illicite<!--Adultère, fornication, homosexualité, lesbienne, rapports avec des animaux etc.--> ! Nous avons un seul Père, Elohîm.
8:42	Yéhoshoua donc leur dit : Si Elohîm était votre Père, vous m'aimeriez, car c'est d'Elohîm que je suis sorti et que je viens. Car je ne suis pas venu de moi-même, mais c'est lui qui m'a envoyé.
8:43	En raison de quoi ne comprenez-vous pas mon langage ? C'est parce que vous ne pouvez pas écouter ma parole.
8:44	Le père dont vous êtes issus c'est le diable, et vous voulez accomplir les désirs de votre père. Il a été meurtrier dès le commencement, et il ne s'est pas tenu dans la vérité, parce que la vérité n'est pas en lui. Quand il parle le mensonge, il parle à partir des biens qui lui sont en propre, parce qu'il est menteur et le père du mensonge.
8:45	Mais pour moi, parce que je dis la vérité, vous ne me croyez pas.
8:46	Qui d'entre vous me convainc de péché ? Et si je dis la vérité, en raison de quoi ne me croyez-vous pas ?
8:47	Celui qui est issu d'Elohîm entend les paroles d'Elohîm. Mais vous n'entendez pas, parce que vous n'êtes pas issus d'Elohîm.
8:48	Alors les Juifs répondirent, et lui dirent : N'avons-nous pas raison de dire que tu es un Samaritain et que tu as un démon ?
8:49	Yéhoshoua répondit : Je n'ai pas de démon, mais j'honore mon Père et vous, vous me déshonorez.
8:50	Or je ne cherche pas ma gloire. Il y en a un qui la cherche et qui juge.
8:51	Amen, amen, je vous le dis : Si quelqu'un garde ma parole, il ne verra pas la mort, à jamais.
8:52	Les Juifs lui dirent donc : Maintenant, nous savons que tu as un démon. Abraham est mort, et les prophètes aussi, et toi tu dis : Si quelqu'un garde ma parole, il ne goûtera pas la mort, à jamais !
8:53	Es-tu plus grand que notre père Abraham qui est mort ? Les prophètes aussi sont morts. Qui te fais-tu toi-même ?
8:54	Yéhoshoua répondit : Si je me glorifie moi-même, ma gloire n'est rien. C'est mon Père qui me glorifie, lui que vous dites être votre Elohîm.
8:55	Et vous ne l’avez pas connu, mais moi je le connais. Et si je disais que je ne le connais pas, je serai semblable à vous : un menteur. Mais je le connais et je garde sa parole.
8:56	Abraham, votre père, a exulté de ce qu'il verrait mon jour. Et il l'a vu et il s'est réjoui.
8:57	Les Juifs lui dirent donc : Tu n'as pas encore 50 ans et tu as vu Abraham !
8:58	Yéhoshoua leur dit : Amen, amen, je vous le dis : Avant qu'Abraham soit venu à l'existence, moi, JE SUIS<!--L'évangile de Yohanan (Jean) rapporte plusieurs déclarations incroyables que Yéhoshoua a faites à son sujet : Je suis le Pain de vie (6:35), Je suis la Lumière du monde (8:12), Je suis le Bon Berger (10:11), Je suis la Porte (10:7), Je suis la Résurrection (11:25), Je suis la Voie, la Vérité et la Vie (14:6), Je suis le véritable cep (15:1). Toutefois, dans ce verset, en déclarant être « Je suis », il s'identifie clairement au Nom que YHWH avait révélé à Moshé dans Ex. 3:14. C'est précisément pour cette raison que les Juifs ont voulu le lapider.-->.
8:59	Alors ils prirent des pierres pour les jeter contre lui, mais Yéhoshoua se cacha et sortit du temple, passant au milieu d'eux. Et c'est ainsi qu'il s'en alla.

## Chapitre 9

### Yéhoshoua guérit un aveugle-né

9:1	Et en passant, il vit un homme aveugle de naissance.
9:2	Et ses disciples l'interrogèrent, en disant : Rabbi, qui a péché ? Celui-ci, ou son père, ou sa mère, pour qu'il soit né aveugle ?
9:3	Yéhoshoua répondit : Ni celui-ci, ni son père, ni sa mère n'ont péché, mais c'est afin que les œuvres d'Elohîm soient manifestées en lui.
9:4	Il me faut travailler aux œuvres de celui qui m'a envoyé, pendant qu’il est jour : la nuit vient où personne ne peut travailler.
9:5	Pendant que je suis dans le monde, je suis la Lumière du monde.
9:6	Ayant dit ces paroles, il cracha à terre et fit de l'argile<!--« Argile », « glaise », que les potiers utilisent. Voir Ro. 9:21.--> avec sa salive, et mit de cette argile sur les yeux de l'aveugle.
9:7	Et il lui dit : Va et lave-toi au réservoir de Siloé (ce qui est interprété envoyé). Il y alla donc, se lava et s'en retourna voyant clair.
9:8	Les voisins donc, et ceux qui le voyaient auparavant lorsqu'il était aveugle, disaient : N'est-ce pas celui qui était assis et qui demandait l'aumône ?
9:9	Les uns disaient : C'est lui. D’autres disaient : c’est quelqu’un qui lui ressemble. Mais lui-même disait : C'est moi.
9:10	Ils lui disaient donc : Comment tes yeux ont-ils été ouverts ?
9:11	Il répondit et dit : Un humain appelé Yéhoshoua, a fait de l'argile et m'a oint les yeux, et m'a dit : Va au réservoir de Siloé et lave-toi. Or étant allé et m’étant lavé, j’ai recouvré la vue.
9:12	Alors ils lui dirent : Où est-il celui-là ? Il dit : Je ne sais pas.
9:13	Ils amenèrent aux pharisiens celui qui auparavant avait été aveugle.
9:14	Or c'était un shabbat quand Yéhoshoua avait fait de l'argile et lui avait ouvert les yeux.
9:15	Alors les pharisiens lui demandèrent de nouveau comment il avait recouvré la vue. Et il leur dit : Il a mis de l'argile sur mes yeux, et je me suis lavé, et je vois.
9:16	Alors certains des pharisiens disaient : Cet homme n'est pas d'Elohîm, parce qu'il n'observe pas le shabbat. D'autres disaient : Comment un homme pécheur peut-il produire de tels signes ? Et il y avait division parmi eux.
9:17	Ils disent encore à l'aveugle : Toi, que dis-tu de lui, sur ce qu'il t'a ouvert les yeux ? Il dit : C'est un prophète.
9:18	Les Juifs donc ne crurent pas à ceci : qu'il avait été aveugle et qu'il avait recouvré la vue, jusqu'à ce qu'ils eurent appelé les parents de celui qui avait recouvré la vue.
9:19	Et ils les interrogèrent en disant : Celui-ci est-il votre fils que vous dites être né aveugle ? Comment donc voit-il maintenant ?
9:20	Ses parents leur répondirent et dirent : Nous savons que celui-ci est notre fils et qu'il est né aveugle.
9:21	Mais comment il voit maintenant, ou qui lui a ouvert les yeux, nous ne le savons pas. Interrogez-le lui-même, il a de l’âge, lui-même parlera de ce qui le concerne.
9:22	Ses parents dirent cela parce qu’ils avaient peur des Juifs. Car les Juifs étaient déjà convenus que si quelqu'un le confessait<!--Jn. 12:42 ; Ro. 10:9 ; 1 Jn. 4:2-3.--> comme le Mashiah, il serait exclu de la synagogue.
9:23	Pour cette raison ses parents dirent : Il a de l'âge, interrogez-le lui-même.
9:24	Ils appelèrent donc pour la seconde fois l'homme qui avait été aveugle, et ils lui dirent : Donne gloire à Elohîm ! Nous savons que cet homme est un pécheur.
9:25	Lui donc répondit et dit : Je ne sais pas si c'est un pécheur ; je sais une chose, c'est que j'étais aveugle et que maintenant je vois.
9:26	Mais ils lui dirent de nouveau : Que t'a-t-il fait ? Comment a-t-il ouvert tes yeux ?
9:27	Il leur répondit : Je vous l'ai déjà dit, et vous ne l'avez pas entendu ? Pourquoi voulez-vous l'entendre encore ? Voulez-vous aussi devenir ses disciples ?
9:28	Alors ils l'injurièrent et lui dirent : Toi, tu es le disciple de celui-là ! Nous, nous sommes disciples de Moshé.
9:29	Nous savons qu'Elohîm a parlé à Moshé, mais celui-ci, nous ne savons pas d'où il est.
9:30	Cet homme répondit et leur dit : Voilà en effet ce qui est étonnant, que vous, vous ne sachiez pas d'où il est, et il m'a ouvert les yeux !
9:31	Or nous savons qu'Elohîm n'exauce pas les pécheurs, mais si quelqu'un est pieux envers Elohîm et fait sa volonté, il l'exauce.
9:32	On n'a jamais entendu dire que quelqu'un ait ouvert les yeux d'un aveugle-né.
9:33	Si celui-ci n'était pas d'Elohîm, il ne pourrait rien faire.
9:34	Ils répondirent et lui dirent : Tu as été engendré tout entier dans les péchés et tu nous enseignes ! Et ils le jetèrent dehors.
9:35	Yéhoshoua apprit qu'ils l'avaient jeté dehors. Et, l'ayant trouvé, il lui dit : Crois-tu au Fils d'Elohîm ?
9:36	Il répondit et dit : Qui est-il Seigneur, afin que je croie en lui ?
9:37	Et Yéhoshoua lui dit : Tu l'as vu, et c'est celui qui te parle.
9:38	Et il dit : Je crois, Seigneur. Et il l'adora<!--Au travers de la lecture de la Bible, on constate que les anges refusent l'adoration (Ap. 19:9-10) de même que les apôtres (Ac. 10:25-26, 14:5-18). Seul Elohîm accepte l'adoration puisqu'il en est le seul digne. Yéhoshoua n'a jamais refusé l'adoration des hommes, car il est Elohîm.-->.
9:39	Et Yéhoshoua dit : Je suis venu dans ce monde pour exercer le jugement, afin que ceux qui ne voient pas voient, et que ceux qui voient deviennent aveugles.
9:40	Et ceux des pharisiens qui étaient avec lui entendirent cela et lui dirent : Et nous, sommes-nous aussi aveugles ?
9:41	Yéhoshoua leur dit : Si vous étiez aveugles, vous n'auriez pas de péché. Mais maintenant vous dites : Nous voyons. C'est à cause de cela que votre péché demeure.

## Chapitre 10

### Yéhoshoua, le Bon Berger<!--Ps. 23 ; Hé. 13:20 ; 1 Pi. 5:4.-->

10:1	Amen, amen, je vous le dis : Celui qui n'entre pas par la porte dans la bergerie des brebis, mais qui y monte par ailleurs, celui-là est un voleur et un brigand.
10:2	Mais celui qui entre par la porte est le berger des brebis.
10:3	C'est à celui-ci que le gardien de la porte ouvre, et les brebis entendent sa voix, et il appelle ses propres brebis par leur nom et il les conduit dehors.
10:4	Et quand il a fait sortir toutes ses propres brebis, il marche devant elles et les brebis le suivent parce qu'elles connaissent sa voix.
10:5	Mais elles ne suivront jamais un étranger, mais elles fuiront loin de lui, parce qu'elles ne connaissent pas la voix des étrangers.
10:6	Yéhoshoua leur dit cette parabole, mais ils ne comprirent pas quelles étaient les choses dont il leur parlait.
10:7	Yéhoshoua donc leur dit encore : Amen, amen, je vous le dis : Je suis la Porte des brebis<!--Les animaux destinés au sacrifice passaient sans doute par la porte des brebis construite du temps de Nehemyah (Néhémie) (Né. 3:1).-->.
10:8	Tous ceux qui sont venus avant moi sont des voleurs et des brigands, mais les brebis ne les ont pas écoutés.
10:9	Moi, je suis la Porte. Si quelqu'un entre par moi, il sera sauvé. Il entrera et il sortira, et il trouvera du pâturage.
10:10	Le voleur ne vient que pour voler, et tuer et détruire. Moi, je suis venu afin qu'elles aient la vie et qu'elles l'aient même en abondance.
10:11	Moi, je suis le Bon Berger. Le Bon Berger dépose<!--Mettre, poser, placer.--> son âme en faveur de ses brebis.
10:12	Mais le mercenaire, qui n'est pas le berger, et auquel les brebis n’appartiennent pas en propre, voit venir le loup, abandonne les brebis et s'enfuit, et le loup arrache et disperse les brebis.
10:13	Mais le mercenaire s'enfuit, parce qu'il est mercenaire et qu'il ne se soucie pas des brebis.
10:14	Moi, je suis le Bon Berger. Je connais les miens et les miens me connaissent,
10:15	comme le Père me connaît, je connais aussi le Père, et je dépose mon âme en faveur de mes brebis.
10:16	J'ai encore d'autres brebis qui ne sont pas de cette bergerie. Celles-là aussi, il faut que je les amène ; elles entendront ma voix et elles deviendront un seul troupeau, un seul Berger.
10:17	À cause de ceci le Père m'aime, parce que je dépose mon âme afin de la reprendre.
10:18	Personne ne me l'ôte, mais je la dépose de moi-même. J'ai le pouvoir de la déposer, et j'ai le pouvoir de la reprendre. J'ai reçu cet ordre de mon Père.
10:19	Il y eut de nouveau de la division parmi les Juifs à cause de ces discours.
10:20	Mais beaucoup d'entre eux disaient : Il a un démon, il est fou ! Pourquoi l'écoutez-vous ?
10:21	Et les autres disaient : Ce ne sont pas les paroles d'un démoniaque. Un démon peut-il ouvrir les yeux des aveugles ?

### Yéhoshoua affirme sa divinité<!--Jn. 5:26-27, 14:9, 20:28-29.-->

10:22	Or arriva à Yeroushalaim la fête de la Dédicace<!--Le mot « dédicace » se dit « chanukkah » en hébreu, ce qui signifie « consécration ». Ce terme est employé pour l'inauguration et la consécration de l'autel destiné aux sacrifices rituels (No. 7:10-88), du temple (1 R. 8:63 ; 2 Ch. 7:5) ou encore pour les murailles de Yeroushalaim (Jérusalem) (Né. 12:27). La fête de Hanoukka, célébrée chaque année le 25 du mois de Chislev (mi-décembre), fut instituée en 164 av. J.-C. par Yéhouda Makabi (Judas Maccabée) pour commémorer la purification du temple profané par Antiochos Épiphane en 168 et 167 av. J.-C.-->, et c'était l'hiver.
10:23	Et Yéhoshoua marchait dans le temple, sous le portique de Shelomoh.
10:24	Les Juifs l'encerclèrent donc et lui dirent : Jusqu'à quand tiens-tu notre âme en suspens ? Si tu es le Mashiah, dis-le-nous franchement.
10:25	Yéhoshoua leur répondit : Je vous l'ai dit et vous ne le croyez pas. Les œuvres que je fais au nom de mon Père rendent témoignage de moi.
10:26	Mais vous ne croyez pas, car vous n'êtes pas de mes brebis, comme je vous l'ai dit.
10:27	Mes brebis entendent ma voix, je les connais, et elles me suivent.
10:28	Et moi, je leur donne la vie éternelle, et elles ne périront jamais, et personne ne les arrachera de ma main.
10:29	Mon Père, qui me les a données, est plus grand que tous et, personne ne peut les arracher de la main de mon Père.
10:30	Moi et le Père nous sommes un.
10:31	Alors les Juifs prirent de nouveau des pierres pour le lapider.
10:32	Et Yéhoshoua leur répondit : Je vous ai fait voir beaucoup de bonnes œuvres de la part de mon Père : Pour laquelle me lapidez-vous ?
10:33	Les Juifs répondirent, en lui disant : Nous ne te lapidons pas pour quelque bonne œuvre, mais pour un blasphème, et parce que, n'étant qu'un être humain, tu te fais Elohîm.
10:34	Yéhoshoua leur répondit : N'est-il pas écrit dans votre torah : J'ai dit : Vous êtes des elohîm<!--Ps. 82:6 : le sens du mot « elohîm » peut désigner des personnes ayant un certain pouvoir. D'ailleurs, le mot hébreu utilisé dans Ps. 82:6 est « elohîm », or ce mot signifie aussi « juge ». De plus, dans le contexte du psaume, « vous êtes des elohîm » ne s'applique pas à tous, mais seulement à une certaine catégorie de personnes qui exerçaient un pouvoir en Israël : rois, scribes, grands-prêtres, etc. Rappelons-nous aussi qu'Elohîm a fait de Moshé un elohîm pour Aaron (Ex. 7:1-2), mais cela n'a pas fait de lui l'Elohîm Créateur pour autant. En Jn. 17:3, Yéhoshoua atteste qu'il n'y a qu'un seul vrai Elohîm. Satan veut nous faire croire que nous sommes des elohîm et nous amener ainsi à pécher par l'orgueil (Ge. 3:5). Toutefois, comme le souligne si bien Paulos (Paul), même s'il existe des créatures qu'on appelle elohîm, il ne reste pas moins vrai qu'il n'y a qu'un seul Elohîm (1 Co. 8:5-7).--> ?
10:35	Si elle a appelé elohîm ceux à qui la parole d'Elohîm a été adressée, et l'Écriture ne peut être renversée,
10:36	celui que le Père a sanctifié et envoyé dans le monde, vous lui dites : «Tu blasphèmes», parce que j'ai dit : «Je suis le Fils d'Elohîm.»
10:37	Si je ne fais pas les œuvres de mon Père, ne me croyez pas.
10:38	Mais si je les fais, et que vous ne vouliez pas me croire, croyez à ces œuvres ; afin que vous sachiez et que vous croyiez que le Père est en moi, et moi en lui.
10:39	Ils cherchaient donc encore à le saisir, mais il s'échappa de leurs mains.
10:40	Et il s'en alla de nouveau au-delà du Yarden, à l'endroit où Yohanan baptisait d'abord, et il demeura là.
10:41	Et beaucoup de gens vinrent à lui et ils disaient : En effet, Yohanan n'a produit aucun signe, mais toutes les choses que Yohanan a dites à propos de celui-ci étaient vraies.
10:42	Et dans ce lieu-là, beaucoup crurent en lui.

## Chapitre 11

### Résurrection d'Èl’azar (Lazare) de Béthanie : Martha reconnaît Yéhoshoua comme étant le Mashiah

11:1	Mais il y avait un certain malade, Èl’azar<!--« El a secouru. » Ex. 6:23.-->, de Béthanie, le village de Myriam et de Martha, sa sœur.
11:2	Or, c'était cette Myriam qui oignit le Seigneur d'un baume et qui essuya ses pieds avec ses cheveux. Son frère Èl’azar était malade.
11:3	Les sœurs envoyèrent donc quelqu'un vers lui pour lui dire : Seigneur, voici que celui que tu aimes est malade.
11:4	Mais Yéhoshoua l'ayant entendu, dit : Cette maladie n'est pas à la mort, mais elle est en faveur de la gloire d'Elohîm, afin que le Fils d'Elohîm soit glorifié par elle.
11:5	Or Yéhoshoua aimait Martha, et sa sœur, et Èl’azar.
11:6	Ayant donc appris qu'il était malade, il resta encore en effet deux jours à l'endroit où il était.
11:7	Après quoi il dit alors à ses disciples : Retournons en Judée.
11:8	Les disciples lui dirent : Rabbi, les Juifs tout récemment cherchaient à te lapider et tu y vas encore !
11:9	Yéhoshoua répondit : N'y a-t-il pas douze heures dans le jour ? Si quelqu'un marche pendant le jour, il ne trébuche pas, car il voit la lumière de ce monde.
11:10	Mais si quelqu'un marche pendant la nuit, il trébuche, parce que la lumière n'est pas en lui.
11:11	Il parla ainsi, et après cela il leur dit : Notre ami Èl’azar s'est endormi, mais je vais afin de le réveiller.
11:12	Ses disciples lui dirent donc : Seigneur, s'il s'est endormi, il sera sauvé.
11:13	Or Yéhoshoua avait parlé de sa mort, mais ils pensaient qu'il parlait du repos du sommeil.
11:14	Yéhoshoua donc leur dit alors ouvertement : Èl’azar est mort.
11:15	Et je me réjouis, à cause de vous, de ce que je n'étais pas là, afin que vous croyiez. Mais allons vers lui.
11:16	Alors Thomas, appelé Didymos<!--Deux, jumeaux.-->, dit aux compagnons disciples : Allons, nous aussi, afin de mourir avec lui.
11:17	Yéhoshoua, étant donc arrivé, trouva qu'il était déjà depuis quatre jours dans le sépulcre.
11:18	Or Béthanie était près de Yeroushalaim, à quinze stades environ<!--Trois kilomètres.-->.
11:19	Et beaucoup de Juifs étaient venus vers Martha et Myriam pour les consoler au sujet de leur frère.
11:20	Martha ayant donc appris que Yéhoshoua venait, alla au-devant de lui, mais Myriam se tenait assise à la maison.
11:21	Martha donc dit à Yéhoshoua : Seigneur, si tu avais été ici, mon frère ne serait pas mort.
11:22	Mais, même à présent, je sais que tout ce que tu demanderas à Elohîm, Elohîm te le donnera.
11:23	Yéhoshoua lui dit : Ton frère ressuscitera.
11:24	Martha lui dit : Je sais qu'il ressuscitera à la résurrection, au dernier jour.
11:25	Yéhoshoua lui dit : Moi, JE SUIS la Résurrection<!--Ps. 68:21.--> et la Vie<!--De. 30:20.-->. Celui qui croit en moi vivra même s'il meurt.
11:26	Et quiconque vit et croit en moi ne mourra pas, à jamais. Crois-tu cela ?
11:27	Elle lui dit : Oui, Seigneur, je crois que tu es le Mashiah, le Fils d'Elohîm, qui vient dans le monde.
11:28	Et ayant dit cela, elle s'en alla et appela secrètement Myriam, sa sœur, en disant : Le Docteur est ici et il t'appelle.
11:29	Celle-ci, l’ayant entendu, se réveilla promptement et vint vers lui.
11:30	Or Yéhoshoua n'était pas encore entré dans le village, mais il était au lieu où Martha l'avait rencontré.
11:31	Les Juifs donc qui étaient avec elle dans la maison et qui la consolaient, ayant vu que Myriam s'était levée rapidement et qu'elle était sortie, la suivirent en disant : Elle va au sépulcre pour y pleurer.
11:32	Alors Myriam arriva là où était Yéhoshoua. Et quand elle le vit, elle tomba à ses pieds, en lui disant : Seigneur, si tu avais été ici, mon frère ne serait pas mort.
11:33	Alors quand Yéhoshoua la vit pleurant, et les Juifs qui étaient venus avec elle, pleurant aussi, il donna un sérieux avertissement en esprit et fut troublé.
11:34	Et il dit : Où l'avez-vous déposé ? Ils lui dirent : Seigneur, viens et vois.
11:35	Yéhoshoua pleura.
11:36	Les Juifs disaient donc : Voyez comme il l'aimait.
11:37	Mais quelques-uns d'entre eux disaient : Lui qui a ouvert les yeux de l'aveugle, ne pouvait-il pas faire aussi que celui-ci ne meure pas ?
11:38	Alors Yéhoshoua, donnant de nouveau un sérieux avertissement en lui-même, se rendit au sépulcre. C'était une grotte, et il y avait une pierre placée devant.
11:39	Yéhoshoua dit : Ôtez la pierre ! Mais Martha, la sœur du mort, lui dit : Seigneur, il sent déjà, car il est au quatrième jour.
11:40	Yéhoshoua lui dit : Ne t'ai-je pas dit que si tu crois tu verras la gloire d'Elohîm ?
11:41	Ils ôtèrent donc la pierre de dessus le lieu où le mort était couché. Et Yéhoshoua levant ses yeux au ciel, dit : Père, je te rends grâce de ce que tu m'as entendu.
11:42	Or moi je savais que tu m’entends toujours, mais je l'ai dit à cause de la foule qui est autour de moi, afin qu'elle croie que tu m'as envoyé.
11:43	Et ayant dit ces choses, il cria d'une grande voix : Èl’azar, sors dehors !
11:44	Alors le mort sortit<!--Ps. 68:21.-->, ayant les mains et les pieds liés de bandes, et son visage était enveloppé d'un mouchoir. Yéhoshoua leur dit : Déliez-le et laissez-le aller.
11:45	Alors beaucoup de Juifs qui étaient venus vers Myriam et qui avaient vu ce que Yéhoshoua avait fait, crurent en lui.
11:46	Mais quelques-uns d'entre eux allèrent vers les pharisiens et leur dirent les choses que Yéhoshoua avait faites.

### Les principaux prêtres et les pharisiens complotent contre Yéhoshoua<!--Jn. 12:10-11.-->

11:47	Alors les principaux prêtres et les pharisiens rassemblèrent le sanhédrin, et ils dirent : Que ferons-nous ? Parce que cet homme produit beaucoup de signes.
11:48	Si nous le laissons faire, tout le monde croira en lui, et les Romains viendront et ils détruiront et ce lieu et notre nation.
11:49	Mais l’un d’entre eux, Kaïaphas, étant le grand-prêtre cette année-là, leur dit : Vous n'y comprenez rien,
11:50	et vous ne raisonnez pas qu'il nous est profitable qu'un homme meure en faveur du peuple, et que toute la nation ne périsse pas.
11:51	Or il ne dit pas cela de lui-même, mais étant grand-prêtre cette année-là, il prophétisa que Yéhoshoua était sur le point de mourir en faveur de la nation.
11:52	Et non pas seulement en faveur de la nation, mais aussi afin qu'il rassemblât en un seul corps les enfants d'Elohîm dispersés.
11:53	Alors depuis ce jour-là, ils se concertèrent ensemble pour le faire mourir.
11:54	Yéhoshoua donc ne marchait plus ouvertement parmi les Juifs, mais il se retira dans la contrée voisine du désert, dans une ville appelée Éphraïm. Et il demeura là avec ses disciples.
11:55	Or la Pâque des Juifs était proche. Et beaucoup montèrent de la région à Yeroushalaim avant la Pâque, afin de se purifier.
11:56	Ils cherchaient donc Yéhoshoua et se disaient les uns les autres, se tenant là dans le temple : Que pensez-vous de ce qu'il ne vient pas à la fête ?
11:57	Or les principaux prêtres et les pharisiens avaient donné l'ordre que, si quelqu'un savait où il était, il le déclarât, afin qu'on se saisît de lui.

## Chapitre 12

### Myriam (Marie) de Béthanie répand du baume sur Yéhoshoua<!--Mt. 26:6-13 ; Mc. 14:3-9.-->

12:1	Six jours avant la Pâque, Yéhoshoua donc arriva à Béthanie où était Èl’azar, le mort, qu'il avait réveillé d'entre les morts.
12:2	On lui fit donc là un souper, et Martha servait. Et Èl’azar était un de ceux qui étaient à table avec lui.
12:3	Alors Myriam, ayant pris une livre d'un baume de nard pur de grand prix, en oignit les pieds de Yéhoshoua et les essuya avec ses cheveux. Et la maison fut remplie de l'odeur du baume.
12:4	Alors Yéhouda, de Shim’ôn Iskariote, l'un de ses disciples, qui était sur le point de le livrer, dit :
12:5	En raison de quoi ce baume n'a-t-il pas été vendu 300 deniers et donné aux pauvres ?
12:6	Or il dit cela, non parce qu'il prenait soin des pauvres, mais parce qu'il était voleur<!--Vient du Grec « kleptes » qui signifie : un détourneur, un chapardeur. Kleptes était le nom donné aux faux enseignants qui ne s'occupaient pas d'instruire les gens, mais abusaient de la confiance pour leur propre gain.-->, et que, tenant la bourse, il prenait ce qu'on y mettait.
12:7	Alors Yéhoshoua lui dit : Laisse-la ! Elle l'a gardé pour le jour de ma sépulture.
12:8	Car vous avez toujours les pauvres avec vous, mais moi, vous ne m’avez pas toujours.
12:9	Alors une grande foule de Juifs ayant su qu'il était là et ils vinrent, non seulement à cause de Yéhoshoua, mais aussi pour voir Èl’azar, qu'il avait réveillé d'entre les morts.
12:10	Mais les principaux prêtres résolurent de faire mourir aussi Èl’azar,
12:11	parce que beaucoup de Juifs se retiraient d'eux à cause de lui et croyaient en Yéhoshoua.

### Entrée de Yéhoshoua à Yeroushalaim (Jérusalem)<!--Za. 9:9 ; Mt. 21:1-11 ; Mc. 11:1-11 ; Lu. 19:28-40 ; Ap. 19:11-16.-->

12:12	Le lendemain, une grande foule qui était venue à la fête ayant entendu dire que Yéhoshoua se rendait à Yeroushalaim,
12:13	ils prirent des branches de palmiers et sortirent à sa rencontre, et ils criaient : Hosanna ! Béni soit celui qui vient au nom du Seigneur, le roi d’Israël !
12:14	Et Yéhoshoua ayant trouvé un ânon, s'assit dessus, selon qu'il est écrit :
12:15	N'aie pas peur, fille de Sion ! Voici que ton Roi vient, assis sur l’ânon d’une ânesse<!--Za. 9:9.-->.
12:16	Or ses disciples ne comprirent pas d'abord ces choses, mais, quand Yéhoshoua fut glorifié, ils se souvinrent alors qu'elles étaient écrites de lui et qu’on les lui avait faites.
12:17	La foule qui était avec lui, quand il avait appelé Èl’azar hors du sépulcre et l'avait réveillé d'entre les morts, rendait témoignage.
12:18	C'est aussi pour cela que la foule alla au-devant de lui, parce qu'elle avait entendu dire qu'il avait produit ce signe.
12:19	Les pharisiens donc disaient entre eux : Vous ne voyez pas que vous ne gagnez rien ? Voilà que le monde s’en est allé après lui.
12:20	Or il y avait quelques Grecs d'entre ceux qui étaient montés pour adorer pendant la fête,
12:21	qui s'approchèrent de Philippos, qui était de Bethsaïda de Galilée, et le prièrent en disant : Seigneur, nous désirons voir Yéhoshoua.
12:22	Philippos vient et le dit à Andreas, et de nouveau Andreas et Philippos le disent à Yéhoshoua.

### Yéhoshoua annonce sa crucifixion

12:23	Et Yéhoshoua leur répondit en disant : L'heure est venue pour que le Fils d'humain soit glorifié.
12:24	Amen, amen, je vous le dis : Si le grain de blé qui est tombé en terre ne meurt, il reste seul ; mais s'il meurt, il porte beaucoup de fruit.
12:25	Celui qui aime son âme la détruira, et celui qui hait son âme dans ce monde la gardera pour la vie éternelle.
12:26	Si quelqu'un me sert, qu'il me suive, et là où je suis, là aussi sera mon serviteur. Et si quelqu'un me sert, le Père l'honorera.
12:27	Maintenant mon âme est troublée. Et que dirai-je ? Père, sauve-moi hors de cette heure ? Mais c’est à cause de cela que je suis venu en vue de cette heure.
12:28	Père, glorifie ton nom ! Alors une voix vint du ciel : Je l'ai glorifié et je le glorifierai encore.
12:29	La foule donc qui était là et qui avait entendu disait qu'un tonnerre était arrivé. D'autres disaient : Un ange lui a parlé.
12:30	Yéhoshoua répondit et dit : Ce n'est pas à cause de moi que cette voix est arrivée, mais à cause de vous.
12:31	C'est maintenant le jugement de ce monde, maintenant le chef<!--Un prince, un chef, un meneur, un magistrat. Voir Jn. 14:30, 16:11 et Ep. 2:2.--> de ce monde sera jeté dehors.
12:32	Et moi, quand je serai élevé de la Terre, je tirerai<!--Traîner. Voir Jn. 6:44.--> tout le monde à moi.
12:33	Or il disait cela pour indiquer de quelle mort il était sur le point de mourir.
12:34	La foule lui répondit : Nous avons appris par la torah que le Mashiah demeure pour l’éternité et comment dis-tu, toi, qu’il faut que le Fils d'humain soit élevé ? Qui est ce Fils d'humain ?
12:35	Alors Yéhoshoua leur dit : C’est pour un peu de temps que la lumière est encore avec vous : marchez pendant que vous avez la lumière, de peur que la ténèbre ne vous saisisse, car celui qui marche dans la ténèbre ne sait où il va.
12:36	Pendant que vous avez la lumière, croyez en la lumière, afin que vous deveniez des fils de lumière<!--Les disciples de Yéhoshoua sont les enfants de la lumière (1 Th. 5:5 ; Ep. 5:8). Yéhoshoua est la lumière du monde (Jn. 8:12, 12:46).-->. Yéhoshoua dit ces choses, et il s'en alla et se cacha de devant eux.
12:37	Et bien qu’il eût fait de si grands signes devant eux, ils ne crurent pas en lui,
12:38	afin que fût accomplie la parole qu’a dite Yesha`yah le prophète : Seigneur, qui a cru ce qu'il a entendu de nous ? Et à qui a été révélé le bras du Seigneur<!--Es. 53:1 ; Ro. 10:16.--> ?
12:39	C’est pour cela qu’ils ne pouvaient croire, parce que Yesha`yah a dit encore :
12:40	Il a aveuglé leurs yeux et il a endurci leur cœur, de peur qu'ils ne voient de leurs yeux, qu'ils ne comprennent du cœur, qu'ils ne se convertissent et que je ne les guérisse<!--Es. 6:9-10.-->.
12:41	Yesha`yah dit ces choses quand il vit sa gloire et qu'il parla de lui.
12:42	Toutefois, il est vrai, même parmi les chefs, beaucoup crurent en lui, mais à cause des pharisiens, ils ne le confessaient<!--Jn. 9:22 ; Ro. 10:9 ; 1 Jn. 4:2-3.--> pas, de peur d'être exclus<!--Jn. 9:22, 16:2.--> de la synagogue.
12:43	Car ils aimèrent la gloire des humains plus que la gloire d'Elohîm.
12:44	Or Yéhoshoua s'écria et dit : Celui qui croit en moi, ne croit pas en moi, mais en celui qui m'a envoyé,
12:45	et celui qui me voit, voit celui qui m'a envoyé.
12:46	Moi, lumière, je suis venu dans le monde afin que quiconque croit en moi ne demeure pas dans la ténèbre.
12:47	Et si quelqu'un entend mes paroles et ne les croit pas, je ne le juge pas, car je ne suis pas venu pour juger le monde, mais pour sauver le monde.
12:48	Celui qui me rejette et qui ne reçoit pas mes paroles a son juge : la parole que j'ai annoncée sera celle qui le jugera dans le dernier jour.
12:49	Parce que moi, je n'ai pas parlé de moi-même, mais le Père qui m’a envoyé, lui, m’a donné commandement : que dire et comment parler.
12:50	Et je sais que son commandement est la vie éternelle. Les choses donc que moi je dis, je les dis comme le Père m’a dit.

## Chapitre 13

### Yéhoshoua lave les pieds de ses disciples<!--Mt. 26:20-24 ; Mc. 14:17 ; Lu. 22:14,21-23.-->

13:1	Or avant la fête de Pâque, Yéhoshoua, sachant que son heure était venue de passer de ce monde vers le Père, comme il avait aimé les siens dans le monde, il les aima jusqu'à la fin.
13:2	Et au cours du souper, le diable ayant déjà jeté dans le cœur de Yéhouda, de Shim’ôn Iskariote, de le livrer,
13:3	Yéhoshoua, sachant que le Père lui a donné toutes choses entre les mains, qu'il sortit d'Elohîm et qu’il s’en va à Elohîm,
13:4	se lève du souper et pose ses vêtements et, ayant pris un linge, il se ceignit.
13:5	Ensuite il verse de l'eau dans un bassin, et il se mit à laver les pieds de ses disciples, et à les essuyer avec le linge dont il était ceint.
13:6	Alors il vient à Shim’ôn Petros, et celui-ci lui dit : Toi, Seigneur, tu me laves les pieds !
13:7	Yéhoshoua répondit et lui dit : Ce que je fais, tu ne le sais pas maintenant, mais tu le comprendras dans la suite.
13:8	Petros lui dit : Non, tu ne me laveras jamais les pieds ! Yéhoshoua lui répondit : Si je ne te lave pas, tu n'as pas de part avec moi.
13:9	Shim’ôn Petros lui dit : Seigneur, non seulement mes pieds, mais aussi les mains et la tête.
13:10	Yéhoshoua lui dit : Celui qui s'est baigné n'a besoin que de se laver les pieds pour être entièrement pur, et vous êtes purs, mais pas tous.
13:11	Car il connaissait celui qui le livrait. C'est pour cela qu'il dit : Vous n'êtes pas tous purs.
13:12	Après donc qu'il leur eut lavé les pieds et qu'il eut repris ses vêtements, s'étant remis à table, il leur dit : Comprenez-vous ce que je vous ai fait ?
13:13	Vous m'appelez Docteur et Seigneur, et vous dites bien, car ainsi je suis.
13:14	Si donc je vous ai lavé les pieds, moi, le Seigneur et le Docteur, vous devez, vous aussi, vous laver les pieds les uns aux autres,
13:15	car je vous ai donné un exemple, afin que, comme je vous ai fait, vous aussi vous fassiez de même.
13:16	Amen, amen, je vous le dis : L'esclave n'est pas plus grand que son maître, ni l'apôtre plus grand que celui qui l'a envoyé.
13:17	Si vous savez ces choses, vous êtes bénis si vous les faites.
13:18	Je ne parle pas de vous tous : je connais ceux que j'ai choisis. Mais c'est afin que soit accomplie cette Écriture : Celui qui mange le pain avec moi, a levé son talon contre moi<!--Ps. 41:10.-->.
13:19	Je vous le dis dès maintenant, avant que la chose arrive, afin que, lorsqu'elle sera arrivée, vous croyiez que moi, JE SUIS<!--Voir Ex. 3:14-15.-->.
13:20	Amen, amen, je vous le dis : Celui qui reçoit celui que j'aurai envoyé me reçoit, et celui qui me reçoit reçoit celui qui m'a envoyé.

### Yéhoshoua annonce la trahison de Yéhouda Iskariote et le reniement de Petros (Pierre)<!--Mt. 26:21-25 ; Mc. 14:18-21 ; Lu. 22:21-23.-->

13:21	Ayant dit ces choses, Yéhoshoua fut troublé en son esprit, et il rendit témoignage et dit : Amen, amen, je vous le dis, l'un de vous me livrera.
13:22	Alors les disciples se regardent les uns les autres, étant perplexes : de qui parle-t-il ? 
13:23	Or un des disciples, celui que Yéhoshoua aimait, était à table couché sur le sein de Yéhoshoua.
13:24	Shim’ôn Petros lui fait donc signe et lui dit : Demande quel est celui dont il parle.
13:25	Et lui, s'étant penché sur la poitrine de Yéhoshoua, lui dit : Seigneur, qui est-ce ?
13:26	Yéhoshoua répond : C'est celui à qui je donnerai le morceau après l'avoir trempé. Et, ayant trempé le morceau, il le donne à Yéhouda, de Shim’ôn Iskariote.
13:27	Et après le morceau, alors Satan entra en lui. Yéhoshoua donc lui dit : Ce que tu fais, fais-le plus vite.
13:28	Mais aucun de ceux qui étaient à table ne comprit pourquoi il lui avait dit cela.
13:29	Car quelques-uns pensaient que, comme Yéhouda avait la bourse, Yéhoshoua lui disait : Achète ce qui nous est nécessaire pour la fête, ou : Donne quelque chose aux pauvres.
13:30	Celui-ci donc ayant pris le morceau, sortit immédiatement. Or c'était la nuit.
13:31	Quand il fut sorti, Yéhoshoua dit : Maintenant, le Fils d'humain est glorifié, et Elohîm est glorifié en lui.
13:32	Si Elohîm est glorifié en lui, Elohîm aussi le glorifiera en lui-même, et il le glorifiera immédiatement.
13:33	Petits enfants, je suis encore pour un peu de temps avec vous. Vous me chercherez, mais, comme j'ai dit aux Juifs : Vous ne pouvez pas venir là où je vais, je vous le dis aussi maintenant.
13:34	Je vous donne un nouveau commandement : Aimez-vous les uns les autres. Comme je vous ai aimés, vous aussi, aimez-vous les uns les autres.
13:35	À ceci tous connaîtront que vous êtes mes disciples, si vous avez de l'amour les uns pour les autres.
13:36	Shim’ôn Petros lui dit : Seigneur, où vas-tu ? Yéhoshoua lui répondit : Là où je vais, tu ne peux pas me suivre maintenant, mais tu me suivras plus tard.
13:37	Petros lui dit : Seigneur, en raison de quoi ne puis-je pas te suivre maintenant ? Je déposerai mon âme en ta faveur.
13:38	Yéhoshoua lui répondit : Tu déposeras ton âme en ma faveur ? Amen, amen, je te le dis, le coq ne chantera pas, que tu ne m’aies renié trois fois.

## Chapitre 14

### Yéhoshoua est le Chemin, la Vérité et la Vie

14:1	Que votre cœur ne se trouble pas. Vous croyez en Elohîm, croyez aussi en moi.
14:2	Dans la maison de mon Père il y a beaucoup de demeures. S’il en était autrement, je vous l'aurais dit. Je vais vous préparer une place.
14:3	Et quand je serai allé et que je vous aurai préparé une place, je viens de nouveau, et je vous prendrai avec moi, afin que là où je suis, vous y soyez aussi.
14:4	Et vous savez où je vais, et vous en savez la voie.
14:5	Thomas lui dit : Seigneur, nous ne savons pas où tu vas, comment donc pouvons-nous en savoir la voie ?
14:6	Yéhoshoua lui dit : Moi, je suis la Voie<!--Voir Ps. 139:24 ; Pr. 15:24.-->, la Vérité et la Vie<!--De. 30:20.-->. Personne ne vient au Père excepté par moi.

### Le Père et le Fils sont un

14:7	Si vous me connaissiez, vous connaîtriez aussi mon Père. Et dès maintenant vous le connaissez et vous l'avez vu.
14:8	Philippos lui dit : Seigneur, montre-nous le Père, et cela nous suffit.
14:9	Yéhoshoua lui dit : Je suis avec vous depuis si longtemps et tu ne m'as pas connu, Philippos ! Celui qui m'a vu a vu le Père. Et comment dis-tu : Montre-nous le Père ?
14:10	Ne crois-tu pas que je suis dans le Père et que le Père est en moi ? Les paroles que je vous dis, je ne les dis pas de moi-même ; mais le Père qui demeure en moi est celui qui fait les œuvres.
14:11	Croyez-moi, que je suis dans le Père et que le Père est en moi. Sinon, croyez-moi à cause des œuvres elles-mêmes !
14:12	Amen, amen, je vous le dis, celui qui croit en moi fera, lui aussi, les œuvres que moi je fais, et il en fera de plus grandes que celles-ci, parce que je m'en vais vers mon Père.
14:13	Et tout ce que vous demanderez en mon nom, je le ferai, afin que le Père soit glorifié dans le Fils.
14:14	Si vous demandez en mon nom quelque chose, je le ferai.

### Le Saint-Esprit, le parakletos

14:15	Si vous m'aimez, gardez mes commandements.
14:16	Et moi, je prierai le Père et il vous donnera un autre parakletos<!--« Convoqué », « appelé aux côtés », « appelé à l'aide » ; « celui qui plaide la cause d'un autre », « un juge », « un plaideur », « un conseil pour la défense », « un assistant légal », « un avocat ». Voir 1 Jn. 2:1.-->, pour demeurer avec vous pour l’éternité,
14:17	l'Esprit de vérité que le monde ne peut recevoir parce qu'il ne le voit pas et qu'il ne le connaît pas. Mais vous, vous le connaissez, car il demeure avec vous et il sera en vous.
14:18	Je ne vous laisserai pas orphelins, je viens à vous.
14:19	Encore un peu et le monde ne me voit plus ; mais vous, vous me voyez, parce que, moi, je vis, et que vous aussi, vous vivrez.
14:20	En ce jour-là, vous connaîtrez que je suis en mon Père, et vous en moi, et moi en vous.
14:21	Celui qui a mes commandements et qui les garde, c'est celui qui m'aime. Or celui qui m’aime sera aimé de mon Père : et moi je l’aimerai et je me manifesterai moi-même à lui.
14:22	Yéhouda, non pas l'Iskariote, lui dit : Seigneur, qu'est-il arrivé pour que tu sois sur le point de te manifester à nous et non pas au monde ?
14:23	Yéhoshoua répondit et lui dit : Si quelqu'un m'aime, il gardera ma parole, et mon Père l'aimera, nous viendrons à lui et nous ferons notre demeure chez lui.
14:24	Celui qui ne m'aime pas ne garde pas mes paroles. Et la parole que vous entendez n'est pas la mienne, mais celle du Père qui m'a envoyé.
14:25	Je vous ai dit ces choses pendant que je demeure avec vous.
14:26	Mais le parakletos, le Saint-Esprit, que le Père enverra en mon nom, lui, vous enseignera toutes choses et il vous rappellera toutes les choses que je vous ai dites.

### Le Mashiah nous donne sa paix

14:27	Je vous laisse la paix, je vous donne ma paix. Je ne vous donne pas comme le monde donne. Que votre cœur ne se trouble pas et ne s'alarme pas.
14:28	Vous avez entendu que je vous ai dit : Je m'en vais et je viens vers vous. Si vous m'aimiez, vous vous réjouiriez de ce que j’ai dit : Je m'en vais au Père, parce que mon Père est plus grand que moi.
14:29	Et maintenant je vous l'ai dit avant que cela soit arrivé, afin que, quand il sera arrivé, vous croyiez.
14:30	Je ne parlerai plus beaucoup avec vous, car le chef de ce monde vient, et il n'a rien en moi.
14:31	Mais afin que le monde sache que j'aime le Père et que je fais ce que le Père m'a commandé, réveillez-vous ! Partons d'ici !

## Chapitre 15

### Le cep et les sarments

15:1	Moi, je suis le véritable cep<!--Yéhoshoua est l'arbre de vie qui produit de bons fruits en nous, à condition que nous nous tenions loin de l'arbre de la connaissance de ce qui est bon ou mauvais. Yéhoshoua, le vrai cep, est la Source de vie. La viabilité du sarment dépend de son attachement au cep. Yéhoshoua a été pendu au bois (Ac. 5:30), s'est chargé de nos malédictions (Ga. 3:13) et a été retranché à notre place.-->, et mon Père est le vigneron.
15:2	Tout sarment en moi ne portant pas de fruit, il le retranche, et il nettoie<!--nettoyer, de crasse, impuretés, saletés, tailler les arbres et vignes des pousses inutiles.--> tout porte-fruit, afin qu’il porte plus de fruit.
15:3	Déjà, vous, vous êtes purs par le moyen de la parole que je vous ai dite.
15:4	Demeurez en moi, et moi en vous. Comme le sarment ne peut de lui-même porter du fruit à moins qu'il ne demeure dans le cep, vous ne le pouvez pas non plus, à moins que vous ne demeuriez en moi.
15:5	Moi, je suis le cep, vous, les sarments. Celui qui demeure en moi, et moi en lui, celui-là porte beaucoup de fruit, car hors de moi vous ne pouvez rien produire.
15:6	Si quelqu’un ne demeure pas en moi, il est jeté dehors comme le sarment et il sèche ; et on le ramasse et on le jette au feu, et il brûle.
15:7	Si vous demeurez en moi et que mes paroles demeurent en vous, vous demanderez ce que vous voudrez et cela vous arrivera.
15:8	Mon Père est glorifié en ceci : que vous portiez beaucoup de fruits, et vous deviendrez mes disciples.
15:9	Tout comme le Père m'a aimé, moi aussi, je vous ai aimés. Demeurez dans mon amour.
15:10	Si vous gardez mes commandements, vous demeurerez dans mon amour, comme j'ai gardé les commandements de mon Père et je demeure dans son amour.
15:11	Je vous ai dit ces choses, afin que ma joie demeure en vous et que votre joie soit accomplie.
15:12	Ceci est mon commandement : Que vous vous aimiez les uns les autres, comme je vous ai aimés.
15:13	Personne n'a de plus grand amour que celui qui dépose son âme en faveur de ses amis.
15:14	Vous êtes mes amis, si vous faites tout ce que je vous commande.
15:15	Je ne vous appelle plus esclaves, car l'esclave ne sait pas ce que fait son seigneur, mais je vous ai appelés amis, parce que je vous ai fait connaître tout ce que j'ai appris de mon Père.
15:16	Ce n'est pas vous qui m'avez choisi, mais moi, je vous ai choisis, et je vous ai établis, afin que vous alliez, que vous produisiez du fruit et que votre fruit demeure, afin que tout ce que vous demanderez au Père en mon nom, il vous le donne.
15:17	Ce que je vous commande, c'est de vous aimer les uns les autres.

### La haine du monde envers le Mashiah et ses disciples

15:18	Si le monde vous hait, sachez qu'il m'a haï avant vous.
15:19	Si vous étiez issus du monde, le monde aimerait ce qui lui appartient. Mais parce que vous n'êtes pas issus du monde et que je vous ai choisis hors du monde, à cause de cela le monde vous hait.
15:20	Rappelez-vous la parole que je vous ai dite : L'esclave n'est pas plus grand que son seigneur<!--Mt. 10:24 ; Jn. 13:16.-->. S'ils m'ont persécuté, ils vous persécuteront aussi ; s'ils ont gardé ma parole, ils garderont aussi la vôtre.
15:21	Mais ils vous feront toutes ces choses à cause de mon nom, parce qu'ils ne connaissent pas celui qui m'a envoyé.
15:22	Si je n'étais pas venu et que je ne leur avais pas parlé, ils n'auraient pas de péché, mais maintenant ils n'ont pas de prétexte pour leur péché.
15:23	Celui qui me hait hait aussi mon Père.
15:24	Si je n'avais pas fait parmi eux les œuvres qu'aucun autre n'a faites, ils n'auraient pas de péché ; et maintenant, ils ont vu, et ils ont haï, et moi, et mon Père.
15:25	Mais c'est afin que fût accomplie la parole qui est écrite dans leur torah : Ils m'ont haï gratuitement<!--Ps. 35:19, 69:5, 109:3.-->.
15:26	Mais quand sera venu le parakletos que je vous enverrai de la part du Père, l'Esprit de vérité qui procède du Père, il rendra témoignage à mon sujet.
15:27	Et vous aussi, vous rendrez témoignage, parce que dès le commencement vous êtes avec moi.

## Chapitre 16

### Yéhoshoua annonce la persécution de ses disciples<!--Mt. 24:9-10 ; Lu. 21:16-19.-->

16:1	Je vous ai dit ces choses, afin que vous ne soyez pas scandalisés.
16:2	Ils vous excluront<!--Jn. 9:22, 12:42.--> des synagogues, et même l'heure vient où quiconque vous fera mourir pensera rendre un service sacré à Elohîm.
16:3	Et ils vous feront ces choses, parce qu'ils n'ont connu ni le Père ni moi.
16:4	Mais je vous ai dit ces choses, afin que, lorsque l'heure sera venue, vous vous rappeliez que je vous les ai dites. Et je ne vous en ai pas parlé dès le commencement, parce que j'étais avec vous.
16:5	Mais maintenant je m'en vais vers celui qui m'a envoyé, et aucun de vous ne me demande : Où vas-tu ?
16:6	Mais, parce que je vous ai dit ces choses, la douleur a rempli votre cœur.

### L'Esprit convainc le monde

16:7	Mais moi je vous dis la vérité : il vous est avantageux que je m'en aille. Car si je ne m'en vais pas, le parakletos ne viendra pas à vous. Mais si je m'en vais, je vous l'enverrai.
16:8	Et quand il sera venu, il convaincra le monde au sujet du péché, de la justice et du jugement :
16:9	au sujet du péché en effet, parce qu'ils ne croient pas en moi ;
16:10	et au sujet de la justice, parce que je m'en vais à mon Père et que vous ne me voyez plus ;
16:11	et au sujet du jugement, parce que le chef de ce monde est jugé.

### L'Esprit révélera la vérité

16:12	J'ai encore beaucoup de choses à vous dire, mais vous ne pouvez pas les supporter maintenant.
16:13	Mais quand il viendra, lui, l'Esprit de vérité, il vous guidera dans toute la vérité, car il ne parlera pas de lui-même, mais il parlera de tout ce qu'il aura entendu et il vous annoncera les choses à venir.
16:14	Il me glorifiera, parce qu'il prendra du mien et vous l'annoncera.
16:15	Toutes les choses que le Père a, sont à moi. C'est pourquoi j'ai dit qu'il prendra du mien et qu'il vous l'annoncera.

### Yéhoshoua annonce sa mort, sa résurrection et sa parousie

16:16	Un peu de temps et vous ne me voyez pas, et encore un peu de temps et vous me verrez, parce que je m'en vais au Père.
16:17	Quelques-uns donc de ses disciples se dirent les uns aux autres : Qu'est-ce qu'il nous dit : Un peu de temps et vous ne me voyez pas, et encore un peu de temps et vous me verrez, car je m'en vais à mon Père ?
16:18	Ils disaient donc : Que signifie ce peu de temps dont il parle ? Nous ne voyons pas de quoi il parle.
16:19	Yéhoshoua sut donc qu'ils voulaient l'interroger, et leur dit : Vous vous demandez entre vous sur ce que j'ai dit : Un peu de temps et vous ne me voyez pas, et encore un peu de temps et vous me verrez.
16:20	Amen, amen, je vous le dis : Vous pleurerez et vous vous lamenterez et le monde se réjouira. Vous serez attristés, mais votre douleur sera changée en joie.
16:21	Lorsqu'une femme accouche, elle a des douleurs parce que son heure est venue, mais, lorsque l'enfant est né, elle ne se rappelle plus de sa tribulation, à cause de la joie qu'elle a de ce qu'un être humain est né dans le monde.
16:22	Vous donc aussi, vous êtes maintenant dans la douleur, mais je vous verrai de nouveau en effet, et votre cœur se réjouira, et personne ne vous ôtera votre joie.
16:23	Et en ce jour-là, vous ne m'interrogerez plus sur rien. Amen, amen, je vous dis que ce que vous demanderez au Père en mon nom, il vous le donnera.
16:24	Jusqu'à présent vous n'avez rien demandé en mon Nom. Demandez et vous recevrez, afin que votre joie soit parfaite.
16:25	Je vous ai dit ces choses en paraboles. Mais l'heure vient où je ne vous parlerai plus en paraboles, mais où je vous annoncerai ouvertement ce qui concerne le Père.
16:26	En ce jour-là, vous demanderez en mon nom, et je ne vous dis pas que je prierai le Père pour vous,
16:27	car le Père lui-même vous aime, parce que vous m'avez aimé et que vous avez cru que je suis sorti d'Elohîm.
16:28	Je suis sorti du Père et je suis venu dans le monde. Maintenant je quitte le monde et je m'en vais au Père.
16:29	Ses disciples lui disent : Voici que maintenant, tu parles ouvertement et que tu ne dis rien en parabole.
16:30	Maintenant, nous savons que tu sais toutes choses et que tu n'as pas besoin que quelqu'un t'interroge. À cause de cela, nous croyons que tu viens d'Elohîm.
16:31	Yéhoshoua leur répondit : Vous croyez maintenant ?
16:32	Voici, l'heure vient, et elle est déjà venue, où vous serez dispersés chacun de son côté, et vous me laisserez seul. Mais je ne suis pas seul, car le Père est avec moi.
16:33	Je vous ai dit ces choses, afin que vous ayez la paix en moi. Vous avez de la tribulation dans le monde. Mais ayez du courage ! Moi, j'ai remporté la victoire sur le monde.

## Chapitre 17

### L'intercession du Mashiah, le Grand-Prêtre

17:1	Ayant ainsi parlé, Yéhoshoua leva les yeux au ciel et dit : Père, l'heure est venue ! Glorifie ton Fils, afin que ton Fils te glorifie,
17:2	selon que tu lui as donné autorité sur toute chair, afin qu'il donne la vie éternelle à tous ceux que tu lui as donnés.
17:3	Or la vie éternelle, c'est qu'ils te connaissent, toi, le seul Véritable Elohîm, et celui que tu as envoyé, Yéhoshoua Mashiah.
17:4	Moi, je t'ai glorifié sur la Terre, j'ai accompli l'œuvre que tu m'avais donnée à faire.
17:5	Et maintenant glorifie-moi, toi Père, auprès de toi-même, de la gloire que j'avais auprès de toi avant qu’existe le monde.
17:6	J'ai manifesté<!--Vient du grec « phaneroo » qui signifie « rendre manifeste ou visible ou connu ce qui a été caché », « manifester, que ce soit par des mots ou des faits ou toute autre manière », « exposer à la vue », « se montrer », « apparaître ».--> ton Nom<!--Ps. 22:23 ; Hé. 2:12.--> aux humains que tu as tirés<!--Vient du grec « ek » qui signifie : « hors de », « loin de ».--> du monde pour me les donner. Ils étaient à toi et tu me les as donnés, et ils ont gardé ta parole.
17:7	Maintenant ils ont connu que tout ce que tu m'as donné vient de toi.
17:8	Parce que je leur ai donné les paroles que tu m'as données, ils les ont reçues et ils ont connu vraiment que je suis sorti de toi, et ils ont cru que tu m'as envoyé.
17:9	Moi, je prie pour eux. Je ne prie pas pour le monde, mais pour ceux que tu m'as donnés, parce qu'ils sont à toi.
17:10	Et tout ce qui est à moi est à toi et ce qui est à toi est à moi, et je suis glorifié en eux.
17:11	Et je ne suis plus dans le monde, mais ceux-ci sont dans le monde. Et moi, je viens vers toi. Père saint, garde-les en ton Nom que tu m'as donné, afin qu'ils soient un comme nous.
17:12	Quand j'étais avec eux dans le monde, moi je les gardais en ton<!--σοῦ (sou) est un terme grec qui peut être traduit en français par « toi », « à toi », « ton », « ta », « tes », « tu ».--> Nom que tu m'as donné<!--Ou encore : « je les gardais en ton Nom, le Nom que tu m'as donné ». Dans le grec, le nom est au singulier. Il est question du Nom qui a été donné et non des disciples. Voir Ac. 4:12 ; Ph. 2:9.-->. J’ai veillé et aucun d'eux ne s'est perdu, excepté le fils de perdition, afin que l'Écriture soit accomplie.
17:13	Et maintenant je vais à toi, et je dis ces choses dans le monde, afin qu'ils aient ma joie parfaite en eux-mêmes.
17:14	Moi, je leur ai donné ta parole et le monde les a haïs parce qu'ils ne sont pas du monde, comme moi je ne suis pas du monde.
17:15	Je ne te prie pas de les enlever du monde, mais de les garder du mauvais.
17:16	Ils ne sont pas du monde, de même que moi je ne suis pas du monde.
17:17	Sanctifie-les par ta vérité ! Ta parole est la vérité.
17:18	Comme tu m'as envoyé dans le monde, je les ai moi aussi envoyés dans le monde.
17:19	Et je me sanctifie moi-même en leur faveur, afin qu'eux aussi soient sanctifiés en vérité.
17:20	Or je ne prie pas seulement pour eux, mais aussi pour ceux qui croiront en moi par le moyen de leur parole,
17:21	afin que tous soient un, comme toi, Père, tu es en moi, et moi en toi, afin qu'eux aussi soient un en nous, et que le monde croie que c'est toi qui m'as envoyé.
17:22	Et moi, je leur ai donné la gloire que tu m'as donnée, afin qu'ils soient un comme nous sommes un.
17:23	Moi en eux et toi en moi, afin qu'ils soient rendus parfaitement un, et que le monde connaisse que c'est toi qui m'as envoyé, et que tu les aimes comme tu m'as aimé.
17:24	Père, mon désir est que ceux que tu m'as donnés soient avec moi là où je suis, afin qu'ils contemplent la gloire que tu m'as donnée, parce que tu m'as aimé avant la fondation du monde.
17:25	Père juste ! En effet, le monde ne t'a pas connu, mais moi, je t'ai connu, et ceux-ci ont connu que c'est toi qui m'as envoyé.
17:26	Et je leur ai fait connaître<!--Vient du grec « gnorizo » qui signifie « faire connaître », « savoir », « obtenir la connaissance ». Dans le grec ancien : « gagner un savoir » ou « avoir une complète connaissance de ».--> ton Nom et je le leur ferai connaître, afin que l'amour dont tu m'as aimé soit en eux et que je sois en eux.

## Chapitre 18

### Gethsémané<!--Mt. 26:36-46 ; Mc. 14:32-42 ; Lu. 22:39-46.-->

18:1	Ayant dit ces choses, Yéhoshoua s'en alla avec ses disciples de l'autre côté du torrent du Cédron, où était un jardin dans lequel il entra, lui et ses disciples.

### L'arrestation de Yéhoshoua<!--Mt. 26:47-56 ; Mc. 14:43-50 ; Lu. 22:47-54.-->

18:2	Or Yéhouda aussi, celui qui le livrait, connaissait le lieu parce que Yéhoshoua s'y était souvent réuni avec ses disciples.
18:3	Yéhouda donc, ayant pris la cohorte, et des huissiers de la part des principaux prêtres et des pharisiens, arrive là avec des torches et des lampes et des armes.
18:4	Yéhoshoua donc, sachant toutes les choses qui venaient sur lui, sortit et leur dit : Qui cherchez-vous ?
18:5	Ils lui répondirent : Yéhoshoua, le Nazaréen. Yéhoshoua leur dit : Moi, je suis<!--« Moi, je suis » (en grec « ego eimi »), ce qui fait écho au Nom sous lequel Elohîm s'était révélé à Moshé (Moïse) en Ex. 3:14.-->. Or Yéhouda aussi, celui qui le livrait, se tenait là avec eux.
18:6	Quand donc il leur dit : Moi, je suis, ils reculèrent et tombèrent par terre.
18:7	Alors il leur demanda encore : Qui cherchez-vous ? Et ils dirent : Yéhoshoua, le Nazaréen.
18:8	Yéhoshoua répondit : Je vous ai dit que moi, je suis. Si donc vous me cherchez, laissez aller ceux-ci.
18:9	Ainsi s’accomplit la parole qu'il avait dite : Je n'ai perdu aucun de ceux que tu m'as donnés<!--Jn. 17:12.-->.

### Petros (Pierre) frappe Malchos

18:10	Alors Shim’ôn Petros, ayant une épée, la tira, frappa un esclave du grand-prêtre et lui coupa l'oreille droite. Le nom de l’esclave était Malchos.
18:11	Alors Yéhoshoua dit à Petros : Remets ton épée dans le fourreau. Ne boirai-je pas la coupe que le Père m'a donnée ?

### Yéhoshoua devant le grand-prêtre<!--Mt. 26:57-68 ; Mc. 14:53-65 ; Lu. 22:54.-->

18:12	Alors la cohorte, et le tribun et les huissiers des Juifs se saisirent de Yéhoshoua et le lièrent.
18:13	Et ils l'emmenèrent premièrement chez Chananyah, car il était le beau-père de Kaïaphas qui était le grand-prêtre de cette année-là.
18:14	Or Kaïaphas était celui qui avait donné ce conseil aux Juifs : Il est avantageux qu'un seul homme meure en faveur du peuple<!--Jn. 11:49-52.-->.

### Le triple reniement de Petros (Pierre)<!--Mt. 26:69-75 ; Mc. 14:66-72 ; Lu. 22:54-62.-->

18:15	Or Shim’ôn Petros et un autre disciple suivaient Yéhoshoua. Et ce disciple était connu du grand-prêtre, et il entra avec Yéhoshoua dans la cour du grand-prêtre,
18:16	mais Petros était dehors à la porte. Et l'autre disciple, qui était connu du grand-prêtre, sortit dehors et parla à la gardienne de la porte, et fit entrer Petros.
18:17	Alors la servante, la gardienne de la porte dit à Petros : N'es-tu pas, toi aussi, des disciples de cet homme ? Il dit : Je n’en suis pas.
18:18	Or les esclaves et les huissiers qui se tenaient là avaient fait un tas de charbons allumés, parce qu'il faisait froid et ils se chauffaient. Mais Petros aussi se tenait avec eux et se chauffait.
18:19	Alors le grand-prêtre interrogea Yéhoshoua sur ses disciples et sur sa doctrine.
18:20	Yéhoshoua lui répondit : Moi, j’ai parlé ouvertement au monde. Moi, j’ai toujours enseigné dans la synagogue et dans le temple, là où tous les Juifs se rassemblent, et je n'ai rien dit en secret.
18:21	Pourquoi m'interroges-tu ? Interroge ceux qui m'ont entendu sur ce que je leur ai dit. Voici, ceux-là savent ce que j'ai dit.
18:22	Or comme il disait ces choses, un des huissiers qui se tenait là, donna un coup de sa verge à Yéhoshoua, en disant : Est-ce ainsi que tu réponds au grand-prêtre ?
18:23	Yéhoshoua lui répondit : Si j'ai mal parlé, témoigne de ce qui est mal. Mais si c'est bien, pourquoi me frappes-tu ?
18:24	Or Chananyah l'envoya lié à Kaïaphas, le grand-prêtre.
18:25	Et Shim’ôn Petros se tenait là et se chauffait. Alors on lui dit : N'es-tu pas, toi aussi, de ses disciples ? Il le nia et dit : Je n'en suis pas.
18:26	Un des esclaves du grand-prêtre, parent de celui à qui Petros avait coupé l'oreille, dit : Ne t'ai-je pas vu dans le jardin avec lui ?
18:27	Mais Petros le nia de nouveau. Et immédiatement le coq chanta.

### Yéhoshoua comparaît devant Pilate<!--Mt. 27:2,11-21 ; Mc. 15:1-15 ; Lu. 23:1-7,13-25.-->

18:28	Alors ils mènent Yéhoshoua de chez Kaïaphas au prétoire<!--Le prétoire était à l'origine le nom du quartier général de la légion romaine. Il s'agissait plus particulièrement de la tente du général en chef d'une armée.-->. Or c'était le matin. Et ils n'entrèrent pas eux-mêmes dans le prétoire, afin de ne pas se souiller et de pouvoir manger la Pâque.
18:29	Pilate<!--Ponce Pilate était le préfet procurateur de la province romaine de Judée au 1er siècle (de 26 à 36).--> donc sortit vers eux et dit : Quelle accusation portez-vous contre cet homme ?
18:30	Ils répondirent et lui dirent : Si ce n'était pas un malfaiteur, nous ne te l'aurions pas livré.
18:31	Alors Pilate leur dit : Prenez-le vous-mêmes et jugez-le selon votre torah. Alors les Juifs lui dirent : Il n'est pas légal pour nous de tuer quelqu'un.
18:32	Afin que fût accomplie la parole de Yéhoshoua, celle qu’il avait dite pour indiquer de quelle mort il était sur le point de mourir.
18:33	Alors Pilate entra de nouveau dans le prétoire et ayant appelé Yéhoshoua, il lui dit : Toi, tu es le roi des Juifs ?
18:34	Yéhoshoua lui répondit : Est-ce de toi-même que tu dis cela, ou d'autres te l'ont dit de moi ?
18:35	Pilate répondit : Suis-je un Juif, moi ? Ta nation et les principaux prêtres t'ont livré à moi. Qu'as-tu fait ?
18:36	Yéhoshoua répondit : Mon Royaume n'est pas de ce monde. Si mon Royaume était de ce monde, mes serviteurs auraient combattu pour moi afin que je ne sois pas livré aux Juifs. Mais maintenant mon Royaume n'est pas d'ici-bas.
18:37	Alors Pilate lui dit : Es-tu donc roi ? Yéhoshoua répondit : C'est toi qui dis que moi je suis roi. Moi, je suis né pour cela, et c'est pour cela que je suis venu dans le monde, afin de rendre témoignage à la vérité. Quiconque est de la vérité entend ma voix.
18:38	Pilate lui dit : Qu'est-ce que la vérité ? Et quand il eut dit cela, il sortit de nouveau vers les Juifs, et il leur dit : Je ne trouve aucun crime en lui.
18:39	Mais comme c'est parmi vous une coutume que je vous relâche quelqu'un à la Pâque, voulez-vous donc que je vous relâche le Roi des Juifs ?
18:40	Alors tous s'écrièrent encore, disant : Non pas celui-ci, mais Barabbas. Or Barabbas était un brigand.

## Chapitre 19

### Le Roi couronné d'épines<!--Mt. 27:30 ; Mc. 15:16-18.-->

19:1	Alors Pilate prit donc Yéhoshoua et le châtia avec un fouet.
19:2	Et les soldats tressèrent une couronne d'épines qu'ils posèrent sur sa tête et le vêtirent d'un vêtement de pourpre.
19:3	Et ils disaient : Roi des Juifs, nous te saluons ! Et ils lui donnaient des coups avec leurs verges.
19:4	Alors Pilate sortit de nouveau dehors et leur dit : Voici, je vous l'amène dehors, afin que vous sachiez que je ne trouve aucun crime en lui.
19:5	Yéhoshoua donc sortit, portant la couronne d'épines et le vêtement de pourpre. Et il leur dit : Voici l'homme !
19:6	Quand donc les principaux prêtres et leurs huissiers le virent, ils s'écrièrent, en disant : Crucifie-le ! Crucifie-le ! Pilate leur dit : Prenez-le vous-mêmes et crucifiez-le ! Car moi, je ne trouve pas de crime en lui.
19:7	Les Juifs lui répondirent : Nous avons une torah, et selon notre torah, il doit mourir, car il s'est fait Fils d'Elohîm.
19:8	Quand donc Pilate entendit cette parole, il fut plus effrayé.
19:9	Et il rentra dans le prétoire et dit à Yéhoshoua : D'où es-tu ? Mais Yéhoshoua ne lui donna pas de réponse.
19:10	Alors Pilate lui dit : Est-ce à moi que tu ne parles pas ? Ne sais-tu pas que j'ai autorité pour te crucifier, et que j'ai autorité pour te relâcher ?
19:11	Yéhoshoua répondit : Tu n'aurais aucune autorité sur moi, si elle ne t'avait été donnée d'en haut<!--Voir Ro. 13:1-3.-->. C'est pourquoi celui qui me livre entre tes mains a un plus grand péché.
19:12	Dès ce moment, Pilate cherchait à le relâcher. Mais les Juifs criaient, en disant : Si tu le relâches, tu n'es pas ami de César. Quiconque se fait roi s'oppose à César.
19:13	Pilate donc, ayant entendu ces paroles, amena Yéhoshoua dehors et siégea au tribunal à l'endroit appelé le Pavé<!--Le mot pour « pavé » ne se trouve nulle part ailleurs dans le Testament de Yéhoshoua. Dans 2 R. 16:17, il est question de l'apostasie abjecte du roi Achaz. Il en est de même de Pilate se rabaissant au niveau des Juifs apostats. Dans le premier cas, il s'agit d'une règle juive dominée par un apostat gentil (gens des nations) ; dans l'autre cas, c'est un idolâtre gentil qui est dominé par des Juifs ayant rejeté leur Mashiah.-->, et en hébreu Gabbatha.
19:14	Or c'était la préparation de la Pâque, et il était environ la sixième heure. Et il dit aux Juifs : Voici votre Roi !
19:15	Mais ils criaient : Ôte ! ôte ! Crucifie-le ! Pilate leur dit : Crucifierai-je votre Roi ? Les principaux prêtres répondirent : Nous n'avons pas d'autre roi, excepté César.

### Yéhoshoua crucifié<!--Mt. 27:31-50 ; Mc. 15:20-37 ; Lu. 23:26-46.-->

19:16	Alors il le leur livra à cet instant pour être crucifié. Et ils prirent Yéhoshoua et l'emmenèrent.
19:17	Et Yéhoshoua, portant sa croix, arriva au lieu appelé le Crâne, qui se dit en hébreu Golgotha,
19:18	où ils le crucifièrent, et deux autres avec lui, un de chaque côté, et Yéhoshoua au milieu.
19:19	Et Pilate fit aussi une inscription qu'il mit sur la croix. Et il y était écrit : YÉHOSHOUA, LE NAZARÉEN, LE ROI DES JUIFS.
19:20	Beaucoup de Juifs lurent donc cette inscription, parce que le lieu où Yéhoshoua était crucifié était près de la ville. Et elle était écrite en hébreu, en grec et en latin.
19:21	Les principaux prêtres des Juifs dirent donc à Pilate : N'écris pas : Le roi des Juifs, mais que celui-ci a dit : Je suis le roi des Juifs.
19:22	Pilate répondit : Ce que j'ai écrit, je l'ai écrit !
19:23	Alors les soldats, après avoir crucifié Yéhoshoua, prirent ses vêtements et ils en firent quatre parts, une part pour chaque soldat, et la tunique. Or la tunique était sans couture, d'un seul tissu depuis le haut jusqu'en bas.
19:24	Alors ils se dirent entre eux : Ne la déchirons pas, mais jetons-la au sort, à qui elle sera, afin que l'Écriture fût accomplie : Ils se sont partagé mes vêtements et ils ont jeté le sort sur mon habit<!--Ps. 22:19.-->. Alors les soldats firent en effet ces choses.
19:25	Or près de la croix de Yéhoshoua se tenaient sa mère, la sœur de sa mère, Myriam la femme de Clopas et Myriam-Magdeleine.
19:26	Alors Yéhoshoua voyant sa mère et, auprès d'elle, le disciple qu'il aimait, il dit à sa mère : Femme, voici ton fils.
19:27	Ensuite il dit au disciple : Voici ta mère. Et dès cette heure-là le disciple la prit chez lui.
19:28	Après cela, Yéhoshoua, sachant que toutes choses étaient déjà accomplies, il dit, afin que l'Écriture fût accomplie : J'ai soif.
19:29	Il y avait donc là un vase plein de vinaigre. Et ils emplirent de vinaigre une éponge et, l'ayant mise sur de l'hysope, ils la lui présentèrent à la bouche.
19:30	Quand donc Yéhoshoua eut pris le vinaigre, il dit : C’est accompli<!--La fin de la période de la première alliance n'a pas eu lieu à la naissance du Seigneur. Ga. 4:4 nous dit que Yéhoshoua (Jésus) est né sous la torah de Moshé (Moïse) et le récit des quatre évangiles atteste que depuis sa naissance jusqu'à sa mort, Yéhoshoua a scrupuleusement respecté et accompli toute la loi. En effet, il a lui-même dit : « Ne croyez pas que je sois venu pour détruire la torah ou les prophètes. Je ne suis pas venu pour détruire, mais pour accomplir. » (Mt. 5:17). Ainsi, durant son service terrestre, le Seigneur demandait à ce qu'on applique la torah (Mt. 8:4, 23:23 ; Lu. 17:11-14), tout en préparant ses disciples à la nouvelle alliance. L'évangile de Mattithyah (Matthieu) nous relate un événement capital qui a eu lieu juste après la mort du Seigneur : « Mais Yéhoshoua poussa de nouveau un grand cri et rendit l'esprit. Et voici, le voile du temple se déchira en deux, depuis le haut jusqu'en bas ; et la terre trembla et les pierres se fendirent. » (Mt. 27:50-51). Il convient de rappeler que le temple était divisé en trois parties : le parvis, le lieu saint et le Saint des saints. Le parvis était accessible à tout le monde, y compris aux non-Juifs. Le lieu saint n'était accessible qu'aux prêtres. La troisième partie, le Saint des saints, n'était accessible qu'au grand-prêtre. Le lieu saint était séparé du Saint des saints par un voile qui symbolisait le mur d'inimitié (Es. 59:2 ; Ro. 3:23) qui sépare l'être humain pécheur de la présence d'Elohîm, représentée dans le temple par l'arche de l'alliance. Ce voile n'avait rien d'un tissu léger et vaporeux, mais il ressemblait davantage à un épais tapis, opaque et surtout très résistant, et donc très difficile à déchirer. Le grand-prêtre rentrait seulement une fois par an dans le Saint des saints pour y offrir le sacrifice d'expiation pour le peuple, ainsi que pour lui-même (Lé. 16 ; Hé. 9:7). Toutefois, la nécessité de répéter ce sacrifice chaque année prouvait que les exigences de la justice divine n'étaient pas pleinement satisfaites (Hé. 10:3-4). L'auteur de la lettre aux Hébreux nous apprend que le voile symbolisait également le corps physique du Mashiah (Christ) (Hé. 10:19-20). Ainsi, lorsque le Seigneur a succombé à ses meurtrissures, le fameux voile s'est déchiré du haut jusqu'au bas, or tant que le voile subsistait, l'accès à la présence d'Elohîm était fermé (Hé. 9:8). La déchirure atteste donc qu'en Mashiah, nous pouvons désormais nous approcher avec assurance du trône d'Elohîm, sans autre médiateur que le Seigneur lui-même (1 Ti. 2:5). « Or, là où il y a eu pardon, il n'y a plus d'offrande au sujet du péché. Ayant donc, frères, la liberté pour entrer dans les lieux saints par le sang de Yéhoshoua, chemin nouveau et vivant qu'il nous a inauguré au travers du voile, c'est-à-dire de sa propre chair, et ayant un grand-prêtre établi sur la maison d'Elohîm, approchons-nous de lui avec un cœur sincère et une foi inébranlable, les cœurs, par aspersion, purifiés d'une mauvaise conscience et le corps lavé d'une eau pure. Retenons fermement la profession de l'espérance, car celui qui a fait la promesse est fidèle. » Hé. 10:18-23. Yéhoshoua ha Mashiah (Jésus-Christ) est notre Pâque (1 Co. 5:5-8), il est le sacrifice parfait qui a expié nos péchés une fois pour toutes (Hé. 10:10). Par conséquent, il est celui à qui nous devons nous adresser pour recevoir pardon, miséricorde et compassion. « C'est accompli », en s'écriant de la sorte, Yéhoshoua ha Mashiah a proclamé la fin de la première alliance. En effet, la torah a été donnée par le moyen de Moshé, la grâce et la vérité sont venues par le moyen de Yéhoshoua ha Mashiah (Jn. 1:17). Toutefois, la nouvelle alliance n'a réellement débuté qu'à la pentecôte avec l'effusion du Saint-Esprit.-->. Et ayant baissé la tête, il rendit l'esprit.

### FIN DE LA LOI MOSAÏQUE OU DE LA PREMIÈRE ALLIANCE<!--Hé. 9:16-18.-->

19:31	Alors les Juifs, afin que les corps ne restent pas sur la croix durant le shabbat<!--Il est question ici du shabbat de la fête des pains sans levain. Ce shabbat n'a rien à voir avec le shabbat hebdomadaire qui commence le vendredi à 18h pour se terminer le samedi à 18h. Voir Mt. 28:1.-->, parce que c'était la préparation - car le jour de ce shabbat était un grand, demandèrent à Pilate qu'on leur brise les jambes et qu'on les enlève.
19:32	Les soldats vinrent donc et brisèrent en effet les jambes au premier, et de même à l'autre qui était crucifié avec lui.
19:33	Mais s'étant approché de Yéhoshoua, et voyant qu'il était déjà mort, ils ne lui brisèrent pas les jambes.
19:34	Mais un des soldats lui perça le côté avec une lance, et immédiatement il sortit du sang et de l'eau.
19:35	Et celui qui l'a vu a rendu témoignage, et son témoignage est véritable. Et lui, il sait qu'il dit vrai, afin que vous croyiez.
19:36	Car ces choses sont arrivées afin que l'Écriture fût accomplie : Aucun de ses os ne sera brisé<!--Ex. 12:46 ; No. 9:12 ; Ps. 34:21.-->.
19:37	Et encore une autre Écriture, qui dit : Ils verront celui qu'ils ont percé<!--Za. 12:10.-->.

### Yossef d'Arimathée demande le corps de Yéhoshoua<!--Mt. 27:57-66 ; Mc. 15:42-47 ; Lu. 23:50-56.-->

19:38	Or après ces choses, Yossef d'Arimathée, qui était disciple de Yéhoshoua, mais en secret parce qu'il craignait les Juifs, demanda à Pilate la permission d'enlever le corps de Yéhoshoua. Et Pilate le permit. Alors il vint et prit le corps de Yéhoshoua.
19:39	Mais Nikodemos, qui auparavant était allé de nuit vers Yéhoshoua, vint aussi, apportant un mélange de myrrhe et d'aloès d'environ 100 livres.
19:40	Ils prirent donc le corps de Yéhoshoua et l'enveloppèrent de petites étoffes de lin, avec des aromates, comme les Juifs ont coutume d'ensevelir.
19:41	Or il y avait un jardin dans le lieu où il avait été crucifié et, dans le jardin un sépulcre neuf où personne n'avait encore été déposé.
19:42	Ce fut donc là qu'ils déposèrent Yéhoshoua, à cause de la préparation des Juifs, parce que le sépulcre était proche.

## Chapitre 20

### Les disciples se rendent au sépulcre<!--Mt. 28:1-15 ; Mc. 16:1-14 ; Lu. 24:1-32.-->

20:1	Mais l'un des shabbats<!--Il est question ici du shabbat hebdomadaire, c'est-à-dire le septième jour ou le samedi. Jn. 20:19. Voir Mt. 28:1.-->, Myriam-Magdeleine vient au sépulcre dès le matin, la ténèbre étant encore là, et elle voit que la pierre a été enlevée du sépulcre.
20:2	Elle court donc et vient vers Shim’ôn Petros et vers l'autre disciple que Yéhoshoua aimait, et elle leur dit : On a enlevé le Seigneur hors du sépulcre et nous ne savons pas où on l'a déposé.
20:3	Alors Petros sortit et l'autre disciple, et allèrent au sépulcre.
20:4	Or ils couraient tous les deux ensemble. Et l'autre disciple courut en avant plus vite que Petros et il arriva le premier au sépulcre.
20:5	Et ayant regardé avec la tête penchée en avant, il voit les petites étoffes de lin posées là, mais il n'y entra pas.
20:6	Alors arrive Shim’ôn Petros, qui le suivait. Et il entra dans le sépulcre, et il voit les petites étoffes de lin posées là,
20:7	et le mouchoir qu'on avait mis sur sa tête et qui n'était pas posé avec les petites étoffes de lin, mais enroulé à part dans un lieu.
20:8	À cet instant donc, l'autre disciple, qui était arrivé le premier au sépulcre, y entra aussi, il vit et crut.
20:9	Car ils ne comprenaient pas encore que, selon l'Écriture, il devait ressusciter des morts.
20:10	Alors les disciples s'en retournèrent chez eux.

### Yéhoshoua apparaît à Myriam-Magdeleine (Marie de Magdala) et aux disciples<!--Mc. 16:14 ; Lu. 24:13-49.-->

20:11	Mais Myriam se tenait près du sépulcre dehors, pleurant. Comme donc elle pleurait, elle se baissa vers le sépulcre,
20:12	et elle voit deux anges vêtus de blanc, assis à la place où avait été couché le corps de Yéhoshoua, l'un à la tête et l'autre aux pieds.
20:13	Et ils lui disent : Femme, pourquoi pleures-tu ? Elle leur dit : Parce qu'on a enlevé mon Seigneur, et je ne sais pas où on l'a déposé.
20:14	Et en disant cela, elle se tourna en arrière, et elle voit Yéhoshoua se tenant debout, et elle ne savait pas que c'était Yéhoshoua.
20:15	Yéhoshoua lui dit : Femme, pourquoi pleures-tu ? Qui cherches-tu ? Elle, pensant que c'était le jardinier, lui dit : Seigneur, si c'est toi qui l'as emporté, dis-moi où tu l'as déposé, et je le prendrai.
20:16	Yéhoshoua lui dit : Myriam ! Et elle, s'étant retournée, lui dit : Rhabboni ! C'est-à-dire, Maître !
20:17	Yéhoshoua lui dit : Ne t'attache pas à moi, car je ne suis pas encore monté vers mon Père. Mais va vers mes frères et dis-leur que je monte vers mon Père et votre Père, vers mon Elohîm et votre Elohîm.
20:18	Myriam-Magdeleine vient annoncer aux disciples qu'elle avait vu le Seigneur et qu'il lui avait dit ces choses.
20:19	C'est donc le soir<!--La période qui va de trois à six heures de l'après-midi. Yohanan parlait des heures selon la méthode romaine.--> de ce jour, l'un des shabbats<!--Vient du grec « sabbaton » qui signifie « shabbat ». Jn. 20:1. Voir commentaire en Mt. 28:1. Le grec « sabbaton » est au pluriel dans ce passage.-->, les portes du lieu où les disciples étaient rassemblés, à cause de la crainte qu'ils avaient des Juifs, étaient fermées. Yéhoshoua vint et se tint au milieu et leur dit : Shalôm à vous ! 
20:20	Et quand il leur eut dit cela, il leur montra ses mains et son côté. Alors les disciples furent dans la joie en voyant le Seigneur.
20:21	Alors Yéhoshoua leur dit de nouveau : Shalôm à vous ! Comme le Père m'a envoyé, moi aussi je vous envoie.
20:22	Et ayant dit cela, il souffla<!--Ce mot est utilisé seulement une fois par les traducteurs de la Septante, dans Genèse 2:7, où YHWH Elohîm souffla sur Adam qui devint une âme vivante. Comme la création originelle a été complétée par une action d'Elohîm, la nouvelle création a été complétée par un acte provenant du Chef de cette nouvelle création.--> sur eux et leur dit : Recevez le Saint-Esprit !
20:23	Ceux à qui vous remettrez les péchés, ils leur seront remis. Et ceux à qui vous les retiendrez, ils leur seront retenus.
20:24	Or Thomas, appelé Didymos, l'un des douze, n'était pas avec eux quand Yéhoshoua vint.
20:25	Alors les autres disciples lui dirent : Nous avons vu le Seigneur. Mais il leur dit : Si je ne vois pas la marque des clous dans ses mains, et si je ne mets pas mon doigt dans la marque des clous, et si je ne mets pas ma main dans son côté, je ne croirai jamais.
20:26	Et huit jours après, les disciples étaient de nouveau à l'intérieur et Thomas avec eux. Yéhoshoua vient, les portes étant fermées, il se tint au milieu et il dit : Shalôm à vous ! 
20:27	Et il dit à Thomas : Porte ton doigt ici et regarde mes mains. Porte aussi ta main et mets-la dans mon côté. Et ne deviens pas incrédule, mais croyant !
20:28	Et Thomas répondit et lui dit : Mon Seigneur et mon Elohîm !
20:29	Yéhoshoua lui dit : Parce que tu m'as vu, Thomas, tu as cru. Bénis sont ceux qui n'ont pas vu et qui ont cru.
20:30	En effet, Yéhoshoua a fait aussi en présence de ses disciples beaucoup d'autres signes, qui ne sont donc pas écrits dans ce livre.
20:31	Mais ces choses sont écrites afin que vous croyiez que Yéhoshoua est le Mashiah, le Fils d'Elohîm, et qu'en croyant vous ayez la vie par son Nom.

## Chapitre 21

### Yéhoshoua apparaît à ses disciples

21:1	Après ces choses, Yéhoshoua se manifesta encore à ses disciples, près de la Mer de Tibériade. Et il se manifesta de cette manière.
21:2	Shim’ôn Petros, Thomas, appelé Didymos, Netanél, de Qanah en Galilée, et ceux de Zabdi<!--Généralement traduit par Zébédée.-->, et deux autres de ses disciples étaient ensemble.
21:3	Shim’ôn Petros leur dit : Je vais pêcher. Ils lui disent : Nous allons aussi avec toi. Ils partirent donc et montèrent aussitôt dans un bateau, mais ils ne prirent rien cette nuit-là.
21:4	Le matin étant venu, Yéhoshoua se tint là sur le rivage, mais les disciples ne savaient pas que c'était Yéhoshoua.
21:5	Alors Yéhoshoua leur dit : Enfants, n'avez-vous pas du poisson grillé ? Ils lui répondirent : Non.
21:6	Et il leur dit : Jetez le filet du côté droit du bateau et vous en trouverez. Ils le jetèrent donc, et ils ne pouvaient plus le retirer à cause de la grande quantité de poissons.
21:7	Alors le disciple que Yéhoshoua aimait dit à Petros : C'est le Seigneur ! Shim’ôn Petros ayant donc entendu que c'était le Seigneur, ceignit sa tunique, car il était nu, et se jeta dans la mer.
21:8	Et les autres disciples vinrent avec le petit bateau, car ils n'étaient pas loin de terre, mais seulement à environ deux cents coudées<!--Une centaine de mètres.-->, traînant le filet de poissons.
21:9	Et étant descendus à terre, ils voient un tas de charbons allumés, et du poisson posé dessus et du pain.
21:10	Yéhoshoua leur dit : Apportez des poissons que vous venez maintenant de prendre.
21:11	Shim’ôn Petros monta et tira le filet à terre, plein de 153 grands poissons et, quoiqu'il y en eût tant, le filet n'avait pas été déchiré.
21:12	Yéhoshoua leur dit : Venez déjeuner. Mais aucun de ses disciples n'osait lui demander : Qui es-tu ? Sachant que c'était le Seigneur.
21:13	Yéhoshoua donc vient et prend le pain et leur en donne, et le poisson de même.
21:14	Ce fut déjà la troisième fois que Yéhoshoua, réveillé d'entre les morts, se manifesta à ses disciples.
21:15	Après donc qu'ils eurent déjeuné, Yéhoshoua dit à Shim’ôn Petros : Shim’ôn, de Yonah, m'aimes<!--Le texte grec nous permet d'avoir un éclairage intéressant sur l'échange qu'ont eu Yéhoshoua et Petros (Pierre). Aux versets 15 et 16, le Seigneur emploie le verbe agapao (aimer chèrement, d'un amour divin) lorsqu'il demande à son apôtre « m'aimes-tu ? ». Sans doute accablé par le poids de la culpabilité d'avoir renié le Seigneur, Petros (Pierre) répond en employant le verbe phileo (avoir de l'affection) qui exprime un amour d'une moindre intensité. Lorsque Yéhoshoua le questionne pour la troisième fois, il se met à son niveau en employant à son tour le verbe phileo. Remarquez également que Yéhoshoua utilise à trois reprises l'adjectif possessif « mes » (« mes agneaux », « mes brebis ») comme pour insister sur le fait que les âmes sont sa propriété et non celle d'un berger humain.-->-tu plus que ceux-ci ? Il lui répondit : Oui, Seigneur, tu sais que je t’affectionne. Il lui dit : Pais mes agneaux.
21:16	Il lui dit encore une seconde fois : Shim’ôn, de Yonah, m'aimes-tu ? Il lui dit : Oui, Seigneur, tu sais que je t’affectionne. Il lui dit : Garde mes brebis.
21:17	Il lui dit pour la troisième fois : Shim’ôn, de Yonah, m’affectionnes-tu<!--Vient du grec « phileo » qui signifie « avoir de l'affection ».--> ? Petros fut attristé de ce qu'il lui avait dit pour la troisième fois : M’affectionnes-tu ? Et il lui dit : Seigneur, tu sais toutes choses, tu sais que je t’affectionne. Yéhoshoua lui dit : Pais mes brebis.

### Le Maître révèle à Petros (Pierre) sa mort : conclusion

21:18	Amen, amen, je te le dis, quand tu étais plus jeune, tu te ceignais toi-même et tu marchais où tu voulais, mais quand tu seras devenu vieux, tu étendras tes mains et c'est un autre qui te ceindra et te portera où tu ne voudras pas.
21:19	Or il dit cela pour indiquer par quelle mort il glorifiera Elohîm<!--Voir 2 Pi. 1:14.-->. Et ayant ainsi parlé, il lui dit : Suis-moi !
21:20	Mais Petros, s'étant retourné, voit le disciple que Yéhoshoua aimait les suivre, celui qui, pendant le souper, s'était penché sur sa poitrine et lui avait dit<!--Jn. 13:25.--> : Seigneur, qui est celui qui te livre ?
21:21	En le voyant, Petros dit à Yéhoshoua : Et celui-ci, Seigneur ?
21:22	Yéhoshoua lui dit : Si je veux qu'il demeure jusqu'à ce que je vienne, en quoi cela te concerne-t-il ? Toi, suis-moi !
21:23	Cette parole donc sortit parmi les frères que ce disciple ne mourrait pas. Cependant, Yéhoshoua ne lui avait pas dit qu'il ne mourrait pas, mais : Si je veux qu'il demeure jusqu'à ce que je vienne, en quoi cela te concerne-t-il ? 
21:24	C'est ce disciple qui rend témoignage de ces choses et qui les a écrites. Et nous savons que son témoignage est vrai.
21:25	Mais il y a encore beaucoup d'autres choses que Yéhoshoua a faites. Si on les écrivait une à une, je ne pense pas que le monde même pourrait contenir les livres écrits<!--Voir Ps. 40:6.-->. Amen !
