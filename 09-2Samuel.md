# 2 Shemouél (2 Samuel) (2 S.)

Signification : Entendu, exaucé de El

Auteur : Inconnu

Thème : Le règne de David

Date de rédaction : 10ème siècle av. J.-C.

Suite du premier livre de Shemouél (1 Samuel), ce livre commence par le récit de la mort de Shaoul (Saül) et l'accession progressive de David à la royauté. La faveur d'Elohîm dans sa vie lui donna du succès et lui permit d'étendre son royaume jusqu'au nord de Damas. Au détriment de sa piété et de son alliance avec Elohîm, David commit de lourdes erreurs. Il s'en repentit sincèrement, mais il dut en assumer les conséquences.

## Chapitre 1

### Attitude de David à la mort de Shaoul (Saül)

1:1	Il arriva, après la mort de Shaoul, quand David fut revenu après avoir battu les Amalécites, que David resta deux jours à Tsiklag.
1:2	Le troisième jour, voici qu'un homme arriva du camp, d'auprès de Shaoul, avec ses vêtements déchirés et de la terre sur sa tête. Il arriva que lorsqu'il vint vers David, il tomba à terre et se prosterna.
1:3	David lui dit : D'où viens-tu ? Il lui dit : Je me suis échappé du camp d'Israël.
1:4	David lui dit : Comment la chose est-elle arrivée ? S'il te plaît, raconte-moi ! Il dit : Le peuple s'est enfui de la bataille, et parmi le peuple beaucoup sont tombés et sont morts. Shaoul aussi et Yehonathan, son fils, sont morts.
1:5	David dit à ce jeune homme qui lui disait ces nouvelles : Comment sais-tu que Shaoul et Yehonathan, son fils, sont morts ?
1:6	Le jeune homme qui lui disait ces nouvelles lui dit : Je me trouvais, je me trouvais sur la Montagne de Guilboa, et voici Shaoul était appuyé sur sa lance, et voici les chars et les maîtres cavaliers l'avaient rattrapé.
1:7	S'étant retourné, il m'aperçut et m'appela. Je lui dis : Me voici !
1:8	Il me dit : Qui es-tu ? Je lui dis : Je suis Amalécite.
1:9	Et il dit : S’il te plaît, reste près de moi et tue-moi, car je suis dans une grande angoisse, car toute mon âme est encore en moi.
1:10	Je m'approchai de lui et le tuai<!--Cet homme amalécite a peut-être menti afin de gagner la faveur de David (1 S. 31:3-5).-->, car je savais qu'il ne vivrait pas après être tombé. J'ai pris la couronne qu'il avait sur sa tête, et le bracelet qui était à son bras, et je les apporte ici à mon seigneur.
1:11	David saisit ses vêtements et les déchira, et tous les hommes qui étaient avec lui firent de même.
1:12	Ils se lamentèrent, pleurèrent et jeûnèrent jusqu'au soir sur Shaoul, sur Yehonathan, son fils, sur le peuple de YHWH et sur la maison d'Israël, parce qu'ils étaient tombés par l'épée.
1:13	David dit au jeune homme qui lui avait apporté ces nouvelles : D'où es-tu ? Il dit : Je suis fils d'un homme étranger, d'un Amalécite.
1:14	David lui dit : Comment n'as-tu pas craint d'avancer ta main pour tuer le mashiah de YHWH ?
1:15	David appela l'un de ses serviteurs et lui dit : Approche-toi, jette-toi sur lui ! Ce dernier le frappa et il mourut.
1:16	Et David lui dit : Que ton sang soit sur ta tête, car ta bouche a témoigné contre toi, en disant : C'est moi qui ai tué le mashiah de YHWH !

### Chant funèbre de David

1:17	David chanta ce chant funèbre sur Shaoul et sur Yehonathan, son fils.
1:18	Il dit : (Pour apprendre l'arc aux fils de Yéhouda. C'est écrit dans le livre du Juste.)
1:19	L'élite d'Israël a succombé sur tes collines ! Comment des hommes vaillants sont-ils tombés ?
1:20	Ne l'annoncez pas dans Gath et n’en portez pas la nouvelle dans les rues d'Askalon, de peur que les filles des Philistins ne se réjouissent, de peur que les filles des incirconcis n'en tressaillent de joie.
1:21	Montagnes de Guilboa ! qu'il n'y ait sur vous ni rosée, ni pluie, ni champs qui donnent des prémices pour les offrandes ! Car là ont été jetés les boucliers des hommes vaillants, le bouclier de Shaoul. L'huile a cessé de les oindre.
1:22	L'arc de Yehonathan ne reculait pas devant le sang des blessés et devant la graisse des hommes vaillants, et l'épée de Shaoul ne retournait pas à vide.
1:23	Shaoul et Yehonathan, aimables et agréables pendant leur vie, n'ont pas été séparés dans leur mort. Ils étaient plus légers que les aigles, ils étaient plus forts que des lions.
1:24	Filles d'Israël ! Pleurez sur Shaoul, qui vous revêtait magnifiquement de cramoisi, qui mettait des ornements d'or à vos habits.
1:25	Comment les hommes vaillants sont-ils tombés au milieu du combat ? Yehonathan a été tué sur tes collines !
1:26	Yehonathan, mon frère, je suis dans la douleur à cause de toi ! Tu étais pour moi très charmant. L'amour que j'avais pour toi était plus merveilleux que l'amour des femmes.
1:27	Comment sont tombés les hommes vaillants et ont péri les armes de guerre ?

## Chapitre 2

### David oint roi de Yéhouda

2:1	Il arriva après cela que David consulta YHWH, en disant : Monterai-je dans l'une des villes de Yéhouda ? YHWH lui dit : Monte. David dit : Où monterai-je ? YHWH dit : À Hébron.
2:2	David y monta, avec ses deux femmes, Achinoam de Yizre`e'l, et Abigaïl de Carmel, femme de Nabal.
2:3	David fit monter aussi les hommes qui étaient avec lui, chaque homme avec sa famille. Ils habitèrent dans les villes d'Hébron.
2:4	Les hommes de Yéhouda vinrent, et là ils oignirent David pour roi sur la maison de Yéhouda. On fit un rapport à David en disant : Les hommes de Yabesh en Galaad ont enterré Shaoul.
2:5	David envoya des messagers vers les hommes de Yabesh en Galaad, pour leur dire : Soyez bénis de YHWH, puisque vous avez fait preuve d'une telle bonté envers Shaoul, votre seigneur, et que vous l'avez enterré.
2:6	Maintenant que YHWH use envers vous de bonté et de fidélité. Moi aussi je vous ferai du bien, parce que vous avez agi de la sorte.
2:7	Et maintenant, que vos mains se fortifient ! Soyez des fils talentueux, car Shaoul votre seigneur est mort et la maison de Yéhouda m'a oint pour être roi sur elle.

### Ish-Bosheth établi roi d'Israël

2:8	Abner, fils de Ner, chef de l'armée de Shaoul, prit Ish-Bosheth, fils de Shaoul, et le fit passer à Mahanaïm.
2:9	Il le fit roi sur Galaad, sur les Guéshouriens, sur Yizre`e'l, sur Éphraïm, sur Benyamin et sur tout Israël.
2:10	Ish-Bosheth, fils de Shaoul, était fils de 40 ans quand il régna sur Israël et il régna 2 ans. Il n'y eut que la maison de Yéhouda qui suivit David.
2:11	Le nombre de jours pendant lesquels David régna à Hébron sur la maison de Yéhouda fut de 7 ans et 6 mois.

### Guerre entre Yéhouda et Israël

2:12	Abner, fils de Ner, et les serviteurs d'Ish-Bosheth, fils de Shaoul, sortirent de Mahanaïm pour marcher vers Gabaon.
2:13	Yoab, fils de Tserouyah, et les serviteurs de David, sortirent aussi. Ils se rencontrèrent ensemble près de l'étang de Gabaon, et ils restèrent ceux-ci de ce côté-ci de l'étang et ceux-là de ce côté-là de l'étang.
2:14	Abner dit à Yoab : Que ces jeunes hommes se lèvent, s’il te plaît, et qu'ils se battent devant nous ! Yoab dit : Qu'ils se lèvent !
2:15	Ils se levèrent, et s'avancèrent en nombre égal, 12 pour Benyamin et pour Ish-Bosheth, fils de Shaoul, et 12 parmi les serviteurs de David.
2:16	Chaque homme saisit la tête de son compagnon, avec son épée dans le flanc de son compagnon, et ils tombèrent ensemble. On appela ce lieu Helqath-Hatsourim<!--Helqath-Hatsourim signifie « champ des épées ».-->. Il est près de Gabaon.
2:17	Il y eut ce jour-là un combat très rude, dans lequel Abner et les hommes d'Israël furent battus par les serviteurs de David.
2:18	Les trois fils de Tserouyah, Yoab, Abishaï et Asaël étaient là. Asaël avait les pieds légers comme une gazelle des champs.
2:19	Asaël poursuivit Abner, sans se détourner de lui ni à droite ni à gauche.
2:20	Abner regarda derrière lui et dit : Est-ce toi, Asaël ? Il dit : C'est moi.
2:21	Abner lui dit : Détourne-toi à droite ou à gauche ! Saisis-toi de l'un de ces jeunes hommes et prends sa dépouille. Mais Asaël ne voulut pas se détourner de lui.
2:22	Abner dit encore à Asaël : Détourne-toi de moi ! Pourquoi te frapperais-je et t'abattrais-je à terre ? Comment ensuite lèverais-je mes faces devant ton frère Yoab ?
2:23	Mais Asaël refusa de se détourner. Abner le frappa au ventre avec l'extrémité inférieure de sa lance, qui sortit par-derrière. Il tomba là, raide mort sur place. Tous ceux qui arrivaient au lieu où Asaël était tombé mort, s'y arrêtaient.
2:24	Yoab et Abishaï poursuivirent Abner, et le soleil se couchait quand ils arrivèrent au coteau d'Amma, qui est en face de Guiach, sur le chemin du désert de Gabaon.
2:25	Les fils de Benyamin se rassemblèrent auprès d'Abner et formèrent un corps de troupe et ils s'arrêtèrent sur le sommet d'une colline.
2:26	Abner appela Yoab et dit : L'épée dévorera-t-elle sans cesse ? Ne sais-tu pas qu'il y aura de l'amertume à la fin ? Jusqu'à quand tarderas-tu à dire au peuple qu'il cesse de poursuivre ses frères ?
2:27	Yoab dit : Elohîm est vivant ! Si tu n'avais parlé ainsi, le peuple n'aurait pas cessé avant le matin de poursuivre ses frères.
2:28	Yoab sonna du shofar et tout le peuple s'arrêta. Ils ne poursuivirent plus Israël et ils ne continuèrent plus à se battre.
2:29	Abner et ses hommes marchèrent toute la nuit dans la région aride. Ils passèrent le Yarden, traversèrent tout le Bithron, et arrivèrent à Mahanaïm.
2:30	Yoab aussi revint de la poursuite d'Abner et rassembla tout le peuple. Il manquait 19 hommes parmi les serviteurs de David, et Asaël.
2:31	Mais les serviteurs de David avaient frappé à mort 360 hommes de Benyamin et des hommes d'Abner.
2:32	Ils emportèrent Asaël et l'enterrèrent dans le sépulcre de son père à Bethléhem. Yoab et ses hommes marchèrent toute la nuit et arrivèrent à Hébron au point du jour.

## Chapitre 3

### L'autorité de David s'accroît<!--1 Ch. 3:1-4.-->

3:1	La guerre fut longue entre la maison de Shaoul et la maison de David. David allait en se fortifiant, tandis que la maison de Shaoul allait en s'affaiblissant.
3:2	Il naquit à David des fils à Hébron. Son premier-né fut Amnon, d'Achinoam de Yizre`e'l ;
3:3	le second, Kileab, d'Abigaïl de Carmel, femme de Nabal ; le troisième, Abshalôm, fils de Ma'akah, fille de Talmaï, roi de Guéshour ;
3:4	le quatrième, Adoniyah, fils de Haggith ; le cinquième, Shephatyah, fils d'Abithal ;
3:5	et le sixième, Yithream, d'Églah, femme de David. Ce sont là ceux qui naquirent à David à Hébron.

### Abner fait alliance avec David

3:6	Et il arriva que pendant la guerre entre la maison de Shaoul et la maison de David, Abner tint ferme pour la maison de Shaoul.
3:7	Or Shaoul avait eu une concubine, nommée Ritspah, fille d'Ayah. Il dit à Abner : Pourquoi es-tu venu vers la concubine de mon père ?
3:8	Abner fut très irrité à cause du discours d'Ish-Bosheth, et il lui dit : Suis-je une tête de chien, au service de Yéhouda ? Je fais aujourd'hui preuve de bonté envers la maison de Shaoul, ton père, envers ses frères et ses amis, je ne t'ai pas livré entre les mains de David, et c'est aujourd'hui que tu me reproches une faute avec cette femme ?
3:9	Qu'ainsi Elohîm traite Abner et qu'ainsi il y ajoute, si je n'agis pas avec David selon ce que YHWH a juré à David,
3:10	en faisant passer le royaume de la maison de Shaoul, et en établissant le trône de David sur Israël et sur Yéhouda depuis Dan jusqu'à Beer-Shéba.
3:11	Il ne put répondre une parole à Abner, parce qu'il le craignait.
3:12	Abner envoya des messagers vers David pour dire de sa part : À qui est la terre ? - Pour dire : Fais alliance avec moi, et voici, ma main sera avec toi, pour tourner vers toi tout Israël.
3:13	Il dit : Bien, moi, je ferai alliance avec toi. Je te demande seulement une chose, c'est que tu ne voies pas mes faces, à moins que tu n'amènes d'abord Miykal, fille de Shaoul, quand tu viendras pour voir mes faces.
3:14	David envoya des messagers à Ish-Bosheth, fils de Shaoul, pour lui dire : Rends-moi ma femme Miykal, que j'ai épousée pour 100 prépuces des Philistins.
3:15	Ish-Bosheth l'envoya prendre de chez son homme, Palthiel, fils de Laïsh.
3:16	Son homme alla avec elle, marchant et pleurant après elle, jusqu'à Bahourim. Abner lui dit : Va, retourne-t'en ! Et il s'en retourna.
3:17	Abner parla aux anciens d'Israël et leur dit : Vous désiriez autrefois avoir David pour roi.
3:18	Maintenant agissez ! Car YHWH a parlé de David et a dit : Par la main de David, mon serviteur, je sauverai mon peuple d'Israël de la main des Philistins et de la main de tous ses ennemis.
3:19	Abner parla aussi aux oreilles de Benyamin. Puis Abner alla parler aux oreilles de David à Hébron, tout ce qui semblait bon aux yeux d'Israël et aux yeux de toute la maison de Benyamin.
3:20	Abner vint vers David à Hébron, accompagné de 20 hommes, et David fit un festin à Abner et aux hommes qui étaient avec lui.
3:21	Abner dit à David : Je me lèverai et je partirai pour rassembler tout Israël auprès du roi, mon seigneur. Ils feront alliance avec toi, et tu régneras selon le désir de ton âme. David renvoya Abner, qui s'en alla en paix.
3:22	Voici, les serviteurs de David et Yoab revinrent d'une excursion et amenèrent avec eux un grand butin. Abner n'était plus avec David à Hébron, car David l'avait renvoyé, et il s'en était allé en paix.
3:23	Lorsque arrivèrent Yoab et toute l'armée qui était avec lui, on informa Yoab en disant : Abner, fils de Ner, est venu auprès du roi, qui l'a renvoyé, et il s'en est allé en paix.
3:24	Yoab vint vers le roi, et dit : Qu'as-tu fait ? Voici, Abner est venu vers toi. Pourquoi l'as-tu renvoyé et s'en est-il allé, allé ?
3:25	Tu connais Abner, fils de Ner ! C'est pour te tromper qu'il est venu, pour connaître tes sorties et tes entrées, et pour connaître tout ce que tu as fait !
3:26	Yoab sortit de chez David et envoya derrière Abner des messagers. Ils le firent revenir depuis la fosse de Sira, et David n’en savait rien.

### Mort d'Abner

3:27	Lorsque Abner revint à Hébron, Yoab le tira à l'écart au milieu de la porte, comme pour lui parler tranquillement. Là il le frappa au ventre et le tua pour venger la mort de son frère Asaël.
3:28	David apprit ce qui était arrivé, et dit : Je suis à jamais innocent, mon royaume et moi, devant YHWH, du sang d'Abner, fils de Ner.
3:29	Qu'il retombe sur la tête de Yoab et sur toute la maison de son père ! Que soit retranchée la maison de Yoab, qu'il y ait toujours un homme qui soit atteint d'un flux ou de la lèpre, ou qui s'appuie sur un bâton, ou qui tombe par l'épée, ou qui manque de pain !
3:30	Yoab et Abishaï, son frère, tuèrent Abner, parce qu'il avait tué Asaël, leur frère, à Gabaon, dans la bataille.
3:31	David dit à Yoab et à tout le peuple qui était avec lui : Déchirez vos vêtements, ceignez-vous de sacs et lamentez-vous devant Abner ! Et le roi David marchait derrière le cercueil.
3:32	On enterra Abner à Hébron. Le roi éleva la voix et pleura sur la tombe d'Abner, et tout le peuple pleura.
3:33	Le roi chanta un chant funèbre sur Abner et dit : Abner devait-il mourir comme meurt un insensé ?
3:34	Tes mains n'étaient pas liées, et tes pieds n'étaient pas mis dans des chaînes ! Tu es tombé comme on tombe devant les injustes. Et tout le peuple pleura de nouveau sur Abner.
3:35	Tout le peuple vint pour faire prendre quelque nourriture à David, pendant qu'il était encore jour, mais David jura, en disant : Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute, si je goûte du pain ou quelque chose d'autre avant le coucher du soleil !
3:36	Tout le peuple en eut connaissance et cela fut agréable à leurs yeux, car tout ce que faisait le roi était bon aux yeux de tout le peuple.
3:37	En ce jour, tout le peuple et tout Israël surent que ce n'était pas par ordre du roi qu'Abner, fils de Ner, avait été tué.
3:38	Le roi dit à ses serviteurs : Ne savez-vous pas qu'un chef, un grand homme, est tombé aujourd'hui en Israël ?
3:39	Je suis faible aujourd'hui, bien que j'aie été oint roi, et ces hommes, les fils de Tserouyah, sont trop puissants pour moi. Que YHWH rende à celui qui fait le mal selon sa méchanceté !

## Chapitre 4

### Mort d'Ish-Bosheth

4:1	Quand le fils de Shaoul apprit qu'Abner était mort à Hébron, ses mains restèrent sans force et tout Israël fut terrifié.
4:2	Il y avait deux hommes, chefs de bandes du fils de Shaoul. Le nom de l'un était Ba`anah, et le nom du second Rékab : ils étaient fils de Rimmon de Beéroth, d'entre les fils de Benyamin. Car Beéroth aussi était compté dans Benyamin.
4:3	Les Beérothiens s'étaient enfuis à Guitthaïm, où ils ont habité jusqu'à ce jour.
4:4	Yehonathan, fils de Shaoul, avait un fils perclus des pieds. Il était fils de 5 ans lorsque la nouvelle concernant Shaoul et Yehonathan arriva de Yizre`e'l. Sa nourrice le prit et s'enfuit. Il arriva que, comme elle se hâtait de fuir, il tomba et devint boiteux. Son nom était Mephibosheth.
4:5	Les fils de Rimmon de Beéroth, Rékab et Ba`anah, se rendirent pendant la chaleur du jour à la maison d'Ish-Bosheth, qui était couché pour son repos du midi.
4:6	Ils entrèrent jusqu'au milieu de la maison pour y prendre du froment et le frappèrent au ventre. Puis Rékab et Ba`anah son frère se sauvèrent.
4:7	Ils entrèrent dans la maison pendant qu'Ish-Bosheth était couché sur son lit dans sa chambre à coucher. Ils le frappèrent et le firent mourir, et ils lui coupèrent la tête. Ils prirent sa tête et ils marchèrent toute la nuit par le chemin de la région aride.
4:8	Ils apportèrent la tête d'Ish-Bosheth à David dans Hébron, et ils dirent au roi : Voici la tête d'Ish-Bosheth, fils de Shaoul, ton ennemi, qui en voulait à ton âme. YHWH venge aujourd'hui le roi mon seigneur, de Shaoul et de sa postérité.
4:9	Mais David répondit à Rékab et à Ba`anah, son frère, fils de Rimmon de Beéroth, et leur dit : YHWH, qui a délivré mon âme de toute angoisse, est vivant !
4:10	Car, celui qui m’avait informé en disant : Voici, Shaoul est mort ! et qui était devenu à ses propres yeux comme un porteur de bonnes nouvelles, je l'ai saisi et tué à Tsiklag, pour lui donner la récompense de sa bonne nouvelle,
4:11	combien plus, quand des hommes méchants ont tué un homme juste dans sa maison et sur sa couche, ne redemanderai-je pas maintenant son sang de vos mains et ne vous exterminerai-je pas de la terre ?
4:12	David ordonna à ses jeunes hommes de les tuer. Ils leur coupèrent les mains et les pieds, et les pendirent près de l'étang d'Hébron. Ils prirent la tête d'Ish-Bosheth et l'enterrèrent dans le sépulcre d'Abner à Hébron.

## Chapitre 5

### David oint roi sur tout Israël<!--1 Ch. 11:1-3.-->

5:1	Toutes les tribus d'Israël vinrent vers David à Hébron et dirent : Voici, nous sommes tes os et ta chair.
5:2	Aussi hier et aussi avant-hier, quand Shaoul était roi sur nous, c'est toi qui conduisais et qui ramenais Israël. YHWH t'a dit : Tu paîtras mon peuple d'Israël, et tu seras le chef d'Israël.
5:3	Tous les anciens d'Israël vinrent vers le roi à Hébron, et le roi David fit alliance avec eux à Hébron, devant YHWH. Ils oignirent David pour roi sur Israël.
5:4	David était fils de 30 ans quand il régna. Il régna 40 ans.
5:5	Il régna sur Yéhouda à Hébron 7 ans et 6 mois, puis il régna 33 ans à Yeroushalaim sur tout Israël et Yéhouda.

### Yeroushalaim (Jérusalem), capitale de tout Israël<!--1 Ch. 11:4-9.-->

5:6	Le roi marcha avec ses hommes sur Yeroushalaim contre les Yebousiens qui habitaient cette terre. Ils dirent à David : Tu n'entreras pas ici, car les aveugles mêmes et les boiteux t'en détourneront en disant : David n'entrera pas ici.
5:7	Mais David s'empara de la forteresse de Sion : c'est la cité de David.
5:8	David avait dit en ce jour-là : Quiconque battra les Yebousiens et atteindra le canal, ces aveugles et ces boiteux, qui sont haïs de l'âme de David... C'est pourquoi l'on dit : Aucun aveugle ni boiteux n'entrera dans cette maison.
5:9	Et David habita dans la forteresse et l'appela la cité de David. Il bâtit tout autour, depuis Millo jusqu'au-dedans.
5:10	David allait s’avançant et grandissant, et YHWH, Elohîm Tsevaot, était avec lui.

### YHWH affermit le règne de David

5:11	Hiram, roi de Tyr, envoya des messagers à David, du bois de cèdre, des artisans du bois et des artisans de pierre pour les murs, et ils bâtirent la maison de David.
5:12	David sut que YHWH l'affermissait comme roi sur Israël, et qu'il élevait son royaume à cause de son peuple d'Israël.

### Fils de David nés à Yeroushalaim (Jérusalem)<!--2 S. 3:2-5 ; 1 Ch. 3:1-4.-->

5:13	David prit encore des concubines et des femmes de Yeroushalaim, après qu'il fut venu d'Hébron, et il lui naquit encore des fils et des filles.
5:14	Voici les noms de ceux qui lui naquirent à Yeroushalaim : Shammoua, Shobad, Nathan, Shelomoh,
5:15	Yibhar, Éliyshoua, Népheg, Yaphiya,
5:16	Éliyshama, Élyada et Éliyphelet.

### YHWH livre les Philistins à David<!--2 S. 23:13-17 ; 1 Ch. 11:15-19, 12:9-16, 14:8-17.-->

5:17	Or quand les Philistins apprirent qu'on avait oint David pour roi sur Israël, ils montèrent tous pour chercher David. Et David l'ayant appris, il descendit vers la forteresse.
5:18	Les Philistins arrivèrent et se répandirent dans la vallée des géants.
5:19	David consulta YHWH, en disant : Monterai-je contre les Philistins ? Les livreras-tu entre mes mains ? YHWH dit à David : Monte, car je livrerai, je livrerai les Philistins entre tes mains.
5:20	David vint à Baal-Peratsim, où il les battit. Puis il dit : YHWH a dispersé mes ennemis devant moi, comme des eaux qui s'écoulent. C'est pourquoi il appela ce lieu du nom de Baal-Peratsim.
5:21	Ils laissèrent là leurs faux elohîm, et David et ses hommes les emportèrent.
5:22	Les Philistins montèrent encore une autre fois et se répandirent dans la vallée des géants.
5:23	David consulta YHWH. Et YHWH dit : Tu ne monteras pas ! Contourne-les par-derrière, et tu les atteindras vis-à-vis des mûriers.
5:24	Il arrivera que quand tu entendras le bruit d’une marche au sommet des mûriers, alors hâte-toi, car alors YHWH sera sorti devant toi pour battre l'armée des Philistins.
5:25	David fit ce que YHWH lui avait ordonné, et il battit les Philistins depuis Guéba jusqu'à Guézer.

## Chapitre 6

### Désobéissance dans le transport de l'arche<!--1 Ch. 13:1-14.-->

6:1	David rassembla encore tous les hommes sélectionnés d'Israël : 30 000 hommes.
6:2	David se leva, et alla avec tout le peuple qui était avec lui, de Baalé-Yéhouda, pour faire monter de là l'arche d'Elohîm, qui est appelée du nom, du nom de YHWH Tsevaot, qui est assis sur les chérubins.
6:3	Ils mirent l'arche<!--L'arche ne devait être portée que par les Lévites. Les meilleures intentions pour le service de YHWH ne suffisent pas pour que le Seigneur nous agrée. Nous devons nous conformer à la parole d'Elohîm (1 R. 18:36-39).--> d'Elohîm sur un chariot tout neuf et l'emmenèrent de la maison d'Abinadab qui était sur la colline. Ouzza et Ahyo, fils d'Abinadab, conduisaient le chariot neuf.
6:4	Ils l'emportèrent de la maison d'Abinadab sur la colline. Ahyo allait devant l'arche.
6:5	David et toute la maison d'Israël jouaient devant YHWH de tous les bois de cyprès, de harpes, de luths, de tambourins, de sistres et de cymbales.
6:6	Quand ils furent arrivés à l'aire de Nacon, Ouzza étendit la main vers l'arche d'Elohîm et la saisit parce que les bœufs la faisaient pencher.
6:7	Les narines de YHWH s'enflammèrent contre Ouzza et Elohîm le frappa là à cause de sa faute. Il mourut là, près de l'arche d'Elohîm.
6:8	David fut fâché de ce que YHWH avait fait une brèche en Ouzza. C'est pourquoi on a appelé ce lieu jusqu'à ce jour Pérets-Ouzza.
6:9	David eut peur de YHWH en ce jour-là, et il dit : Comment l'arche de YHWH entrerait-elle chez moi ?
6:10	David ne voulut pas déposer l'arche de YHWH chez lui dans la cité de David, mais il la fit conduire dans la maison d'Obed-Édom de Gath.
6:11	L'arche de YHWH demeura 3 mois dans la maison d'Obed-Édom de Gath, et YHWH bénit Obed-Édom et toute sa maison.

### Accueil de l'arche à Yeroushalaim (Jérusalem)<!--1 Ch. 15:26-16:1.-->

6:12	On vint dire au roi David : YHWH a béni la maison d'Obed-Édom et tout ce qui lui appartient, à cause de l'arche d'Elohîm. Alors David s'y rendit, et il fit monter l'arche d'Elohîm depuis la maison d'Obed-Édom jusqu'à la cité de David, au milieu des réjouissances.
6:13	Il arriva que quand ceux qui portaient l'arche d'Elohîm eurent fait six pas, on sacrifia des taureaux et des béliers gras.
6:14	David dansait de toute sa force devant YHWH - David était ceint d'un éphod de lin.
6:15	David et toute la maison d'Israël firent monter l'arche de YHWH avec des cris de joie et au son du shofar.
6:16	Il arriva que quand l’arche de YHWH entra dans la cité de David, Miykal, fille de Shaoul, regarda par la fenêtre, et voyant le roi David sauter et danser devant YHWH, elle le méprisa dans son cœur.
6:17	Ils amenèrent l'arche de YHWH et la posèrent au milieu de la tente que David avait dressée pour elle, et David fit monter devant YHWH des holocaustes et des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->.
6:18	Quand David eut achevé de faire monter des holocaustes et des sacrifices d'offrande de paix, il bénit le peuple au Nom de YHWH Tsevaot.
6:19	Il partagea à tout le peuple, à toute la multitude d'Israël, tant aux hommes qu'aux femmes, un gâteau de pain, un morceau de viande, un gâteau aux raisins par homme. Et tout le peuple s’en alla, chaque homme dans sa maison.
6:20	David s'en retourna pour bénir aussi sa maison, et Miykal, fille de Shaoul, sortit à sa rencontre. Elle dit : Quel honneur s'est fait aujourd'hui le roi d'Israël, en se découvrant aujourd'hui aux yeux des servantes et de ses serviteurs, comme se découvrirait, se découvrirait un homme sans valeur !
6:21	David dit à Miykal : C'est devant YHWH, qui m'a choisi plutôt que ton père et toute sa maison pour m'établir chef sur le peuple de YHWH, sur Israël, c'est devant YHWH que je me suis réjoui.
6:22	Je me rendrai encore plus insignifiant que cela et je m'humilierai à mes propres yeux. Toutefois je serai honoré auprès des servantes dont tu parles.
6:23	Or Miykal, fille de Shaoul, n'eut pas de progéniture jusqu'au jour de sa mort.

## Chapitre 7

### David veut construire une maison à YHWH<!--1 Ch. 17:1-2.-->

7:1	Il arriva, lorsque le roi habita dans sa maison, et que YHWH lui eut donné du repos de tous ses ennemis qui l'entouraient,
7:2	qu'il dit à Nathan le prophète : Regarde, s'il te plaît ! J'habite dans une maison de cèdres, et l'arche d'Elohîm habite sous des tapis<!--Le terme hébreu est « yeriy'ah », ce qui signifie « rideau », « drap », « tapis ». Voir Ex. 26.-->.
7:3	Nathan dit au roi : Va, fais tout ce qui est dans ton cœur, car YHWH est avec toi.

### YHWH traite alliance avec David et sa postérité<!--1 Ch. 17:3-15.-->

7:4	Il arriva cette nuit-là que la parole de YHWH apparut à Nathan, en disant :
7:5	Va, et dis à David, mon serviteur : Ainsi parle YHWH : Me bâtirais-tu une maison afin que j'y habite ?
7:6	Puisque je n'ai pas habité dans une maison depuis le jour où j'ai fait monter les fils d'Israël hors d'Égypte jusqu'à ce jour. Mais j'ai marché çà et là sous une tente et dans un tabernacle.
7:7	Partout où j'ai marché avec tous les fils d'Israël, ai-je dit un seul mot à l'une des tribus d'Israël à qui j'avais ordonné de paître mon peuple d'Israël, ai-je dit : Pourquoi ne me bâtissez-vous pas une maison de cèdres ?
7:8	Maintenant tu parleras ainsi à David, mon serviteur : Ainsi parle YHWH Tsevaot : Je t'ai pris au pâturage, derrière les brebis, afin que tu deviennes chef sur mon peuple, sur Israël.
7:9	J'ai été avec toi partout où tu as marché, j'ai exterminé tous tes ennemis devant toi et j'ai rendu ton nom grand comme le nom des grands qui sont sur la Terre.
7:10	J'ai établi une demeure à mon peuple, à Israël, et je l'ai planté pour qu'il y habite et ne soit plus agité, pour que les fils de l'injustice ne recommencent plus à l'humilier comme autrefois,
7:11	comme du temps où j'avais établi des juges sur mon peuple d'Israël. Je t'ai accordé du repos face à tous tes ennemis. Et YHWH t'annonce qu'il te bâtira une maison.
7:12	Quand tes jours seront accomplis et que tu seras couché avec tes pères, je susciterai après toi ta postérité, celui qui sera sorti de tes entrailles et j'affermirai son règne.
7:13	Ce sera lui qui bâtira une maison à mon Nom, et j'affermirai pour toujours le trône de son règne<!--Le Royaume millénaire était promis à David et à sa postérité. Il fut proclamé par Yohanan le Baptiste (Mt. 3:2), le Mashiah (Mt. 4:17) et les apôtres (Mt. 10:5-7) comme étant proche. Présentement, le Royaume d'Elohîm se manifeste par la vie sanctifiée des saints en Mashiah (Lu. 17:20-21 ; Jn. 3:1-8 ; Ro. 14:17). Il n'apparaîtra pas de manière visible avant la « moisson », c'est-à-dire le jugement des nations (Mt. 13:39-50). En effet, ce n'est qu'après cette moisson que le Royaume sera installé ici-bas, lorsque le Mashiah rétablira la monarchie et la dynastie de David en sa propre personne. Il rassemblera alors les fils d'Israël dispersés dans le monde entier et établira sa domination sur toute la terre pendant 1 000 ans. Ce Royaume sera remis au Père par le Mashiah après avoir vaincu le dernier ennemi, c'est-à-dire la mort (1 Co. 15:24-26). De ce fait, personne ne mourra pendant le millénium. Toutes les nations monteront tous les ans à Yeroushalaim (Jérusalem) pour adorer YHWH et célébrer la fête des cabanes (ou des tabernacles) qui sera restaurée (Za. 14:16-21). Le gouvernement théocratique en Israël sera alors restauré (Es. 1:26).-->.
7:14	Moi, je deviendrai pour lui un père, et lui, il deviendra pour moi un fils. S'il fait le mal, je le châtierai avec une verge d'hommes et avec des plaies des fils d'humains,
7:15	mais ma grâce ne se retirera pas de lui, comme je l'ai retirée de Shaoul, que j'ai ôté de devant toi.
7:16	Ta maison et ton règne seront assurés à jamais devant toi, ton trône sera pour toujours affermi.
7:17	Nathan rapporta à David toutes ces paroles et toute cette vision.

### Louange et reconnaissance de David envers YHWH<!--1 Ch. 17:16-27.-->

7:18	Le roi David alla et s’assit devant YHWH et dit : Qui suis-je, Adonaï YHWH, et quelle est ma maison, pour que tu m’aies fait arriver jusqu’ici ?
7:19	Mais cela est encore trop peu à tes yeux, Adonaï YHWH, car tu as parlé aussi de la maison de ton serviteur pour les temps éloignés. Telle est la tôwrâh<!--coutume, manière, mode, loi (de l'homme).--> de l'être humain, Adonaï YHWH !
7:20	Qu’est-ce David continuerait encore à te dire ? Tu connais ton serviteur, Adonaï YHWH !
7:21	À cause de ta parole et selon ton cœur, tu as fait monter toute cette grandeur, pour la faire connaître à ton serviteur.
7:22	C'est pourquoi tu t'es montré grand, Adonaï YHWH ! Car nul n'est semblable à toi, et il n'y a pas d'autre Elohîm que toi, d'après tout ce que nous avons entendu de nos oreilles.
7:23	Qui est comme ton peuple, comme Israël, la seule nation de la Terre qu'Elohîm est venu racheter pour en faire son peuple, y mettre son Nom et pour accomplir sur ta terre, devant ton peuple que tu t'es racheté d'Égypte, des choses grandes et terribles contre les nations et contre leurs elohîm ?
7:24	Tu as établi ton peuple d'Israël pour qu'il soit ton peuple à perpétuité, et toi, YHWH, tu es devenu son Elohîm.
7:25	Maintenant, YHWH Elohîm, la parole que tu as dite sur ton serviteur et sur sa maison, accomplis-la à perpétuité, et fais comme tu as dit.
7:26	Que ton Nom soit glorifié à perpétuité, afin qu’on dise : YHWH Tsevaot est l'Elohîm d'Israël ! Et que la maison de David, ton serviteur, demeure stable devant toi !
7:27	Car toi, YHWH Tsevaot, Elohîm d'Israël, tu as découvert l'oreille de ton serviteur en disant : Je te bâtirai une maison ! C'est pourquoi ton serviteur a trouvé le courage de t'adresser cette prière.
7:28	Maintenant, Adonaï YHWH, tu es Elohîm, tes paroles sont vérité, et tu as promis cette grâce à ton serviteur.
7:29	Maintenant, veuille bénir la maison de ton serviteur, afin qu'elle existe à perpétuité devant toi ! Car c'est toi, Adonaï YHWH, qui a parlé, et par ta bénédiction la maison de ton serviteur sera bénie à perpétuité.

## Chapitre 8

### YHWH donne à David la victoire sur ses ennemis<!--1 Ch. 18:1-17.-->

8:1	Après cela, il arriva que David battit les Philistins et les humilia, et il prit Métheg-Amma de la main des Philistins.
8:2	Il battit aussi les Moabites et les mesura au cordeau en les faisant coucher par terre. Il en mesura deux cordeaux pour les faire mourir, et un plein cordeau pour leur laisser la vie. Et les Moabites furent assujettis à David et lui payèrent un tribut.
8:3	David battit aussi Hadadézer, fils de Rehob, roi de Tsoba, quand celui-ci allait remettre la main sur le Fleuve.
8:4	David lui prit 1 700 cavaliers et 20 000 hommes de pied. David coupa les jarrets de tous les chars et ne laissa que 100 chars.
8:5	Les Syriens de Damas vinrent au secours d'Hadadézer, roi de Tsoba, et David battit 22 000 Syriens.
8:6	David mit des garnisons dans la Syrie de Damas. Et les Syriens furent assujettis à David et lui payèrent un tribut. YHWH sauvait David partout où il allait.
8:7	Et David prit les boucliers d'or qui étaient aux serviteurs d'Hadadézer, et les apporta à Yeroushalaim.
8:8	Le roi David emporta aussi une grande quantité de cuivre de Béthach et de Bérothaï, villes d'Hadadézer.
8:9	Thoï, roi de Hamath, apprit que David avait battu toute l'armée d'Hadadézer,
8:10	et Thoï envoya Yoram son fils vers le roi David pour demander sa paix et le bénir pour avoir fait la guerre contre Hadadézer et l'avoir battu. En effet, Thoï était un homme en guerre contre Hadadézer. Et dans sa main il y avait des vases en argent, des vases en or et des vases en cuivre.
8:11	Ceux-là aussi, le roi David les consacra à YHWH, comme il avait consacré l’argent et l’or de toutes les nations qu'il s'était assujetties,
8:12	de la Syrie, de Moab, des fils d'Ammon, des Philistins, d'Amalek, et du butin d'Hadadézer, fils de Rehob, roi de Tsoba.
8:13	Au retour de la défaite des Syriens, David se fit encore un nom, en battant dans la vallée du sel 18 000 Édomites.
8:14	Il mit des garnisons dans Édom, il mit des garnisons dans tout Édom. Et tout Édom fut assujetti à David. YHWH protégeait David partout où il allait.
8:15	David régna sur tout Israël, et il agissait envers tout son peuple selon le jugement et la justice.
8:16	Yoab, fils de Tserouyah, commandait l'armée ; Yehoshaphat, fils d'Ahiloud, était archiviste ;
8:17	Tsadok, fils d'Ahitoub, et Achiymélek, fils d'Abiathar, étaient prêtres ; Serayah était scribe ;
8:18	Benayah, fils de Yehoyada, était chef des Kéréthiens et des Péléthiens. Et les fils de David étaient prêtres.

## Chapitre 9

### Mephibosheth à la table de David

9:1	David dit : Ne reste-t-il personne de la maison de Shaoul, afin que je lui fasse du bien pour l'amour de Yehonathan ?
9:2	Il y avait dans la maison de Shaoul un serviteur nommé Tsiba, que l'on fit venir auprès de David. Le roi lui dit : Es-tu Tsiba ? Il dit : Je suis ton serviteur !
9:3	Le roi dit : N'y a-t-il plus personne de la maison de Shaoul, pour que j'use envers lui de la bonté d'Elohîm ? Tsiba dit au roi : Il y a encore un des fils de Yehonathan, qui est perclus des pieds.
9:4	Le roi lui dit : Où est-il ? Et Tsiba dit au roi : Il est dans la maison de Makir, fils d'Ammiel, à Lodebar.
9:5	Le roi David l'envoya chercher dans la maison de Makir, fils d'Ammiel, à Lodebar.
9:6	Quand Mephibosheth, fils de Yehonathan, fils de Shaoul, vint auprès de David, il tomba sur ses faces et se prosterna. David dit : Mephibosheth ! Il dit : Voici ton serviteur.
9:7	David lui dit : N'aie pas peur, car je te traiterai, je te traiterai avec bonté par égard pour ton père Yehonathan. Je te restituerai tous les champs de Shaoul, ton père, et tu mangeras toujours du pain à ma table.
9:8	Il se prosterna et dit : Qui suis-je, moi ton serviteur, pour que tu regardes un chien mort tel que moi ?
9:9	Le roi appela Tsiba, serviteur de Shaoul, et lui dit : Je donne au fils de ton maître tout ce qui appartenait à Shaoul et à toute sa maison.
9:10	Tu travailleras pour lui le sol, toi, tes fils, et tes serviteurs, et tu en recueilleras les fruits, afin que le fils de ton maître ait du pain à manger. Et Mephibosheth, le fils de ton maître, mangera toujours du pain à ma table. Or Tsiba avait 15 fils et 20 serviteurs.
9:11	Tsiba dit au roi : Ton serviteur fera tout ce que le roi, mon seigneur, ordonne à son serviteur. Et Mephibosheth mangea à la table de David comme l'un des fils du roi.
9:12	Mephibosheth avait un jeune fils, nommé Miyka, et tous ceux qui demeuraient dans la maison de Tsiba étaient serviteurs de Mephibosheth.
9:13	Mephibosheth habitait à Yeroushalaim parce qu'il mangeait toujours à la table du roi. Il était boiteux des deux pieds.

## Chapitre 10

### David monte contre les Ammonites et les Syriens<!--1 Ch. 19.-->

10:1	Or il arriva après cela que le roi des fils d'Ammon mourut, et Hanoun, son fils, régna à sa place.
10:2	David dit : J'userai de bonté envers Hanoun, fils de Nachash, comme son père a usé de bonté envers moi. David lui envoya donc, par la main de ses serviteurs, ses consolations au sujet de son père. Lorsque les serviteurs de David arrivèrent dans la terre des fils d'Ammon,
10:3	les chefs des fils d'Ammon dirent à Hanoun, leur maître : Penses-tu que ce soit pour honorer ton père que David t'envoie des consolateurs ? N’est-ce pas pour examiner la ville, pour l’espionner et la détruire que David envoie ses serviteurs auprès de toi ?
10:4	Hanoun saisit les serviteurs de David, et fit raser la moitié de leur barbe, et couper la moitié de leurs habits jusqu'aux fesses, et les renvoya.
10:5	David en fut informé et envoya des gens à leur rencontre, car ces hommes étaient très humiliés. Le roi leur fit dire : Restez à Yeriycho jusqu'à ce que votre barbe ait poussé, et vous reviendrez.
10:6	Les fils d'Ammon voyant qu’ils étaient en mauvaise odeur auprès de David, les fils d'Ammon envoyèrent et prirent à leur solde 20 000 hommes de pied chez les Syriens de Beth-Rehob, et chez les Syriens de Tsoba, 1 000 hommes chez le roi de Ma'akah, et 12 000 hommes chez les hommes de Tob.
10:7	Et lorsque David l'apprit, il envoya Yoab et toute l'armée, les hommes les plus vaillants.
10:8	Les fils d'Ammon sortirent et se rangèrent en bataille à l'entrée de la porte. Les Syriens de Tsoba de Rehob et les hommes de Tob et de Ma'akah étaient à part dans la campagne.
10:9	Yoab, voyant que la face de l'armée était contre lui, en face et derrière, choisit alors des jeunes hommes parmi tous ceux d'Israël et les rangea contre les Syriens.
10:10	Il plaça le reste du peuple sous la main d'Abishaï, son frère, qui les rangea contre les fils d'Ammon.
10:11	Il dit : Si les Syriens sont plus forts que moi, tu viendras à mon secours, et si les fils d'Ammon sont plus forts que toi, j'irai te secourir.
10:12	Sois vaillant, et portons-nous vaillamment pour notre peuple et pour les villes de notre Elohîm, et que YHWH fasse ce qui est bon à ses yeux !
10:13	Yoab et le peuple qui était avec lui s'approchèrent pour livrer bataille aux Syriens et ils s'enfuirent en face de lui.
10:14	Quand les fils d'Ammon virent que les Syriens avaient pris la fuite, ils s'enfuirent aussi en face d'Abishaï et rentrèrent dans la ville. Yoab s'éloigna des fils d'Ammon et revint à Yeroushalaim.
10:15	Les Syriens, voyant qu'ils avaient été battus en face d’Israël, se rassemblèrent.
10:16	Hadarézer envoya chercher les Syriens qui étaient de l'autre côté du fleuve. Ils arrivèrent à Hélam, et Shobac, chef de l'armée d'Hadarézer, était devant eux.
10:17	Cela fut rapporté à David, qui rassembla tout Israël, passa le Yarden et vint à Hélam. Les Syriens se rangèrent en bataille contre David et se battirent avec lui.
10:18	Mais les Syriens s'enfuirent en face d'Israël. Et David défit 700 chars des Syriens et 40 000 cavaliers. Il frappa aussi Shobac, le chef de leur armée, qui mourut sur place.
10:19	Tous les rois soumis à Hadarézer, se voyant battus en face d'Israël, firent en effet la paix avec Israël et lui furent assujettis. Les Syriens craignirent de porter encore secours aux fils d'Ammon.

## Chapitre 11

### Péché de David avec Bath-Shéba

11:1	Il arriva, au retour de l’année, au temps où sortent les rois, que David envoya Yoab, avec ses serviteurs et tout Israël, pour détruire les fils d'Ammon et assiéger Rabba. Mais David resta à Yeroushalaim<!--Au lieu d'aller en guerre et de diriger ses troupes, David resta à Yeroushalaim (Jérusalem). Cette négligence l'a conduit à la convoitise, à l'adultère et au meurtre d'Ouriyah. La distraction peut conduire à la mort. Il y a un temps pour toutes choses (Ec. 3:1-8).-->.
11:2	Et il arriva, au temps du soir, que David, s'étant levé de sa couche et se promenant sur le toit de la maison royale, aperçut du toit une femme qui se baignait. Cette femme était très belle de figure.
11:3	David envoya demander qui était cette femme, et on lui dit : N'est-ce pas Bath-Shéba, fille d'Éliy`am, femme d'Ouriyah, le Héthien ?
11:4	Et David envoya des messagers pour la chercher. Elle vint vers lui, et il coucha avec elle. Après s'être purifiée de sa souillure, elle retourna dans sa maison.
11:5	La femme devint enceinte, et elle envoya l’annoncer à David, en lui disant : Je suis enceinte.
11:6	David envoya dire à Yoab : Envoie-moi Ouriyah, le Héthien. Et Yoab envoya Ouriyah à David.
11:7	Ouriyah se rendit auprès de David, qui l'interrogea sur la paix de Yoab, sur la paix du peuple et sur la paix de la guerre.
11:8	David dit à Ouriyah : Descends dans ta maison et lave tes pieds. Ouriyah sortit de la maison du roi, et on fit porter après lui un présent du roi.
11:9	Mais Ouriyah se coucha à la porte de la maison du roi, avec tous les serviteurs de son maître, et il ne descendit pas dans sa maison.
11:10	On en informa David en disant : Ouriyah n'est pas descendu dans sa maison. David dit à Ouriyah : N'arrives-tu pas de voyage ? Pourquoi n'es-tu pas descendu dans ta maison ?
11:11	Ouriyah dit à David : L'arche, Israël et Yéhouda habitent sous des tentes, mon seigneur Yoab et les serviteurs de mon seigneur campent aux champs, et moi j'entrerais dans ma maison pour manger et boire et pour coucher avec ma femme ! Tu es vivant et ton âme est vivante, je ne ferai pas une telle chose.
11:12	David dit à Ouriyah : Reste ici encore aujourd'hui, et demain je te renverrai. Ouriyah resta ce jour-là et le lendemain à Yeroushalaim.
11:13	David l'invita à manger et à boire en sa présence et l'enivra. Mais le soir, il sortit pour se coucher sur son lit avec tous les serviteurs de son maître, et il ne descendit pas dans sa maison.
11:14	Et il arriva, au matin, que David écrivit une lettre à Yoab et l'envoya par la main d'Ouriyah.
11:15	Il écrivit en disant : Placez Ouriyah face au plus fort de la guerre et retirez-vous en arrière de lui, afin qu'il soit frappé et qu'il meure !
11:16	Yoab, en observant la ville, plaça Ouriyah à l'endroit où il savait qu'il y avait des hommes talentueux.
11:17	Les hommes de la ville sortirent et combattirent contre Yoab. Il en tomba parmi le peuple, parmi les serviteurs de David. Ouriyah le Héthien fut lui aussi tué.
11:18	Yoab envoya un messager à David pour lui faire savoir tout ce qui était arrivé dans ce combat.
11:19	Il donna cet ordre au messager en disant : Quand tu auras achevé de raconter au roi tout ce qui est arrivé au combat,
11:20	s'il arrive que le roi se mette en colère et qu'il te dise : Pourquoi vous êtes-vous approchés de la ville pour combattre ? Ne savez-vous pas qu'on tire de dessus la muraille ?
11:21	Qui a tué Abiymélek, fils de Yeroubbésheth ? N'est-ce pas une femme qui lança sur lui de dessus la muraille une pièce de meule de moulin, et n'en est-il pas mort à Thébets ? Pourquoi vous êtes-vous approchés de la muraille ? Alors tu lui diras : Ton serviteur Ouriyah, le Héthien, est mort aussi.
11:22	Le messager partit. À son arrivée, il fit savoir à David tout ce pourquoi Yoab l'avait envoyé.
11:23	Le messager dit à David : Oui, ces hommes ont été plus forts que nous. Ils avaient fait une sortie contre nous dans les champs, mais nous les avons repoussés jusqu'à l'entrée de la porte.
11:24	Les archers ont tiré sur tes serviteurs du haut de la muraille et des serviteurs du roi ont été tués. Ton serviteur Ouriyah, le Héthien est mort aussi.
11:25	David dit au messager : Tu diras ainsi à Yoab : Que cette affaire ne paraisse pas mauvaise à tes yeux, car l'épée dévore tantôt l'un, tantôt l'autre. Renforce ta guerre contre la ville et détruis-la. Quant à toi, encourage-le !
11:26	La femme d'Ouriyah apprit qu'Ouriyah son homme était mort et elle se lamenta sur son mari.
11:27	Quand le deuil fut passé, David l'envoya chercher et la recueillit dans sa maison. Elle devint sa femme, et lui enfanta un fils. Mais la chose que David avait faite parut mauvaise aux yeux de YHWH.

## Chapitre 12

### Le prophète Nathan envoyé pour reprendre David

12:1	YHWH envoya Nathan vers David. Nathan vint vers lui et lui dit : Il y avait deux hommes dans une ville, l'un riche et l'autre pauvre.
12:2	Le riche avait des brebis et des bœufs en très grand nombre.
12:3	Mais le pauvre n'avait rien du tout, sauf une petite brebis, qu'il avait achetée. Il la nourrissait et elle grandissait chez lui avec ses enfants. Elle mangeait de son pain, buvait dans sa coupe, dormait sur son sein et elle était comme sa fille.
12:4	Un voyageur arriva chez l'homme riche, mais celui-ci a épargné ses brebis et ses bœufs, pour préparer un repas au voyageur qui était venu chez lui. Il a pris la brebis du pauvre homme et l'a préparée pour l'homme qui était venu chez lui.
12:5	Les narines de David s'enflammèrent violemment contre cet homme, et il dit à Nathan : YHWH est vivant ! L'homme qui fait cela est un fils de la mort.
12:6	La brebis, il la paiera au quadruple<!--Lé. 5:20-24 ; Lu. 18:8.--> pour avoir fait cette chose et pour ne pas l’avoir épargnée.
12:7	Nathan dit à David : Tu es cet homme-là ! Ainsi parle YHWH, l'Elohîm d'Israël : Je t'ai oint pour roi sur Israël et je t'ai délivré de la main de Shaoul ;
12:8	je t'ai même donné la maison de ton maître, et les femmes de ton maître dans ton sein, et je t'ai donné la maison d'Israël et de Yéhouda. Et si cela avait été peu, j'aurais ajouté pour toi ceci ou cela.
12:9	Pourquoi as-tu méprisé la parole de YHWH, en faisant ce qui est mal à ses yeux ? Tu as frappé de l'épée Ouriyah, le Héthien. Sa femme, tu l’as prise pour ta femme, et lui-même, tu l’as tué par l'épée des fils d'Ammon.
12:10	Maintenant, l'épée ne s'éloignera jamais de ta maison, parce que tu m'as méprisé et que tu as pris la femme d'Ouriyah le Héthien pour qu’elle devienne ta femme.
12:11	Ainsi parle YHWH : Voici que je vais faire lever, de ta maison, le malheur sur toi. Je prendrai sous tes yeux tes femmes pour les donner à ton compagnon. Il couchera avec tes femmes sous les yeux de ce soleil.
12:12	Car tu l'as fait dans un lieu caché, mais moi, je ferai cette chose face à tout Israël et face au soleil !

### Repentance de David

12:13	David dit à Nathan : J'ai péché contre YHWH ! Et Nathan dit à David : YHWH passe par-dessus ton péché, tu ne mourras pas.
12:14	Toutefois, parce que, par cette chose, tu as fait mépriser, mépriser YHWH par ses ennemis, le fils aussi qui t'est né, mourir, il mourra.
12:15	Et Nathan retourna dans sa maison. YHWH frappa l'enfant que la femme d'Ouriyah avait enfanté à David, et il tomba malade.
12:16	David pria Elohîm pour le garçon, et David jeûna et, quand il rentra, il passa la nuit couché par terre.
12:17	Les anciens de sa maison se levèrent et vinrent vers lui pour le faire lever de terre, mais il ne voulut pas et ne mangea pas le pain avec eux.
12:18	Il arriva que l'enfant mourut le septième jour. Les serviteurs de David craignaient de lui annoncer que l'enfant était mort. Car ils se disaient : Voici, quand l'enfant vivait encore, nous lui avons parlé, et il n'a pas écouté notre voix. Comment pourrions-nous lui dire : L'enfant est mort ! Il fera quelque chose de mauvais.
12:19	David vit que ses serviteurs parlaient à voix basse, et il comprit que l'enfant était mort. David dit à ses serviteurs : L'enfant est-il mort ? Ils dirent : Il est mort.
12:20	David se leva de terre. Il se lava, s'oignit, et changea de vêtements. Il alla dans la maison de YHWH et s'y prosterna. De retour chez lui, il demanda qu’on mît du pain devant lui, et il mangea.
12:21	Ses serviteurs lui dirent : Qu'est-ce que tu fais ? Tu jeûnais et pleurais pour l'amour de l'enfant lorsqu'il vivait encore. Maintenant que l'enfant est mort, tu te lèves et tu manges !
12:22	Il dit : Quand l'enfant vivait encore, je jeûnais et pleurais, car je me disais : Qui sait si YHWH n'aura pas pitié de moi et si l'enfant ne vivra pas ?
12:23	Maintenant qu'il est mort, pourquoi jeûnerais-je ? Pourrais-je le faire revenir ? Moi, j’irai vers lui. Lui, il ne reviendra pas vers moi.

### Naissance de Shelomoh

12:24	David consola sa femme Bath-Shéba, et il alla auprès d'elle et coucha avec elle. Elle lui enfanta un fils qu'il appela du nom de Shelomoh. YHWH l'aima,
12:25	et il envoya par la main de Nathan, le prophète, qui l’appela du nom de Yediydeyah, à cause de YHWH.

### Le territoire et le roi de Rabba livrés à Yoab et David<!--1 Ch. 20:1-3.-->

12:26	Yoab combattait contre Rabba qui appartenait aux fils d'Ammon, il s'empara de la ville royale,
12:27	et envoya des messagers à David pour lui dire : J'ai attaqué Rabba, et j'ai pris la ville des eaux.
12:28	C'est pourquoi rassemble maintenant le reste du peuple, campe contre la ville et prends-la, de peur que je ne prenne la ville et qu'on ne l'appelle de mon nom.
12:29	David rassembla tout le peuple et marcha contre Rabba. Il l'attaqua et la prit.
12:30	Il enleva la couronne de dessus la tête de son roi. Elle pesait un talent d'or et était garnie de pierres précieuses. On la mit sur la tête de David, qui emporta de la ville un très grand butin.
12:31	Il fit sortir aussi le peuple qui s'y trouvait, et il le plaça sous des scies, des herses de fer, des haches de fer et le fit passer par un fourneau où l'on cuit les briques. Il traita ainsi toutes les villes des fils d'Ammon. Et David retourna avec tout le peuple à Yeroushalaim.

## Chapitre 13

### David subit les conséquences de son péché

13:1	Après cela il arriva qu'Abshalôm, fils de David, ayant une sœur qui était belle et qui se nommait Tamar, Amnon, fils de David, l'aima.

### Inceste au sein de la famille royale

13:2	Amnon fut si tourmenté qu'il tomba malade à cause de Tamar sa sœur, car elle était vierge, et il était difficile aux yeux d’Amnon de lui faire quoi que ce soit.
13:3	Amnon avait un ami, nommé Yonadab, fils de Shim`ah, frère de David, et Yonadab était un homme très rusé.
13:4	Il lui dit : Fils de roi, pourquoi maigris-tu ainsi de matin en matin ? Ne veux-tu pas me le dire ? Amnon lui dit : J'aime Tamar, la sœur de mon frère, Abshalôm.
13:5	Yehonadab lui dit : Couche-toi dans ton lit et fais le malade. Quand ton père viendra te voir, tu lui diras : S’il te plaît, que Tamar, ma sœur, vienne et qu’elle me donne à manger du pain, et qu’elle prépare de la nourriture sous mes yeux, afin que je le voie et que je le prenne de sa main.
13:6	Amnon se coucha et fit le malade. Le roi vint le voir, et Amnon dit au roi : S'il te plaît, que ma sœur Tamar vienne faire deux pains sous mes yeux, et que je les mange de sa main.
13:7	David envoya dire à Tamar dans la maison : S’il te plaît, va à la maison de ton frère Amnon, et prépare-lui de la nourriture.
13:8	Tamar alla dans la maison de son frère Amnon, qui était couché. Elle prit de la pâte, la pétrit, et en fit devant lui des pains et les fit cuire.
13:9	Elle prit ensuite la poêle et les versa devant lui, mais il refusa de manger. Amnon dit : Faites sortir tout homme loin de moi ! Ils firent sortir tout homme loin de lui. 
13:10	Amnon dit à Tamar : Apporte la nourriture dans la chambre, et que je la mange de ta main. Tamar prit les pains qu'elle avait faits, et les apporta à Amnon, son frère dans la chambre.
13:11	Comme elle les lui présentait pour qu'il en mange, il se saisit d'elle et lui dit : Viens, couche avec moi, ma sœur !
13:12	Elle lui dit : Non, mon frère, ne m'humilie pas, car cela ne se fait pas en Israël. Ne commets pas cette infamie.
13:13	Et moi, où irais-je avec mon insulte ? Et toi, tu serais comme l'un des infâmes en Israël. Maintenant, s'il te plaît, parle au roi, et il ne s'opposera pas à ce que je sois ta femme.
13:14	Mais il ne voulut pas écouter sa voix. Il fut plus fort qu'elle, l'humilia et coucha avec elle<!--Le viol et l'inceste que commit Amnon, fils de David, sur Tamar, sa demi-sœur, furent les conséquences du péché de David avec Bath-Shéba.-->.
13:15	Amnon la haït d’une très grande haine, car la haine dont il la haïssait était plus grande que l’amour dont il l’avait aimée. Amnon lui dit : Lève-toi, va-t'en !
13:16	Elle lui dit : Tu n'as aucune raison de me faire cela, car me renvoyer serait un mal plus grand que l'autre, celui que tu m'as déjà fait.
13:17	Mais il ne voulut pas l'écouter, et appelant le garçon qui le servait, il dit : S’il te plaît, envoie celle-la loin de moi, dehors ! Et ferme la porte derrière elle !
13:18	Elle était habillée d'une tunique de couleurs, car c'était la robe dont étaient vêtues les filles vierges du roi. Le serviteur d'Amnon la mit dehors, et ferma la porte après elle.
13:19	Tamar prit de la cendre sur la tête, et déchira sa tunique de couleurs et, ayant mis la main sur la tête, elle s’en alla en criant.
13:20	Son frère Abshalôm lui dit : Ton frère, Amnon, a-t-il été avec toi ? Maintenant, ma sœur, tais-toi, c'est ton frère. Ne fixe pas ton cœur sur cette affaire. Et Tamar, dévastée, resta dans la maison d'Abshalôm son frère.
13:21	Quand le roi David eut appris toutes ces choses, il fut très irrité.
13:22	Abshalôm ne parla ni en bien ni en mal avec Amnon, mais il éprouva de la haine pour lui parce qu'il avait humilié Tamar, sa sœur.

### Vengeance d'Abshalôm sur Amnon

13:23	Deux ans après, il arriva comme Abshalôm avait les tondeurs à Baal-Hatsor, près d'Éphraïm, qu'Abshalôm invita tous les fils du roi.
13:24	Abshalôm alla vers le roi et dit : Voici ton serviteur a les tondeurs. S'il te plaît, que le roi et ses serviteurs viennent avec ton serviteur.
13:25	Et le roi dit à Abshalôm : Non, mon fils, nous n'irons pas tous, s’il te plaît, de peur que nous ne te soyons à charge. Abshalôm le pressa, mais le roi ne voulut pas aller et il le bénit.
13:26	Abshalôm dit : Sinon, qu'Amnon, mon frère, vienne avec nous, s'il te plaît. Le roi lui dit : Pourquoi irait-il ?
13:27	Abshalôm le pressa tellement qu'il laissa aller Amnon et tous les fils du roi avec lui.
13:28	Or Abshalôm avait donné cet ordre à ses serviteurs, en disant : Discernez bien, s'il vous plaît, quand le cœur d'Amnon sera égayé par le vin et que je vous dirai : Frappez Amnon ! Vous le tuerez. N'ayez pas peur, n'est-ce pas moi qui vous l'ordonne ? Soyez forts, soyez des fils talentueux !
13:29	Les serviteurs d'Abshalôm traitèrent Amnon comme Abshalôm l'avait ordonné. Tous les fils du roi se levèrent et montèrent chaque homme sur son mulet et s'enfuirent.
13:30	Il arriva, comme ils étaient en chemin, que le bruit parvint à David, en disant : Abshalôm a tué tous les fils du roi et il n’en reste pas un seul !
13:31	Le roi se leva, déchira ses vêtements et se coucha par terre. Tous ses serviteurs se tenaient là, avec leurs vêtements déchirés.
13:32	Yonadab, fils de Shim`ah, frère de David, prit la parole et dit : Que mon seigneur ne dise pas que tous les jeunes hommes, fils du roi, ont été tués, car seul Amnon est mort. En effet, c'était chose réglée par la bouche d'Abshalôm depuis le jour où Amnon a humilié Tamar, sa sœur.
13:33	Maintenant, que le roi mon seigneur ne prenne pas la chose à cœur, en disant que tous les fils du roi sont morts, car Amnon seul est mort.
13:34	Abshalôm prit la fuite. Or le jeune homme placé en sentinelle leva les yeux et regarda. Et voici, un grand peuple venait par le chemin qui était derrière lui, du côté de la montagne.
13:35	Yonadab dit au roi : Voici les fils du roi qui arrivent ! Selon la parole de ton serviteur, ainsi il en est arrivé.
13:36	Il arriva que comme il achevait de parler, voici les fils du roi arrivèrent. Ils élevèrent la voix et pleurèrent. Le roi aussi et tous ses serviteurs pleurèrent des pleurs très abondants.

### Abshalôm s'enfuit loin de son père

13:37	Abshalôm s'était enfui, et il alla chez Talmaï, fils d'Ammihour, roi de Guéshour<!--Abshalôm s'était réfugié chez Talmaï, roi de Guéshour (Transjordanie, au nord de la Syrie), qui était le père de Ma'akah, sa mère (2 S. 3:3). Il est donc allé chez son grand-père maternel.-->. Et David pleurait tous les jours son fils.
13:38	Abshalôm resta 3 ans à Guéshour, où il était allé, après avoir pris la fuite.
13:39	Le roi David cessa de poursuivre Abshalôm, car il était consolé de la mort d'Amnon.

## Chapitre 14

### Yoab convainc le roi de faire revenir Abshalôm

14:1	Yoab, fils de Tserouyah, sachant que le cœur du roi était pour Abshalôm,
14:2	Yoab envoya chercher à Tekoa une femme habile, et il lui dit : Fais semblant de te lamenter, s'il te plaît, et revêts des habits de deuil : ne t'oins pas d'huile, mais sois comme une femme qui de longs jours pleure un mort.
14:3	Tu entreras chez le roi et tu lui parleras selon cette parole. Yoab mit les paroles dans sa bouche.
14:4	La femme de Tekoa alla parler au roi. Elle tomba le visage contre terre, se prosterna, et dit : Roi, sauve-moi !
14:5	Le roi lui dit : Qu'as-tu ? Elle dit : Vraiment, je suis une femme veuve, et mon mari est mort !
14:6	Or ta servante avait deux fils. Ils se sont tous deux querellés dans les champs et il n'y avait personne pour les séparer. L'un a frappé l'autre et l'a tué.
14:7	Et voici, toute la famille s'est élevée contre ta servante, en disant : Donne-nous le meurtrier de son frère ! Nous voulons le faire mourir, pour l'âme de son frère qu'il a tué, et que nous exterminions même l'héritier ! Ils veulent ainsi éteindre le charbon vif qui me restait, pour ne laisser à mon mari ni nom ni survivant sur les faces du sol.
14:8	Le roi dit à la femme : Va-t-en dans ta maison, et je donnerai des ordres en ta faveur.
14:9	La femme de Tekoa dit au roi : Mon seigneur le roi ! Que l'iniquité soit sur moi et sur la maison de mon père, et que le roi et son trône en soient innocents.
14:10	Le roi dit : Si quelqu'un parle contre toi, amène-le-moi, et jamais il ne lui arrivera de te toucher.
14:11	Elle dit : S'il te plaît, que le roi se souvienne de YHWH, son Elohîm, afin que le racheteur du sang n'augmente pas la ruine et qu'on ne fasse pas périr mon fils. Et il dit : YHWH est vivant ! Il ne tombera pas à terre un seul des cheveux de ton fils.
14:12	La femme dit : S'il te plaît, que ta servante dise un mot au roi, mon seigneur. Et il dit : Parle !
14:13	La femme dit : Pourquoi as-tu pensé ainsi contre le peuple d'Elohîm ? Le roi, en prononçant ce discours, le roi est coupable de ne pas faire revenir celui qu'il a banni.
14:14	Car nous mourrons, nous mourrons, et nous sommes comme l'eau versée sur la terre qu'on ne peut recueillir. Elohîm ne prend pas l'âme, mais il forme des projets pour que le banni ne soit plus banni loin de sa présence.
14:15	Maintenant, si je suis venue dire cette parole au roi, mon seigneur, c'est parce que le peuple m’a fait peur. Et ta servante s'est dit : Que je parle au roi, s’il te plaît. Peut-être le roi accomplira-t-il la parole de sa servante.
14:16	Car le roi écoutera sa servante pour la délivrer de la paume de l'homme qui nous extermine, moi et mon fils ensemble, de l'héritage d'Elohîm.
14:17	Ta servante s'est dit : Que la parole du roi, mon seigneur, s’il te plaît, nous apporte du repos. Car le roi mon seigneur est comme un ange d'Elohîm, pour entendre le bien et le mal. Que YHWH, ton Elohîm, soit avec toi !
14:18	Le roi répondit et dit à la femme : S'il te plaît, ne me cache pas une parole de ce que je vais te demander. La femme dit : Que le roi mon seigneur parle !
14:19	Le roi dit : La main de Yoab n'est-elle pas avec toi dans tout ceci ? La femme répondit et dit : Ton âme est vivante, ô roi, mon seigneur, qu’il n’y a rien à droite ou à gauche de tout ce qu’a dit le roi, mon seigneur. Oui, c’est ton serviteur Yoab qui m’a donné cet ordre et qui a mis dans la bouche de ta servante toutes ces paroles.
14:20	C’est afin de tourner les faces de la parole, que ton serviteur Yoab a agi selon cette parole. Mais mon seigneur est sage comme un ange d'Elohîm, pour savoir tout ce qui se passe sur la Terre.

### Retour d'Abshalôm à Yeroushalaim (Jérusalem)

14:21	Le roi dit à Yoab : Voici donc, j'agirai selon cette parole. Va, et fais revenir le jeune homme Abshalôm.
14:22	Yoab tomba sur ses faces à terre et se prosterna, et il bénit le roi. Yoab dit : Aujourd'hui, ton serviteur sait qu'il a trouvé grâce à tes yeux, roi mon seigneur, puisque le roi agit selon la parole de son serviteur.
14:23	Yoab se leva et partit pour Guéshour, et il ramena Abshalôm à Yeroushalaim.
14:24	Le roi dit : Qu'il retourne dans sa maison et qu'il ne voie pas mes faces. Abshalôm retourna dans sa maison et ne vit pas les faces du roi.
14:25	Il n'y avait pas d'homme dans tout Israël aussi renommé qu'Abshalôm pour sa beauté. Depuis la plante des pieds jusqu'au sommet de la tête, il n'y avait pas en lui de défaut.
14:26	Quand il se rasait la tête, il la rasait à la fin de chaque année, parce que cela devenait pesant et il se rasait. Les cheveux de sa tête pesaient 200 sicles, poids du roi.
14:27	Il naquit à Abshalôm trois fils, et une fille nommée Tamar, qui était une femme belle de figure.
14:28	Abshalôm demeura deux années de jours à Yeroushalaim, sans voir les faces du roi.
14:29	Abshalôm fit demander Yoab, pour l'envoyer vers le roi, mais il ne voulut pas venir vers lui. Il le fit demander encore pour la seconde fois mais il ne voulut pas venir.
14:30	Il dit à ses serviteurs : Vous voyez le champ de Yoab qui est à côté du mien, il y a de l'orge. Allez et mettez-y le feu ! Et les serviteurs d'Abshalôm mirent le feu au champ.
14:31	Yoab se leva et vint vers Abshalôm dans sa maison. Il lui dit : Pourquoi tes serviteurs ont-ils mis le feu à mon champ ?
14:32	Et Abshalôm dit à Yoab : Voici, j'ai envoyé vers toi en disant : Viens ici, et je t'enverrai vers le roi, afin que tu lui dises : Pourquoi suis-je revenu de Guéshour ? Il vaudrait mieux pour moi que j'y sois encore. Maintenant, je verrai les faces du roi et, s'il y a de l'iniquité en moi, qu'il me fasse mourir !
14:33	Yoab alla vers le roi et lui rapporta cela. Et le roi appela Abshalôm, qui vint vers lui et se prosterna le visage contre terre, en face du roi. Le roi embrassa Abshalôm.

## Chapitre 15

### Mauvaises intentions d'Abshalôm

15:1	Or il arriva qu'après cela, Abshalôm se procura des chars et des chevaux, et il avait 50 hommes qui couraient devant lui<!--La révolte d'Abshalôm était une autre conséquence du péché de David avec Bath-Shéba.-->.
15:2	Abshalôm se levait de bon matin et se tenait au bord du chemin de la porte. Et chaque fois qu'un homme ayant une querelle venait vers le roi pour un jugement, Abshalôm l'appelait et lui disait : De quelle ville es-tu ? Il disait : Ton serviteur est de l'une des tribus d'Israël.
15:3	Abshalôm lui disait : Vois ! Ton discours est bon et juste, mais personne ne t'écoutera chez le roi.
15:4	Abshalôm disait encore : Qui m'établira juge sur la terre ? Tout homme qui aurait une querelle et un jugement viendrait vers moi, et je lui ferais justice.
15:5	Et il arrivait aussi que quand quelqu'un s'approchait de lui pour se prosterner, il lui tendait sa main, le saisissait et l'embrassait.
15:6	Abshalôm agissait de cette manière envers tous ceux d’Israël qui venaient vers le roi pour un jugement. Et Abshalôm volait le cœur des hommes d'Israël.

### Conspiration d'Abshalôm

15:7	Et il arriva qu'au bout de 40 ans, Abshalôm dit au roi : S'il te plaît, que j'aille à Hébron pour accomplir le vœu que j'ai fait à YHWH.
15:8	Car quand ton serviteur demeurait à Guéshour en Syrie, il fit un vœu, en disant : Si YHWH me ramène, s'il me ramène à Yeroushalaim, je servirai YHWH.
15:9	Le roi lui dit : Va en paix. Abshalôm se leva et s'en alla à Hébron.
15:10	Abshalôm envoya des espions dans toutes les tribus d'Israël, pour dire : Aussitôt que vous entendrez le son du shofar, vous direz : Abshalôm est fait roi à Hébron !
15:11	200 hommes de Yeroushalaim qui avaient été invités s'en allèrent avec Abshalôm. Ils y allèrent en leur intégrité, ne sachant rien.
15:12	Pendant qu'Abshalôm sacrifiait les sacrifices, il envoya chercher à la ville de Guilo, Achitophel, le Guilonite, conseiller de David. Il se forma une puissante conspiration, parce que le peuple était de plus en plus nombreux auprès d'Abshalôm.

### David fuit son fils Abshalôm

15:13	Un messager se rendit auprès de David et lui dit : Le cœur des hommes d'Israël est derrière Abshalôm.
15:14	David dit à tous ses serviteurs qui étaient avec lui à Yeroushalaim : Levez-vous, fuyons, car il n'y aura pas de délivrance pour nous en face d'Abshalôm. Hâtez-vous de partir ! De peur qu’il ne se hâte, et ne nous atteigne, et ne fasse tomber le malheur sur nous, et ne frappe la ville à bouche d’épée.
15:15	Les serviteurs du roi dirent au roi : Selon tout ce que choisira mon seigneur le roi, voici tes serviteurs !
15:16	Le roi sortit avec toute sa maison, à pied, et le roi laissa dix femmes, des concubines, pour garder la maison.
15:17	Le roi sortit avec tout le peuple à pied, et ils s'arrêtèrent à Beth-Merkhak.
15:18	Tous ses serviteurs marchaient à côté de lui. Tous les Kéréthiens, tous les Péléthiens et tous les Gathiens, 600 hommes venus de Gath à pied, marchaient en face du roi.
15:19	Le roi dit à Ittaï de Gath : Pourquoi viendrais-tu aussi avec nous ? Retourne et reste avec le roi, car tu es un étranger, tu es même un exilé de ton lieu.
15:20	Tu es arrivé hier, et te ferais-je aujourd'hui errer çà et là avec nous ? Moi j’irai où j’irai ! Retourne et fais retourner tes frères avec toi. Que la bonté et la vérité soient avec toi !
15:21	Mais Ittaï répondit au roi et dit : YHWH est vivant, et le roi mon seigneur est vivant ! Quel que soit le lieu où le roi mon seigneur sera, soit pour mourir, soit pour vivre, ton serviteur y sera aussi.
15:22	David dit à Ittaï : Va, passe ! Et Ittaï de Gath passa avec tous ses hommes et tous les enfants qui étaient avec lui.
15:23	Toute la terre pleurait à grands cris au passage du peuple. Le roi passa le torrent de Cédron, et tout le peuple passa en face du chemin qui mène au désert.

### L'arche de l'alliance à Yeroushalaim (Jérusalem)

15:24	Tsadok était aussi là, et avec lui, tous les Lévites portant l'arche de l'alliance d'Elohîm. Ils posèrent l'arche d'Elohîm, et Abiathar montait pendant que tout le peuple achevait de sortir de la ville.
15:25	Le roi dit à Tsadok : Rapporte l'arche d'Elohîm dans la ville. Si je trouve grâce aux yeux de YHWH, il me ramènera, et il me fera voir l'arche et sa demeure.
15:26	Mais s'il dit : Je ne prends pas de plaisir en toi ! Me voici, qu'il fasse de moi ce qui sera bon à ses yeux.
15:27	Le roi dit au prêtre Tsadok : Vois-tu ? Retourne en paix à la ville, avec Achimaats, ton fils, et Yehonathan, fils d'Abiathar, vos deux fils.
15:28	Voyez, j'attendrai dans les régions arides du désert, jusqu'à ce qu'on vienne m'apporter des nouvelles de votre part.
15:29	Tsadok et Abiathar ramenèrent l'arche d'Elohîm à Yeroushalaim et ils y restèrent.
15:30	David monta par la montée des oliviers. Il montait en pleurant, la tête couverte et il marchait pieds nus. Tout le peuple aussi qui était avec lui montait, chacun ayant la tête couverte. Et en montant ils pleuraient.
15:31	On informa David en disant : Achitophel est parmi ceux qui ont conspiré avec Abshalôm. Et David dit : S'il te plaît, YHWH, tourne en folie le conseil d'Achitophel !

### Houshaï, espion pour David dans la cour d'Abshalôm

15:32	Il arriva que quand David fut arrivé au sommet, où il se prosterna devant Elohîm, Houshaï, l'Arkien, vint au-devant de lui, la tunique déchirée et de la terre sur sa tête.
15:33	David lui dit : Si tu passes avec moi, tu seras une charge pour moi.
15:34	Mais si tu retournes à la ville et que tu dises à Abshalôm : « Roi, je suis ton serviteur, j’étais autrefois le serviteur de ton père, mais maintenant je serai ton serviteur », tu annuleras pour moi les conseils d'Achitophel.
15:35	Les prêtres Tsadok et Abiathar ne seront-ils pas là avec toi ? Il arrivera que toutes les paroles que tu entendras de la maison du roi, tu les rapporteras aux prêtres Tsadok et Abiathar.
15:36	Voici, ils ont là avec eux leurs deux fils, Achimaats, de Tsadok, et Yehonathan d'Abiathar, c'est par leur main que vous m'enverrez toute parole que vous entendrez.
15:37	Houshaï, l'ami de David, retourna dans la ville, et Abshalôm entra à Yeroushalaim.

## Chapitre 16

### Tsiba retrouve David en fuite

16:1	Quand David eut un peu dépassé le sommet, voici, Tsiba, serviteur de Mephibosheth, vint au-devant de lui avec deux ânes bâtés, sur lesquels il y avait 200 pains, 100 grappes de raisins secs, 100 de fruits d'été, et une outre de vin.
16:2	Le roi dit à Tsiba : Que veux-tu faire de cela ? Et Tsiba dit : Les ânes serviront de montures pour la maison du roi, le pain et les autres fruits d'été sont pour nourrir les jeunes hommes, et le vin pour que boive celui qui sera fatigué dans le désert.
16:3	Le roi lui dit : Mais où est le fils de ton maître ? Et Tsiba dit au roi : Voici, il est resté à Yeroushalaim, car il a dit : Aujourd'hui, la maison d'Israël me rendra le royaume de mon père.
16:4	Le roi dit à Tsiba : Voici, tout ce qui est à Mephibosheth est à toi. Et Tsiba dit : Je me prosterne ! Que je trouve grâce à tes yeux, mon seigneur le roi !

### Shimeï maudit le roi David

16:5	Le roi David était arrivé jusqu'à Bahourim. Et voici, il sortit de là un homme de la famille et de la maison de Shaoul, nommé Shimeï, fils de Guéra. Il sortit, il sortit en maudissant,
16:6	il jeta des pierres contre David, contre tous ses serviteurs, et contre tout le peuple. Tous les hommes vaillants étaient à la droite et à la gauche du roi.
16:7	Shimeï parlait ainsi en le maudissant : Sors, sors, homme de sang, homme de Bélial<!--Voir commentaire en De. 13:14.--> !
16:8	YHWH fait retomber sur toi tout le sang de la maison de Shaoul, à la place duquel tu régnais, et YHWH a mis le royaume entre les mains de ton fils, Abshalôm. Et te voilà dans ton malheur, parce que tu es un homme de sang !
16:9	Abishaï, fils de Tserouyah, dit au roi : Pourquoi ce chien mort maudit-il le roi, mon seigneur ? Laisse-moi passer, s’il te plaît, et lui ôter la tête !
16:10	Mais le roi dit : Qu'ai-je à faire avec vous, fils de Tserouyah ? S'il maudit, c'est que YHWH lui a dit : Maudis David ! Qui lui dira : Pourquoi agis-tu ainsi ?
16:11	Et David dit à Abishaï et à tous ses serviteurs : Voici, mon propre fils, qui est sorti de mes entrailles, cherche mon âme. Combien plus maintenant ce Benyamite ! Laissez-le et qu'il maudisse, car YHWH lui a parlé.
16:12	Peut-être YHWH verra-t-il mon affliction et YHWH me retournera-t-il un bien contre sa malédiction aujourd'hui.
16:13	David et ses hommes continuèrent leur chemin. Et Shimeï marchait sur le flanc de la montagne vis-à-vis de lui, continuant à maudire, jetant des pierres contre lui et de la poussière en l'air.
16:14	Le roi David et tout le peuple qui était avec lui arrivèrent fatigués et là ils reprirent leur souffle.

### Abominations d'Abshalôm à Yeroushalaim (Jérusalem)

16:15	Abshalôm et tout le peuple, les hommes d'Israël, étaient entrés dans Yeroushalaim. Achitophel était avec lui.
16:16	Il arriva que quand Houshaï, l'Arkien, l'ami de David, fut arrivé auprès d'Abshalôm, il lui dit : Vive le roi ! Vive le roi !
16:17	Et Abshalôm dit à Houshaï : Est-ce là l'affection que tu as pour ton ami ? Pourquoi n'es-tu pas allé avec ton ami ?
16:18	Houshaï dit à Abshalôm : Non, mais je serai à celui qui a été choisi par YHWH, par ce peuple et par tous les hommes d'Israël, et je demeurerai avec lui.
16:19	Deuxièmement : Qui servirai-je, moi ? N’est-ce pas en face de son fils ? Comme j’ai servi en face de son père, ainsi serai-je en face de toi.
16:20	Abshalôm dit à Achitophel : Donnez un conseil sur ce que nous ferons.
16:21	Achitophel dit à Abshalôm : Va vers les concubines que ton père a laissées pour garder la maison. Ainsi tout Israël saura que tu t'es rendu odieux envers ton père, et les mains de tous ceux qui sont avec toi se fortifieront.
16:22	On dressa une tente pour Abshalôm sur le toit et Abshalôm alla vers les concubines de son père, aux yeux de tout Israël<!--2 S. 12:11-12.-->.
16:23	Or, le conseil d'Achitophel, ce qu’il conseillait en ces jours-là, c’était comme lorsqu’un homme interrogeait la parole d'Elohîm. Il en était ainsi de tous les conseils d'Achitophel, soit pour David, soit pour Abshalôm.

## Chapitre 17

### Le conseil d'Achitophel dissipé

17:1	Achitophel dit à Abshalôm : S’il te plaît, laisse-moi choisir 12 000 hommes. Je me lèverai et je poursuivrai David cette nuit.
17:2	Je l'atteindrai pendant qu'il est fatigué et que ses mains sont affaiblies. Je le ferai tellement trembler que tout le peuple qui est avec lui s'enfuira. Je frapperai seulement le roi,
17:3	et je ramènerai à toi tout le peuple. L'homme que tu cherches vaut le retour de tous et tout le peuple sera en paix.
17:4	Cette parole fut bonne aux yeux d'Abshalôm et aux yeux de tous les anciens d'Israël.
17:5	Abshalôm dit : S'il vous plaît, appelez aussi Houshaï, l'Arkien, pour que nous entendions aussi ce qui est dans sa bouche.
17:6	Houshaï vint vers Abshalôm et Abshalôm lui dit : Achitophel a parlé selon cette parole. Devons-nous agir selon sa parole ? Sinon, parle toi-même.
17:7	Houshaï dit à Abshalôm : Le conseil qu’a conseillé Achitophel n'est pas bon cette fois-ci.
17:8	Houshaï dit : Tu connais ton père et ses hommes, ce sont des hommes vaillants, et ils ont l'amertume dans l'âme comme une ourse des champs privée de ses petits. Ton père est un homme de guerre, il ne passera pas la nuit avec le peuple.
17:9	Voici, il est maintenant caché dans une fosse ou dans un autre endroit. Et il arrivera que, si quelques-uns tombent dès le commencement, quelqu'un l'apprendra, il l'apprendra et dira : Il y a un massacre parmi le peuple qui suit Abshalôm !
17:10	Même un fils talentueux, celui dont le cœur est comme un cœur de lion se fondra, il se fondra. En effet, tout Israël sait que ton père est un homme vaillant, et que ceux qui sont avec lui sont des fils talentueux.
17:11	Oui, je conseille que tout Israël se rassemble auprès de toi, depuis Dan jusqu'à Beer-Shéba, multitude pareille au sable qui est sur le bord de la mer, et qu'en personne tu marches au combat.
17:12	Nous irons vers lui dans un des lieux où il se trouvera. Nous nous poserons sur lui comme la rosée tombe sur le sol. Il ne lui restera aucun de tous les hommes qui sont avec lui.
17:13	S'il se retire dans une ville, tout Israël portera des cordes vers cette ville-là, et nous la traînerons jusqu'au torrent, jusqu'à ce qu'on n'en trouve plus un caillou.
17:14	Abshalôm et tous les hommes d'Israël dirent : Le conseil de Houshaï, l'Arkien, est meilleur que le conseil d'Achitophel. Car YHWH avait résolu de faire échouer le bon conseil d'Achitophel afin que YHWH fasse venir le malheur sur Abshalôm.

### Houshaï avertit David du danger

17:15	Houshaï dit aux prêtres Tsadok et Abiathar : Achitophel a conseillé ceci et cela à Abshalôm et aux anciens d'Israël, et moi j’ai conseillé ceci et cela.
17:16	Maintenant, envoyez tout de suite informer David, en disant : Ne passe pas la nuit dans les régions arides du désert, mais passe, passe de peur que le roi et tout le peuple qui est avec lui ne soient engloutis.
17:17	Yehonathan et Achimaats se tenaient près de la fontaine de Roguel. Une servante vint leur dire d'aller informer le roi David, car ils n'osaient pas se montrer et entrer dans la ville.
17:18	Mais un garçon les aperçut, et le rapporta à Abshalôm. Et ils partirent tous deux en hâte et ils arrivèrent à Bahourim, à la maison d'un homme qui avait un puits dans sa cour, dans lequel ils descendirent.
17:19	La femme prit une couverture qu'elle étendit sur les faces du puits et y répandit du grain pilé, et la chose ne fut pas connue.
17:20	Les serviteurs d'Abshalôm entrèrent dans la maison auprès de cette femme et lui dirent : Où sont Achimaats et Yehonathan ? La femme leur dit : Ils ont passé le ruisseau. Ils cherchèrent, et ne les trouvant pas, ils retournèrent à Yeroushalaim.
17:21	Il arriva, après leur départ, qu'ils<!--Achimaats et Yehonathan.--> remontèrent du puits et allèrent informer le roi David. Ils lui dirent : Levez-vous, et hâtez-vous de passer l'eau, car Achitophel a conseillé telle chose contre vous.
17:22	David et tout le peuple qui était avec lui se levèrent et ils passèrent le Yarden. À la lumière du matin, il n'en manqua pas un qui n'eût passé le Yarden.
17:23	Or Achitophel voyant qu'on n'avait pas fait ce qu'il avait conseillé, fit seller son âne, se leva, et s'en alla en sa maison, dans sa ville. Après avoir donné des ordres à sa maison, il s'étrangla et mourut. On l'enterra dans le sépulcre de son père.

### Abshalôm et Israël en marche contre David

17:24	David arriva à Mahanaïm. Et Abshalôm passa le Yarden, lui et tous les hommes d'Israël avec lui.
17:25	Abshalôm établit Amasa sur l'armée, à la place de Yoab. Or Amasa était fils d'un homme nommé Yithra, l'Israélite, qui était allé vers Abigaïl, fille de Nachash, et sœur de Tserouyah, mère de Yoab.
17:26	Israël et Abshalôm campèrent en terre de Galaad.

### Mahanaïm bienveillant envers David

17:27	Or il arriva qu'aussitôt que David fut arrivé à Mahanaïm, Shobi, fils de Nachash de Rabba, des fils d'Ammon, Makir, fils d'Ammiel de Lodebar, et Barzillaï, le Galaadite de Roguelim,
17:28	apportèrent des lits, des bassins, des vases de terre, du froment, de l'orge, de la farine, du grain rôti, des fèves, des lentilles, des pois rôtis,
17:29	du miel, de la crème, des brebis, et des fromages de vache. Ils apportèrent ces choses à David et au peuple qui était avec lui, afin qu'ils mangent, car ils disaient : Le peuple est affamé, épuisé et assoiffé dans le désert.

## Chapitre 18

### Bataille dans la forêt d'Éphraïm ; instructions de David sur Abshalôm

18:1	David fit le dénombrement du peuple qui était avec lui, et il établit sur eux des chefs de milliers et des chefs de centaines.
18:2	David envoya le peuple, un tiers aux mains de Yoab, un tiers aux mains d'Abishaï, fils de Tserouyah, frère de Yoab, et un tiers aux mains d'Ittaï, de Gath. Et le roi dit au peuple : Moi aussi, je sortirai, je sortirai avec vous.
18:3	Mais le peuple lui dit : Tu ne sortiras pas ! Car si nous nous enfuyons, si nous nous enfuyons, ils ne fixeraient pas le cœur sur nous. Même quand la moitié d'entre nous y serait tuée, ils ne fixeraient pas le cœur sur nous. Mais toi, tu es comme 10 000 d'entre nous, et maintenant il vaut mieux que de la ville tu puisses venir à notre secours.
18:4	Le roi leur dit : Je ferai ce qui est bon à vos yeux. Le roi se tint à côté de la porte, et tout le peuple sortit par centaines et par milliers.
18:5	Le roi donna cet ordre à Yoab, à Abishaï, et à Ittaï, et dit : Par égard pour moi, doucement avec le jeune Abshalôm ! Et tout le peuple entendit ce que le roi commandait à tous les chefs au sujet d'Abshalôm.
18:6	Le peuple sortit dans les champs à la rencontre d'Israël, et la bataille eut lieu dans la forêt d'Éphraïm.
18:7	Là, le peuple d'Israël fut battu par les serviteurs de David, et il y eut en ce jour-là dans ce même lieu, une grande défaite de 20 000 hommes.
18:8	Et là, la bataille s’était étendue sur les faces de toute la terre. La forêt dévora en ce jour plus de peuple que n’en dévora l’épée.

### Yoab tue Abshalôm

18:9	Abshalôm se retrouva devant les serviteurs de David. Il était monté sur un mulet. Le mulet entra sous les branches entrelacées d'un grand chêne, et la tête d'Abshalôm fut prise dans le chêne. Il demeura suspendu entre les cieux et la terre, et le mulet qui était sous lui passa outre.
18:10	Un homme ayant vu cela, le rapporta à Yoab et lui dit : Voici, j'ai vu Abshalôm suspendu à un chêne.
18:11	Et Yoab dit à l'homme qui lui rapportait cela : Tu l'as vu ! Pourquoi ne l'as-tu pas abattu par terre ? Je t'aurais donné 10 sicles d'argent et une ceinture.
18:12	Mais cet homme dit à Yoab : Quand je pèserais dans mes paumes 1 000 pièces d'argent, je ne mettrais pas ma main sur le fils du roi. Car c’est à nos oreilles que le roi t’a donné ordre, ainsi qu’à Abishaï et à Ittaï, en disant : qui que ce soit, faites attention au jeune homme Abshalôm !
18:13	Et si j’avais agi frauduleusement contre son âme, toute l’affaire ne serait pas restée cachée au roi, et toi, tu te mettrais à l’écart.
18:14	Yoab dit : Je ne m'attarderai pas auprès de toi ! Et il prit dans sa paume trois javelots, et les enfonça dans le cœur d'Abshalôm qui était encore vivant au milieu du chêne.
18:15	Les dix jeunes hommes qui portaient les armes de Yoab entourèrent Abshalôm, le frappèrent et le firent mourir<!--La mort d'Abshalôm fut une conséquence du péché de David avec Bath-Shéba. Le péché a donc des conséquences graves et cause beaucoup de souffrances.-->.
18:16	Yoab fit sonner le shofar et le peuple revint de la poursuite d'Israël, parce que Yoab le retint.
18:17	Ils prirent Abshalôm, le jetèrent dans la forêt dans une grande fosse, et mirent sur lui un très grand monceau de pierres. Tout Israël s'enfuit, chaque homme dans sa tente.
18:18	Or Abshalôm s'était fait ériger, de son vivant, un monument dans la vallée du roi. Il se disait en effet : Je n'ai pas de fils pour conserver la mémoire de mon nom. Et il donna son propre nom au monument, qu'on appelle encore aujourd'hui la main d'Abshalôm.

### David apprend la mort d'Abshalôm

18:19	Achimaats, fils de Tsadok, dit : S’il te plaît, laisse-moi courir, et porter au roi la bonne nouvelle que YHWH lui a fait justice de la main de ses ennemis.
18:20	Yoab lui dit : Tu ne seras pas porteur de bonnes nouvelles aujourd’hui : tu porteras les nouvelles un autre jour, mais ne les porte pas aujourd’hui, puisque le fils du roi est mort.
18:21	Et Yoab dit à un Éthiopien : Va, annonce au roi ce que tu as vu. L'Éthiopien se prosterna devant Yoab, puis il se mit à courir.
18:22	Achimaats, fils de Tsadok, continua de dire encore à Yoab : Quoi qu'il arrive, laisse-moi courir, moi aussi, s’il te plaît, derrière l'Éthiopien. Yoab lui dit : Pourquoi veux-tu courir, mon fils ? Pour toi il ne se trouve pas de bonnes nouvelles ! 
18:23	Quoiqu'il arrive, je courrai ! Il lui dit : Cours ! Achimaats courut par le chemin de la plaine, et il dépassa l'Éthiopien.
18:24	David était assis entre les deux portes. La sentinelle alla sur le toit de la porte vers la muraille. Elle leva les yeux et elle regarda. Et voici un homme qui courait tout seul.
18:25	La sentinelle cria et avertit le roi. Le roi dit : S'il est seul, c'est qu'il y a des nouvelles dans sa bouche. Et cet homme marchait, marchait et se rapprochait.
18:26	La sentinelle vit un autre homme qui courait. Elle cria au portier : Voici un homme courant seul. Et le roi dit : Celui-ci est aussi un porteur de nouvelles.
18:27	La sentinelle dit : La course du premier me paraît comme la course d'Achimaats, fils de Tsadok. Et le roi dit : C'est un homme bon, il vient avec une bonne nouvelle.
18:28	Achimaats cria et dit au roi : Shalôm ! Et il se prosterna devant le roi, le visage contre terre et dit : Béni soit YHWH, ton Elohîm, qui a livré les hommes qui levaient leurs mains contre le roi, mon seigneur !
18:29	Le roi dit : Y a-t-il paix pour le jeune homme Abshalôm ? Achimaats lui dit : J’ai vu un grand tumulte lorsque Yoab envoyait le serviteur du roi et ton serviteur, mais je ne sais pas pourquoi.
18:30	Le roi lui dit : Tourne-toi et tiens-toi là. Il se tourna et se tint là.
18:31	Et voici arriva l'Éthiopien, et l'Éthiopien dit : Que le roi mon seigneur reçoive une bonne nouvelle, car YHWH t’a aujourd’hui fait justice de la main de tous ceux qui s’étaient levés contre toi.
18:32	Le roi dit à l'Éthiopien : Y a-t-il paix pour le jeune homme Abshalôm ? Et l'Éthiopien lui dit : Que les ennemis du roi, mon seigneur, et tous ceux qui s'élèvent contre toi pour te faire du mal soient comme ce jeune homme !

## Chapitre 19

### Souffrance de David ; indignation de Yoab

19:1	Le roi agité et, montant à la chambre haute au-dessus de la porte, il se mit à pleurer. Il disait ainsi en marchant : Mon fils Abshalôm ! Mon fils, mon fils Abshalôm ! Pourquoi ne suis-je pas mort à ta place ? Abshalôm, mon fils, mon fils !
19:2	Et on fit ce rapport à Yoab : Voici, le roi pleure et se lamente sur Abshalôm.
19:3	La délivrance devint en ce jour un deuil pour tout le peuple, car le peuple avait entendu dire en ce jour : Le roi est affligé à cause de son fils.
19:4	Ce jour-là le peuple entra dans la ville à la dérobée, comme se déroberait un peuple déshonoré pour avoir fui dans la bataille.
19:5	Le roi s'était couvert le visage, et le roi criait à grande voix : Mon fils Abshalôm ! Abshalôm, mon fils, mon fils !
19:6	Yoab entra dans la chambre où était le roi, et lui dit : Tu couvres aujourd'hui de confusion les faces de tous tes serviteurs, qui ont en ce jour sauvé ton âme, l'âme de tes fils et de tes filles, l'âme de tes femmes et l'âme de tes concubines.
19:7	Tu aimes ceux qui te haïssent et tu hais ceux qui t'aiment. Oui, tu as déclaré aujourd’hui que chefs et serviteurs ne sont rien pour toi. Oui, je sais aujourd’hui que, si Abshalôm était vivant et nous tous morts, aujourd’hui, oui, ce serait juste à tes yeux.
19:8	Maintenant lève-toi, sors, et parle au cœur de tes serviteurs ! Oui, je jure par YHWH que si tu ne sors pas, il ne restera pas un seul homme avec toi cette nuit. Et ce mal sera pire que tous ceux qui te sont arrivés depuis ta jeunesse jusqu'à présent.

### Retour du roi David à Yeroushalaim (Jérusalem)

19:9	Le roi se leva et s'assit à la porte. On en informa tout le peuple en disant : Voici que le roi est assis à la porte. Et tout le peuple vint devant le roi. Israël s'était enfui, chaque homme dans sa tente.
19:10	Et dans toutes les tribus d'Israël, tout le peuple était en contestation, disant : Le roi nous a délivrés de la paume de nos ennemis, c'est lui qui nous a sauvés de la paume des Philistins, et maintenant il a dû fuir de la terre loin d'Abshalôm.
19:11	Or Abshalôm, que nous avions oint sur nous, est mort dans la bataille. Maintenant, pourquoi ne parlez-vous pas de faire revenir le roi ?
19:12	Le roi David envoya dire aux prêtres Tsadok et Abiathar : Parlez aux anciens de Yéhouda et dites-leur : Pourquoi deviendriez-vous les derniers à ramener le roi dans sa maison, alors que la parole de tout Israël est venue vers le roi dans sa maison ?
19:13	Vous êtes mes frères, vous êtes mes os et ma chair. Pourquoi deviendriez-vous les derniers à ramener le roi ?
19:14	Dites même à Amasa : N'es-tu pas mon os et ma chair ? Qu'ainsi me traite Elohîm et qu'ainsi il y ajoute si tu ne deviens pas devant moi pour toujours chef de l'armée à la place de Yoab !
19:15	Il inclina le cœur de tous les hommes de Yéhouda comme celui d’un seul homme, et ils envoyèrent dire au roi : Reviens, toi, et tous tes serviteurs.
19:16	Le roi revint et arriva jusqu'au Yarden. Yéhouda se rendit jusqu'à Guilgal, pour aller à la rencontre du roi afin de lui faire repasser le Yarden.
19:17	Shimeï, fils de Guéra, Benyamite, qui était de Bahourim, se hâta de descendre avec les hommes de Yéhouda à la rencontre du roi David.
19:18	Il avait avec lui 1 000 hommes de Benyamin, et Tsiba, serviteur de la maison de Shaoul, ses 15 enfants, et ses 20 serviteurs étaient aussi avec lui. Ils passèrent le Yarden en présence du roi.
19:19	Et ils traversèrent à gué pour faire passer la maison du roi et pour faire ce qui était bon à ses yeux. Au moment où le roi allait passer le Yarden, Shimeï, fils de Guéra, se prosterna devant lui.
19:20	Il dit au roi : Que mon seigneur ne tienne pas compte de mon iniquité. Qu'il ne se souvienne pas du mal que ton serviteur a fait le jour où le roi mon seigneur sortait de Yeroushalaim, et que le roi ne le prenne pas à cœur !
19:21	Oui, ton serviteur le sait, oui, moi, j’ai péché. Et voici, je suis venu aujourd'hui le premier de toute la maison de Yossef pour descendre à la rencontre du roi, mon seigneur.
19:22	Mais Abishaï, fils de Tserouyah, répondit et dit : À cause de cela, ne fera-t-on pas mourir Shimeï, puisqu'il a maudit le mashiah de YHWH ?
19:23	Et David dit : Qu'ai-je à faire avec vous, fils de Tserouyah, pour qu’aujourd’hui vous deveniez pour moi un satan ? Ferait-on mourir aujourd'hui quelqu'un en Israël ? Ne sais-je pas que je règne aujourd'hui sur Israël ?
19:24	Le roi dit à Shimeï : Tu ne mourras pas ! Et le roi le lui jura.
19:25	Mephibosheth, fils de Shaoul, descendit aussi à la rencontre du roi. Il n'avait pas lavé ses pieds, ni fait sa barbe, ni lavé ses vêtements, depuis que le roi s'en était allé, jusqu'au jour où il revenait en paix.
19:26	Il arriva, lorsqu’il vint à Yeroushalaim à la rencontre du roi, que le roi lui dit : Pourquoi n'es-tu pas venu avec moi, Mephibosheth ?
19:27	Il dit : Roi, mon seigneur, mon serviteur m'a trompé, car ton serviteur qui est boiteux avait dit : Je ferai seller mon âne, je monterai dessus et j'irai avec le roi.
19:28	Et il a calomnié ton serviteur auprès du roi, mon seigneur. Mais le roi mon seigneur est comme un ange d'Elohîm. Fais ce qui semblera bon à tes yeux.
19:29	En effet, toute la maison de mon père n’était que des hommes morts devant le roi, mon seigneur. Tu as en effet mis ton serviteur parmi ceux qui mangent à ta table. Quel droit puis-je encore avoir, pour me plaindre encore au roi ?
19:30	Le roi lui dit : Pourquoi toutes ces paroles ? Je l'ai dit : Toi et Tsiba, vous partagerez les terres.
19:31	Mephibosheth dit au roi : Qu'il prenne même tout, puisque le roi mon seigneur rentre en paix dans sa maison.
19:32	Barzillaï, le Galaadite, descendit de Roguelim, et passa le Yarden avec le roi, pour l'accompagner jusqu'au-delà du Yarden.
19:33	Barzillaï était très vieux, fils de 80 ans. Il avait nourri le roi pendant qu'il avait séjourné à Mahanaïm, car c'était un homme fort riche.
19:34	Le roi dit à Barzillaï : Viens avec moi, je te nourrirai chez moi à Yeroushalaim.
19:35	Barzillaï dit au roi : Combien d'années vivrai-je pour que je monte avec le roi à Yeroushalaim ?
19:36	Je suis aujourd'hui fils de 80 ans. Distinguerai-je ce qui est bon de ce qui est mauvais ? Ton serviteur goûterait-il ce qu’il mangerait ou boirait ? Ou entendrais-je encore la voix des chanteurs et des chanteuses ? Et pourquoi ton serviteur serait-il encore à charge à mon seigneur, le roi ?
19:37	À peine ton serviteur passe-t-il le Yarden avec le roi, et pourquoi le roi me rendrait-il une telle récompense ?
19:38	S'il te plaît, que ton serviteur s'en retourne ! Que je meure dans ma ville, près du sépulcre de mon père et de ma mère ! Mais voici ton serviteur Kimham : il passera avec le roi mon seigneur. Fais pour lui ce qui sera bon à tes yeux.
19:39	Le roi dit : Que Kimham passe avec moi, et je ferai pour lui ce qui sera bon à tes yeux. Tout ce que tu voudras de moi, je te l'accorderai.
19:40	Tout le peuple passa le Yarden avec le roi. Le roi embrassa Barzillaï et le bénit. Celui-ci s’en retourna dans sa demeure.

### Yéhouda et Israël se disputent le roi

19:41	Le roi passa à Guilgal et Kimham passa avec lui. Ainsi, tout le peuple de Yéhouda et même la moitié du peuple d'Israël firent passer le roi.
19:42	Et voici que tous les hommes d'Israël vinrent vers le roi et lui dirent : Pourquoi nos frères, les hommes de Yéhouda, t'ont-ils enlevé, et ont-ils fait passer le Yarden au roi et à sa maison, et à tous les hommes de David ?
19:43	Tous les hommes de Yéhouda répondirent aux hommes d'Israël : Parce que nous sommes plus proches du roi. Pourquoi vous fâchez-vous à propos de cette chose ? Avons-nous mangé, mangé de ce qui est au roi ? Nous a-t-il fait des présents ?
19:44	Les hommes d'Israël répondirent aux hommes de Yéhouda et dirent : J’ai dix mains sur le roi et sur David aussi, moi, plus que toi ! Pourquoi m’as-tu maudit ? Ma parole n'a-t-elle pas été la première pour ramener mon roi ? Mais la parole des hommes de Yéhouda fut plus dure que la parole des hommes d'Israël.

## Chapitre 20

### Yéhouda reste fidèle au roi David

20:1	Il se trouvait là un homme de Bélial<!--Voir commentaire en De. 13:14.--> du nom de Shéba, fils de Bicri, Benyamite. Il sonna du shofar et dit : Nous n'avons pas de part avec David ni d'héritage avec le fils d'Isaï ! Israël, chaque homme à ses tentes !
20:2	Tous les hommes d'Israël s’éloignèrent de la suite de David pour suivre Shéba, fils de Bicri. Mais les hommes de Yéhouda s'attachèrent à leur roi, depuis le Yarden jusqu'à Yeroushalaim.
20:3	David vint dans sa maison à Yeroushalaim. Le roi prit les dix femmes, les concubines qu'il avait laissées pour garder sa maison, et les mit dans une maison gardée où il les nourrissait, mais il ne vint plus vers elles, et elles furent enfermées jusqu'au jour de leur mort, vivant dans le veuvage.

### Bataille contre Shéba ; Yoab tue Amasa

20:4	Le roi dit à Amasa : Rassemble-moi dans 3 jours les hommes de Yéhouda et toi, sois présent ici.
20:5	Amasa s'en alla pour rassembler Yéhouda, mais il tarda au-delà du temps que le roi lui avait fixé.
20:6	David dit à Abishaï : Maintenant Shéba, fils de Bicri, nous fera plus de mal qu'Abshalôm. Prends toi-même les serviteurs de ton maître et poursuis-le, de peur qu'il ne trouve pour lui des villes fortifiées et n'échappe à nos yeux.
20:7	Les hommes de Yoab sortirent derrière lui, avec les Kéréthiens, les Péléthiens et tous les hommes vaillants. Ils sortirent de Yeroushalaim pour poursuivre Shéba, fils de Bicri.
20:8	Et comme ils furent près de la grande pierre qui est à Gabaon, Amasa vint au-devant d'eux. Yoab était ceint d'une épée par-dessus les habits dont il était revêtu. Elle était attachée à ses reins dans le fourreau, et comme il s'avançait, elle tomba.
20:9	Yoab dit à Amasa : Es-tu en paix, mon frère ? Puis Yoab prit de sa main droite la barbe d'Amasa pour l'embrasser.
20:10	Amasa ne prenait pas garde à l'épée qui était dans la main de Yoab, et Yoab l'en frappa au ventre et répandit ses entrailles à terre, sans le frapper une seconde fois. Et il mourut. Après cela, Yoab et Abishaï, son frère, poursuivirent Shéba, fils de Bicri.
20:11	Un homme d'entre les serviteurs de Yoab se tint près d'Amasa et dit : Qui prend plaisir<!--Voir 1 S. 19:1.--> en Yoab et qui est pour David ? Qu'il suive Yoab !
20:12	Amasa se roulait dans son sang au milieu de la route. Voyant que tout le peuple s'arrêtait, cet homme le poussa hors de la route dans un champ et jeta sur lui un vêtement. C'est ce qu'il fit quand il vit que tous ceux qui arrivaient près de lui s'arrêtaient.
20:13	Quand il fut ôté de la route, tous les hommes qui suivaient Yoab passaient au-delà, afin de poursuivre Shéba, fils de Bicri.

### La mort de Shéba

20:14	Yoab passa par toutes les tribus d'Israël jusqu'à Abel-Beth-Ma'akah, avec tous les Bériens, qui s'étaient rassemblés et qui l'avaient suivi.
20:15	Ils vinrent assiéger Shéba dans Abel-Beth-Ma'akah et ils élevèrent contre la ville un tertre qui atteignait le rempart. Tout le peuple qui était avec Yoab détruisait la muraille pour la faire tomber.
20:16	Une femme sage cria depuis la ville : Écoutez, écoutez ! Dites, s'il vous plaît, à Yoab : Approche jusqu'ici, je te parlerai !
20:17	Il s'approcha d'elle, et la femme dit : Es-tu Yoab ? Il dit : C’est moi. Elle lui dit : Écoute les paroles de ta servante. Il dit : J'écoute.
20:18	Elle parla et dit : On parlait, on parlait jadis en disant : Questionnez, questionnez Abel, et ainsi on en finissait.
20:19	Moi, je suis paisible et fidèle en Israël, et tu cherches à détruire une ville qui est une mère en Israël ! Pourquoi détruirais-tu l'héritage de YHWH ?
20:20	Yoab répondit et dit : Loin de moi ! Loin de moi de vouloir engloutir et détruire !
20:21	La chose n'est pas ainsi. Mais un homme de la Montagne d'Éphraïm, nommé Shéba, fils de Bicri, a levé sa main contre le roi David. Livrez-le, lui seul et je m'éloignerai de la ville. La femme dit à Yoab : Voici, sa tête te sera jetée par-dessus la muraille.
20:22	La femme alla vers tout le peuple, avec sa sagesse, et ils coupèrent la tête de Shéba, fils de Bicri, et la jetèrent à Yoab. Celui-ci sonna du shofar, et l’on se dispersa loin de la ville, chaque homme vers ses tentes. Yoab retourna vers le roi à Yeroushalaim.
20:23	Yoab était préposé sur toute l'armée d'Israël, et Benayah, fils de Yehoyada, sur les Kéréthiens et les Péléthiens.
20:24	Adoram, sur la corvée. Yehoshaphat, fils d'Ahiloud, était archiviste.
20:25	Sheva était scribe ; Tsadok et Abiathar étaient les prêtres,
20:26	et Ira de Yaïr aussi était prêtre de David.

## Chapitre 21

### Vengeance des Gabaonites sur la maison de Shaoul (Saül)

21:1	Il y eut du temps de David une famine de trois années, année après année. David chercha les faces de YHWH, et YHWH lui dit : C'est à cause de Shaoul, de cette maison des sangs, parce qu'il a fait mourir les Gabaonites.
21:2	Le roi appela les Gabaonites pour leur parler. Or les Gabaonites n'étaient pas des fils d'Israël, mais un reste des Amoréens. Les fils d'Israël leur avaient prêté serment<!--Jos. 9.-->, mais Shaoul dans son zèle pour les fils d'Israël et de Yéhouda, avait cherché à les faire mourir.
21:3	Et David dit aux Gabaonites : Que ferais-je pour vous et avec quoi ferai-je la propitiation afin que vous bénissiez l'héritage de YHWH ?
21:4	Les Gabaonites lui dirent : Il ne s'agit pas pour nous d'argent ou d'or avec Shaoul et avec sa maison, et ce n'est pas à nous de faire mourir un homme en Israël. Il dit : Ce que vous direz, je le ferai pour vous.
21:5	Ils dirent au roi : Puisque cet homme nous a consumés, et qu'il avait résolu de nous exterminer pour nous faire disparaître de tout le territoire d'Israël,
21:6	qu'on nous livre sept hommes d'entre ses fils, et nous les pendrons devant YHWH à Guibea de Shaoul, l'élu de YHWH. Et le roi dit : Je vous les livrerai.
21:7	Le roi épargna Mephibosheth, fils de Yehonathan, fils de Shaoul, à cause du serment<!--1 S. 18:8.--> que David et Yehonathan, fils de Shaoul, avaient fait entre eux, devant YHWH.
21:8	Mais le roi prit les deux fils que Ritspah, fille d'Ayah, avait enfantés à Shaoul, Armoni et Mephibosheth, et les cinq fils que Mérab, fille de Shaoul, avait enfantés à Adriel de Meholah, fils de Barzillaï,
21:9	et il les livra entre les mains des Gabaonites, qui les pendirent sur la montagne, devant YHWH. Tous les sept furent tués ensemble. On les fit mourir dans les premiers jours de la moisson, au commencement de la moisson des orges.
21:10	Ritspah, fille d'Ayah, prit un sac et l'étendit sous elle au-dessus d'un rocher, depuis le commencement de la moisson jusqu'à ce que l'eau des cieux tombe sur eux. Elle ne permit pas aux créatures volantes des cieux de s'approcher d'eux pendant le jour, ni aux bêtes des champs pendant la nuit.
21:11	On informa David de ce qu'avait fait Ritspah, fille d'Ayah, concubine de Shaoul.
21:12	Et David alla prendre les os de Shaoul et les os de Yehonathan, son fils, chez les habitants de Yabesh en Galaad, qui les avaient enlevés de la place de Beth-Shan, où les Philistins les avaient pendus lorsqu'ils tuèrent Shaoul à Guilboa.
21:13	Il fit monter de là les os de Shaoul et les os de Yehonathan, son fils. On recueillit aussi les os de ceux qui avaient été pendus.
21:14	On les enterra avec les os de Shaoul et de Yehonathan, son fils, en terre de Benyamin, à Tséla, dans le sépulcre de Kis, père de Shaoul. Et l'on fit tout ce que le roi avait ordonné. Après cela, Elohîm intercéda<!--« Prier », « implorer », « supplier ». Voir 2 S. 24:25 ; Hé. 7:25.--> pour la terre.

### Nouvelles batailles contre les Philistins

21:15	Il y eut encore une guerre entre les Philistins et Israël. David descendit avec ses serviteurs. Ils combattirent tellement contre les Philistins que David fut épuisé.
21:16	Et Yishbo-Benob, qui était né de géant, parlait de tuer David. Il avait une lance dont le fer pesait 300 sicles de cuivre, et il était ceint d'une armure neuve.
21:17	Mais Abishaï, fils de Tserouyah, vint au secours de David, frappa le Philistin, et le tua. Alors les hommes de David jurèrent, en disant : Tu ne sortiras plus avec nous à la bataille, de peur que tu n'éteignes la lampe d'Israël.
21:18	Il arriva, après cela, qu’il y eut encore une guerre à Gob avec les Philistins. Sibbecaï, le Houshatite, tua Saph, qui était né de géant.
21:19	Il y eut encore une guerre à Gob avec les Philistins. Et Elchanan, fils de Ya`arey Oregiym, de Bethléhem, tua Goliath de Gath, qui avait une lance dont le bois était comme une ensouple de tisserand.
21:20	Il y eut encore une guerre à Gath. Là était un homme de haute taille. Les doigts de ses mains et les doigts de ses pieds, six par six, étaient au nombre de 24. Lui aussi était né de géants.
21:21	Il jeta un défi à Israël et Yehonathan, fils de Shim`a, frère de David, le tua.
21:22	Ces quatre-là étaient nés de géants, à Gath. Ils moururent par les mains de David et par les mains de ses serviteurs.

## Chapitre 22

### Louange à YHWH, l'Elohîm qui délivre

22:1	David déclara à YHWH les paroles de ce cantique, le jour où YHWH l'eut délivré de la paume de tous ses ennemis et de la paume de Shaoul.
22:2	Il dit : YHWH est mon Rocher, ma forteresse, mon Sauveur !
22:3	Elohîm est mon Rocher où je me réfugie, mon bouclier et la corne de mon salut<!--Voir Ps. 18:3 ; Lu. 1:69.-->, ma haute retraite et mon refuge. Mon Sauveur ! Tu me sauveras de la violence.
22:4	Je m'écrie : Loué soit YHWH ! Et je suis sauvé de mes ennemis<!--Ps. 18:4.-->.
22:5	Car les flots de la mort m'avaient environné, les torrents de Bélial<!--Voir De. 13:14.--> m'avaient terrifié.
22:6	Les cordes du shéol m'avaient entouré, les filets de la mort m'avaient confronté.
22:7	Dans ma détresse, j'ai invoqué YHWH, j'ai crié à mon Elohîm. De son temple, il a entendu ma voix, mon appel au secours de ses oreilles.
22:8	La Terre fut ébranlée et trembla, les fondements des cieux s'agitèrent, et ils furent ébranlés, parce qu'il était irrité.
22:9	Une fumée montait de ses narines, et de sa bouche sortait un feu dévorant : il en jaillissait des charbons embrasés.
22:10	Il abaissa les cieux, et descendit : il y avait des ténèbres épaisses sous ses pieds.
22:11	Il était monté sur un chérubin, et il volait, il paraissait sur les ailes du vent.
22:12	Il faisait des ténèbres une cabane autour de lui - de masses d'eaux et de sombres nuages.
22:13	Des charbons ardents étaient allumés par l'éclat qui le précédait.
22:14	YHWH tonna des cieux, Élyon fit retentir sa voix.
22:15	Il envoya des flèches pour les disperser, des éclairs, pour les mettre en déroute.
22:16	Le canal de la mer apparut, les fondements du monde furent mis à découvert, par la menace de YHWH, par le souffle du vent de sa colère.
22:17	Il étendit sa main d'en haut, il me saisit, il m'arracha des grandes eaux ;
22:18	il me délivra de mon ennemi puissant, de ceux qui me haïssaient, car ils étaient plus forts que moi.
22:19	Ils m'avaient surpris au jour de ma détresse, mais YHWH devint mon appui.
22:20	Il m'a mis au large, il m'a sauvé, parce qu'il a pris son plaisir en moi.
22:21	YHWH m'a traité selon ma droiture, il m'a rendu selon la pureté de mes mains ;
22:22	parce que j'ai gardé les voies de YHWH, et que je ne me suis pas détourné de mon Elohîm.
22:23	Toutes ses ordonnances ont été devant moi, et je ne me suis pas écarté de ses statuts.
22:24	J'ai été intègre envers lui, et je me suis gardé de mon iniquité.
22:25	YHWH m'a rendu selon ma droiture, selon ma pureté devant ses yeux.
22:26	Avec le fidèle, tu es bon, avec l'homme vaillant qui est intègre, tu agis avec intégrité,
22:27	avec celui qui est pur, tu es pur, avec le pervers tu es tordu.
22:28	Tu sauves le peuple qui s'humilie, et de tes yeux, tu abaisses les orgueilleux.
22:29	Oui, toi, ma lampe, YHWH ! YHWH fait briller mes ténèbres.
22:30	Car avec toi je cours vers une troupe, avec mon Elohîm je saute la muraille.
22:31	La voie de El est parfaite, la parole de YHWH est éprouvée ; il est le bouclier de tous ceux qui se confient en lui.
22:32	Car qui est El, si ce n'est YHWH ? Qui est le rocher<!--2 S. 23:3 ; Es. 44:8 ; 1 Co. 10:4. Voir commentaire Es. 8:13-17.-->, si ce n'est notre Elohîm ?
22:33	C'est El qui est ma puissante forteresse et qui me conduit dans la voie droite.
22:34	Il a rendu mes pieds semblables à ceux des biches, et il me fait tenir debout sur mes lieux élevés.
22:35	Il exerce mes mains au combat, et mes bras tendent l'arc de cuivre.
22:36	Tu me donnes le bouclier de ton salut, et ton humilité me fait devenir grand.
22:37	Tu élargis le chemin sous mes pas, et mes pieds ne chancellent pas.
22:38	Je poursuis mes ennemis et je les détruis, je ne reviens qu'après les avoir exterminés.
22:39	Je les anéantis, je les transperce, ils ne se relèvent plus, et ils tombent sous mes pieds.
22:40	Tu me ceins de force pour le combat, tu fais plier sous moi mes adversaires.
22:41	Tu fais tourner le dos à mes ennemis devant moi et j'extermine ceux qui me haïssent.
22:42	Ils regardent - et pas de sauveur - vers YHWH, mais il ne leur répond pas !
22:43	Je les broie comme la poussière de la terre, je les écrase, je les foule, comme la boue des rues.
22:44	Tu me délivres des dissensions de mon peuple. Tu me gardes pour être chef des nations. Un peuple que je ne connaissais pas m'est asservi.
22:45	Les fils de l'étranger me flattent, dès qu'ils ont entendu parler de moi, ils m'ont obéi.
22:46	Les fils de l'étranger défaillent et sortent tremblants de leurs forteresses.
22:47	Vivant est YHWH, et béni soit mon rocher ! Qu'Elohîm, le rocher de mon salut, soit exalté,
22:48	le El qui me donne vengeance, qui fait descendre des peuples sous moi,
22:49	et qui me fait échapper à mes ennemis ! Tu m'élèves au-dessus de mes adversaires, tu me délivres de l'homme violent.
22:50	C'est pourquoi, YHWH, je te louerai parmi les nations, je chanterai des louanges en ton nom.
22:51	C'est lui qui est la Tour du salut de son roi, et qui fait miséricorde à son mashiah, à David, et à sa postérité, à jamais.

## Chapitre 23

### Paroles prophétiques de David

23:1	Voici les dernières paroles de David. Déclaration de David, fils d'Isaï, déclaration de l'homme fort qui a été élevé, du mashiah de l'Elohîm de Yaacov, du chant agréable d'Israël :
23:2	L'Esprit de YHWH parle par moi, et sa parole est sur ma langue.
23:3	L'Elohîm d'Israël a parlé, le Rocher<!--2 S. 22:32. Voir commentaire Es. 8:13-17.--> d'Israël m'a dit : Le dominateur<!--Il est question ici du Mashiah voir Mi. 5:1 et 2 Pi. 1:19.--> des humains, le Juste, le dominateur ayant la crainte d'Elohîm,
23:4	est la lumière du matin, le soleil qui brille au matin sans nuages, l’éclat de la pluie, la végétation de la terre.
23:5	Oui, n’est-elle pas ainsi, ma maison avec El ? Oui, il a fait avec moi une alliance éternelle, arrangée en tout et gardée. Oui, tout mon salut et tout mon plaisir, oui, ne les fera-t-il pas germer ?
23:6	Mais les Bélials<!--Voir De. 13:14.--> sont tous comme des épines que l'on jette au loin parce qu'on ne les prend pas avec la main.
23:7	L’homme qui les touchera se remplira la main de fer, de bois de lance, et au feu, il les brûlera, il les brûlera sur le lieu même.

### Les vaillants hommes de David<!--1 Ch. 11:10-47.-->

23:8	Voici les noms des vaillants hommes qui étaient au service de David. Yosheb-Basshébeth, le Tachkemonite, était l'un des principaux chefs. C'était Hadino le Hetsnite, qui eut le dessus sur 800 hommes qu'il tua en une seule fois.
23:9	Après lui, Èl’azar, fils de Dodo, fils d'Achochi. Il était parmi les trois vaillants hommes de David, lorsqu'ils défièrent les Philistins rassemblés pour combattre, tandis que les hommes d'Israël se retiraient.
23:10	Il se leva et frappa les Philistins jusqu’à ce que sa main fut lasse et que sa main resta collée à l’épée ! Ce jour-là, YHWH opéra une grande délivrance. Le peuple revint après Èl’azar, seulement pour prendre les dépouilles.
23:11	Après lui, Shammah, fils d'Agué d'Harar. Les Philistins s'étaient rassemblés en troupe. Il y avait là une parcelle de champ pleine de lentilles et le peuple fuyait devant les Philistins.
23:12	Shammah se mit au milieu de cette parcelle, la défendit, et frappa les Philistins. Et YHWH opéra une grande délivrance.
23:13	Trois des 30 chefs descendirent au temps de la moisson et vinrent vers David, dans la caverne d'Adoullam, lorsqu'une troupe de Philistins était campée dans la vallée des géants.
23:14	David était alors dans la forteresse, et la garnison des Philistins était alors en ce temps-là à Bethléhem.
23:15	David convoita et dit : Qui me fera boire de l’eau de la citerne qui est à la porte de Bethléhem ?
23:16	Ces trois vaillants hommes passèrent au travers du camp des Philistins et puisèrent de l'eau de la citerne qui est à la porte de Bethléhem. Ils la prirent et l’apportèrent à David. Il ne voulut pas la boire, mais il la versa pour YHWH.
23:17	Il dit : Loin de moi, YHWH, de faire une telle chose ! N'est-ce pas le sang de ces hommes qui sont allés au péril de leur âme ? Il ne voulut pas la boire. Voilà ce que firent ces trois vaillants hommes.
23:18	Il y avait aussi Abishaï, frère de Yoab, fils de Tserouyah, qui était la tête des trois. Il brandit sa lance sur 300 hommes, les blessa à mort et il se fit un nom parmi les trois.
23:19	Des trois, il était le plus glorieux. Il devint leur chef, mais il n'atteignit pas les trois.
23:20	Benayah, fils de Yehoyada, fils d'un vaillant homme, grand par ses actions, de Kabtseel. C'est lui qui frappa les deux héros de Moab. Il descendit au milieu d'une fosse, où il frappa un lion, un jour de neige.
23:21	C'est lui qui tua un Égyptien, un homme d'un aspect redoutable. Cet Égyptien avait en main une lance. Il descendit vers lui, armé d'un bâton, arracha la lance de la main de l'Égyptien et le tua avec sa propre lance.
23:22	Voilà ce que fit Benayah, fils de Yehoyada. Il se fit un nom parmi les trois vaillants hommes.
23:23	Il était le plus glorieux des trente, mais il n'atteignit pas les trois. David l'établit sur sa garde du corps.
23:24	Asaël, frère de Yoab, était des trente. Elchanan, fils de Dodo, de Bethléhem.
23:25	Shammah, de Harod. Éliyqa, de Harod.
23:26	Hélets, de Péleth. Ira, fils d'Ikkesh, de Tekoa.
23:27	Abiézer, d'Anathoth. Mebounnaï, de Houshah.
23:28	Tsalmon, d'Achoach. Maharaï, de Netophah.
23:29	Héleb, fils de Ba`anah, de Netophah. Ittaï, fils de Ribaï, de Guibea des fils de Benyamin.
23:30	Benayah, de Pirathon. Hiddaï, de Nachalé-Ga`ash.
23:31	Abi-Albon, d'Araba. Azmaveth, de Barhoum.
23:32	Élyachba, de Shaalbon. Ben-Yashen. Yehonathan.
23:33	Shammah, d'Harar. Achiam, fils de Sharar, d'Arar.
23:34	Éliyphelet, fils d'Achasbaï, fils d'un Maakathien. Éliy`am, fils d'Achitophel, de Guilo.
23:35	Hetsro, de Carmel. Paaraï, d'Arab.
23:36	Yigal, fils de Nathan, de Tsoba. Bani, de Gad.
23:37	Tsélek, l'Ammonite. Naharaï, de Beéroth, le porteur des armes de guerre de Yoab, fils de Tserouyah.
23:38	Ira, de Yithriy. Gareb, de Yithriy.
23:39	Ouriyah, le Héthien. En tout, 37.

## Chapitre 24

### Péché de David ; plaie mortelle sur Israël<!--1 Ch. 21:1-17.-->

24:1	Les narines de YHWH s'enflammèrent encore contre Israël et il incita David contre eux, en disant : Va, fais le dénombrement d'Israël et de Yéhouda<!--1 Ch. 21.--> !
24:2	Le roi dit à Yoab, chef de l'armée qui se trouvait près de lui : Parcours, s’il te plaît, toutes les tribus d'Israël, depuis Dan jusqu'à Beer-Shéba et dénombre le peuple, afin que je sache le nombre du peuple.
24:3	Yoab dit au roi : YHWH, ton Elohîm, augmentera ton peuple autant, et 100 fois autant. Les yeux du roi mon seigneur le voient. Mais pourquoi le roi mon seigneur prend-il plaisir à cette parole ?
24:4	La parole du roi prévalut contre Yoab et contre les chefs de l'armée. Yoab et les chefs de l'armée sortirent de la présence du roi pour dénombrer le peuple d'Israël.
24:5	Ils passèrent le Yarden et ils campèrent à Aroër, au sud de la ville qui est au milieu de la vallée du torrent de Gad, et vers Ya`azeyr.
24:6	Ils allèrent en Galaad et dans le territoire de ceux qui habitent vers le bas de la terre de Thachthim-Hodshi. Ils allèrent à Dan Ya`an, et aux environs de Sidon.
24:7	Ils vinrent jusqu'à la forteresse de Tyr, et dans toutes les villes des Héviens et des Kena'ânéens. Ils sortirent vers le midi de Yéhouda à Beer-Shéba.
24:8	Ils parcoururent toute la terre et arrivèrent à Yeroushalaim au bout de 9 mois et 20 jours.
24:9	Et Yoab donna au roi le nombre du recensement du peuple : il y avait en Israël 800 000 hommes talentueux tirant l'épée, et les hommes de Yéhouda étaient 500 000 hommes.
24:10	Le cœur de David lui battit après qu'il eut dénombré le peuple. Et David dit à YHWH : J'ai commis un grand péché en faisant cette chose ! Et maintenant, fais passer, s'il te plaît, YHWH, l'iniquité de ton serviteur, car j'ai agi très follement !
24:11	Et le matin, quand David se leva, la parole de YHWH apparut à Gad le prophète, le voyant de David, en disant :
24:12	Va dire à David : Ainsi parle YHWH : J'apporte trois choses contre toi. Choisis l'une d'elles et je l'exécuterai contre toi.
24:13	Gad alla vers David et lui rapporta cela en disant : Que veux-tu qu'il t'arrive : 7 ans de famine sur ta terre, ou que durant 3 mois tu fuies devant tes ennemis qui te poursuivront, ou que durant 3 jours la peste soit sur ta terre ? Sache maintenant et vois quelle parole je rapporterai à celui qui m'a envoyé.
24:14	David dit à Gad : Je suis dans une très grande détresse ! Nous tomberons, s’il te plaît, entre les mains de YHWH, car ses matrices sont grandes. Mais je ne tomberai pas entre les mains des humains !
24:15	YHWH envoya la peste en Israël, depuis le matin jusqu'au temps fixé. Depuis Dan jusqu'à Beer-Shéba, il mourut 70 000 hommes parmi le peuple.
24:16	L'ange étendit sa main sur Yeroushalaim pour la ravager, mais YHWH se repentit de ce mal, et il dit à l'ange qui ravageait le peuple : C'est assez ! Retire maintenant ta main ! Or l'Ange de YHWH était près de l'aire d'Aravnah, le Yebousien.
24:17	David parla à YHWH, quand il vit l'ange qui frappait le peuple. Il dit : Voici, c'est moi qui ai péché ! C'est moi qui ai commis l'iniquité ! Mais ces brebis, qu'ont-elles fait ? S'il te plaît, que ta main soit contre moi et contre la maison de mon père !

### Sacrifice de David ; YHWH met fin à la plaie<!--1 Ch. 21:18-30.-->

24:18	Ce jour-là, Gad vint vers David et lui dit : Monte et dresse un autel à YHWH dans l'aire d'Aravnah, le Yebousien.
24:19	David monta, selon la parole de Gad, comme YHWH l'avait ordonné.
24:20	Aravnah regarda et vit le roi et ses serviteurs qui venaient vers lui. Aravnah sortit et se prosterna devant le roi, le visage contre terre.
24:21	Aravnah dit : Pourquoi le roi mon seigneur vient-il vers son serviteur ? Et David dit : Pour acheter ton aire et y bâtir un autel à YHWH, afin que cette plaie se retire de dessus le peuple.
24:22	Aravnah dit à David : Que le roi mon seigneur le prenne et qu'il fasse monter ce qui est bon à ses yeux ! Regarde, les bœufs pour l'holocauste, les battes et les jougs des bœufs pour le bois.
24:23	Tout cela, le roi Aravnah le donna au roi. Aravnah dit au roi : Que YHWH, ton Elohîm, te soit favorable !
24:24	Le roi dit à Aravnah : Non, mais je l'achèterai de toi, je l'achèterai de toi pour son prix. Je ne ferai pas monter pour YHWH, mon Elohîm, des holocaustes qui ne me coûtent rien. Ainsi, David acheta l'aire et les bœufs pour 50 sicles d'argent.
24:25	David bâtit là un autel à YHWH, et fit monter des holocaustes et des sacrifices d'offrande de paix. YHWH intercéda<!--Voir 2 S. 21:14.--> pour la terre, et la plaie fut retenue loin d'Israël.
