# Yaacov (Jacques) (Ja.)

Signification : Qui supplante

Auteur : Yaacov (Jacques)

(Gr. : Iakobos)

Thème : La vie chrétienne sous son aspect pratique

Date de rédaction : Env. 45 – 50 ap. J.-C.

Yaacov, frère de Yéhoshoua ha Mashiah (Jésus-Christ) homme, et ancien au sein de la première assemblée chrétienne située à Jérusalem (Yeroushalaim), écrit aux chrétiens d'origine juive dispersés dans l'Empire romain. Il les console suite à l'adversité qu'ils rencontraient et les exhorte à tenir ferme, leur expliquant que la foi authentique doit être accompagnée d'œuvres. Il les met en garde contre la convoitise, source de toutes les tentations, et les prévient également quant à l'amour du monde et la confiance que certains peuvent mettre dans l'argent. Pour terminer, il leur recommande d'être patients dans l'épreuve et de prier sans cesse jusqu'au retour du Seigneur.

## Chapitre 1

1:1	Yaacov, esclave d'Elohîm et Seigneur Yéhoshoua Mashiah, aux douze tribus qui sont dans la diaspora, salut !
1:2	Mes frères, estimez comme une entière joie quand vous rencontrez diverses tentations,
1:3	sachant que le test<!--La preuve, ce par quoi une chose est essayée, éprouvée, un test. 1 Pi. 1:7.--> de votre foi produit la persévérance.
1:4	Mais que la persévérance ait une œuvre parfaite, afin que vous soyez parfaits et complets, ne manquant de rien.
1:5	Mais si quelqu'un d’entre vous manque de sagesse, qu'il la demande à Elohîm qui la donne à tous simplement et sans reproche, et elle lui sera donnée.
1:6	Mais qu'il la demande avec foi, ne doutant nullement, car celui qui doute est semblable au flot de la mer, agité et poussé çà et là par le vent.
1:7	Que cet humain, en effet, ne s'attende pas à recevoir quelque chose du Seigneur.
1:8	L'homme double de cœur est inconstant dans toutes ses voies.
1:9	Mais que le frère de basse condition se glorifie dans son élévation,
1:10	et le riche dans sa bassesse, parce qu'il passera comme la fleur de l'herbe.
1:11	Car le soleil s'est levé avec sa chaleur brûlante et il a desséché l'herbe, et sa fleur est tombée, et la beauté de son aspect a été détruite. Ainsi le riche se flétrira dans ses entreprises.
1:12	Béni est l'homme qui endure la tentation<!--Le terme grec « peirasmos » utilisé dans ce verset veut aussi dire « épreuve ».--> ! Parce que, ayant été approuvé<!--Vient du grec « dokimos ». Dans le monde ancien, il n'y avait pas nos systèmes bancaires actuels et toute la monnaie était en métal. Ce métal était fondu, versé dans des moules. Après démoulage, il était nécessaire d'enlever les bavures. De nombreuses personnes les rognaient soigneusement pour récupérer du métal. En un siècle, plus de 80 lois ont été promulguées à Athènes pour arrêter la pratique du rognage des pièces en circulation. Il existait quelques changeurs intègres qui n'acceptaient pas de fausse monnaie, et qui ne mettaient en circulation que des pièces au bon poids. On appelait ces hommes des « dokimos » ou « approuvés ». Voir 2 Ti. 2:15.-->, il recevra la couronne de vie, que le Seigneur a promise à ceux qui l'aiment.
1:13	Que personne, lorsqu'il est tenté, ne dise : C’est de la part d'Elohîm que je suis tenté. Car Elohîm ne peut être tenté par le mal, et lui-même ne tente personne.
1:14	Mais chacun est tenté quand il est attiré et attrapé avec un appât par sa propre convoitise.
1:15	Après que la convoitise a conçu, elle enfante le péché, et le péché étant accompli, engendre la mort.
1:16	Mes frères bien-aimés, ne vous égarez pas :
1:17	tout ce qui nous est donné d'excellent et tout don parfait viennent d'en haut et descendent du Père des lumières, en qui il n'y a ni changement ni ombre de variation.
1:18	Parce qu'il l'a voulu, il nous a engendrés par la parole de vérité afin que nous soyons en quelque sorte l'offrande du premier fruit<!--Les prémices. Lé. 23:9-11.--> de ses créatures.
1:19	Ainsi, mes frères bien-aimés, que tout être humain soit prompt à écouter, lent à parler et lent à la colère,
1:20	car la colère de l'homme n'accomplit pas la justice d'Elohîm.
1:21	C'est pourquoi, étant débarrassés<!--Voir Hé. 12:1.--> de toute souillure et de tout résidu<!--Le mot « résidu » vient du grec « perisseia » qui signifie « abondance », « surabondamment », « tout excès », « reste ». Les Grecs utilisaient ce terme pour décrire l'excès de cire dans leurs oreilles. Il est question de la méchanceté qui reste dans un chrétien et qui provient de son état antérieur à sa conversion.--> de méchanceté, recevez avec douceur la parole qui a été plantée en vous et qui peut sauver vos âmes.
1:22	Et mettez en pratique la parole, et ne l'écoutez pas seulement, en vous trompant vous-mêmes par de faux raisonnements.
1:23	Parce que si quelqu'un est auditeur et non observateur de la parole, il est semblable à un homme qui observe le visage de sa genèse<!--Origine.--> dans un miroir,
1:24	car il s'est observé lui-même et s'en est allé et, immédiatement, il a oublié comment il était.
1:25	Mais celui qui aura regardé avec la tête penchée en avant dans la torah parfaite de la liberté, et qui aura persévéré, n'étant pas un auditeur oublieux, mais un faiseur d’œuvre, celui-là sera béni dans son activité.
1:26	Si quelqu'un parmi vous pense être dans l'adoration d'Elohîm sans brider sa langue, mais séduit son cœur, sa religion est vaine.
1:27	La religion pure et sans souillure devant notre Elohîm et Père, c'est de visiter les orphelins et les veuves dans leurs tribulations, et de se conserver sans tache loin du monde.

## Chapitre 2

2:1	Mes frères, n'ayez pas d'égard à l'apparence des personnes dans la foi en notre glorieux Seigneur Yéhoshoua Mashiah.
2:2	Car, s'il entre dans votre synagogue un homme qui porte un anneau d'or et un habit magnifique, et qu'il y entre aussi un pauvre misérablement vêtu,
2:3	et que vous tourniez les yeux vers celui qui porte l'habit magnifique, et lui disiez : Toi, assieds-toi ici honorablement ! Et que vous disiez au pauvre : Toi, tiens-toi là debout ! ou : Assieds-toi ici sur mon marchepied !
2:4	Ne faites-vous pas en vous-mêmes une distinction et n'êtes-vous pas des juges qui avez des pensées injustes ?
2:5	Écoutez, mes frères bien-aimés : Elohîm n'a-t-il pas choisi les pauvres de ce monde pour qu'ils soient riches dans la foi et héritiers du Royaume qu'il a promis à ceux qui l'aiment ?
2:6	Mais vous, vous avez déshonoré le pauvre ! Pourtant, ce sont les riches qui vous oppriment et qui vous traînent devant les tribunaux.
2:7	N'est-ce pas eux qui blasphèment le beau Nom qui a été invoqué sur vous ?
2:8	Néanmoins, si vous accomplissez la torah royale qui est selon l'Écriture : Tu aimeras ton prochain comme toi-même<!--Lé. 19:18.-->, vous faites bien.
2:9	Mais si vous avez égard à l'apparence des personnes, vous commettez un péché et vous êtes convaincus par la torah comme des transgresseurs.
2:10	Car quiconque gardera toute la torah et trébuchera sur un seul point devient coupable de tout.
2:11	Car celui qui a dit : Tu ne commettras pas d'adultère, a dit aussi : Tu n'assassineras pas. Or si donc tu ne commets pas d'adultère<!--Ex. 20:13-14.-->, mais que tu assassines, tu deviens transgresseur de la torah.
2:12	Ainsi parlez et ainsi agissez, comme étant sur le point d'être jugés au moyen de la torah de la liberté,
2:13	car il y aura un jugement sans miséricorde pour celui qui n'aura pas usé de miséricorde<!--Mt. 7:2.-->. Mais la miséricorde triomphe du jugement.
2:14	Quel avantage, mes frères, si quelqu’un dit avoir la foi, tandis qu’il n’a pas les œuvres ? La foi peut-elle le sauver ?
2:15	Et si un frère ou une sœur sont nus et manquent de la nourriture de chaque jour,
2:16	et que l'un d'entre vous leur dise : Allez en paix, chauffez-vous et rassasiez-vous ! Et que vous ne leur donniez pas les choses nécessaires pour le corps, quel en serait l’avantage ? 
2:17	De même aussi la foi, si elle n'a pas les œuvres, elle est morte en elle-même.
2:18	Mais quelqu’un dira : Toi, tu as la foi, et moi j’ai les œuvres. Montre-moi ta foi à partir de tes œuvres, et moi, je te montrerai ma foi à partir de mes œuvres.
2:19	Tu crois que l'Elohîm est un<!--Za. 14:9 ; 1 Ti. 2:5 ; Ga. 3:20 ; Mc. 12:29.--> ? Tu fais bien. Les démons le croient aussi et ils frissonnent<!--se hérisser, se raidir, se durcir, frissonner, être frappé d'une extrême crainte, être horrifié. Le mot grec fait allusion à une manifestation physique telle que le hérissement des poils ou la paralysie que l’on éprouve face à une expérience de forte peur et d’horreur.-->.
2:20	Mais veux-tu savoir, ô homme vain, que la foi sans les œuvres est morte ?
2:21	Abraham, notre père, ne fut-il pas justifié à partir des œuvres, quand il offrit son fils Yitzhak sur l'autel ?
2:22	Tu vois que la foi agissait avec ses œuvres, et que c'est à partir de ses œuvres que sa foi fut rendue parfaite ?
2:23	Ainsi s'accomplit ce que dit l'Écriture : Abraham crut en Elohîm, et cela lui fut compté comme justice<!--Ge. 15:6.-->, et il fut appelé ami d'Elohîm.
2:24	Vous voyez donc que l'être humain est justifié à partir des œuvres, et non à partir de la foi seulement.
2:25	Et de même aussi Rahab, la prostituée, ne fut-elle pas justifiée à partir des œuvres en recevant les messagers et en les renvoyant par un autre chemin<!--Jos. 2:1-21.--> ?
2:26	Car, comme le corps sans esprit est mort<!--Ps. 146:4.-->, de même la foi sans les œuvres est morte.

## Chapitre 3

3:1	Ne soyez pas nombreux, mes frères, à devenir des docteurs<!--Du grec « didaskalos » : « maître », « professeur », « docteur chargé d'instruire, d'enseigner la parole ». Mt. 8:19, 22:16 ; 1 Co. 12:28.-->, sachant que nous recevrons un jugement plus sévère.
3:2	Car tous, nous trébuchons beaucoup. Si quelqu'un ne trébuche pas en parole, c'est un homme parfait, et il peut même tenir en bride tout son corps.
3:3	Voici, nous mettons des mors dans la bouche des chevaux afin qu'ils nous obéissent, et nous dirigeons çà et là tout leur corps.
3:4	Voici aussi les navires : si grands soient-ils et poussés par des vents violents, ils sont dirigés partout çà et là par un petit gouvernail, selon le désir de celui qui les gouverne.
3:5	De même aussi la langue est un petit membre et elle se vante de grandes choses. Voyez quel petit feu embrase une grande forêt !
3:6	La langue aussi est un feu, c'est le monde de l'injustice. Ainsi, la langue est placée parmi nos membres, souillant tout le corps et enflammant la roue de la vie<!--Source, origine, ou la roue de l'origine humaine, où les hommes commencent par naître, puis marcher, c'est donc la roue de la vie.-->, étant elle-même enflammée par la géhenne.
3:7	Car toutes les espèces d'animaux sauvages et d'oiseaux, de reptiles et d'animaux marins, se domptent et ont été domptées par la nature humaine.
3:8	Mais aucun humain ne peut dompter la langue. C'est un mal qui ne peut être contenu : elle est pleine d'un venin mortel.
3:9	Par elle nous bénissons l'Elohîm et Père, et par elle nous maudissons les humains faits à la ressemblance d'Elohîm.
3:10	De la même bouche sortent la bénédiction et la malédiction. Il ne faut pas qu'il en soit ainsi, mes frères.
3:11	Une fontaine fait-elle jaillir par la même ouverture le doux et l'amer ?
3:12	Mes frères, un figuier peut-il produire des olives, ou une vigne des figues ? De même, aucune fontaine ne peut produire de l'eau salée et de l'eau douce.
3:13	Qui est sage et intelligent parmi vous ? Qu'il montre ses œuvres par une bonne conduite avec douceur et sagesse.
3:14	Mais si vous avez dans votre cœur un zèle amer et un esprit partisan<!--Voir Ph. 2:3.-->, ne vous glorifiez pas et ne mentez pas contre la vérité.
3:15	Car ce n'est pas là la sagesse qui descend d'en haut : mais elle est terrestre, animale<!--Animale ou charnelle.--> et diabolique.
3:16	Car là où il y a de la jalousie et un esprit partisan<!--Voir Ph. 2:3.-->, il y a du désordre et toutes sortes de mauvaises choses.
3:17	Mais la sagesse d'en haut est premièrement pure en effet, ensuite pacifique, douce, conciliante, pleine de miséricorde et de bons fruits, sans partialité et sans hypocrisie.
3:18	Mais le fruit de la justice se sème dans la paix pour ceux qui pratiquent la paix.

## Chapitre 4

4:1	D'où viennent parmi vous les guerres et les querelles ? N'est-ce pas de vos plaisirs qui combattent dans vos membres ?
4:2	Vous convoitez et vous n’avez pas. Vous assassinez, vous êtes jaloux et vous ne pouvez rien obtenir. Vous combattez et vous faites la guerre. Mais vous n’avez pas, parce que vous ne demandez pas.
4:3	Vous demandez et vous ne recevez pas, parce que vous demandez mal, afin de tout gaspiller pour vos plaisirs.
4:4	Adultères<!--Vient du grec « moichos » qui signifie « adultère », « être infidèle envers Elohîm », « un impie ».--> et femmes adultères<!--Vient du grec « moichalis » et signifie « femme adultère ». L'alliance intime d'Elohîm avec le peuple d'Israël étant comme un mariage, ceux qui rechutent dans l'idolâtrie sont considérés comme commettant l'adultère ou se livrant à la prostitution ou à l'apostasie. Voir Ro. 7:3.--> ! Ne savez-vous pas que l'amitié pour le monde est inimitié contre Elohîm ? Celui donc qui veut être ami du monde, se constitue ennemi d'Elohîm.
4:5	Pensez-vous que l'Écriture parle en vain ? L’Esprit qui demeure en nous a-t-il du désir qui pousse à l'envie ? 
4:6	Mais il donne une grâce plus grande, c'est pourquoi il dit : Elohîm résiste aux orgueilleux, mais il donne sa grâce aux humbles<!--Pr. 3:34.-->.
4:7	Soumettez-vous donc à Elohîm, résistez au diable et il fuira loin de vous.
4:8	Approchez-vous d'Elohîm et il s'approchera de vous. Pécheurs, nettoyez vos mains, et vous qui êtes doubles de cœur, purifiez vos cœurs.
4:9	Affligez-vous vous-mêmes, lamentez-vous aussi et pleurez, que votre rire se change en pleurs et votre joie en tristesse.
4:10	Humiliez-vous en présence du Seigneur et il vous élèvera.
4:11	Frères, ne vous diffamez pas les uns les autres. Celui qui diffame son frère et qui juge son frère, diffame la torah et juge la torah. Or si tu juges la torah, tu n'es pas l'observateur de la torah, mais le juge.
4:12	Un seul est le législateur, celui qui peut sauver et perdre. Qui es-tu, toi qui juges les autres ?
4:13	À vous maintenant qui dites : Aujourd'hui ou demain nous irons dans telle ou telle ville, nous y passerons une année, nous trafiquerons et nous gagnerons !
4:14	Vous qui ne savez pas ce qu'il en sera du lendemain ! Car qu'est-ce que votre vie ? En effet, c'est une vapeur<!--Voir Ps. 39:6 et 12 ; Ps. 144:4.-->, qui apparaît<!--« Briller », « être brillant ou resplendissant ».--> pour un peu de temps et qui ensuite s'évanouit<!--« Mettre hors de vue », « rendre invisible quelque chose qui s'évanouit », « détruire », « consumer », « priver d'éclat », « rendre laid », « défigurer », « enlaidir ».-->.
4:15	Au lieu de dire : Si le Seigneur le veut, nous vivrons et nous ferons ceci ou cela.
4:16	Mais maintenant vous vous glorifiez dans votre assurance insolente. Se glorifier de cette façon, c'est mauvais !
4:17	Celui donc qui sait faire ce qui est bon et qui ne le fait pas, c'est un péché pour lui.

## Chapitre 5

5:1	À vous maintenant riches ! Pleurez et gémissez sur les malheurs qui viennent sur vous !
5:2	Votre richesse est pourrie et vos vêtements sont mangés par les mites,
5:3	votre or et votre argent sont rouillés, et leur rouille sera pour vous un témoignage et dévorera vos chairs comme un feu. Vous avez accumulé des richesses dans les derniers jours.
5:4	Voici, le salaire des ouvriers qui ont moissonné vos champs, volé par vous crie, et les cris des moissonneurs sont parvenus aux oreilles du Seigneur Tsevaot.
5:5	Vous avez vécu dans les grands luxes sur la Terre, vous vous êtes donnés à une vie de douceur et de luxe, et vous avez engraissé vos cœurs comme en un jour de brebis destinées à la boucherie.
5:6	Vous avez condamné, vous avez assassiné le juste sans qu'il vous résiste.
5:7	Soyez donc patients, frères, jusqu'à la parousie du Seigneur. Voici, le laboureur attend le précieux fruit de la terre, prenant patience à son égard, jusqu'à ce qu'il ait reçu les pluies de la première et de la dernière saison<!--Voir commentaire en Za. 10:1.-->.
5:8	Vous aussi, attendez patiemment et affermissez vos cœurs, parce que la parousie du Seigneur s'approche.
5:9	Frères, ne gémissez pas les uns contre les autres afin que vous ne soyez pas condamnés. Voici, le Juge se tient à la porte.
5:10	Mes frères, prenez pour exemple de patience dans l'affliction les prophètes qui parlèrent au Nom du Seigneur.
5:11	Voici, nous tenons pour bénis ceux qui persévèrent. Vous avez appris quelle a été la persévérance de Iyov<!--Job.-->, et vous avez vu la fin du Seigneur, car le Seigneur est plein de compassion et de miséricorde.
5:12	Mais avant toutes choses, mes frères, ne jurez ni par le ciel, ni par la Terre, ni par aucun autre serment. Mais que votre oui soit oui, et votre non, non, afin que vous ne tombiez pas dans l'hypocrisie<!--l'interprétation d'un acteur de théâtre. Mt. 5:37, 12:36.-->.
5:13	Quelqu'un parmi vous souffre-t-il ? Qu'il prie. Quelqu'un est-il joyeux ? Qu'il chante des louanges.
5:14	Quelqu'un parmi vous est-il malade ? Qu'il appelle les anciens de l'assemblée, et qu'ils prient pour lui en l'oignant d'huile au Nom du Seigneur,
5:15	et la prière de la foi sauvera le malade, et le Seigneur le réveillera. Et s'il a commis des péchés, ils lui seront remis.
5:16	Confessez donc vos fautes les uns les autres, et priez les uns en faveur des autres, afin que vous soyez guéris. La supplication du juste agit avec une grande force.
5:17	Éliyah<!--Élie.--> était un être humain de même nature que nous. Et en prière, il a prié pour qu'il ne pleuve pas et il n'est pas tombé de pluie sur la terre pendant 3 ans et 6 mois<!--1 R. 17:1.-->.
5:18	Et il a de nouveau prié, et le ciel a donné de la pluie et la terre a produit son fruit.
5:19	Frères, si quelqu'un parmi vous s'est égaré loin de la vérité, et qu'un autre l'y ramène,
5:20	qu'il sache que celui qui ramènera un pécheur de l'égarement de sa voie, sauvera une âme de la mort et couvrira une multitude de péchés.
