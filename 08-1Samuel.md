# 1 Shemouél (1 Samuel) (1 S.)

Signification : Entendu, exaucé de El

Auteur : Inconnu

Thème : Histoire de Shemouél (Samuel), Shaoul (Saül) et David

Date de rédaction : 10ème siècle av. J.-C.

Shemouél était le fils d'Elqanah, de la Montagne d'Éphraïm. Channah (Anne), sa mère, avait longtemps désiré un enfant. Elle fit donc une alliance avec Elohîm en lui promettant de lui consacrer son premier fils s'il la rendait féconde. Ainsi, dès son plus jeune âge, Shemouél fut amené à la maison d'Elohîm où il grandit aux côtés d'Éli, le prêtre. À la mort de ce dernier, Shemouél exerça les fonctions de juge, prêtre et prophète. C'est en son temps qu'Israël exprima le désir d'avoir un roi, marquant ainsi la fin de l'ère des juges et le début de la monarchie en Israël.

Ce livre relate l'histoire de Shaoul, premier roi d'Israël, à qui YHWH accorda de puissantes victoires notamment sur les Philistins, grands ennemis du peuple d'Elohîm. Mais très vite, Shaoul s'écarta de la volonté d'Elohîm, aussi YHWH le disqualifia et choisit pour lui succéder au trône un homme de la tribu de Yéhouda : David, fils d'Isaï. Son accession à la royauté ne fut pas immédiate. David dut faire preuve de patience, de courage et de foi en son Elohîm au milieu de nombreuses persécutions. L'expérience des deux premiers rois d'Israël est une exhortation à l'obéissance à Elohîm.

## Chapitre 1

### Stérilité de Channah (Anne)

1:1	Il y avait un homme de Ramathaïm-Tsophim, de la Montagne d'Éphraïm, du nom d'Elqanah, fils de Yeroham, fils d'Éliyhou, fils de Tohou, fils de Tsouph, Éphratien.
1:2	Il avait deux femmes : le nom de l'une était Channah, et le nom de la seconde, Peninnah. Peninnah avait des enfants, mais Channah n'avait pas d'enfants.
1:3	D'année en année, cet homme montait de sa ville à Shiyloh<!--Jos. 18:1.--> pour adorer et pour sacrifier à YHWH Tsevaot<!--YHWH des armées. Voir Es. 8:13 et Ap. 19.-->. Là étaient les deux fils d'Éli, Hophni et Phinées, prêtres de YHWH.
1:4	Il arrivait que, le jour où Elqanah sacrifiait, il donnait des portions à Peninnah, sa femme, à tous les fils et à toutes les filles qu'il avait d'elle.
1:5	Mais il donnait à Channah une portion à double narine, car il aimait Channah, mais YHWH avait fermé sa matrice<!--Elohîm est celui qui ferme et ouvre les portes des bénédictions.-->.
1:6	Sa rivale la vexait pour la pousser à s'irriter parce que YHWH avait fermé sa matrice.
1:7	C’est ainsi qu’elle agissait, d’année en année, chaque fois qu’elle montait à la maison de YHWH. C’est ainsi qu'elle lui causait du chagrin, si bien qu'elle pleurait et ne mangeait pas.
1:8	Elqanah, son homme, lui disait : Channah, pourquoi pleures-tu et pourquoi ne manges-tu pas ? Pourquoi ton cœur est-il triste ? Est-ce que je ne vaux pas pour toi mieux que dix fils ?

### Prière et vœu de Channah à YHWH

1:9	Channah se leva après avoir mangé et bu à Shiyloh. Et le prêtre Éli était assis sur un siège, près de l'un des poteaux du temple de YHWH.
1:10	Elle, ayant l'âme remplie d'amertume, pria YHWH en pleurant, en pleurant.
1:11	Et elle fit un vœu, en disant : YHWH Tsevaot ! Si tu regardes, si tu regardes l'affliction de ta servante, et si tu te souviens de moi, et n'oublies pas ta servante, et que tu donnes à ta servante un enfant mâle, je le donnerai à YHWH pour tous les jours de sa vie, et aucun rasoir ne passera sur sa tête.
1:12	Il arriva que comme elle multipliait sa prière devant YHWH, Éli observait sa bouche.
1:13	Or Channah parlait dans son cœur, elle ne faisait que remuer ses lèvres et on n'entendait pas sa voix. C'est pourquoi Éli estima qu'elle était ivre,
1:14	et Éli lui dit : Jusqu'à quand seras-tu ivre ? Éloigne-toi du vin.
1:15	Channah répondit et dit : Je ne suis pas ivre, mon seigneur, je suis une femme affligée en son esprit, je n'ai bu ni vin ni boisson forte, mais je répandais mon âme devant YHWH.
1:16	Ne donne pas à ta servante les faces d’une fille de Bélial<!--Voir commentaire en De. 13:14.-->, car c'est l'excès de ma douleur et de mon affliction qui m'a fait parler jusqu'à présent.
1:17	Éli répondit et dit : Va en paix ! L'Elohîm d'Israël accordera ta requête, ce que tu lui as demandé !
1:18	Elle dit : Que ta servante trouve grâce à tes yeux ! Et cette femme s’en alla son chemin. Elle mangea et ses faces n'étaient plus les mêmes.

### Naissance de Shemouél (Samuel)

1:19	S’étant levés tôt le matin, ils se prosternèrent devant YHWH, puis ils s'en retournèrent et revinrent dans leur maison à Ramah. Elqanah connut Channah, sa femme, et YHWH se souvint d'elle.
1:20	Il arriva, quelques jours après, que Channah devint enceinte et enfanta un fils. Elle l'appela du nom de Shemouél, parce que, dit-elle, je l'ai demandé à YHWH.
1:21	Cet homme, Elqanah, monta avec toute sa maison pour sacrifier à YHWH le sacrifice annuel et son vœu.
1:22	Mais Channah ne monta pas, car elle dit à son mari : Lorsque le garçon sera sevré, je le ferai venir. Il sera vu en face de YHWH et habitera là pour toujours.
1:23	Elqanah son mari lui dit : Fais ce qui est bon à tes yeux, reste jusqu'à ce que tu l'aies sevré. Seulement que YHWH accomplisse sa parole ! Ainsi, cette femme resta et allaita son fils, jusqu'à ce qu'elle l'ait sevré.

### Shemouél (Samuel) chez Éli : Channah accomplit son vœu

1:24	Elle le fit monter avec elle quand elle l’eut sevré, avec 3 jeunes taureaux, un épha de farine et une outre de vin. Elle le fit venir à la maison de YHWH à Shiyloh. Le garçon était encore garçon.
1:25	Ils tuèrent un jeune taureau et ils amenèrent le garçon à Éli.
1:26	Elle dit : Excuse-moi, mon seigneur ! Aussi vrai que ton âme vit, mon seigneur, je suis cette femme qui me tenais en ta présence pour prier YHWH.
1:27	J'ai prié pour avoir ce garçon, et YHWH m’a accordé la requête que je lui demandais.
1:28	Aussi, moi je l’avais demandé pour YHWH : il sera demandé pour YHWH tous les jours de sa vie. Ils se prosternèrent là devant YHWH.

## Chapitre 2

### Prière de Channah

2:1	Channah pria et dit : Mon cœur se réjouit en YHWH, ma corne a été relevée par YHWH. Ma bouche s'est ouverte contre mes ennemis, parce que je me suis réjouie de ton salut<!--Le mot « salut » vient de l'hébreu « yeshuw'ah » c'est-à-dire « Yéhoshoua » (Jésus). Voir commentaire en Es. 26:1.-->.
2:2	Nul n'est saint comme YHWH. Car il n'y en a pas d'autres que toi, il n'y a pas de rocher<!--Voir commentaire en Es. 8:13-17.--> tel que notre Elohîm.
2:3	Ne vous multipliez pas à parler haut, haut ! L'arrogance sort de votre bouche. Oui, YHWH est le El de connaissance, et c'est par lui que les actions sont pesées.
2:4	L'arc des puissants est brisé, mais ceux qui chancellent ont la force pour ceinture.
2:5	Ceux qui étaient rassasiés se louent pour du pain, mais les affamés ont cessé de l'être. Même la stérile en a enfanté sept, et celle qui avait beaucoup de fils est devenue languissante.
2:6	YHWH est celui qui fait mourir et qui fait vivre, qui fait descendre au shéol et qui fait monter.
2:7	YHWH appauvrit et il enrichit, il abaisse et il élève.
2:8	Il élève le pauvre de la poussière, et il tire le misérable de dessus le fumier, pour le faire asseoir avec les nobles. Et il leur donne en héritage un trône de gloire. Car les colonnes de la Terre sont à YHWH, et il a posé le monde sur elles.
2:9	Il gardera les pieds de ses fidèles, et les méchants se tairont dans les ténèbres. Car l'homme ne triomphera pas par sa force.
2:10	YHWH ! Ils seront abattus, ceux qui contestent contre lui. Il tonnera sur eux des cieux. YHWH jugera les extrémités de la Terre. Il donnera la force à son Roi<!--Le Roi dont il est question ici est le Seigneur Yéhoshoua ha Mashiah (Jésus-Christ), le Roi des rois (Za. 14:9 ; Ap. 19:16).-->, il élèvera la corne de son Mashiah<!--Channah (Anne) a annoncé la glorification ou la résurrection du Seigneur Yéhoshoua, le Mashiah (Jésus-Christ) (Jn. 3:14).-->.
2:11	Elqanah s'en alla à Ramah dans sa maison, et le garçon resta au service de YHWH, en face du prêtre Éli.

### Corruption des fils d'Éli

2:12	Or les fils d'Éli<!--Les fils d'Éli, Hophni et Phinées étaient corrompus. Ils volaient les offrandes d'Elohîm, couchaient avec les femmes qui venaient servir et adorer Elohîm. L'esprit qui animait ces prêtres n'a pas disparu après leur mort, mais il opère encore dans beaucoup d'institutions religieuses actuelles. Beaucoup de dirigeants d'assemblées continuent à s'approprier ce qui appartient à Elohîm (l'adoration, les âmes...). Ils ne craignent pas YHWH. Ils abusent de leur position et de leur autorité pour contraindre leurs fidèles à leur donner la dîme et toutes sortes d'offrandes. Ils font payer les entretiens, les prières, et les divers dons qu'ils peuvent avoir. Non seulement l'esprit qui animait les fils d'Éli existe encore, mais il s'est accru en ces temps actuels.--> étaient des fils de Bélial<!--Voir commentaire en De. 13:14. Ce terme est également utilisé au sujet des méchants qui incitèrent les Israélites à servir les dieux étrangers (De. 13:13-15), des hommes iniques de Guibea (Jg. 19:22, 20:13), des deux vauriens qui accusèrent Naboth (1 R. 21:10-13) et des individus qui s'opposèrent à la monarchie (1 S. 10:27 ; 2 S. 20:1 ; 2 Ch. 13:7). Voir aussi Job 34:18 ; Ps. 18:4, 34:17 ; Pr. 6:12, 16:27, 19:28 ; Na. 1:11.--> et ils ne connaissaient pas YHWH,
2:13	ni l'ordonnance des prêtres avec le peuple. Chaque fois qu'un homme sacrifiait un sacrifice, un serviteur du prêtre venait pendant qu’on faisait cuire la chair, ayant à la main une fourchette à trois dents,
2:14	il frappait dans la chaudière, dans le chaudron, dans la marmite, dans le pot, le prêtre prenait pour lui tout ce que la fourchette enlevait. C'est ainsi qu'ils agissaient envers tout Israël qui arrivait là, à Shiyloh.
2:15	Même avant qu'on fasse brûler la graisse, le serviteur du prêtre venait et disait à l'homme qui sacrifiait : Donne-moi de la chair à rôtir pour le prêtre ! Il ne prendra pas de toi de chair cuite, mais de la chair fraîche !
2:16	Si l'homme lui disait : Qu'on brûle, qu'on brûle aujourd'hui la graisse, ensuite tu prendras ce que ton âme souhaitera, il disait : Non ! Mais tu la donneras maintenant, sinon, je la prendrai de force. 
2:17	Le péché de ces jeunes hommes fut très grand devant YHWH, car ces hommes méprisaient l'offrande de YHWH.

### Shemouél (Samuel) au service de YHWH

2:18	Shemouél faisait le service en présence de YHWH, étant jeune homme, ceint d'un éphod en lin.
2:19	Sa mère lui faisait une petite tunique, qu'elle lui apportait d'année en année, quand elle montait avec son mari pour sacrifier le sacrifice annuel.
2:20	Éli bénit Elqanah et sa femme en disant : YHWH te donnera de cette femme une postérité, à la place de celui de sa requête, qui a été demandé pour YHWH ! Et ils s’en allèrent vers leur lieu.
2:21	Oui, YHWH visita Channah, elle devint enceinte et enfanta trois fils et deux filles. Le garçon Shemouél croissait avec YHWH.

### Éli averti des péchés commis par ses fils

2:22	Or Éli était très vieux. Il entendait dire comment ses fils agissaient à l'égard de tout Israël, et comment ils couchaient avec les femmes qui s'assemblaient à la porte de la tente de réunion.
2:23	Et il leur dit : Pourquoi commettez-vous de telles choses ? Car j'apprends vos méchantes actions de tout le peuple.
2:24	Ne faites plus cela, mes fils, car ce que j'entends dire de vous n'est pas bon. Vous faites pécher le peuple de YHWH.
2:25	Si un homme a péché contre un autre homme, le Juge<!--Ici, le mot « juge » vient de l'hébreu « elohîm ». Elohîm est le juste Juge (Ec. 3:17 ; Ac. 10:42).--> interviendra, mais si quelqu'un pèche contre YHWH, qui interviendra pour lui ? Mais ils n'obéirent pas à la voix de leur père parce que YHWH voulait les faire mourir.
2:26	Cependant le jeune homme Shemouél croissait, et il était agréable à YHWH et aux hommes.
2:27	Or un homme d'Elohîm vint auprès d'Éli, et lui dit : Ainsi parle YHWH : Ne me suis-je pas révélé, révélé à la maison de ton père, quand ils étaient en Égypte, dans la maison de pharaon ?
2:28	Je l'ai choisi parmi toutes les tribus d'Israël pour être mon prêtre, pour monter à mon autel, pour brûler de l'encens, pour porter l'éphod devant moi, et j'ai donné à la maison de ton père tous les holocaustes des fils d'Israël.
2:29	Pourquoi avez-vous foulé aux pieds mes sacrifices et mes offrandes, que j'ai ordonné dans ma demeure ? Et tu as honoré tes fils plus que moi, afin de vous engraisser de toutes les premières offrandes d'Israël, mon peuple !
2:30	C'est pourquoi, - déclaration de YHWH, l'Elohîm d'Israël - j'avais parlé en disant que ta maison et la maison de ton père marcheraient devant moi pour toujours. Mais maintenant, - déclaration de YHWH -, loin de moi cela ! Car j'honorerai ceux qui m'honorent, et ceux qui me méprisent seront insignifiants.
2:31	Voici, les jours viennent où je couperai ton bras, et le bras de la maison de ton père, de telle sorte qu'il n'y ait plus de vieillards dans ta maison.
2:32	Tu verras un adversaire dans ma demeure, et tout le bien qu'il fera à Israël, et il n'y aura plus jamais de vieillards dans ta maison.
2:33	Il y aura un homme des tiens que je ne retrancherai pas d'auprès de mon autel afin de consumer tes yeux et affliger ton âme, mais tous les hommes qui viendront accroître ta maison mourront prématurément.
2:34	Ce qui arrivera à tes deux fils, Hophni et Phinées, sera pour toi un signe : ils mourront tous les deux le même jour.
2:35	Et je m'établirai un prêtre fidèle<!--Hé. 2:17, 7:26-28.-->, qui agira selon mon cœur et selon mon âme. Et je lui bâtirai une maison fidèle<!--La maison fidèle fait premièrement allusion à Israël (Mi. 4) et ensuite à l'Assemblée (Église) (Mt. 16:18). Cette prophétie sera pleinement réalisée lors du millénium (Za. 14).-->, et il marchera pour toujours devant mon Mashiah.
2:36	Et il arrivera que quiconque sera resté de ta maison viendra se prosterner devant lui pour avoir une pièce d'argent et un morceau de pain, et dira : Attache-moi, s'il te plaît, dans une des charges de la prêtrise pour manger un morceau de pain.

## Chapitre 3

### YHWH appelle Shemouél (Samuel)

3:1	Le garçon Shemouél servait YHWH en présence d'Éli. La parole de YHWH était rare en ce temps-là, et les visions n'étaient pas fréquentes.
3:2	Il arriva en ce temps qu'Éli était couché à sa place, ses yeux commençaient à se ternir et il ne pouvait plus voir.
3:3	Et avant que les lampes<!--Le chandelier d'or à 7 branches du tabernacle et du temple de Yeroushalaim (Jérusalem) a été décrit avec une extrême minutie dans plusieurs passages de la Bible. Il a été réalisé selon le modèle imposé par Elohîm à Moshé au Sinaï (Ex. 25:31-40, 37:17-24 ; No. 8:4).--> d'Elohîm soient éteintes, Shemouél était aussi couché dans le temple de YHWH, où était l'arche d'Elohîm.
3:4	YHWH appela Shemouél. Il dit : Me voici !
3:5	Et il courut vers Éli, et lui dit : Me voici, car tu m'as appelé. Mais Éli dit : Je ne t'ai pas appelé, retourne te coucher ! Il alla se coucher.
3:6	YHWH continua et appela encore : Shemouél ! Shemouél se leva, et s'en alla vers Éli, et lui dit : Me voici, car tu m'as appelé. Et Éli dit : Mon fils, je ne t'ai pas appelé, retourne te coucher ! 
3:7	Or Shemouél ne connaissait pas encore YHWH, et la parole de YHWH ne s’était pas encore découverte à lui.
3:8	YHWH appela encore Shemouél pour la troisième fois. Shemouél se leva, alla vers Éli et dit : Me voici, car tu m'as appelé. Éli discerna que c’était YHWH qui appelait le garçon.
3:9	Éli dit à Shemouél : Va et couche-toi et, si on t'appelle, tu diras : Parle, YHWH, car ton serviteur écoute. Shemouél s'en alla et se coucha à sa place.
3:10	YHWH vint, et se tint là, et appela comme les autres fois : Shemouél ! Shemouél ! Shemouél dit : Parle, car ton serviteur écoute.

### Jugement de YHWH sur la maison d'Éli

3:11	YHWH dit à Shemouél : Voici, je vais faire une chose en Israël. Qui l’entendra, ses deux oreilles en tinteront.
3:12	En ce jour-là, j'accomplirai sur Éli tout ce que j'ai déclaré contre sa maison ; je commencerai et j'achèverai.
3:13	Car je l'ai averti que je vais punir sa maison à perpétuité, à cause de l'iniquité dont il a connaissance, par laquelle ses fils se sont rendus infâmes, sans qu'il les ait réprimés.
3:14	C'est pourquoi j'ai juré à la maison d'Éli, qu'aucune propitiation ne sera jamais faite pour l'iniquité de la maison d'Éli, ni par des sacrifices ni par des offrandes.
3:15	Et Shemouél resta couché jusqu'au matin, puis il ouvrit les portes de la maison de YHWH. Or Shemouél craignait de rapporter cette vision à Éli.
3:16	Éli appela Shemouél et lui dit : Shemouél, mon fils ! Il dit : Me voici !
3:17	Il dit : Quelle est la parole qui t'a été déclarée ? S'il te plaît, ne me la cache pas. Qu'Elohîm te fasse ceci et qu'il y ajoute cela, si tu me caches une parole de toute la parole qu’il t’a déclarée.
3:18	Shemouél lui déclara toutes ces paroles, et ne lui en cacha rien. Et il dit : C'est YHWH, qu'il fasse ce qui est bon à ses yeux !
3:19	Shemouél grandissait. Et YHWH était avec lui, il ne laissa tomber à terre aucune de ses paroles.
3:20	Tout Israël, depuis Dan jusqu'à Beer-Shéba, reconnut que Shemouél était établi prophète de YHWH.
3:21	YHWH continuait à se faire voir à Shiyloh, car YHWH se découvrait à Shemouél, à Shiyloh, par la parole de YHWH.

## Chapitre 4

### Les Philistins s'emparent de l'arche ; YHWH juge la maison d'Éli

4:1	La parole de Shemouél fut pour tout Israël. Israël sortit à la rencontre des Philistins pour la bataille. Ils campèrent près d'Eben-Ezer, et les Philistins campaient à Aphek.
4:2	Les Philistins se rangèrent pour aller à la rencontre d’Israël, la bataille fut perdue, et Israël fut battu par les Philistins, qui tuèrent environ 4 000 hommes sur le champ de bataille, dans les champs.
4:3	Quand le peuple rentra au camp, les anciens d'Israël dirent : Pourquoi YHWH nous a-t-il battus aujourd'hui par les Philistins ? Ramenons de Shiyloh l'arche de l'alliance de YHWH, et qu'elle vienne au milieu de nous, et nous sauve de la paume de nos ennemis.
4:4	Le peuple envoya à Shiyloh, d'où l'on apporta l'arche de l'alliance de YHWH Tsevaot qui habite entre les chérubins. Les deux fils d'Éli, Hophni et Phinées, étaient là, avec l'arche de l'alliance d'Elohîm.
4:5	Il arriva que comme l'arche de YHWH entrait dans le camp, tout Israël poussa de grands cris de joie et la terre en fut ébranlée.
4:6	Les Philistins entendirent le bruit de ces cris de joie, et ils dirent : Quel est ce grand bruit de voix dans le camp des Hébreux ? Et ils surent que c’était l’arche de YHWH qui était entrée dans le camp.
4:7	Les Philistins eurent peur, car ils disaient : Elohîm est entré dans le camp. Et ils dirent : Malheur à nous ! Car il n'en a pas été ainsi hier ni avant-hier.
4:8	Malheur à nous ! Qui nous délivrera de la main de ces elohîm majestueux ? Ce sont ces elohîm qui ont frappé les Égyptiens de toutes sortes de plaies dans le désert.
4:9	Philistins, prenez courage et agissez en hommes, pour que vous ne serviez pas les Hébreux comme ils vous ont servis ! Agissez en hommes et combattez !
4:10	Les Philistins combattirent, et Israël fut battu. Et chaque homme s'enfuit dans sa tente. La défaite fut très grande, 30 000 hommes de pied d'Israël périrent.
4:11	L'arche d'Elohîm fut prise, et les deux fils d'Éli, Hophni et Phinées, moururent.
4:12	Un homme de Benyamin courut de la ligne de bataille et arriva à Shiyloh ce même jour, ayant ses vêtements déchirés et la tête recouverte de terre.
4:13	Quand il arriva, voici qu'Éli était assis sur un siège à côté du chemin, il guettait, car son cœur tremblait à cause de l'arche d'Elohîm. Cet homme entra dans la ville et donna les nouvelles, et toute la ville se mit à crier.
4:14	Éli, entendant le bruit de ces clameurs, dit : Qu’est-ce que ce bruit de tumulte ? L’homme vint en hâte et informa Éli.
4:15	Or Éli était fils de 98 ans, ses yeux étaient fixes, il ne pouvait plus voir.
4:16	L'homme dit à Éli : Je viens de la ligne de bataille, car je me suis enfui aujourd'hui de la ligne de bataille. Il dit : Quelle est la parole, mon fils ?
4:17	Celui qui portait les nouvelles répondit et dit : Israël a fui devant les Philistins, et il y a eu une grande défaite du peuple ; tes deux fils Hophni et Phinées sont morts, et l'arche d'Elohîm a été prise !
4:18	Et il arriva qu'aussitôt qu'il eut fait mention de l'arche d'Elohîm, il tomba à la renverse, de dessus son siège, à côté de la porte, se rompit le cou et mourut, car cet homme était vieux et pesant. Il avait été juge en Israël pendant 40 ans.
4:19	Sa belle-fille, femme de Phinées, était enceinte, sur le point d'accoucher. Lorsqu'elle apprit la nouvelle de la prise de l'arche d'Elohîm, de la mort de son beau-père et de son homme, elle se coucha et enfanta, car les douleurs la surprirent.
4:20	Au temps où elle allait mourir, celles qui se tenaient auprès d’elle lui dirent : N'aie pas peur, car tu as enfanté un fils ! Mais elle ne répondit rien et n'en tint pas compte.
4:21	Elle appela le garçon I-Kabod, en disant : La gloire a quitté Israël, en référence à la prise de l’arche de YHWH et en référence à son beau-père et à son homme.
4:22	Elle dit : La gloire a quitté Israël, car l'arche d'Elohîm est prise !

## Chapitre 5

### Jugements de YHWH sur les Philistins

5:1	Les Philistins prirent l'arche d'Elohîm et la firent entrer dans Eben-Ezer à Asdod.
5:2	Les Philistins prirent l'arche d'Elohîm et la firent entrer dans la maison de Dagon<!--L'étymologie du nom « Dagon » avait justifié la représentation qu'on faisait de cet elohîm : une sorte de sirène mâle ou un homme avec une queue de poisson. En effet, « dâg », en hébreu signifie « poisson ». Il était l'elohîm des semences et de l'agriculture chez les peuples d'origine sémite, mais également l'un des principaux elohîm des Philistins.--> et la placèrent à côté de Dagon.
5:3	Le lendemain, les Asdodiens se levèrent de bon matin, voilà que Dagon était tombé sur ses faces, par terre, en face de l’arche de YHWH. Ils prirent Dagon et le remirent à sa place.
5:4	Ils se levèrent de bonne heure le lendemain matin, et voilà que Dagon était tombé sur ses faces, par terre, en face de l'arche de YHWH. La tête de Dagon et les deux paumes de ses mains découpées étaient sur le seuil, et il ne lui restait que le tronc.
5:5	C'est pour cela que les prêtres de Dagon et tous ceux qui entrent dans la maison de Dagon, ne marchent pas sur le seuil de Dagon, à Asdod, jusqu’à ce jour.
5:6	Et la main de YHWH s'appesantit sur les Asdodiens et les dévasta. Il les frappa d'hémorroïdes à Asdod et dans tout son territoire.
5:7	Les hommes d'Asdod, voyant qu'il en était ainsi, dirent : L'arche de l'Elohîm d'Israël ne restera pas chez nous, car sa main s'est appesantie sur nous, et sur Dagon, notre elohîm.
5:8	Ils firent appeler et rassemblèrent auprès d'eux tous les seigneurs des Philistins, et dirent : Que ferons-nous de l'arche de l'Elohîm d'Israël ? Ils dirent : Qu'on transporte à Gath l'arche d'Elohîm d'Israël. Et l'on transporta là-bas l'arche d'Elohîm d'Israël.
5:9	Mais il arriva après qu'on l'eut transportée, la main de YHWH fut sur la ville et il y eut une très grande confusion, et il frappa les hommes de la ville, depuis le plus petit jusqu'au plus grand, par une éruption d'hémorroïdes.
5:10	Ils envoyèrent l'arche d'Elohîm à Ékron. Il arriva lorsque l'arche d'Elohîm entra dans Ékron, que les habitants d'Ékron s'écrièrent en disant : Ils ont transporté chez nous l'arche de l'Elohîm d'Israël pour nous faire mourir, nous et notre peuple !
5:11	Ils envoyèrent rassembler tous les seigneurs des Philistins, en disant : Renvoyez l'arche d'Elohîm d'Israël ! Qu'elle retourne en son lieu et qu'elle ne nous fasse pas mourir, nous et notre peuple. Car il y eut une confusion mortelle dans toute la ville, et la main d'Elohîm s'y appesantissait fortement.
5:12	Les hommes qui n'en mouraient pas étaient frappés d'hémorroïdes, de sorte que l'appel au secours de la ville montait jusqu'aux cieux.

## Chapitre 6

### L'arche de YHWH revient en Israël

6:1	L'arche de YHWH était depuis sept mois dans le champ des Philistins.
6:2	Les Philistins appelèrent les prêtres et les devins, et leur dirent : Que ferons-nous de l'arche de YHWH ? Faites-nous savoir comment nous le renverrons à son lieu.
6:3	Ils dirent : Si l'arche d'Elohîm d'Israël est renvoyée, ne la renvoyez pas à vide, mais payez-lui, payez-lui un sacrifice de culpabilité. Alors vous serez guéris et vous saurez pourquoi sa main ne s'est pas détournée de vous.
6:4	Ils dirent : Quelle offrande lui payerons-nous pour la culpabilité<!--Le mot « culpabilité » vient de l'hébreu « asham » qui signifie « délit », « offense », « ce qui est acquis par un délit, mal acquis ». Il est question ici de l'arche qui avait été volée par les Philistins. Aux yeux d'Elohîm, cet acte était un délit.--> ? Ils dirent : Selon le nombre des seigneurs des Philistins, vous donnerez 5 hémorroïdes d'or, et 5 souris d'or, car une même plaie a été sur vous tous et sur vos seigneurs.
6:5	Vous ferez des figures de vos hémorroïdes, et des figures des souris qui détruisent la terre, et vous donnerez gloire à l'Elohîm d'Israël : peut-être retirera-t-il sa main de dessus vous, et de dessus votre elohîm, et de dessus votre terre.
6:6	Pourquoi endurciriez-vous votre cœur, comme l'Égypte et pharaon ont endurci leur cœur ? Après les avoir traités avec sévérité, ne les laissèrent-ils pas aller ? Et ils s’en allèrent.
6:7	Maintenant prenez et faites un chariot tout neuf, deux vaches qui allaitent leurs veaux et qui n'aient pas porté le joug. Attelez au chariot les deux vaches et ramenez leurs petits à la maison.
6:8	Prenez l'arche de YHWH et mettez-la sur le chariot. Mettez les ouvrages d'or que vous lui aurez payés pour la culpabilité dans un petit coffre à côté de l'arche, et laissez-la aller et elle s'en ira.
6:9	Regardez si elle monte vers Beth-Shémesh, par le chemin de sa frontière, c'est lui qui nous a fait ce grand mal : sinon, nous saurons que ce n’est pas sa main qui nous a atteints : c’est un accident qui nous est arrivé.
6:10	Ces hommes firent ainsi. Ils prirent deux vaches qui allaitaient, ils les attelèrent au chariot, et ils enfermèrent leurs petits dans l'étable.
6:11	Ils mirent sur le chariot l'arche de YHWH, et le coffre avec les souris d'or et les figures de leurs hémorroïdes.
6:12	Les vaches allèrent droit leur chemin, sur le chemin de Beth-Shémesh. Elles allèrent sur un seul chemin, allant et mugissant, et elles ne se détournèrent ni à droite ni à gauche. Les seigneurs des Philistins allèrent derrière elles jusqu'à la frontière de Beth-Shémesh.
6:13	Or ceux de Beth-Shémesh étaient impatients de moissonner les blés dans la vallée. Levant les yeux, ils virent l'arche et se réjouirent en la voyant.
6:14	Le chariot arriva dans le champ de Yéhoshoua de Beth-Shémesh, et s'arrêta là. Or il y avait là une grande pierre, et on fendit le bois du chariot, et on fit monter les vaches en holocauste à YHWH.
6:15	Les Lévites descendirent l'arche de YHWH ainsi que le coffre dans lequel étaient les objets d'or, et ils les mirent sur la grande pierre. Les hommes de Beth-Shémesh firent monter des holocaustes et sacrifièrent des sacrifices à YHWH ce jour-là.
6:16	Les cinq seigneurs des Philistins, après avoir vu cela, retournèrent le même jour à Ékron.
6:17	Voici les hémorroïdes d'or que les Philistins donnèrent à YHWH en offrande pour la culpabilité : un pour Asdod, un pour Gaza, un pour Askalon, un pour Gath, un pour Ékron.
6:18	Les souris d'or, selon le nombre de toutes les villes des Philistins, appartenant aux cinq seigneurs, tant des villes fortifiées que des villages sans murailles. Or la grande pierre<!--« Abel » pour certains, et « Eben » pour d'autres. Abel est une ville du nord d'Israël, près de Beth-Maaka.--> sur laquelle on posa l'arche de YHWH, est encore jusqu’à ce jour dans le champ de Yéhoshoua de Beth-Shémesh.
6:19	Il frappa les hommes de Beth-Shémesh parce qu'ils avaient regardé dans l'arche de YHWH. Il frappa parmi le peuple 50 070 hommes, et le peuple mena le deuil parce que YHWH l'avait frappé d'une grande plaie.
6:20	Les hommes de Beth-Shémesh dirent : Qui pourra tenir debout en face de YHWH, l'Elohîm Saint ? Vers qui montera-t-il loin de nous ?
6:21	Ils envoyèrent des messagers aux habitants de Qiryath-Yéarim, en disant : Les Philistins ont ramené l'arche de YHWH. Descendez et faites-la monter vers vous.

## Chapitre 7

### Un réveil après l'apostasie

7:1	Les hommes de Qiryath-Yéarim vinrent et firent monter l'arche de YHWH. Ils la conduisirent dans la maison d'Abinadab, sur la colline, et ils consacrèrent Èl’azar son fils pour garder l'arche de YHWH.
7:2	Il arriva que depuis le jour où l'arche de YHWH demeura à Qiryath-Yéarim, beaucoup de jours s'écoulèrent : vingt ans. Toute la maison d'Israël gémit après YHWH.
7:3	Et Shemouél parla à toute la maison d'Israël, en disant : Si vous revenez à YHWH de tout votre cœur, ôtez du milieu de vous les elohîm étrangers et les Astartés, dirigez votre cœur vers YHWH et servez-le, lui seul. Et il vous délivrera de la main des Philistins.
7:4	Les fils d'Israël ôtèrent les Baalim et les Astartés, et ils servirent YHWH seul.
7:5	Shemouél dit : Rassemblez tout Israël à Mitspah, et j'intercéderai pour vous auprès de YHWH.
7:6	Ils se rassemblèrent à Mitspah. Ils puisèrent de l'eau qu'ils répandirent devant YHWH, et ils jeûnèrent ce jour-là, en disant : Nous avons péché contre YHWH ! Et Shemouél jugea les fils d'Israël à Mitspah.
7:7	Or quand les Philistins eurent appris que les fils d'Israël étaient rassemblés à Mitspah, les seigneurs des Philistins montèrent contre Israël. Les fils d'Israël l'apprirent et ils eurent peur des Philistins.
7:8	Les fils d'Israël dirent à Shemouél : Ne reste pas muet ! Crie pour nous vers YHWH, notre Elohîm, afin qu'il nous délivre de la main des Philistins !

### Victoire d'Israël contre les Philistins

7:9	Shemouél prit un agneau de lait et le fit monter tout entier en holocauste à YHWH. Et Shemouél cria vers YHWH pour Israël, et YHWH l'exauça.
7:10	Il arriva, pendant que Shemouél faisait monter l'holocauste, que les Philistins s'approchèrent pour combattre contre Israël, mais YHWH tonna à grande voix ce jour-là contre les Philistins et les mit en déroute. Ils furent battus devant Israël.
7:11	Les hommes d'Israël sortirent de Mitspah, poursuivirent les Philistins et les frappèrent jusqu'au-dessous de Beth-Car.
7:12	Shemouél prit une pierre qu'il plaça entre Mitspah et Shen, et il l'appela du nom de Eben-Ezer, en disant : YHWH nous a secourus jusqu'ici.
7:13	Les Philistins furent humiliés et ils ne vinrent plus sur le territoire d'Israël. La main de YHWH fut contre les Philistins tous les jours de Shemouél.
7:14	Les villes que les Philistins avaient prises sur Israël retournèrent à Israël, depuis Ékron jusqu'à Gath, avec leurs territoires ; Israël les délivra de la main des Philistins. Et il y eut la paix entre Israël et les Amoréens.

### Shemouél (Samuel), juge en Israël

7:15	Shemouél fut juge en Israël tous les jours de sa vie.
7:16	Il allait d'année en année faire le tour de Béth-El, de Guilgal et de Mitspah, et il jugeait Israël dans tous ces lieux.
7:17	Il revenait ensuite à Ramah, car c'est là qu'il avait sa maison et qu'il jugeait Israël. Il y bâtit un autel pour YHWH.

## Chapitre 8

### Les fils de Shemouél (Samuel) corrompus ; Israël demande un roi

8:1	Il arriva que lorsque Shemouél fut devenu vieux, il établit ses fils pour juges sur Israël.
8:2	Le nom de son fils premier-né était Yoel, et le nom de son second Abiyah. Ils jugeaient à Beer-Shéba.
8:3	Mais ses fils ne marchèrent pas dans ses voies, ils s'en détournèrent pour les profits acquis par la violence. Ils recevaient des pots-de-vin et pervertissaient<!--Voir La. 3:35-36.--> la justice.
8:4	Tous les anciens d'Israël se rassemblèrent et vinrent auprès de Shemouél à Ramah.
8:5	Ils lui dirent : Voici, tu es devenu vieux, et tes fils ne marchent pas dans tes voies. Maintenant, établis sur nous un roi pour nous juger comme toutes les nations.

### YHWH accepte la requête du peuple

8:6	Aux yeux de Shemouél c'était une mauvaise chose qu'ils aient dit : Établis sur nous un roi pour nous juger. Et Shemouél pria YHWH.
8:7	YHWH dit à Shemouél : Écoute la voix du peuple dans tout ce qu'il te dira, car ce n'est pas toi qu'ils ont rejeté, mais c'est moi qu'ils ont rejeté, afin que je ne règne plus sur eux.
8:8	Ils agissent à ton égard comme ils ont agi depuis le jour où je les ai fait monter hors d'Égypte jusqu'à ce jour. Ils m'ont abandonné, pour servir d'autres elohîm.
8:9	Maintenant, écoute leur voix, mais avertis-les, avertis-les en leur déclarant comment le roi qui régnera sur eux les traitera.

### Avertissement : Le roi sera un joug pour le peuple

8:10	Shemouél dit toutes les paroles de YHWH au peuple qui lui avait demandé un roi.
8:11	Il leur dit : Voici comment vous traitera le roi qui régnera sur vous. Il prendra vos fils et les mettra sur ses chars et parmi ses cavaliers, afin qu'ils courent devant son char.
8:12	Il fera d'eux des chefs de mille et des chefs de cinquante, pour labourer ses terres, pour récolter ses moissons, pour fabriquer ses armes de guerre et l'équipement de ses chars.
8:13	Il prendra aussi vos filles pour en faire des parfumeuses, des cuisinières et des boulangères.
8:14	Il prendra le meilleur de vos champs, de vos vignes et de vos oliviers et il les donnera à ses serviteurs.
8:15	Il prendra la dîme de ce que vous aurez semé et de ce que vous aurez vendangé, et il la donnera à ses eunuques et à ses serviteurs.
8:16	Il prendra vos serviteurs et vos servantes, l'élite de vos jeunes hommes, vos ânes, et les emploiera à ses ouvrages.
8:17	Il prendra la dîme de vos troupeaux, et vous, vous deviendrez ses esclaves.
8:18	En ce jour-là, vous crierez à cause du roi que vous vous serez choisi, mais YHWH ne vous répondra pas en ce jour-là.
8:19	Mais le peuple refusa d'écouter la voix de Shemouél, et ils dirent : Non ! Mais il y aura un roi sur nous.
8:20	Nous deviendrons aussi comme toutes les nations : notre roi nous jugera, il sortira devant nous et il conduira nos guerres.
8:21	Shemouél écouta toutes les paroles du peuple et les rapporta aux oreilles de YHWH.
8:22	YHWH dit à Shemouél : Écoute leur voix et fais régner sur eux un roi. Et Shemouél dit aux hommes d'Israël : Allez-vous-en, chaque homme dans sa ville !

## Chapitre 9

### Shaoul (Saül) choisi pour devenir le premier roi d'Israël

9:1	Or il y avait un homme de Benyamin, du nom de Kis, vaillant et talentueux, fils d'Abiel, fils de Tseror, fils de Becorath, fils d'Aphiach, fils d'un Benyamite.
9:2	Il avait un fils du nom de Shaoul<!--Saül.-->. C'était un beau jeune homme, et aucun homme parmi les fils d'Israël n'était plus beau que lui. Des épaules en haut, il dépassait tout le peuple.
9:3	Les ânesses de Kis, père de Shaoul, s'étant égarées, Kis dit à Shaoul, son fils : S'il te plaît, prends avec toi l'un des serviteurs, lève-toi et va chercher les ânesses !
9:4	Il passa par la Montagne d'Éphraïm et traversa la terre de Shalisha, mais ils ne les trouvèrent pas. Puis ils passèrent par la terre de Shaalim, mais elles n'y étaient pas ; ils passèrent ensuite par la terre de Benyamin, mais ils ne les trouvèrent pas.
9:5	Quand ils furent arrivés en terre de Tsouph, Shaoul dit à son serviteur qui était avec lui : Viens, et retournons, de peur que mon père ne cesse avec les ânesses et ne soit inquiet pour nous.
9:6	Le serviteur lui dit : Voici, s'il te plaît, il y a dans cette ville un homme d'Elohîm, et c'est un homme honorable. Tout ce qu'il déclare arrive, arrive. Allons-y maintenant, peut-être nous fera-t-il connaître le chemin sur lequel nous marchons.
9:7	Et Shaoul dit à son serviteur : Mais si nous y allons, qu'apporterons-nous à l'homme d'Elohîm ? Nous n'avons plus de provisions, et nous n'avons aucun présent pour l'homme d'Elohîm. Qu'est-ce que nous avons ?
9:8	Le serviteur répondit de nouveau à Shaoul et dit : Voici, j'ai encore entre mes mains le quart d'un sicle d'argent : je le donnerai à l'homme d'Elohîm et il nous fera connaître notre chemin.
9:9	Autrefois, en Israël, l'homme qui allait consulter Elohîm disait ainsi : Venez, allons vers le voyant ! Car le prophète s'appelait autrefois le voyant.
9:10	Shaoul dit à son serviteur : Ta parole est bonne ! Viens, allons ! Et ils s'en allèrent dans la ville où était l'homme d'Elohîm.
9:11	Et comme ils montaient par la montée de la ville, ils trouvèrent des jeunes filles qui sortaient pour puiser de l'eau, et ils leur dirent : Le voyant n'est-il pas ici ?
9:12	Elles leur répondirent et dirent : Il y est, le voilà devant toi, hâte-toi maintenant, car aujourd'hui il est venu à la ville, parce que le peuple a aujourd'hui un sacrifice sur le haut lieu.
9:13	Quand vous entrerez dans la ville, vous le trouverez avant qu'il monte au haut lieu pour manger. Car le peuple ne mangera pas jusqu'à ce qu'il soit venu, parce qu'il doit bénir le sacrifice ; après quoi, les conviés mangeront. Montez maintenant, car vous le trouverez aujourd'hui.
9:14	Ils montèrent à la ville. Comme ils entraient dans la ville, Shemouél, qui sortait pour monter au haut lieu, les rencontra.
9:15	Or un jour avant l'arrivée de Shaoul, YHWH avait découvert l'oreille de Shemouél, en disant :
9:16	Demain, à cette même heure, je t'enverrai un homme de la terre de Benyamin, et tu l'oindras pour être le chef de mon peuple d'Israël. Il délivrera mon peuple de la main des Philistins, car j'ai regardé mon peuple, parce que son cri de détresse est venu jusqu'à moi.
9:17	Quand Shemouél vit Shaoul, YHWH lui répondit : Voici l'homme dont je t'ai parlé, c'est lui qui contiendra mon peuple.
9:18	Shaoul s'approcha de Shemouél au milieu de la porte, et dit : Informe-moi, s’il te plaît, où est la maison du voyant. 
9:19	Et Shemouél répondit à Shaoul et dit : Je suis le voyant. Monte devant moi au haut lieu, et vous mangerez aujourd'hui avec moi. Je te laisserai partir demain, et je te dirai tout ce que tu as sur le cœur.
9:20	Mais quant aux ânesses que tu as perdues il y a trois jours, ne t'en inquiète pas, parce qu'elles ont été retrouvées. Et qui est tout le désir d’Israël ? N’est-ce pas toi et toute la maison de ton père ?
9:21	Shaoul répondit et dit : Ne suis-je pas de Benyamin, l'une des moindres tribus d'Israël, et ma famille n'est-elle pas la plus petite de toutes les familles de la tribu de Benyamin ? Pourquoi m'as-tu tenu de tels discours ?
9:22	Shemouél prit Shaoul avec son serviteur, les fit entrer dans la salle, et leur donna une place à la tête des conviés qui étaient environ trente hommes.
9:23	Shemouél dit au cuisinier : Apporte la portion que je t'ai donnée, en te disant : Mets-la à part.
9:24	Le cuisinier prit la cuisse et ce qui l'entoure, et il la servit à Shaoul. Et Shemouél dit : Voici ce qui a été réservé ; mets-le devant toi et mange, car cela a été gardé pour toi, pour le temps fixé, lorsque j'ai résolu de convier le peuple. Et Shaoul mangea avec Shemouél ce jour-là.
9:25	Ils descendirent du haut lieu dans la ville, et il parla avec Shaoul sur le toit.
9:26	Ils se levèrent de bonne heure. Il arriva que comme l’aurore montait, Shemouél appela Shaoul sur le toit et lui dit : Lève-toi et je te laisserai aller. Shaoul se leva, et ils sortirent tous les deux dehors, lui et Shemouél.
9:27	Et comme ils descendaient à l'extrémité de la ville, Shemouél dit à Shaoul : Dis au serviteur de passer devant nous. Et le serviteur passa devant. Arrête-toi maintenant, afin que je te fasse entendre la parole d'Elohîm.

## Chapitre 10

### Shemouél (Samuel) oint Shaoul (Saül) comme roi

10:1	Shemouél prit une fiole d'huile, qu'il répandit sur la tête de Shaoul. Il l'embrassa et lui dit : YHWH ne t'a-t-il pas oint pour être le chef de son héritage ?
10:2	Aujourd'hui, après m'avoir quitté, tu trouveras deux hommes près du sépulcre de Rachel, sur la frontière de Benyamin à Tseltsach, qui te diront : Les ânesses que tu es allé chercher sont retrouvées ; et voici, ton père ne pense plus à l'affaire des ânesses, mais il s'inquiète pour vous. Il se dit : Que dois-je faire à propos de mon fils ?
10:3	En allant plus loin, tu arriveras au grand arbre de Thabor, où tu seras rencontré par trois hommes qui montent vers Elohîm, à Béth-El, et l'un porte 3 chevreaux, l'autre 3 pains, et l'autre une outre de vin.
10:4	Ils te questionneront sur la paix et te donneront deux pains. Tu les recevras de leurs mains.
10:5	Après cela tu arriveras à Guibea-Elohîm, où se trouve une garnison de Philistins. Et il arrivera qu'en entrant là, dans la ville, tu rencontreras une troupe de prophètes descendant du haut lieu, précédés du luth, du tambourin, de la flûte et de la harpe, et qui prophétisent.
10:6	L'Esprit de YHWH fondra sur toi, tu prophétiseras avec eux et tu seras changé en un autre homme.
10:7	Il arrivera que quand ces signes viendront sur toi, fais toi-même ce que ta main trouvera, car Elohîm est avec toi.
10:8	Tu descendras devant moi à Guilgal. Voici, je descendrai vers toi pour faire monter des holocaustes et des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->. Tu m'attendras là 7 jours, jusqu'à ce que je vienne et que je te déclare ce que tu devras faire.
10:9	Il arriva qu'aussitôt qu'il eut tourné le dos pour se séparer de Shemouél, Elohîm changea son cœur, et tous ces signes s'accomplirent le même jour.
10:10	Quand ils arrivèrent là, à Guibea, voici, une troupe de prophètes le rencontra. L'Esprit d'Elohîm fondit sur lui, et il prophétisa au milieu d'eux.
10:11	Il arriva que, quand tous ceux qui l'avaient connu d'hier et d'avant-hier le virent, et voici, il prophétisait avec les prophètes. Le peuple dit, chaque homme à son compagnon : Qu'est-il arrivé au fils de Kis ? Shaoul est-il aussi parmi les prophètes ?
10:12	Un homme répondit en disant : Et qui est leur père ? De là le proverbe : Shaoul est-il aussi parmi les prophètes ?
10:13	Lorsqu'il eut cessé de prophétiser, il se rendit au haut lieu.
10:14	L'oncle de Shaoul dit à Shaoul et à son serviteur : Où êtes-vous allés ? Il dit : Chercher les ânesses, mais ne les trouvant pas, nous sommes allés vers Shemouél.
10:15	Et l'oncle de Shaoul dit : Déclare-moi, s'il te plaît, ce que vous a dit Shemouél.
10:16	Shaoul dit à son oncle : Il nous a rapporté, rapporté que les ânesses étaient retrouvées. Le discours sur la royauté, il ne le lui rapporta pas, ce qu’avait déclaré Shemouél.
10:17	Shemouél convoqua le peuple devant YHWH à Mitspah.
10:18	Et il dit aux fils d'Israël : Ainsi parle YHWH, l'Elohîm d'Israël : J'ai fait monter Israël hors d'Égypte, et je vous ai délivrés de la main des Égyptiens et de la main de tous les royaumes qui vous opprimaient.
10:19	Mais aujourd'hui, vous avez rejeté votre Elohîm, celui qui vous a délivrés de tous vos malheurs et de vos afflictions, et vous avez dit : Non, établis un roi sur nous ! Présentez-vous maintenant, devant YHWH, par tribus et par familles.
10:20	Shemouél fit approcher toutes les tribus d'Israël, et la tribu de Benyamin fut prise.
10:21	Il fit approcher la tribu de Benyamin selon ses familles, et la famille de Matri fut prise. Puis Shaoul<!--Voir en annexe le tableau « Chronologie : Les rois et les prophètes ».-->, fils de Kis, fut désigné. On le chercha, mais on ne le trouva pas.
10:22	On consulta de nouveau YHWH : Un homme est-il venu encore ici ? YHWH dit : Il est caché au milieu des armes.
10:23	Ils coururent le chercher, et il se présenta au milieu du peuple, et il était plus grand que tout le peuple, depuis les épaules en haut.
10:24	Et Shemouél dit à tout le peuple : Voyez-vous celui que YHWH a choisi ? Il n'y a personne dans tout le peuple qui soit semblable à lui. Et le peuple poussa des cris de joie, et dit : Vive le roi !
10:25	Shemouél fit connaître au peuple les règles de la royauté et les écrivit dans un livre, qu'il déposa devant YHWH. Puis Shemouél renvoya le peuple, chacun dans sa maison.
10:26	Shaoul aussi s'en alla chez lui à Guibea. Il fut accompagné par des vaillants hommes dont Elohîm avait touché le cœur.
10:27	Mais il y eut des fils de Bélial<!--Voir commentaire en De. 13:14.--> qui dirent : Comment celui-ci nous délivrerait-il ? Et ils le méprisèrent et ne lui apportèrent pas de présent. Mais Shaoul fit le sourd.

## Chapitre 11

### Shaoul (Saül) bat les Ammonites

11:1	Nachash, l'Ammonite, vint et assiégea Yabesh en Galaad. Les habitants de Yabesh dirent à Nachash : Traite alliance avec nous et nous te servirons.
11:2	Mais Nachash, l'Ammonite, leur dit : Je traiterai avec vous à la condition que je vous crève à tous l'œil droit et que je mette cette insulte sur tout Israël.
11:3	Les anciens de Yabesh lui dirent : Donne-nous sept jours de trêve, et nous enverrons des messagers dans tout le territoire d'Israël. Si personne ne vient nous délivrer, nous nous rendrons à toi.
11:4	Les messagers arrivèrent à Guibea de Shaoul, et dirent ces paroles devant le peuple. Tout le peuple éleva sa voix et pleura.
11:5	Et voici, Shaoul revenait des champs derrière ses bœufs, et il dit : Qu'est-ce qu'a ce peuple pour pleurer ainsi ? Et on lui raconta ce qu'avaient dit ceux de Yabesh.
11:6	L'Esprit d'Elohîm fondit sur Shaoul quand il entendit ces paroles, et ses narines s'enflammèrent extrêmement.
11:7	Il prit une paire de bœufs et les coupa en morceaux, qu'il envoya dans tout le territoire d'Israël par la main des messagers, en disant : Les bœufs de tous ceux qui ne sortiront pas pour suivre Shaoul et Shemouél, seront traités de la même manière. Et la frayeur de YHWH tomba sur le peuple, et ils sortirent comme un seul homme.
11:8	Il en fit le dénombrement à Bézek : les fils d'Israël étaient 300 000 et ceux de Yéhouda 30 000.
11:9	Ils dirent aux messagers qui étaient venus : Vous parlerez ainsi aux hommes de Yabesh en Galaad : Vous serez délivrés demain, quand le soleil deviendra chaud. Les messagers vinrent et rapportèrent cela aux hommes de Yabesh, qui se réjouirent.
11:10	Les hommes de Yabesh dirent : Demain nous nous rendrons à vous, et vous nous traiterez selon tout ce qui sera bon à vos yeux.
11:11	Et il arriva que, le lendemain, Shaoul disposa le peuple en trois corps. Ils entrèrent dans le camp des Ammonites à la veille du matin et ils les battirent jusqu'à la chaleur du jour. Ceux qui échappèrent furent dispersés si bien qu'il n'en resta pas deux ensemble.

### Le peuple reconnaît Shaoul (Saül) comme roi

11:12	Le peuple dit à Shemouél : Qui est-ce qui dit : Shaoul régnera-t-il sur nous ? Donnez-nous ces hommes-là, et nous les ferons mourir.
11:13	Shaoul dit : Aucun homme ne sera mis à mort en ce jour, car YHWH a délivré Israël aujourd'hui.
11:14	Et Shemouél dit au peuple : Venez, allons à Guilgal, et nous y renouvellerons la royauté.
11:15	Tout le peuple se rendit à Guilgal. Et là, ils firent Shaoul roi devant YHWH, à Guilgal. Là, ils sacrifièrent des sacrifices d'offrande de paix devant YHWH. Là, Shaoul et tous les hommes d'Israël se réjouirent beaucoup.

## Chapitre 12

### Le peuple rend un bon témoignage de Shemouél (Samuel)

12:1	Shemouél dit à tout Israël : Voici, j'ai obéi à votre voix dans tout ce que vous m'avez dit, et j'ai établi un roi sur vous.
12:2	Et maintenant, voici le roi qui marchera devant vous. Pour moi, je suis devenu vieux, j'ai blanchi et mes fils sont parmi vous. J'ai marché devant vous depuis ma jeunesse jusqu'à ce jour.
12:3	Me voici ! Témoignez contre moi, devant YHWH, et devant son mashiah. De qui ai-je pris le bœuf ? De qui ai-je pris l'âne ? Qui ai-je opprimé ? Qui ai-je traité durement ? De la main de qui ai-je reçu une rançon, afin de fermer les yeux sur lui ? Et je vous le rendrai.
12:4	Ils dirent : Tu ne nous as pas opprimés, tu ne nous as pas traités durement et tu n’as rien pris de la main d’un homme.
12:5	Il leur dit : YHWH est témoin contre vous, et son mashiah aussi est témoin aujourd'hui, que vous n'avez rien trouvé entre mes mains. Et ils dirent : Il en est témoin.

### Rappel des péchés du peuple ; exhortation à craindre YHWH

12:6	Shemouél dit au peuple : C'est YHWH qui a fait Moshé et Aaron, et qui a fait monter vos pères hors de la terre d'Égypte.
12:7	Maintenant, présentez-vous, et je vous jugerai devant YHWH sur tous les bienfaits que YHWH vous a accordés, à vous et à vos pères.
12:8	Après que Yaacov fut entré en Égypte, vos pères crièrent à YHWH, et YHWH envoya Moshé et Aaron qui firent sortir vos pères hors d'Égypte, et les firent habiter en ce lieu.
12:9	Mais ils oublièrent YHWH, leur Elohîm, et il les livra entre les mains de Sisera, chef de l'armée de Hatsor, et entre les mains des Philistins, et entre les mains du roi de Moab, qui leur firent la guerre.
12:10	Ils crièrent encore à YHWH, et dirent : Nous avons péché, car nous avons abandonné YHWH, et nous avons servi les Baalim et les Astartés. Maintenant, délivre-nous de la main de nos ennemis, et nous te servirons.
12:11	YHWH envoya Yeroubbaal, Bedan, Yiphtah et Shemouél, et il vous délivra de la main de tous vos ennemis d'alentour, et vous avez habité en sécurité.
12:12	Voyant que Nachash, roi des fils d'Ammon, marchait contre vous, vous m'avez dit : Non, mais un roi régnera sur nous, quoique YHWH, votre Elohîm, fût votre Roi.
12:13	Maintenant, voici le roi que vous avez choisi, que vous avez demandé, et voici YHWH l'a établi roi sur vous.
12:14	Si vous craignez YHWH, si vous le servez, et obéissez à sa voix, et que vous n'êtes pas désobéissants au commandement de YHWH, alors vous, et le roi qui règne sur vous, vous irez après YHWH, votre Elohîm.
12:15	Si vous n’écoutez pas la voix de YHWH, si vous êtes désobéissants à la bouche de YHWH, la main de YHWH sera contre vous et contre vos pères.
12:16	Maintenant, préparez-vous, et voyez cette grande chose que YHWH va opérer sous vos yeux.
12:17	N'est-ce pas aujourd'hui la moisson des blés ? Je crierai à YHWH, et il donnera des voix et de la pluie. Sachez alors et voyez combien vous avez mal agi aux yeux de YHWH en demandant un roi.
12:18	Shemouél cria à YHWH, et YHWH donna des voix et de la pluie ce même jour. Tout le peuple eut une grande crainte de YHWH et de Shemouél.
12:19	Et tout le peuple dit à Shemouél : Intercède auprès de YHWH, ton Elohîm, pour tes serviteurs, afin que nous ne mourions pas, car nous avons ajouté à nos péchés, celui d'avoir demandé un roi.
12:20	Shemouél dit au peuple : N'ayez pas peur ! Vous avez fait tout ce mal, néanmoins ne vous détournez pas de YHWH, mais servez YHWH de tout votre cœur.
12:21	Ne vous détournez pas en effet pour suivre le tohu, qui n’apporte ni profit ni délivrance, parce que ce n’est qu'un tohu.
12:22	Car YHWH n'abandonne pas son peuple, pour l'amour de son grand Nom, car YHWH est déterminé à faire de vous son peuple.
12:23	Loin de moi aussi de pécher contre YHWH, de cesser de prier pour vous ! Je vous enseignerai le bon et le droit chemin.
12:24	Craignez seulement YHWH, et servez-le en vérité, de tout votre cœur. Car voyez quelles grandes choses il a faites pour vous.
12:25	Mais si vous faites le mal, si vous faites le mal vous serez détruits vous et votre roi.

## Chapitre 13

### Impatience et désobéissance de Shaoul (Saül) ; la royauté lui sera enlevée

13:1	Shaoul était fils de... ans<!--L'âge n'a pas été déterminé, probablement dû à une erreur de copiste.-->, lorsqu’il régna et il régna deux ans sur Israël.
13:2	Shaoul choisit 3 000 hommes d'Israël, 2 000 avec lui à Micmash, et sur la Montagne de Béth-El, et 1 000 étaient avec Yonathan<!--Yehonathan et Yonathan correspondent au même nom, généralement traduit par « Jonathan ».--> à Guibea de Benyamin. Il renvoya le reste du peuple, chaque homme à ses tentes.
13:3	Yonathan battit le poste des Philistins qui était à Guéba, et les Philistins en furent informés. Et Shaoul fit sonner le shofar sur toute la terre, en disant : Que les Hébreux écoutent !
13:4	Tout Israël apprit que Shaoul avait battu le poste des Philistins, et Israël se rendit odieux aux Philistins. Et le peuple fut convoqué auprès de Shaoul, à Guilgal.
13:5	Les Philistins se rassemblèrent pour combattre Israël, ayant 30 000 chars et 6 000 cavaliers, et le peuple était aussi nombreux que le sable au bord de la mer, tant il était en grand nombre. Ils montèrent camper à Micmash, à l'orient de Beth-Aven.
13:6	Les hommes d'Israël virent qu’ils étaient en effet serrés de près, car le peuple était opprimé, et le peuple se cacha dans les cavernes, dans les buissons, dans les rochers, dans les tours et dans des citernes.
13:7	Les Hébreux passèrent le Yarden, vers la terre de Gad et de Galaad. Shaoul était encore à Guilgal, et derrière lui tout le peuple tremblait.
13:8	Il attendit 7 jours, jusqu'au temps fixé par Shemouél. Mais Shemouél ne venait pas à Guilgal et le peuple se dispersait.
13:9	Shaoul dit : Amenez-moi un holocauste et des sacrifices d'offrande de paix. Et il fit monter l'holocauste.
13:10	Il arriva qu'aussitôt qu'il eut achevé de faire monter l'holocauste, voici que Shemouél arriva, et Shaoul sortit au-devant de lui pour le bénir.
13:11	Shemouél lui dit : Qu'as-tu fait ? Shaoul dit : Lorsque j'ai vu que le peuple se dispersait, que tu ne venais pas au jour fixé, et que les Philistins étaient rassemblés à Micmash,
13:12	je me suis dit : Maintenant les Philistins descendront contre moi à Guilgal et je n'ai pas supplié les faces de YHWH ! Étant malade et, après m’être retenu, j'ai fait monter l'holocauste.
13:13	Shemouél dit à Shaoul : C'est en insensé que tu as agi ! Car tu n'as pas gardé le commandement que YHWH, ton Elohîm, t'avait donné. En effet, YHWH aurait maintenu à jamais ta royauté sur Israël.
13:14	Maintenant ta royauté ne tiendra pas. YHWH s'est choisi un homme selon son cœur, et YHWH l'a destiné à être le chef de son peuple, parce que tu n'as pas respecté le commandement de YHWH.

### Shaoul (Saül) et ses hommes à Guibea de Benyamin

13:15	Shemouél se leva et monta de Guilgal à Guibea de Benyamin. Et Shaoul dénombra le peuple qui se trouvait avec lui, environ 600 hommes.
13:16	Or Shaoul vint s'établir avec son fils Yonathan, et le peuple qui était sous ses ordres à Guéba de Benyamin, et les Philistins étaient campés à Micmash.
13:17	Les Philistins sortirent du camp en trois divisions pour ravager : l'une de ces divisions prit le chemin d'Ophrah, vers la terre de Shoual,
13:18	l'autre division prit le chemin de Beth-Horon, et la troisième prit le chemin de la frontière qui regarde vers la vallée de Tseboïm, du côté du désert.
13:19	Or dans toute la terre d'Israël, il ne se trouvait aucun artisan, car les Philistins avaient dit : Que les Hébreux ne fassent pas des épées ou des lances.
13:20	Tout Israël descendait chez les Philistins pour aiguiser, chaque homme son instrument pour labourer, son soc de charrue, sa hache et sa bêche,
13:21	le prix était d’un pim<!--Le pim était un poids en pierre et pesait à peu près deux tiers d’un sicle.--> pour les bêches, pour les socs de charrue et pour les fourches à trois dents, ainsi que pour les haches et pour redresser les aiguillons.
13:22	Il arriva qu'au jour du combat, il ne se trouvait ni épée ni lance entre les mains de tout le peuple qui était avec Shaoul et Yonathan. On en trouva néanmoins pour Shaoul et pour son fils Yonathan.
13:23	Un poste des Philistins sortit vers le passage de Micmash.

## Chapitre 14

### Courage de Yehonathan (Yonathan)

14:1	Il arriva qu’un jour Yonathan<!--Voir commentaire en 1 S. 13:2.-->, fils de Shaoul, dit au garçon qui portait ses armes : Viens, et passons vers le poste des Philistins qui est de l'autre côté. Mais il ne dit rien à son père.
14:2	Shaoul se tenait à l'extrémité de Guibea sous un grenadier, à Migron, entouré d'environ 600 hommes.
14:3	Achiyah, fils d'Ahitoub, frère d'I-Kabod, fils de Phinées, fils d'Éli, prêtre de YHWH à Shiyloh, portait l'éphod. Et le peuple ignorait que Yonathan s'en était allé.
14:4	Entre les passages par lesquels Yonathan cherchait à passer vers le poste des Philistins, il y avait une dent de rocher d'un côté et une dent de rocher de l'autre, le nom de l'un était Botsets et le nom de l'autre Séné.
14:5	L'une des dents était une colonne au nord, en face de Micmash, et l'autre était au sud, en face de Guéba.
14:6	Yehonathan dit au garçon porteur de ses armes : Va, passons vers le poste de ces incirconcis. Peut-être YHWH agira-t-il pour nous, car on ne saurait empêcher YHWH de délivrer avec peu ou beaucoup de gens.
14:7	Et le porteur de ses armes lui dit : Fais tout ce que tu as dans le cœur. Étends-toi ! Me voici avec toi selon ton cœur.
14:8	Yehonathan lui dit : Voici, passons vers ces hommes en nous découvrant à eux.
14:9	S’ils nous disent ainsi : Soyez silencieux jusqu'à ce que nous venions à vous ! Nous nous tiendrons sur place et ne monterons pas vers eux.
14:10	S'ils disent ainsi : Montez vers nous ! Nous irons, car YHWH les aura livrés entre nos mains. Que cela soit pour nous un signe.
14:11	Ils se montrèrent tous les deux au poste des Philistins, et les Philistins dirent : Voici, les Hébreux sortent des trous où ils s'étaient cachés.
14:12	Les hommes de garde répondirent à Yonathan et au porteur de ses armes et dirent : Montez vers nous, nous avons quelque chose à vous apprendre. Yonathan dit au porteur de ses armes : Monte avec moi, car YHWH les a livrés entre les mains d'Israël.
14:13	Yonathan monta de ses mains et de ses pieds, le porteur de ses armes derrière lui. Les Philistins tombèrent face à Yonathan, et le porteur de ses armes les tuait derrière lui.
14:14	Et du premier coup porté par Yonathan et le porteur de ses armes ils tuèrent environ vingt hommes, dans un espace d'environ une moitié d'un arpent de champ.
14:15	Ce fut la terreur dans le camp, dans les champs et parmi tout le peuple. Le poste et ceux qui ravageaient furent eux aussi effrayés. La terre trembla et ce fut une terreur d'Elohîm.

### Victoire d'Israël

14:16	Les sentinelles de Shaoul, qui étaient à Guibea de Benyamin, virent que la multitude était en désordre et s'en allait en s'entre-tuant.
14:17	Shaoul dit au peuple qui était avec lui : Comptez, s’il vous plaît, et voyez qui s'en est allé du milieu de nous. Ils comptèrent, et voici Yonathan n'y était pas, ni le porteur de ses armes.
14:18	Et Shaoul dit à Achiyah : Fais approcher l'arche d'Elohîm ! Car l'arche d'Elohîm était, en ce jour-là, avec les fils d'Israël.
14:19	Il arriva que pendant que Shaoul parlait au prêtre, le tumulte venant du camp des Philistins augmentait de plus en plus. Shaoul dit au prêtre : Retire ta main !
14:20	Shaoul et tout le peuple furent rassemblés à grand cri et arrivèrent jusqu’à la bataille, et voici que l’épée de chaque homme était contre son compagnon, et la confusion fut très grande.
14:21	Les Hébreux qui étaient pour les Philistins comme hier et avant-hier, eux qui étaient montés avec eux au camp, autour, eux aussi étaient avec Israël, du côté de Shaoul et de Yonathan.
14:22	Tous les hommes d'Israël qui s'étaient cachés dans la Montagne d'Éphraïm, ayant appris que les Philistins s'enfuyaient, les poursuivirent aussi pour les combattre.
14:23	Ce jour-là YHWH sauva Israël. Le combat s'étendit au-delà de Beth-Aven.

### Yehonathan (Yonathan) épargné des conséquences du vœu de Shaoul (Saül)

14:24	Les hommes d'Israël furent épuisés ce jour-là. Or Shaoul avait fait jurer le peuple, en disant : Maudit soit l'homme qui prendra de la nourriture avant le soir, avant que je me sois vengé de mes ennemis ! Et le peuple ne goûta pas de nourriture.
14:25	Tout le peuple arriva dans une forêt, où il y avait du miel sur les faces du champ.
14:26	Lorsque le peuple entra dans la forêt, il vit le miel qui coulait, mais personne ne porta la main à sa bouche, car le peuple craignait le serment.
14:27	Or Yonathan n'avait pas entendu son père lorsqu'il avait fait jurer le peuple. Il étendit le bout du bâton qu'il avait à la main, le trempa dans un rayon de miel et porta sa main à sa bouche et ses yeux devinrent brillants.
14:28	Un homme du peuple répondit et dit : Ton père a fait jurer, il a fait jurer le peuple en disant : Maudit soit l'homme qui mangera aujourd'hui quelque chose ! Alors que le peuple était fatigué.
14:29	Yonathan dit : Mon père trouble le peuple. Voyez, s’il vous plaît, comme mes yeux sont devenus brillants après avoir goûté un peu de ce miel.
14:30	Certes, si le peuple avait mangé, mangé du butin de ses ennemis, la défaite des Philistins n'aurait-elle pas été plus grande ?
14:31	En ce jour-là ils frappèrent les Philistins de Micmash à Ayalon. Le peuple était très fatigué.
14:32	Il se jeta sur le butin, il prit des brebis, des bœufs et des veaux, et les tua sur la terre, et le peuple les mangeait avec le sang.
14:33	On informa Shaoul en disant : Voici, le peuple pèche contre YHWH, en mangeant du sang. Et il dit : Vous avez trahi ! Roulez-moi ici une grosse pierre !
14:34	Shaoul dit : Dispersez-vous parmi le peuple et dites-leur : Amenez-moi chaque homme son bœuf, chaque homme sa brebis, et tuez-les ici ! Vous les mangerez et vous ne pécherez plus contre YHWH, en mangeant du sang. Tout le peuple amena cette nuit, chaque homme son bœuf à la main, et ils les tuèrent là.
14:35	Shaoul bâtit un autel à YHWH. Ce fut le premier autel qu'il bâtit à YHWH.
14:36	Shaoul dit : Descendons après les Philistins cette nuit ! Pillons-les jusqu’à la lumière du matin ! Nous ne leur laisserons pas un homme. Ils lui dirent : Fais ce qui est bon à tes yeux. Mais le prêtre dit : Approchons-nous d'abord d'Elohîm.
14:37	Shaoul consulta Elohîm : Descendrai-je après les Philistins ? Les livreras-tu entre les mains d'Israël ? Mais il ne lui répondit pas ce jour-là.
14:38	Shaoul dit : Approchez ici, vous tous les chefs du peuple, recherchez et voyez par qui ce péché est arrivé aujourd'hui.
14:39	Oui, YHWH est vivant, le Sauveur d'Israël ! Même si c'est mon fils Yonathan, mourir, il mourra ! Mais dans tout le peuple, personne ne lui répondit.
14:40	Il dit à tout Israël : Mettez-vous d'un côté, et nous serons de l'autre, moi et mon fils, Yonathan. Le peuple dit à Shaoul : Fais ce qui est bon à tes yeux.
14:41	Shaoul dit à YHWH, l'Elohîm d'Israël : Fais connaître la vérité. Yonathan et Shaoul furent pris, et le peuple fut écarté.
14:42	Shaoul dit : Jetez le sort entre moi et Yonathan, mon fils. Et Yonathan fut pris.
14:43	Shaoul dit à Yonathan : Déclare-moi ce que tu as fait. Et Yonathan lui déclara et dit : Goûter, j'ai goûté avec le bout de mon bâton que j'avais à la main un peu de miel : me voici, que je meurs !
14:44	Shaoul dit : Qu’ainsi fasse Elohîm et qu’ainsi il y ajoute, mourir, tu mourras<!--Répétition des mots « tu mourras, tu mourras ». Voir commentaire en Ge. 2:17.-->, Yonathan.
14:45	Le peuple dit à Shaoul : Yonathan qui a accompli cette grande délivrance en Israël mourrait-il ? Loin de là ! YHWH est vivant ! Il ne tombera pas à terre un seul des cheveux de sa tête, car c'est avec Elohîm qu'il a agi en ce jour ! Ainsi le peuple racheta Yonathan et il ne mourut pas.
14:46	Shaoul remonta de la poursuite des Philistins, et les Philistins s’en allèrent vers leur lieu.

### Les guerres sous le règne de Shaoul (Saül)

14:47	Shaoul ayant pris possession de la royauté sur Israël, fit la guerre de tous côtés contre ses ennemis : contre Moab, contre les fils d'Ammon, contre Édom, contre les rois de Tsoba et contre les Philistins ; partout où il se tournait, il était vainqueur.
14:48	Il agit avec puissance et frappa Amalek. Il délivra Israël de la main de ceux qui le pillaient.
14:49	Les fils de Shaoul étaient Yonathan, Yishviy et Malkiyshoua. Et quant aux noms de ses deux filles, le nom de l'aînée était Mérab et le nom de la plus jeune, Miykal.
14:50	Le nom de la femme de Shaoul était Achinoam, fille d'Achimaats. Le nom du chef de son armée était Abner, fils de Ner, oncle de Shaoul.
14:51	Kis, père de Shaoul, et Ner, père d'Abner, étaient fils d'Abiel.
14:52	Il y eut une forte guerre contre les Philistins pendant tous les jours de Shaoul. Quand Shaoul voyait tout homme vaillant ou tout fils talentueux, il le prenait auprès de lui.

## Chapitre 15

### Shaoul (Saül) désobéit une fois de plus

15:1	Shemouél dit à Shaoul : YHWH m'a envoyé pour t'oindre afin que tu sois roi sur son peuple, sur Israël. Maintenant, écoute les paroles de YHWH !
15:2	Ainsi parle YHWH Tsevaot : Je me rappelle de ce qu'Amalek a fait à Israël, comment il s'opposa à lui sur le chemin, à sa sortie d'Égypte.
15:3	Va maintenant et frappe Amalek. Détruisez par interdit tout ce qui lui appartient. Ne l'épargne pas, mais fais mourir hommes et femmes, enfants et nourrissons, bœufs et petit bétail, chameaux et ânes.
15:4	Shaoul convoqua le peuple et en fit la revue à Thelaïm : il y avait 200 000 hommes de pied, et 10 000 hommes de Yéhouda.
15:5	Shaoul marcha jusqu'à la ville d'Amalek et mit une embuscade dans la vallée.
15:6	Et Shaoul dit aux Qeyniens : Allez, retirez-vous, séparez-vous des Amalécites, de peur que je ne vous détruise avec eux. En effet, vous avez agi avec bonté envers tous les fils d'Israël, quand ils montèrent d'Égypte. Et les Qeyniens se séparèrent des Amalécites.
15:7	Et Shaoul frappa les Amalécites depuis Haviylah jusqu'à Shour, qui est face à l'Égypte.
15:8	Il fit passer tout le peuple à bouche d’épée, le dévouant par interdit, mais il épargna Agag, roi d'Amalek.
15:9	Shaoul et le peuple épargnèrent Agag, les meilleures brebis, les bœufs, les bêtes grasses, les agneaux, ce qu'il y avait de meilleur. Ils ne voulurent pas les dévouer par interdit, détruisant seulement tout ce qui était chétif et méprisable.
15:10	La parole de YHWH apparut à Shemouél en disant :
15:11	Je me repens d'avoir fait Shaoul roi, car il s'est détourné de moi et n'a pas exécuté mes paroles. Shemouél fut très irrité, et il cria à YHWH toute la nuit.

### YHWH rejette Shaoul (Saül)

15:12	Shemouél se leva de bon matin pour aller rencontrer Shaoul. On informa Shemouél en disant : Shaoul est venu à Carmel, et voici, il s’est érigé un monument, puis il s’est tourné, a traversé et est descendu à Guilgal.
15:13	Shemouél se rendit auprès de Shaoul, et Shaoul lui dit : Sois béni de YHWH ! J'ai accompli la parole de YHWH.
15:14	Shemouél dit : Quel est ce bruit de brebis à mes oreilles, et ce bruit de bœufs que moi-même j’entends ?
15:15	Shaoul dit : Ils les ont fait venir de chez les Amalécites, car le peuple a épargné les meilleures brebis et les bœufs, pour les sacrifier à YHWH, ton Elohîm. Nous avons détruit le reste, nous l'avons dévoué par interdit.
15:16	Shemouél dit à Shaoul : Laisse-moi te déclarer ce que YHWH m'a dit cette nuit. Il lui dit : Parle !
15:17	Shemouél dit : Même si tu es petit à tes yeux, n’es-tu pas à la tête des tribus d’Israël, toi ? YHWH t'a oint pour roi sur Israël.
15:18	YHWH t'avait envoyé par un chemin et t'avait dit : Va et dévoue par interdit ces pécheurs, les Amalécites, et fais-leur la guerre jusqu'à ce qu'ils soient exterminés.
15:19	Pourquoi n'as-tu pas obéi à la voix de YHWH ? Pourquoi t'es-tu jeté sur le butin et as-tu fait ce qui est mal aux yeux de YHWH ?
15:20	Shaoul dit à Shemouél : Si ! j’ai obéi à la voix de YHWH et je suis allé par le chemin par lequel YHWH m'a envoyé : j'ai amené Agag, roi des Amalécites et j'ai dévoué les Amalécites, par interdit ;
15:21	mais le peuple a pris des brebis, des bœufs, du butin, les prémices de ce qui était voué à une entière destruction, pour le sacrifier à YHWH, ton Elohîm, à Guilgal.
15:22	Shemouél dit : YHWH prend-il plaisir aux holocaustes et aux sacrifices, autant qu'à l'obéissance à sa voix ? Voici, l'obéissance vaut mieux que les sacrifices, l'observation de sa parole vaut mieux que la graisse des béliers.
15:23	Car la rébellion est un péché de divination, et la résistance, c'est l'idolâtrie et les théraphim. Puisque tu as rejeté la parole de YHWH, il te rejette aussi afin que tu ne sois plus roi.
15:24	Et Shaoul dit à Shemouél : J'ai péché car j'ai transgressé la bouche de YHWH et tes paroles, car je craignais le peuple, et j'ai obéi à sa voix.
15:25	Maintenant, s'il te plaît, pardonne-moi mon péché, et reviens avec moi pour que je me prosterne devant YHWH.
15:26	Et Shemouél dit à Shaoul : Je n'irai pas avec toi : parce que tu as rejeté la parole de YHWH, YHWH te rejette afin que tu ne sois plus roi d'Israël.
15:27	Comme Shemouél se détournait pour s'en aller, il le saisit par le pan de son manteau qui se déchira.
15:28	Shemouél lui dit : YHWH déchire aujourd'hui le royaume d'Israël de dessus toi et le donne à ton compagnon, meilleur que toi.
15:29	En effet, le Puissant d'Israël ne ment pas, il ne se repent pas, car il n'est pas un être humain pour se repentir.
15:30	Et Shaoul dit : J'ai péché ! Honore-moi maintenant, s'il te plaît, en présence des anciens de mon peuple et en présence d'Israël, reviens avec moi et je me prosternerai devant YHWH, ton Elohîm.
15:31	Shemouél revint derrière Shaoul, et Shaoul se prosterna devant YHWH.
15:32	Shemouél dit : Amenez-moi Agag, roi d'Amalek ! Et Agag s'avança vers lui délicatement. Agag se disait : Certainement l'amertume de la mort est passée.
15:33	Shemouél dit : Comme ton épée a privé les femmes de leurs enfants, ainsi ta mère entre les femmes sera privée d'enfants. Et Shemouél mit Agag en pièces devant YHWH à Guilgal.
15:34	Il s'en alla à Ramah, et Shaoul monta dans sa maison à Guibea de Shaoul.
15:35	Shemouél n'alla plus voir Shaoul jusqu'au jour de sa mort. En effet, Shemouél pleurait sur Shaoul, parce que YHWH s'était repenti d'avoir fait Shaoul roi sur Israël.

## Chapitre 16

### Shemouél (Samuel) envoyé à Bethléhem pour oindre David

16:1	YHWH dit à Shemouél : Quand cesseras-tu de pleurer sur Shaoul ? Je l'ai rejeté afin qu'il ne règne plus sur Israël. Remplis ta corne d'huile et va ! Je t'enverrai chez Isaï, Bethléhémite, car j’ai vu parmi ses fils un roi pour moi.
16:2	Shemouél dit : Comment irai-je ? Car Shaoul l'apprendra et il me tuera. YHWH dit : Tu prendras en ta main une jeune vache du troupeau et tu diras : C'est pour sacrifier à YHWH que je suis venu.
16:3	Tu appelleras Isaï au sacrifice. Je te ferai savoir moi-même ce que tu auras à faire, et tu oindras pour moi celui que je te dirai.
16:4	Shemouél fit comme YHWH lui avait dit, et il alla à Bethléhem. Les anciens de la ville effrayés accoururent au-devant de lui et lui dirent : Ta venue est-elle la paix ?
16:5	Il dit : La paix ! Je suis venu pour sacrifier à YHWH. Sanctifiez-vous et venez avec moi au sacrifice. Il fit sanctifier aussi Isaï et ses fils, et les invita au sacrifice.
16:6	Il arriva que, comme ils entraient, il se dit en voyant Éliy'ab : Certainement le mashiah de YHWH est devant lui.
16:7	Mais YHWH dit à Shemouél : Ne regarde pas à son apparence ni à la hauteur de sa taille, car je l'ai rejeté. Il ne s'agit pas de ce que voient les humains. Car les humains voient de leurs yeux, mais YHWH voit le cœur.
16:8	Isaï appela Abinadab et le fit passer devant Shemouél, mais il dit : YHWH n'a pas non plus choisi celui-ci.
16:9	Isaï fit passer Shammah. Il dit : YHWH n'a pas non plus choisi celui-ci.
16:10	Isaï fit passer ses sept fils devant Shemouél. Shemouél dit à Isaï : YHWH n'a pas choisi ceux-ci.
16:11	Shemouél dit à Isaï : Sont-ce là tous tes garçons ? Et il dit : Il reste encore le plus jeune, et voici, il fait paître les brebis. Shemouél dit à Isaï : Envoie-le chercher, car nous ne nous installerons pas avant qu'il ne soit venu ici.
16:12	Il le fit venir. Il était roux, avec de beaux yeux et une belle apparence. YHWH dit : Lève-toi et oins-le, car c'est celui que j'ai choisi !
16:13	Shemouél prit la corne d'huile et l'oignit au milieu de ses frères. Et depuis ce jour-là, l'Esprit de YHWH fondit sur David. Et Shemouél se leva et s'en alla à Ramah.

### David entre au service de Shaoul (Saül)

16:14	L'Esprit de YHWH se retira de Shaoul, et un mauvais esprit<!--Shaoul (Saül) a été frappé d'un esprit d'égarement (2 Th. 2:9-12). Voir commentaires en Ge. 6:3 et Mt. 12:31.--> de YHWH le terrifiait.
16:15	Les serviteurs de Shaoul lui dirent : Voici maintenant, un mauvais esprit d'Elohîm te terrifie.
16:16	Que notre seigneur parle, s’il te plaît ! Tes serviteurs sont devant toi. Ils chercheront un homme sachant jouer de la harpe. Il arrivera que, quand le mauvais esprit d'Elohîm viendra sur toi, il jouera de sa main pour ton bien.
16:17	Shaoul dit à ses serviteurs : Cherchez-moi, s’il vous plaît, un homme qui joue bien et amenez-le-moi.
16:18	L'un des serviteurs répondit et dit : Voici, j'ai vu l'un des fils d'Isaï, le Bethléhémite, qui sait jouer des instruments, il est vaillant et talentueux, c'est un homme de guerre, à la parole intelligente, un bel homme, et YHWH est avec lui.
16:19	Shaoul envoya des messagers à Isaï, pour lui dire : Envoie-moi David, ton fils, qui est avec les brebis.
16:20	Isaï prit un âne avec du pain, une outre de vin et un jeune chevreau, qu'il envoya par la main de David, son fils, à Shaoul.
16:21	David arriva chez Shaoul et se tint debout en face de lui. Il l'aima beaucoup et il devint son porteur d’armes.
16:22	Shaoul envoya dire à Isaï : S'il te plaît, que David se tienne debout en face de moi, car il a trouvé grâce à mes yeux.
16:23	Il arrivait que quand le mauvais esprit d'Elohîm venait sur Shaoul, David prenait la harpe et en jouait de sa main. Shaoul respirait et se trouvait bien, et le mauvais esprit se retirait de dessus lui.

## Chapitre 17

### Goliath sème la terreur dans le camp d'Israël

17:1	Les Philistins réunirent leurs armées pour la bataille. Ils se rassemblèrent à Soco, qui appartient à Yéhouda. Ils campèrent entre Soco et Azéqah, à Éphès-Dammim.
17:2	Shaoul et les hommes d'Israël se rassemblèrent aussi. Ils campèrent dans la vallée du chêne et ils se rangèrent en bataille pour rencontrer les Philistins.
17:3	Les Philistins se tenaient sur la montagne de ce côté-ci et Israël se tenait sur la montagne de ce côté-là, et la vallée était entre eux.
17:4	Un homme, un champion sortit du camp des Philistins. Il se nommait Goliath, il était de Gath, et sa hauteur était de 6 coudées et un empan.
17:5	Il avait un casque de cuivre sur sa tête et était armé d'une cuirasse à écailles pesant 5 000 sicles de cuivre.
17:6	Il avait des jambières de cuivre sur ses jambes et un javelot de cuivre entre ses épaules.
17:7	Le bois de sa lance était comme une ensouple d'un tisserand, et la pointe de sa lance pesait 600 sicles de fer. Le porteur de son bouclier marchait devant lui.
17:8	Il se tint debout et appela les lignes de bataille d'Israël en leur disant : Pourquoi sortez-vous pour vous ranger en bataille ? Ne suis-je pas un Philistin, et vous pas esclaves de Shaoul ? Donnez à manger à un homme parmi vous, et il descendra contre moi.
17:9	S’il peut combattre avec moi et qu’il me tue, nous deviendrons vos esclaves. Si moi je le peux et le tue, vous deviendrez nos esclaves et vous nous servirez.
17:10	Le Philistin dit : Moi, je lance en ce jour un défi à la ligne de bataille d'Israël : donnez-moi un homme et nous combattrons ensemble.
17:11	Shaoul et tout Israël entendirent ces paroles du Philistin, ils furent épouvantés et eurent très peur.
17:12	Or David était le fils d'un homme éphratien de Bethléhem de Yéhouda nommé Isaï qui avait huit fils. Cet homme, aux jours de Shaoul, était vieux parmi les hommes.
17:13	Les trois fils aînés d'Isaï étaient allés à la bataille à la suite de Shaoul. Les noms de ses trois fils qui étaient allés à la bataille étaient : Éliy'ab, le premier-né, Abinadab, le second, et Shammah, le troisième.
17:14	David était le plus jeune, et les trois plus grands allèrent à la suite de Shaoul.
17:15	David allait et revenait d'auprès de Shaoul pour paître les brebis de son père à Bethléhem.
17:16	Le Philistin s'approchait matin et soir et il se présenta ainsi pendant 40 jours.

### David prêt à affronter Goliath

17:17	Isaï dit à David, son fils : S'il te plaît, prends pour tes frères un épha de ce blé rôti, ces 10 pains et porte-les promptement au camp, à tes frères.
17:18	Tu porteras aussi ces 10 fromages au chef de leur millier, tu t'informeras de la paix de tes frères et prendras d’eux un gage.
17:19	Or Shaoul, et eux, et tous les hommes d'Israël étaient dans la vallée du chêne, combattant contre les Philistins.
17:20	David se leva tôt le matin. Il laissa les brebis à un gardien, prit sa charge et s'en alla, comme son père Isaï le lui avait ordonné. Il arriva au retranchement où l'armée sortait vers la ligne de bataille et poussait des cris de guerre.
17:21	Israël et les Philistins se rangèrent ligne de bataille contre ligne de bataille.
17:22	David laissa les objets qu’il avait sur lui entre les mains du gardien des objets et courut vers la ligne de bataille. Quand il arriva, il s'informa sur la paix de ses frères.
17:23	Et comme il parlait avec eux, voici le champion, l'homme nommé Goliath, le Philistin de Gath, qui montait des lignes de bataille des Philistins, et il déclara les mêmes paroles, et David les entendit.
17:24	En voyant cet homme, tous les hommes d'Israël s'enfuirent devant lui, saisis d'une grande frayeur.
17:25	Les hommes d'Israël disaient : Avez-vous vu cet homme qui monte ? C'est pour lancer un défi à Israël qu'il monte. Mais l'homme qui le tuera, le roi l'enrichira de grandes richesses, il lui donnera sa fille et affranchira la maison de son père en Israël.
17:26	David parla aux hommes qui se tenaient debout près de lui, en disant : Quel bien fera-t-on à l'homme qui tuera ce Philistin et qui ôtera l'insulte de dessus Israël ? Car qui est ce Philistin, cet incirconcis, pour lancer un défi à la ligne de bataille d'Elohîm le Vivant ?
17:27	Le peuple lui parla selon cette parole et dit : C’est ainsi qu’on fera à l’homme qui le tuera.
17:28	Son frère aîné Éliy'ab l'entendit qui parlait à ces hommes et les narines d'Éliy'ab s'enflammèrent contre David et il lui dit : Pourquoi es-tu descendu, et à qui as-tu laissé ce peu de brebis dans le désert ? Je connais ton orgueil et la malice de ton cœur, car tu es descendu pour voir la bataille.
17:29	Et David dit : Qu'ai-je fait ? Ne puis-je pas parler ainsi ?
17:30	Il se détourna de lui vers un autre et lui dit la même parole. Le peuple lui rétorqua une parole semblable à la première parole.
17:31	Les paroles que David avait dites furent entendues et rapportées devant Shaoul qui le fit venir.
17:32	David dit à Shaoul : Que le cœur d'aucun homme ne soit abattu à cause de ce Philistin ! Ton serviteur ira et se battra contre lui.
17:33	Shaoul dit à David : Tu ne peux aller vers ce Philistin pour combattre avec lui, car tu n'es qu'un jeune homme, et il est un homme de guerre depuis sa jeunesse.
17:34	David dit à Shaoul : Ton serviteur faisait paître les brebis de son père. Quand un lion ou un ours venait emporter une brebis du troupeau,
17:35	je sortais après lui, je le frappais et je l'arrachais de sa gueule. S'il se jetait sur moi, je le saisissais par la mâchoire, je le frappais et je le tuais.
17:36	Même le lion, même l’ours, ton serviteur les a frappés. Ce Philistin, cet incirconcis, deviendra comme l'un d'eux, car il a déshonoré la ligne de bataille d'Elohîm le Vivant.
17:37	David dit encore : YHWH qui m'a délivré de la main du lion et de la main de l’ours me délivrera de la main de ce Philistin. Shaoul dit à David : Va, YHWH sera avec toi !

### David tue Goliath ; les Philistins sont battus

17:38	Shaoul habilla David de ses propres vêtements. Il mit sur sa tête un casque de cuivre, puis l'habilla d'une cuirasse.
17:39	David ceignit l'épée par-dessus ses vêtements et voulut marcher, car il ne l'avait jamais essayé. Et David dit à Shaoul : Je ne pourrais pas marcher avec ces choses, car je ne les ai pas essayées. David les enleva de dessus lui.
17:40	Il prit en main son bâton, et se choisit dans le torrent cinq pierres bien polies et les mit dans le sac de berger, dans sa poche et, sa fronde à la main, il s’approcha du Philistin.
17:41	Et le Philistin s’avança, allant et s’approchant de David, et, devant lui, l’homme qui portait son bouclier.
17:42	Le Philistin regarda, et lorsqu'il vit David, il le méprisa, car c'était un jeune homme, rouge avec une belle figure.
17:43	Le Philistin dit à David : Suis-je un chien, pour que tu viennes contre moi avec des bâtons ? Et le Philistin maudit David par ses elohîm.
17:44	Le Philistin dit à David : Viens vers moi, et je donnerai ta chair aux créatures volantes des cieux et aux bêtes des champs.
17:45	Et David dit au Philistin : Tu viens contre moi avec l'épée, la lance et le javelot, mais moi, je viens contre toi au Nom de YHWH Tsevaot, l'Elohîm des lignes de bataille d’Israël que tu as défié.
17:46	En ce jour, YHWH te livrera entre mes mains, je te frapperai et j’enlèverai ta tête de dessus toi. Je donnerai les cadavres du camp des Philistins aux créatures volantes des cieux et aux animaux de la terre en ce jour. Et toute la Terre saura qu'Israël a un Elohîm.
17:47	Et toute cette assemblée saura que YHWH ne sauve ni par l’épée ni par la lance, car la bataille est à YHWH, qui vous livrera entre nos mains.
17:48	Et il arriva que comme le Philistin se levait et s'avançait à la rencontre de David, David se hâta de courir vers la ligne de bataille à la rencontre du Philistin.
17:49	David mit la main dans son sac, y prit une pierre et la lança avec sa fronde. Il frappa tellement le Philistin au front que la pierre s'enfonça dans son front. Il tomba les faces contre terre.
17:50	Ainsi avec une fronde et une pierre, David fut plus fort que le Philistin. Il frappa le Philistin et le tua. David n’avait pas d’épée en sa main.
17:51	David courut, se jeta sur le Philistin, prit son épée, la tira de son fourreau, le tua, et lui coupa la tête. Les Philistins, voyant que leur homme vaillant était mort, prirent la fuite.
17:52	Les hommes d'Israël et de Yéhouda se levèrent, poussèrent des cris de guerre et poursuivirent les Philistins jusqu'à la vallée et jusqu'aux portes d'Ékron. Les Philistins blessés à mort tombèrent dans le chemin de Shaaraïm, jusqu'à Gath et jusqu'à Ékron.
17:53	Les fils d'Israël revinrent de la poursuite des Philistins et pillèrent leurs camps.
17:54	David prit la tête du Philistin et l'amena à Yeroushalaim, et ses armes, il les mit dans sa tente.
17:55	Quand Shaoul vit David sortant à la rencontre du Philistin, il dit à Abner, chef de l'armée : Abner, de qui ce jeune homme est-il le fils ? Abner dit : Que ton âme vive, roi ! Je n'en sais rien.
17:56	Le roi lui dit : Informe-toi de qui ce jeune homme est le fils.
17:57	Quand David revint après avoir tué le Philistin, Abner le prit et l'amena devant Shaoul, avec la tête du Philistin à la main.
17:58	Shaoul lui dit : Jeune homme, de qui es-tu le fils ? David dit : Je suis le fils d'Isaï, Bethléhémite, ton serviteur.

## Chapitre 18

### Alliance entre Yehonathan (Yonathan) et David

18:1	Or il arriva qu'aussitôt qu'il eut achevé de parler à Shaoul, l'âme de Yehonathan fut attachée à l'âme de David. Yehonathan l'aima comme son âme.
18:2	Ce jour-là Shaoul le retint, et ne lui permit plus de retourner à la maison de son père.
18:3	Yehonathan fit alliance avec David, parce qu'il l'aimait comme son âme.
18:4	Yehonathan se dépouilla de la robe qu'il portait et la donna à David, avec ses habits, jusqu'à son épée, son arc et sa ceinture.

### Shaoul (Saül) est jaloux de David et cherche à le tuer

18:5	David allait partout où l'envoyait Shaoul et il prospérait, de sorte que Shaoul le mit à la tête des hommes de guerre. Il était agréable aux yeux de tout le peuple et même aux yeux des serviteurs de Shaoul.
18:6	Or il arriva que, comme ils rentraient, lors du retour de David après qu'il eut tué le Philistin, des femmes sortirent de toutes les villes d'Israël, en chantant et dansant devant le roi Shaoul, avec des tambourins, des triangles et en poussant des cris de joie.
18:7	Les femmes qui jouaient répondaient et disaient : Shaoul a frappé ses 1 000, et David ses 10 000.
18:8	Shaoul fut très irrité car cette parole fut mauvaise à ses yeux. Il dit : Elles en ont donné 10 000 à David, et à moi 1 000 ! Il ne lui manque plus que le royaume.
18:9	Il arriva, à partir de ce jour et dans la suite, que Shaoul regardait David méchamment.
18:10	Il arriva, le lendemain, qu'un mauvais esprit d'Elohîm fondit sur Shaoul, et il prophétisa au milieu de la maison. David jouait de sa main comme les autres jours et Shaoul avait une lance dans sa main.
18:11	Shaoul jeta sa lance, se disant : Je frapperai David contre le mur. Mais David se détourna en face de lui par deux fois.
18:12	Shaoul avait peur en face de David, parce que YHWH était avec David, et qu'il s'était retiré de Shaoul.
18:13	Shaoul l’éloigna de lui et l'établit chef de mille. Il allait et venait devant le peuple.
18:14	Il arriva que David prospérait dans toutes ses voies<!--Voir Jos. 1:8.-->, car YHWH était avec lui.
18:15	Shaoul, voyant que David prospérait beaucoup, avait peur en face de lui.
18:16	Tout Israël et Yéhouda aimaient David, parce qu'il allait et venait devant eux.

### David épouse Miykal, fille de Shaoul (Saül)

18:17	Shaoul dit à David : Voici, je te donnerai Mérab ma fille aînée pour femme. Mais deviens pour moi un fils talentueux et conduis les guerres de YHWH. Car Shaoul disait : Que ma main ne le touche pas, mais que ce soit la main des Philistins.
18:18	David dit à Shaoul : Qui suis-je, et quelle est ma vie, et la famille de mon père en Israël, pour que je devienne le gendre du roi ?
18:19	Il arriva qu'au temps où l'on devait donner Mérab, fille de Shaoul, à David, elle fut donnée pour femme à Adriel de Meholah.
18:20	Or Miykal, fille de Shaoul, aima David. On en informa Shaoul, et la chose fut bonne à ses yeux.
18:21	Shaoul dit : Je la lui donnerai, afin qu'elle devienne pour lui un piège et que la main des Philistins vienne sur lui. Shaoul dit à David pour la seconde fois : Tu deviendras aujourd'hui mon gendre.
18:22	Et Shaoul ordonna à ses serviteurs de parler à David en secret et de lui dire : Voici, le roi prend plaisir en toi, et tous ses serviteurs t'aiment ; deviens maintenant le gendre du roi.
18:23	Les serviteurs de Shaoul dirent ces paroles aux oreilles de David, et David dit : Est-ce peu de chose à vos yeux que de devenir le gendre du roi ? Moi, je suis un homme pauvre et de peu d'importance. 
18:24	Les serviteurs de Shaoul le lui rapportèrent en disant : David a tenu tel discours.
18:25	Shaoul dit : Vous parlerez ainsi à David : Le roi ne désire pas de dot, mais 100 prépuces de Philistins, afin d'être vengé de ses ennemis. Or Shaoul comptait faire tomber David entre les mains des Philistins.
18:26	Les serviteurs de Shaoul rapportèrent tous ces discours à David, et le discours fut juste aux yeux de David de devenir le gendre du roi. Et les jours n’étaient pas accomplis,
18:27	que David se leva et s'en alla, lui et ses hommes, et tua 200 hommes parmi les Philistins. Il apporta leurs prépuces, et on les livra au complet au roi, afin qu'il devienne le gendre du roi. Shaoul lui donna pour femme Miykal, sa fille.
18:28	Shaoul vit et comprit que YHWH était avec David et que Miykal, fille de Shaoul, l'aimait.
18:29	Shaoul eut encore plus peur en face de David et Shaoul devint l'ennemi de David pour toujours.
18:30	Les chefs des Philistins faisaient souvent des sorties. Il arrivait que, chaque fois qu’ils sortaient, David prospérait plus que tous les serviteurs de Shaoul, et son nom fut très estimé.

## Chapitre 19

### David échappe aux assauts de Shaoul (Saül)

19:1	Shaoul parla à Yonathan, son fils, et à tous ses serviteurs de faire mourir David. Mais Yehonathan<!--Voir commentaire en 1 S. 13:2.-->, fils de Shaoul, prenait beaucoup plaisir<!--Le roi Shaoul aussi prenait plaisir en David. 1 S. 18:22. YHWH, de même prenait plaisir en David. 2 S. 15:26, 22:20. Voir 2 S. 20:11.--> en David.
19:2	Yehonathan informa David en disant : Shaoul, mon père, cherche à te faire mourir. Maintenant, s’il te plaît, tiens-toi sur tes gardes jusqu'au matin, demeure dans un lieu secret et cache-toi.
19:3	Pour moi, je sortirai et je me tiendrai à côté de mon père dans le champ où tu seras. Je parlerai de toi à mon père. Je verrai ce qu'il en est et je t’en informerai.
19:4	Yehonathan parla favorablement de David à Shaoul, son père, et lui dit : Que le roi ne pèche pas contre son serviteur David, car il n'a pas péché contre toi. Au contraire, ses actions ont été très bonnes à ton égard :
19:5	il a mis son âme dans sa paume, il a tué le Philistin et YHWH a opéré une grande délivrance pour tout Israël. Tu l'as vu et tu t'en es réjoui. Pourquoi pécherais-tu contre le sang innocent en faisant mourir David sans cause ?
19:6	Shaoul écouta la voix de Yehonathan et Shaoul jura : YHWH est vivant ! Il ne mourra pas.
19:7	Yehonathan appela David, et Yehonathan lui rapporta toutes ces choses. Yehonathan l'introduisit auprès de Shaoul, et il fut à son service comme hier et avant-hier.
19:8	Il y eut de nouveau la guerre. David sortit pour combattre les Philistins. Il les frappa à grand coup et ils s'enfuirent.
19:9	Or le mauvais esprit de YHWH vint sur Shaoul : comme il était assis dans sa maison, sa lance à la main, et que David jouait de sa main,
19:10	Shaoul chercha à frapper de sa lance David et le mur, mais il se détourna en face de Shaoul, qui, de sa lance, frappa le mur. David s'enfuit et se sauva cette nuit-là.
19:11	Shaoul envoya des messagers à la maison de David pour le garder et le faire mourir au matin. Miykal, femme de David, l'en informa en disant : Si tu ne sauves pas ton âme cette nuit, demain on te fera mourir.
19:12	Miykal fit descendre David par une fenêtre, et ainsi il s'en alla et se sauva.
19:13	Miykal prit un théraphim, qu'elle plaça dans le lit. Elle mit une peau de chèvre à son chevet et l'enveloppa d'une couverture.
19:14	Lorsque Shaoul envoya des messagers pour prendre David, elle dit : Il est malade.
19:15	Shaoul envoya encore des messagers pour prendre David, en disant : Apportez-le-moi dans son lit, afin que je le fasse mourir.
19:16	Ces messagers entrèrent, et voici, un théraphim était au lit, et la peau de chèvre à son chevet.
19:17	Shaoul dit à Miykal : Pourquoi m'as-tu trompé de la sorte et as-tu laissé aller mon ennemi, de sorte qu'il s'est sauvé ? Miykal dit à Shaoul : Il m'a dit : Laisse-moi aller, pourquoi te tuerais-je ?
19:18	Et David s’enfuit et se sauva. Il se rendit auprès de Shemouél à Ramah, et lui raconta tout ce que Shaoul lui avait fait. Il s'en alla avec Shemouél, et ils demeurèrent à Nayoth.
19:19	On informa Shaoul en disant : Voici, David est à Nayoth, en Ramah.
19:20	Shaoul envoya des messagers pour s'emparer de David. Ils virent une assemblée de prophètes qui prophétisaient, et Shemouél était debout, présidant sur eux. L'Esprit d'Elohîm saisit les messagers de Shaoul, qui prophétisèrent aussi.
19:21	Shaoul, informé, envoya d'autres messagers, et eux aussi prophétisèrent. Shaoul en envoya encore pour la troisième fois, et ils prophétisèrent aussi.
19:22	Il alla lui-même à Ramah, et vint jusqu'à la grande citerne qui est à Sécou, il s'informa en disant : Où sont Shemouél et David ? On lui dit : Ils sont à Nayoth, à Ramah.
19:23	Il va là, à Nayoth, à Ramah, et l'Esprit d'Elohîm vint sur lui aussi, et il allait, il allait en prophétisant jusqu'à son arrivée à Nayoth, à Ramah.
19:24	Il se dépouilla lui aussi de ses vêtements et prophétisa devant Shemouél. Il se jeta à terre nu, tout ce jour-là et toute la nuit. C'est pourquoi on dit : Shaoul est-il aussi parmi les prophètes ?

## Chapitre 20

### Renouvellement de l'alliance entre David et Yehonathan (Yonathan)

20:1	David s'enfuit de Nayoth-de-Ramah et vint dire en face de Yehonathan : Qu'ai-je fait ? Quelle est mon iniquité, et quel est mon péché en face de ton père, pour qu'il cherche mon âme ?
20:2	Il<!--Yehonathan.--> lui dit : Loin de là ! Tu ne mourras pas. Voici, mon père ne fait ni grande chose ni petite chose, sans la découvrir à mon oreille. Pourquoi mon père me cacherait-il cette chose-là ? Il n'en est rien.
20:3	David dit encore en jurant : Ton père sait, il le sait que j'ai trouvé grâce à tes yeux et il s’est dit : Que Yehonathan ne sache rien de ceci, de peur qu'il n'en soit attristé. Mais YHWH est vivant et ton âme est vivante ! Il n'y a qu'un pas entre moi et la mort.
20:4	Yehonathan dit à David : Ce que ton âme dira, je le ferai pour toi.
20:5	David dit à Yehonathan : Voici, c'est demain la nouvelle lune et je m'assiérai, je m'assiérai avec le roi pour manger. Tu me renverras et je me cacherai dans les champs jusqu’au troisième soir.
20:6	Si ton père me cherche, s'il me cherche tu diras : David m'a demandé la permission de courir à Bethléhem, sa ville, parce que toute sa famille fait un sacrifice annuel.
20:7	S'il dit ainsi : C'est bon ! Il y a paix pour ton serviteur. Mais s'il se fâche, s'il se fâche, sache qu'il a résolu mon malheur.
20:8	Use de bonté envers ton serviteur, car c’est dans une alliance de YHWH que tu as fait entrer avec toi ton serviteur. S'il y a de l'iniquité en moi, tue-moi toi-même, car pourquoi me ferais-tu entrer jusqu'à ton père ?
20:9	Yehonathan lui dit : Loin de moi ! Car si je savais, si je savais que mon père est décidé à faire venir sur toi un malheur, ne t'en informerais-je pas ?
20:10	David dit à Yehonathan : Qui m’informera si ton père te répond durement ?
20:11	Et Yehonathan dit à David : Viens et sortons dans les champs. Ils sortirent eux deux dans les champs.
20:12	Yehonathan dit à David : Par YHWH, l'Elohîm d'Israël ! Je sonderai mon père en ce temps, demain, pour la troisième fois, et s'il est favorable envers David, et que je n'envoie personne vers toi pour t'en informer,
20:13	qu'ainsi YHWH traite Yehonathan et qu'ainsi il y ajoute ! Si mon père trouve bon de te faire du mal, je découvrirai ton oreille, je te renverrai et tu t'en iras en paix. YHWH sera avec toi comme il a été avec mon père.
20:14	Sinon, si je suis encore vivant, tu useras de la bonté de YHWH envers moi, pour que je ne meure pas.
20:15	Ne retranche jamais ta bonté de ma maison, pas même quand YHWH retranchera les ennemis de David, chaque homme de dessus les faces du sol.
20:16	Yehonathan traita alliance avec la maison de David, en disant : Que YHWH le redemande de la main des ennemis de David !
20:17	Yehonathan fit encore jurer David à cause de son amour pour lui, car il l'aimait comme son âme.
20:18	Yehonathan lui dit : C'est demain la nouvelle lune, et on te cherchera, car ta place sera vide.
20:19	Le troisième jour, au soir, tu descendras en hâte, jusqu'au fond du lieu où tu t'étais caché le jour de cette affaire, et tu resteras près de la pierre d'Ézel.
20:20	Moi, je tirerai trois flèches de ce côté, comme si je les envoyais sur une cible.
20:21	Et voici, j'enverrai un garçon : Va, trouve les flèches ! Si je dis, si je dis au garçon : Voici, les flèches sont au-deçà de toi, prends-les et viens, car la paix est avec toi, pas de parole, YHWH est vivant !
20:22	Mais si je dis ainsi au jeune homme : Voici, les flèches sont au-delà de toi ! Alors va-t'en, car YHWH te renvoie.
20:23	Quant à la parole que nous nous sommes déclarée, toi et moi, voici, YHWH est entre moi et toi, à jamais.

### Shaoul (Saül) en colère contre Yehonathan (Yonathan)

20:24	David se cacha dans le champ. La nouvelle lune étant venue, le roi s'assit pour prendre son repas.
20:25	Le roi s’assit sur son siège, comme les autres fois, sur le siège près du mur. Yehonathan se leva, et Abner s'assit à côté de Shaoul. La place de David resta vide.
20:26	Shaoul ne dit rien ce jour-là, car il se disait : C'est un accident, il n'est pas pur, certainement il n'est pas pur.
20:27	Et il arriva, le lendemain de la nouvelle lune, le second jour, que la place de David resta vide. Et Shaoul dit à Yehonathan, son fils : Pourquoi le fils d'Isaï n'a-t-il été ni hier ni aujourd'hui au repas ?
20:28	Et Yehonathan répondit à Shaoul : David m'a demandé, il m'a demandé la permission d'aller jusqu’à Bethléhem.
20:29	Il m'a dit : S'il te plaît, laisse-moi aller, car notre famille fait un sacrifice dans la ville et mon frère me l'a ordonné. Maintenant si j'ai trouvé grâce à tes yeux, s'il te plaît, je me sauverai et je verrai mes frères. C'est pour cela qu'il n'est pas venu à la table du roi.
20:30	Les narines de Shaoul s'enflammèrent contre Yehonathan et il lui dit : Fils déformé et rebelle, ne sais-je pas que tu as choisi le fils d'Isaï à ta honte et à la honte de la nudité de ta mère ?
20:31	Car aussi longtemps que le fils d'Isaï vivra sur le sol, tu ne seras pas stable, ni toi, ni ta royauté. Maintenant, envoie quelqu'un le prendre et amène-le moi, car c'est un fils de la mort.
20:32	Yehonathan répondit à Shaoul son père et lui dit : Pourquoi le ferait-on mourir ? Qu'a-t-il fait ?
20:33	Et Shaoul lança sa lance contre lui pour le frapper. Yehonathan sut que son père avait résolu la mort de David.
20:34	Yehonathan se leva de table, enflammé de narines. Il ne mangea pas le pain le deuxième jour de la nouvelle lune, car il était affligé à cause de David, parce que son père l'avait insulté.
20:35	Et il arriva, au matin, que Yehonathan sortit dans les champs, au lieu convenu avec David, avec lui un petit garçon.
20:36	Il dit à son garçon : Cours, s’il te plaît, trouve les flèches que je tire. Le garçon courut, et il<!--Yehonathan.--> tira une flèche qui le dépassa.
20:37	Lorsque le garçon arriva au lieu où était la flèche que Yehonathan avait tirée, Yehonathan cria après lui et lui dit : La flèche n'est-elle pas plus loin de toi ?
20:38	Yehonathan cria encore après le garçon : Hâte-toi, ne t'arrête pas ! Et le garçon de Yehonathan ramassa les flèches et revint vers son maître.
20:39	Le garçon ne savait rien de cette affaire, car seuls David et Yehonathan le savaient.
20:40	Yehonathan remit ses armes au garçon et lui dit : Va, porte-les à la ville.
20:41	Le garçon parti, David se leva du côté du sud, se jeta le visage contre terre et se prosterna à trois reprises. Les deux amis s'embrassèrent et pleurèrent ensemble, David versa d'abondantes larmes.
20:42	Yehonathan dit à David : Va en paix, comme nous l'avons juré au Nom de YHWH, en disant : Que YHWH soit entre moi et toi, entre ma postérité et ta postérité.

## Chapitre 21

### David et Achiymélek le prêtre

21:1	David se leva, s'en alla, et Yehonathan rentra dans la ville.
21:2	David se rendit à Nob, vers Achiymélek, le prêtre, qui tout effrayé courut au-devant de David, et lui dit : Pourquoi es-tu seul, sans homme avec toi ? 
21:3	David dit à Achiymélek, le prêtre : Le roi m'a donné un ordre à propos d'une affaire et m'a dit : Que personne ne sache rien de l'affaire pour laquelle je t'envoie, ni de l'ordre que je t'ai donné. J'ai donné rendez-vous à mes jeunes hommes en un certain lieu.
21:4	Maintenant qu'as-tu sous la main ? Donne-moi en main cinq pains ou ce qui s'y trouvera.
21:5	Le prêtre répondit à David et dit : Je n'ai pas de pain profane sous la main, il n'y a que du pain sacré<!--Mt. 12:4.-->, pourvu que tes jeunes hommes se soient abstenus de femmes !
21:6	David répondit au prêtre et lui dit : Nous nous sommes abstenus de femmes depuis trois jours que je suis parti. Si les armes de mes jeunes hommes sont consacrées pour un voyage profane, à plus forte raison aujourd'hui sont-ils tous consacrés avec leurs armes.
21:7	Le prêtre lui donna du pain sacré, car il n'y avait pas là d’autre pain que le pain des faces qui était écarté en face de YHWH, pour le remplacer par du pain chaud le jour où on l'avait pris.
21:8	Or il y avait là un homme d'entre les serviteurs de Shaoul, retenu ce jour-là devant YHWH. Il s'appelait Doëg, un Édomite, le plus puissant de tous les bergers de Shaoul.
21:9	David dit à Achiymélek : Mais n'as-tu pas ici sous la main quelque lance ou quelque épée ? Car je n'ai pris dans ma main ni mon épée ni mes armes, parce que l’affaire du roi était urgente.
21:10	Et le prêtre dit : Voici l'épée de Goliath, le Philistin, que tu as tué dans la vallée du chêne. Elle est enveloppée d'un drap, derrière l'éphod. Si tu veux la prendre pour toi, prends-la, car il n'y en a pas ici d'autres que celle-là. Et David dit : Il n'y en a pas de pareille. Donne-la-moi !

### David s'enfuit à Gath

21:11	David se leva, et s'enfuit ce jour-là en face de Shaoul, et s'en alla vers Akish, roi de Gath.
21:12	Et les serviteurs d'Akish lui dirent : N'est-ce pas là David, roi de la terre ? N'est-ce pas celui dont on chantait et répondait en dansant : Shaoul a tué ses 1 000, et David ses 10 000 ?
21:13	David mit ces paroles dans son cœur et eut une grande peur en face d'Akish, roi de Gath.
21:14	Il altéra son goût<!--Ps. 34:1.--> sous leurs yeux et se conduisit entre leurs mains comme un fou. Il faisait des marques sur les battants des portes et laissait couler sa salive sur sa barbe.
21:15	Akish dit à ses serviteurs : Ne voyez-vous pas que cet homme est fou ? Pourquoi me l'avez-vous amené ?
21:16	Est-ce que je manque de fous, pour que vous m'ameniez celui-ci pour faire le fou devant moi ? Faudrait-il que cet homme entre dans ma maison ?

## Chapitre 22

### David se réfugie dans la caverne d'Adoullam<!--1 Ch. 12:17-19.-->

22:1	David partit de là et se sauva dans la caverne d'Adoullam. Ses frères et toute la maison de son père l'ayant appris, ils descendirent vers lui.
22:2	Alors se rassemblèrent autour de lui tout homme en détresse, tout homme ayant des créanciers, tout homme ayant l'âme pleine d'amertume, et il devint leur chef. Il y eut avec lui environ 400 hommes.
22:3	David s'en alla de là à Mitspé de Moab. Il dit au roi de Moab : Que mon père et ma mère, s’il te plaît, restent chez vous jusqu’à ce que je sache ce qu'Elohîm fera de moi.
22:4	Il les amena devant le roi de Moab, et ils demeurèrent chez lui, tout le temps que David fut dans cette forteresse.
22:5	Or Gad, le prophète, dit à David : Ne reste pas dans cette forteresse, va-t'en et entre en terre de Yéhouda. David s'en alla et vint dans la forêt de Héreth.

### Shaoul (Saül) fait tuer les prêtres

22:6	Shaoul apprit qu'on avait découvert David et ses hommes. Or Shaoul était assis sous le tamaris, à Guibea, sur la hauteur. Il avait sa lance à la main et tous ses serviteurs se tenaient près de lui.
22:7	Et Shaoul dit à ses serviteurs qui se tenaient près de lui : Écoutez, s’il vous plaît, Benyamites ! Le fils d'Isaï vous donnera-t-il à vous tous des champs et des vignes ? Vous établira-t-il tous chefs de mille et chefs de cent ?
22:8	Oui, vous avez tous conspiré contre moi, et personne ne découvre mon oreille quand mon fils traite une alliance avec le fils d'Isaï. Nul parmi vous n'est affligé pour moi, nul ne découvre mon oreille. Oui, mon fils a dressé mon serviteur contre moi afin qu’il me dresse des embuscades, ainsi qu’il en est en ce jour.
22:9	Doëg, l'Édomite, qui était établi sur les serviteurs de Shaoul, répondit et dit : J'ai vu le fils d'Isaï venir à Nob, auprès d'Achiymélek, fils d'Ahitoub.
22:10	Il a consulté YHWH pour lui, il lui a donné des provisions ainsi que l'épée de Goliath, le Philistin.
22:11	Le roi envoya appeler Achiymélek, le prêtre, fils d'Ahitoub, la maison de son père, et les prêtres qui étaient à Nob. Et ils vinrent tous vers le roi.
22:12	Shaoul dit : Écoute, s’il te plaît, fils d'Ahitoub ! Et il dit : Me voici, mon seigneur !
22:13	Shaoul lui dit : Pourquoi avez-vous conspiré contre moi, toi et le fils d'Isaï ? Tu lui as donné du pain et une épée. Tu as consulté Elohîm pour lui, afin qu'il se dresse contre moi et qu'il me dresse des embuscades, ainsi qu’il en est en ce jour.
22:14	Achiymélek répondit au roi et dit : Y a-t-il, parmi tous tes serviteurs, quelqu'un d'aussi fidèle que David ? Il est le gendre du roi, il est parti sur ton ordre<!--« Sujets », « garde du corps », « auditeurs », « obéissants ».-->, il est honoré dans ta maison.
22:15	Est-ce aujourd'hui que j'ai commencé à consulter Elohîm pour lui ? Loin de moi ! Que le roi n’impute aucune chose à son serviteur, à personne de la maison de mon père, car de tout ceci ton serviteur ne connaît aucune chose ni petite ni grande.
22:16	Le roi lui dit : Mourir, tu mourras<!--Le mot est répété deux fois. Voir commentaire en Ge. 2:16.-->, Achiymélek, toi et toute la maison de ton père.
22:17	Le roi dit aux coureurs qui se tenaient près de lui : Tournez-vous et tuez les prêtres de YHWH, parce que leur main aussi est avec David et parce qu’ils savaient qu’il fuyait et qu’ils ne l’ont pas révélé à mon oreille. Mais les serviteurs du roi ne voulurent pas étendre leurs mains pour attaquer les prêtres de YHWH.
22:18	Le roi dit à Doëg : Tourne-toi et attaque les prêtres ! Et Doëg, l'Édomite, se tourna et attaqua les prêtres. Il tua en ce jour-là 85 hommes qui portaient l'éphod en lin.
22:19	Il frappa encore à bouche d’épée Nob, la ville des prêtres, hommes et femmes, enfants et nourrissons, bœufs, ânes et brebis, tombèrent sous le tranchant de l'épée.
22:20	Un fils d'Achiymélek, fils d'Ahitoub, dont le nom était Abiathar, se sauva et s'enfuit auprès de David.
22:21	Abiathar rapporta à David que Shaoul avait tué les prêtres de YHWH.
22:22	David dit à Abiathar : Doëg, l'Édomite, était présent ce jour-là, et je savais bien qu'il raconterait, qu'il raconterait tout à Shaoul. C'est moi qui ai fait du tort à toutes les âmes de la maison de ton père.
22:23	Reste avec moi, ne crains rien, car celui qui cherche mon âme cherche ton âme, car avec moi tu seras en dépôt.

## Chapitre 23

### David libère Qe'iylah (Keïla)

23:1	On fit ce rapport à David, en disant : Voici, les Philistins font la guerre à Qe'iylah, et pillent les aires de battage.
23:2	David consulta YHWH<!--La clé du succès de David était YHWH. Il consultait régulièrement Elohîm avant de s'engager dans une guerre (Ps. 60:14.).--> en disant : Irai-je et frapperai-je ces Philistins ? YHWH dit à David : Va et tu frapperas les Philistins, et tu délivreras Qe'iylah.
23:3	Les hommes de David lui dirent : Voici, nous avons peur ici en Yéhouda. Que sera-ce quand nous irons à Qe'iylah contre les lignes de bataille des Philistins ?
23:4	David consulta encore YHWH, et YHWH lui répondit et dit : Lève-toi, descends à Qe'iylah, car je vais livrer les Philistins entre tes mains.
23:5	David s'en alla avec ses hommes à Qe'iylah, et combattit contre les Philistins. Il emmena leur bétail et leur infligea une grande défaite. C'est ainsi que David délivra les habitants de Qe'iylah.
23:6	Or il était arrivé que quand Abiathar, fils d'Achiymélek, s'était enfui vers David à Qe'iylah, il avait en main l'éphod.
23:7	On rapporta à Shaoul que David était venu à Qe'iylah et Shaoul dit : Elohîm l'a livré entre mes mains car il s'est enfermé en entrant dans une ville qui a des portes et des barres.
23:8	Shaoul convoqua tout le peuple à la guerre, afin de descendre à Qe'iylah et d'assiéger David et ses hommes.
23:9	David, ayant eu connaissance des mauvais desseins de Shaoul à son égard, dit au prêtre Abiathar : Apporte l'éphod !
23:10	David dit : YHWH, Elohîm d'Israël, ton serviteur a entendu, il a entendu que Shaoul cherche à venir à Qe'iylah pour détruire la ville à cause de moi.
23:11	Les chefs de Qe'iylah me livreront-ils entre ses mains ? Shaoul descendra-t-il comme ton serviteur l'a entendu dire ? YHWH, Elohîm d'Israël, s'il te plaît, informe ton serviteur. YHWH dit : Il descendra.
23:12	David dit encore : Les chefs de Qe'iylah me livreront-ils, moi et mes hommes, entre les mains de Shaoul ? YHWH dit : Ils te livreront.

### David échappe encore à Shaoul (Saül)

23:13	David se leva avec ses hommes au nombre d'environ 600 hommes. Ils sortirent de Qe'iylah et s’en allèrent où ils s’en allèrent. On rapporta à Shaoul que David s'était sauvé de Qe'iylah, et il cessa de sortir.
23:14	David resta dans le désert, dans des lieux forts, et il se tint sur la montagne au désert de Ziyph. Et Shaoul le cherchait tous les jours, mais Elohîm ne le livra pas entre ses mains.
23:15	David vit que Shaoul était sorti pour chercher son âme. David se tenait dans le désert de Ziyph, dans la forêt.
23:16	Yehonathan, fils de Shaoul, se leva et s'en alla dans la forêt vers David. Il fortifia ses mains en Elohîm,
23:17	et lui dit : N'aie pas peur, car la main de Shaoul mon père ne t'atteindra pas. Mais tu régneras sur Israël, et moi je serai le second après toi. Mon père Shaoul le sait bien aussi.
23:18	Ils firent tous les deux alliance devant YHWH. David resta dans la forêt, mais Yehonathan retourna dans sa maison.
23:19	Or les Ziyphiens montèrent auprès de Shaoul à Guibea et lui dirent : David ne se tient-il pas caché parmi nous dans des lieux forts, dans la forêt, sur la colline de Hakila, qui est au sud du désert ?
23:20	Maintenant, roi, puisque tout le désir de ton âme est de descendre, descends, et ce sera à nous de le livrer entre les mains du roi.
23:21	Et Shaoul dit : Que YHWH vous bénisse de ce que vous avez eu pitié de moi !
23:22	Allez, s'il vous plaît, assurez-vous encore davantage, connaissez et voyez son lieu, où sera son pied, qui l’a vu là ? Car on m’a dit qu’il est très rusé.
23:23	Examinez et reconnaissez tous les lieux où il se tient caché, puis retournez vers moi quand vous en serez assurés, et j'irai avec vous. S'il est sur la terre, je le chercherai soigneusement parmi tous les milliers de Yéhouda.
23:24	Ils se levèrent et s'en allèrent à Ziyph avant Shaoul. David et ses hommes étaient dans le désert de Maon, dans la région aride, au sud du désert<!--Ps. 63:1.-->.
23:25	Shaoul et ses hommes partirent à la recherche de David. Et l'on en informa David, qui descendit le rocher et resta dans le désert de Maon. Shaoul, l'ayant appris, poursuivit David dans le désert de Maon.
23:26	Shaoul marchait d'un côté de la montagne, et David et ses hommes de l'autre côté de la montagne. David fuyait précipitamment pour échapper à Shaoul. Mais Shaoul et ses hommes entouraient David et ses hommes pour s'emparer d'eux,
23:27	lorsqu'un messager vint à Shaoul, en disant : Hâte-toi de venir, car les Philistins envahissent la terre !
23:28	Alors Shaoul s’en retourna de la poursuite de David et marcha au-devant des Philistins. C'est pourquoi on appela ce lieu Séla-Hammachlekoth.

## Chapitre 24

### David épargne la vie de Shaoul (Saül) à En-Guédi

24:1	David monta de là et demeura dans les lieux forts d'En-Guédi.
24:2	Et il arriva que, quand Shaoul fut revenu de la poursuite des Philistins, on lui fit ce rapport, en disant : Voilà David dans le désert d'En-Guédi.
24:3	Shaoul prit 3 000 hommes sélectionnés sur tout Israël et il s'en alla chercher David et ses hommes jusque sur le rocher des chèvres de montagne.
24:4	Shaoul arriva aux bergeries de brebis qui étaient près du chemin, où il y avait une caverne dans laquelle il entra pour se couvrir les pieds. David et ses hommes se tenaient au fond de la caverne.
24:5	Les hommes de David lui dirent : Voici le jour où YHWH te dit : Je te livre ton ennemi entre tes mains, afin que tu le traites comme cela paraîtra bon à tes yeux. David se leva et coupa tout doucement la frange de la robe de Shaoul<!--1 S. 15:28 ; 1 S. 28:17.-->.
24:6	Et il arriva, après cela, que le cœur de David battit, parce qu'il avait coupé la frange de Shaoul.
24:7	Et il dit à ses hommes : Que YHWH me garde de commettre une telle action contre mon seigneur, le mashiah de YHWH, en mettant ma main sur lui ! Car il est le mashiah de YHWH<!--David épargna Shaoul parce qu'il faisait confiance à YHWH. Il laissa Elohîm agir plutôt que d'agir par lui-même.-->.
24:8	David détourna ses hommes par ses paroles, et il ne leur permit pas de s'élever contre Shaoul. Puis Shaoul se leva de la caverne et poursuivit son chemin.
24:9	Après cela, David se leva, sortit de la caverne, et cria après Shaoul, en disant : Mon seigneur, le roi ! Shaoul regarda derrière lui, et David s'inclina le visage contre terre et se prosterna.
24:10	David dit à Shaoul : Pourquoi écouterais-tu les paroles des hommes qui te disent : Voici, David cherche ton malheur ?
24:11	Aujourd'hui, tes yeux ont vu que YHWH t'avait livré entre mes mains dans la caverne. On m'a dit de te tuer, mais je t'ai épargné et j'ai dit : Je ne porterai pas la main sur mon seigneur, car il est le mashiah de YHWH.
24:12	Regarde, mon père, regarde la frange de ta robe dans ma main. Car j'ai coupé la frange de ta robe et je ne t'ai pas tué. Sache et regarde qu'il n'y a ni méchanceté ni transgression en ma main et que je n'ai pas péché contre toi. Et toi, tu dresses des embûches à mon âme pour me l’enlever !
24:13	YHWH sera juge entre moi et toi, et YHWH me vengera de toi, mais ma main ne sera pas sur toi.
24:14	Comme dit le proverbe des anciens : C'est des méchants que vient la méchanceté ! C'est pourquoi je ne porterai pas la main contre toi.
24:15	Contre qui est sorti le roi d'Israël ? Qui poursuis-tu ? Un chien mort, une puce !
24:16	YHWH sera juge et jugera entre moi et toi. Il regardera et plaidera ma cause, il me rendra justice en me délivrant de ta main.
24:17	Or il arriva qu'aussitôt que David eut achevé d'adresser ces paroles à Shaoul, Shaoul dit : N'est-ce pas là ta voix, mon fils David ? Et Shaoul éleva la voix et pleura.
24:18	Et il dit à David : Tu es plus juste que moi, car tu m'as rendu le bien pour le mal que je t'ai fait.
24:19	Tu m’as fait connaître aujourd’hui la bonté avec laquelle tu agis envers moi, puisque YHWH m’avait livré entre tes mains et que tu ne m’as pas tué.
24:20	Si quelqu'un rencontre son ennemi, le laisserait-il partir sur le bon chemin ? YHWH te récompensera du bien que tu m’as fait aujourd’hui !
24:21	Et maintenant voici, je sais que tu régneras, que tu régneras et que le royaume d'Israël sera ferme entre tes mains.
24:22	Maintenant jure-moi par YHWH que tu ne détruiras pas ma postérité après moi et que tu n'extermineras pas mon nom de la maison de mon père.
24:23	David le jura à Shaoul. Puis Shaoul s'en alla dans sa maison, et David et ses hommes montèrent au lieu fort.

## Chapitre 25

### Israël pleure la mort de Shemouél (Samuel)

25:1	Shemouél mourut. Et tout Israël se rassembla pour le pleurer et on l'enterra dans sa maison à Ramah. David se leva et descendit dans le désert de Paran.

### Méchanceté de Nabal ; bon sens d'Abigaïl

25:2	Il y avait à Maon un homme qui avait ses affaires à Carmel. Cet homme-là était très grand. Il avait 3 000 brebis et 1 000 chèvres. Et il arriva qu’il faisait tondre ses brebis à Carmel.
25:3	Le nom de l'homme était Nabal, et le nom de sa femme, Abigaïl. C'était une femme de bonne compréhension<!--Ou « de bon sens ».--> et belle de visage, mais l'homme était cruel et méchant dans ses actions. Il était de la race de Kaleb.
25:4	Or David apprit dans le désert que Nabal tondait ses brebis.
25:5	Et David envoya dix jeunes hommes, et David dit aux jeunes hommes : Montez à Carmel et rendez-vous auprès de Nabal. Demandez-lui en mon nom le Shalôm.
25:6	Vous parlerez ainsi : Pour la vie ! Shalôm à toi, shalôm à ta maison et shalôm à tout ce qui t'appartient !
25:7	Et maintenant, j'ai appris que tu as les tondeurs. Or tes bergers ont été avec nous, et nous ne leur avons fait aucune injure, et ils n'ont subi aucune perte pendant tout le temps qu'ils ont été à Carmel.
25:8	Demande-le à tes serviteurs et ils te le diront. Que ces jeunes hommes trouvent grâce à tes yeux, puisque nous venons dans un jour favorable. S'il te plaît, donne à tes serviteurs et à David, ton fils, ce que tu trouveras sous ta main.
25:9	Les jeunes hommes de David arrivèrent et dirent à Nabal, au nom de David, toutes ces paroles. Puis ils se turent.
25:10	Nabal répondit aux serviteurs de David et dit : Qui est David, et qui est le fils d'Isaï ? Aujourd’hui, ils se sont multipliés, les serviteurs qui font brèche, l’homme en face de son maître.
25:11	Prendrais-je mon pain, mon eau et la viande que j'ai tuée pour mes tondeurs, afin de les donner à des hommes dont je ne sais même pas d'où ils sont ?
25:12	Les jeunes hommes de David rebroussèrent chemin et s’en retournèrent. Ils vinrent et lui rapportèrent toutes ces paroles.
25:13	Et David dit à ses hommes : Que chaque homme ceigne son épée ! Et ils ceignirent chaque homme son épée. David aussi ceignit son épée, et environ 400 hommes montèrent avec David. Il en resta 200 près des armes.
25:14	Un serviteur parmi les serviteurs informa Abigaïl, femme de Nabal, en disant : Voici, David a envoyé des messagers du désert pour bénir notre maître, mais celui-ci s’est jeté sur eux.
25:15	Cependant ces hommes ont été très bons envers nous, et ne nous ont fait aucune injure, et rien ne nous a été enlevé, tout le temps que nous avons été avec eux lorsque nous étions dans les champs.
25:16	Ils ont été pour nous une muraille nuit et jour, tout le temps que nous avons été avec eux, faisant paître les troupeaux.
25:17	Sache maintenant et vois ce que tu as à faire, car le mal est résolu contre notre maître et contre toute sa maison, parce qu'il est un fils de Bélial<!--Voir commentaire en De. 13:14.-->, et personne n'oserait lui parler.
25:18	Abigaïl se hâta, et prit 200 pains, 2 outres de vin, 5 pièces de petit bétail, 5 mesures de grain rôti, 100 grappes de raisins secs et 200 gâteaux de figues, et les mit sur des ânes.
25:19	Elle dit à ses jeunes hommes : Passez devant moi, je vais vous suivre. Elle n'en dit rien à Nabal, son mari.
25:20	Et étant montée sur un âne, elle descendait de la montagne par un chemin couvert ; voici, David et ses hommes descendaient en face d'elle, et elle les rencontra.
25:21	David avait dit : Certainement c'est en vain que j'ai gardé tout ce que cet homme a dans le désert, en sorte qu'il ne s'est rien perdu de tout ce qu'il possède. Il m'a rendu le mal pour le bien.
25:22	Qu'Elohîm traite ainsi les ennemis de David et qu'ainsi il y ajoute, si je laisse de tout ce qui lui appartient, jusqu'au matin, quelqu'un qui urine contre le mur !
25:23	Lorsque Abigaïl aperçut David, elle se hâta de descendre de son âne, et tomba sur ses faces devant David, et se prosterna contre terre.
25:24	Elle se jeta à ses pieds et lui dit : À moi la faute, mon seigneur ! S’il te plaît, que ta servante parle à tes oreilles ! Écoute les paroles de ta servante.
25:25	S'il te plaît, que mon seigneur ne prenne pas garde à cet homme de Bélial, à Nabal, car il est comme son nom : Nabal est son nom, et il y a de la folie en lui. Et moi, ta servante, je n'ai pas vu les jeunes hommes que mon seigneur a envoyés.
25:26	Maintenant, mon seigneur, YHWH est vivant et ton âme est vivante, YHWH t'a empêché d'en venir au sang, il te sauve de ta main ! Et maintenant, que tes ennemis et ceux qui cherchent du mal à mon seigneur deviennent comme Nabal !
25:27	Mais maintenant, voici un présent que ta servante a apporté à mon seigneur, afin qu'on le donne aux jeunes hommes qui sont à la suite de mon seigneur.
25:28	Pardonne, s'il te plaît, la transgression de ta servante, car YHWH fera, il fera à mon seigneur une maison stable, car mon seigneur conduit les batailles de YHWH, et il ne s'est trouvé en toi aucun mal durant tes jours.
25:29	Si les humains se lèvent pour te persécuter et pour chercher ton âme, l'âme de mon seigneur sera liée au paquet des vivants auprès de YHWH ton Elohîm. Mais il lancera au loin, du milieu d'une fronde, l'âme de tes ennemis.
25:30	Il arrivera que, lorsque YHWH aura fait à mon seigneur selon tout le bien dont il a parlé à ton sujet, et qu’il t’aura établi chef sur Israël,
25:31	ceci ne deviendra pas pour toi une pierre d'achoppement, ni une occasion de trébucher pour le cœur de mon seigneur, d’avoir sans cause versé le sang, que mon seigneur se soit sauvé lui-même. Et quand YHWH aura fait du bien à mon seigneur, souviens-toi de ta servante.
25:32	David dit à Abigaïl : Béni soit YHWH, l'Elohîm d'Israël, qui t'a aujourd'hui envoyée à ma rencontre !
25:33	Ton goût est béni, et bénie es-tu, toi qui m'as aujourd'hui empêché d'en venir au sang en me sauvant de ma main !
25:34	Mais YHWH, l'Elohîm d'Israël, qui m'a empêché de te faire du mal, est vivant ! Oui, si tu ne t'étais hâtée de venir à ma rencontre, il ne serait resté à Nabal, avant la lumière du matin, personne qui urine contre un mur.
25:35	David prit de sa main ce qu'elle lui avait apporté, et lui dit : Monte en paix dans ta maison. Regarde, j'ai écouté ta voix et je porte tes faces.

### Mort de Nabal ; Abigaïl et Achinoam deviennent les femmes de David

25:36	Abigaïl revint auprès de Nabal. Et voici, il faisait un festin dans sa maison, comme un festin de roi. Nabal avait le cœur joyeux, et il était complètement ivre. C'est pourquoi elle ne lui dit aucune chose petite ou grande, jusqu'au matin.
25:37	Il arriva au matin, quand Nabal fut sorti de son vin, que sa femme lui raconta ces choses. Alors son cœur mourut au dedans de lui, et il devint comme une pierre.
25:38	Or il arriva qu'environ dix jours après, YHWH frappa Nabal, et il mourut.
25:39	David apprit que Nabal était mort et dit : Béni soit YHWH, qui a combattu dans la lutte de mon insulte par la main de Nabal, et qui a préservé son serviteur du mal. YHWH a fait retomber le mal de Nabal sur sa tête ! Puis David envoya parler à Abigaïl, afin de la prendre pour femme.
25:40	Les serviteurs de David vinrent auprès d'Abigaïl à Carmel et lui parlèrent en disant : David nous a envoyés vers toi, afin de te prendre pour femme.
25:41	Elle se leva, se prosterna le visage contre terre, et dit : Voici, ta servante sera à ton service, afin de laver les pieds des serviteurs de mon seigneur.
25:42	Abigaïl se hâta de se lever et monta sur un âne, ainsi que cinq de ses servantes qui allaient sur ses pas. Elle partit derrière les messagers de David et devint sa femme.
25:43	David avait pris aussi Achinoam de Yizre`e'l, et toutes les deux furent ses femmes.
25:44	Shaoul avait donné Miykal, sa fille, femme de David, à Palthi, fils de Laïsh, qui était de Gallim.

## Chapitre 26

### David épargne encore la vie de Shaoul (Saül)

26:1	Les Ziyphiens allèrent vers Shaoul à Guibea en disant : David ne se cache-t-il pas sur la colline de Hakila, en face du désert ?
26:2	Shaoul se leva et descendit dans le désert de Ziyph, avec 3 000 hommes sélectionnés d'Israël, pour chercher David dans le désert de Ziyph.
26:3	Shaoul campa sur la colline de Hakila, en face du désert, près du chemin. David demeurait dans le désert. Il vit que Shaoul était venu derrière lui dans le désert.
26:4	David envoya des espions et sut avec certitude que Shaoul était arrivé.
26:5	David se leva et alla au lieu où Shaoul campait. David vit le lieu où couchait Shaoul, avec Abner, fils de Ner, chef de son armée. Shaoul couchait au milieu, et le peuple campait autour de lui.
26:6	David répondit et parla à Achiymélek, le Héthien, et à Abishaï, fils de Tserouyah, frère de Yoab, en disant : Qui veut descendre avec moi dans le camp vers Shaoul ? Et Abishaï dit : J'y descendrai avec toi.
26:7	David et Abishaï allèrent de nuit vers le peuple. Et voici, Shaoul dormait, couché au milieu, et sa lance était plantée en terre à son chevet. Et Abner et le peuple étaient couchés autour de lui.
26:8	Abishaï dit à David : Aujourd'hui, Elohîm a livré ton ennemi entre tes mains. Maintenant, s’il te plaît, que je le frappe avec la lance jusqu'en terre, une seule fois et je ne le referai pas.
26:9	Et David dit à Abishaï : Ne le tue pas ! Car qui porterait sa main sur le mashiah de YHWH et resterait impuni ?
26:10	David dit encore : YHWH est vivant ! C'est YHWH seul qui le frappera, soit que son jour vienne, soit qu'il descende au combat et qu'il y périsse.
26:11	Que YHWH me garde de mettre ma main sur le mashiah de YHWH ! Mais prends maintenant, s’il te plaît, la lance qui est à son chevet et la cruche d'eau, et allons pour nous.
26:12	David prit la lance et la cruche d'eau qui étaient au chevet de Shaoul, et ils s’en allèrent. Personne n’en vit rien, personne ne le sut, personne ne se réveilla, car ils dormaient tous, car un profond sommeil de YHWH était tombé sur eux.
26:13	David passa de l'autre côté et s'arrêta au loin sur le sommet de la montagne, et il y avait une grande distance entre eux.
26:14	Il cria au peuple et à Abner, fils de Ner, en disant : Ne répondras-tu pas, Abner ? Abner répondit et dit : Qui es-tu, toi, qui cries vers le roi ?
26:15	David dit à Abner : N'es-tu pas un vaillant homme ? Qui est semblable à toi en Israël ? Pourquoi n'as-tu pas veillé sur le roi, ton seigneur ? Car quelqu'un du peuple est venu pour tuer le roi, ton seigneur.
26:16	Elle n’est pas bonne, cette chose que tu as faite. YHWH est vivant ! Vous êtes des fils de la mort, vous qui n'avez pas veillé sur votre seigneur, le mashiah de YHWH. Et maintenant, regarde où sont la lance du roi et la cruche d'eau qui étaient à son chevet.
26:17	Shaoul reconnut la voix de David et dit : N'est-ce pas là ta voix, mon fils David ? Et David dit : C'est ma voix, roi, mon seigneur.
26:18	Il dit encore : Pourquoi mon seigneur poursuit-il son serviteur ? Car qu'ai-je fait, et quel mal y a t-il dans ma main ?
26:19	Maintenant, s'il te plaît, que le roi mon seigneur, écoute les paroles de son serviteur ! Si c'est YHWH qui te pousse contre moi, que ton offrande lui soit agréable. Mais si ce sont les fils des humains, qu'ils soient maudits devant YHWH, car aujourd'hui ils m'ont chassé, afin que je ne puisse me joindre à l'héritage de YHWH, en disant : Va, sers les elohîm étrangers !
26:20	Que mon sang ne tombe pas à terre loin de la face de YHWH ! Car le roi d'Israël est sorti pour chercher une puce, comme on poursuivrait une perdrix dans les montagnes.

### Shaoul (Saül) se repent devant David

26:21	Shaoul dit : J'ai péché ! Reviens, mon fils David, car je ne te ferai plus de mal, parce qu'aujourd'hui mon âme a été précieuse à tes yeux. Voici, j'ai agi en insensé et je me suis lourdement trompé.
26:22	David répondit et dit : Voici la lance du roi. Que l'un de tes jeunes hommes vienne la prendre.
26:23	Que YHWH rende à chacun selon sa justice et sa fidélité, car il t'avait livré aujourd'hui entre mes mains, mais je n'ai pas voulu mettre ma main sur le mashiah de YHWH.
26:24	Voici, comme ton âme a été aujourd'hui de grand prix à mes yeux, ainsi mon âme sera de grand prix aux yeux de YHWH, et il me délivrera de toutes les angoisses.
26:25	Shaoul dit à David : Tu es béni, David, mon fils ! Oui, tu agiras, tu agiras ! Oui, tu vaincras, tu vaincras ! David alla son chemin et Shaoul retourna à son lieu.

## Chapitre 27

### David se réfugie en terre des Philistins

27:1	David dit en son cœur : Maintenant, je périrai un jour par les mains de Shaoul. Il n’y a rien de bon pour moi que de me sauver, de me sauver en terre des Philistins. Shaoul se désespérera de me chercher encore dans tout le territoire d'Israël. Je me sauverai de ses mains.
27:2	David se leva, lui et les 600 hommes qui étaient avec lui, et il passa chez Akish, fils de Maoc, roi de Gath.
27:3	David et ses hommes restèrent à Gath auprès d'Akish. Chaque homme avec sa famille et David avait ses deux femmes, Achinoam de Yizre`e'l et Abigaïl de Carmel, la femme de Nabal.
27:4	On informa Shaoul que David s'était enfui à Gath et il ne continua plus à le chercher.
27:5	David dit à Akish : S'il te plaît, si j'ai trouvé grâce à tes yeux, qu’on me donne un lieu dans l’une des villes des champs et j’habiterai là. Pourquoi ton serviteur habiterait-il dans la ville royale avec toi ?
27:6	Akish lui donna ce même jour, Tsiklag. C'est pourquoi Tsiklag est aux rois de Yéhouda jusqu'à ce jour.
27:7	Le nombre des jours que David habita dans les champs des Philistins fut d'un an et quatre mois.
27:8	David montait avec ses hommes faire des incursions chez les Guéshouriens, les Guirziens et les Amalécites, car ils habitaient dans le territoire dès les temps anciens, depuis Shour jusqu'à la terre d'Égypte.
27:9	David ravageait ce territoire. Il ne laissait vivre ni homme ni femme et il prenait les brebis, les bœufs, les ânes, les chameaux et les vêtements, puis il s'en retournait et allait chez Akish.
27:10	Akish disait : Où avez-vous fait vos incursions aujourd'hui ? Et David disait : Vers le sud de Yéhouda, vers le sud des Yerachmeélites et vers le sud des Qeyniens.
27:11	David ne laissait vivre ni homme ni femme pour les amener à Gath, de peur, se disait-il, qu'ils ne fassent des rapports contre nous en disant : Voilà ce que David a fait. Telle fut sa coutume pendant tout le temps qu'il demeura dans les champs des Philistins.
27:12	Akish croyait David, et il disait : Il se rend odieux à Israël, son peuple, c'est pourquoi il sera mon serviteur à jamais.

## Chapitre 28

### Les Philistins vont en guerre contre Shaoul (Saül)

28:1	Et il arriva, en ces jours-là, que les Philistins rassemblèrent leurs camps pour la guerre, pour combattre Israël. Akish dit à David : Savoir, tu sais que tu viendras avec moi au camp, toi et tes hommes.
28:2	David dit à Akish : Ainsi tu sauras toi-même ce que ton serviteur fera. Akish dit à David : Ainsi je t’établirai gardien de ma tête, tous les jours.
28:3	Or Shemouél était mort et tout Israël s’était lamenté sur lui, et on l'avait enterré à Ramah, dans sa ville. Shaoul avait ôté de la terre ceux qui évoquaient les morts et ceux qui ont un esprit de divination.
28:4	Les Philistins se rassemblèrent et vinrent camper à Shouném. Shaoul aussi rassembla tout Israël et ils campèrent à Guilboa.
28:5	En voyant le camp des Philistins, Shaoul eut peur et son cœur trembla très fort.
28:6	Shaoul consulta YHWH, mais YHWH ne lui répondit rien, ni par des rêves, ni par l'ourim, ni par les prophètes.

### Shaoul (Saül) chez la femme qui évoque les morts

28:7	Shaoul dit à ses serviteurs : Cherchez-moi une femme maîtresse qui évoque les morts, j'irai vers elle et je la consulterai. Ses serviteurs lui dirent : Voici, à En-Dor, il y a une femme maîtresse qui évoque les morts<!--celui qui évoque les morts, l'esprit d'un mort, revenant.-->.
28:8	Shaoul se déguisa, il revêtit d’autres vêtements et s'en alla, lui et deux hommes avec lui. Ils arrivèrent de nuit chez la femme. Et il dit : Pratique la divination pour moi en évoquant un mort, en faisant monter pour moi celui que je te dirai.
28:9	La femme lui dit : Voici, tu sais ce que Shaoul a fait, comment il a exterminé de la terre ceux qui évoquent les morts et ceux qui ont un esprit de divination. Pourquoi tends-tu un piège à mon âme pour me faire mourir ?
28:10	Shaoul lui jura par YHWH et lui dit : YHWH est vivant ! Il ne t'arrivera aucun mal pour cela.
28:11	La femme dit : Qui ferai-je monter pour toi ? Il dit : Fais monter pour moi Shemouél.
28:12	La femme voyant Shemouél s'écria à grande voix, en disant à Shaoul : Pourquoi m'as-tu trompée ? Car tu es Shaoul !
28:13	Le roi lui dit : N'aie pas peur ! Mais que vois-tu ? La femme dit à Shaoul : Je vois un elohîm qui monte de la terre !
28:14	Il lui dit encore : Comment est-il fait ? Elle dit : C'est un vieil homme qui monte et il est couvert d'une robe. Shaoul sut que c'était Shemouél, il s'inclina le visage contre terre et se prosterna.
28:15	Shemouél dit à Shaoul : Pourquoi m'as-tu troublé en me faisant monter ? Shaoul dit : Je suis dans une grande détresse : les Philistins me font la guerre, et Elohîm lui-même s’est retiré de moi, il ne m'a plus répondu, ni par la main des prophètes ni par des rêves, et je t'ai appelé<!--Elohîm interdit formellement ces pratiques (De. 18:9-14 ; Es. 8:19.).--> pour que tu me fasses connaître ce que je dois faire.
28:16	Shemouél dit : Pourquoi me consultes-tu, puisque YHWH s'est retiré de toi, et qu'il est devenu ton ennemi ?
28:17	YHWH a agi comme il l'avait déclaré par ma main : YHWH a déchiré le royaume d'entre tes mains et l'a donné à ton compagnon, à David<!--1 S. 15:28 ; 1 S. 24:5.-->.
28:18	Parce que tu n'as pas écouté la voix de YHWH, et que tu n'as pas exécuté la chaleur de ses narines contre Amalek, à cause de cela, YHWH te traite de cette manière aujourd'hui.
28:19	Et même YHWH livrera Israël avec toi entre les mains des Philistins, et vous serez demain avec moi, toi et tes fils. YHWH livrera aussi le camp d'Israël entre les mains des Philistins.
28:20	Aussitôt Shaoul tomba à terre de toute sa hauteur, car il avait très peur à cause des paroles de Shemouél, et même les forces lui manquèrent parce qu'il n'avait rien mangé ce jour, ni toute cette nuit.
28:21	La femme vint auprès de Shaoul, et voyant qu'il était extrêmement terrifié, elle lui dit : Voici, ta servante a écouté ta voix, j'ai mis mon âme dans ma paume et j'ai obéi aux paroles que tu m'as dites.
28:22	Maintenant, s'il te plaît, écoute, toi aussi, ce que ta servante te dira : Laisse-moi mettre devant toi un morceau de pain, afin que tu manges pour avoir la force de te remettre en route.
28:23	Il refusa et dit : Je ne mangerai pas. Ses serviteurs et la femme aussi le pressèrent tellement qu'il écouta leur voix. Il se leva de terre et s'assit sur un lit.
28:24	Or cette femme avait dans sa maison un veau qu'elle engraissait et elle se hâta de le tuer. Puis elle prit de la farine, la pétrit et fit cuire des pains sans levain.
28:25	Elle les mit devant Shaoul et devant ses serviteurs. Et ils mangèrent. Puis s'étant levés, ils s'en allèrent cette nuit-là.

## Chapitre 29

### Les Philistins refusent que David combatte Israël

29:1	Or les Philistins rassemblèrent toutes leurs armées à Aphek, et Israël campa près de la fontaine de Yizre`e'l.
29:2	Les seigneurs des Philistins s'avancèrent par centaines et par milliers, et David et ses hommes marchèrent à l'arrière-garde avec Akish.
29:3	Les princes des Philistins dirent : Que font ici ces Hébreux ? Et Akish dit aux princes des Philistins : N'est-ce pas ce David, serviteur de Shaoul, roi d'Israël ? Il est avec moi depuis des jours ou des années, et je n’ai rien trouvé contre lui, du jour de sa chute jusqu’à ce jour.
29:4	Mais les princes des Philistins se mirent en colère contre lui et lui dirent : Renvoie cet homme ! Qu'il retourne dans le lieu où tu l'as établi. Qu'il ne descende pas avec nous dans la bataille, de peur qu'il ne devienne pour nous un satan dans la bataille. En quoi celui-là serait-il favorable à son maître, sinon avec les têtes de ces hommes ?
29:5	N'est-ce pas ce David pour qui l'on chantait et répondait en dansant : Shaoul a frappé ses 1 000, et David ses 10 000 ?
29:6	Akish appela David et lui dit : YHWH est vivant ! Tu es certainement juste, cela a été bon à mes yeux que tu sortes et rentres avec moi dans le camp, car je n'ai pas trouvé de mal en toi depuis le jour où tu es arrivé auprès de moi jusqu'à ce jour. Mais tu n'es pas agréable aux yeux des seigneurs.
29:7	Maintenant, retourne et va-t'en en paix, afin que tu ne fasses aucune chose qui déplaise aux yeux des seigneurs des Philistins.
29:8	David dit à Akish : Mais qu'ai-je fait ? Et qu'as-tu trouvé en ton serviteur depuis que je suis avec toi jusqu'à ce jour, pour que je n'aille pas combattre contre les ennemis du roi, mon seigneur ?
29:9	Akish répondit et dit à David : Je le sais, car tu es agréable à mes yeux comme un ange d'Elohîm. Mais c'est seulement les chefs des Philistins qui disent : Il ne montera pas avec nous dans la bataille.
29:10	Et maintenant lève-toi tôt le matin, avec les serviteurs de ton maître qui sont venus avec toi. Levez-vous de bon matin et partez dès que vous verrez le jour, allez-vous-en !
29:11	David et ses hommes se levèrent de bonne heure pour partir dès le matin et retourner vers la terre des Philistins, et les Philistins montèrent à Yizre`e'l.

## Chapitre 30

### David libère Tsiklag

30:1	Il arriva, le troisième jour, quand David entra à Tsiklag avec ses hommes, que les Amalécites avaient fait une incursion dans le midi et à Tsiklag, et qu’ils avaient frappé Tsiklag et l’avaient brûlée par le feu.
30:2	Ils en avaient emmené les femmes captives, depuis le petit jusqu’au grand. Ils n'avaient tué personne, mais ils les avaient emmenés et s’en étaient allés leur chemin.
30:3	David et ses hommes entrèrent dans la ville : et voici, elle était brûlée, et leurs femmes, leurs fils, et leurs filles étaient emmenés captifs.
30:4	David et le peuple qui était avec lui élevèrent leur voix et pleurèrent jusqu’à ne plus avoir en eux la force de pleurer.
30:5	Les deux femmes de David avaient été emmenées captives, Achinoam de Yizre`e'l, et Abigaïl de Carmel, femme de Nabal.
30:6	David fut très déprimé, parce que le peuple parlait de le lapider, car tout le peuple avait de l'amertume dans l'âme à cause de leurs fils et de leurs filles. Toutefois, David se fortifia en YHWH, son Elohîm.
30:7	Et il dit à Abiathar, le prêtre, fils d'Achiymélek : Apporte-moi, s'il te plaît, l'éphod ! Abiathar apporta l'éphod à David.
30:8	David consulta YHWH, en disant : Poursuivrai-je cette troupe ? L'atteindrai-je ? Et il lui dit : Poursuis-la, car tu l'atteindras, tu l'atteindras, et tu les délivreras, tu les délivreras.
30:9	David s'en alla avec les 600 hommes qui étaient avec lui, et ils arrivèrent au torrent de Besor, où s'arrêtèrent ceux qui restaient en arrière.
30:10	David les poursuivit, lui et 400 hommes. 200 hommes s'arrêtèrent, trop fatigués pour pouvoir passer le torrent de Besor.
30:11	Ayant trouvé un homme égyptien dans les champs, ils l'amenèrent à David, et lui donnèrent du pain, il mangea, puis ils lui donnèrent de l'eau à boire.
30:12	Ils lui donnèrent un gâteau de figues et deux grappes de raisins secs. Il mangea et le cœur lui revint, car cela faisait trois jours et trois nuits qu'il n'avait pas mangé de pain ni bu d'eau.
30:13	Et David lui dit : À qui es-tu ? Et d'où es-tu ? Et il dit : Je suis un garçon égyptien, serviteur d'un homme amalécite, et mon maître m'a abandonné, parce que j'étais malade il y a trois jours.
30:14	Nous avons fait une incursion au midi des Kéréthiens, et sur ce qui est à Yéhouda et sur le midi de Kaleb, et nous avons brûlé Tsiklag par le feu.
30:15	David lui dit : Me conduiras-tu vers cette troupe ? Et il dit : Jure-moi par Elohîm que tu ne me feras pas mourir, et que tu ne me livreras pas entre les mains de mon maître, et je te conduirai vers cette troupe.
30:16	Il le fit descendre, et voici, ils étaient abandonnés sur les faces de toute la terre, mangeant, buvant et dansant, à cause de ce grand butin qu'ils avaient pris de la terre des Philistins et de la terre de Yéhouda.
30:17	David les frappa depuis l'aube du jour, jusqu'au soir du lendemain. Pas un homme d’entre eux n’échappa, hormis 400 jeunes hommes qui montèrent sur des chameaux et s'enfuirent.
30:18	David sauva tout ce que les Amalécites avaient pris, David sauva aussi ses deux femmes.
30:19	Il ne leur manqua personne, depuis le plus petit jusqu'au plus grand, ni fils ni filles, ni butin, ni rien de ce qu'ils leur avaient pris : David ramena tout.
30:20	David prit tout le petit bétail et le gros bétail. Ceux qui marchaient devant ce troupeau pour le guider disaient : C'est le butin de David !

### David partage le butin

30:21	David arriva auprès des 200 hommes qui avaient été trop fatigués pour aller derrière David, et qu'on avait laissés au torrent de Besor. Ils sortirent au-devant de David et au-devant du peuple qui était avec lui. David s'étant approché du peuple, il les questionna sur la paix.
30:22	Mais tous les hommes de Bélial et méchants parmi les hommes qui étaient allés avec David répondirent et dirent : Puisqu'ils ne sont pas venus avec nous, nous ne leur donnerons rien du butin que nous avons sauvé, sinon à chaque homme sa femme et ses enfants. Qu'ils les emmènent et s'en aillent !
30:23	Mais David dit : Mes frères, n'agissez pas ainsi au sujet de ce que YHWH nous a donné. Il nous a gardés et a livré entre nos mains la troupe qui était venue contre nous.
30:24	Qui vous écouterait dans cette affaire ? Oui, la part de celui qui est descendu à la guerre sera comme la part de celui qui est resté près des armes. Ils partageront ensemble.
30:25	Et il arriva, depuis ce jour-là, qu'on en a fait une ordonnance et une coutume en Israël, jusqu'à ce jour.
30:26	David, étant arrivé à Tsiklag, envoya une partie du butin aux anciens de Yéhouda, à ses amis, en disant : Voici, un présent pour vous, du butin des ennemis de YHWH,
30:27	à ceux de Béth-El, à ceux qui étaient à Ramoth du sud, à ceux de Yattiyr,
30:28	à ceux d'Aroër, à ceux de Siphmoth, à ceux d'Eshthemoa,
30:29	à ceux de Racal, à ceux des villes des Yerachmeélites, à ceux des villes des Qeyniens,
30:30	à ceux d'Hormah, à ceux de Kor-Ashan, à ceux d'Athac,
30:31	à ceux d'Hébron, et dans tous les lieux où David avait demeuré, lui et ses hommes.

## Chapitre 31

### Les Philistins battent Israël ; Shaoul (Saül) et ses fils meurent<!--1 Ch. 10:1-14.-->

31:1	Les Philistins livrèrent bataille à Israël. Les hommes d'Israël s'enfuirent devant les Philistins et furent tués sur la Montagne de Guilboa.
31:2	Les Philistins atteignirent Shaoul et ses fils. Ils tuèrent Yehonathan, Abinadab et Malkiyshoua, fils de Shaoul.
31:3	Le poids de la guerre se porta sur Shaoul. Les hommes qui tirent l'arc le trouvèrent et il se mit à trembler devant les tireurs.
31:4	Shaoul dit au porteur de ses armes : Tire ton épée et transperce-moi, de peur que ces incirconcis ne viennent, ne me transpercent et ne m'outragent. Mais le porteur de ses armes refusa, parce qu'il avait très peur. Shaoul prit l'épée et se jeta dessus.
31:5	Voyant que Shaoul était mort, le porteur de ses armes se jeta aussi sur son épée et mourut avec lui.
31:6	Ainsi moururent ensemble, ce jour-là, Shaoul et ses trois fils, le porteur de ses armes et tous ses hommes.
31:7	Les hommes d'Israël qui étaient de ce côté de la vallée et de ce côté du Yarden, ayant vu que les hommes d’Israël s'étaient enfuis, que Shaoul et ses fils étaient morts, abandonnèrent les villes et s'enfuirent. Les Philistins vinrent et y habitèrent.
31:8	Or il arriva que, dès le lendemain, les Philistins vinrent pour dépouiller les morts, et ils trouvèrent Shaoul et ses trois fils, étendus sur la Montagne de Guilboa.
31:9	Ils coupèrent la tête de Shaoul, le dépouillèrent de ses armes qu'ils envoyèrent en terre des Philistins, de tous côtés, pour porter la nouvelle dans les maisons de leurs idoles et parmi le peuple.
31:10	Ils déposèrent les armes de Shaoul dans le temple d'Astarté, et ils attachèrent son cadavre sur les murs de Beth-Shan.
31:11	Lorsque les habitants de Yabesh en Galaad apprirent ce que les Philistins avaient fait à Shaoul,
31:12	tous les hommes talentueux se levèrent et marchèrent toute la nuit, et ils enlevèrent des murs de Beth-Shan le cadavre de Shaoul et les cadavres de ses fils. Ils revinrent à Yabesh, où ils les brûlèrent.
31:13	Ils prirent leurs ossements et les enterrèrent sous un tamaris près de Yabesh, et ils jeûnèrent 7 jours.
