# Yéhoshoua (Josué) (Jos.)

Signification : YHWH est salut

Auteur : Probablement Yéhoshoua (Josué)

Thème : La conquête de Kena'ân (Canaan)

Date de rédaction : 14ème siècle av. J.-C.

Né en Égypte, Yéhoshoua, fils de Noun, originaire de la tribu d'Éphraïm, servit le prophète Moshé (Moïse) de la sortie d'Égypte jusqu'à la mort de ce dernier. Choisi par Elohîm pour succéder au prophète, il fut le seul de l'ancienne génération, avec Kaleb, à avoir survécu à la longue épreuve du désert. Ce livre relate les étapes du voyage du peuple d'Israël et sa conquête de la terre promise. Il présente par ailleurs les victoires acquises par la puissance de YHWH sous la conduite de Yéhoshoua. C'est l'histoire de la prise de Kena'ân et de son partage entre les douze tribus d'Israël.

## Chapitre 1

### Yéhoshoua (Josué) succède à Moshé (Moïse), à sa mort<!--De. 34:9.-->

1:1	Or il arriva après la mort de Moshé, serviteur de YHWH, que YHWH parla à Yéhoshoua, fils de Noun, qui servait Moshé, en disant :
1:2	Moshé mon serviteur est mort. Maintenant, lève-toi, passe ce Yarden<!--Jourdain.-->, toi et tout ce peuple, vers la terre que moi-même je donne aux fils d'Israël.
1:3	Tout lieu que foulera la plante de votre pied, je vous l'ai donné, comme je l'ai déclaré à Moshé<!--De. 11:24.-->.
1:4	Depuis ce désert et le Liban, jusqu'au grand fleuve, le fleuve Euphrate, toute la terre des Héthiens jusqu'à la grande mer, vers le soleil couchant, seront vos frontières.
1:5	Aucun homme ne tiendra face à toi, tous les jours de ta vie. Comme j'ai été avec Moshé, je serai avec toi. Je ne te délaisserai pas et je ne t'abandonnerai pas<!--De. 31:6 ; Hé. 13:5-6.-->.
1:6	Fortifie-toi et prends courage, car c’est toi qui feras hériter à ce peuple la terre que j'ai juré à leurs pères de leur donner.
1:7	Seulement, fortifie-toi et sois très courageux pour veiller à agir selon toute la torah que Moshé mon serviteur t'a ordonnée. Ne t'en détourne ni à droite ni à gauche, afin que tu prospères partout où tu iras.
1:8	Ce livre de la torah ne s'éloignera pas de ta bouche, tu le méditeras jour et nuit, pour garder et faire tout ce qui y est écrit<!--La clé d'une vie chrétienne épanouie est la parole d'Elohîm. Méditer signifie :\\- Murmurer la parole d'Elohîm : partout où nous sommes, nous pouvons dans nos cœurs murmurer les promesses du Seigneur (Ps. 63:5-8, 119:11).\\- Proclamer à haute voix : il est intéressant de noter que le mot hébreu traduit dans Jos. 1:8 par « méditer » est traduit par « prononcer » ou « publier » dans Pr. 8:7 ; Ps. 35:28.\\- Réfléchir profondément : il faut être dans le secret (Mt. 6:5-6). En Israël, il est de coutume d'aller étudier la torah à l'ombre d'un figuier. Voir Jn. 1:43-51.-->, car alors tu feras réussir ta voie, alors tu prospéreras<!--Voir Ge. 3:6.-->.
1:9	Ne t'ai-je pas donné cet ordre : Fortifie-toi et prends courage ? Ne tremble pas et ne t’épouvante pas, car YHWH ton Elohîm est avec toi partout où tu iras.

### Yéhoshoua prend la direction du peuple

1:10	Et Yéhoshoua donna cet ordre aux commissaires du peuple, en disant :
1:11	Passez par le camp, et donnez cet ordre au peuple en disant : Préparez-vous des provisions, car dans trois jours vous passerez ce Yarden pour aller prendre possession de la terre que YHWH votre Elohîm vous donne pour en prendre possession.
1:12	Yéhoshoua parla aux Reoubénites, aux Gadites et à la demi-tribu de Menashè, en disant :
1:13	Souvenez-vous de la parole que vous a ordonnée Moshé, le serviteur de YHWH, en disant : YHWH votre Elohîm vous a accordé du repos, et vous a donné cette terre.
1:14	Vos femmes, vos petits-enfants, et vos bêtes resteront sur la terre que Moshé vous a donnée de l'autre côté du Yarden. Mais vous tous, hommes vaillants et talentueux, vous passerez en armes devant vos frères, et vous les aiderez<!--De. 3:18.-->,
1:15	jusqu'à ce que YHWH ait accordé du repos à vos frères comme à vous, et qu'ils prennent aussi possession de la terre que YHWH, votre Elohîm, leur donne. Puis vous reviendrez prendre possession de la terre qui est votre propriété, et que vous a donnée Moshé, serviteur de YHWH, de l'autre côté du Yarden, du côté du soleil levant.
1:16	Ils répondirent à Yéhoshoua, en disant : Nous ferons tout ce que tu nous as ordonné, et nous irons partout où tu nous enverras.
1:17	Nous t'obéirons comme nous avons obéi à Moshé. Seulement que YHWH ton Elohîm soit avec toi, comme il a été avec Moshé.
1:18	Tout homme qui sera désobéissant envers ta bouche et n’écoutera pas tes paroles dans tout ce que tu lui commanderas, sera mis à mort. Seulement, fortifie-toi et prends courage !

## Chapitre 2

### Yéhoshoua envoie deux espions à Yeriycho (Jéricho) ; ils sont reçus par Rahab<!--Ja. 2:25.-->

2:1	Or Yéhoshoua fils de Noun, envoya secrètement de Sittim deux hommes pour espionner, en disant : Allez, examinez la terre et Yeriycho ! Ils partirent et entrèrent dans la maison d'une femme prostituée, nommée Rahab<!--Rahab avait entendu parler de l'Elohîm des Hébreux et avait placé son espérance de salut en lui (Ro. 10:11). Par cet acte de foi, sa destinée a changé. Cette femme, qui était vouée à une double condamnation du fait de sa condition de prostituée (De. 23:18) et de son appartenance à une nation païenne qui devait être dévouée par interdit (Jos. 6), a été sauvée avec sa famille (Ac. 2:21, 16:31). Ainsi, bien des siècles plus tard, on ne la mentionnera plus comme une prostituée, mais comme une ancêtre du Sauveur et une héroïne de la foi (Mt. 1:5 ; Hé. 11:31). Rahab est donc l'archétype des païens qui sont rentrés dans l'alliance d'Elohîm par la foi.-->, et ils y couchèrent.
2:2	On dit au roi de Yeriycho : Voici, des hommes sont venus ici cette nuit de la part des fils d'Israël pour explorer la terre.
2:3	Le roi de Yeriycho envoya dire à Rahab : Fais sortir les hommes qui sont venus chez toi et qui sont entrés dans ta maison, car ils sont venus pour explorer toute la terre.
2:4	Or la femme prit les deux hommes et les cacha. Puis elle dit : Il est vrai que des hommes sont venus chez moi, mais je ne savais pas d'où ils étaient.
2:5	Il arriva qu'au moment où l'on fermait la porte dans l'obscurité, ces hommes sont sortis. Je ne sais pas où ces hommes sont allés. Poursuivez-les en hâte, car vous les atteindrez !
2:6	Or elle les avait fait monter sur le toit et les avait cachés sous des tiges de lin qu'elle avait arrangées sur le toit.
2:7	Ces hommes les poursuivirent par le chemin du Yarden jusqu'aux passages et l'on ferma la porte après que ceux qui les poursuivaient furent sortis.
2:8	Or avant qu'ils se couchent, elle monta vers eux sur le toit.
2:9	Elle dit aux hommes : Je sais que YHWH vous a donné cette terre, que votre terreur est tombée sur nous, et que tous les habitants de la terre se fondent devant vous<!--Ex. 23:27.-->.
2:10	Car nous avons entendu que YHWH a mis à sec devant vous les eaux de la Mer Rouge à votre sortie de la terre d'Égypte, et ce que vous avez fait aux deux rois des Amoréens qui étaient de l'autre côté du Yarden, à Sihon et à Og, que vous avez détruits complètement en les dévouant par interdit.
2:11	Nous l'avons entendu, notre cœur s'est fondu et il ne s'élève plus de souffle dans aucun homme devant vous. Oui, c'est YHWH votre Elohîm qui est Elohîm, en haut dans les cieux et ici-bas sur la Terre<!--De. 4:39.-->.
2:12	Maintenant, s’il vous plaît, jurez-moi par YHWH, puisque j'ai usé de bonté envers vous, vous userez aussi de bonté envers la maison de mon père. Donnez-moi un signe de votre fidélité, 
2:13	que vous laisserez vivre mon père et ma mère, mes frères et mes sœurs, et tous ceux qui leur appartiennent, et que vous sauverez nos âmes de la mort.
2:14	Les hommes lui dirent : Nos âmes mourront à votre place si vous n'exposez pas cette affaire, et il arrivera que lorsque YHWH nous aura donné la terre nous userons envers toi de bonté et de vérité.

### Les espions s'enfuient aidés par Rahab

2:15	Elle les fit descendre avec une corde par la fenêtre, car sa maison était d’un côté de la muraille, et c’est sur la muraille qu’elle habitait.
2:16	Elle leur dit : Allez à la montagne, de peur que ceux qui vous poursuivent ne vous rencontrent, et cachez-vous là pendant trois jours jusqu'à ce qu'ils soient de retour. Après cela vous suivrez votre chemin.
2:17	Ces hommes lui dirent : Nous serons innocents de ton serment que tu nous as fait jurer.
2:18	Voici, quand nous entrerons sur la terre, tu attacheras ce cordon de fil d'écarlate à la fenêtre par laquelle tu nous auras fait descendre, et tu recueilleras chez toi, dans cette maison, ton père et ta mère, tes frères, et toute la famille de ton père.
2:19	Et il arrivera que quiconque sortira hors de la porte de ta maison, son sang sera sur sa tête, et nous en serons innocents ; mais quiconque sera avec toi, dans la maison, son sang sera sur notre tête si l’on porte la main sur lui.
2:20	Si tu rapportes notre parole, celle-là, nous serons innocents du serment que tu nous as fait jurer.
2:21	Elle dit : Qu'il en soit selon vos paroles. Puis elle les renvoya, et ils s'en allèrent. Elle attacha le cordon de fil d'écarlate à la fenêtre.
2:22	Ils marchèrent et arrivèrent à la montagne, où ils restèrent trois jours, jusqu'à ce que ceux qui les poursuivaient soient de retour. Ceux qui les poursuivaient les cherchèrent par tout le chemin, mais ils ne les trouvèrent pas.
2:23	Ces deux hommes s'en retournèrent, descendirent de la montagne, passèrent le Yarden. Ils vinrent auprès de Yéhoshoua, fils de Noun. Ils lui racontèrent tout ce qui leur était arrivé.
2:24	Ils dirent à Yéhoshoua : Oui, YHWH a livré toute la terre entre nos mains et même tous les habitants de la terre se fondent devant nous.

## Chapitre 3

### Israël traverse le Yarden (Jourdain) à sec

3:1	Yéhoshoua se leva tôt le matin, lui et tous les fils d'Israël partirent de Sittim, ils vinrent jusqu'au Yarden, et ils logèrent là cette nuit, avant de le traverser.
3:2	Il arriva qu'au bout de trois jours les commissaires traversèrent le milieu du camp,
3:3	et donnèrent cet ordre au peuple, en disant : Dès que vous verrez l'arche de l'alliance de YHWH, votre Elohîm, portée par les prêtres, les Lévites, vous partirez de votre lieu et vous marcherez derrière elle.
3:4	Toutefois, il y aura entre vous et elle une distance de la mesure d'environ 2 000 coudées : n’en approchez pas, afin que vous connaissiez le chemin par où vous irez, car vous n'êtes pas passé par ce chemin, ni hier ni avant-hier.
3:5	Yéhoshoua dit au peuple : Sanctifiez-vous, car demain YHWH fera des choses merveilleuses au milieu de vous<!--Ex. 19:10-11.-->.
3:6	Yéhoshoua parla aux prêtres, en disant : Portez l'arche de l'alliance et passez face au peuple ! Ils portèrent l'arche de l'alliance et marchèrent face au peuple.
3:7	YHWH dit à Yéhoshoua : En ce jour je commencerai à te rendre grand aux yeux de tout Israël, afin qu'ils sachent que je suis avec toi, comme j'ai été avec Moshé.
3:8	Et toi, ordonne aux prêtres qui portent l'arche de l'alliance, en disant : Dès que vous arriverez au bord des eaux du Yarden, vous vous arrêterez dans le Yarden.
3:9	Et Yéhoshoua dit aux fils d'Israël : Approchez-vous d'ici, et écoutez les paroles de YHWH, votre Elohîm.
3:10	Yéhoshoua dit : À ceci vous saurez que le El Hayi<!--El Vivant.--> est au milieu de vous et qu'il dépossédera, qu'il dépossédera face à vous les Kena'ânéens, les Héthiens, les Héviens, les Phéréziens, les Guirgasiens, les Amoréens et les Yebousiens.
3:11	Voici, l'arche de l'alliance d'Adonaï de toute la Terre passe en face de vous dans le Yarden.
3:12	Maintenant, prenez douze hommes des tribus d'Israël, un homme de chaque tribu.
3:13	Il arrivera qu'aussitôt que les plantes des pieds des prêtres qui portent l'arche de YHWH, Adonaï de toute la Terre, seront posées dans les eaux du Yarden, les eaux du Yarden seront coupées, les eaux qui descendent d'en haut, et elles s'arrêteront en un tas<!--Ps. 114:3.-->.
3:14	Il arriva que le peuple partit de ses tentes pour passer le Yarden, et les prêtres qui portaient l'arche de l'alliance étaient devant le peuple.
3:15	Quand ceux qui portaient l'arche furent arrivés au Yarden, et que les prêtres qui portaient l'arche plongèrent les pieds dans les eaux, au bord - or le Yarden est plein sur toutes ses rives tous les jours de la moisson<!--La traversée s'est faite à la période où le Yarden était en crue, c'est-à-dire la fin de la saison des pluies, au début des moissons. Cette époque correspondait à la fin du mois d'avril, dans cette région du monde, ce qui, dans le calendrier hébreu, équivaut au mois de Nisan ou Abib. Voir 1 Ch. 12:16.--> -
3:16	les eaux qui descendaient d'en haut s'arrêtèrent et s'élevèrent en un tas très loin, à Adam-la-Ville, du côté de Tsarthan. Celles qui descendaient vers la mer de la région aride, la Mer Salée, furent complètement coupées. Le peuple passa vis-à-vis de Yeriycho.
3:17	Mais les prêtres qui portaient l'arche de l'alliance de YHWH, se tinrent immobiles sur le sec, au milieu du Yarden, pendant que tout Israël passait à sec, jusqu'à ce que tout le peuple ait achevé de passer le Yarden.

## Chapitre 4

### Yéhoshoua (Josué) dresse un monument de pierres en souvenir de la traversée

4:1	Or il arriva que quand tout le peuple eut achevé de passer le Yarden, que YHWH parla à Yéhoshoua et dit :
4:2	Prenez douze hommes parmi le peuple : un homme, un homme par tribu.
4:3	Et donnez-leur cet ordre, en disant : Prenez ici, du milieu du Yarden, de la place où les prêtres se sont tenus immobiles, douze pierres que vous emporterez avec vous et vous les poserez au lieu où vous passerez cette nuit.
4:4	Yéhoshoua appela les douze hommes qu'il choisit parmi les fils d'Israël, un homme, un homme par tribu.
4:5	Yéhoshoua leur dit : Passez face à l'arche de YHWH, votre Elohîm, au milieu du Yarden ! Que chaque homme soulève une pierre sur son épaule, selon le nombre des tribus des fils d'Israël,
4:6	afin que cela devienne un signe au milieu de vous. Quand demain vos fils vous demanderont en disant : Ces pierres, que sont-elles pour vous ?
4:7	Vous leur direz : Les eaux du Yarden ont été coupées face à l'arche de l'alliance de YHWH. Lorsqu'elle passa le Yarden, les eaux du Yarden ont été coupées. C'est pourquoi ces pierres sont un souvenir pour les fils d'Israël à perpétuité.
4:8	C’est ainsi que les fils d’Israël firent ce que Yéhoshoua leur avait ordonné. Ils prirent douze pierres du milieu du Yarden, comme YHWH l'avait ordonné à Yéhoshoua, selon le nombre des tribus des fils d'Israël. Ils les emportèrent avec eux et les posèrent au lieu où ils devaient passer la nuit.
4:9	Yéhoshoua dressa douze pierres au milieu du Yarden, à la place où les pieds des prêtres qui portaient l'arche de l'alliance s'étaient arrêtés, elles y sont restées jusqu'à ce jour.
4:10	Les prêtres qui portaient l'arche se tinrent debout au milieu du Yarden, jusqu'à ce que tout ce que YHWH avait ordonné à Yéhoshoua de dire au peuple soit accompli, selon tout ce que Moshé avait prescrit à Yéhoshoua. Et le peuple se hâta de passer.
4:11	Il arriva que quand tout le peuple eut achevé de passer, l'arche de YHWH et les prêtres passèrent en face du peuple.
4:12	Et les fils de Reouben, les fils de Gad, et la demi-tribu de Menashè passèrent en armes en face des fils d'Israël, comme Moshé le leur avait dit<!--No. 32:20-29.-->.
4:13	Ils passèrent dans les régions arides de Yeriycho environ 40 000 hommes équipés pour la guerre, en face de YHWH, pour combattre.
4:14	Ce jour-là, YHWH rendit Yéhoshoua grand aux yeux de tout Israël, et ils le craignirent, comme ils avaient craint Moshé, tous les jours de sa vie.
4:15	YHWH parla à Yéhoshoua, et dit :
4:16	Ordonne aux prêtres qui portent l'arche du témoignage qu'ils montent hors du Yarden.
4:17	Yéhoshoua donna cet ordre aux prêtres, en disant : Montez hors du Yarden !
4:18	Il arriva que, comme les prêtres qui portaient l'arche de l'alliance de YHWH montèrent du milieu du Yarden, que les plantes des pieds des prêtres se retirèrent sur le sol sec, les eaux du Yarden retournèrent à leur place et coulèrent comme hier et avant-hier sur toutes ses rives.
4:19	Le peuple monta hors du Yarden le dixième jour du premier mois, et il campa à Guilgal, à l'est de Yeriycho.
4:20	Yéhoshoua dressa aussi à Guilgal les douze pierres qu'ils avaient prises du Yarden.
4:21	Il parla aux fils d'Israël et leur dit : Quand vos fils demanderont, demain, à leurs pères en disant : Que sont ces pierres ?
4:22	Vous ferez savoir à vos fils en disant : Israël a passé ce Yarden à sec.
4:23	Parce que YHWH, votre Elohîm, a mis à sec face à vous les eaux du Yarden jusqu’à votre passage, comme YHWH, votre Elohîm, l'avait fait à la Mer Rouge, qu'il a mise à sec en face de nous jusqu’après notre passage,
4:24	afin que tous les peuples de la Terre sachent que la main de YHWH est puissante, afin que vous ayez toujours la crainte de YHWH, votre Elohîm.

## Chapitre 5

### La crainte s'empare des Amoréens

5:1	Il arriva qu'aussitôt que tous les rois des Amoréens qui étaient au-delà du Yarden, vers l'occident, et tous les rois des Kena'ânéens qui étaient près de la mer, apprirent que YHWH avait mis à sec les eaux du Yarden devant les fils d'Israël, jusqu'à ce qu'ils aient passé, leur cœur fondit et leur esprit fut abattu devant les fils d'Israël.

### Israël circoncis à nouveau ; la fin de la manne

5:2	En ce temps-là, YHWH dit à Yéhoshoua : Fais-toi des couteaux de pierre, et circoncis de nouveau les fils d'Israël, une seconde fois.
5:3	Et Yéhoshoua se fit des couteaux de pierre tranchants, et circoncit les fils d'Israël sur la colline d'Araloth.
5:4	C’est ici la raison pour laquelle Yéhoshoua les circoncit : tout le peuple qui était sorti d'Égypte, les mâles, tous les hommes de guerre étaient morts en chemin dans le désert, après leur sortie d'Égypte.
5:5	Tout le peuple sorti d'Égypte était circoncis, mais tout le peuple né dans le désert, en chemin, après sa sortie d'Égypte, n’était pas circoncis.
5:6	Car les fils d'Israël avaient marché dans le désert 40 ans jusqu'à ce que soit consumée toute la nation des hommes de guerre qui étaient sortis d'Égypte, qui n'avaient pas écouté la voix de YHWH. YHWH leur avait juré de ne pas leur faire voir la terre que YHWH avait juré à leurs pères de nous donner, terre où coulent le lait et le miel<!--No. 14:32-33.-->.
5:7	Il avait établi leurs fils à leur place. Ce sont eux que Yéhoshoua circoncit, parce qu'ils étaient incirconcis. En effet, on ne les avait pas circoncis en chemin.
5:8	Il arriva que lorsqu’on eut achevé de circoncire toute la nation, ils restèrent dans leur camp, jusqu'à ce qu'ils soient guéris.
5:9	YHWH dit à Yéhoshoua : Aujourd'hui j'ai roulé de dessus vous l'insulte de l'Égypte. Et ce lieu fut appelé du nom de Guilgal jusqu'à ce jour.
5:10	Les fils d'Israël campèrent à Guilgal, et ils firent la Pâque le quatorzième jour du mois, sur le soir, dans les régions arides de Yeriycho<!--Ex. 12:6.-->.
5:11	Et dès le lendemain de la Pâque, ils mangèrent du blé de la terre, des pains sans levain et du grain rôti, en ce même jour<!--Ex. 12:39 ; Lé. 2:14.-->.
5:12	La manne cessa dès le lendemain, après qu'ils eurent mangé du blé de la terre. Les fils d'Israël n'eurent plus de manne, mais ils mangèrent les récoltes de la terre de Kena'ân cette année-là<!--Ex. 16:35.-->.

### Rencontre avec le chef de l'armée de YHWH

5:13	Or il arriva, comme Yéhoshoua était près de Yeriycho, qu'il leva les yeux et regarda. Voici, un homme qui avait son épée tirée à la main, se tenait debout devant lui. Yéhoshoua alla vers lui et lui dit : Es-tu pour nous ou pour nos ennemis ?
5:14	Il dit : Non, mais je suis le chef de l'armée de YHWH, je viens maintenant. Yéhoshoua tomba sur ses faces, contre terre, se prosterna et lui dit : Que dit Adonaï à son serviteur ?
5:15	Le chef de l'armée de YHWH dit à Yéhoshoua : Ôte tes sandales de tes pieds, car le lieu sur lequel tu te tiens est saint<!--Ex. 3:5.-->. Et Yéhoshoua fit ainsi.

## Chapitre 6

### Yeriycho (Jéricho) miraculeusement livrée à Israël ; Rahab sauvée

6:1	Or Yeriycho était fermée, fermée face aux fils d'Israël. Personne ne sortait et personne n'entrait.
6:2	Et YHWH dit à Yéhoshoua : Regarde, j'ai livré entre tes mains Yeriycho, son roi, ses hommes vaillants et talentueux.
6:3	Vous tous, hommes de guerre, vous ferez le tour de la ville, en tournant une fois autour d'elle. Tu feras ainsi durant six jours.
6:4	Et sept prêtres porteront sept shofars en cornes de béliers<!--Les termes « cornes de béliers » viennent de l'hébreu « yowbel » qui signifie aussi « jubilé », « trompette retentissante ». Voir commentaire en Ex. 19:13.--> face à l'arche. Mais au septième jour, vous ferez sept fois le tour de la ville et les prêtres sonneront des shofars.
6:5	Il arrivera que, lorsque la voix de la corne<!--Vient de « keh’-ren » qui au sens figuré parle de la force.--> de bélier<!--Ou « du Jubilé ». Voir Ex. 19:13.--> se prolongera, aussitôt que vous entendrez la voix du shofar, tout le peuple poussera un grand cri de joie et la muraille de la ville tombera sur elle. Le peuple montera, chaque homme devant lui.
6:6	Yéhoshoua, fils de Noun, appela les prêtres et leur dit : Portez l'arche de l'alliance et que sept prêtres portent sept shofars des Jubilés devant l'arche de YHWH.
6:7	Il dit au peuple : Passez et faites le tour de la ville, que tous ceux qui seront armés passent devant l'arche de YHWH.
6:8	Il arriva, quand Yéhoshoua eut parlé au peuple, que les sept prêtres qui portaient les sept shofars en cornes de bélier en face de YHWH passèrent et sonnèrent des shofars. L'arche de l'alliance de YHWH marchait derrière eux.
6:9	Et les hommes qui étaient armés marchaient en face des prêtres qui sonnaient des shofars, mais l'arrière-garde marchait derrière l'arche. On sonnait des shofars en marchant.
6:10	Or Yéhoshoua avait donné cet ordre au peuple, en disant : Vous ne crierez pas et vous ne ferez pas entendre votre voix. Il ne sortira pas une parole de votre bouche jusqu’au jour où je vous dirai : Criez ! Alors vous crierez.
6:11	L'arche de YHWH fit ainsi le tour de la ville, en tournant tout autour une fois, puis on revint au camp, et on y passa la nuit.
6:12	Yéhoshoua se leva tôt le matin, et les prêtres portèrent l'arche de YHWH.
6:13	Les sept prêtres qui portaient les sept shofars des Jubilés en face de l'arche de YHWH marchaient, ils marchaient et sonnaient des shofars. Ceux qui étaient armés marchaient en face d’eux, et l'arrière-garde marchait derrière l'arche de YHWH. On sonnait des shofars en marchant.
6:14	Ils firent une fois le tour de la ville le deuxième jour, et ils retournèrent au camp. Ils firent de même durant six jours.
6:15	Le septième jour étant arrivé, ils se levèrent dès le matin à l'aube du jour, et firent le tour de la ville, de cette manière sept fois, seulement ce jour ils firent le tour de la ville sept fois.
6:16	Il arriva que la septième fois, comme les prêtres sonnaient des shofars, Yéhoshoua dit au peuple : Criez, car YHWH vous a donné la ville !
6:17	La ville sera vouée à une entière destruction pour YHWH, elle et toutes les choses qui y sont, seulement Rahab, la prostituée<!--Rahab sauva sa famille par sa foi en Elohîm (Ac. 16:31). Voir Jos. 2.-->, vivra, elle et tous ceux qui seront avec elle dans la maison, parce qu'elle a caché les messagers que nous avions envoyés.
6:18	Quant à vous, gardez-vous seulement de ce qui est voué à une entière destruction, de peur que vous ne soyez voués à une entière destruction. Car en prenant de ce qui est voué à une entière destruction, vous mettriez le camp d'Israël en chose vouée à une entière destruction et vous le troublerez<!--De. 7:26.-->.
6:19	Tout l'argent et tout l'or, tous les objets de cuivre et de fer seront consacrés à YHWH. Ils entreront dans le trésor de YHWH<!--No. 31:54.-->.
6:20	Le peuple cria et on sonna des shofars. Il arriva que quand le peuple entendit le son des shofars, il cria et la muraille tomba sur elle-même<!--Hé. 11:30.-->. Le peuple monta dans la ville, chaque homme devant soi, et ils prirent la ville.
6:21	Ils la dévouèrent entièrement par interdit, à bouche d’épée, tout ce qui était dans la ville, depuis l'homme jusqu'à la femme, depuis le jeune homme jusqu'au vieillard, même jusqu'aux bœufs, aux brebis et aux ânes.
6:22	Yéhoshoua dit aux deux hommes qui avaient espionné la terre : Entrez dans la maison de cette femme prostituée et faites-la sortir de là et tout ce qui est à elle, comme vous le lui avez juré.
6:23	Les jeunes hommes qui avaient espionné entrèrent et firent sortir Rahab, son père, sa mère et ses frères, avec tout ce qui est à elle. Ils firent sortir toutes les familles des siens et les firent se reposer en dehors du camp d'Israël.
6:24	Ils allumèrent le feu et brûlèrent la ville et tout ce qui s'y trouvait, seulement ils mirent l'argent et l'or, les objets de cuivre et de fer, dans le trésor de la maison de YHWH.
6:25	Mais Rahab la prostituée, ainsi que la maison de son père et tout ce qui est à elle, Yéhoshoua les laissa vivre. Elle a habité au milieu d'Israël jusqu'à ce jour, parce qu'elle avait caché les messagers que Yéhoshoua avait envoyés pour explorer Yeriycho.
6:26	Et en ce temps-là Yéhoshoua jura, en disant : Maudit soit devant YHWH l'homme qui se lèvera et rebâtira cette ville de Yeriycho ! Il la fondera sur son premier-né et il en posera les portes sur son cadet<!--Cette parole s'est accomplie en 1 R. 16:34.-->.
6:27	YHWH fut avec Yéhoshoua et sa renommée se répandit sur toute la terre.

## Chapitre 7

### Israël battu à Aï suite au péché d'Akan

7:1	Les fils d'Israël commirent un délit, une transgression, au sujet de ce qui est voué à une entière destruction. Car Akan, fils de Karmiy, fils de Zabdi, fils de Zérach, de la tribu de Yéhouda, prit de ce qui est voué à une entière destruction, et la colère de YHWH s'enflamma contre les fils d'Israël.
7:2	Yéhoshoua envoya de Yeriycho des hommes vers Aï, près de Beth-Aven, à l'orient de Béth-El. Il leur parla et dit : Montez espionner la terre. Ces hommes montèrent pour espionner Aï.
7:3	Et étant retournés vers Yéhoshoua, ils lui dirent : Que tout le peuple ne monte pas ! Que deux ou trois mille hommes environ montent frapper Aï ! Ne fatigue pas là tout le peuple, car ils sont peu nombreux.
7:4	Il monta du peuple environ 3 000 hommes qui s’enfuirent devant les hommes de Aï.
7:5	Les hommes de Aï leur tuèrent environ 36 hommes, car ils les poursuivirent depuis la porte jusqu'à Shebarim, et les battirent à la descente. Le cœur du peuple se fondit et devint comme de l'eau.
7:6	Yéhoshoua déchira ses vêtements et tomba sur ses faces contre terre, devant l'arche de YHWH jusqu'au soir, lui et les anciens d'Israël, et ils jetèrent de la poussière sur leur tête.
7:7	Et Yéhoshoua dit : Ah ! Adonaï YHWH, pourquoi as-tu fait passer, passer le Yarden à ce peuple ? Est-ce pour nous livrer entre les mains des Amoréens et nous faire périr ? Si seulement nous avions su rester au-delà du Yarden !
7:8	Excuse-moi, Adonaï, que dirai-je, puisqu'Israël a tourné le dos devant ses ennemis ?
7:9	Les Kena'ânéens et tous les habitants de la terre l'entendront. Ils nous envelopperont, et ils retrancheront notre nom de dessus la Terre. Et que feras-tu à ton grand Nom ?
7:10	YHWH dit à Yéhoshoua : Lève-toi ! Pourquoi cela, tomber sur tes faces ? 
7:11	Israël a péché. Ils ont transgressé mon alliance que je leur avais prescrite. Ils ont même pris de ce qui était voué à une entière destruction. Ils en ont même volé, et ils ont même menti. Ils l'ont même mis parmi leurs objets<!--Il est impossible de remporter une victoire contre Satan en ayant avec soi des choses qui lui appartiennent (Jn. 14:30). Celui qui pèche est du diable nous dit la parole d'Elohîm (1 Jn. 3:4-10). Les grandes victoires sont remportées par ceux qui se sanctifient et invoquent le Nom de Yéhoshoua ha Mashiah (Jésus le Christ).-->.
7:12	C'est pourquoi les fils d'Israël ne pourront se dresser en face de leurs ennemis. Ils tourneront le dos devant leurs ennemis, car ils sont devenus une chose vouée à une entière destruction. Je ne serai plus avec vous si vous ne détruisez pas ce qui est voué à une entière destruction du milieu de vous.
7:13	Lève-toi, sanctifie le peuple. Tu diras : Sanctifiez-vous pour demain, car ainsi parle YHWH, l'Elohîm d'Israël : Il y a une chose vouée à une entière destruction au milieu de toi, Israël ! Tu ne pourras pas te dresser en face de tes ennemis, jusqu’à ce que vous ayez ôté du milieu de vous la chose vouée à une entière destruction.
7:14	Vous vous approcherez le matin selon vos tribus, et la tribu que YHWH aura saisie s'approchera selon les familles, et la famille que YHWH aura saisie s'approchera selon les maisons, et la maison que YHWH aura saisie s'approchera par hommes forts.
7:15	Il arrivera que celui qui aura été saisi avec la chose vouée à une entière destruction sera brûlé au feu, lui et tout ce qui lui appartient parce qu'il a transgressé l'alliance de YHWH, et qu'il a commis une infamie en Israël.
7:16	Yéhoshoua se leva tôt le matin et fit approcher Israël selon ses tribus, et la tribu de Yéhouda fut saisie.
7:17	Il fit approcher les familles de Yéhouda, et la famille de Zérach fut saisie. Il fit approcher la famille de Zérach par hommes forts, et Zabdi fut saisi.
7:18	Il fit approcher sa maison par hommes forts et Akan, fils de Karmiy, fils de Zabdi, fils de Zérach, de la tribu de Yéhouda, fut saisi.
7:19	Yéhoshoua dit à Akan : Mon fils, s’il te plaît, donne gloire à YHWH, l'Elohîm d'Israël, et donne-lui louange. Déclare-moi, s’il te plaît, ce que tu as fait, ne me le cache pas.
7:20	Et Akan répondit à Yéhoshoua et dit : J'ai péché il est vrai, contre YHWH, l'Elohîm d'Israël, et voici ce que j'ai fait.
7:21	J'ai vu parmi le butin un beau manteau de Shinear<!--Ge. 10:6-12.-->, 200 sicles d'argent et un lingot d'or du poids de 50 sicles. Je les ai convoités et pris. Les voici, ils sont cachés dans la terre au milieu de ma tente, et l'argent est sous le manteau.
7:22	Yéhoshoua envoya des messagers qui coururent à cette tente, et voici, le manteau était caché dans la tente d'Akan et l'argent sous le manteau.
7:23	Ils les prirent du milieu de la tente et les apportèrent à Yéhoshoua et à tous les fils d'Israël, et ils les versèrent en face de YHWH.
7:24	Yéhoshoua et tout Israël avec lui, prirent Akan, fils de Zérach, l'argent, le manteau, le lingot d'or, ses fils et ses filles, ses bœufs, ses ânes et ses brebis, sa tente et tout ce qui lui appartenait, et ils les firent monter dans la vallée d'Acor.
7:25	Yéhoshoua dit : Pourquoi nous as-tu troublés ? YHWH te troublera en ce jour. Et tout Israël le lapida avec des pierres. On les brûla au feu, on les lapida avec des pierres.
7:26	Ils élevèrent sur lui un grand monceau de pierres, jusqu'à ce jour. YHWH revint de la chaleur de ses narines. C'est pourquoi on a appelé ce lieu du nom de vallée d'Acor<!--2 S. 18:17.--> jusqu'à ce jour.

## Chapitre 8

### Victoire d'Israël à Aï

8:1	YHWH dit à Yéhoshoua : N'aie pas peur et ne t'effraie pas<!--De. 1:21, 7:18.--> ! Prends avec toi tout le peuple de guerre et lève-toi, monte contre Aï. Regarde, j'ai livré entre tes mains le roi de Aï et son peuple, sa ville et sa terre.
8:2	Tu traiteras Aï et son roi, comme tu as traité Yeriycho et son roi : seulement vous pillerez pour vous le butin et les bêtes. Place des gens en embuscade derrière la ville.
8:3	Yéhoshoua se leva avec tout le peuple de guerre, pour monter contre Aï. Yéhoshoua choisit 30 000 hommes vaillants et talentueux, et les envoya de nuit.
8:4	Il leur donna cet ordre, en disant : Voyez, vous qui serez en embuscade derrière la ville. Ne vous éloignez pas beaucoup de la ville, mais tenez-vous prêts.
8:5	Moi et tout le peuple qui est avec moi, nous nous approcherons de la ville. Il arrivera que quand ils sortiront à notre rencontre, comme la première fois, nous nous enfuirons en face d’eux.
8:6	Ils sortiront derrière nous, jusqu'à ce que nous les ayons déracinés de la ville, car ils diront : Ils s’enfuient en face de nous comme la première fois ! Et nous nous enfuirons en face d’eux.
8:7	Vous vous lèverez de l'embuscade et vous prendrez possession de la ville, car YHWH, votre Elohîm, la livrera entre vos mains.
8:8	Il arrivera que quand vous aurez pris la ville, vous y mettrez le feu. Vous agirez selon la parole de YHWH. Regardez, je vous l'ai ordonné.
8:9	Yéhoshoua les envoya, et ils allèrent se mettre en embuscade, et se tinrent entre Béth-El et Aï, à l'ouest d'Aï. Mais Yéhoshoua resta cette nuit-là au milieu du peuple.
8:10	Yéhoshoua se leva tôt le matin, dénombra le peuple et monta, lui et les anciens d'Israël, devant le peuple vers Aï.
8:11	Tout le peuple de guerre qui était avec lui monta et s'approcha. Ils vinrent en face de la ville et campèrent du côté du nord de Aï, et la vallée était entre lui et Aï.
8:12	Il prit environ 5 000 hommes et les mit en embuscade entre Béth-El et Aï, à l'ouest de la ville.
8:13	Le peuple mit son camp au nord de la ville et son arrière-garde à l'ouest la ville. Et Yéhoshoua alla cette nuit-là au milieu de la vallée.
8:14	Il arriva que, lorsque le roi de Aï vit cela, les hommes de la ville se hâtèrent et se levèrent tôt le matin et, au temps fixé, le roi et tout son peuple sortirent face à la région aride contre Israël pour le combattre. Or il ne savait pas qu'il y avait des gens en embuscade contre lui derrière la ville.
8:15	Yéhoshoua et tout Israël se firent battre en face d’eux et s'enfuirent vers le désert.
8:16	Tout le peuple qui était dans Aï fut appelé pour les poursuivre. Ils poursuivirent Yéhoshoua et furent déracinés de la ville.
8:17	Il ne resta pas un seul homme dans Aï ni dans Béth-El qui ne sortit contre Israël. Ils laissèrent la ville ouverte, et ils poursuivirent Israël.
8:18	YHWH dit à Yéhoshoua : Étends vers Aï le javelot qui est dans ta main, car je la livrerai entre tes mains. Et Yéhoshoua étendit vers la ville le javelot qui était dans sa main.
8:19	Dès qu'il eut étendu sa main, ceux qui étaient en embuscade se levèrent rapidement du lieu où ils étaient, et ils se mirent à courir. Ils entrèrent dans la ville, la prirent et se hâtèrent de mettre le feu dans la ville.
8:20	Les hommes de Aï se retournèrent : ils virent la fumée de la ville qui montait jusqu'aux cieux, et il n'y eut en eux aucune force pour fuir çà ou là. Et le peuple qui fuyait vers le désert se tourna contre ceux qui le poursuivaient.
8:21	Yéhoshoua et tout Israël voyant que ceux qui étaient en embuscade avaient pris la ville, et que la fumée de la ville montait, se retournèrent et frappèrent les hommes de Aï.
8:22	Ceux-là sortirent de la ville à leur rencontre. Ils se trouvèrent au milieu d'Israël, les uns d'un côté, et les autres de l'autre. Ils les frappèrent au point qu’il ne leur resta ni survivant ni rescapé<!--De. 7:2.-->.
8:23	Ils prirent aussi vivant le roi de Aï, et le présentèrent à Yéhoshoua.
8:24	Il arriva que, lorsque Israël eut achevé de tuer tous les habitants de Aï dans la campagne, dans le désert, où ils les avaient poursuivis, et que tous furent tombés à bouche d’épée, jusqu'à être entièrement défaits, tout Israël revint vers Aï et la frappa du tranchant de l'épée.
8:25	Tous ceux qui tombèrent ce jour-là, tant des hommes que des femmes, furent 12 000, tous hommes de Aï.
8:26	Yéhoshoua ne retira pas sa main qu'il tenait étendue avec le javelot, jusqu'à ce que tous les habitants de Aï aient été entièrement dévoués par interdit.
8:27	Seulement, Israël pilla pour lui les bêtes et le butin de cette ville-là, suivant ce que YHWH avait prescrit à Yéhoshoua<!--No. 31:22-26.-->.
8:28	Yéhoshoua brûla Aï, et en fit pour toujours un monceau de ruines, une désolation, jusqu'à ce jour.
8:29	Il pendit le roi de Aï à un arbre jusqu'au temps du soir. Et comme le soleil se couchait, Yéhoshoua ordonna qu'on descende de l'arbre son cadavre. On le jeta à l'entrée de la porte de la ville, puis on dressa sur lui un grand amas de pierres, jusqu’à ce jour.

### Sacrifices offerts à YHWH et lecture de la torah de Moshé (Moïse)

8:30	Alors Yéhoshoua bâtit un autel à YHWH, l'Elohîm d'Israël, sur la montagne d'Ébal,
8:31	comme Moshé, serviteur de YHWH, l'avait ordonné aux fils d'Israël, ainsi qu'il est écrit<!--De. 27:4.--> dans le livre de la torah de Moshé : Il fit cet autel de pierres brutes sur lesquelles personne ne porta le fer<!--L'autel devait être construit avec des pierres taillées par Elohîm lui-même dans la nature (Ex. 20:25). L'Assemblée (Église) du Seigneur est construite avec des pierres vivantes, taillées par Elohîm et non par les humains (Mt. 16:18). Babel ou Babylone est construite avec des briques, œuvre des humains (Ge. 11:1-3).-->. Ils firent monter dessus des holocaustes à YHWH, et sacrifièrent des sacrifices d'offrande de paix<!--Voir commentaire en Lé. 3:1.-->.
8:32	Là, il écrivit sur les pierres une copie de la torah de Moshé, que celui-ci avait écrite devant les fils d'Israël.
8:33	Tout Israël, avec ses anciens, ses commissaires et ses juges, se tenaient debout des deux côtés de l'arche, en face des prêtres, les Lévites, qui portaient l'arche de l'alliance de YHWH, les étrangers comme les autochtones, une moitié du côté du Mont Garizim<!--Voir Jn. 4:19-24.-->, et l'autre moitié du côté du Mont Ébal, selon l'ordre qu'avait précédemment donné Moshé, serviteur de YHWH, de bénir le peuple d'Israël.
8:34	Et après cela, il lut tout haut toutes les paroles de la torah, tant les bénédictions que les malédictions, selon tout ce qui est écrit dans le livre de la torah.
8:35	Il n'y eut pas une parole de toutes celles que Moshé avait prescrites, que Yéhoshoua ne proclama devant toute l'assemblée d'Israël, des femmes, des petits-enfants et des étrangers qui marchaient au milieu d'eux.

## Chapitre 9

### Yéhoshoua (Josué) tombe dans la ruse des Gabaonites

9:1	Il arriva que, lorsque tous les rois qui étaient au-delà du Yarden, dans la montagne et dans la plaine, et sur toute la côte de la grande mer, jusque près du Liban : les Héthiens, les Amoréens, les Kena'ânéens, les Phéréziens, les Héviens et les Yebousiens, eurent entendu ces choses,
9:2	ils se réunirent ensemble pour faire la guerre à Yéhoshoua et à Israël, d’une même bouche.
9:3	Mais les habitants de Gabaon, ayant entendu ce que Yéhoshoua avait fait à Yeriycho et à Aï,
9:4	agirent même avec ruse. Ils partirent déguisés comme s'ils étaient des ambassadeurs. Ils prirent de vieux sacs pour leurs ânes, et de vieilles outres de vin déchirées et recousues,
9:5	à leurs pieds de vieilles sandales rapiécées et sur eux de vieux habits, tout leur pain, leur provision, était sec et en miettes.
9:6	Et ils arrivèrent auprès de Yéhoshoua au camp de Guilgal, et lui dirent, ainsi qu'à tous les hommes d'Israël : Nous sommes venus d'une terre éloignée, maintenant traitez alliance avec nous.
9:7	Les hommes d'Israël dirent à ces Héviens : Peut-être que vous habitez au milieu de nous, et comment traiterions-nous alliance avec vous ?
9:8	Mais ils dirent à Yéhoshoua : Nous sommes tes serviteurs. Yéhoshoua leur dit : Qui êtes-vous et d’où venez-vous ? 
9:9	Ils lui dirent : Tes serviteurs sont venus d'une terre très éloignée, au nom de YHWH, ton Elohîm. Car nous avons entendu sa renommée et tout ce qu’il a fait contre Égypte,
9:10	et tout ce qu'il a fait aux deux rois des Amoréens, qui étaient au-delà du Yarden, Sihon, roi de Hesbon, et Og, roi de Bashân, à Ashtaroth.
9:11	Nos anciens et tous les habitants de notre terre nous ont dit : Prenez en vos mains des provisions pour le chemin, et allez au-devant d'eux, et dites-leur : Nous sommes vos serviteurs, et maintenant traitez alliance avec nous.
9:12	Voici notre pain : nous l'avons pris dans nos maisons tout chaud pour notre provision, le jour où nous sommes partis pour venir vers vous. Et maintenant, le voilà sec et en miettes.
9:13	Voici les outres de vin neuves que nous avons remplies, elles se sont déchirées. Nos habits et nos sandales sont usés à cause de la longueur de la marche.
9:14	Les hommes d'Israël prirent de leur provision et ne consultèrent pas la bouche de YHWH.
9:15	Yéhoshoua fit la paix avec eux et traita avec eux une alliance<!--Ex. 23:32.--> par laquelle il devait leur laisser la vie, et les princes de l'assemblée le leur jurèrent.

### Les Gabaonites démasqués

9:16	Il arriva, trois jours après qu’ils eurent traité alliance avec eux, qu'ils apprirent que c'étaient leurs voisins et qu'ils habitaient parmi eux.
9:17	Les fils d'Israël partirent et entrèrent dans leurs villes le troisième jour. Leurs villes étaient Gabaon, Kephiyrah, Beéroth et Qiryath-Yéarim.
9:18	Les fils d'Israël ne les frappèrent pas, parce que les princes de l'assemblée leur avaient juré par YHWH, l'Elohîm d'Israël. Mais toute l'assemblée murmura contre les princes.
9:19	Tous les princes dirent à toute l'assemblée : Nous leur avons juré par YHWH, l'Elohîm d'Israël, c'est pourquoi maintenant nous ne pouvons pas les frapper.
9:20	Voici comment nous les traiterons : nous les laisserons vivre, afin qu'il n'y ait pas de colère contre nous, à cause du serment que nous leur avons fait.
9:21	Ils vivront ! leur dirent les princes. Et ils devinrent des coupeurs de bois et des puiseurs d'eau pour toute l'assemblée, comme les princes le leur avaient dit<!--Voir 2 S. 21:1-14. La présence des Gabaonites en plein centre de Kena'ân (Canaan) tendait à isoler les tribus du nord de celles du sud, favorisant ainsi le schisme des deux royaumes (1 R. 12).-->.
9:22	Yéhoshoua les fit appeler et leur parla, en disant : Pourquoi nous avez-vous trompés, en disant : Nous sommes très éloignés de vous, alors que vous habitez au milieu de nous ?
9:23	Maintenant vous êtes maudits et vous ne cesserez pas d'être esclaves, coupeurs de bois et puiseurs d'eau pour la maison de mon Elohîm.
9:24	Ils répondirent à Yéhoshoua et dirent : Oui, il a été rapporté, rapporté à tes serviteurs que YHWH, ton Elohîm, avait ordonné à Moshé, son serviteur, de vous donner toute la terre, pour exterminer tous les habitants de la terre en face de vous. Nous avons eu extrêmement peur pour nos âmes en face de vous. C'est pourquoi nous avons agi de cette manière.
9:25	Et maintenant nous voici entre tes mains. Traite-nous comme il semble bon et juste à tes yeux de nous traiter !
9:26	C'est ainsi qu'il les traita. Il les délivra de la main des fils d'Israël qui ne les firent pas mourir.
9:27	De ce jour, Yéhoshoua les donna pour être coupeurs de bois et porteurs d’eau pour l'assemblée et pour l'autel de YHWH jusqu'à ce jour, dans le lieu qu'il choisirait.

## Chapitre 10

### Yéhoshoua (Josué) secourt Gabaon des cinq rois des Amoréens

10:1	Il arriva qu'Adoniy-Tsédeq, roi de Yeroushalaim, apprit que Yéhoshoua avait pris Aï et l'avait dévouée par interdit, traitant Aï et son roi comme il avait traité Yeriycho et son roi et que les habitants de Gabaon avaient fait la paix avec Israël et demeuraient au milieu d'eux,
10:2	il eut très peur, car Gabaon était une grande ville, comme une ville royale, elle était plus grande qu'Aï, et tous ses hommes étaient vaillants.
10:3	Adoniy-Tsédeq, roi de Yeroushalaim, envoya dire à Hoham, roi d'Hébron, et à Piream, roi de Yarmouth, et à Yaphiya, roi de Lakis, et à Debir, roi d'Églon :
10:4	Montez vers moi, et aidez-moi afin que nous frappions Gabaon, car elle a fait la paix avec Yéhoshoua et avec les fils d'Israël.
10:5	Cinq rois des Amoréens, le roi de Yeroushalaim, le roi d'Hébron, le roi de Yarmouth, le roi de Lakis et le roi d'Églon, se rassemblèrent et montèrent avec toutes leurs armées. Ils campèrent près de Gabaon et lui firent la guerre.
10:6	Les hommes de Gabaon envoyèrent dire à Yéhoshoua au camp de Guilgal : Ne retire pas tes mains de tes serviteurs, monte vers nous en hâte, sauve-nous, aide-nous, car tous les rois des Amoréens qui habitent aux montagnes se sont rassemblés contre nous.
10:7	Yéhoshoua monta de Guilgal, lui et tout le peuple de guerre avec lui, ainsi que tous les hommes vaillants et talentueux.

### YHWH accorde à Israël une grande victoire à Maqqédah

10:8	YHWH dit à Yéhoshoua : N'aie pas peur d'eux, car je les ai livrés entre tes mains et aucun d'eux ne tiendra devant toi.
10:9	Yéhoshoua arriva soudainement sur eux, après avoir marché toute la nuit depuis Guilgal.
10:10	YHWH les mit en déroute en face d’Israël, il les frappa d’un grand coup près de Gabaon, les poursuivit par le chemin de la montagne de Beth-Horon et les battit jusqu'à Azéqah et jusqu'à Maqqédah.
10:11	Il arriva que, comme ils fuyaient devant Israël - ils étaient à la descente de Beth-Horon, - YHWH fit tomber des cieux sur eux de grosses pierres jusqu'à Azéqah, et ils moururent. Ceux qui moururent des pierres de grêle furent plus nombreux que ceux qui furent tués avec l'épée par les fils d'Israël.
10:12	Alors Yéhoshoua parla à YHWH, le jour où YHWH livra les Amoréens en face des fils d'Israël et dit aux yeux d’Israël : Soleil, attends<!--Être silencieux, être tranquille, attendre, être muet.--> à Gabaon, et toi lune, sur la vallée d'Ayalon !
10:13	Le soleil attendit, et la lune stationna jusqu'à ce que la nation se soit vengée de ses ennemis. Cela n'est-il pas écrit dans le livre du Juste ? Le soleil stationna au milieu des cieux et ne se hâta pas de se coucher environ un jour entier<!--Ha. 3:11.-->.
10:14	Il n'y a pas eu de jour semblable à celui-là, ni avant ni après, où YHWH a écouté la voix d'un homme, car YHWH combattait pour Israël.
10:15	Yéhoshoua, et tout Israël avec lui, retourna au camp à Guilgal.
10:16	Les cinq rois s'enfuirent et se cachèrent dans une caverne à Maqqédah.
10:17	On le rapporta à Yéhoshoua, en disant : On a trouvé les cinq rois cachés dans une caverne à Maqqédah.
10:18	Yéhoshoua dit : Roulez de grosses pierres à l'entrée de la caverne et mettez près d'elle quelques hommes pour les garder.
10:19	Mais vous, ne vous arrêtez pas, poursuivez vos ennemis, attaquez-les par-derrière jusqu'au dernier, ne les laissez pas entrer dans leurs villes, car YHWH, votre Elohîm, les a livrés entre vos mains.
10:20	Il arriva que lorsque Yéhoshoua et les fils d'Israël eurent achevé de les frapper d'un très grand coup, jusqu’à leur achèvement, les survivants d’entre eux échappèrent et entrèrent dans les villes fortifiées.
10:21	Tout le peuple revint en paix au camp vers Yéhoshoua à Maqqédah. Pas un homme ne remua la langue contre les fils d'Israël.
10:22	Yéhoshoua dit : Ouvrez l'entrée de la caverne et amenez-moi ces cinq rois hors de la caverne.
10:23	C'est ainsi qu'on fit sortir vers lui de la caverne ces cinq rois : Le roi de Yeroushalaim, le roi d'Hébron, le roi de Yarmouth, le roi de Lakis et le roi d'Églon.
10:24	Et il arriva, quand on eut fait sortir ces rois de la caverne vers Yéhoshoua, que Yéhoshoua appela tous les hommes d'Israël et dit aux chefs des hommes de guerre qui avaient marché avec lui : Approchez-vous, mettez vos pieds sur les cous de ces rois. Ils s'approchèrent et mirent leurs pieds sur leurs cous<!--Ps. 110:1.-->.
10:25	Yéhoshoua leur dit : N'ayez pas peur et ne soyez pas effrayés, fortifiez-vous et ayez du courage, car YHWH traitera ainsi tous vos ennemis contre lesquels vous combattez.
10:26	Après cela, Yéhoshoua les frappa et les fit mourir. Il les fit pendre à cinq arbres et ils restèrent pendus à ces arbres jusqu'au soir.
10:27	Et il arriva, au temps du coucher du soleil, que Yéhoshoua ordonna qu'on les descende des arbres. On les jeta dans la caverne où ils s'étaient cachés, et on mit à l'entrée de la caverne de grosses pierres, jusqu'à ce jour<!--De. 21:23.-->.
10:28	Yéhoshoua prit aussi Maqqédah le même jour et la frappa à bouche d’épée. Il dévoua par interdit son roi ainsi que toutes les âmes qui s'y trouvaient. Il ne laissa aucun survivant et il traita le roi de Maqqédah comme il avait traité le roi de Yeriycho.

### Conquête des territoires du sud

10:29	Yéhoshoua, et tout Israël avec lui, passa de Maqqédah à Libnah, et fit la guerre à Libnah.
10:30	YHWH la livra aussi entre les mains d'Israël, avec son roi, et il la frappa à bouche d’épée, elle et toutes les âmes qui s'y trouvaient. Il n'y laissa aucun survivant et il traita son roi comme il avait traité le roi de Yeriycho.
10:31	Yéhoshoua, et tout Israël avec lui, passa de Libnah à Lakis, campa devant elle et lui fit la guerre.
10:32	YHWH livra Lakis entre les mains d'Israël, qui la prit le deuxième jour, et la frappa à bouche d’épée, avec toutes les âmes qui s'y trouvaient, comme il avait traité Libnah.
10:33	Alors Horam, roi de Guézer, monta pour secourir Lakis. Yéhoshoua le frappa, lui et son peuple, sans laisser un seul survivant.
10:34	Yéhoshoua, et tout Israël avec lui, passa de Lakis à Églon. Ils campèrent devant elle et lui firent la guerre.
10:35	Ils la prirent le jour même, la frappèrent à bouche d’épée. Il dévoua par interdit ce jour-là toutes les âmes qui y étaient, comme il avait traité Lakis.
10:36	Yéhoshoua, et tout Israël avec lui, monta d'Églon à Hébron, et ils lui firent la guerre.
10:37	Ils la prirent et la frappèrent à bouche d’épée, avec son roi, toutes ses villes et toutes les âmes qui y étaient. Il n'en laissa échapper aucune, comme il avait traité Églon et il dévoua par interdit toutes les âmes qui y étaient.
10:38	Yéhoshoua, et tout Israël avec lui, retourna vers Debir et lui fit la guerre.
10:39	Il la prit, avec son roi et toutes ses villes, et ils les frappèrent à bouche d’épée, et dévouèrent par interdit toutes les âmes qui y étaient. Il n'en laissa échapper aucune. Il traita Debir et son roi comme il avait traité Hébron, et comme il avait traité Libnah et son roi.
10:40	Yéhoshoua frappa toute cette terre, la montagne et le midi, la plaine et les coteaux, et tous leurs rois. Il n'en laissa échapper aucun et il dévoua par interdit toutes les âmes qui y respiraient, comme YHWH, l'Elohîm d'Israël, l'avait ordonné<!--De. 20:16-17.-->.
10:41	Yéhoshoua les battit depuis Qadesh-Barnéa jusqu'à Gaza, et toute la région de Goshen jusqu'à Gabaon.
10:42	Yéhoshoua prit tous ces rois en même temps et leur terre, parce que YHWH, l'Elohîm d'Israël, combattait pour Israël.
10:43	Yéhoshoua et tout Israël avec lui retourna au camp à Guilgal.

## Chapitre 11

### Conquête des territoires du nord

11:1	Il arriva que lorsque Yabiyn, roi de Hatsor, entendit ces choses, il envoya des messagers à Yobab, roi de Madon, au roi de Shimron, au roi d'Acshaph,
11:2	aux rois qui étaient au nord, dans la montagne, dans la région aride au sud de Kinnéreth, dans la vallée et dans les hauteurs de Dor à l'ouest,
11:3	aux Kena'ânéens qui étaient à l'est et à l'ouest, aux Amoréens, aux Héthiens, aux Phéréziens, aux Yebousiens dans les montagnes, et aux Héviens au pied de la montagne de l'Hermon, en terre de Mitspah.
11:4	Ils sortirent avec toutes leurs armées, un peuple nombreux comme le sable qui est sur le bord de la mer, il y avait aussi des chevaux et des chars en très grande quantité.
11:5	Tous ces rois se réunirent et campèrent ensemble près des eaux de Mérom, pour combattre contre Israël.
11:6	Et YHWH dit à Yéhoshoua : N'aie pas peur d'eux, car demain, à cette même heure, je les livrerai tous, blessés à mort, devant Israël. Tu couperas les jarrets à leurs chevaux et tu brûleras au feu leurs chars<!--2 S. 8:4.-->.
11:7	Yéhoshoua, et tous les hommes de guerre avec lui vinrent subitement sur eux près des eaux de Mérom, et ils se précipitèrent au milieu d'eux.
11:8	Et YHWH les livra entre les mains d'Israël. Ils les frappèrent, et les poursuivirent jusqu'à Sidon la grande, jusqu'aux eaux de Misrephoth-Maïm, et jusqu'à la vallée de Mitspah vers l'est, et ils les frappèrent tellement qu'ils ne laissèrent aucun survivant.
11:9	Yéhoshoua les traita comme YHWH lui avait dit. Il coupa les jarrets de leurs chevaux et brûla au feu leurs chars.
11:10	En ce temps-là, Yéhoshoua revint et s’empara de Hatsor. Il tua son roi avec l'épée, car Hatsor avait été auparavant la capitale de tous ces royaumes.
11:11	On tua aussi à bouche d’épée et l'on dévoua par interdit toutes les âmes qui s’y trouvaient, il ne resta rien de ce qui respirait et l'on brûla au feu Hatsor.
11:12	Et toutes les villes de ces rois et tous leurs rois, Yéhoshoua les prit. Il les frappa à bouche d’épée et les dévoua par interdit, comme Moshé, serviteur de YHWH, l'avait ordonné.
11:13	Mais Israël ne brûla aucune des villes situées sur des collines, excepté Hatsor seule, que Yéhoshoua brûla.
11:14	Les fils d'Israël pillèrent pour eux tout le butin de ces villes et le bétail, mais ils frappèrent à bouche d’épée tous les humains, jusqu'à ce qu'ils les aient exterminés, ils n'y laissèrent aucun qui respirait.
11:15	Ce que YHWH avait ordonné à Moshé son serviteur, Moshé l'avait ordonné à Yéhoshoua et Yéhoshoua le fit. Il n'écarta pas une parole de tout ce que YHWH avait ordonné à Moshé.

### Yéhoshoua (Josué) s'empare de toute la terre

11:16	Yéhoshoua prit toute cette terre-là, la montagne et toute la terre du midi, avec toute la région de Goshen, la vallée et la région aride, la montagne d'Israël et ses vallées :
11:17	depuis la montagne de Halak, qui s'élève vers Séir, jusqu'à Baal-Gad dans la vallée du Liban, au pied de la montagne de l'Hermon. Il prit aussi tous leurs rois, les battit et les fit mourir.
11:18	Yéhoshoua fit la guerre plusieurs jours contre tous ces rois.
11:19	Il n'y eut aucune ville qui fit la paix avec les fils d'Israël, excepté les Héviens qui habitaient à Gabaon. Ils les prirent toutes par la guerre.
11:20	Car cela venait de YHWH, qu'ils endurcissent leur cœur pour qu'ils sortent en guerre contre Israël, afin qu'il les dévoue par interdit, sans qu'il y ait pour eux de miséricorde, et qu'il les extermine, comme YHWH l'avait ordonné à Moshé<!--Ex. 4:21 ; De. 2:30 ; 1 R. 12:15.-->.
11:21	En ce temps-là Yéhoshoua vint et extermina les Anakim des montagnes d'Hébron, de Debir, d'Anab, et de toute la montagne de Yéhouda, et de toute la montagne d'Israël. Yéhoshoua les dévoua par interdit avec leurs villes.
11:22	Il ne resta aucun Anakim en terre des fils d'Israël. Il n'en resta seulement qu'à Gaza, à Gath et à Asdod<!--2 S. 21:20.-->.
11:23	Yéhoshoua prit toute la terre, suivant tout ce que YHWH avait dit à Moshé. Et Yéhoshoua la donna en héritage à Israël, selon leurs portions, et leurs tribus. Et la terre fut en paix, sans guerre.

## Chapitre 12

### Liste des rois vaincus par Moshé et Yéhoshoua (Moïse et Josué)

12:1	Voici les rois de la terre que les fils d'Israël frappèrent, et dont ils prirent possession de la terre de l'autre côté du Yarden, du côté du soleil levant, depuis le torrent de l'Arnon jusqu'à la montagne de l'Hermon, et toute la région aride vers l'est.
12:2	Sihon, roi des Amoréens, qui habitait à Hesbon. Il dominait depuis Aroër, sur le bord du torrent de l'Arnon et le milieu du torrent, sur la moitié de Galaad, jusqu'au torrent de Yabboq, frontière des fils d'Ammon<!--De. 3:8-16.-->,
12:3	et la région aride jusqu'à la Mer de Kinnéreth vers l'est, et jusqu'à la mer de la région aride, la Mer Salée, vers l'est, sur le chemin de Beth-Yeshiymoth, et vers le Théman, sous les pentes du Pisgah<!--les montagnes de Pisga, qui incluent le Mont Nebo.-->.
12:4	Les contrées d'Og, roi de Bashân, un reste des géants, qui habitait à Ashtaroth et à Édréi.
12:5	Il dominait sur la montagne de l'Hermon, sur Salcah, et sur tout Bashân, jusqu'à la frontière des Guéshouriens et des Maakathiens, et sur la moitié de Galaad, frontière de Sihon, roi de Hesbon.
12:6	Moshé, serviteur de YHWH, et les fils d'Israël, les frappèrent. Moshé, serviteur de YHWH, en donna la possession aux Reoubénites, aux Gadites, et à la demi-tribu de Menashè<!--No. 32:33.-->.
12:7	Voici les rois de la terre que Yéhoshoua et les fils d'Israël frappèrent de ce côté-ci du Yarden vers l'occident, depuis Baal-Gad, dans la vallée du Liban, jusqu'à la montagne de Halak qui monte vers Séir, et que Yéhoshoua donna aux tribus d'Israël en possession, selon leurs portions,
12:8	dans la montagne, dans la vallée, dans la région aride et sur les coteaux, dans le désert et dans le midi. C'était la terre des Héthiens, des Amoréens, des Kena'ânéens, des Phéréziens, des Héviens et des Yebousiens.
12:9	Le roi de Yeriycho, un. Le roi de Aï qui est à côté de Béth-El, un.
12:10	Le roi de Yeroushalaim, un. Le roi d'Hébron, un.
12:11	Le roi de Yarmouth, un. Le roi de Lakis, un.
12:12	Le roi d'Églon, un. Le roi de Guézer, un.
12:13	Le roi de Debir, un. Le roi de Guéder, un.
12:14	Le roi de Hormah, un. Le roi d'Arad, un.
12:15	Le roi de Libnah, un. Le roi d'Adoullam, un.
12:16	Le roi de Maqqédah, un. Le roi de Béth-El, un.
12:17	Le roi de Tappouah, un. Le roi de Hépher, un.
12:18	Le roi d'Aphek, un. Le roi de Sharôn, un.
12:19	Le roi de Madon, un. Le roi de Hatsor, un.
12:20	Le roi de Shimron-Meron, un. Le roi d'Acshaph, un.
12:21	Le roi de Taanac, un. Le roi de Meguiddo, un.
12:22	Le roi de Kédesh, un. Le roi de Yoqne`am, au Carmel, un.
12:23	Le roi de Dor, sur les hauteurs de Dor, un. Le roi de Goyim, près de Guilgal, un.
12:24	Le roi de Tirtsah, un. En tout 31 rois.

## Chapitre 13

### Les territoires de Reouben (Ruben), de Gad et de la demi-tribu de Menashè (Manassé)

13:1	Yéhoshoua étant devenu vieux et avancé en âge, YHWH lui dit : Toi, tu es devenu vieux, tu es avancé en âge, et il reste une très grande terre à prendre en possession.
13:2	Voici la terre qui reste, toutes les contrées des Philistins et des Guéshouriens,
13:3	depuis le Shiychor, qui coule devant l'Égypte, jusqu'à la frontière d'Ékron au nord, contrée considérée comme kena'ânéenne et qui est occupée par les cinq seigneurs des Philistins, celui de Gaza, celui d'Asdod, celui d'Askalon, celui de Gath, celui d'Ékron, et par les Avviens :
13:4	du Théman, toute la terre des Kena'ânéens, et Me`arah qui est aux Sidoniens, jusqu'à Aphek, jusqu'à la frontière des Amoréens,
13:5	la terre des Guibliens et tout le Liban, vers le soleil levant, depuis Baal-Gad, au-dessous de la montagne de l'Hermon, jusqu'à l'entrée de Hamath,
13:6	tous les habitants de la montagne, depuis le Liban jusqu'aux eaux de Misrephoth-Maïm, tous les Sidoniens. Je les déposséderai moi-même devant les fils d'Israël. Donne seulement cette terre en héritage par le sort à Israël, comme je te l'ai prescrit.
13:7	Maintenant divise cette terre en héritage aux neuf tribus, et à la demi-tribu de Menashè,
13:8	avec l'autre moitié de laquelle les Reoubénites et les Gadites ont pris leur héritage, lequel Moshé leur a donné au-delà du Yarden, vers l'est, selon que Moshé, serviteur de YHWH, le leur a donné :
13:9	depuis Aroër, qui est sur le bord du torrent de l'Arnon, et la ville qui est au milieu de la vallée, et toute la plaine de Médeba, jusqu'à Dibon,
13:10	et toutes les villes de Sihon, roi des Amoréens, qui régnait à Hesbon, jusqu'à la frontière des enfants d'Ammon.
13:11	Galaad, et les territoires des Guéshouriens et des Maakathiens, toute la montagne de l'Hermon, et tout Bashân jusqu'à Salcah :
13:12	tout le royaume d'Og en Bashân, qui régnait à Ashtaroth et à Édréi ; c’est lui qui était resté du reste des géants. Moshé battit ces rois et les déposséda.
13:13	Or les fils d'Israël ne dépossédèrent pas les Guéshouriens et les Maakathiens, mais les Guéshouriens et les Maakathiens ont habité au milieu d'Israël jusqu'à ce jour.
13:14	Seulement il ne donna pas d'héritage à la tribu de Lévi. Les offrandes consumées par le feu devant YHWH, l'Elohîm d'Israël, tel fut son héritage, comme il le lui avait dit<!--No. 18:20-24 ; De. 10:9, 18:2 ; Ez. 44:28.-->.
13:15	Moshé donna un héritage à la tribu des fils de Reouben selon leurs familles.
13:16	Leurs frontières furent depuis Aroër qui est sur le bord du torrent de l'Arnon, et de la ville qui est au milieu du torrent, et toute la plaine qui est près de Médeba.
13:17	Hesbon et toutes ses villes, qui étaient dans la plaine, Dibon, Bamoth-Baal, Beth-Baal-Meon,
13:18	Yahats, Qedémoth et Méphaath,
13:19	Qiryathayim, Sibma, Tséreth-Hashachar sur la montagne de la vallée,
13:20	Beth-Peor, sous les pentes du Pisgah<!--les montagnes de Pisga, qui incluent le Mont Nebo.--> et Beth-Yeshiymoth,
13:21	et toutes les villes de la plaine, et tout le royaume de Sihon, roi des Amoréens qui régnait à Hesbon. Moshé l'avait battu, lui et les princes de Madian, Évi, Rékem, Tsour, Hour et Réba, princes qui relevaient de Sihon, et qui habitaient sur la terre.
13:22	Les fils d'Israël firent passer aussi par l'épée Balaam<!--Voir No. 22. Balaam était l'exemple type du prophète corrompu, soucieux de tirer profit de son service.-->, fils de Beor, le devin, avec les autres qui y furent tués.
13:23	Et les frontières des fils d'Israël fut le Yarden et sa frontière. Tel fut l'héritage des fils de Reouben, selon leurs familles, leurs villes et leurs villages<!--No. 34:14-15.-->.
13:24	Moshé avait donné un héritage à la tribu de Gad, pour les fils de Gad, selon leurs familles.
13:25	Leur terre fut Ya`azeyr, et toutes les villes de Galaad et la moitié de la terre des fils d'Ammon, jusqu'à Aroër qui est vis-à-vis de Rabba,
13:26	et depuis Hesbon jusqu'à Ramath-Mitspé, et Bethonim, et depuis Mahanaïm jusqu'à la frontière de Debir,
13:27	dans la vallée, Beth-Haram, Beth-Nimrah, Soukkoth et Tsaphon, le reste du royaume de Sihon, roi de Hesbon, le Yarden et la frontière jusqu'à l'extrémité de la Mer de Kinnéreth, de l'autre côté du Yarden, vers l'est.
13:28	Tel fut l'héritage des fils de Gad, selon leurs familles, leurs villes et leurs villages.
13:29	Moshé avait donné à la demi-tribu de Menashè un héritage, qui est resté à la demi-tribu des fils de Menashè, selon leurs familles.
13:30	Ce qui devint leur territoire : depuis Mahanaïm, tout Bashân, et tout le royaume d'Og, roi de Bashân, et tous les villages de Yaïr qui sont en Bashân, 60 villes.
13:31	Et la moitié de Galaad, Ashtaroth et Édréi, villes du royaume d'Og en Bashân, furent aux fils de Makir, fils de Menashè, à la moitié des fils de Makir, selon leurs familles.
13:32	Voilà ce que Moshé avait donné en héritage, dans les régions arides de Moab, de l'autre côté du Yarden, vis-à-vis de Yeriycho, à l'est.
13:33	Moshé ne donna pas d'héritage à la tribu de Lévi. YHWH, l'Elohîm d'Israël, était leur héritage, comme il le lui avait dit.

## Chapitre 14

### Kaleb reçoit Hébron

14:1	Et voici ce qu’ont possédé les fils d'Israël en terre de Kena'ân, que leur ont fait posséder le prêtre Èl’azar, Yéhoshoua, fils de Noun, et les têtes des pères des tribus des fils d'Israël.
14:2	Selon le sort de leur héritage, comme YHWH l'avait ordonné par la main de Moshé pour les neuf tribus et la demi-tribu<!--No. 26:55.-->.
14:3	Car Moshé avait donné un héritage aux deux tribus et à la demi-tribu de l'autre côté du Yarden, mais il n'avait pas donné d'héritage aux Lévites parmi eux.
14:4	Car les fils de Yossef étaient deux tribus : Menashè et Éphraïm. On ne donna pas de part aux Lévites sur la terre, excepté des villes pour habitation, et les faubourgs pour leurs troupeaux et pour leurs biens.
14:5	Les fils d'Israël firent comme YHWH l'avait ordonné à Moshé, et ils partagèrent la terre.
14:6	Or les fils de Yéhouda s'approchèrent de Yéhoshoua à Guilgal. Kaleb, fils de Yephounné, le Qenizzien, lui dit : Tu sais la parole que YHWH a déclarée à Moshé, homme d'Elohîm, à mon sujet et à ton sujet à Qadesh-Barnéa<!--No. 14:24, 32:12 ; De. 1:36.-->.
14:7	J'étais fils de 40 ans quand Moshé, serviteur de YHWH, m'envoya à Qadesh-Barnéa pour espionner la terre, et je lui fis un rapport avec droiture de cœur.
14:8	Mes frères qui étaient montés avec moi firent fondre le cœur du peuple, mais moi j’étais pleinement<!--remplir, être plein, être accompli, consacrer.--> derrière YHWH, mon Elohîm.
14:9	Et ce jour-là Moshé jura, en disant : Si la terre que ton pied a foulée ne deviens pas ton héritage à perpétuité, pour toi et pour tes fils, parce que tu as été pleinement derrière YHWH, mon Elohîm !
14:10	Et maintenant voici, YHWH m'a fait vivre comme il l'a dit. Il y a déjà 45 ans que YHWH déclarait cette parole à Moshé, lorsque Israël marchait dans le désert. Et maintenant voici, je suis aujourd'hui fils de 85 ans.
14:11	Aujourd’hui encore, je suis fort comme au jour où Moshé m'avait envoyé : telle était ma force alors, telle est ma force maintenant, pour la guerre, pour sortir et entrer.
14:12	Maintenant, donne-moi cette montagne, dont YHWH a parlé ce jour-là. Car tu as appris en ce jour qu'il s'y trouve des Anakim, et qu'il y a de grandes villes fortifiées. YHWH sera peut-être avec moi, et je les déposséderai, comme YHWH a dit.
14:13	Et Yéhoshoua bénit Kaleb, fils de Yephounné, et lui donna Hébron pour héritage.
14:14	C’est pourquoi Hébron fut à Kaleb, fils de Yephounné, le Qenizzien, en héritage jusqu’à ce jour, parce qu’il était pleinement derrière YHWH, l'Elohîm d'Israël.
14:15	Le nom d'Hébron était auparavant Qiryath-Arba. Il avait été le plus grand homme parmi les Anakim. La terre fut en paix et sans guerre.

## Chapitre 15

### Le territoire de Yéhouda

15:1	Il y eut le lot pour la tribu des fils de Yéhouda, pour leurs familles, vers la frontière d'Édom, jusqu'au désert de Tsin au sud, à l'extrémité du Théman.
15:2	Leur frontière du midi était depuis l'extrémité de la Mer Salée, depuis la langue<!--Le mot langue signifie également « une baie de la forme d'une langue ».--> qui tourne vers le midi.
15:3	Elle sortait vers le midi de la montée d'Akrabbim, et passait vers Tsin et montait au midi de Qadesh-Barnéa. Elle passait de là par Hetsron, montait vers Addar et tournait à Karkaa.
15:4	Elle passait par Atsmon et sortait au torrent d'Égypte. Les extrémités de cette frontière étaient vers la mer. Ce sera là votre frontière, du côté du midi.
15:5	La frontière vers l'orient était la Mer Salée jusqu'à l'embouchure du Yarden. La frontière du côté du nord partait de la langue<!--Voir commentaire en Jos. 15:2.--> de mer où est l'embouchure du Yarden.
15:6	La frontière montait jusqu'à Beth-Hoglah et passait du côté du nord de Beth-ha-`Arabah. La frontière montait jusqu'à la pierre de Bohan, fils de Reouben.
15:7	La frontière montait vers Debir, depuis la vallée d'Acor, et même vers le nord, du côté de Guilgal, qui est vis-à-vis de la montée d'Adoummim, au sud du torrent. La frontière passait près des eaux d'En-Shémesh et ses extrémités étaient à En-Roguel.
15:8	La frontière montait de là par la vallée de Ben-Hinnom, au côté du midi de Yebous, qui est Yeroushalaim, puis cette frontière montait jusqu'au sommet de la montagne, qui est vis-à-vis de la vallée de Hinnom, à l'occident, à l'extrémité de la vallée des géants, au nord.
15:9	La frontière fut tracée du sommet de la montagne vers la source des eaux de Nephthoach, et allait vers les villes de la montagne d'Éphron. La frontière fut tracée vers Ba`alah, qui est Qiryath-Yéarim.
15:10	La frontière tournait de Ba`alah vers l'occident, jusqu'à la montagne de Séir, et elle passait vers le côté nord de la montagne de Ye`ariym, à Kesalon, puis descendait à Beth-Shémesh et passait par Timnah.
15:11	La frontière sortait vers le côté d'Ékron, vers le nord. La frontière fut tracée vers Shicron, passait par la montagne de Ba`alah et allait jusqu'à Yabneel. Les extrémités de cette frontière étaient vers la mer.
15:12	La frontière de la mer, la Grande Mer est la frontière. Voilà la frontière des fils de Yéhouda autour, selon leurs familles.
15:13	On donna à Kaleb, fils de Yephounné, une part au milieu des fils de Yéhouda, comme YHWH l'avait ordonné à Yéhoshoua : Qiryath-Arba, le père d'Anak, qui est Hébron.
15:14	Kaleb en déposséda les trois fils d'Anak : Sheshaï, Ahiman, et Talmaï, enfants d'Anak.
15:15	Et de là il monta contre les habitants de Debir. Debir s'appelait autrefois Qiryath-Sépher.
15:16	Kaleb dit : Je donnerai ma fille Akcah pour femme à celui qui battra Qiryath-Sépher et la prendra<!--Jg. 1:12-14.-->.
15:17	Othniel, fils de Qenaz, frère de Kaleb, la prit et il lui donna sa fille Akcah pour femme.
15:18	Il arriva que comme elle s'en allait, elle l'incita à demander à son père un champ. Puis elle descendit impétueusement de dessus son âne et Kaleb lui dit : Qu'as-tu ?
15:19	Elle dit : Donne-moi un présent, puisque tu m'as donné une terre du sud, donne-moi aussi des sources d'eau. Et il lui donna les sources supérieures et les sources inférieures.
15:20	Tel fut l'héritage de la tribu des fils de Yéhouda, selon leurs familles.
15:21	Les villes situées dans la contrée du midi, à l'extrémité de la tribu des fils de Yéhouda, près de la frontière d'Édom, étaient : Kabtseel, Éder, Yagour,
15:22	Qiynah, Diymownah, Ad`adah
15:23	Kédesh, Hatsor, Yithnan,
15:24	Ziyph, Thélem, Bealoth,
15:25	Hatsor-Hadattah, Qeriyoth-Hetsron qui est Hatsor,
15:26	Amam, Shema, Moladah,
15:27	Hatsar-Gaddah, Heshmon, Beth-Paleth,
15:28	Hatsar-Shoual, Beer-Shéba, Bizyotheyah,
15:29	Ba`alah, Iyîm, Atsem,
15:30	Eltholad, Keciyl, Hormah,
15:31	Tsiklag, Madmannah, Sansannah,
15:32	Lebaoth, Shilchiym, Aïn et Rimmon. En tout : 29 villes et leurs villages.
15:33	Dans la plaine : Eshthaol, Tsor`ah, Ashnah,
15:34	Zanoach, En-Gannim, Tappouah, Énam,
15:35	Yarmouth, Adoullam, Soco, Azéqah,
15:36	Shaaraïm, Adithaïm, Gederah et Gederothayim : 14 villes et leurs villages.
15:37	Tsenan, Hadashah, Migdal-Gad,
15:38	Dilean, Mitspé, Yoktheel,
15:39	Lakis, Botskath, Églon,
15:40	Cabbon, Lachmas, Kithlish,
15:41	Guedéroth, Beth-Dagon, Na`amah, et Maqqédah : 16 villes et leurs villages.
15:42	Libnah, Éther, Ashan,
15:43	Yiphtah, Ashnah, Netsib,
15:44	Qe'iylah, Aczib et Maréshah : 9 villes et leurs villages.
15:45	Ékron, ses filles et ses villages.
15:46	Depuis Ékron et à l'occident, tout ce qui est à côté d'Asdod et leurs villages.
15:47	Asdod, ses filles et ses villages, Gaza, ses filles et ses villages, jusqu'au torrent d'Égypte, et à la grande mer qui sert de frontière.
15:48	Dans la montagne : Shamir, Yattiyr, Soco,
15:49	Dannah, Qiryath-Sanna, qui est Debir,
15:50	Anab, Eshthemo, Anim,
15:51	Goshen, Holon, et Guilo : 11 villes et leurs villages.
15:52	Arab, Doumah, Eshean,
15:53	Yanoum, Beth-Tappouah, Aphéka,
15:54	Houmeta, Qiryath-Arba, qui est Hébron et Tsior : 9 villes et leurs villages.
15:55	Maon, Carmel, Ziyph, Yuttah,
15:56	Yizre`e'l, Yoqde`am, Zanoach,
15:57	Qayin, Guibea, et Timnah : 10 villes et leurs villages.
15:58	Halhoul, Beth-Tsour, Guedor,
15:59	Maarath, Beth-Anoth, et Elthekon : 6 villes et leurs villages.
15:60	Qiryath-Baal, qui est Qiryath-Yéarim, et Rabba : 2 villes et leurs villages.
15:61	Dans le désert : Beth-ha-`Arabah, Middin, Sekakah,
15:62	Nibshan, Ir-Hammélach, et En-Guédi : 6 villes et leurs villages.
15:63	Les fils de Yéhouda ne purent pas déposséder les Yebousiens qui habitaient à Yeroushalaim, c'est pourquoi les Yebousiens ont habité avec les fils de Yéhouda à Yeroushalaim jusqu'à ce jour.

## Chapitre 16

### Le territoire d'Éphraïm

16:1	Le lot sortit pour les fils de Yossef depuis le Yarden de Yeriycho, vers les eaux de Yeriycho, à l'est, le désert qui monte de Yeriycho dans la montagne de Béth-El.
16:2	Elle sortait de Béth-El à Louz, puis passait vers la frontière des Arkiens jusqu'à Atharoth.
16:3	Elle descendait vers l'occident, vers la frontière des Yaphléthiens, jusqu'à celle de Beth-Horon la basse et jusqu'à Guézer. Ses extrémités étaient vers la mer.
16:4	Tel fut l’héritage des fils de Yossef, Menashè et Éphraïm.
16:5	La frontière des fils d'Éphraïm fut selon leurs familles : la frontière de leur héritage était à l'est, Atharoth-Addar, jusqu'à Beth-Horon la haute.
16:6	La frontière allait vers la mer à Micmethath, au nord. La frontière tournait vers l'est jusqu'à Thaanath-Shiloh, et passait devant elle du côté est, jusqu'à Yanoach.
16:7	Elle descendait de Yanoach à Atharoth et à Na`arah, atteignait Yeriycho et allait jusqu'au Yarden.
16:8	De Tappouah, la frontière allait vers l'occident, jusqu'au torrent de Qanah. Son extrémité était vers la mer. Ce fut là l'héritage de la tribu des fils d'Éphraïm, selon leurs familles.
16:9	Les fils d'Éphraïm avaient aussi des villes séparées au milieu de l'héritage des fils de Menashè, toutes ces villes, avec leurs villages.
16:10	Or ils ne dépossédèrent pas les Kena'ânéens qui habitaient à Guézer, c'est pourquoi les Kena'ânéens ont habité parmi Éphraïm jusqu'à ce jour, mais ils furent assujettis à un tribut<!--Jg. 1:29 ; 1 R. 9:16.-->.

## Chapitre 17

### Le territoire de Menashè (Manassé)

17:1	Il y eut un lot pour la tribu de Menashè, car il était le premier-né de Yossef. Quant à Makir, premier-né de Menashè et père de Galaad, parce qu'il devint un homme de guerre, Galaad et Bashân étaient pour lui.
17:2	Puis ce fut pour les fils de Menashè, ceux qui restent, pour leurs familles, pour les fils d'Abiézer, pour les fils de Hélek, pour les fils d'Asriel, pour les fils de Shekem, pour les fils de Hépher et pour les fils de Shemida. Ce sont là les fils de Menashè, les fils de Yossef, les mâles, selon leurs familles.
17:3	Tselophchad, fils de Hépher, fils de Galaad, fils de Makir, fils de Menashè, n'avait pas de fils, mais seulement des filles dont voici les noms : Machlah, No`ah, Hoglah, Milkah et Tirtsah.
17:4	Elles vinrent se présenter en face du prêtre Èl’azar, en face de Yéhoshoua, fils de Noun et en face des princes, en disant : YHWH a ordonné à Moshé de nous donner un héritage parmi nos frères. C'est pourquoi on leur donna un héritage parmi les frères de leur père, sur la bouche de YHWH<!--No. 27:7, 36:2.-->.
17:5	Et il échut à Menashè dix portions, outre la terre de Galaad et de Bashân, qui est de l'autre côté du Yarden.
17:6	Car les filles de Menashè eurent un héritage parmi ses fils, et la terre de Galaad fut pour les autres fils de Menashè.
17:7	La frontière de Menashè fut d'Asher à Micmethath, qui est en face de Shekem, et cette frontière allait vers le sud, vers les habitants d'Eyn-Tappouah.
17:8	Or la terre de Tappouah était à Menashè, mais Tappouah, à la frontière de Menashè, était aux fils d'Éphraïm.
17:9	De là, cette frontière descendait au torrent de Qanah, au sud du torrent. Ces villes étaient à Éphraïm parmi les villes de Menashè. La frontière de Menashè était au côté du nord du torrent et ses extrémités étaient vers la mer.
17:10	Au sud, c’était à Éphraïm, et au nord, à Menashè, et la mer devint sa frontière. Ils touchaient, au nord, à Asher et, à l’est, à Yissakar.
17:11	Menashè avait dans Yissakar et dans Asher : Beth-Shean et ses filles, Yible`am et ses filles, les habitants de Dor et ses filles, les habitants d'En-Dor et ses filles, les habitants de Thaanac et ses filles, les habitants de Meguiddo et ses filles, qui sont trois contrées.
17:12	Les fils de Menashè ne purent pas prendre possession de ces villes, et les Kena'ânéens consentirent à habiter sur la même terre.
17:13	Il arriva que, lorsque les fils d'Israël se furent fortifiés, ils réduisirent les Kena'ânéens à la servitude, mais les déposséder, ils ne les dépossédèrent pas.
17:14	Les fils de Yossef parlèrent à Yéhoshoua et dirent : Pourquoi nous as-tu donné en héritage un seul lot et une seule portion, vu que nous sommes un peuple nombreux, et que YHWH nous a bénis jusqu'à présent ?
17:15	Yéhoshoua leur dit : Si tu es un peuple nombreux, monte à la forêt et coupe-la pour toi, en terre des Phéréziens et des géants, si la montagne d'Éphraïm est trop étroite pour toi.
17:16	Les fils de Yossef dirent : Cette montagne ne sera pas suffisante pour nous, et tous les Kena'ânéens qui habitent la vallée ont des chars de fer, ceux qui sont à Beth-Shean et ses filles, et ceux qui habitent dans la vallée de Yizre`e'l<!--Jg. 1:19, 4:3.-->.
17:17	Yéhoshoua parla à la maison de Yossef, à Éphraïm et à Menashè et dit : Vous êtes un peuple nombreux et vous avez de grandes forces, vous n'aurez pas qu'une seule part.
17:18	Oui, la montagne sera à vous, oui, c'est une forêt que vous couperez et dont les extrémités seront à vous. Oui, vous déposséderez les Kena'ânéens ! Oui, ils ont des chars de fer, oui, ils sont forts !

## Chapitre 18

### La tente de réunion à Shiyloh

18:1	Toute l'assemblée des fils d'Israël se rassembla à Shiyloh<!--Shiyloh fut pendant la période des Juges le centre religieux d'Israël car c'est dans cette ville que l'on avait déposé l'arche jusqu'à ce que le roi David l'amène à Yeroushalaim (Jos. 18:1 ; 2 S. 6 ; 1 Ch. 15:3). Durant le schisme, Shiyloh, située en Samarie, fit office de capitale du royaume du nord. La ville fut finalement détruite par les Philistins aux alentours de 1050 av. J.-C.--> et ils firent demeurer là la tente de réunion. La terre était assujettie en face d'eux.
18:2	Il restait sept tribus parmi les fils d'Israël dont l'héritage n’a pas été partagé.
18:3	Yéhoshoua dit aux fils d'Israël : Jusqu'à quand négligerez-vous de prendre possession de la terre que YHWH, l'Elohîm de vos pères, vous a donnée ?
18:4	Donnez-vous trois hommes par tribu, et je les enverrai. Ils se lèveront, traverseront la terre, l’enregistreront en vue de l'héritage, puis ils reviendront vers moi.
18:5	Ils la partageront en sept parts. Yéhouda restera dans ses limites au midi, et la maison de Yossef restera dans ses limites au nord.
18:6	Et vous, vous enregistrerez la terre en sept parts, puis faites-les venir à moi, ici ! Je tirerai pour vous le sort ici, en face de YHWH, notre Elohîm.
18:7	Car il n'y aura pas de part pour les Lévites au milieu de vous, parce que la prêtrise de YHWH est leur héritage. Quant à Gad et à Reouben, et à la demi-tribu de Menashè, ils ont reçu leur héritage de l'autre côté du Yarden, vers l'est, que Moshé, serviteur de YHWH, leur a donné.
18:8	Ces hommes se levèrent et s'en allèrent pour enregistrer la terre, Yéhoshoua leur donna cet ordre, en disant : Allez et traversez la terre, et enregistrez-la, puis revenez vers moi ! Ici, je jetterai pour vous le sort, en face de YHWH, à Shiyloh.
18:9	Ces hommes s'en allèrent et parcoururent la terre. Ils l'enregistrèrent selon les villes, en sept parts, dans un livre, puis ils revinrent vers Yéhoshoua dans le camp à Shiyloh.
18:10	Yéhoshoua jeta le sort pour eux à Shiyloh en face de YHWH, et Yéhoshoua partagea la terre entre les fils d'Israël, selon leurs portions.

### Le territoire de Benyamin (Benjamin)

18:11	Alors monta le sort de la tribu des fils de Benyamin selon leurs familles. La frontière de leur sort sortit entre les fils de Yéhouda et les fils de Yossef.
18:12	Du côté nord, leur frontière fut depuis le Yarden. La frontière montait à côté de Yeriycho vers le nord, puis montait dans la montagne vers l'occident et ses extrémités étaient au désert de Beth-Aven.
18:13	La frontière passait de là vers Louz, à côté de Louz, qui est Béth-El au midi. La frontière descendait à Hatroth-Addar, près de la montagne qui est du côté du midi de Beth-Horon la basse.
18:14	La frontière s'étendait et tournait du côté occidental vers le midi, depuis la montagne qui est vis-à-vis de Beth-Horon, vers le midi. Ses extrémités étaient à Qiryath-Baal, qui est Qiryath-Yéarim, ville des fils de Yéhouda. C'est là le côté de la mer.
18:15	Mais le côté du midi est l'extrémité de Qiryath-Yéarim. La frontière allait vers l'occident, puis elle allait à la fontaine des eaux de Nephtoah.
18:16	La frontière descendait à l'extrémité de la montagne qui est vis-à-vis de la vallée de Ben-Hinnom, dans la vallée des géants, vers le nord. Elle descendait par la vallée de Hinnom, sur le côté méridional des Yebousiens, puis descendait jusqu'à En-Roguel.
18:17	Elle s'étendait au nord, et allait à En-Shémesh, de là à Gueliloth, qui est vis-à-vis de la montée d'Adoummim, et descendait à la pierre de Bohan, fils de Reouben.
18:18	Elle passait sur le côté nord en face de la région aride, et descendait vers la région aride,
18:19	La frontière passait à côté de Beth-Hoglah vers le nord. Les extrémités de la frontière étaient vers la langue<!--Voir commentaire en Jos. 15:2.--> de la Mer Salée, au nord, et vers l'embouchure du Yarden, au midi. C'était la frontière du midi.
18:20	Le Yarden bornait du côté de l'orient. Ce fut là l'héritage des fils de Benyamin avec ses frontières tout autour, selon leurs familles.
18:21	Les villes de la tribu des fils de Benyamin, selon leurs familles, étaient : Yeriycho, Beth-Hoglah, Émek-Ketsits,
18:22	Beth-ha-`Arabah, Tsemaraïm, Béth-El,
18:23	Avvim, Parah, Ophrah,
18:24	Kephar-Ammonaï, Ophni et Guéba, soit 12 villes et leurs villages.
18:25	Gabaon, Ramah, Beéroth,
18:26	Mitspé, Kephiyrah, Motsah,
18:27	Rékem, Yirpe'el, Tar'alah,
18:28	Tséla, Éleph, Yebous, qui est Yeroushalaim, Guibeath et Qiryath-Yéarim : 14 villes et leurs villages. Tel fut l'héritage des fils de Benyamin selon leurs familles.

## Chapitre 19

### Le territoire de Shim’ôn (Siméon)

19:1	Le deuxième sort sortit pour Shim’ôn, pour la tribu des fils de Shim’ôn selon leurs familles. Leur héritage était parmi l'héritage des fils de Yéhouda<!--Ge. 49:5-7.-->.
19:2	Ils eurent dans leur héritage Beer-Shéba, Shéba, Moladah,
19:3	Hatsar-Shoual, Balah, Atsem,
19:4	Eltholad, Betoul, Hormah,
19:5	Tsiklag, Beth-Marcaboth, Hatsar-Sousah,
19:6	Beth-Lebaoth et Sharouhen : 13 villes et leurs villages.
19:7	Aïn, Rimmon, Éther, et Ashan : 4 villes et leurs villages,
19:8	et tous les villages qui étaient autour de ces villes-là jusqu'à Baalath-Beer, qui est Ramath du midi. Tel fut l'héritage de la tribu des fils de Shim’ôn, selon leurs familles.
19:9	L'héritage des fils de Shim’ôn fut dans le territoire des fils de Yéhouda. En effet, la portion des fils de Yéhouda était trop grande pour eux. Voilà pourquoi les fils de Shim’ôn reçurent leur héritage parmi le leur.

### Le territoire de Zebouloun (Zabulon)

19:10	Le troisième sort monta pour les fils de Zebouloun selon leurs familles. La frontière de leur héritage fut jusqu’à Sarid.
19:11	Leur frontière montait à l'occident vers Mareala et touchait Dabbésheth, puis le torrent qui est vis-à-vis de Yoqne`am.
19:12	Elle revenait de Sarid vers l'orient, vers le soleil levant, jusqu'à la frontière de Kisloth-Tabor, puis allait vers Dabrath et montait à Yaphiya.
19:13	De là elle passait à l'est par Guittha-Hépher, par Ittha-Katsin, puis allait vers Rimmon, jusqu'à Néa.
19:14	La frontière tournait du côté du nord vers Hannathon. Ses extrémités allaient vers la vallée de Yiphtah-El.
19:15	Avec Katthath, Nahalal, Shimron, Yidalah, et Bethléhem. Il y avait 12 villes et leurs villages.
19:16	Tel fut l'héritage des fils de Zebouloun selon leurs familles, ces villes-là, et leurs villages.

### Le territoire de Yissakar (Issacar)

19:17	Le quatrième sort sortit pour Yissakar, pour les fils de Yissakar selon leurs familles.
19:18	Leur frontière fut Yizre`e'l, Kesulloth, Shouném,
19:19	Hapharaïm, Shion, Anacharath,
19:20	Rabbith, Qishyon, Abets,
19:21	Rémeth, En-Gannim, En-Hadda et Beth-Patsets.
19:22	La frontière touchait Thabor, Shachatsima et Beth-Shémesh. Les extrémités de leur frontière étaient au Yarden : 16 villes et leurs villages.
19:23	Tel fut l'héritage de la tribu des fils de Yissakar, selon leurs familles, ces villes-là et leurs villages.

### Le territoire d'Asher

19:24	Le cinquième sort sortit pour la tribu des fils d'Asher selon leurs familles.
19:25	Et leur frontière fut Helkath, Hali, Béthen, Acshaph,
19:26	Allammélek, Amead et Misheal. Elle touchait le Carmel vers la mer, et Shiychor-Libnath.
19:27	Elle revenait vers l'est, à Beth-Dagon, et touchait Zebouloun et la vallée de Yiphtah-El, vers le nord de Beth-Émek et de Neïel, puis allait vers Kaboul, au nord,
19:28	et vers Ébron, Rehob, Hammon et Qanah, jusqu'à Sidon la grande.
19:29	La frontière revenait vers Ramah, jusqu'à la ville forte de Tyr. Cette frontière revenait vers Hosah et ses extrémités étaient vers la mer, dans le territoire d'Aczib.
19:30	Avec Oummah, Aphek et Rehob : 22 villes et leurs villages.
19:31	Tel fut l'héritage de la tribu des fils d'Asher, selon leurs familles : ces villes-là et leurs villages.

### Le territoire de Nephthali

19:32	Le sixième sort sortit pour les fils de Nephthali selon leurs familles.
19:33	Leur frontière fut depuis Héleph, depuis Allon par Tsaanannim, Adami-Nékeb et Yabneel, jusqu'à Lakkoum. Ses extrémités allaient au Yarden.
19:34	La frontière revenait vers l'occident, vers Aznoth-Thabor, et allait de là à Houqoq. Du côté du midi elle touchait Zebouloun et du côté d'occident touchait Asher et Yéhouda, près du Yarden, vers le soleil levant.
19:35	Les villes fortifiées étaient : Tsiddim, Tser, Hammath, Rakkath, Kinnéreth,
19:36	Adamah, Ramah, Hatsor,
19:37	Kédesh, Édréi, En-Hatsor,
19:38	Yirôn, Migdal-El, Horem, Beth-Anath et Beth-Shémesh ; 19 villes et leurs villages.
19:39	Tel fut l'héritage de la tribu des fils de Nephthali, selon leurs familles : ces villes-là et leurs villages.

### Le territoire de Dan

19:40	Le septième sort sortit pour la tribu des fils de Dan selon leurs familles.
19:41	La frontière de leur héritage fut, Tsor`ah, Eshthaol, Ir-Shémesh,
19:42	Shaalabbin, Ayalon, Yithlah,
19:43	Eylon, Timnata, Ékron,
19:44	Elteqé, Guibbethon, Baalath,
19:45	Yehoud, Bené-Berak, Gath-Rimmon,
19:46	Mé-Yarkon et Rakkon, avec le territoire qui est vis-à-vis de Yapho.
19:47	Le territoire des fils de Dan sortait hors de chez eux. C'est pourquoi les fils de Dan montèrent et combattirent contre Léshem. Ils s'en emparèrent et la frappèrent à bouche d’épée. Ils en prirent possession, s'y établirent et ils appelèrent Léshem, Dan, du nom de Dan leur père.
19:48	Tel fut l'héritage de la tribu des fils de Dan selon leurs familles : ces villes-là et leurs villages.

### Yéhoshoua (Josué) reçoit Timnath-Sérach

19:49	Lorsqu’ils eurent achevé de partager la terre selon ses frontières, les fils d'Israël donnèrent à Yéhoshoua, fils de Noun, une possession au milieu d'eux.
19:50	Sur la bouche de YHWH, ils lui donnèrent la ville qu'il demanda, Timnath-Sérach, dans la montagne d'Éphraïm. Il rebâtit la ville, et y habita.
19:51	Ce sont là les héritages que le prêtre Èl’azar, Yéhoshoua, fils de Noun, et les têtes de pères des tribus des fils d'Israël partagèrent par le sort à Shiyloh, en face de YHWH, à l'entrée de la tente de réunion, et ils achevèrent ainsi le partage de la terre.

## Chapitre 20

### Les six villes de refuge<!--No. 35.-->

20:1	YHWH parla à Yéhoshoua et dit :
20:2	Parle aux fils d'Israël et dis : Établissez-vous les villes de refuge<!--Ex. 21:13 ; No. 35:6. Ces villes préfigurent le Mashiah notre Refuge (Mt. 11:28-30 ; Ro. 8:1).--> dont je vous ai parlé par la main de Moshé,
20:3	afin que le meurtrier qui aura tué une âme involontairement, sans le savoir, puisse s'y enfuir. Elles deviendront pour vous un refuge contre le racheteur du sang.
20:4	Il s’enfuira dans l'une de ces villes. Il se tiendra à l'entrée de la porte de la ville et parlera de son cas aux oreilles des anciens de cette ville. Ceux-ci le recueilleront auprès d'eux dans la ville et lui donneront un lieu chez eux pour qu'il y habite.
20:5	Si le racheteur du sang le poursuit, ils ne livreront pas le meurtrier entre ses mains, car c'est sans intention qu'il a tué son prochain et non parce qu'il le haïssait d'hier et d'avant-hier.
20:6	Il habitera dans cette ville jusqu’à ce qu’il se tienne en face de l’assemblée pour le jugement, jusqu'à la mort du grand-prêtre qui sera en ce temps-là. Alors le meurtrier s'en retournera, et reviendra dans sa ville et dans sa maison, dans la ville d'où il s'était enfui<!--Ex. 21:13 ; No. 35:9-34 ; De. 19.-->.
20:7	Ils consacrèrent Kédesh, en Galilée, dans la montagne de Nephthali, Shekem dans la montagne d'Éphraïm et Qiryath-Arba, qui est Hébron, dans la montagne de Yéhouda.
20:8	Et au-delà du Yarden, à l'est de Yeriycho, ils choisirent Betser, dans la tribu de Reouben, dans le désert, dans la plaine, Ramoth en Galaad, dans la tribu de Gad, et Golan en Bashân, dans la tribu de Menashè<!--De. 4:43.-->.
20:9	Telles furent les villes assignées à tous les fils d'Israël et à l'étranger demeurant parmi eux, afin que quiconque aurait tué une âme involontairement puisse s'y enfuir et ne meure pas de la main du racheteur du sang, jusqu’à ce qu’il se tienne en face de l’assemblée.

## Chapitre 21

### Les 48 villes des Lévites

21:1	Les têtes des pères des Lévites s'approchèrent d'Èl’azar, le prêtre, de Yéhoshoua, fils de Noun, et des têtes des pères des tribus des fils d'Israël.
21:2	Elles leur parlèrent à Shiyloh, en terre de Kena'ân, en disant : YHWH a ordonné par la main de Moshé de nous donner des villes pour habiter, et leurs faubourgs pour nos bêtes<!--No. 35:2-3.-->.
21:3	Les fils d'Israël donnèrent aux Lévites, sur leur héritage, ces villes et leurs faubourgs, sur la bouche de YHWH.
21:4	On sortit le sort pour les familles des Qehathites, et les Lévites, fils d'Aaron, le prêtre, eurent par le sort de la part de la tribu de Yéhouda, de la tribu de Shim’ôn et de la tribu de Benyamin, 13 villes.
21:5	Pour les fils de Qehath qui restaient, il y eut, par le sort, 10 villes sur les familles de la tribu d'Éphraïm, sur la tribu de Dan, et sur la demi-tribu de Menashè.
21:6	Pour les fils de Guershon, il y eut, par le sort, 13 villes sur les familles de la tribu de Yissakar, sur la tribu d'Asher, sur la tribu de Nephthali, et sur la demi-tribu de Menashè en Bashân.
21:7	Pour les fils de Merari, d’après leurs familles, il y eut 12 villes, sur la tribu de Reouben, sur la tribu de Gad, et sur la tribu de Zebouloun.
21:8	Les fils d'Israël donnèrent par le sort aux Lévites ces villes-là avec leurs faubourgs, comme YHWH l'avait ordonné par la main de Moshé.
21:9	C’est dans la tribu des fils de Yéhouda et dans la tribu des fils de Shim’ôn qu’on donna ces villes, qui furent appelées par leur nom,
21:10	et elles furent pour ceux des fils d'Aaron, qui étaient des familles des Qehathites, fils de Lévi, car le premier sort fut pour eux.
21:11	On leur donna Qiryath-Arba, le père d'Anak, qui est Hébron, dans la montagne de Yéhouda, avec ses faubourgs tout autour.
21:12	Mais quant au territoire de la ville, et à ses villages, on les donna à Kaleb, fils de Yephounné, pour sa propriété.
21:13	On donna aux fils d'Aaron, le prêtre, les villes de refuge pour les meurtriers, Hébron avec ses faubourgs, Libnah avec ses faubourgs.
21:14	Yattiyr avec ses faubourgs, Eshthemoa avec ses faubourgs,
21:15	Holon avec ses faubourgs, Debir avec ses faubourgs,
21:16	Aïn avec ses faubourgs, Yuttah avec ses faubourgs, Beth-Shémesh avec ses faubourgs : 9 villes de ces deux tribus.
21:17	Et de la tribu de Benyamin, Gabaon avec ses faubourgs, Guéba avec ses faubourgs,
21:18	Anathoth avec ses faubourgs, Almon avec ses faubourgs : 4 villes.
21:19	Toutes les villes des prêtres, fils d'Aaron, furent 13 villes avec leurs faubourgs.
21:20	Or quant aux familles des fils de Qehath, Lévites, qui étaient le reste des fils de Qehath, il y eut dans leur sort des villes de la tribu d'Éphraïm.
21:21	On leur donna les villes de refuge pour les meurtriers, Shekem avec ses faubourgs dans la montagne d'Éphraïm, et Guézer avec ses faubourgs,
21:22	Qibtsayim avec ses faubourgs, Beth-Horon avec ses faubourgs : 4 villes.
21:23	Et de la tribu de Dan, Elteqé avec ses faubourgs, Guibbethon avec ses faubourgs,
21:24	Ayalon avec ses faubourgs, Gath-Rimmon avec ses faubourgs : 4 villes.
21:25	Et de la demi-tribu de Menashè, Thaanac avec ses faubourgs, Gath-Rimmon avec ses faubourgs : 2 villes.
21:26	Total des villes : 10 villes avec leurs faubourgs pour les familles des fils restants de Qehath.
21:27	On donna aussi aux fils de Guershon, d'entre les familles des Lévites : De la demi-tribu de Menashè les villes de refuge pour les meurtriers, Golan en Bashân avec ses faubourgs et Beeshthra avec ses faubourgs : 2 villes.
21:28	Et de la tribu de Yissakar, Qishyon, avec ses faubourgs, Dabrath, avec ses faubourgs,
21:29	Yarmouth, avec ses faubourgs, En-Gannim, avec ses faubourgs : 4 villes.
21:30	Et de la tribu d'Asher, Misheal, avec ses faubourgs, Abdon, avec ses faubourgs,
21:31	Helkath, avec ses faubourgs, et Rehob, avec ses faubourgs : 4 villes,
21:32	et de la tribu de Nephthali, les villes de refuge pour les meurtriers, Kédesh en Galilée avec ses faubourgs, Hammoth-Dor avec ses faubourgs, et Karthan avec ses faubourgs : 3 villes.
21:33	Total des villes des Guershonites, selon leurs familles : 13 villes et leurs faubourgs.
21:34	On donna aussi au reste des Lévites, qui appartenaient aux familles des fils de Merari : De la tribu de Zebouloun, Yoqne`am avec ses faubourgs, Kartha avec ses faubourgs,
21:35	Dimna avec ses faubourgs, et Nahalal avec ses faubourgs : 4 villes.
21:36	Et de la tribu de Reouben, Betser avec ses faubourgs, et Yahtsa avec ses faubourgs,
21:37	Qedémoth avec ses faubourgs, et Méphaath avec ses faubourgs : 4 villes.
21:38	Et de la tribu de Gad, les villes de refuge pour les meurtriers, Ramoth en Galaad avec ses faubourgs, Mahanaïm avec ses faubourgs,
21:39	Hesbon avec ses faubourgs, Ya`azeyr avec ses faubourgs : en tout 4 villes.
21:40	Toutes les villes pour les fils de Merari, selon leurs familles, le reste des familles de Lévites, leur sort fut de 12 villes.
21:41	Toutes les villes des Lévites au milieu de la propriété des fils d'Israël : 48 villes et leurs faubourgs.
21:42	Ces villes devinrent chacune une ville avec ses faubourgs autour d'elle. Il en était ainsi de toutes ces villes-là.

### YHWH accomplit sa promesse

21:43	YHWH donna à Israël toute la terre qu'il avait juré de donner à leurs pères. Ils en prirent possession et y habitèrent<!--Elohîm accomplit toujours ses promesses (Jé. 1:12).-->.
21:44	YHWH leur donna du repos de tous côtés, selon tout ce qu'il avait juré à leurs pères et, de tous leurs ennemis, pas un homme ne tint devant eux. YHWH les livra entre leurs mains.
21:45	Il ne tomba pas une seule parole de toutes les bonnes paroles que YHWH avait déclarées à la maison d'Israël : tout arriva.

## Chapitre 22

### Reouben, Gad et la demi-tribu de Menashè retournent sur leurs terres

22:1	Alors Yéhoshoua appela les Reoubénites, les Gadites et la demi-tribu de Menashè.
22:2	Il leur dit : Vous avez gardé tout ce que Moshé, serviteur de YHWH, vous a prescrit, et vous avez obéi à ma voix dans tout ce que je vous ai ordonné.
22:3	Vous n'avez pas abandonné vos frères pendant ces jours nombreux, jusqu’à ce jour, et vous avez gardé les injonctions, les commandements de YHWH, votre Elohîm.
22:4	Et maintenant, YHWH, votre Elohîm a donné du repos à vos frères, comme il le leur avait dit. Maintenant, retournez et allez pour vous vers vos tentes, vers la terre de votre propriété, que Moshé, serviteur de YHWH, vous a donnée de l'autre côté du Yarden<!--No. 32:33 ; De. 3:13.-->.
22:5	Seulement soyez extrêmement sur vos gardes pour pratiquer les ordonnances et les lois que Moshé, serviteur de YHWH, vous a prescrites : Aimez YHWH, votre Elohîm, marchez dans toutes ses voies, gardez ses commandements, attachez-vous à lui et servez-le de tout votre cœur et de toute votre âme<!--De. 10:12-13, 29:8.-->.
22:6	Yéhoshoua les bénit, les renvoya, et ils allèrent vers leurs tentes.
22:7	Moshé avait donné à la moitié de la tribu de Menashè son héritage en Bashân. Yéhoshoua donna à l'autre moitié son héritage avec leurs frères de l'autre côté du Yarden vers l'occident. Yéhoshoua les renvoya dans leurs demeures et les bénit.
22:8	Il leur parla en disant : Vous retournez à vos demeures avec de grandes richesses, une très nombreuse quantité de bétail, avec une quantité considérable d'argent, d'or, de cuivre, de fer et de vêtements. Partagez avec vos frères le butin de vos ennemis.
22:9	Les fils de Reouben, les fils de Gad et la demi-tribu de Menashè s'en retournèrent et s’en allèrent de chez les fils d’Israël, de Shiyloh, en terre de Kena'ân, pour s'en aller en terre de Galaad, sur la terre de leur propriété, dont ils avaient été mis en possession selon le commandement de YHWH par la main de Moshé.

### L'autel de 3 tribus, sujet d'incompréhension

22:10	Arrivés aux frontières du Yarden, en terre de Kena'ân, les fils de Reouben, les fils de Gad, et la demi-tribu de Menashè bâtirent un autel au bord du Yarden. C'était un autel de grande apparence.
22:11	Les fils d'Israël apprirent que l'on disait : Voici, les fils de Reouben, les fils de Gad, et la demi-tribu de Menashè ont bâti un autel en face de la terre de Kena'ân, sur les frontières du Yarden, du côté des fils d'Israël.
22:12	Les fils d'Israël apprirent cela, et toute l'assemblée des fils d'Israël se rassembla à Shiyloh, pour monter en bataille contre eux.
22:13	Les fils d'Israël envoyèrent vers les fils de Reouben, vers les fils de Gad, et vers la demi-tribu de Menashè, en terre de Galaad, Phinées, fils du prêtre Èl’azar,
22:14	et avec lui dix princes, un prince, un prince par maison de père de toutes les tribus d'Israël. C'étaient des hommes à la tête de la maison de leurs pères parmi les milliers d'Israël.
22:15	Ceux-ci vinrent vers les fils de Reouben, les fils de Gad et de la demi-tribu de Menashè en terre de Galaad, et leur parlèrent, en disant :
22:16	Ainsi parle toute l'assemblée de YHWH : Quelle est cette transgression, ce délit que vous avez commis contre l'Elohîm d'Israël, et pourquoi vous détournez-vous aujourd'hui de YHWH, en vous bâtissant un autel, pour vous rebeller aujourd'hui contre YHWH ?
22:17	Était-ce peu, pour nous, l'iniquité de Peor<!--Peor : No. 25:1-9.-->, dont nous ne sommes pas purifiés jusqu’à ce jour, quoiqu’il y ait eu une plaie sur l’assemblée de YHWH ?
22:18	Et vous vous détournez aujourd’hui de YHWH ! Il arrivera que, si vous vous rebellez aujourd’hui contre YHWH, demain sa colère s'enflamme contre toute l'assemblée d'Israël.
22:19	Toutefois, si la terre de votre propriété est souillée, passez sur la terre qui est la propriété de YHWH, où est fixé le tabernacle de YHWH, et ayez votre possession parmi nous, et ne vous rebellez pas contre YHWH, et ne soyez pas rebelles contre nous, en vous bâtissant un autel, outre l'autel de YHWH, notre Elohîm.
22:20	N’est-ce pas Akan<!--Akan : Jos. 7:1-26.-->, le fils de Zérach qui a commis un délit, une transgression au sujet des choses vouées à une entière destruction ? N’est-ce pas contre toute l’assemblée d’Israël qu’est venue la colère ? Et il n’a pas été le seul homme qui ait expiré dans son iniquité.
22:21	Les fils de Reouben, les fils de Gad, et la demi-tribu de Menashè répondirent et dirent aux chefs des milliers d'Israël :
22:22	El<!--Terme hébreu qui signifie « puissant ».-->, Elohîm<!--Terme hébreu qui signifie « juge, ange ».--> YHWH, El, Elohîm YHWH, lui le sait, et Israël, lui le saura, si c'est par rébellion et par infidélité envers YHWH, qu'il ne nous sauve pas en ce jour !
22:23	Si nous nous sommes bâti un autel pour nous détourner de YHWH, si c'est pour y faire monter des holocaustes ou des offrandes de grain, ou si c'est pour y faire des sacrifices d'offrande de paix, que YHWH lui-même nous en demande compte !
22:24	Si ce n’est plutôt par inquiétude de cette chose, que nous avons fait cela, en disant : Dans l’avenir vos fils parleront à nos fils, en disant : Qu'y a-t-il de commun entre vous et YHWH, l'Elohîm d'Israël ?
22:25	YHWH a mis le Yarden pour frontière entre nous et vous, fils de Reouben et fils de Gad. Pas de part pour vous en YHWH ! Et à cause de vos fils, nos fils cesseraient de craindre YHWH<!--Né. 2:20 ; Ac. 8:21.-->.
22:26	Nous nous sommes dit : Agissons maintenant, bâtissons un autel non pour des holocaustes ni pour des sacrifices,
22:27	mais afin qu'il soit un témoin entre nous et vous, et entre nos âges après nous, que nous voulons servir YHWH devant sa face par nos holocaustes et nos sacrifices d'expiation et d'offrande de paix, afin que vos fils ne disent pas un jour à nos fils : Pas de part pour vous en YHWH<!--Ge. 31:48.--> !
22:28	Nous avons dit : S’il arrive qu’il nous disent cela, à nous et à nos âges dans l’avenir, nous leur dirons : Voyez la forme de l'autel de YHWH qu'ont fait nos pères, non pour des holocaustes, ni pour des sacrifices, mais il est un témoin entre nous et vous.
22:29	Loin de nous de nous révolter contre YHWH et de nous détourner aujourd'hui de YHWH, en bâtissant un autel pour des holocaustes, pour des offrandes de grain, et pour des sacrifices, outre l'autel de YHWH, notre Elohîm, qui est devant son tabernacle !
22:30	Or après que le prêtre Phinées, et les princes de l'assemblée, les chefs des milliers d'Israël qui étaient avec lui, eurent entendu les paroles que les fils de Reouben, les fils de Gad, et les fils de Menashè leur dirent, cela parut bon à leurs yeux.
22:31	Et Phinées, fils du prêtre Èl’azar, dit aux fils de Reouben, aux fils de Gad, et aux fils de Menashè : Nous reconnaissons aujourd'hui que YHWH est au milieu de nous, puisque vous n'avez pas commis ce délit, cette transgression contre YHWH. Vous avez ainsi délivré les fils d'Israël de la main de YHWH.
22:32	Phinées, fils du prêtre Èl’azar, et les princes revinrent de chez les fils de Reouben et de chez les fils de Gad, de la terre de Galaad, de la terre de Kena'ân, vers les fils d'Israël, et ils leur rapportèrent la parole.
22:33	La parole fut bonne aux yeux des fils d'Israël. Ils bénirent Elohîm et ne parlèrent plus de monter en armes contre eux pour détruire la terre où habitaient les fils de Reouben, et les fils de Gad.
22:34	Les fils de Reouben et les fils de Gad appelèrent l’autel Ed. Oui, il est témoin entre nous, oui, YHWH est Elohîm.

## Chapitre 23

### Avertissements de Yéhoshoua (Josué)

23:1	Il arriva beaucoup de jours après que YHWH eut donné du repos à Israël devant tous ses ennemis d'alentour, que Yéhoshoua, étant devenu vieux et avancé en âge,
23:2	Yéhoshoua appela tout Israël, ses anciens, ses chefs, ses juges, ses commissaires et leur dit : Je suis devenu vieux, avancé en âge.
23:3	Vous avez vu tout ce que YHWH, votre Elohîm, a fait à toutes ces nations en face de vous. En effet, YHWH, votre Elohîm, est celui qui combat pour vous.
23:4	Voyez, je vous ai donné en héritage par le sort, selon vos tribus, ces nations qui sont restées, depuis le Yarden, et toutes les nations que j'ai exterminées, jusqu'à la grande mer vers le soleil couchant.
23:5	YHWH, votre Elohîm, les expulsera en face de vous et les dépossèdera. Vous prendrez possession de leur terre, comme YHWH votre Elohîm vous l'a dit<!--Ex. 14:14, 23:27 ; No. 33:53 ; De. 6:18-19.-->.
23:6	Soyez très forts pour garder et pratiquer tout ce qui est écrit dans le livre de la torah de Moshé, sans vous en détourner ni à droite ni à gauche<!--De. 5:32, 28:14.-->.
23:7	N'entrez pas chez ces nations qui sont restées parmi vous, ne mentionnez pas le nom de leurs elohîm, ne jurez pas par eux, ne les servez pas et ne vous prosternez pas devant eux<!--Ex. 23:13 ; De. 6:14, 12:3 ; Jé. 5:7.-->.
23:8	Mais attachez-vous à YHWH, votre Elohîm, comme vous l'avez fait jusqu'à ce jour<!--De. 11:22.-->.
23:9	C'est pour cela que YHWH a dépossédé devant vous des nations grandes et puissantes, aucun homme n’a tenu en face de vous jusqu’à ce jour.
23:10	Un seul homme d'entre vous en poursuivait 1 000, car YHWH, votre Elohîm, est celui qui combat pour vous, comme il vous l'a dit<!--Lé. 26:8 ; De. 32:30.-->.
23:11	Veillez attentivement sur vos âmes afin d'aimer YHWH, votre Elohîm.
23:12	Car si vous vous détournez et que vous vous attachez au reste de ces nations qui sont restées parmi vous, si vous vous mariez parmi eux, si vous venez parmi eux et eux parmi vous,
23:13	sachez, sachez que YHWH, votre Elohîm, ne continuera pas à déposséder ces nations en face de vous. Mais elles deviendront un piège pour vous et un leurre, un fouet dans vos côtés et des épines dans vos yeux, jusqu'à ce que vous ayez péri de dessus ce bon sol que YHWH, votre Elohîm, vous a donné<!--Ex. 23:33 ; De. 7:16 ; Jg. 2:3.-->.
23:14	Voici, je m'en vais aujourd'hui par le chemin de toute la Terre. Vous le savez, de tout votre cœur et de toute votre âme que pas une seule parole n'a échoué de toutes les bonnes paroles qu'avait déclarées YHWH, votre Elohîm, sur vous. Toutes sont arrivées pour vous, pas une seule de ces paroles n'a échoué<!--Jos. 21:45 ; 2 R. 10:10.-->.
23:15	Il arrivera que comme toutes les bonnes paroles que YHWH, votre Elohîm, avait déclarées sur vous sont arrivées, de même YHWH fera venir sur vous toutes les paroles mauvaises, jusqu'à ce qu'il vous ait exterminés de dessus ce bon sol que YHWH, votre Elohîm, vous a donné.
23:16	Si vous transgressez l'alliance que YHWH, votre Elohîm, vous a prescrite, et si vous allez servir d'autres elohîm et vous prosterner devant eux, les narines de YHWH s'enflammeront contre vous, et vous périrez rapidement de dessus cette bonne terre qu'il vous a donnée.

## Chapitre 24

### Yéhoshoua (Josué) rappelle à Israël son histoire

24:1	Yéhoshoua rassembla toutes les tribus d'Israël à Shekem, et il convoqua les anciens d'Israël, ses chefs, ses juges et ses commissaires, qui se présentèrent devant Elohîm.
24:2	Yéhoshoua dit à tout le peuple : Ainsi parle YHWH, l'Elohîm d'Israël : Vos pères, Térach, père d'Abraham et père de Nachor, ont anciennement habité de l'autre côté du fleuve, où ils servaient d'autres elohîm.
24:3	J'ai pris votre père Abraham de l'autre côté du fleuve, je lui ai fait parcourir toute la terre de Kena'ân, j'ai multiplié sa postérité et je lui ai donné Yitzhak<!--Ge. 12, 21:2-3.-->.
24:4	J'ai donné à Yitzhak, Yaacov et Ésav. J'ai donné à Ésav la montagne de Séïr, pour en prendre possession. Mais Yaacov et ses fils sont descendus en Égypte<!--Ge. 25:24, 36:6.-->.
24:5	J'ai envoyé Moshé et Aaron, et j'ai frappé l'Égypte par les fléaux que j'ai produits au milieu d'elle, puis je vous en ai fait sortir<!--Ex. 3:10.-->.
24:6	J'ai fait sortir vos pères hors de l'Égypte et vous êtes arrivés à la mer. Les Égyptiens ont poursuivi vos pères avec des chars et des cavaliers jusqu'à la Mer Rouge<!--Ex. 14:9.-->.
24:7	Ils ont crié vers YHWH, et il a mis des ténèbres entre vous et les Égyptiens. Il a ramené sur eux la mer et elle les a recouverts. Vos yeux ont vu ce que j'ai fait contre l'Égypte. Vous êtes restés dans le désert des jours nombreux.
24:8	Je vous ai conduits en terre des Amoréens qui habitaient de l'autre côté du Yarden et ils vous ont fait la guerre. Je les ai livrés entre vos mains, vous avez pris possession de leur terre et je les ai détruits devant vous.
24:9	Balak<!--Voir No. 22:2-14.-->, fils de Tsippor, roi de Moab, s'est levé et a fait la guerre à Israël. Il a envoyé appeler Balaam<!--Voir No. 22.-->, fils de Beor, pour qu'il vous maudisse.
24:10	Mais je n'ai pas voulu écouter Balaam. Il vous a bénis, il vous a bénis et je vous ai délivrés de la main de Balak.
24:11	Et vous avez passé le Yarden et vous êtes arrivés à Yeriycho. Les habitants de Yeriycho, les Amoréens, les Phéréziens, les Kena'ânéens, les Héthiens, les Guirgasiens, les Héviens et les Yebousiens vous ont fait la guerre. Je les ai livrés entre vos mains.
24:12	J'ai envoyé en face de vous les frelons, ils ont chassé en face de vous les deux rois des Amoréens. Ce n'était ni par ton épée, ni par ton arc<!--Ex. 23:28 ; De. 7:20.-->.
24:13	Je vous ai donné une terre que vous n'avez pas cultivée, des villes que vous n'avez pas bâties et que vous habitez, et vous mangez des vignes et des oliviers que vous n'avez pas plantés<!--De. 6:10 ; Ps. 105:44 ; Né. 9:25.-->.

### Le peuple choisit de servir YHWH

24:14	Maintenant, craignez YHWH, et servez-le avec intégrité et avec fidélité. Ôtez les elohîm que vos pères ont servis de l'autre côté du fleuve et en Égypte, et servez YHWH<!--1 S. 12:23-24 ; Ez. 20:7-44.-->.
24:15	Mais si c'est mal à vos yeux de servir YHWH, choisissez aujourd'hui qui vous voulez servir, ou les elohîm que servaient vos pères au-delà du fleuve, ou les elohîm des Amoréens dont vous habitez la terre. Mais moi et ma maison, nous servirons YHWH.
24:16	Le peuple répondit et dit : Loin de nous d'abandonner YHWH pour servir d'autres elohîm !
24:17	Car YHWH notre Elohîm, est celui qui nous a fait monter, nous et nos pères, hors de la terre d'Égypte, de la maison des esclaves. C'est lui qui a fait sous nos yeux ces grands signes, qui nous a gardés dans tout le chemin par lequel nous avons marché et parmi tous les peuples au sein desquels nous sommes passés.
24:18	YHWH a chassé en face de nous tous les peuples, et même les Amoréens qui habitaient cette terre. Nous servirons aussi YHWH, car il est notre Elohîm.
24:19	Yéhoshoua dit au peuple : Vous ne pourrez pas servir YHWH, car il est l'Elohîm Saint, il est le El jaloux, il ne pardonnera pas vos transgressions et vos péchés.
24:20	Oui, vous abandonnerez YHWH, vous servirez les elohîm des étrangers, il reviendra vous faire du mal, et il vous consumera après vous avoir fait du bien.
24:21	Le peuple dit à Yéhoshoua : Non ! Car nous servirons YHWH.
24:22	Yéhoshoua dit au peuple : Vous êtes témoins contre vous-mêmes que c'est vous qui avez choisi YHWH pour le servir. Ils dirent : Nous en sommes témoins.
24:23	Maintenant ôtez les elohîm étrangers qui sont au milieu de vous et tournez votre cœur vers YHWH, l'Elohîm d'Israël !
24:24	Le peuple dit à Yéhoshoua : Nous servirons YHWH, notre Elohîm et nous obéirons à sa voix.
24:25	Ce jour-là, Yéhoshoua traita alliance avec le peuple, et lui donna des lois et des ordonnances à Shekem.
24:26	Yéhoshoua écrivit ces paroles dans le livre de la torah d'Elohîm. Il prit une grande pierre<!--Cette pierre est une préfiguration de Yéhoshoua ha Mashiah (Jésus le Christ), le « Fidèle et Véritable » (Ap. 19:11). Voir commentaire en Es. 8:14.-->, qu'il dressa là sous le chêne qui était dans le sanctuaire de YHWH.
24:27	Yéhoshoua dit à tout le peuple : Voici, cette pierre deviendra un témoin contre nous, car elle a entendu toutes les paroles que YHWH nous a déclarées. Elle deviendra un témoin contre vous, afin que vous ne reniiez pas votre Elohîm.
24:28	Yéhoshoua renvoya le peuple, chaque homme à son héritage.

### Mort de Yéhoshoua (Josué) et d'Èl’azar ; ensevelissement des os de Yossef<!--Ge. 50:26.-->

24:29	Il arriva après ces choses que Yéhoshoua, fils de Noun, serviteur de YHWH, mourut fils de 110 ans.
24:30	On l’enterra dans le territoire de son héritage, à Timnath-Sérach, dans la montagne d'Éphraïm, du côté du nord de la montagne de Ga`ash.
24:31	Israël servit YHWH tous les jours de Yéhoshoua, et tous les jours des anciens dont les jours se prolongèrent après Yéhoshoua et qui avaient connu toute l’œuvre que YHWH avait faite pour Israël.
24:32	Les os de Yossef<!--Ge. 50:25 ; Ex. 13:19 ; Hé. 11:22.-->, que les fils d'Israël avaient fait monter d'Égypte, furent enterrés à Shekem, dans la portion du champ que Yaacov avait achetée des fils de Hamor, père de Shekem, pour 100 kesita. Cela devint la propriété des fils de Yossef.
24:33	Et Èl’azar, fils d'Aaron, mourut, on l'enterra à Guibeath-Phinées, qui avait été donné à son fils Phinées, dans la montagne d'Éphraïm.
